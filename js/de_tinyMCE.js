tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	plugins : "safari,advimage,advlink,inlinepopups,insertdatetime,paste,style,fullscreen",
	
	// Theme Options
	theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|formatselect,fontselect,fontsizeselect",
	theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
	theme_advanced_buttons3 : "visualaid,styleselect",
	theme_advanced_buttons3_add : "styleprops,fullscreen",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,
	
	external_image_list_url : "js/imageList.cfm",
	
	fullscreen_new_window : true,
	fullscreen_settings : {theme_advanced_path_location : "top"},
	
	content_css : "/css/stemacademy.css"
});