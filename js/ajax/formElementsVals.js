function formVals(frm) {
		var vals = {};
		for (var loop=0; loop < frm.elements.length; loop++) {
			if (frm.elements[loop].type == 'text' || frm.elements[loop].type == 'select-one' || frm.elements[loop].type == 'hidden' || frm.elements[loop].type == 'textarea') {
				vals[frm.elements[loop].name] = frm.elements[loop].value;
			} 
			else if ( frm.elements[loop].type == 'radio') {
				if (frm.elements[loop].checked == true) {
					vals[frm.elements[loop].name] = frm.elements[loop].value;
				}
			} 
			else if (frm.elements[loop].type == 'checkbox') {
				if (frm.elements[loop].checked == true) {
					if (vals[frm.elements[loop].name]) {
						vals[frm.elements[loop].name] = vals[frm.elements[loop].name] + ',' + frm.elements[loop].value;
					} else {
						vals[frm.elements[loop].name] = frm.elements[loop].value;
					}
				}
			}
		}
	return vals;
}