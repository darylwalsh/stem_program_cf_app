pSel = "tab-recent";
function landingTabs(el){
	$("tab-overlay").show();
	var tHeight = $("tabs-landing").getHeight() - 66;
	$("tab-overlay").style.height=tHeight+"px";

	var pnSel = $(pSel);
	var pnSelT = $(pSel+'-sel');
	var pnSelC = $(pSel+'-con');
	pnSelT.hide();
	//pnSel.appear({ duration: .5 });
	pnSel.show();
	pnSelC.hide();

	var sel = $(el);
	var selT = $(el+'-sel');
	var selC = $(el+'-con');
	sel.hide();
	selT.show();
	selC.show();

	//var tHeight = $("tabs-landing").getHeight() - 50;
	//$("tab-overlay").style.height=tHeight+"px";

	$("tab-overlay").fade();

	/*set selected*/
	pSel = el;
}
function _featuredVideo() {
	new deGlobalWin({
		div: new Element('div').insert( new Element('div',{id:'vidPlaceholder'}).update('Get the latest Adobe Flash plugin to see the video') ),
		loader: false,
		width: '400px',
		height: '225px',
		title: 'Featured Video'
	});
	setTimeout(_delaySWF,1000);
}
function _delaySWF() {
	if( $('vidPlaceholder') ){
		swfobject.embedSWF('/swf/media_player.swf','vidPlaceholder','400','225','8', null, {autoStart:'true',localHostURL:'/swf/redactedEducation_STEM_FINAL.flv'}, {allowfullscreen:'true',allowscriptaccess:'always'});
		$$('a.closeWin').each(function(E){E.href='#'});
	} else {
		//Wait for it...
		setTimeout(_delaySWF,1000);
	}
}
function verify() {
	var themessage = "none";
	if ($('criteria').value == "") {
		themessage = "Please enter one or more search terms.";
	}
	//alert if fields are empty and cancel form submit
	if (themessage == "none") {
		return true;
	}
	else {
		alert(themessage);
		return false;
	}
}

/*
function loginAjax() {
	alert('test');
	var username = $('strLoginUsername').value;
	var password = $('strLoginPassword').value;
	$('invalid').hide();
	$('strLoginUsername').disable();
	$('strLoginPassword').disable();
	$('login-loader').appear();

	alert('test2');

	new Ajax.Request('index.cfm?event=xhr.login', {
	  method: 'get',
	  parameters: {
	  	strLoginUsername: username,
	  	strLoginPassword: password},
	  onSuccess: function(transport) {
		var successText = String(transport.responseText);
		if (successText.indexOf('false')!=-1){

			alert('testFAIL');
			$('login-loader').fade();
			$('strLoginUsername').enable();
			$('strLoginPassword').enable();
			$('invalid').appear();
		}else{

			alert('testSUCCESS');
			var curHREF = String(location.href).toLowerCase();

			location.href = location.href; // always do this so it checks profile and forces it if needed.
			*/
			/*
			if (curHREF.indexOf(String("showSTEMApplication").toLowerCase()) >= 0 || curHREF.indexOf(String("showGiveaway").toLowerCase()) >= 0 || curHREF.indexOf(String("showResourceLanding").toLowerCase()) >= 0){
				location.href = location.href
			}
			*/
/*
			$('login-loader').hide();
			$('logins').update(transport.responseText);
	  	}
	  }
	})
}
*/
function getResourceId(text, li) {
	if(li.id != '') {
		window.location = 'index.cfm?event=showResource&resourceId=' + li.id;
	}
}
document.observe("dom:loaded", function() {
	new Ajax.Autocompleter("criteria", "criteriaSuggestions", "index.cfm?event=xhr.autosuggest", {
		afterUpdateElement : getResourceId
	});
	_hashPoll(_hu);
});
function _hu() {
	var hash = window.location.hash;
	if( hash.substr(0,1) == '#' ) hash=hash.substr(1);
	if( hash.indexOf('anchor') != -1 ) { return; }
	if( '_oldHash' in window && window._oldHash == hash ) return;
	window._oldHash=hash;
	if( hash == '' ) return;
	else if (hash == 'featured_video' ) return _featuredVideo();
	/*$$('.tabs a[href=#'+hash+']').invoke('up').invoke('addClassName','active').invoke('siblings').invoke('invoke','removeClassName','active');
	$$('a[name='+hash+']').invoke('up').invoke('show').invoke('siblings').invoke('invoke','hide');
	$$('.buttons button').invoke('observe','click',_sectionValidate);
	$$('input[type=file]').each(_genericUpload);*/
}
function _hashPoll(_F){
	var O=window, E='hashchange';
	if ( ('on'+E in O) && ('addEventListener' in O) ) {
		O.addEventListener(E, _F, true);
	} else {
		setInterval(_F,250);
		if( typeof(_ieFix) == 'function' ) _ieFix();
	}
	setTimeout(_F, 100);
}
function forgotPasswordAction()
{
	var emailAddress = $("lbEmail").value;

	new Ajax.Request('/cfcs/register.cfc?method=ForgotPass&email=' + emailAddress + '&returnFormat=plain', {
		  method: 'get',
		  onSuccess: function(transport) {
			var successText = String(transport.responseText).toLowerCase();
			if (successText.indexOf('success') >= 0){
				$('errorMsg').hide();
				$('successText').appear();
				window.setTimeout("clearWin()", 5000);
			}
			else {
				$('successText').hide();
				$('errorMsg').show();
				return;
			}
		  }
	});
}
function swapFields(hideField,showField) {
	$(hideField).hide();
	$(showField).show();
	$(showField).focus();
}
function checkField(hideField,showField) {
    if($(hideField).value == '') {
        $(showField).show();
        $(hideField).hide();
    }
}
function focusUsername(field) {
	var default_value = 'Email';
	if($(field).value == default_value) {
		$(field).value = '';
	}
}
function blurUsername(field) {
	if($(field).value == '') {
		$(field).value = 'Email';
	}
}