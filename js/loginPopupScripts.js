/**
*
*  URL encode / decode
*  http://www.webtoolkit.info/
*
**/

var Url = {
	// public method for url encoding
	encode : function (string) {
		return escape(this._utf8_encode(string));
	},
	 
	// public method for url decoding
	decode : function (string) {
		return this._utf8_decode(unescape(string));
	},
	
	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
	
		for (var n = 0; n < string.length; n++) {
			var c = string.charCodeAt(n);
	
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
		}
		return utftext;
	},
	 
	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
	
		while ( i < utftext.length ) {
			c = utftext.charCodeAt(i);
	
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
		}
		return string;
	}
}
	
	
function redirect(r) 
{
	window.location = r;
}


function ajaxSubmitForm(formName) 
{
	var form = $(formName);
	var r = Url.decode(form['r'].value);
	
	if (r == "") {
		r = location.href;
	}
	
	if (r == "contest") {
		r = location.href; 
		
		if (form["programInstitute"].value != "") {
			r += "&programInstitute=1";
		}
		if (form["programSTARsOR"].value != "") {
			r += "&programSTARsOR=1";
		}
		if (form["programSTARsPN"].value != "") {
			r += "&programSTARsPN=1";
		}
		
		
		if (form["programPreference1"].value != "") {
			r += "&programPreference1=" + form["programPreference1"].value;
		}
		if (form["programPreference2"].value != "") {
			r += "&programPreference2=" + form["programPreference2"].value;
		}
		
	}
	
	$('lb-invalid').hide();
	form.request({
		onSuccess: function(transport) {
			var successText = String(transport.responseText);
			if (successText.indexOf('false')!=-1){
				$('lb-invalid').appear();
			}else{
				//clearWin();
				$('lbLoginForm').hide();
				$('lbLoading').show();
				setTimeout("redirect('" + r + "');", 1000);
			}
		}
	});
}