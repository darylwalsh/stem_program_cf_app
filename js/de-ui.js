
// JavaScript Document
//Contains global UI functions




//Global Loader:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
//The Global loader currently provides 3 different sizes.  Small, Large, & Text (Text will only display text and no graphic)
//:::: binder = the div that will be matched in size and height by the white screen
//:::: text = text string to be displayed above the loader
//:::: size = takes a size parameter 'small' OR 'large' OR 'text' (note: 'text' removes the graphic and will only display the text past in the previous 'text' parameter)
//:::: newId = If declared, assigns an id to the loader for instances where multiple loading graphics may be present at once.  newId must be passed when clearLoad() is called, if supplied to globalLoad()
//ex: globalLoad('elemen-to-insert-into', 'text-to-display', 'small or large or text', 'optional: an id if multiple loaders are open');
globalLoad = function (binder, text, size, newId) {
		lHeight = $(binder).getHeight();
		lWidth = $(binder).getWidth();
		Element.insert($(binder), '<div id="contentLoader" class="contentLoader" style="height:' + lHeight + 'px;width:' + lWidth + 'px;" align="center"><div id="loaderImg' + size + '">' + text +'</div></div>');
		
		if (typeof newId != "undefined") {
			$('contentLoader').id = "contentLoader" + newId;
		}		
}


//Clear loader
clearLoad = function (closeId) {
	
	if (typeof closeId != "undefined") {
		Element.remove($('contentLoader' + closeId));
	} else {
		Element.remove($('contentLoader'));
	}
}





//Global Lightwindow:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
Note: globalWin utilizes the globalBase element that is created with the DE global header and footer
- to init an instance of the global light window, call globalWin('path-to-file','title of light window')
- globalWin will adapt to the size of the file being called.  The file should not contain body or html tags or links to styles.  Stylesheets, etc should be loaded into the parent document.
- a height and width must be defined in the top node/element of the file being loaded into the light window
*/

//check for IE
var IE = document.all?true:false;

globalWin = function (path,title) {
	
	//hide flash objects
	if($$("object")){
		var flashEls = $$("object");
		for (var i=0; i<flashEls.length; i++) {
			flashEls[i].style.visibility = "hidden";
		}
	}
		
	if($$("iframe")){
		var frames = $$("iframe");
		for (var i=0; i<frames.length; i++) {
			frames[i].style.visibility = "hidden";
		}
	}

	
	//create the screen
	var win = "<div id='globalWin' onclick='clearWin();' align='center'></div>";
	
	//insert into DOM
	Element.insert($('globalBase'), win);
	
	//set it's width and height
	var newDim = document.viewport.getHeight()
	var glblDim = $('globalBase').getHeight()
	if (newDim < glblDim){
		newHt = glblDim;	
	}else{
		newHt = newDim;	
	}

	var loaderCoord = newDim/2-80;

	$('globalWin').style.height = newHt+"px";
	$('globalWin').style.width = "100%";
	
	//if IE remove els by default	
	if	(IE) {
		removeEls('hide');
	}
	
	
	//create the content window
	var conWin = "<div id='conWin' align='center'><div id='winLoader'>Loading...<br><a href='#' onclick='clearWin();' id='winCancel'>cancel</a></div><div id='conWinInner' style='top:40px;visibility:hidden;' align='center'></div></div>";
	//insert into DOM
	$('globalWin').insert({before: conWin});
	resizeLoader();
	$('conWinInner').style.visibility = "hidden";
	

	//scroll observer
	Event.observe(window, 'scroll', function() {
		if ($('globalWin')){										 
			resizeWin(1);
		}
	});

	//resize observer
	Event.observe(window, "resize", function() {
		if ($('globalWin')){										 
			resizeWin(1);
		}
	});
	
	
	//creat the content window
	new Ajax.Updater('conWinInner', path, {
  		parameters: {  },
		evalScripts: true,
		onComplete: function(){
			var badTag = $$('#conWinInner title');
			var badTag2 = $$('#conWinInner meta');
			
			//var badTags = $$('title');
			//Element.remove($$('title'));
			for (var i=0; i<badTag.length; i++) {
				Element.remove(badTag[i]);
			}
			for (var i=0; i<badTag2.length; i++) {
				Element.remove(badTag2[i]);
			}
			
			var windowContent = $('conWinInner').firstDescendant();
			var conWidth = $(windowContent).getWidth();
			$('conWinInner').style.width = conWidth+"px";
			
			$('conWinInner').style.border = "1px solid #565656";
			
			if(title != ''){
				//insert the header
				var titleWin = "<h4 class='win' align='left'>"+title+"</h4>";
				$('conWinInner').insert({top: titleWin});
			}
			//insert close window link
			var closeWin = "<div id='closeWin' onclick='clearWin();'>X close</div>";
			$('conWinInner').insert(closeWin);
	
			resizeWin();
			Element.remove($('winLoader'));
			$('conWinInner').style.visibility = "";
			
		}
	});
	
}

removeEls = function(act) {
	if (act == 'hide'){
		$$('select').each(function(s,index) {
			s.hide();
		});
	} else {
		$$('select').each(function(s,index) {
			s.show();
		});
	}
}



resizeWin = function(observeCall) {
	//set a default for now
	var element = 'conWinInner';
	if($(element) != null) {
		if(typeof window.innerHeight != 'undefined') {
			
			yWithScroll = document.body.scrollHeight;
			var maxTop = Math.round(yWithScroll - $(element).getHeight());
			
			var newHeight = Math.round(document.viewport.getScrollOffsets().top + 
			((window.innerHeight - $(element).getHeight()))/2);

			if (newHeight < 0) {
				var newHeight = 1;
			} else if (newHeight > maxTop){//if the new position is great than the height of the lightwindow - body height
				var newHeight = maxTop - 1;
			}
			
			if (window.innerHeight < $(element).getHeight() && observeCall != 1){
				$(element).style.top = newHeight+'px';
			} else if (window.innerHeight > $(element).getHeight()){
				$(element).style.top = newHeight+'px';
			}
				
		} else {//this is IE primarily 
			
			yWithScroll = document.body.scrollHeight;
			var maxTop = Math.round(yWithScroll - $(element).getHeight());
			
			var newHeight = Math.round(document.viewport.getScrollOffsets().top + 
			((document.viewport.getHeight() - $(element).getHeight()))/2)
			
			if (newHeight < 0) {
				var newHeight = 1;
			} else if (newHeight > maxTop){
				var newHeight = maxTop - 1;
			}
		
			
			if (document.viewport.getHeight() < $(element).getHeight() && observeCall != 1){
				$(element).style.top = newHeight+'px';
			} else if (document.viewport.getHeight() > $(element).getHeight()){
				$(element).style.top = newHeight+'px';
			}
			
		}
	}
}

resizeLoader = function(observeCall) {
	//set a default for now
	var element = 'winLoader';
	if($(element) != null) {
		if(typeof window.innerHeight != 'undefined') {
			
			yWithScroll = document.body.scrollHeight;
			var maxTop = Math.round(yWithScroll - $(element).getHeight());
			
			var newHeight = Math.round(document.viewport.getScrollOffsets().top + 
			((window.innerHeight - $(element).getHeight()))/2);

			if (newHeight < 0) {
				var newHeight = 1;
			} else if (newHeight > maxTop){//if the new position is great than the height of the lightwindow - body height
				var newHeight = maxTop - 1;
			}
			
			if (window.innerHeight < $(element).getHeight() && observeCall != 1){
				$(element).style.top = newHeight+'px';
			} else if (window.innerHeight > $(element).getHeight()){
				$(element).style.top = newHeight+'px';
			}
				
		} else {//this is IE primarily 
			
			yWithScroll = document.body.scrollHeight;
			var maxTop = Math.round(yWithScroll - $(element).getHeight());
			
			var newHeight = Math.round(document.viewport.getScrollOffsets().top + 
			((document.viewport.getHeight() - $(element).getHeight()))/2)
			
			if (newHeight < 0) {
				var newHeight = 1;
			} else if (newHeight > maxTop){
				var newHeight = maxTop - 1;
			}
		
			
			if (document.viewport.getHeight() < $(element).getHeight() && observeCall != 1){
				$(element).style.top = newHeight+'px';
			} else if (document.viewport.getHeight() > $(element).getHeight()){
				$(element).style.top = newHeight+'px';
			}
			
		}
	}
}

clearWin = function () {
	Element.remove($('globalWin'));
	Element.remove($('conWin'));
	
	var flashEls = $$("object");
	for (var i=0; i<flashEls.length; i++) {
		flashEls[i].style.visibility = "visible";
	}
	
	if($$("iframe")){
		var frames = $$("iframe");
		for (var i=0; i<frames.length; i++) {
			frames[i].style.visibility = "visible";
		}
	}
	
	
	if	(IE) {
		removeEls('show');
	}	
	
	//special exception for player to hide embedded media
	//if (embedHide == 1){
//		embedHide = 1;
//		$('vidWindow').style.position = "relative";
//		$('vidWindow').style.left = "0px";
//	}
}


//Ratings:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
This script is created for a 5 star rating widget
Requirements:  prototype and scriptaculous and a single png star image

bgDefault = default background color behind stars (yellow probably)
bgHover = default background color behind stars when mouseover
starWidth = Important! - this will be multiplied by the rating to fill a container behind the transparent stars
*/

bgDefault = "#fab41b";
bgHover = "#0084ff";
starWidth = 24;
inHover = 0;

		ratingsAction = function(size){
			
			starWidth = size;
			
			
			//start observing mouseover on stars	
			initStars();
			//set the default star ratings to that of form field
			setRating();
			//grab the default rating and display the text
			var initRating = $('my-rating').value;
				if(initRating == 0 || initRating == null){
					var disText = ""
				}else{
					var disText = " | Your rating: "+initRating;	 
				}
				//update the filed
				$('rating-text').update(disText);
			
			$('rating-status').morph('background:'+bgDefault+'',0.1);
			
			//Set click events on stars
			Event.observe('rate-1','click',function() {
				setStars(this.id);
			}	
			);
			
			Event.observe('rate-2','click',function() {
				setStars(this.id);
			}	
			);
			
			Event.observe('rate-3','click',function() {
				setStars(this.id);
			}	
			);
			
			Event.observe('rate-4','click',function() {
				setStars(this.id);
			}	
			);
			
			Event.observe('rate-5','click',function() {
				setStars(this.id);
			}	
			);
			
			//observ mouseout on the ratings box to set the default rating back
			Event.observe('ratings-container','mouseout',function() {
				setTimeout("resetRating();",500);
			}	
			);
	
	}

	
	
	setRating = function(){
			$('rating-status').morph('background:'+bgDefault+'',0.1);
			var initRating = $('de-rating').value;
			slideRating(initRating*starWidth+'px');
	}
	
	resetRating = function(){
		//if user isn't hovering on a star, then reset the stars colors to that of the rating form field
		if(inHover != 1){
			$('rating-status').morph('background:'+bgDefault+'',0.1);
			var initRating = $('my-rating').value;
			slideRating(initRating*starWidth+'px');
		}
	}
	
	
	
	//start observing mouseover on stars
	initStars = function(){
			var sfEls = $$('ul.de-ratings li');///*td:not([class~=noHover])*/td with class=noHover will not be added
			for (var i=0; i<sfEls.length; i++) {
				var starNum = i+1;
				starAction(starNum);
			}	
	}
	
	starAction = function(starNum){
			Event.observe('rate-'+starNum,'mouseover',function() {
			follow(starNum*starWidth+'px');											
			inHover = 1;
			}	
			);
			Event.observe('rate-'+starNum,'mouseout',function() {
				inHover = 0;
			}	
			);	
	}
	
	//Stop observing the mouseover stars for a moment after one is selected
	stopStars = function(){
			Event.stopObserving('rate-1','mouseover');
			Event.stopObserving('rate-2','mouseover');
			Event.stopObserving('rate-3','mouseover');
			Event.stopObserving('rate-4','mouseover');
			Event.stopObserving('rate-5','mouseover');
			//provide a bit of buffer time then start observing mouseover again
			setTimeout("initStars();",2500);
	}
	
	

	setStars = function(rate){
			//$('rating-status').morph('background:'+bgDefault+'',0.1);
			$('rating-status').style.background = bgDefault;
			//stop observing mouseover for a few seconds
			stopStars();
			//fade text element
			//new Effect.Fade('rating-text');
			//determin which star was clicked
			switch( rate ) {
			case 'rate-1':
				var rating = 1;
				var newText = " | Your rating: 1";
				break;
			case 'rate-2':
				var rating = 2;
				var newText = " | Your rating: 2";
				break;
			case 'rate-3':
				var rating = 3;
				var newText = " | Your rating: 3";
				break;
			case 'rate-4':
				var rating = 4;
				var newText = " | Your rating: 4";
				break;	
			case 'rate-5':
				var rating = 5;
				var newText = " | Your rating: 5";
				break;	
			}
			//fade in updated text
			setTimeout("$('rating-text').update('"+newText+"');",1500);
			//set form field's value
			$('my-rating').value = rating;
			//Put your ajax call here to record rating
			var guidAssetId = document.getElementById('my-guid').value;
			new Ajax.Request('/ajax/rating_submit.cfm?rating=' + rating + '&guidAssetId=' + guidAssetId,
			{
				method:'get',
				onLoading: function() {
				},
				onSuccess: function(transport){
					$('rating_panel').toggle();
			    },
			    onFailure: function(){
			      	$('debug').update('error submitting rating');
			    }
			});
			
	}
	
	follow = function(state) {
		//$('rating-status').morph('background:'+bgHover+'',0);
		$('rating-status').style.background = bgHover;
		
		slideRating(state);
		//$('rating-status').style.width = state;
		
	}
	
	slideRating = function(state) {
		//$('rating-status').morph('width:'+state+'',0);
		$('rating-status').style.width = state;
	}
	


	//function to expose more options in the header search
	searchMore = function() {
		if ($('search-more-options').style.display=='none'){
			Effect.SlideDown('search-more-options', { duration: .5 });
		}
		
	}
	
	hideSearchMore = function() {
		Effect.SlideUp('search-more-options', { duration: .0 });

	}





	//DE Tabs
	//When using globalTabs()
	//(a).  use a domready observer to get all tabs w/ class=de-tab
	//(b).  pass in a function (f) argument -- this will be evaluated onclick
	globalTabs = function(f) {
		func = f;
		$$('a.de-tab').each(function(item) {
			item.observe('click', tabAction);
			
		});		
	}
	
	
	tabAction = function() {
		
		var tabID = this.id;
		var currClass=$(tabID).className;
		var pos=currClass.endsWith('-sel')
		
		if (pos!=true)
		{
			$$('a.de-tab').each(function(item) {
				item.className = currClass;
			});	
			
			$$('a.de-tab-sel').each(function(item) {
				item.className = currClass;
			});	
			
			$(tabID).className = currClass+"-sel";
			

			var fr = func; //Here I am passing a reference to function 'f', without parameters
			fr(tabID); //the 'f' function is invoked, without parameter			
			
			
		} else {
			return;
		}
	}
	


	function popWindow(url,width,height){
		// set the width and height of the playlist popup window to be created
		popupWidth = width;
		popupHeight = height;
		popupURL = url;
		// attempt to determine the width and height of the user's current window - default to 800x600
		windowWidth = 800;
		windowHeight = 600;
		try {
			if (self.innerWidth) {
				windowWidth = self.innerWidth;
				windowHeight = self.innerHeight;
			} else if (parent.document.documentElement && parent.document.documentElement.clientWidth) {
				windowWidth = parent.document.documentElement.clientWidth;
				windowHeight = parent.document.documentElement.clientHeight;
			} else if (document.documentElement && document.documentElement.clientWidth) {
				windowWidth = document.documentElement.clientWidth;
				windowHeight = document.documentElement.clientHeight;
			} else if (parent.document.body) {
				windowWidth = parent.document.body.clientWidth;
				windowHeight = parent.document.body.clientHeight;
			} else if (document.body) {
				windowWidth = document.body.clientWidth;
				windowHeight = document.body.clientHeight;
			}
		} catch(err) {
			windowWidth = 800;
			windowHeight = 600;
		}
	
		// determine the position of our popup window based on the current window size
		popupLeft = (windowWidth/2)-(popupWidth/2);
		popupTop = (windowHeight/2)-(popupHeight/2);
	
		myUrl = popupURL;
		myTarget = "refPopupWindow";
		myArgs = 'width='+popupWidth+',height='+popupHeight+',location=0,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=1,toolbar=0,hotkeys=0,top='+popupTop+',left='+popupLeft;
		refPopupWindow = window.open( myUrl, myTarget, myArgs );
		refPopupWindow.focus();
	}
	
	
	function popupWindow(url,width,height){
		// set the width and height of the playlist popup window to be created
		popupWidth = width;
		popupHeight = height;
		popupURL = url;
		// attempt to determine the width and height of the user's current window - default to 800x600
		windowWidth = 800;
		windowHeight = 600;
		try {
			if (self.innerWidth) {
				windowWidth = self.innerWidth;
				windowHeight = self.innerHeight;
			} else if (parent.document.documentElement && parent.document.documentElement.clientWidth) {
				windowWidth = parent.document.documentElement.clientWidth;
				windowHeight = parent.document.documentElement.clientHeight;
			} else if (document.documentElement && document.documentElement.clientWidth) {
				windowWidth = document.documentElement.clientWidth;
				windowHeight = document.documentElement.clientHeight;
			} else if (parent.document.body) {
				windowWidth = parent.document.body.clientWidth;
				windowHeight = parent.document.body.clientHeight;
			} else if (document.body) {
				windowWidth = document.body.clientWidth;
				windowHeight = document.body.clientHeight;
			}
		} catch(err) {
			windowWidth = 800;
			windowHeight = 600;
		}
	
		// determine the position of our popup window based on the current window size
		popupLeft = (windowWidth/2)-(popupWidth/2);
		popupTop = (windowHeight/2)-(popupHeight/2);
	
		myUrl = popupURL;
		myTarget = "refPopupWindow";
		myArgs = 'width='+popupWidth+',height='+popupHeight+',location=0,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=1,toolbar=0,hotkeys=0,top='+popupTop+',left='+popupLeft;
		refPopupWindow = window.open( myUrl, myTarget, myArgs );
		refPopupWindow.focus();
	}


	flashClearWin = function() {
		setTimeout("clearWin();",100);
	}
	
	flashCloseAddTo = function() {
		setTimeout("closeAddTo();",100);
	}