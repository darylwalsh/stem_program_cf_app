
function CheckLandingAppType(aform)
{
	var i = 0;
	var numbChecked = 0;
	
	if (aform.programInstitute.checked) {
		numbChecked++;
	}
	if (aform.programSTARsOR.checked) {
		numbChecked++;
	}
	if (aform.programSTARsPN.checked) {
		numbChecked++;
	}
	
	aform.programPreference1.innerHTML = "";
	aform.programPreference2.innerHTML = "";
	
	document.getElementById("firstPreference").style.display = "none";
	document.getElementById("secondPreference").style.display = "none";
	

	if (numbChecked >= 2){
		i = 0;
		
		aform.programPreference1[i] = new Option("- Select One -", "");
		i++
		
		if (aform.programInstitute.checked) {
			aform.programPreference1[i] = new Option("Institute", "Institute");
			i++;
		}
		if (aform.programSTARsOR.checked) {
			aform.programPreference1[i] = new Option("STARs - Oak Ridge", "STARs - Oak Ridge");
			i++;
		}
		if (aform.programSTARsPN.checked) {
			aform.programPreference1[i] = new Option("STARs - Pacific Northwest", "STARs - Pacific Northwest");
			i++;
		}
		
		document.getElementById("firstPreference").style.display = "";
	}
	
	if (numbChecked >= 3){
		i = 0;
		
		aform.programPreference2[0] = new Option("- Select One -", "");
		i++;
		
		if (aform.programInstitute.checked) {
			aform.programPreference2[i] = new Option("Institute", "Institute");
			i++;
		}
		if (aform.programSTARsOR.checked) {
			aform.programPreference2[i] = new Option("STARs - Oak Ridge", "STARs - Oak Ridge");
			i++;
		}
		if (aform.programSTARsPN.checked) {
			aform.programPreference2[i] = new Option("STARs - Pacific Northwest", "STARs - Pacific Northwest");
			i++;
		}
		
		document.getElementById("secondPreference").style.display = "";
	}
}

function CheckLandingPreference2 (aform) 
{
	var i = 0;
	
	aform.programPreference2.innerHTML = "";
	
	aform.programPreference2[0] = new Option("- Select One -", "");
	i++;
	
	if (aform.programInstitute.checked && aform.programPreference1.value != "Institute") {
		aform.programPreference2[i] = new Option("Institute", "Institute");
		i++;
	}
	if (aform.programSTARsOR.checked && aform.programPreference1.value != "STARs - Oak Ridge") {
		aform.programPreference2[i] = new Option("STARs - Oak Ridge", "STARs - Oak Ridge");
		i++;
	}
	if (aform.programSTARsPN.checked && aform.programPreference1.value != "STARs - Pacific Northwest") {
		aform.programPreference2[i] = new Option("STARs - Pacific Northwest", "STARs - Pacific Northwest");
		i++;
	}
}


function clearHidSchoolData () 
{
	document.getElementById('PID').value="";
	document.getElementById('schoolName').value="";
	document.getElementById('schoolAddress').value="";
	document.getElementById('schoolCity').value="";
	document.getElementById('schoolState').value="";
	document.getElementById('schoolZipCode').value="";
	document.getElementById('schoolPhone').value = "";
	document.getElementById('schoolCountry').value = "";
	
	document.getElementById('span_schoolName_text').innerHTML="<b><i>- none -</i></b>";
	document.getElementById('span_schoolAddress').innerHTML="";
	document.getElementById('span_schoolCity').innerHTML="";
	document.getElementById('span_schoolState').innerHTML="";
	document.getElementById('span_schoolZipCode').innerHTML="";
	document.getElementById('span_schoolPhone').innerHTML = "";
	document.getElementById('span_schoolCountry').innerHTML = "";
}

function copyToOpener (pid,school,address,city,state,state_display,zip,phone,country) 
{
	$('PID').value = pid;
	$('schoolName').value = school;
	$('schoolAddress').value = address;
	$('schoolCity').value = city;
	$('schoolState').value = state;
	$('schoolZipCode').value = zip;
	$('schoolPhone').value = phone;
	$('schoolCountry').value = country;
	
	$('span_schoolName_text').innerHTML = school;
	$('span_schoolAddress').innerHTML = address;
	$('span_schoolCity').innerHTML = city;
	$('span_schoolState').innerHTML = state_display;
	$('span_schoolZipCode').innerHTML = zip;
	$('span_schoolPhone').innerHTML = phone;
	$('span_schoolCountry').innerHTML = country;
	
	$('tblSchoolInfo').style.display = 'block';
	$('email').focus();
	
	clearWin();
}

function customCopy () 
{
	$('PID').value = "custom";
	$('schoolName').value = $('txtSchoolName').value;
	$('schoolAddress').value = $('txtSchoolAddress').value;
	$('schoolCity').value = $('txtSchoolCity').value;
	$('schoolState').value = $('txtSchoolState').value;
	$('schoolZipCode').value = $('txtSchoolZip').value;
	$('schoolPhone').value = $('txtSchoolPhone').value;
	$('schoolCountry').value = $('txtSchoolCountry').value;
	
	$('span_schoolName_text').innerHTML = $('txtSchoolName').value;
	$('span_schoolAddress').innerHTML = $('txtSchoolAddress').value;
	$('span_schoolCity').innerHTML = $('txtSchoolCity').value;
	var txtSchoolState = $('txtSchoolState');
	$('span_schoolState').innerHTML = txtSchoolState[txtSchoolState.selectedIndex].text;
	$('span_schoolZipCode').innerHTML = $('txtSchoolZip').value;
	$('span_schoolPhone').innerHTML = $('txtSchoolPhone').value;
	var txtSchoolCountry = $('txtSchoolCountry');
	$('span_schoolCountry').innerHTML = txtSchoolCountry[txtSchoolCountry.selectedIndex].text;
	
	$('tblSchoolInfo').style.display = 'block';
	$('email').focus();
	
	clearWin();
}

function emailChanged (emailObj)
{
	document.getElementById('span_email').className = "";
	document.getElementById('span_email').innerHTML = "";
	checkUserEmail(emailObj);
	document.getElementById('confirmEmail').focus();
}

function GetAttribute (obj, attName)
{
	var attValue = null;
	var attName = attName.toLowerCase();
	
	for (var i = obj.attributes.length - 1; i >= 0; i--) {
		if (obj.attributes[i].name.toLowerCase() == attName) {
			attValue = obj.attributes[i].value;
			break;
		}
	}
	
	return attValue;
}

function getCity(strState) 
{
	// turn off dropdowns to prevent an ajax error
	$("selState").disabled = true;
	$("selCity").disabled = true;
	$("processing").style.display = "";
	DWREngine._execute(_ajaxConfig.mdrCFC, null, 'getCities', strState, getCitiesResponse);
}

function getCitiesResponse (qResult) 
{
	if ( qResult.mcity.length > 0 ) {
		$("selCity").length = 0;
		$("selCity").options[0] = new Option("- Choose a City -", "",true);
		
		// finally load values into dropdown
		for (var i=0; i < qResult.mcity.length; i++) {
			$("selCity").options[i+1] = new Option(qResult.mcity[i], qResult.mcity[i],false);
		}
		// next enable dropdowns
		$("selState").disabled = false;
		$("selCity").disabled = false;
		$("processing").style.display = "none";
	}
	else {
		$("processing").style.display = "none";
		$("selState").disabled = false;
	}
}

function getRegions()
{
	var txtSchoolCountry = $("txtSchoolCountry");
	var txtSchoolState = $("txtSchoolState");
	
	if (txtSchoolCountry.getValue() == "") {
		txtSchoolState.length = 0;
		txtSchoolState.options[0] = new Option("- Select a Country -", "")
	}
	else {
		hideError("txtSchoolState");
		DWREngine._execute(_ajaxConfig.mdrCFC, null, "getRegions", txtSchoolCountry.getValue(), getRegionsResponse);
	}
}

function getRegionsResponse (qResults)
{
	var txtSchoolCountry = $("txtSchoolCountry");
	var txtSchoolState = $("txtSchoolState");
	
	txtSchoolState.length = 0;
	
	if (qResults.region_name.length <= 0){
		txtSchoolState.options[0] = new Option("- N/A -", "");
	}
	else {
		txtSchoolState.options[0] = new Option("- Select a State -", "");
		
		for (var i=0; i < qResults.region_name.length; i++) {
			if (String(qResults.region_code_alpha[i]).toLowerCase() != "null"){
				txtSchoolState.options[i + 1] = new Option(qResults.region_name[i], qResults.region_code_alpha[i]);
			}
			else {
				txtSchoolState.options[i + 1] = new Option(qResults.region_name[i], qResults.region_name[i]);
			}
		}
	}
}

function getSchool(strCity) 
{
	// turn off dropdowns to prevent an ajax error
	$("selState").disabled = true;
	$("selCity").disabled = true;
	$("processing").style.display = "";
	
	var vals = {};
	vals['strState'] = $("selState").value;	
	vals['strCity'] = strCity;
	
	DWREngine._execute(_ajaxConfig.mdrCFC, null, 'getSchools', vals, getSchoolResponse);
}

function getSchoolResponse (qResult) 
{			
	$("myTableData").innerHTML = qResult;
	// next enable dropdowns
	$("selState").disabled = false;
	$("selCity").disabled = false;
	$("processing").style.display = "none";
}

function getSchoolInfo()
{
	$('span_schoolName').className = '';
	$('tblSchoolInfo').className = '';
	$('tblSchoolInfo').setStyle({display:"none"});
	clearHidSchoolData();
		
	globalWin('/index.cfm?event=xhr.MDR_School_LookUpAjax','');
}

function hasAtLeastOneChecked (fieldsName)
{
	var checkedFields = $$("[name=" + fieldsName + "]");
	var hasChecked = false;
	
	for (var i = 0; i < checkedFields.length; i++){
		if (checkedFields[i].checked) {
			hasChecked = true;
			break;
		}
	}
	
	return hasChecked;
}

function hideError (fieldName) 
{
	$("span_" + fieldName).removeClassName("invalid").innerHTML = "";
}

function hideErrorRecommendation (errorID)
{
	$(errorID + "Label").setStyle({color:"#000000"});
}

function showError (errorID, errorMSG)
{
	$(errorID).addClassName("invalid").innerHTML = errorMSG;
	
	return false;
}

function showErrorRecommendation (errorID)
{
	$(errorID).setStyle({color:"#cc0000"});
	
	return false;
}

function tabPhoneNext (obj,strNext)	{
	if(obj.value.length >= obj.maxLength)	{
		document.getElementById(strNext).focus();
	}
}

function validateCantFindForm ()
{
	var isValid = true;
	
	if (Trim($("txtSchoolName").value) == "") {
		isValid = showError("span_txtSchoolName", "Required");
	}
	if (Trim($("txtSchoolAddress").value) == "") {
		isValid = showError("span_txtSchoolAddress", "Required");
	}
	if ($("txtSchoolCountry").selectedIndex == 0) {
		isValid = showError("span_txtSchoolCountry", "Required");
	}
	if ($("txtSchoolCountry").selectedIndex > 0 && $("txtSchoolState").options.length > 1 && $("txtSchoolState").selectedIndex == 0) {
		isValid = showError("span_txtSchoolState", "Required");
	}
	else {
		hideError("txtSchoolState");
	}
	if (Trim($("txtSchoolCity").value) == "") {
		isValid = showError("span_txtSchoolCity", "Required");
	}
	if (Trim($("txtSchoolZip").value) == "") {
		isValid = showError("span_txtSchoolZip", "Required");
	}
	if (Trim($("txtSchoolPhone").value) == "") {
		isValid = showError("span_txtSchoolPhone", "Required");
	}
	
	if (isValid) {
		customCopy();
	}
}

function validateRecommendationForm ()
{
	var isValid = true;
	
	$("btnSubmit").setStyle({display:"none"});
	$("processingForm").setStyle({display:""});
	
	if (Trim($("firstName").value) == "") {
		isValid = showErrorRecommendation("firstNameLabel");
	}
	if (Trim($("lastName").value) == "") {
		isValid = showErrorRecommendation("lastNameLabel");
	}
	if ($("title").selectedIndex == 0) {
		isValid = showErrorRecommendation("titleLabel");
	}
	if (Trim($("institution").value) == "") {
		isValid = showErrorRecommendation("institutionLabel");
	}
	if (Trim($("street").value) == "") {
		isValid = showErrorRecommendation("streetLabel");
	}
	if (Trim($("city").value) == "") {
		isValid = showErrorRecommendation("cityLabel");
	}
	if ($("state").selectedIndex == 0) {
		isValid = showErrorRecommendation("stateLabel");
	}
	if (!IsZip(Trim($("zip").value), true)) {
		isValid = showErrorRecommendation("zipLabel");
	}
	if (Trim($("phone").value) == "") {
		isValid = showErrorRecommendation("phoneLabel");
	}
	if (!IsEmail($("email").value, true)) {
		isValid = showErrorRecommendation("emailLabel");
	}
	if (Trim($("capacityKnowApplicant").value) == "") {
		isValid = showErrorRecommendation("capacityKnowApplicantLabel");
	}
	if (Trim($("describeApplicant").value) == "") {
		isValid = showErrorRecommendation("describeApplicantLabel");
	}
	if (!hasAtLeastOneChecked("categoryClassroomInstructionalSkills")) {
		isValid = showErrorRecommendation("categoryClassroomInstructionalSkillsLabel");
	}
	if (!hasAtLeastOneChecked("categorySubjectMatterKnowledge")) {
		isValid = showErrorRecommendation("categorySubjectMatterKnowledgeLabel");
	}
	if (!hasAtLeastOneChecked("categoryInitiativeAndSelfReliance")) {
		isValid = showErrorRecommendation("categoryInitiativeAndSelfRelianceLabel");
	}
	if (!hasAtLeastOneChecked("categoryCreativity")) {
		isValid = showErrorRecommendation("categoryCreativityLabel");
	}
	if (!hasAtLeastOneChecked("categoryIntegrity")) {
		isValid = showErrorRecommendation("categoryIntegrityLabel");
	}
	if (!hasAtLeastOneChecked("categoryLeadershipSkills")) {
		isValid = showErrorRecommendation("categoryLeadershipSkillsLabel");
	}
	if (!hasAtLeastOneChecked("categoryInterpersonalSkills")) {
		isValid = showErrorRecommendation("categoryInterpersonalSkillsLabel");
	}
	if (!hasAtLeastOneChecked("categoryOralCommunicationSkills")) {
		isValid = showErrorRecommendation("categoryOralCommunicationSkillsLabel");
	}
	if (!hasAtLeastOneChecked("categoryWrittenCommunicationSkills")) {
		isValid = showErrorRecommendation("categoryWrittenCommunicationSkillsLabel");
	}
	if (!hasAtLeastOneChecked("categoryTeamworkSkills")) {
		isValid = showErrorRecommendation("categoryTeamworkSkillsLabel");
	}
	
	if (isValid) {
		$("tblErrMessageTable").setStyle({display:"none"});
		//alert("lets submit the form");
		
		$("recommendationForm").submit();
	}
	else {
		$("tblErrMessageTable").setStyle({display:""});
		window.scrollTo(0, 320);
		
		$("processingForm").setStyle({display:"none"});
		$("btnSubmit").setStyle({display:""});
	}
}

function validateRegistrationEmail ()
{
	var isValid = true;
	
	if (Trim($("email").value) == "") {
		isValid = showError("span_email", "");
	}
	else if (!IsEmail($("email").value, true)){
		isValid = showError("span_email", "Invalid email");
	}
	if (Trim($("confirmEmail").value) == "") {
		isValid = showError("span_confirmEmail", "");
	}
	else if (Trim($("confirmEmail").value).toLowerCase() != Trim($("email").value).toLowerCase()){
		isValid = showError("span_confirmEmail", "Not a match");
	}
	
	
	return isValid;
}

function validateRegistrationForm ()
{
	var isValid = true;
	
	document.getElementById("btnSubmit").style.display = "none";
	document.getElementById("processingForm").style.display = "";
	document.getElementById("tblErrMessageTable").style.display = "none";
	
	if (Trim($("firstName").value) == "") {
		isValid = showError("span_firstName", "");
	}
	if (Trim($("lastName").value) == "") {
		isValid = showError("span_lastName", "");
	}
	if ($("title").selectedIndex == 0) {
		isValid = showError("span_title", "");
	}
	if (Trim($("areaPhone").value) == "" || Trim($("prePhone").value) == "" || Trim($("phone").value) == "") {
		isValid = showError("span_phone", "");
	}
	if (!hasAtLeastOneChecked("locationType")) {
		isValid = showError("span_locationType", "");
	}
	if (!hasAtLeastOneChecked("grades")) {
		isValid = showError("span_grades", "");
	}
	if (!hasAtLeastOneChecked("subjects")) {
		isValid = showError("span_subjects", "");
	}
	if (!hasAtLeastOneChecked("studentCount")) {
		isValid = showError("span_studentCount", "");
	}
	if (!hasAtLeastOneChecked("yearsTeaching")) {
		isValid = showError("span_yearsTeaching", "");
	}
	if (Trim($("span_schoolAddress").innerHTML) == ""){
		isValid = showError("span_schoolName", "");
	}
	if (validateRegistrationEmail() == false){
		isValid = false;
	}
	if (validateRegistrationPassword() == false){
		isValid = false;
	}
	if ($("SECURITY_QUESTION_GUID").selectedIndex == 0) {
		isValid = showError("span_SECURITY_QUESTION_GUID", "");
	}
	if (Trim($("SECURITY_QUESTION_ANSWER").value) == "") {
		isValid = showError("span_SECURITY_QUESTION_ANSWER", "");
	}
	
	if (isValid) {
		new Ajax.Request('/cfcs/register.cfc?method=emailExists', {
			method: "post",
			parameters: {email:$("email").value},
			onSuccess: function(resp) {
				if(Trim(resp.responseText).toLowerCase() == "true"){
					isValid = showError("span_email", "Already exists.<br /> Try another or<br /> go login.");
					
					document.getElementById("btnSubmit").style.display = "";		
					document.getElementById("processingForm").style.display = "none";
					document.getElementById("tblErrMessageTable").style.display = "";
					
					document.getElementById("email").focus();
				}
				else if(Trim(resp.responseText).toLowerCase() == "false"){
					
					new Ajax.Request('/cfcs/register.cfc?method=registerUser', {
						method: "post",
						parameters: $("registrationForm").serialize(true),
						onSuccess: function (resp2) {
							
							if (Trim(resp2.responseText).toLowerCase() == "true") {
								alert("Error, try again later.");
				
								document.getElementById("btnSubmit").style.display = "";		
								document.getElementById("processingForm").style.display = "none";
								document.getElementById("tblErrMessageTable").style.display = "";
							}
							else if (Trim(resp2.responseText).toLowerCase() == "false") {
								location.href = fwdURL; // SUCCESS !!!! User info saved ... now foward to index page
							}
						},
						onError: function (resp2) {
							alert("Error, try again later.");
				
							document.getElementById("btnSubmit").style.display = "";		
							document.getElementById("processingForm").style.display = "none";
							document.getElementById("tblErrMessageTable").style.display = "";
						}
					});
					
				}
			},
			onFailure: function(response) {
				alert("Error, try again later.");
				
				document.getElementById("btnSubmit").style.display = "";		
				document.getElementById("processingForm").style.display = "none";
				document.getElementById("tblErrMessageTable").style.display = "";
			}
		});
	}
	else {
		document.getElementById("btnSubmit").style.display = "";		
		document.getElementById("processingForm").style.display = "none";
		document.getElementById("tblErrMessageTable").style.display = "";
		
		window.scrollTo(0,0);
	}
}

function validateRegistrationPassword ()
{
	var isValid = true;
	
	if (Trim($("passwordField").value) == "") {
		isValid = showError("span_password", "");
	}
	else if (Trim($("passwordField").value).length <= 4)	{
		isValid = showError("span_password", "Too short");
	}
	
	if (Trim($("confirmPassword").value) == "") {
		isValid = showError("span_confirmPassword", "");
	}
	else if (Trim($("confirmPassword").value) != Trim($("passwordField").value)){
		isValid = showError("span_confirmPassword", "Not a match");
	}
	
	
	return isValid;
}

	
	