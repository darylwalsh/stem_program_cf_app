/*_________________________________________________________________________________*/
	
	/*  MDR  */
	function clearHidSchoolData () {
		document.getElementById('hidPID').value="";
		document.getElementById('hidSchoolName').value="";
		document.getElementById('hidSchoolAddress').value="";
		document.getElementById('hidSchoolCity').value="";
		document.getElementById('hidSchoolState').value="";
		document.getElementById('hidSchoolZip').value="";
		document.getElementById('hidSchoolPhone').value = "";
		document.getElementById('hidSchoolCountry').value = "";
		
		document.getElementById('spanSchoolName').innerHTML="<b><i>- none -</i></b>";
		document.getElementById('spanAddress').innerHTML="";
		document.getElementById('spanCity').innerHTML="";
		document.getElementById('spanState').innerHTML="";
		document.getElementById('spanZip').innerHTML="";
		document.getElementById('spanPhone').innerHTML = "";
		document.getElementById('spanCountry').innerHTML = "";
	}	
	
	function getMDRForm () {
		displayFloatingDiv('Choose School',895,720,'hidden');
		DWREngine._execute(_ajaxConfig.userAjaxCFC,null,'getPublicPage',"/views/MDR_School_LookUpAjax.cfm",resultGetMDRForm);
		//document.getElementById("flashBox").style.display = "none";
	}
	
	function resultGetMDRForm (r) {
		document.getElementById("divHiddenForm").innerHTML = r.html;
	}
/*_________________________________________________________________________________*/



	function tabNext (obj,strNext)	{
		if(obj.value.length >= obj.maxLength)	{
			document.getElementById(strNext).focus();
		}
	}
	


/*_________________________________________________________________________________*/
	/* Email section */
	
	function checkUserEmail (obj) {
		var at="@"
		var dot="."
		var lat=obj.value.indexOf(at)
		var lstr=obj.value.length
		var ldot=obj.value.indexOf(dot)
		
		var blnInvalidEmail = false;
		
		if (obj.value.indexOf(at)==-1){
		   blnInvalidEmail = true;
		}

		if (obj.value.indexOf(at)==-1 || obj.value.indexOf(at)==0 || obj.value.indexOf(at)==lstr){
		   blnInvalidEmail = true;
		}

		if (obj.value.indexOf(dot)==-1 || obj.value.indexOf(dot)==0 || obj.value.indexOf(dot)==lstr){
			blnInvalidEmail = true;
		}

		 if (obj.value.indexOf(at,(lat+1))!=-1){
			blnInvalidEmail = true;
		 }

		 if (obj.value.substring(lat-1,lat)==dot || obj.value.substring(lat+1,lat+2)==dot){
			blnInvalidEmail = true;
		 }

		 if (obj.value.indexOf(dot,(lat+2))==-1){
			blnInvalidEmail = true;
		 }
		
		 if (obj.value.indexOf(" ")!=-1){
			blnInvalidEmail = true;
		 }
		 
		 if (blnInvalidEmail == true) {
			 if (obj.id == "strUsername")	{
				dimmerEnablePage();
				document.getElementById("spanstrUsername").className  = "invalid";
				document.getElementById("spanstrUsername").innerHTML = "Invalid Email.";
				document.getElementById("strUsername").focus();
			 }
			 else if (obj.id == "strEmail")	{
				dimmerEnablePage();
				document.getElementById("spanstrEmail").className  = "invalid";
				document.getElementById("spanstrEmail").innerHTML = "Invalid Email.";
				document.getElementById("strEmail").focus();
			 }
			 else {
				dimmerEnablePage();
				document.getElementById("spanstrConfirmEmail").className  = "invalid";
				document.getElementById("spanstrConfirmEmail").innerHTML = "Invalid Email.";
				document.getElementById("strConfirmEmail").focus();
			 }
		 }
		 else {
			 if (obj.id == "strUsername")	{
				checkUserEmailAjax(obj.value)
			 }
			 else {
				document.getElementById("spanstrConfirmEmail").className  = "";
				document.getElementById("spanstrConfirmEmail").innerHTML = "";
			 }
		 }
		 return blnInvalidEmail
	}
	
	function checkUserEmailAjax (strUN) {
		DWREngine._execute(_ajaxConfig.userAjaxCFC,null,'checkUserName',strUN,resultCheckUserEmailAjax);
	}
	
	function resultCheckUserEmailAjax (r) {
		document.getElementById("spanstrUsername").innerHTML = r.strmessage;
	}
	
	function confirmEmails () {
		if (document.getElementById("strEmail").value.replace(/^\s+|\s+$/g,"") != document.getElementById("strConfirmEmail").value.replace(/^\s+|\s+$/g,"") ) {
			document.getElementById("spanstrConfirmEmail").className  = "invalid";
			document.getElementById("spanstrConfirmEmail").innerHTML = "Emails do not match.";
			document.getElementById("strEmail").focus();
			return false;
		}
		else if (document.getElementById("strEmail").value.replace(/^\s+|\s+$/g,"") == document.getElementById("strConfirmEmail").value.replace(/^\s+|\s+$/g,"") ) {
			var blnInvalid = checkUserEmail (document.getElementById("strEmail"));
			if (blnInvalid)	{
				return false;	
			}
			else {
				document.getElementById("spanstrConfirmEmail").className  = "";
				document.getElementById("spanstrConfirmEmail").innerHTML = "";
				return true;	
			}
		}
		else {
			document.getElementById("spanstrConfirmEmail").className  = "";
			document.getElementById("spanstrConfirmEmail").innerHTML = "";
			return true;
		}
	}

/*_________________________________________________________________________________*/

	function validatePassword (str) {
		if (str.replace(/^\s+|\s+$/g,"") == "")	{
			document.getElementById("spanstrPassword").className  = "invalid";
			document.getElementById("spanstrPassword").innerHTML = "No Password.";
			document.getElementById("strPassword").focus();
		}
		else if (str.replace(/^\s+|\s+$/g,"").length < 4)	{
			document.getElementById("spanstrPassword").className  = "invalid";
			document.getElementById("spanstrPassword").innerHTML = "Too short.";
			document.getElementById("strPassword").focus();
		}
		else if (str.replace(/^\s+|\s+$/g,"") == document.getElementById("strUsername").value)	{
			document.getElementById("spanstrPassword").className  = "invalid";
			document.getElementById("spanstrPassword").innerHTML = "Can't match Username.";
			document.getElementById("strPassword").focus();
		}
		else {
			var blnErr = false
			var strListCantUser = "user,username,test,pass,password,free,blank";	// can't use these words for a password
			var valueArray = strListCantUser.split(",");
			for(var z=0; z<valueArray.length; z++)	{
				if (valueArray[z] == str) 	{
					blnErr = true
				}
			}
			if (blnErr == true)	{
				document.getElementById("spanstrPassword").className  = "invalid";
				document.getElementById("spanstrPassword").innerHTML = "Too simple.";
				document.getElementById("strPassword").focus();
			}
			else {
				document.getElementById("spanstrPassword").className  = "";
				document.getElementById("spanstrPassword").innerHTML = "";
				
			}
		}
	}

	function validateConfirmPassword () {
		if (document.getElementById("strConfirmPassword").value.replace(/^\s+|\s+$/g,"") != "")	{
			if (document.getElementById("strPassword").value.replace(/^\s+|\s+$/g,"") != document.getElementById("strConfirmPassword").value.replace(/^\s+|\s+$/g,"")) {
				dimmerEnablePage();
				document.getElementById("spanstrPassword").className  = "invalid";
				document.getElementById("spanstrPassword").innerHTML = "Not match.";
				document.getElementById("strPassword").focus();
			}
			else {
				document.getElementById("spanstrPassword").className  = "";
				document.getElementById("spanstrPassword").innerHTML = "";	
			}
		}
	}
/*_________________________________________________________________________________*/
	/* Validate form */
	
	function validateForm (docForm) {
		var blnError=false;
		var strRadioList = "";
		var strRadioErrList = "";
		var strRadioID = "";
		var strCheckList = "";
		var strCheckErrList = "";
		var strCheckID = "";
		
		dimmerDisablePage();  //found in dimmingDIV.js
		document.getElementById("btnSubmit").value="Wait ...";		
		document.getElementById("processingForm").style.display = "";
		
		
		for (var i=0; i < docForm.elements.length; i++) {
			if (docForm.elements[i].type == 'text' || docForm.elements[i].type == 'select-one' || docForm.elements[i].type == 'textarea' || docForm.elements[i].type == 'password') {
				if (docForm.elements[i].value.replace(/^\s+|\s+$/g,"") == "")	{
					if (document.getElementById("span" + docForm.elements[i].id)) {
						document.getElementById("span" + docForm.elements[i].id).className  = "invalid";	// no user data provide, show error graphic for this element
					}
					blnError=true;
				}
				else {
					if (document.getElementById("span" + docForm.elements[i].id)) {	// user provided info for this element, so hide error graphic
						document.getElementById("span" + docForm.elements[i].id).className  = "";
						document.getElementById("span" + docForm.elements[i].id).innHTML  = "";
					}
				}				
			} 
			else if ( docForm.elements[i].type == 'radio') {
				if (strRadioID != docForm.elements[i].name) {	// just make a list of Radio button names for now
					strRadioID = docForm.elements[i].name;
					strRadioList = strRadioList + docForm.elements[i].name + ",";					
				}
			} 
			else if (docForm.elements[i].type == 'checkbox') {
				if (strCheckID != docForm.elements[i].name) {	// just make a list of Checkbox names for now
					strCheckID = docForm.elements[i].name;
					strCheckList = strCheckList + docForm.elements[i].name + ",";					
				}
			}
		}
	//end of loop
	
	if (blnError == true) 	{
		dimmerEnablePage();
		document.getElementById("btnSubmit").value="  Submit   ";		
		document.getElementById("processingForm").style.display = "none";
		document.getElementById("tblErrMessageTable").style.display = "";
		window.scrollTo(0,0);
	}
	
	if (confirmEmails ()) {
		/* GAiello - 09-30-2008 - Only Require to pick School if the users Title is NOT a Siemens Volunteer */
		if ((document.getElementById("hidSchoolName").value == "") && (document.getElementById("strTitle").value != "Siemens Volunteer"))	{
			document.getElementById("spanhidSchoolName").className  = "invalid";
			blnError=true;
		}
		else	{
			document.getElementById("spanhidSchoolName").className  = "";
		}

/* GAiello - 09-30-2008 - the checkboxes are no longer required on the Siemens form */
/*
		// radio button section
			var valueArray = strRadioList.split(",");
			for(var j=0; j<valueArray.length; j++){
			  blnChecked=false;
			  for (var k=0; k < document.getElementsByName(valueArray[j]).length; k++) {
				if (document.getElementsByName(valueArray[j])[k].checked == true) {
					if (document.getElementById("span" + valueArray[j])) {
						document.getElementById("span" + valueArray[j]).style.display = "none"; // no user data provide, show error graphic for this element
					}
					blnChecked=true;				
				}		  
			  }
			  
			  if (blnChecked == false)	{
				strRadioErrList = strRadioErrList + valueArray[j] + ",";
			  }
			}
			
			var valueArray = strRadioErrList.split(",");
			for(var m=0; m<valueArray.length; m++)	{
				if (document.getElementById("span" + valueArray[m])) {
					document.getElementById("span" + valueArray[m]).style.display = "";		// user provided info for this element, so hide error graphic				
				}
				if (valueArray[m] != "") {
					blnError=true;
				}
			}
		// end of - radio button section
		
		// check box section
			var valueArray = strCheckList.split(",");
			for(var n=0; n<valueArray.length; n++){
			  blnChecked=false;
			  for (var p=0; p < document.getElementsByName(valueArray[n]).length; p++) {
				if (document.getElementsByName(valueArray[n])[p].checked == true) {
					if (document.getElementById("span" + valueArray[n])) {
						document.getElementById("span" + valueArray[n]).style.display = "none";	// no user data provide, show error graphic for this element
					}
					blnChecked=true;
				}		  
			  }
			  
			  if (blnChecked == false)	{
				strCheckErrList = strCheckErrList + valueArray[n] + ",";
			  }
			}
			
			var valueArray = strCheckErrList.split(",");
			for(var q=0; q<valueArray.length; q++)	{
				if (document.getElementById("span" + valueArray[q])) {
					document.getElementById("span" + valueArray[q]).style.display = "";		// user provided info for this element, so hide error graphic					
				}
				if (valueArray[q] != "") {
					blnError=true;
				}
			}
		// end of - check box section
		
*/
			if (blnError == true) 	{
				dimmerEnablePage();
				document.getElementById("btnSubmit").value="  Submit   ";		
				document.getElementById("processingForm").style.display = "none";
				document.getElementById("tblErrMessageTable").style.display = "";
				window.scrollTo(0,0);
			}
			else {
				//valid email before submit
				DWREngine._execute(_ajaxConfig.userAjaxCFC,null,'checkUserName',document.getElementById("strUsername").value,resultSubmitCheckUserEmailAjax);			
			}
			
		}
	}
	
	function resultSubmitCheckUserEmailAjax (r) {
		if (r.blninvalid != "true")	{
			if (r.blnduplicate != "true")	{
				// alls good now save
				DWREngine._execute(_ajaxConfig.registerAjaxCFC,null,'createSponsorUser',DWRUtil.serializeForm(document.getElementById("frmCreateUser")),resultCreateSponsorUser);			
			}
			else {
				// Duplicate email
				document.getElementById("spanstrUsername").className  = "invalid";
				document.getElementById("spanstrUsername").innerHTML = "Try another one.";
				document.getElementById("strUsername").focus();
				dimmerEnablePage();
			}
		}
		else {
			document.getElementById("spanstrUsername").className  = "invalid";
			document.getElementById("spanstrUsername").innerHTML = "Invalid Email.";
			document.getElementById("strUsername").focus();
			dimmerEnablePage();
		}		
	}
	
	function resultCreateSponsorUser (r) {
		if (r.blnerror == "true")	{
		// error
			dimmerEnablePage();
			alert("An error has occured while trying to Save User Info.");
			document.getElementById("divErrMsg").innerHTML = r.strmessage;
		}
		else {
			// SUCCESS !!!! User info saved ... now foward to index page
			location.href = "../index.cfm";
		}
	}