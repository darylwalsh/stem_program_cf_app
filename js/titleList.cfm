<cfsilent>
	<cfset isAuthenticated = false />
	<cfset region_code = "00" />
	<cfif StructKeyExists(cookie,"app_user_guid") AND IsValid("guid",cookie.app_user_guid) AND cookie.app_user_guid NEQ "00000000-0000-0000-0000-000000000000">
		<cfset isAuthenticated = true />
		<cfif StructKeyExists(client,"region_code")>
			<cfset region_code = ListAppend(region_code,client.region_code) />
		</cfif>
	</cfif>
	<cfquery name="qAssets" datasource="Sponsorships">
		SELECT
			r.title
		FROM cms_resources r
		WHERE	0=0
		AND		r.siteId = '#request.siteId#'
		ORDER BY r.title ASC
	</cfquery>
	<cfset output = "var titleList = [" />
	<cfif qAssets.RecordCount GT 0>
		<cfloop query="qAssets">
			<cfset output = output & '"#application.udf.maxLength(title,45)#",' />
		</cfloop>	
		<cfset output = Left(output,Len(output)-1) />
	</cfif>
	<cfset output = output & "];" />
</cfsilent>
<cfcontent type="text/javascript" reset="true" /><cfoutput>#output#</cfoutput>