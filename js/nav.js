function addEvent(obj, type, fn) {
	if( obj.attachEvent ) {
			obj["e"+type+fn] = fn;
			obj[type+fn] = function(){obj["e"+type+fn]( window.event );};
			obj.attachEvent( "on"+type, obj[type+fn] );
	} else {
			obj.addEventListener( type, fn, true );
	};
}

function initMenus() {
	/*Activate/deactivate menus*/
	$$('.smenu').each(function(s,index){
							   
		addEvent(s,'click',function(){
			if($('smenu'+s.id.replace('a','')).visible()){
				
				$('la'+s.id.replace('a','')).className = 'aro_a';
				Effect.BlindUp($('smenu'+s.id.replace('a','')), {duration:0.2}); //ScriptAculoUs Method
				
			} else {
				
				$('la'+s.id.replace('a','')).className = 'aro_b';			
				Effect.BlindDown($('smenu'+s.id.replace('a','')), {duration:0.2}); //ScriptAculoUs Method
				
			}
		});
	});
	
	
	/*effects on menu*/
//	$$('dl#menu dt').each(function(s,index){
//		addEvent(s,'mouseover',function(){
//			//new Effect.Highlight(s, {duration:4, startcolor:'#EFF3FF', endcolor:'#72ACDC', restorecolor:'#72ACDC'});
//		});
//	});
}

addEvent(window, 'load', initMenus);



