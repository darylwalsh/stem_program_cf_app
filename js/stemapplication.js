var _rules = {
	email: {regexp:/^[a-z0-9._-]+@([a-z0-9-]+\.)+[a-z]{2,6}$/i,msg:'Enter a valid email address'},
	phone: {regexp:/^\W?(\d{3})\W*(\d{3})\W?(\d{4})$/,msg:'Enter a 10-digit phone number'},
	zip: {regexp:/^\d{5}(-\d{4})?$/,msg:'Enter a valid zip code'},
	numeric: {regexp:/^\d+/,msg:'Enter a number'}
};
function _genericUpload(E, i){
	var id = E.identify(),
		_c = E.className,
		fv = {
			pathUploadScript: "http://awesome.redactededucation.com/functions/builder-upload.cfm",
			type: 'Videos',
			maxupload: 104857600,
			completeFunc:_getPrefixVal('func',_c,'fileUp'+i)
		},
		params = { allowScriptAccess: "always", wmode: 'transparent' },
		hi= new Element('input');
		hi.type='hidden';
		hi.name=_getPrefixVal('name',_c,E.name);
		hi.className=_c;
		hi.id=id;
	if ( !(fv.completeFunc in window) ){
		window[fv.completeFunc] = function(fn) { hi.value=fn; };
	}
	var atts = {id: id+i, 'class':'simple-upload'};
	E.insert({after:hi});
	swfobject.embedSWF("http://static.redactededucation.com/global/swf/generic-upload.swf",
		id, "535", "150", "9", null, fv, params, atts);
}
function _getPrefixVal(prefix, list, def) {
	var d = def || '';
	var r = new RegExp(prefix+'-[a-zA-Z0-9-_]+');
		a = list.match(r);
	return a?a[0].substr(prefix.length+1):d;
}
function _sectionValidate(_evt) {
	_validate( _evt.element().up().up() );
}
function _datePicker(E) {
	var opts ={formElements:{}};
	opts.formElements[E.identify()]="m-sl-d-sl-Y";
	datePickerController.createDatePicker(opts);
}
function _addRow(E, i) {
	E.insert( new Element('button').update('Add Another').observe('click',_repeatRow) );
}
function _repeatRow(_evt) {
	_evt.stop();
	var E = _evt.element().up();
	if ( _validate(E) ) {
		var b = _evt.element().remove();
		var r = E.clone(true).insert(b);
		r.descendants().select(_isPhone).invoke('observe','blur',_phoneFormat);
		_clearLabels(E).insert({before: r});
		r.childElements().each(_rowItemIncrement);
		E.insert((new Element('button')).update('Delete').observe('click',_rowDelete));
	}
}
function _clearLabels(E) {
	E.addClassName('no-label').childElements().each(_clearLabel);
	return E;
}
function _clearLabel(E) {
	var c=E.childElements().find(_isInput);
	if (!c) return;
	c.previousSiblings().invoke('remove');
	if( !(E.firstChild instanceof Element) ) {
		E.removeChild( E.firstChild );
	}
}
function _isPhone (E) {
	return E.hasClassName('phone');
}
function _isError (E) {
	return E.hasClassName('error');
}
function _rowItemIncrement(E) {
	var attr=['name','id'], a, b, c=E.childElements().find(_isInput);
	if (!c) return;
	if( ('type' in c) && ['radio','checkbox'].member(c.type) ) {
		c.checked = false;
	} else {
		c.value = '';
	}
	while( a=attr.pop() ) {
		if( (a in c) && (b=c[a].match(/^(.*[^0-9])([0-9]+)$/)) ){
			c[a]=b[1]+(parseInt(b[2])+1);
		}
	}
	if ( c.hasClassName('date') ){
		c.nextSiblings().invoke('remove');
		_datePicker(c);
	}
	if( b=c.className.match(/active_when-(.*[^0-9])([0-9]+)-/) ){
		c.className=c.className.replace(new RegExp(b[1]+b[2]),b[1]+(parseInt(b[2])+1))
		_activeWhen(c);
	}
}
function _validate(E) {
	return _removeErrors(E).descendants().select(_isInput).map(_validateField).all();
}
function _validateField(E) {
	if ( E.hasClassName('req') && $F(E) == '' && !(('disabled' in E) && E.disabled) ) return _msg(E,'Field is required'), false;
	if ( E.hasClassName('date') && isNaN(Date.parse($F(E))) ) return _msg(E,'Invalid Date'), false;
	for(var rule in _rules){
		if( E.hasClassName(rule) && !!$F(E) && !($F(E).match(_rules[rule].regexp)) ) {
			return _msg(E,_rules[rule].msg), false;
		}
	}
	return true;
}
function _removeErrors(E) {
	E.descendants().select(_isError)
		.invoke('removeClassName','error')
		.select(_hasTitle)
		.invoke('removeAttribute','title');
	return E;
}
function _msg(E, msg) {
	var label = E.ancestors().find(_isLabel);
	if (!label && ('id' in E) ) {
		 var a=$$('[for='+E.id+']');
		 if (a.size() > 0) label=a[0];
	}
	( label || E )
		.addClassName('error').title = msg;
}
function _isInput(a) {
	var inputs = ['input','select','textarea'];
	return inputs.member(a.tagName.toLowerCase());
}
function _isButton(a) {
	return a.tagName.toLowerCase() == 'button';
}
function _hasTitle(E) {
	return ('title' in E) && (!!E.title);
}
function _isLabel(a) {
	return a.tagName.toLowerCase() == 'label';
}
function _rowDelete(_evt) {
	_evt.element().up().remove();
}
function _activeWhen(E) {
	var c = E.className.match(/active_when-[a-z0-9_-]+/i);
	if(!c) return;
	c=c[0].split('-');
	if ( !(c[1] in window._toggle) ) window._toggle[c[1]]=[];
	window._toggle[c[1]].push({id:E.identify(),value:c[2]});
	$$('[name='+c[1]+']').invoke('observe','change',_activeToggle);
}
function _activeToggle(_evt) {
	var e=_evt.element(), c, bool;
	if( e.name in window._toggle ){
		for(var i=0,j=window._toggle[e.name].length; i<j; i++ ){
			c=window._toggle[e.name][i];
			bool = ( $F(e) != c.value );
			$(c.id)[bool?'removeClassName':'addClassName']('req').disabled = bool;
		}
	}
}
function _wysiwyg () {
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,advimage,advlink,inlinepopups,insertdatetime,paste,style,fullscreen",
		editor_selector:'wysiwyg',
		// Theme Options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "visualaid,styleselect",
		theme_advanced_buttons3_add : "styleprops,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
	
		external_image_list_url : "js/imageList.cfm",
	
		fullscreen_new_window : true,
		fullscreen_settings : {theme_advanced_path_location : "top"},
	
		content_css : "/css/stemacademy.css"
	});
}
function _formInit(){
	$$('a.section-anchor').invoke('hide');
	$$('.tabs').invoke('firstDescendant').invoke('addClassName','active').invoke('down').pluck('href').each(function(h){
		var hash = h.split('#').pop();
		$$('a[name='+hash+']').invoke('up').invoke('show').invoke('siblings').invoke('invoke','hide');
	});
	window._toggle = {};
	$$('[class*=active_when]').each(_activeWhen);
	$$('input.date').each(_datePicker);
	$$('.repeatable').each(_addRow);
	$$('input.phone').invoke('observe','blur',_phoneFormat);
	setTimeout(_wysiwyg,1000);
}
Event.observe(document,'dom:loaded',_formInit);
function _ieFix() {
	$$('table tr').each(function(E, i){ if(i%2) E.addClassName('alt'); });
}
function _phoneFormat (_evt) {
	var E = _evt.element();
	var n = $F(E).match(_rules.phone.regexp);
	if ( n ) {
		E.setValue('('+n[1]+') '+n[2]+'-'+n[3]);
	}
}
