<cfsilent>
	<cfset output = "" />
	<cfset delimiter = "\n" />	
	<cfset output = output & "var tinyMCEImageList = new Array(" />	
	<cfset host = "http://#cgi.http_host#" />
	<cfset imgPath = "/img/" />
	<cfset imgDirectory = GetDirectoryFromPath(ExpandPath(imgPath)) />
	<cfset filters = "*.jpg|*.jpeg|*.gif|*.png" />
	
	<cfif DirectoryExists(imgDirectory)>
		<cfdirectory action="list" directory="#imgDirectory#" name="dirQuery" filter="#filters#" sort="name ASC" />
		<cfloop query="dirQuery">
			<cfif dirQuery.type EQ "File">
				<cfset output = output & '["#name#","#host##imgPath##URLEncodedFormat(name)#"],' />
			</cfif>
		</cfloop>		
		<cfset output = Left(output,Len(output)-1) />
	</cfif>
	
	<cfset output = output & ");" />
</cfsilent>
<cfcontent type="text/javascript" reset="true" /><cfoutput>#output#</cfoutput>