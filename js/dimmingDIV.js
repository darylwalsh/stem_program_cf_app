// GAiello - 09-2008
//
// global variables
//
var isFF=0;
var isIE=0;
var isSafari=0;
var isPC=0;
var isMAC=0;
var isMozilla = (document.all) ? 0 : 1;

var X=0;
var Y=0;

var xPos=0;	
var yPos=0;

var over = false;
var originalDivHTML = "";
var objDiv = null; // needed for moving window
var objDivObj = null;

if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1)	{
	isFF = 1;
}

if (navigator.userAgent.toLowerCase().indexOf('msie') > -1)	{
	isIE = 1;
}

if (navigator.userAgent.toLowerCase().indexOf('safari') > -1)	{
	isSafari = 1;
}

if (navigator.appVersion.toLowerCase().indexOf('windows') > -1)	{
	isPC=1;
}
else {
	isMAC=1;
}


// GAiello - patch for IE 6 - it can't handle the selectboxes properly with the floating div
function IE6patch(strVisibility) {
	for(i=0; i<document.getElementsByTagName('select').length; i++) {
		document.getElementsByTagName('select')[i].style.visibility = strVisibility; //hide all select boxes
	};
	for(i=0; i<document.getElementsByTagName('object').length; i++) {
		document.getElementsByTagName('object')[i].style.visibility = strVisibility; //hide all select boxes
	};
	if (document.getElementById("autocomplete")) {
		document.getElementById("autocomplete").style.visibility = strVisibility; // search box in header
	}
}


function MouseDown(e)	{
    if (over)
    {
		objDivObj = document.getElementById("windowcontent");
        if (isMozilla) {
            X = e.layerX;
            Y = e.layerY;
            return false;
        }
        else {
            objDivObj = objDiv.style;
            X = event.offsetX;
            Y = event.offsetY;
        }
    }
}


function MouseMove(e)	{
    if (objDivObj) {
        if (isMozilla) {
            objDivObj.style.top = (e.pageY-Y) + 'px';
            objDivObj.style.left = (e.pageX-X) + 'px';
            return false;
        }
        else	{
            objDivObj.pixelLeft = event.clientX-X + document.body.scrollLeft;
            objDivObj.pixelTop = event.clientY-Y + document.body.scrollTop;
            return false;
        }
		
    }
	
	/* GAiello - below my addition to get mouse CoOrdinates */
	if (isIE) { // grab the x-y pos.s if browser is IE
		xPos = event.clientX + document.body.scrollLeft;
		yPos = event.clientY + document.body.scrollTop;
	}
	else {  // grab the x-y pos.s if browser is FF
		xPos = e.pageX;
		yPos = e.pageY;
	}
	
	
}

function MouseUp()	{
    objDivObj = null;
}

function init () {
	/* needed to move window */
	 if (isMozilla)  {
		document.captureEvents(Event.MOUSEDOWN | Event.MOUSEMOVE | Event.MOUSEUP);
	}

    document.onmousedown = MouseDown;
    document.onmousemove = MouseMove;
    document.onmouseup = MouseUp;	
}

function buildDimmerDiv(dimmerColor)	{
	
	if (document.getElementsByTagName("div")["globalBase"]) {	// if global  header is available
		w = document.getElementsByTagName("div")["globalBase"].offsetWidth;
		h = document.getElementsByTagName("div")["globalBase"].offsetHeight;	
	}
	else {
		// GAiello -05-20-2008 - extra code to cover the IE background		
		if	(isIE) {
			w =	(window.screen.width - 20); //Internet Explorer
		}
		else {
			w =	(window.innerWidth - 95); // FireFox Amaro - changes this property for Moz and Safari Mac
		}
		
		h = window.screen.height;
		if (document.body.parentNode.scrollHeight > h)	{
			h = document.body.parentNode.scrollHeight;
		}
		if (document.body.scrollHeight > h) {
			h = document.body.scrollHeight;
		}
	}
	
	if ((dimmerColor == undefined) || (dimmerColor == "")) {
		dimmerColor = "#000000";	// default to black background
	}
	var objDivDimmer = document.createElement("div");
	objDivDimmer.id = "shade";
	objDivDimmer.style.cssText = "height: "+ h +"px; width: "+ w +"px; position: absolute; top:0px; left:0px; z-index:2; background-color: " + dimmerColor + "; opacity:.75; filter: alpha(opacity=75); -moz-opacity: 0.75;";
	document.body.appendChild(objDivDimmer);
	init ();
}

function CenterLightWindow(divWidth, divHeight) {
	var vals = {}; 
	vals['leftPos']=0; 
	vals['topPos']=0;
	
	if (isIE) {
	   w = document.body.clientWidth;
	   h = document.body.clientHeight;
	}
	else {
	   w = window.innerWidth;
	   h = window.innerHeight;
	}
	vals['leftPos'] = (w-divWidth)/2;
	vals['topPos'] = 40;
	return vals;
}


function displayFloatingDiv(title, width, height, noScroll)	{
	IE6patch("hidden"); //GAiello - 12-17-2007
	buildDimmerDiv();
	
	if(width == undefined)	{width=600;}
	if(height == undefined)	{height=400;}
	if(noScroll == undefined)	{noScroll="auto";}
	
	stcVals = CenterLightWindow(width, height);
	
	var addHeader;
	
	
	addHeader = '<div id="dimStage" style="width:' + width + 'px; height:' + height + 'px; overflow:' + noScroll + ';">' +
	            '<a href="javascript:closeFloatingDiv(\'windowcontent\')" id="dimClose">Close [X]</a>';
				
	var innerDivHeight = height;
	lightWindowContent = "<div id='divHiddenForm' style='background-color: #ffffff; vertical-align:top; overflow:" + noScroll + "; height: "+ innerDivHeight +"px;'><br /><br /><br /><br /><br />" +
						"<table align='center' width='100%' border='0' cellpadding='2' cellspacing='0'>" +
						"<tr><td align='center' class='loading'>Please Wait...</td></tr><tr><td align='center'><img src='/images/loader.gif' name='wait' title='Please Wait' /></td></tr>" +
						"</div></div>";	
	
	objDiv = document.createElement("div");
	objDiv.id = "windowcontent";
	objDiv.style.cssText = "height: "+ height +"px; width: "+ width +"px; position: absolute; top:" + stcVals['topPos'] + "px; left:" + stcVals['leftPos']  + "px; z-index:3; background-color: #FFF; overflow:" + noScroll + ";";
	objDiv.innerHTML = addHeader + lightWindowContent;
	document.body.appendChild(objDiv);	
	
	
}

function closeFloatingDiv(strDiv)	{
	IE6patch("visible"); //GAiello - 12-17-2007
	document.body.removeChild(document.getElementById(strDiv));
	document.body.removeChild(document.getElementById("shade"));	
}

function dimmerDisablePage () {
	buildDimmerDiv("#FFFFFF");	// white background
}

function dimmerEnablePage () {
	if (document.getElementById("shade")) {
		document.body.removeChild(document.getElementById("shade"));
	}
}

