	function IsAnyCheckboxSelected(checkbox)
	{
		var selected = false;
		
		if (checkbox)
		{
			if (checkbox.length && String(checkbox.length).length > 0 && checkbox[0].type.toLowerCase() == "checkbox")
			{
				for (var i = 0; i < checkbox.length; i++)
				{
					if (checkbox[i].checked)
					{
						selected = true;
						break;
					}
				}
			}
			else if (checkbox.type.toLowerCase() == "checkbox")
			{
				if (checkbox.checked)
				{
					selected = true;
				}
			}
		}
		
		return selected;
	}


	function Trim(astring)
	{
		return astring.replace(/^\s*/, "").replace(/\s*$/, ""); //Trim the start and end of string;
	}
	
	
	// this function doesn't except commas, dollar signs
	function IsDBDollarFormat(dollar_value)
	{
		regex = /^([1-9]{1}\d*|0){0,1}\.\d{2}$/;
		retval = regex.test(dollar_value);
		return retval;
	}
	
	
	
	function IsDollarFormat(dollar_value) // must have commas but doesn't need $
	{
		regex = /^\${0,1}(([1-9]{1}\d{0,2}){1}(\,\d{3}){0,}|0){0,1}\.\d{2}$/;
		retval = regex.test(dollar_value);
		return retval;
	}
	
	
	function IsInteger(object_value)
	{
		if (object_value.length == 0)
			return true;
	
		var decimal_format = ".";
		var check_char = object_value.indexOf(decimal_format);
	
		if (check_char == -1)
			return IsNumber(object_value);
		else
			return false;
	}
	
	
	function IsInNumberRange(object_value, min_value, max_value)
	{
		if (min_value != null)
		{
			if (object_value < min_value)
				return false;
		}
	
		if (max_value != null)
		{
			if (object_value > max_value)
				return false;
		}
	
		return true;
	}
	
	
	function IsNumber(object_value)
	{	
		if (object_value.length == 0)
			return true;
	
		var start_format = " .+-0123456789";
		var number_format = " .0123456789";
		var check_char;
		var decimal = false;
		var trailing_blank = false;
		var digits = false;
	
		check_char = start_format.indexOf(object_value.charAt(0));
	
		if (check_char == 1)
			decimal = true;
		else if (check_char < 1)
			return false;
	
		for (var i = 1; i < object_value.length; i++)
		{
			check_char = number_format.indexOf(object_value.charAt(i));
			if (check_char < 0)
				return false;
			else if (check_char == 1)
			{
				if (decimal)
					return false;
				else
					decimal = true;
			}
			else if (check_char == 0)
			{
				if (decimal || digits)	
					trailing_blank = true;
			}
			else if (trailing_blank)
				return false;
			else
				digits = true;
		}	
	
		return true
	}
	
	
	function IsInRange(object_value, min_value, max_value)
	{
		if (object_value.length == 0)
			return true;
	
		if (!IsNumber(object_value))
			return false;
		else
			return (IsInNumberRange((eval(object_value)), min_value, max_value));
	
		return true;
	}



	function IsDay(checkYear, checkMonth, IsDay) {
		maxDay = 31;
	
		if (checkMonth == 4 || checkMonth == 6 || checkMonth == 9 || checkMonth == 11)
			maxDay = 30;
		else if (checkMonth == 2)
		{
			if (checkYear % 4 > 0)
				maxDay =28;
			else if (checkYear % 100 == 0 && checkYear % 400 > 0)
				maxDay = 28;
			else
				maxDay = 29;
		}
	
		return IsInRange(IsDay, 1, maxDay);
	}



	function IsDate(object_value, required) {
	 //Returns true if value is a date format or is NULL
	 //otherwise returns false
	//trim whitespace before we validate
	
		object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');
		object_value = object_value = object_value.replace(/{d \'/, '').replace(/'}/, '');
		
		if( required )
		{
			if( object_value.length == 0 )
			{
				return false;
			}
		}else{
			if( object_value.length == 0 )
			{
				return true;
			}
		}
		
		
		 if (object_value.length == 0)
			 return true;
		
		
		 //Returns true if value is a date in the mm/dd/yyyy format
		isplit = object_value.indexOf('/');
		splitchr = "/";
		
		if (isplit == -1)
		{
			isplit = object_value.indexOf('.');
			splitchr = ".";
		}
		
		if (isplit == -1)
		{
			isplit = object_value.indexOf('-');
			splitchr = "-";
		}
		
		if (isplit == -1 || isplit == object_value.length)
			return false;
		
		var element1 = object_value.substring(0, isplit);
		// check for yyyy-mm-dd format
		if( element1.length == 4 )
		{
			sYear = object_value.substring(0, isplit);
			isplit = object_value.indexOf(splitchr, isplit + 1);
			if (isplit == -1 || (isplit + 1 ) == object_value.length)
					return false;
			sMonth = object_value.substring((sYear.length + 1), isplit);
			sDay = object_value.substring(isplit + 1);
			
		 } else {
			 
			sMonth = object_value.substring(0, isplit);
			isplit = object_value.indexOf(splitchr, isplit + 1);
			 if (isplit == -1 || (isplit + 1 ) == object_value.length)
					return false;
			sDay = object_value.substring((sMonth.length + 1), isplit);
			sYear = object_value.substring(isplit + 1);
		 }
	
		 if (!IsInteger(sMonth)) //check month
				 return false;
		 else if (!IsInRange(sMonth, 1, 12)) //check month
				 return false;
		 else if (!IsInteger(sYear)) //check year
				 return false;
		 else if( sYear.length != 1 && sYear.length != 2 && sYear.length != 4 )
				return false;
		 else if (!IsInRange(sYear, 0, 9999)) //check year
				 return false;
		 else if (!IsInteger(sDay)) //check day
				 return false;
		 else if (!IsDay(sYear, sMonth, sDay)) // check day
				 return false;
		 else
				 return true;
	}
	
	

	function IsValidRegex(object_value, regexPattern, required) {
		if(required) {
			if(object_value.length == 0) {
				return false;
			}
		} else {
			if(object_value.length == 0) {
				return true;
			}
		}
	
		return regexPattern.test(object_value);
	}



	function IsTime(object_value, required) {
		object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');
		// replace the {t'..'} format around the timestamp if it exists
		object_value = object_value = object_value.replace(/{t \'/, '').replace(/'}/, '');
	
		if(required) {
			if(object_value.length == 0)
			{
				return false;
			}
		} else {
			if(object_value.length == 0) {
				return true;
			}
		}

		var result = IsValidRegex(object_value, /^(([0-1]?[0-9]|[2][1-4]):([0-5]?[0-9])(:[0-5]?[0-9])?).?([AP]M|[AP]m|[ap]m|[ap]M)?$/, required);
		return result;
	}
	


	/**
	 * validate that the value is formatted as a telephone correctly
	 * This pattern matches any US Telephone Number.
	 * This regular expression excludes the first number, after the area code,from being 0 or 1;
	 * it also allows an extension to be added where it does not have to be prefixed by 'x'.
	 *
	 * Matches: 
	 * 617.219.2000 
	 * 219-2000
	 * (617)283-3599 x234
	 * 1(222)333-4444
	 * 1 (222) 333-4444
	 * 222-333-4444
	 * 1-222-333-4444
	 * Non-Matches: 
	 * 44-1344-458606
	 * +44-1344-458606
	 * +34-91-397-6611
	 * 7-095-940-2000
	 * +7-095-940-2000
	 * +49-(0)-889-748-5516
	*/
	function IsPhone(object_value, required) {
		//trim whitespace before we validate
		object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');
	
		if(required) {
			if( object_value.length == 0 ) {
				return false;
			}
		} else {
			if( object_value.length == 0 ) {
				return true;
			}
		}
	
		if (object_value.length == 0) return true;		
	
		return IsValidRegex(object_value, /^(((1))?[ ,\-,\.]?([\\(]?([1-9][0-9]{2})[\\)]?))?[ ,\-,\.]?([^0-1]){1}([0-9]){2}[ ,\-,\.]?([0-9]){4}(( )((x){0,1}([0-9]){1,5}){0,1})?$/, required);
	}





	/**
	 * validate that the value is formatted as a USA zipcode correctly
	 *
	 * This pattern will match any us zip code
	 * matches: 12345, 12345-1234
	 */
	function IsZip(object_value, required)
	{
		//trim whitespace before we validate
		object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');
	
		if(required) {
			if( object_value.length == 0 ) {
				return false;
			}
		} else {
			if( object_value.length == 0 ) {
				return true;
			}
		}
	
		return IsValidRegex(object_value, /^([0-9]){5,5}$|(([0-9]){5,5}(-| ){1}([0-9]){4,4}$)/, required);
	}




	function IsCreditCard(object_value, required) {
		//trim whitespace before we validate
		object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');
	
		if(required) {
			if(object_value.length == 0) {
				return false;
			}
		} else {
			if( object_value.length == 0 ) {
				return true;
			}
		}
	
		if (object_value.length == 0) return true;

		var white_space = " -";
		var creditcard_string="";
		var check_char;

		for (var i = 0; i < object_value.length; i++) {
			check_char = white_space.indexOf(object_value.charAt(i));
			
			if (check_char < 0) creditcard_string += object_value.substring(i, (i + 1));
		}	
	
		if (creditcard_string.length < 13 || creditcard_string.length > 19)
			return false;
	
		if (creditcard_string.charAt(0) == "+")
			return false;
	
		if (!IsInteger(creditcard_string))
			return false;
	
		var doubledigit = creditcard_string.length % 2 == 1 ? false : true;
		var checkdigit = 0;
		var tempdigit;
	
		for (var i = 0; i < creditcard_string.length; i++) {
			tempdigit = eval(creditcard_string.charAt(i));
	
			if (doubledigit) {
				tempdigit *= 2;
				checkdigit += (tempdigit % 10);
	
				if ((tempdigit / 10) >= 1.0) checkdigit++;
	
				doubledigit = false;
				
			} else {
				
				checkdigit += tempdigit;
				doubledigit = true;
			}
		}	
	
		return (checkdigit % 10) == 0 ? true : false;
	}


	/**
	 * validate that the value is formatted as a SSN# correctly
	 *
	 * this Pattern that the SSN number matches the different formats. (X = [0-9])
	 * matches: XXX-XX-XXXX, XXX XX XXXX, 
	 * non-match: XXXXXXXXX
	 */
	function IsSSN(object_value, required)
	{
		//trim whitespace before we validate
		object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');
	
		if(required) {
			if(object_value.length == 0) {
				return false;
			}
		} else {
			if(object_value.length == 0) {
				return true;
			}
		}
	
		return IsValidRegex(object_value, /^[0-9]{3}(-| )[0-9]{2}(-| )[0-9]{4}$/, required);
	}
	
	
	
	
	/**
	 * validate that the value is formatted as an email address correctly
	 *
	 * this regex matches the majoriity of all email address.
	 * example matches.
	 * Matches:  [rick.jones@unit.army.mil], [john_doe@foobar.com], [foo99@foo.co.uk]  
	 * Non-Matches:  [find_the_mistake.@foo.org], [.prefix.@some.net]
	 *
	 * _CF_IsURL mailto uses this same email regex - keep in sync.
	 */
	function IsEmail(object_value, required) {
		//trim whitespace before we validate
		object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');
	
		if(required) {
			if(object_value.length == 0) {
				return false;
			}
		} else {
			if( object_value.length == 0 ) {
				return true;
			}
		}
	
	
		//return _CF_IsValidRegex(object_value, /^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/);
		//return _CF_IsValidRegex(object_value, /^[a-zA-Z_0-9-]+(?:\.[a-zA-Z_0-9-]+)*@(?:[a-zA-Z_0-9-]+\.)+[a-zA-Z]{2,7}$/);
		return IsValidRegex(object_value, /^[a-zA-Z_0-9-]+(\.[a-zA-Z_0-9-]+)*@([a-zA-Z_0-9-]+\.)+[a-zA-Z]{2,7}$/, required);
	}





	/**
	 * validate that the value is formatted correctly for a http/https/ftp url
	 * This pattern will match http/https/ftp urls.
	 *
	 * Matches: http://www.mm.com/index.cfm
	 *          HTTP://WWW.MM.COM
	 *          http://www.mm.com/index.cfm?userid=1&name=mike+nimer
	 *          http://www.mm.com/index.cfm/userid/1/name/mike+nimer - trick used by cf developers so search engines can parse their sites (search engines ignore query strings)
	 *          ftp://www.mm.com/
	 *          ftp://uname:pass@www.mm.com/
	 *          mailto:email@address.com
	 *          news:rec.gardening
	 *          news:rec.gardening
	 *          http://a/
	 *			file://ftp.yoyodyne.com/pub/files/foobar.txt
	 * Non-Matches: www.yahoo.com
	 *              http:www.mm.com
	 *
	 * _CF_IsEmail - the mailto email check is the same as the _CF_IsEmail regex (keep in sync)
	 */
	function IsURL(object_value, required)
	{
		//trim whitespace before we validate
		object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');
	
		if(required) {
			if(object_value.length == 0) {
				return false;
			}
		} else {
			if(object_value.length == 0) {
				return true;
			}
		}
	
		//return _CF_IsValidRegex(object_value.toLowerCase(), /^((http|https|ftp|file)\:\/\/([a-zA-Z0-0]*:[a-zA-Z0-0]*(@))?[a-zA-Z0-9-\.]+(\.[a-zA-Z]{2,3})?(:[a-zA-Z0-9]*)?\/?([a-zA-Z0-9-\._\?\,\'\/\+&amp;%\$#\=~])*)|((mailto)\:[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z0-9]{2,7})|((news)\:[a-zA-Z0-9\.]*)$/);
		return IsValidRegex(object_value.toLowerCase(), /^((http|https|ftp|file)\:\/\/([a-zA-Z0-0]*:[a-zA-Z0-0]*(@))?[a-zA-Z0-9-\.]+(\.[a-zA-Z]{2,3})?(:[a-zA-Z0-9]*)?\/?([a-zA-Z0-9-\._\?\,\'\/\+&amp;%\$#\=~])*)|((mailto)\:[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]{2,7})|((news)\:[a-zA-Z0-9\.]*)$/, required);
	}





	/*
	 * A string UUID value is required. A UUID is a string
	 * of length 35 formatted as XXXXXXXX-XXXX-XXXX-XXXXXXXXXXXXXXXX, where X is a
	 * hexadecimal digit (0-9 or A-F).
	 */
	 
	function IsUUID(object_value, required)
	{
		//trim whitespace before we validate
		object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');
	
		if(required) {
			if(object_value.length == 0) {
				return false;
			}
		} else {
			if(object_value.length == 0) {
				return true;
			}
		}
	
		return IsValidRegex(object_value, /[A-Fa-f0-9]{8,8}-[A-Fa-f0-9]{4,4}-[A-Fa-f0-9]{4,4}-[A-Fa-f0-9]{16,16}/, required);
	}