<!---
License:
Copyright 2007 GreatBizTools, LLC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Copyright: GreatBizTools, LLC
$Id$

Created version: 1.0.0
Updated version: 1.1.1

Notes:
The PluginManager only calls plugin points that are utilized.
Remove any un-implemented plugin point methods (i.e. preProcess(), etc.)
to improve application performance as fewer plugin points will
be called on each request.  For example if your plugin only implements the
preEvent plugin point, then remove the remaining points. (pfarrell)
--->
<cfcomponent 
	displayname="SecurityPlugin" 
	extends="MachII.framework.Plugin" 
	output="false"
	hint="This plugin gets user details and populates cookies.">

	<!---
	PROPERTIES
	--->
	
	<!---
	INITIALIZATION / CONFIGURATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the plugin.">
	</cffunction>	
	<!---
	PUBLIC FUNCTIONS
	--->	
	<cffunction name="preEvent" access="public" returntype="void" output="false">
		<cfargument name="eventContext" type="MachII.framework.EventContext" required="true" />
		
		<!---
		<cfset var user = "" />
		<cfset request.loggedIn = false />
		
		<cfif StructKeyExists(cookie,"app_user_guid") AND IsValid("guid",cookie.app_user_guid) AND cookie.app_user_guid NEQ "00000000-0000-0000-0000-000000000000">
			<cfif  StructKeyExists(cookie, "blnAuthenticatedStem")>
				<cfset request.loggedIn = true />
			<cfelse>
		       	<cfquery name="qGlobalUserCheck" datasource="#getProperty('userDsn')#">
				SELECT APP_USER_GUID
                		, FIRST_NAME
                    	, LAST_NAME
                    	, EMAIL
					FROM APP_USER (NOLOCK)
    	        	WHERE app_user_guid = <cfqueryparam value="#cookie.app_user_guid#" cfsqltype="cf_sql_idstamp" />
    	        </cfquery>            
        	    <cfif qGlobalUserCheck.recordcount>
            	    <cfcookie name="FIRST_NAME" value="#qGlobalUserCheck.FIRST_NAME#"  />
                	<cfcookie name="LAST_NAME" value="#qGlobalUserCheck.LAST_NAME#"  />
	                <cfcookie name="EMAIL" value="#qGlobalUserCheck.EMAIL#"  />
					<cfcookie name="blnAuthenticatedStem" value="true"  />
					<cfset request.loggedIn = true />
            	</cfif>
			</cfif>
		</cfif>
		--->
	</cffunction>
</cfcomponent>