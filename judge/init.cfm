<cfsilent>
	<cfset variables.dsn = "Sponsorships" />
	<cfquery name="qApplications" datasource="#variables.dsn#">
		SELECT DISTINCT
			su.sponsor_user_guid,
			su.LAST_NAME,
			su.FIRST_NAME,
			su.EMAIL,
			si.applicationStatus,
			si.applicationId
		FROM SPONSOR_FORM AS f INNER JOIN
        SPONSOR_FORM_SECTION AS s ON f.FORM_GUID = s.FORM_GUID INNER JOIN
        SPONSOR_FORM_SECTION_FIELD AS sfs ON s.FORM_SECTION_GUID = sfs.FORM_SECTION_GUID INNER JOIN
        SPONSOR_FORM_RESPONSE AS sfr ON sfs.FORM_SECTION_FIELD_GUID = sfr.FORM_SECTION_FIELD_GUID INNER JOIN
        SPONSOR_USER AS su ON sfr.SPONSOR_USER_GUID = su.SPONSOR_USER_GUID
		LEFT JOIN stemInstitute si on si.sponsor_user_guid = su.sponsor_user_guid
		ORDER BY su.FIRST_NAME
	</cfquery>
	<cfloop query="qApplications">
		<cfif applicationId EQ "">
			<cfquery name="qInsert" datasource="#variables.dsn#">
				INSERT INTO stemInstitute
				(sponsor_user_guid,applicationStatus)
				VALUES
				(<cfqueryparam value="#sponsor_user_guid#">,'To Be Reviewed')
			</cfquery>
		</cfif>
	</cfloop>
</cfsilent>