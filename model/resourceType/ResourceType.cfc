<cfcomponent displayname="ResourceType" output="false">
		<cfproperty name="resource_type_id" type="numeric" default="" />
		<cfproperty name="resourceId" type="string" default="" />
		<cfproperty name="typeId" type="string" default="" />
		
	<!---
	PROPERTIES
	--->
	<cfset variables.instance = StructNew() />

	<!---
	INITIALIZATION / CONFIGURATION
	--->
	<cffunction name="init" access="public" returntype="model.resourceType.ResourceType" output="false">
		<cfargument name="resource_type_id" type="string" required="false" default="" />
		<cfargument name="resourceId" type="string" required="false" default="" />
		<cfargument name="typeId" type="string" required="false" default="" />
		
		<!--- run setters --->
		<cfset setresource_type_id(arguments.resource_type_id) />
		<cfset setresourceId(arguments.resourceId) />
		<cfset settypeId(arguments.typeId) />
		
		<cfreturn this />
 	</cffunction>

	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="setMemento" access="public" returntype="model.resourceType.ResourceType" output="false">
		<cfargument name="memento" type="struct" required="yes"/>
		<cfset variables.instance = arguments.memento />
		<cfreturn this />
	</cffunction>
	<cffunction name="getMemento" access="public" returntype="struct" output="false" >
		<cfreturn variables.instance />
	</cffunction>

	<cffunction name="validate" access="public" returntype="array" output="false">
		<cfset var errors = arrayNew(1) />
		<cfset var thisError = structNew() />
		
		<!--- resource_type_id --->
		<cfif (len(trim(getresource_type_id())) AND NOT isNumeric(trim(getresource_type_id())))>
			<cfset thisError.field = "resource_type_id" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "resource_type_id is not numeric" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- resourceId --->
		<cfif (NOT len(trim(getresourceId())))>
			<cfset thisError.field = "resourceId" />
			<cfset thisError.type = "required" />
			<cfset thisError.message = "resourceId is required" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getresourceId())) AND NOT IsSimpleValue(trim(getresourceId())))>
			<cfset thisError.field = "resourceId" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "resourceId is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getresourceId())) GT 0)>
			<cfset thisError.field = "resourceId" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "resourceId is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- typeId --->
		<cfif (NOT len(trim(gettypeId())))>
			<cfset thisError.field = "typeId" />
			<cfset thisError.type = "required" />
			<cfset thisError.message = "typeId is required" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(gettypeId())) AND NOT IsSimpleValue(trim(gettypeId())))>
			<cfset thisError.field = "typeId" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "typeId is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(gettypeId())) GT 0)>
			<cfset thisError.field = "typeId" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "typeId is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<cfreturn errors />
	</cffunction>

	<!---
	ACCESSORS
	--->
	<cffunction name="setresource_type_id" access="public" returntype="void" output="false">
		<cfargument name="resource_type_id" type="string" required="true" />
		<cfset variables.instance.resource_type_id = arguments.resource_type_id />
	</cffunction>
	<cffunction name="getresource_type_id" access="public" returntype="string" output="false">
		<cfreturn variables.instance.resource_type_id />
	</cffunction>

	<cffunction name="setresourceId" access="public" returntype="void" output="false">
		<cfargument name="resourceId" type="string" required="true" />
		<cfset variables.instance.resourceId = arguments.resourceId />
	</cffunction>
	<cffunction name="getresourceId" access="public" returntype="string" output="false">
		<cfreturn variables.instance.resourceId />
	</cffunction>

	<cffunction name="settypeId" access="public" returntype="void" output="false">
		<cfargument name="typeId" type="string" required="true" />
		<cfset variables.instance.typeId = arguments.typeId />
	</cffunction>
	<cffunction name="gettypeId" access="public" returntype="string" output="false">
		<cfreturn variables.instance.typeId />
	</cffunction>


	<!---
	DUMP
	--->
	<cffunction name="dump" access="public" output="true" return="void">
		<cfargument name="abort" type="boolean" default="false" />
		<cfdump var="#variables.instance#" />
		<cfif arguments.abort>
			<cfabort />
		</cfif>
	</cffunction>

</cfcomponent>
