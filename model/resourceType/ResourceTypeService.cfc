<cfcomponent name="ResourceTypeService" output="false">

	<cffunction name="init" access="public" output="false" returntype="model.resourceType.ResourceTypeService">
		<cfargument name="ResourceTypeDAO" type="model.resourceType.ResourceTypeDAO" required="true" />
		<cfargument name="ResourceTypeGateway" type="model.resourceType.ResourceTypeGateway" required="true" />

		<cfset variables.ResourceTypeDAO = arguments.ResourceTypeDAO />
		<cfset variables.ResourceTypeGateway = arguments.ResourceTypeGateway />

		<cfreturn this/>
	</cffunction>

	<cffunction name="createResourceType" access="public" output="false" returntype="model.resourceType.ResourceType">
		<cfargument name="resource_type_id" type="numeric" required="false" />
		<cfargument name="resourceId" type="string" required="false" />
		<cfargument name="typeId" type="string" required="false" />
		
			
		<cfset var ResourceType = createObject("component","model.resourceType.ResourceType").init(argumentCollection=arguments) />
		<cfreturn ResourceType />
	</cffunction>

	<cffunction name="getResourceType" access="public" output="false" returntype="ResourceType">
		<cfargument name="resource_type_id" type="numeric" required="true" />
		
		<cfset var ResourceType = createResourceType(argumentCollection=arguments) />
		<cfset variables.ResourceTypeDAO.read(ResourceType) />
		<cfreturn ResourceType />
	</cffunction>

	<cffunction name="getResourceTypes" access="public" output="false" returntype="array">
		<cfargument name="resource_type_id" type="numeric" required="false" />
		<cfargument name="resourceId" type="string" required="false" />
		<cfargument name="typeId" type="string" required="false" />
		
		<cfreturn variables.ResourceTypeGateway.getByAttributes(argumentCollection=arguments) />
	</cffunction>

	<cffunction name="saveResourceType" access="public" output="false" returntype="boolean">
		<cfargument name="ResourceType" type="model.resourceType.ResourceType" required="true" />

		<cfreturn variables.ResourceTypeDAO.save(ResourceType) />
	</cffunction>

	<cffunction name="deleteResourceType" access="public" output="false" returntype="boolean">
		<cfargument name="resource_type_id" type="numeric" required="true" />
		
		<cfset var ResourceType = createResourceType(argumentCollection=arguments) />
		<cfreturn variables.ResourceTypeDAO.delete(ResourceType) />
	</cffunction>
	
	<cffunction name="deleteResourceTypes" access="public" output="false" returntype="boolean">
		<cfargument name="resourceId" type="string" required="true" />
		
		<cfreturn variables.ResourceTypeGateway.delete(arguments.resourceId) />
	</cffunction>
</cfcomponent>
