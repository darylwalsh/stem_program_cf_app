<cfcomponent name="ResourceService" output="false">

	<cffunction name="init" access="public" output="false" returntype="model.resource.ResourceService">
		<cfargument name="ResourceDAO" type="model.resource.ResourceDAO" required="true" />
		<cfargument name="ResourceGateway" type="model.resource.ResourceGateway" required="true" />

		<cfset variables.ResourceDAO = arguments.ResourceDAO />
		<cfset variables.ResourceGateway = arguments.ResourceGateway />

		<cfreturn this/>
	</cffunction>

	<cffunction name="createResource" access="public" output="false" returntype="model.resource.Resource">
		<cfargument name="resource_id" type="numeric" required="false" />
		<cfargument name="title" type="string" required="false" />
		<cfargument name="body" type="string" required="false" />
		<cfargument name="keywords" type="string" required="false" />
		<cfargument name="views" type="numeric" required="false" />
		<cfargument name="approved" type="numeric" required="false" />
		<cfargument name="dtCreated" type="date" required="false" />
		<cfargument name="dtUpdated" type="date" required="false" />
		<cfargument name="createdBy" type="string" required="false" />
		<cfargument name="updatedBy" type="string" required="false" />
		<cfargument name="ipCreated" type="string" required="false" />
		<cfargument name="ipUpdated" type="string" required="false" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="type" type="string" required="false" />
		<cfargument name="resourceId" type="string" required="false" />
		<cfargument name="summary" type="string" required="false" />
		<cfargument name="createdByName" type="string" required="false" />
		<cfargument name="categories" type="array" required="false" />
		
			
		<cfset var Resource = createObject("component","model.resource.Resource").init(argumentCollection=arguments) />
		<cfreturn Resource />
	</cffunction>

	<cffunction name="getResource" access="public" output="false" returntype="Resource">
		<cfargument name="resourceId" type="string" required="true" />
		
		<cfset var Resource = createResource(argumentCollection=arguments) />
		<cfset variables.ResourceDAO.read(Resource) />
		<cfreturn Resource />
	</cffunction>

	<cffunction name="getResources" access="public" output="false" returntype="array">
		<cfargument name="resource_id" type="numeric" required="false" />
		<cfargument name="title" type="string" required="false" />
		<cfargument name="body" type="string" required="false" />
		<cfargument name="keywords" type="string" required="false" />
		<cfargument name="views" type="numeric" required="false" />
		<cfargument name="approved" type="numeric" required="false" />
		<cfargument name="dtCreated" type="date" required="false" />
		<cfargument name="dtUpdated" type="date" required="false" />
		<cfargument name="createdBy" type="string" required="false" />
		<cfargument name="updatedBy" type="string" required="false" />
		<cfargument name="ipCreated" type="string" required="false" />
		<cfargument name="ipUpdated" type="string" required="false" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="type" type="string" required="false" />
		<cfargument name="resourceId" type="string" required="false" />
		<cfargument name="summary" type="string" required="false" />
		<cfargument name="createdByName" type="string" required="false" />
		
		<cfreturn variables.ResourceGateway.getByAttributes(argumentCollection=arguments) />
	</cffunction>
	
	<cffunction name="getResourcesQuery" access="public" output="false" returntype="query">
		<cfargument name="resource_id" type="numeric" required="false" />
		<cfargument name="title" type="string" required="false" />
		<cfargument name="body" type="string" required="false" />
		<cfargument name="keywords" type="string" required="false" />
		<cfargument name="views" type="numeric" required="false" />
		<cfargument name="approved" type="numeric" required="false" />
		<cfargument name="dtCreated" type="date" required="false" />
		<cfargument name="dtUpdated" type="date" required="false" />
		<cfargument name="createdBy" type="string" required="false" />
		<cfargument name="updatedBy" type="string" required="false" />
		<cfargument name="ipCreated" type="string" required="false" />
		<cfargument name="ipUpdated" type="string" required="false" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="type" type="string" required="false" />
		<cfargument name="resourceId" type="string" required="false" />
		<cfargument name="summary" type="string" required="false" />
		<cfargument name="createdByName" type="string" required="false" />
		
		<cfreturn variables.ResourceGateway.getByAttributesQuery(argumentCollection=arguments) />
	</cffunction>
	
	<cffunction name="getResourcesByCategoryIdQuery" access="public" output="false" returntype="any">
		<cfargument name="categoryId" type="string" required="true" />
		<cfargument name="siteId" type="string" required="true" />
		<cfargument name="type" type="string" required="true" />
		<cfargument name="orderBy" type="string" required="false" default="views DESC" />
		
		<cfreturn variables.ResourceGateway.getByCategoryIdQuery(arguments.categoryId,arguments.siteId,arguments.type,arguments.orderBy) />
	</cffunction>
	
	<cffunction name="getResourcesByTypeIdQuery" access="public" output="false" returntype="any">
		<cfargument name="typeId" type="string" required="true" />
		<cfargument name="siteId" type="string" required="true" />
		<cfargument name="type" type="string" required="true" />
		<cfargument name="orderBy" type="string" required="false" default="views DESC" />
		
		<cfreturn variables.ResourceGateway.getByTypeIdQuery(arguments.typeId,arguments.siteId,arguments.type,arguments.orderBy) />
	</cffunction>
    
	<cffunction name="getResourcesByGradeIdQuery" access="public" output="false" returntype="any">
		<cfargument name="grade_id" type="string" required="true" />
		<cfargument name="siteId" type="string" required="true" />
		<cfargument name="type" type="string" required="true" />
		<cfargument name="orderBy" type="string" required="false" default="views DESC" />
		
		<cfreturn variables.ResourceGateway.getByGradeIdQuery(arguments.grade_id,arguments.siteId,arguments.type,arguments.orderBy) />
	</cffunction>

	<cffunction name="saveResource" access="public" output="false" returntype="boolean">
		<cfargument name="Resource" type="model.resource.Resource" required="true" />

		<cfreturn variables.ResourceDAO.save(Resource) />
	</cffunction>

	<cffunction name="deleteResource" access="public" output="false" returntype="boolean">
		<cfargument name="resourceId" type="string" required="true" />
		
		<cfset var Resource = createResource(argumentCollection=arguments) />
		<cfreturn variables.ResourceDAO.delete(Resource) />
	</cffunction>
</cfcomponent>
