<cfcomponent displayname="ResourceGateway" output="false">
	<cffunction name="init" access="public" output="false" returntype="model.resource.ResourceGateway">
		<cfargument name="dsn" type="string" required="true" />
		<cfset variables.dsn = arguments.dsn />
		<cfreturn this />
	</cffunction>
	
	<cffunction name="setFileService" access="public" output="false" returntype="void">
		<cfargument name="fileService" type="model.file.fileService" required="true" />
		<cfset variables.fileService = arguments.fileService />
	</cffunction>
	<cffunction name="getFileService" access="public" output="false" returntype="model.file.fileService">
		<cfreturn variables.fileService />
	</cffunction>
	
	<cffunction name="getByCategoryIdQuery" access="public" output="false" returntype="query">
		<cfargument name="categoryId" type="string" required="true" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="type" type="string" required="false" default="resource" />
		<cfargument name="orderBy" type="string" required="false" default="r.views DESC" />
		
		<cfset var qList = "" />
		<cfquery name="qList" datasource="#variables.dsn#">
			SELECT
				resource_id,
				title,
				body,
				keywords,
				views,
				approved,
				dtCreated,
				dtUpdated,
				createdBy,
				updatedBy,
				ipCreated,
				ipUpdated,
				siteId,
				type,
				r.resourceId,
				summary,
				createdByName,
				(SELECT TOP 1 thumbnail FROM cms_files f WHERE f.contentId = r.resourceId) AS thumbnail
			FROM	cms_resources r
			JOIN	cms_resource_categories rc ON rc.resourceId = r.resourceId
			WHERE	0=0		
		<cfif structKeyExists(arguments,"categoryId") and len(arguments.categoryId)>
			AND	rc.categoryId = <cfqueryparam value="#arguments.categoryId#" CFSQLType="cf_sql_idstamp" />
		</cfif>
		<cfif structKeyExists(arguments,"siteId") and len(arguments.siteId)>
			AND	r.siteId = <cfqueryparam value="#arguments.siteId#" CFSQLType="cf_sql_idstamp" />
		</cfif>
		<cfif structKeyExists(arguments,"type") and len(arguments.type)>
			AND	r.type = <cfqueryparam value="#arguments.type#" />
		</cfif>
		<cfif structKeyExists(arguments, "orderby") and len(arguments.orderBy)>
			ORDER BY #arguments.orderby#
		</cfif>
		</cfquery>
		
		<cfreturn qList />
	</cffunction>
	
	<cffunction name="getByTypeIdQuery" access="public" output="false" returntype="query">
		<cfargument name="typeId" type="string" required="true" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="type" type="string" required="false" default="resource" />
		<cfargument name="orderBy" type="string" required="false" default="r.views DESC" />
		
		<cfset var qList = "" />
		<cfquery name="qList" datasource="#variables.dsn#">
			SELECT
				resource_id,
				title,
				body,
				keywords,
				views,
				approved,
				dtCreated,
				dtUpdated,
				createdBy,
				updatedBy,
				ipCreated,
				ipUpdated,
				siteId,
				type,
				r.resourceId,
				summary,
				createdByName,
				(SELECT TOP 1 thumbnail FROM cms_files f WHERE f.contentId = r.resourceId) AS thumbnail
			FROM	cms_resources r
			JOIN	cms_resource_types rc ON rc.resourceId = r.resourceId
			WHERE	0=0		
		<cfif structKeyExists(arguments,"typeId") and len(arguments.typeId)>
			AND	rc.typeId = <cfqueryparam value="#arguments.typeId#" CFSQLType="cf_sql_idstamp" />
		</cfif>
		<cfif structKeyExists(arguments,"siteId") and len(arguments.siteId)>
			AND	r.siteId = <cfqueryparam value="#arguments.siteId#" CFSQLType="cf_sql_idstamp" />
		</cfif>
		<cfif structKeyExists(arguments,"type") and len(arguments.type)>
			AND	r.type = <cfqueryparam value="#arguments.type#" />
		</cfif>
		<cfif structKeyExists(arguments, "orderby") and len(arguments.orderBy)>
			ORDER BY #arguments.orderby#
		</cfif>
		</cfquery>
		
		<cfreturn qList />
	</cffunction>
    
	<cffunction name="getByGradeIdQuery" access="public" output="false" returntype="query">
		<cfargument name="grade_id" type="string" required="true" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="type" type="string" required="false" default="resource" />
		<cfargument name="orderBy" type="string" required="false" default="r.views DESC" />
		
		<cfset var qList = "" />
		<cfquery name="qList" datasource="#variables.dsn#">
			SELECT
				resource_id,
				title,
				body,
				keywords,
				views,
				dtCreated,
				r.resourceId,
				summary,
				createdByName,
				(SELECT TOP 1 thumbnail FROM cms_files f WHERE f.contentId = resourceId) AS thumbnail
			FROM	cms_resources r
			WHERE	0=0
		<cfif structKeyExists(arguments,"siteId") and len(arguments.siteId)>
			AND	r.siteId = <cfqueryparam value="#arguments.siteId#" CFSQLType="cf_sql_idstamp" />
		</cfif>
		<cfif structKeyExists(arguments,"type") and len(arguments.type)>
			AND	r.type = <cfqueryparam value="#arguments.type#" />
		</cfif>
			AND r.resourceId IN (SELECT DISTINCT
									resourceId
								FROM cms_resource_grades rg
								WHERE 0=0
								AND rg.grade_id IN (<cfqueryparam value="#arguments.grade_id#" list="true" />))
		<cfif structKeyExists(arguments, "orderby") and len(arguments.orderBy)>
			ORDER BY #arguments.orderby#
		</cfif>
		</cfquery>
		
		<cfreturn qList />
	</cffunction>
	
	<cffunction name="getByAttributesQuery" access="public" output="false" returntype="query">
		<cfargument name="resource_id" type="numeric" required="false" />
		<cfargument name="title" type="string" required="false" />
		<cfargument name="body" type="string" required="false" />
		<cfargument name="keywords" type="string" required="false" />
		<cfargument name="views" type="numeric" required="false" />
		<cfargument name="approved" type="numeric" required="false" />
		<cfargument name="dtCreated" type="date" required="false" />
		<cfargument name="dtUpdated" type="date" required="false" />
		<cfargument name="createdBy" type="string" required="false" />
		<cfargument name="updatedBy" type="string" required="false" />
		<cfargument name="ipCreated" type="string" required="false" />
		<cfargument name="ipUpdated" type="string" required="false" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="type" type="string" required="false" />
		<cfargument name="resourceId" type="string" required="false" />
		<cfargument name="summary" type="string" required="false" />
		<cfargument name="createdByName" type="string" required="false" />
		<cfargument name="orderby" type="string" required="false" />
		<cfargument name="numberOfResults" type="string" required="false" />
		<cfargument name="numberOfFiles" type="string" required="false" />
		
		<cfset var qList = "" />		
		<cfquery name="qList" datasource="#variables.dsn#">
			SELECT
			<cfif StructKeyExists(arguments,"numberOfResults") AND Len(arguments.numberOfResults)>
			TOP	#arguments.numberOfResults#
			</cfif>
				resource_id,
				title,
				body,
				keywords,
				views,
				approved,
				dtCreated,
				dtUpdated,
				createdBy,
				updatedBy,
				ipCreated,
				ipUpdated,
				siteId,
				type,
				resourceId,
				summary,
				createdByName,
				(SELECT TOP 1 thumbnail FROM cms_files f WHERE f.contentId = resourceId) AS thumbnail
			FROM	cms_resources
			WHERE	0=0
		
		<cfif structKeyExists(arguments,"resource_id") and len(arguments.resource_id)>
			AND	resource_id = <cfqueryparam value="#arguments.resource_id#" CFSQLType="cf_sql_integer" />
		</cfif>
		<cfif structKeyExists(arguments,"title") and len(arguments.title)>
			AND	title = <cfqueryparam value="#arguments.title#" CFSQLType="cf_sql_varchar" />
		</cfif>
		<cfif structKeyExists(arguments,"body") and len(arguments.body)>
			AND	body = <cfqueryparam value="#arguments.body#" CFSQLType="cf_sql_longvarchar" />
		</cfif>
		<cfif structKeyExists(arguments,"keywords") and len(arguments.keywords)>
			AND	keywords = <cfqueryparam value="#arguments.keywords#" CFSQLType="cf_sql_varchar" />
		</cfif>
		<cfif structKeyExists(arguments,"views") and len(arguments.views)>
			AND	views = <cfqueryparam value="#arguments.views#" CFSQLType="cf_sql_integer" />
		</cfif>
		<cfif structKeyExists(arguments,"approved") and len(arguments.approved)>
			AND	approved = <cfqueryparam value="#arguments.approved#" CFSQLType="cf_sql_integer" />
		</cfif>
		<cfif structKeyExists(arguments,"dtCreated") and len(arguments.dtCreated)>
			AND	dtCreated = <cfqueryparam value="#arguments.dtCreated#" CFSQLType="cf_sql_timestamp" />
		</cfif>
		<cfif structKeyExists(arguments,"dtUpdated") and len(arguments.dtUpdated)>
			AND	dtUpdated = <cfqueryparam value="#arguments.dtUpdated#" CFSQLType="cf_sql_timestamp" />
		</cfif>
		<cfif structKeyExists(arguments,"createdBy") and len(arguments.createdBy)>
			AND	createdBy = <cfqueryparam value="#arguments.createdBy#" CFSQLType="cf_sql_idstamp" />
		</cfif>
		<cfif structKeyExists(arguments,"updatedBy") and len(arguments.updatedBy)>
			AND	updatedBy = <cfqueryparam value="#arguments.updatedBy#" CFSQLType="cf_sql_idstamp" />
		</cfif>
		<cfif structKeyExists(arguments,"ipCreated") and len(arguments.ipCreated)>
			AND	ipCreated = <cfqueryparam value="#arguments.ipCreated#" CFSQLType="cf_sql_varchar" />
		</cfif>
		<cfif structKeyExists(arguments,"ipUpdated") and len(arguments.ipUpdated)>
			AND	ipUpdated = <cfqueryparam value="#arguments.ipUpdated#" CFSQLType="cf_sql_varchar" />
		</cfif>
		<cfif structKeyExists(arguments,"siteId") and len(arguments.siteId)>
			AND	siteId = <cfqueryparam value="#arguments.siteId#" CFSQLType="cf_sql_idstamp" />
		</cfif>
		<cfif structKeyExists(arguments,"type") and len(arguments.type)>
			AND	type = <cfqueryparam value="#arguments.type#" CFSQLType="cf_sql_varchar" />
		</cfif>
		<cfif structKeyExists(arguments,"resourceId") and len(arguments.resourceId)>
			AND	resourceId = <cfqueryparam value="#arguments.resourceId#" CFSQLType="cf_sql_idstamp" />
		</cfif>
		<cfif structKeyExists(arguments,"numberOfFiles") and len(arguments.numberOfFiles)>
			AND (SELECT COUNT(1) FROM cms_files f WHERE f.contentId = resourceId GROUP by f.contentId) >= <cfqueryparam value="#arguments.numberOfFiles#" />
		</cfif>
		<cfif structKeyExists(arguments, "orderby") and len(arguments.orderBy)>
			ORDER BY #arguments.orderby#
		</cfif>
		</cfquery>
		
		<cfreturn qList />
	</cffunction>

	<cffunction name="getByAttributes" access="public" output="false" returntype="array">
		<cfargument name="resource_id" type="numeric" required="false" />
		<cfargument name="title" type="string" required="false" />
		<cfargument name="body" type="string" required="false" />
		<cfargument name="keywords" type="string" required="false" />
		<cfargument name="views" type="numeric" required="false" />
		<cfargument name="approved" type="numeric" required="false" />
		<cfargument name="dtCreated" type="date" required="false" />
		<cfargument name="dtUpdated" type="date" required="false" />
		<cfargument name="createdBy" type="string" required="false" />
		<cfargument name="updatedBy" type="string" required="false" />
		<cfargument name="ipCreated" type="string" required="false" />
		<cfargument name="ipUpdated" type="string" required="false" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="type" type="string" required="false" />
		<cfargument name="resourceId" type="string" required="false" />
		<cfargument name="orderby" type="string" required="false" />
		
		<cfset var qList = getByAttributesQuery(argumentCollection=arguments) />		
		<cfset var arrObjects = arrayNew(1) />
		<cfset var tmpObj = "" />
		<cfset var i = 0 />
		<cfset var files = ArrayNew(1) />
		<cfloop from="1" to="#qList.recordCount#" index="i">
			<cfset tmpObj = createObject("component","model.resource.Resource").init(argumentCollection=queryRowToStruct(qList,i)) />
			<cfset files = getFileService().getFiles(contentId=tmpObj.getResourceId()) />
			<cfset tmpObj.setFiles(files) />
			<cfset arrayAppend(arrObjects,tmpObj) />
		</cfloop>
				
		<cfreturn arrObjects />
	</cffunction>

	<cffunction name="queryRowToStruct" access="private" output="false" returntype="struct">
		<cfargument name="qry" type="query" required="true">
		
		<cfscript>
			/**
			 * Makes a row of a query into a structure.
			 * 
			 * @param query 	 The query to work with. 
			 * @param row 	 Row number to check. Defaults to row 1. 
			 * @return Returns a structure. 
			 * @author Nathan Dintenfass (nathan@changemedia.com) 
			 * @version 1, December 11, 2001 
			 */
			//by default, do this to the first row of the query
			var row = 1;
			//a var for looping
			var ii = 1;
			//the cols to loop over
			var cols = listToArray(qry.columnList);
			//the struct to return
			var stReturn = structnew();
			//if there is a second argument, use that for the row number
			if(arrayLen(arguments) GT 1)
				row = arguments[2];
			//loop over the cols and build the struct from the query row
			for(ii = 1; ii lte arraylen(cols); ii = ii + 1){
				stReturn[cols[ii]] = qry[cols[ii]][row];
			}		
			//return the struct
			return stReturn;
		</cfscript>
	</cffunction>

</cfcomponent>
