<cfcomponent displayname="ResourceDAO" hint="table ID column = resource_id">

	<cffunction name="init" access="public" output="false" returntype="model.resource.ResourceDAO">
		<cfargument name="dsn" type="string" required="true">
		<cfset variables.dsn = arguments.dsn>
		<cfreturn this>
	</cffunction>
	
	<cffunction name="setCategoryService" access="public" output="false" returntype="void">
		<cfargument name="categoryService" type="com.redactededucation.category.CategoryService" required="true" />
		<cfset variables.categoryService = arguments.categoryService />
	</cffunction>
	<cffunction name="getCategoryService" access="public" output="false" returntype="com.redactededucation.category.CategoryService">
		<cfreturn variables.categoryService />
	</cffunction>
	
	<cffunction name="setTypeService" access="public" output="false" returntype="void">
		<cfargument name="typeService" type="com.redactededucation.type.TypeService" required="true" />
		<cfset variables.typeService = arguments.typeService />
	</cffunction>
	<cffunction name="getTypeService" access="public" output="false" returntype="com.redactededucation.type.TypeService">
		<cfreturn variables.typeService />
	</cffunction>
    
	<cffunction name="setGradeService" access="public" output="false" returntype="void">
		<cfargument name="gradeService" type="model.grade.gradeService" required="true" />
		<cfset variables.gradeService = arguments.gradeService />
	</cffunction>
	<cffunction name="getGradeService" access="public" output="false" returntype="model.grade.gradeService">
		<cfreturn variables.gradeService />
	</cffunction>
	
	<cffunction name="setFileService" access="public" output="false" returntype="void">
		<cfargument name="fileService" type="model.file.fileService" required="true" />
		<cfset variables.fileService = arguments.fileService />
	</cffunction>
	<cffunction name="getFileService" access="public" output="false" returntype="model.file.fileService">
		<cfreturn variables.fileService />
	</cffunction>
	
	<cffunction name="setCommentService" access="public" output="false" returntype="void">
		<cfargument name="commentService" type="model.comment.commentService" required="true" />
		<cfset variables.commentService = arguments.commentService />
	</cffunction>
	<cffunction name="getCommentService" access="public" output="false" returntype="model.comment.commentService">
		<cfreturn variables.commentService />
	</cffunction>
	
	<cffunction name="setBlockService" access="public" output="false" returntype="void">
		<cfargument name="blockService" type="model.block.blockService" required="true" />
		<cfset variables.blockService = arguments.blockService />
	</cffunction>
	<cffunction name="getBlockService" access="public" output="false" returntype="model.block.blockService">
		<cfreturn variables.blockService />
	</cffunction>
	
	<cffunction name="create" access="public" output="false" returntype="boolean">
		<cfargument name="Resource" type="model.resource.Resource" required="true" />

		<cfset var qCreate = "" />
		<cftry>
			<cfquery name="qCreate" datasource="#variables.dsn#">
				INSERT INTO cms_resources
					(
					title,
					body,
					keywords,
					views,
					approved,
					dtCreated,
					dtUpdated,
					createdBy,
					updatedBy,
					ipCreated,
					ipUpdated,
					siteId,
					type,
					resourceId,
					summary,
					createdByName
					)
				VALUES
					(
					<cfqueryparam value="#arguments.Resource.gettitle()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.gettitle())#" />,
					<cfqueryparam value="#arguments.Resource.getbody()#" CFSQLType="cf_sql_longvarchar" null="#not len(arguments.Resource.getbody())#" />,
					<cfqueryparam value="#arguments.Resource.getkeywords()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.getkeywords())#" />,
					<cfqueryparam value="#arguments.Resource.getviews()#" CFSQLType="cf_sql_integer" null="#not len(arguments.Resource.getviews())#" />,
					<cfqueryparam value="#arguments.Resource.getapproved()#" CFSQLType="cf_sql_integer" null="#not len(arguments.Resource.getapproved())#" />,
					<cfqueryparam value="#arguments.Resource.getdtCreated()#" CFSQLType="cf_sql_timestamp" null="#not len(arguments.Resource.getdtCreated())#" />,
					<cfqueryparam value="#arguments.Resource.getdtUpdated()#" CFSQLType="cf_sql_timestamp" null="#not len(arguments.Resource.getdtUpdated())#" />,
					<cfqueryparam value="#arguments.Resource.getcreatedBy()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Resource.getcreatedBy())#" />,
					<cfqueryparam value="#arguments.Resource.getupdatedBy()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Resource.getupdatedBy())#" />,
					<cfqueryparam value="#arguments.Resource.getipCreated()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.getipCreated())#" />,
					<cfqueryparam value="#arguments.Resource.getipUpdated()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.getipUpdated())#" />,
					<cfqueryparam value="#arguments.Resource.getsiteId()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Resource.getsiteId())#" />,
					<cfqueryparam value="#arguments.Resource.gettype()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.gettype())#" />,
					<cfqueryparam value="#arguments.Resource.getresourceId()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Resource.getresourceId())#" />,
					<cfqueryparam value="#arguments.Resource.getSummary()#" CFSQLType="cf_sql_longvarchar" null="#not len(arguments.Resource.getSummary())#" />,
					<cfqueryparam value="#arguments.Resource.getCreatedByName()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.getCreatedByName())#" />
					)
			</cfquery>
			<cfcatch type="database">
				<cfreturn false />
			</cfcatch>
		</cftry>
		<cfreturn true />
	</cffunction>

	<cffunction name="read" access="public" output="false" returntype="void">
		<cfargument name="Resource" type="model.resource.Resource" required="true" />

		<cfset var qRead = "" />
		<cfset var strReturn = structNew() />
		<cfset var categories = ArrayNew(1) />
		<cfset var types = ArrayNew(1) />
		<cfset var grades = ArrayNew(1) />
		<cfset var files = ArrayNew(1) />
		<cfset var comments = ArrayNew(1) />
		<cfset var blocks = ArrayNew(1) />
		<cftry>
			<cfquery name="qRead" datasource="#variables.dsn#">
				SELECT
					resource_id,
					title,
					body,
					keywords,
					views,
					approved,
					dtCreated,
					dtUpdated,
					createdBy,
					updatedBy,
					ipCreated,
					ipUpdated,
					siteId,
					type,
					resourceId,
					summary,
					createdByName
				FROM	cms_resources
				WHERE	resourceId = <cfqueryparam value="#arguments.Resource.getresourceId()#" CFSQLType="cf_sql_idstamp" />
			</cfquery>
			<cfcatch type="database">
				<!--- leave the bean as is and set an empty query for the conditional logic below --->
				<cfset qRead = queryNew("id") />
			</cfcatch>
		</cftry>
		<cfif qRead.recordCount>
			<cfset strReturn = queryRowToStruct(qRead)>
			<cfset arguments.Resource.init(argumentCollection=strReturn)>
			
			<cfset categories = getCategoryService().getCategoriesByResourceId(arguments.Resource.getResourceId()) />
			<cfset types = getTypeService().getTypesByResourceId(arguments.Resource.getResourceId()) />
			<cfset grades = getGradeService().getGradesByResourceId(arguments.Resource.getResourceId()) />
			<cfset files = getFileService().getFiles(contentId=arguments.Resource.getResourceId()) />
			<cfset comments = getCommentService().getComments(contentGuid=arguments.Resource.getResourceId(),orderBy="dtCreated DESC") />
			<cfset blocks = getBlockService().getBlocksByResourceId(arguments.Resource.getResourceId()) />
			
			<cfset arguments.Resource.setCategories(categories) />
			<cfset arguments.Resource.setTypes(types) />
			<cfset arguments.Resource.setGrades(grades) />
			<cfset arguments.Resource.setFiles(files) />
			<cfset arguments.Resource.setComments(comments) />
			<cfset arguments.Resource.setBlocks(blocks) />
		</cfif>
	</cffunction>

	<cffunction name="update" access="public" output="false" returntype="boolean">
		<cfargument name="Resource" type="model.resource.Resource" required="true" />

		<cfset var qUpdate = "" />
		<cftry>
			<cfquery name="qUpdate" datasource="#variables.dsn#">
				UPDATE	cms_resources
				SET
					title = <cfqueryparam value="#arguments.Resource.gettitle()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.gettitle())#" />,
					body = <cfqueryparam value="#arguments.Resource.getbody()#" CFSQLType="cf_sql_longvarchar" null="#not len(arguments.Resource.getbody())#" />,
					keywords = <cfqueryparam value="#arguments.Resource.getkeywords()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.getkeywords())#" />,
					views = <cfqueryparam value="#arguments.Resource.getviews()#" CFSQLType="cf_sql_integer" null="#not len(arguments.Resource.getviews())#" />,
					approved = <cfqueryparam value="#arguments.Resource.getapproved()#" CFSQLType="cf_sql_integer" null="#not len(arguments.Resource.getapproved())#" />,
					dtCreated = <cfqueryparam value="#arguments.Resource.getdtCreated()#" CFSQLType="cf_sql_timestamp" null="#not len(arguments.Resource.getdtCreated())#" />,
					dtUpdated = <cfqueryparam value="#arguments.Resource.getdtUpdated()#" CFSQLType="cf_sql_timestamp" null="#not len(arguments.Resource.getdtUpdated())#" />,
					createdBy = <cfqueryparam value="#arguments.Resource.getcreatedBy()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Resource.getcreatedBy())#" />,
					updatedBy = <cfqueryparam value="#arguments.Resource.getupdatedBy()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Resource.getupdatedBy())#" />,
					ipCreated = <cfqueryparam value="#arguments.Resource.getipCreated()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.getipCreated())#" />,
					ipUpdated = <cfqueryparam value="#arguments.Resource.getipUpdated()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.getipUpdated())#" />,
					siteId = <cfqueryparam value="#arguments.Resource.getsiteId()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Resource.getsiteId())#" />,
					type = <cfqueryparam value="#arguments.Resource.gettype()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.gettype())#" />,
					resourceId = <cfqueryparam value="#arguments.Resource.getresourceId()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Resource.getresourceId())#" />,
					summary = <cfqueryparam value="#arguments.Resource.getSummary()#" CFSQLType="cf_sql_longvarchar" null="#not len(arguments.Resource.getSummary())#" />,
					createdByName = <cfqueryparam value="#arguments.Resource.getCreatedByName()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Resource.getCreatedByName())#" />
				WHERE	resourceId = <cfqueryparam value="#arguments.Resource.getResourceId()#" CFSQLType="cf_sql_idstamp" />
			</cfquery>
			<cfcatch type="database">
				<cfreturn false />
			</cfcatch>
		</cftry>
		<cfreturn true />
	</cffunction>

	<cffunction name="delete" access="public" output="false" returntype="boolean">
		<cfargument name="Resource" type="model.resource.Resource" required="true" />

		<cfset var qDelete = "">
		<cftry>
			<cfquery name="qDelete" datasource="#variables.dsn#">
				DELETE FROM	cms_resources 
				WHERE	resourceId = <cfqueryparam value="#arguments.Resource.getResourceId()#" CFSQLType="cf_sql_idstamp" />
			</cfquery>
			<cfcatch type="database">
				<cfreturn false />
			</cfcatch>
		</cftry>
		<cfreturn true />
	</cffunction>

	<cffunction name="exists" access="public" output="false" returntype="boolean">
		<cfargument name="Resource" type="model.resource.Resource" required="true" />

		<cfset var qExists = "">
		<cfquery name="qExists" datasource="#variables.dsn#" maxrows="1">
			SELECT count(1) as idexists
			FROM	cms_resources
			WHERE	resourceId = <cfqueryparam value="#arguments.Resource.getresourceId()#" CFSQLType="cf_sql_idstamp" />
		</cfquery>

		<cfif qExists.idexists>
			<cfreturn true />
		<cfelse>
			<cfreturn false />
		</cfif>
	</cffunction>

	<cffunction name="save" access="public" output="false" returntype="boolean">
		<cfargument name="Resource" type="model.resource.Resource" required="true" />
		
		<cfset var success = false />
		<cfif exists(arguments.Resource)>
			<cfset success = update(arguments.Resource) />
		<cfelse>
			<cfset success = create(arguments.Resource) />
		</cfif>
		
		<cfreturn success />
	</cffunction>

	<cffunction name="queryRowToStruct" access="private" output="false" returntype="struct">
		<cfargument name="qry" type="query" required="true">
		
		<cfscript>
			/**
			 * Makes a row of a query into a structure.
			 * 
			 * @param query 	 The query to work with. 
			 * @param row 	 Row number to check. Defaults to row 1. 
			 * @return Returns a structure. 
			 * @author Nathan Dintenfass (nathan@changemedia.com) 
			 * @version 1, December 11, 2001 
			 */
			//by default, do this to the first row of the query
			var row = 1;
			//a var for looping
			var ii = 1;
			//the cols to loop over
			var cols = listToArray(qry.columnList);
			//the struct to return
			var stReturn = structnew();
			//if there is a second argument, use that for the row number
			if(arrayLen(arguments) GT 1)
				row = arguments[2];
			//loop over the cols and build the struct from the query row
			for(ii = 1; ii lte arraylen(cols); ii = ii + 1){
				stReturn[cols[ii]] = qry[cols[ii]][row];
			}		
			//return the struct
			return stReturn;
		</cfscript>
	</cffunction>

</cfcomponent>
