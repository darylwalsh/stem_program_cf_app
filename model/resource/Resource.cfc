<cfcomponent displayname="Resource" output="false">
		<cfproperty name="resource_id" type="numeric" default="" />
		<cfproperty name="title" type="string" default="" />
		<cfproperty name="body" type="string" default="" />
		<cfproperty name="keywords" type="string" default="" />
		<cfproperty name="views" type="numeric" default="" />
		<cfproperty name="approved" type="numeric" default="" />
		<cfproperty name="dtCreated" type="date" default="" />
		<cfproperty name="dtUpdated" type="date" default="" />
		<cfproperty name="createdBy" type="string" default="" />
		<cfproperty name="updatedBy" type="string" default="" />
		<cfproperty name="ipCreated" type="string" default="" />
		<cfproperty name="ipUpdated" type="string" default="" />
		<cfproperty name="siteId" type="string" default="" />
		<cfproperty name="type" type="string" default="" />
		<cfproperty name="resourceId" type="string" default="" />
		
	<!---
	PROPERTIES
	--->
	<cfset variables.instance = StructNew() />

	<!---
	INITIALIZATION / CONFIGURATION
	--->
	<cffunction name="init" access="public" returntype="model.resource.Resource" output="false">
		<cfargument name="resource_id" type="string" required="false" default="" />
		<cfargument name="title" type="string" required="false" default="" />
		<cfargument name="body" type="string" required="false" default="" />
		<cfargument name="keywords" type="string" required="false" default="" />
		<cfargument name="views" type="string" required="false" default="" />
		<cfargument name="approved" type="string" required="false" default="" />
		<cfargument name="dtCreated" type="string" required="false" default="" />
		<cfargument name="dtUpdated" type="string" required="false" default="" />
		<cfargument name="createdBy" type="string" required="false" default="" />
		<cfargument name="updatedBy" type="string" required="false" default="" />
		<cfargument name="ipCreated" type="string" required="false" default="" />
		<cfargument name="ipUpdated" type="string" required="false" default="" />
		<cfargument name="siteId" type="string" required="false" default="" />
		<cfargument name="type" type="string" required="false" default="" />
		<cfargument name="resourceId" type="string" required="false" default="" />
		<cfargument name="summary" type="string" required="false" default="" />
		<cfargument name="createdByName" type="string" required="false" default="" />
		<cfargument name="categories" type="array" required="false" default="#arrayNew(1)#" />
		<cfargument name="types" type="array" required="false" default="#arrayNew(1)#" />
		<cfargument name="grades" type="array" required="false" default="#arrayNew(1)#" />
		<cfargument name="files" type="array" required="false" default="#arrayNew(1)#" />
		<cfargument name="comments" type="array" required="false" default="#arrayNew(1)#" />
		<cfargument name="blocks" type="array" required="false" default="#ArrayNew(1)#" />
		
		<!--- run setters --->
		<cfset setresource_id(arguments.resource_id) />
		<cfset settitle(arguments.title) />
		<cfset setbody(arguments.body) />
		<cfset setkeywords(arguments.keywords) />
		<cfset setviews(arguments.views) />
		<cfset setapproved(arguments.approved) />
		<cfset setdtCreated(arguments.dtCreated) />
		<cfset setdtUpdated(arguments.dtUpdated) />
		<cfset setcreatedBy(arguments.createdBy) />
		<cfset setupdatedBy(arguments.updatedBy) />
		<cfset setipCreated(arguments.ipCreated) />
		<cfset setipUpdated(arguments.ipUpdated) />
		<cfset setsiteId(arguments.siteId) />
		<cfset settype(arguments.type) />
		<cfset setresourceId(arguments.resourceId) />
		<cfset setSummary(arguments.summary) />
		<cfset setCreatedByName(arguments.createdByName) />
		<cfset setCategories(arguments.categories) />
		<cfset setTypes(arguments.types) />
		<cfset setGrades(arguments.grades) />
		<cfset setFiles(arguments.files) />
		<cfset setComments(arguments.comments) />
		<cfset setBlocks(arguments.blocks) />
		
		<cfreturn this />
 	</cffunction>

	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="setMemento" access="public" returntype="model.resource.Resource" output="false">
		<cfargument name="memento" type="struct" required="yes"/>
		<cfset variables.instance = arguments.memento />
		<cfreturn this />
	</cffunction>
	<cffunction name="getMemento" access="public" returntype="struct" output="false" >
		<cfreturn variables.instance />
	</cffunction>

	<cffunction name="validate" access="public" returntype="array" output="false">
		<cfset var errors = arrayNew(1) />
		<cfset var thisError = structNew() />
		
		<!--- resource_id --->
		<cfif (len(trim(getresource_id())) AND NOT isNumeric(trim(getresource_id())))>
			<cfset thisError.field = "resource_id" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "resource_id is not numeric" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- title --->
		<cfif (len(trim(gettitle())) AND NOT IsSimpleValue(trim(gettitle())))>
			<cfset thisError.field = "title" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "title is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(gettitle())) GT 255)>
			<cfset thisError.field = "title" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "title is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- body --->
		<cfif (len(trim(getbody())) AND NOT IsSimpleValue(trim(getbody())))>
			<cfset thisError.field = "body" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "body is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getbody())) GT 2147483647)>
			<cfset thisError.field = "body" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "body is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- keywords --->
		<cfif (len(trim(getkeywords())) AND NOT IsSimpleValue(trim(getkeywords())))>
			<cfset thisError.field = "keywords" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "keywords is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getkeywords())) GT 255)>
			<cfset thisError.field = "keywords" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "keywords is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- views --->
		<cfif (len(trim(getviews())) AND NOT isNumeric(trim(getviews())))>
			<cfset thisError.field = "views" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "views is not numeric" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- approved --->
		<cfif (len(trim(getapproved())) AND NOT isNumeric(trim(getapproved())))>
			<cfset thisError.field = "approved" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "approved is not numeric" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- dtCreated --->
		<cfif (len(trim(getdtCreated())) AND NOT isDate(trim(getdtCreated())))>
			<cfset thisError.field = "dtCreated" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "dtCreated is not a date" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- dtUpdated --->
		<cfif (len(trim(getdtUpdated())) AND NOT isDate(trim(getdtUpdated())))>
			<cfset thisError.field = "dtUpdated" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "dtUpdated is not a date" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- createdBy --->
		<cfif (len(trim(getcreatedBy())) AND NOT IsSimpleValue(trim(getcreatedBy())))>
			<cfset thisError.field = "createdBy" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "createdBy is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getcreatedBy())) GT 0)>
			<cfset thisError.field = "createdBy" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "createdBy is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- updatedBy --->
		<cfif (len(trim(getupdatedBy())) AND NOT IsSimpleValue(trim(getupdatedBy())))>
			<cfset thisError.field = "updatedBy" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "updatedBy is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getupdatedBy())) GT 0)>
			<cfset thisError.field = "updatedBy" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "updatedBy is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- ipCreated --->
		<cfif (len(trim(getipCreated())) AND NOT IsSimpleValue(trim(getipCreated())))>
			<cfset thisError.field = "ipCreated" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "ipCreated is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getipCreated())) GT 50)>
			<cfset thisError.field = "ipCreated" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "ipCreated is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- ipUpdated --->
		<cfif (len(trim(getipUpdated())) AND NOT IsSimpleValue(trim(getipUpdated())))>
			<cfset thisError.field = "ipUpdated" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "ipUpdated is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getipUpdated())) GT 50)>
			<cfset thisError.field = "ipUpdated" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "ipUpdated is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- siteId --->
		<cfif (len(trim(getsiteId())) AND NOT IsSimpleValue(trim(getsiteId())))>
			<cfset thisError.field = "siteId" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "siteId is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getsiteId())) GT 0)>
			<cfset thisError.field = "siteId" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "siteId is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- type --->
		<cfif (len(trim(gettype())) AND NOT IsSimpleValue(trim(gettype())))>
			<cfset thisError.field = "type" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "type is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(gettype())) GT 50)>
			<cfset thisError.field = "type" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "type is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- resourceId --->
		<cfif (len(trim(getresourceId())) AND NOT IsSimpleValue(trim(getresourceId())))>
			<cfset thisError.field = "resourceId" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "resourceId is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getresourceId())) GT 0)>
			<cfset thisError.field = "resourceId" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "resourceId is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<cfreturn errors />
	</cffunction>

	<!---
	ACCESSORS
	--->
	<cffunction name="setresource_id" access="public" returntype="void" output="false">
		<cfargument name="resource_id" type="string" required="true" />
		<cfset variables.instance.resource_id = arguments.resource_id />
	</cffunction>
	<cffunction name="getresource_id" access="public" returntype="string" output="false">
		<cfreturn variables.instance.resource_id />
	</cffunction>

	<cffunction name="settitle" access="public" returntype="void" output="false">
		<cfargument name="title" type="string" required="true" />
		<cfset variables.instance.title = arguments.title />
	</cffunction>
	<cffunction name="gettitle" access="public" returntype="string" output="false">
		<cfreturn variables.instance.title />
	</cffunction>

	<cffunction name="setbody" access="public" returntype="void" output="false">
		<cfargument name="body" type="string" required="true" />
		<cfset variables.instance.body = arguments.body />
	</cffunction>
	<cffunction name="getbody" access="public" returntype="string" output="false">
		<cfreturn variables.instance.body />
	</cffunction>

	<cffunction name="setkeywords" access="public" returntype="void" output="false">
		<cfargument name="keywords" type="string" required="true" />
		<cfset variables.instance.keywords = arguments.keywords />
	</cffunction>
	<cffunction name="getkeywords" access="public" returntype="string" output="false">
		<cfreturn variables.instance.keywords />
	</cffunction>

	<cffunction name="setviews" access="public" returntype="void" output="false">
		<cfargument name="views" type="string" required="true" />
		<cfset variables.instance.views = arguments.views />
	</cffunction>
	<cffunction name="getviews" access="public" returntype="string" output="false">
		<cfreturn variables.instance.views />
	</cffunction>

	<cffunction name="setapproved" access="public" returntype="void" output="false">
		<cfargument name="approved" type="string" required="true" />
		<cfset variables.instance.approved = arguments.approved />
	</cffunction>
	<cffunction name="getapproved" access="public" returntype="string" output="false">
		<cfreturn variables.instance.approved />
	</cffunction>

	<cffunction name="setdtCreated" access="public" returntype="void" output="false">
		<cfargument name="dtCreated" type="string" required="true" />
		<cfset variables.instance.dtCreated = arguments.dtCreated />
	</cffunction>
	<cffunction name="getdtCreated" access="public" returntype="string" output="false">
		<cfreturn variables.instance.dtCreated />
	</cffunction>

	<cffunction name="setdtUpdated" access="public" returntype="void" output="false">
		<cfargument name="dtUpdated" type="string" required="true" />
		<cfset variables.instance.dtUpdated = arguments.dtUpdated />
	</cffunction>
	<cffunction name="getdtUpdated" access="public" returntype="string" output="false">
		<cfreturn variables.instance.dtUpdated />
	</cffunction>

	<cffunction name="setcreatedBy" access="public" returntype="void" output="false">
		<cfargument name="createdBy" type="string" required="true" />
		<cfset variables.instance.createdBy = arguments.createdBy />
	</cffunction>
	<cffunction name="getcreatedBy" access="public" returntype="string" output="false">
		<cfreturn variables.instance.createdBy />
	</cffunction>

	<cffunction name="setupdatedBy" access="public" returntype="void" output="false">
		<cfargument name="updatedBy" type="string" required="true" />
		<cfset variables.instance.updatedBy = arguments.updatedBy />
	</cffunction>
	<cffunction name="getupdatedBy" access="public" returntype="string" output="false">
		<cfreturn variables.instance.updatedBy />
	</cffunction>

	<cffunction name="setipCreated" access="public" returntype="void" output="false">
		<cfargument name="ipCreated" type="string" required="true" />
		<cfset variables.instance.ipCreated = arguments.ipCreated />
	</cffunction>
	<cffunction name="getipCreated" access="public" returntype="string" output="false">
		<cfreturn variables.instance.ipCreated />
	</cffunction>

	<cffunction name="setipUpdated" access="public" returntype="void" output="false">
		<cfargument name="ipUpdated" type="string" required="true" />
		<cfset variables.instance.ipUpdated = arguments.ipUpdated />
	</cffunction>
	<cffunction name="getipUpdated" access="public" returntype="string" output="false">
		<cfreturn variables.instance.ipUpdated />
	</cffunction>

	<cffunction name="setsiteId" access="public" returntype="void" output="false">
		<cfargument name="siteId" type="string" required="true" />
		<cfset variables.instance.siteId = arguments.siteId />
	</cffunction>
	<cffunction name="getsiteId" access="public" returntype="string" output="false">
		<cfreturn variables.instance.siteId />
	</cffunction>

	<cffunction name="settype" access="public" returntype="void" output="false">
		<cfargument name="type" type="string" required="true" />
		<cfset variables.instance.type = arguments.type />
	</cffunction>
	<cffunction name="gettype" access="public" returntype="string" output="false">
		<cfreturn variables.instance.type />
	</cffunction>

	<cffunction name="setresourceId" access="public" returntype="void" output="false">
		<cfargument name="resourceId" type="string" required="true" />
		<cfset variables.instance.resourceId = arguments.resourceId />
	</cffunction>
	<cffunction name="getresourceId" access="public" returntype="string" output="false">
		<cfreturn variables.instance.resourceId />
	</cffunction>
	
	<cffunction name="setSummary" access="public" returntype="void" output="false">
		<cfargument name="summary" type="string" required="true" />
		<cfset variables.instance.summary = arguments.summary />
	</cffunction>
	<cffunction name="getSummary" access="public" returntype="string" output="false">
		<cfreturn variables.instance.summary />
	</cffunction>
	
	<cffunction name="setCreatedByName" access="public" returntype="void" output="false">
		<cfargument name="createdByName" type="string" required="true" />
		<cfset variables.instance.createdByName = arguments.createdByName />
	</cffunction>
	<cffunction name="getCreatedByName" access="public" returntype="string" output="false">
		<cfreturn variables.instance.createdByName />
	</cffunction>
	
	<cffunction name="setCategories" access="public" returntype="void" output="false">
		<cfargument name="categories" type="array" required="true" />
		<cfset variables.instance.categories = arguments.categories />
	</cffunction>
	<cffunction name="getCategories" access="public" returntype="array" output="false">
		<cfreturn variables.instance.categories />
	</cffunction>

	<cffunction name="setTypes" access="public" returntype="void" output="false">
		<cfargument name="types" type="array" required="true" />
		<cfset variables.instance.types = arguments.types />
	</cffunction>
	<cffunction name="getTypes" access="public" returntype="array" output="false">
		<cfreturn variables.instance.types />
	</cffunction>
    
	<cffunction name="setGrades" access="public" returntype="void" output="false">
		<cfargument name="grades" type="array" required="true" />
		<cfset variables.instance.grades = arguments.grades />
	</cffunction>
	<cffunction name="getGrades" access="public" returntype="array" output="false">
		<cfreturn variables.instance.grades />
	</cffunction>
	
	<cffunction name="setFiles" access="public" returntype="void" output="false">
		<cfargument name="files" type="array" required="true" />
		<cfset variables.instance.files = arguments.files />
	</cffunction>
	<cffunction name="getFiles" access="public" returntype="array" output="false">
		<cfreturn variables.instance.files />
	</cffunction>
	
	<cffunction name="setComments" access="public" returntype="void" output="false">
		<cfargument name="comments" type="array" required="true" />
		<cfset variables.instance.comments = arguments.comments />
	</cffunction>
	<cffunction name="getComments" access="public" returntype="array" output="false">
		<cfreturn variables.instance.comments />
	</cffunction>
	
	<cffunction name="setBlocks" access="public" returntype="void" output="false">
		<cfargument name="blocks" type="array" required="true" />
		<cfset variables.instance.blocks = arguments.blocks />
	</cffunction>
	<cffunction name="getBlocks" access="public" returntype="array" output="false">
		<cfreturn variables.instance.blocks />
	</cffunction>

	<!---
	DUMP
	--->
	<cffunction name="dump" access="public" output="true" return="void">
		<cfargument name="abort" type="boolean" default="false" />
		<cfdump var="#variables.instance#" />
		<cfif arguments.abort>
			<cfabort />
		</cfif>
	</cffunction>

</cfcomponent>