
<cfset event.setArg("pageTitle","#request.siteName# - Institute Application") />


<cfif not application.cfcs.register.isSTEMUserLoggedIn()>
	
	<cfsavecontent variable="js">
		<script type="text/javascript" src="/js/loginPopupScripts.js"></script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
	
	
	<!--- Need to login --->
	<cfoutput>
	<script type="text/javascript">
		document.observe("dom:loaded", function() {
			deGlobalWin({path:'#BuildUrl('xhr.loginForm')#', closebtn:false,background:'none'});
		});
	</script>        
	<div align="center" style="font-weight:bold; font-size:14px; padding:100px 30px 300px 30px;">
		Please <a href="javascript:void(0);" onclick="deGlobalWin({path:'#BuildUrl('xhr.loginForm')#', closebtn:false,background:'none'});">Login</a> before proceeding.
	</div>       
	</cfoutput>
	
</cfif>


<cfif application.cfcs.register.isSTEMUserLoggedIn()>
	<!--- <cfif not StructKeyExists(application, "tabbedForm")> --->
	<cfset application.tabbedForm = CreateObject("component", "cfcs.tabbedForm.tabbedForm").Init() />
	<!---</cfif> --->
	
	<cfset applicationGUID = application.tabbedForm.GetApplicationGUID(client.STEMRegistrationGUID) />
	
	<cfif not IsValid("guid", applicationGUID)>
		<cfquery name="insertRecord" datasource="Sponsorships">
			DECLARE @applicationGUID  varchar(50) = NewId();
			
			INSERT INTO STEMApplications (applicationGUID, registrationGUID)
			VALUES (
						@applicationGUID,
						<cfqueryparam value="#client.STEMRegistrationGUID#" cfsqltype="cf_sql_varchar" />
					);
						
			SELECT @applicationGUID AS applicationGUID;
		</cfquery>
		<cfset applicationGUID = insertRecord.applicationGUID />
		
	</cfif>
	
	<cfquery name="checkLandingCompleted" datasource="Sponsorships">
		SELECT programInstitute, programSTARs 
		FROM STEMApplications (nolock)
		WHERE applicationGUID = <cfqueryparam value="#applicationGUID#" cfsqltype="cf_sql_varchar" />
	</cfquery>
	
	<cfset pageLandingCompleted = false />
	<cfif checkLandingCompleted.RecordCount gt 0 AND (checkLandingCompleted.programInstitute neq "" OR checkLandingCompleted.programSTARs neq "")>
		<cfset pageLandingCompleted = true />
	</cfif>
	
	<cfif pageLandingCompleted eq false OR StructKeyExists(url, "landing")>
		<cfinclude template="/contest/landing.cfm" />
	<cfelse>
		<cfoutput>#application.tabbedForm.BuildForm(applicationGUID=applicationGUID)#</cfoutput>
	</cfif>
</cfif>



