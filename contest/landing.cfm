
<cfabort />

<cfif StructKeyExists(form, "fieldNames")>
	<cfquery name="savePage" datasource="Sponsorships">
		UPDATE STEMApplications
		SET	lastUpdated = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
			<cfif StructKeyExists(form, "program") AND (form.program eq "Institute" OR form.program eq "Both")>
				programInstitute = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
			<cfelse>
				programInstitute = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
			</cfif>
			<cfif StructKeyExists(form, "program") AND (form.program eq "STARs" OR form.program eq "Both")>
				programSTARs = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
			<cfelse>
				programSTARs = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
			</cfif>
			<cfif form.programPreference eq "">
				programPreference = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />
			<cfelse>
				programPreference = <cfqueryparam value="#form.programPreference#" cfsqltype="cf_sql_varchar" />
			</cfif>
		WHERE applicationGUID = <cfqueryparam value="#form.applicationGUID#" cfsqltype="cf_sql_varchar">
	</cfquery>
	
	<cfset qString = ReplaceNoCase(cgi.query_string, "&landing=1", "", "all") />
	
	<cflocation url="#cgi.script_name#?#qString#" addtoken="false" />
</cfif>


<cfset event.setArg("pageTitle","#request.siteName# - Institute Application") />

<cfquery name="getType" datasource="Sponsorships">
	SELECT programInstitute, programSTARs, programPreference FROM STEMApplications (nolock)
	WHERE applicationGUID = <cfqueryparam value="#applicationGUID#" cfsqltype="cf_sql_varchar">
</cfquery>


<div id="content-single" class="app-landing">
        	<h2 style="text-transform:none;">Siemens STEM Institute</h2>
			<em><b style="font-size:1.3em;">The Siemens Summer of Learning application is now live!</b></em>
            <br />
            <br />
            <p class="bold">The Siemens STEM Academy offers two one-of-a-kind, all-expenses-paid professional development opportunities for educators that bolster STEM learning and enable teachers to gain pragmatic skills that they can translate back into the classroom. </p>
            <p class="bold">Access the application below to apply to the STARs and Institute Programs. You can apply to one or both programs by completing this application only once. If you apply to both programs, you must complete the application requirements for each program before submitting your application. Please do not submit separate applications. </p>

<h3>PROGRAM INFORMATION</h3>
<div class="box institute">
<h4>Siemens STEM Institute</h4>
<p class="image-left"></span>Do you want to learn new tools and technologies that can boost learning in the classroom? Do you want to engage with top STEM leaders, scientists, and personalities? The Institute selects 50 middle and high school educators to attend an all-expenses-paid week at <a href="http://dsc.redacted.com/" target="_blank">redacted's world headquarters</a> outside of Washington, D.C. Fellows will learn about hands-on STEM integration in the classroom, take field trips to leading institutions to observe real-world STEM applications, and network and engage with STEM leaders and peers.  </p>
<ul>
<li><a href="http://stem.redactededucation.com/index.cfm?event=showContent&c=50">Program Overview</a></li>
<li><a href="http://stem.redactededucation.com/index.cfm?event=showContent&id=42&c=36">Criteria </a></li>
<li><a href="http://stem.redactededucation.com/index.cfm?event=showContent&c=36">2010 Photos and Itinerary</a></li>
<li>Apply Below</li>
</ul>
</div>
<div class="box stars last-box">
<h4>Siemens Teachers as Researchers (STARs)</h4>
<p class="image-left"></span>Have you ever wanted to be part of scientific research team? Have you ever wanted to bring the excitement of authentic research into the classroom? STARs gives 20 middle and high school STEM educators the opportunity to spend two weeks, all-expenses-paid, engaging with top scientists and researchers on short-term mentored research projects at Oak Ridge National Laboratory, America's largest, multi-purpose, national research laboratory. </p>
<ul>
<li><a href="http://stem.redactededucation.com/index.cfm?event=showContent&id=47&c=38">Program Overview</a></li>
<li><a href="http://stem.redactededucation.com/index.cfm?event=showContent&id=39&c=38">Criteria </a></li>
<li><a href="http://stem.redactededucation.com/index.cfm?event=showContent&c=38">2010 Photos and Itinerary</a></li>
<li>Apply Below</li>
</ul>
</div>



<hr />
<h3>Access Application <span class="required-legend">Fields with an asterisk(<em>*</em>) are required</span></h3>

<form method="post">
	<p><em>*</em> To begin the application process, please select the program(s) you are applying for:</p>
	<div class="row">
		<cfoutput>
		<input type="hidden" name="applicationGUID" value="#applicationGUID#" />
		
		<label>
			<input type="radio" name="program" value="Institute" <cfif getType.programInstitute eq 1 AND getType.programSTARs eq 0>checked="checked"</cfif> />Institute
		</label>
		<label>
			<input type="radio" name="program" value="STARs" <cfif getType.programInstitute eq 0 AND getType.programSTARs eq 1>checked="checked"</cfif> />STARs
		</label>
		<label>
			<input type="radio" name="program" value="Both" <cfif getType.programInstitute eq 1 AND getType.programSTARs eq 1>checked="checked"</cfif> />Both
		</label>
		
		<label class="bold"><em>*</em> Program Preference
			<select name="programPreference">
				<option value="">- Select One -</option>
				<option value="Institute" <cfif getType.programPreference eq "Institute">selected="selected"</cfif>>Institute</option>
				<option value="STARs" <cfif getType.programPreference eq "STARs">selected="selected"</cfif>>STARs</option>
			</select>
		</label>
		
		<input type="submit" value="Access Application" style="cursor:pointer;" />
		</cfoutput>
	</div>
</form>

