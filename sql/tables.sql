all tables in the Sponsorships database with the prefix 'cms_' need to be moved up to stage and prod:
USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_categories]    Script Date: 01/15/2010 03:29:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_categories](
	[categoryId] [uniqueidentifier] NOT NULL,
	[siteId] [uniqueidentifier] NULL,
	[parentId] [uniqueidentifier] NULL,
	[name] [varchar](255) NOT NULL,
	[isActive] [bit] NULL,
	[dtCreated] [datetime] NULL,
	[dtUpdated] [datetime] NULL,
	[createdBy] [uniqueidentifier] NULL,
	[updatedBy] [uniqueidentifier] NULL,
	[ipCreated] [varchar](50) NULL,
	[ipUpdated] [varchar](50) NULL,
	[orderNo] [int] NULL,
 CONSTRAINT [PK_cms_categories] PRIMARY KEY CLUSTERED 
(
	[categoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_comments]    Script Date: 01/15/2010 03:30:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_comments](
	[comment_id] [int] IDENTITY(1,1) NOT NULL,
	[content_id] [int] NULL,
	[contentGuid] [uniqueidentifier] NULL,
	[author] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[url] [varchar](255) NULL,
	[body] [text] NULL,
	[dtCreated] [datetime] NULL,
	[ipCreated] [varchar](50) NULL,
	[approved] [bit] NULL,
	[agent] [varchar](255) NULL,
	[app_user_guid] [uniqueidentifier] NULL,
	[type] [varchar](50) NULL,
	[parent] [int] NULL,
 CONSTRAINT [PK_cms_comments] PRIMARY KEY CLUSTERED 
(
	[comment_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_contentRatings]    Script Date: 01/15/2010 03:30:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[cms_contentRatings](
	[rating_id] [int] IDENTITY(1,1) NOT NULL,
	[contentId] [uniqueidentifier] NULL,
	[app_user_guid] [uniqueidentifier] NULL,
	[siteId] [uniqueidentifier] NULL,
	[rate] [float] NULL,
	[dtCreated] [datetime] NULL,
 CONSTRAINT [PK_cms_contentRatings] PRIMARY KEY CLUSTERED 
(
	[rating_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_encodings]    Script Date: 01/15/2010 03:31:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_encodings](
	[encoding_id] [int] IDENTITY(1,1) NOT NULL,
	[fileGuid] [uniqueidentifier] NULL,
	[source] [varchar](500) NULL,
	[destination] [varchar](500) NULL,
	[result] [text] NULL,
	[dtCreated] [datetime] NULL,
	[ipCreated] [varchar](50) NULL,
 CONSTRAINT [PK_cms_encodings] PRIMARY KEY CLUSTERED 
(
	[encoding_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_files]    Script Date: 01/15/2010 03:31:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_files](
	[file_Id] [int] IDENTITY(1,1) NOT NULL,
	[fileId] [uniqueidentifier] NULL,
	[contentId] [uniqueidentifier] NULL,
	[siteId] [uniqueidentifier] NULL,
	[filename] [varchar](255) NULL,
	[thumbnail] [varchar](255) NULL,
	[fileSize] [int] NULL,
	[contentType] [varchar](50) NULL,
	[contentSubType] [varchar](50) NULL,
	[fileExt] [varchar](50) NULL,
	[dtCreated] [datetime] NULL,
	[dtUpdated] [datetime] NULL,
	[createdBy] [uniqueidentifier] NULL,
	[updatedBy] [uniqueidentifier] NULL,
	[ipCreated] [varchar](50) NULL,
	[ipUpdated] [varchar](50) NULL,
 CONSTRAINT [PK_cms_files] PRIMARY KEY CLUSTERED 
(
	[file_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_files]    Script Date: 01/15/2010 03:31:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_files](
	[file_Id] [int] IDENTITY(1,1) NOT NULL,
	[fileId] [uniqueidentifier] NULL,
	[contentId] [uniqueidentifier] NULL,
	[siteId] [uniqueidentifier] NULL,
	[filename] [varchar](255) NULL,
	[thumbnail] [varchar](255) NULL,
	[fileSize] [int] NULL,
	[contentType] [varchar](50) NULL,
	[contentSubType] [varchar](50) NULL,
	[fileExt] [varchar](50) NULL,
	[dtCreated] [datetime] NULL,
	[dtUpdated] [datetime] NULL,
	[createdBy] [uniqueidentifier] NULL,
	[updatedBy] [uniqueidentifier] NULL,
	[ipCreated] [varchar](50) NULL,
	[ipUpdated] [varchar](50) NULL,
 CONSTRAINT [PK_cms_files] PRIMARY KEY CLUSTERED 
(
	[file_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_resource_categories]    Script Date: 01/15/2010 03:31:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[cms_resource_categories](
	[resource_category_id] [int] IDENTITY(1,1) NOT NULL,
	[resourceId] [uniqueidentifier] NOT NULL,
	[categoryId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_cms_resource_categories] PRIMARY KEY CLUSTERED 
(
	[resource_category_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_resource_grades]    Script Date: 01/15/2010 03:32:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[cms_resource_grades](
	[resource_grade_id] [int] IDENTITY(1,1) NOT NULL,
	[resourceId] [uniqueidentifier] NOT NULL,
	[grade_id] [int] NOT NULL,
 CONSTRAINT [PK_cms_resource_grades] PRIMARY KEY CLUSTERED 
(
	[resource_grade_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_resources]    Script Date: 01/15/2010 03:32:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_resources](
	[resource_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NULL,
	[body] [text] NULL,
	[keywords] [varchar](255) NULL,
	[views] [int] NULL,
	[approved] [int] NULL,
	[dtCreated] [datetime] NULL,
	[dtUpdated] [datetime] NULL,
	[createdBy] [uniqueidentifier] NULL,
	[updatedBy] [uniqueidentifier] NULL,
	[ipCreated] [varchar](50) NULL,
	[ipUpdated] [varchar](50) NULL,
	[siteId] [uniqueidentifier] NULL,
	[type] [varchar](50) NULL,
	[resourceId] [uniqueidentifier] NULL,
	[summary] [text] NULL,
	[createdByName] [varchar](255) NULL,
 CONSTRAINT [PK_cms_resources] PRIMARY KEY CLUSTERED 
(
	[resource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_searches]    Script Date: 01/15/2010 03:32:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_searches](
	[terms] [varchar](250) NOT NULL,
	[dtCreated] [datetime] NOT NULL,
	[siteId] [uniqueidentifier] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_searches_aggregate]    Script Date: 01/15/2010 03:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_searches_aggregate](
	[terms] [varchar](250) NULL,
	[theDate] [datetime] NULL,
	[numSearches] [int] NULL,
	[siteId] [uniqueidentifier] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_tcontent]    Script Date: 01/15/2010 03:33:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_tcontent](
	[tcontent_id] [int] IDENTITY(1,1) NOT NULL,
	[siteId] [uniqueidentifier] NULL,
	[parentId] [uniqueidentifier] NULL,
	[contentId] [uniqueidentifier] NULL,
	[template] [varchar](50) NULL,
	[type] [varchar](50) NULL,
	[isActive] [bit] NULL,
	[orderNo] [int] NULL,
	[title] [varchar](255) NULL,
	[menuTitle] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[metaDesc] [text] NULL,
	[metaKeywords] [text] NULL,
	[body] [text] NULL,
	[dtCreated] [datetime] NULL,
	[dtUpdated] [datetime] NULL,
	[createdBy] [varchar](50) NULL,
	[createdById] [varchar](50) NULL,
	[ipCreated] [varchar](50) NULL,
	[ipUpdated] [varchar](50) NULL,
	[display] [bit] NULL,
	[approved] [bit] NULL,
	[isNav] [bit] NULL,
 CONSTRAINT [PK_cms_tcontent] PRIMARY KEY CLUSTERED 
(
	[tcontent_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_tcontent]    Script Date: 01/15/2010 03:33:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_tcontent](
	[tcontent_id] [int] IDENTITY(1,1) NOT NULL,
	[siteId] [uniqueidentifier] NULL,
	[parentId] [uniqueidentifier] NULL,
	[contentId] [uniqueidentifier] NULL,
	[template] [varchar](50) NULL,
	[type] [varchar](50) NULL,
	[isActive] [bit] NULL,
	[orderNo] [int] NULL,
	[title] [varchar](255) NULL,
	[menuTitle] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[metaDesc] [text] NULL,
	[metaKeywords] [text] NULL,
	[body] [text] NULL,
	[dtCreated] [datetime] NULL,
	[dtUpdated] [datetime] NULL,
	[createdBy] [varchar](50) NULL,
	[createdById] [varchar](50) NULL,
	[ipCreated] [varchar](50) NULL,
	[ipUpdated] [varchar](50) NULL,
	[display] [bit] NULL,
	[approved] [bit] NULL,
	[isNav] [bit] NULL,
 CONSTRAINT [PK_cms_tcontent] PRIMARY KEY CLUSTERED 
(
	[tcontent_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Sponsorships]
GO

/****** Object:  Table [dbo].[cms_websiteUsers]    Script Date: 01/15/2010 03:33:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cms_websiteUsers](
	[siteId] [uniqueidentifier] NOT NULL,
	[app_user_guid] [uniqueidentifier] NOT NULL,
	[accessType] [varchar](255) NULL,
	[created_by] [uniqueidentifier] NULL,
	[create_date] [datetime] NULL,
	[updated_by] [uniqueidentifier] NULL,
	[update_date] [datetime] NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_cms_websiteUsers] PRIMARY KEY CLUSTERED 
(
	[siteId] ASC,
	[app_user_guid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

