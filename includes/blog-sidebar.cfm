<cfsilent>
	<cfset showPosts = 3 />
	<cfif ArrayLen(posts) LT 3>
		<cfset showPosts = ArrayLen(posts) />
	</cfif>
</cfsilent>
<cfoutput>
<div id="blog-right">
	<a href="http://feeds.feedburner.com/StemBlog" id="blogfeed" title="Subscribe to this Blog" target="_blank">Subscribe to this Blog</a>
	<!---a href="#application.env.stemblog_url#/feed/" id="blogfeed" title="Subscribe to this Blog" target="_blank">Subscribe to this Blog</a--->
	<br />
	<br clear="all" />
	<h4 class="sidebar-light">Recent Posts</h4>
	<ul class="linklist">
	<cfloop index="i" from="1" to="#showPosts#">
	<cfif posts[i].post_status EQ "publish">
		<li><a href="#BuildUrl('showBlogPost','postid=#posts[i].postid#|c=#c#')#" class="wedgelink">#posts[i].title#</a></li>
	</cfif>
	</cfloop>
</cfoutput>
	</ul>
<div class="column">
	<h4 class="blue-col">Meet the Contributors</h4>
	<div class="emc">
		
		<a href="/index.cfm?event=showResource&c=37&resourceId=f6bb1769%2D1438%2Dbbfa%2D606e%2D10c177f234fe"
			><img src="/images/blog-mikegorman.png" class="thumb" /></a>
		<div class="emc-txt">
		<h5><a href="/index.cfm?event=showResource&c=37&resourceId=f6bb1769%2D1438%2Dbbfa%2D606e%2D10c177f234fe">Mike Gorman</a></h5>		
		<p>Mike is an advocate for transforming education and bringing 21st Century Skills to classrooms. He was awarded Indiana STEM Educator of the Year and honored as a Microsoft 365 Global Education Hero.<br /><a href="/index.cfm?event=showResource&c=37&resourceId=f6bb1769%2D1438%2Dbbfa%2D606e%2D10c177f234fe">Learn More</a></p>
		</div><!---emc-txt--->
		<br clear="all" />
		<br />
		<!---  --->
		<a href="/index.cfm?event=showResource&c=37&resourceId=1f314ce9-237d-0718-f45f-923f731c7dde"><img src="/images/blog-lance.jpg" class="thumb" /></a>		
		<div class="emc-txt">
		<h5><a href="/index.cfm?event=showResource&c=37&resourceId=1f314ce9-237d-0718-f45f-923f731c7dde">Lance Rougeux</a></h5>
		<!--em>Harvard Dept. of Hard Knocks</em-->
		<p>Lance has been featured in the <em>Philadelphia Inquirer</em> and was recently highlighted in <em>The Emergency Teacher</em>, a book about urban teaching.<br /><a href="/index.cfm?event=showResource&c=37&resourceId=1f314ce9-237d-0718-f45f-923f731c7dde">Learn More</a></p>
		</div><!---emc-txt--->
	</div><!---emc--->
	<img src="/images/columns-bottom-blue.gif" alt="Curved Blue Box Bottom" />

	<!--- <a href="index.cfm?event=showGiveaway&c=37" id="tile-shark" style="margin-top:20px;">
        <span>Take a bite out of learning -- be one of the first 100 to upload resources and get a free shark USB. (shark usb image)</span>
        <span>Upload Resources</span>
    </a> --->
</div>
</div><!---blog--->