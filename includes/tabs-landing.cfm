<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>SIEMENS STEM ACADEMY</title>
	<style type="text/css">
	body {font-family:Arial, Helvetica, sans-serif;}/*Remove this if it is a global link property*/
	a {outline:none;}/*Remove this if it is a global link property or if it's needed for 508*/
	       
	
	#tabs-landing {background:url(/images/bg-tabs.jpg) repeat-y; position:relative;}
	#tabs-landing-bg {width:428px; height:482px; background:url(/images/sprite-tabs.jpg) no-repeat; position:absolute; top:0px; left:0px; z-index:0;} 
	#tabsWrap {height:45px; position:relative; z-index:1;}
	#tabsWrap a.tabs {background:url(/images/sprite-tabs.jpg) no-repeat; height:30px; display:block; position:absolute; top:6px;}
		#tabsWrap a#tab-most {background-position:-166px -495px; width:147px; left:6px;}
		#tabsWrap a#tab-recent {background-position:0 -495px; width:166px; left:158px;}
		#tabsWrap a#tab-most:hover {background-position:-166px -525px;}
		#tabsWrap a#tab-recent:hover {background-position:0 -525px;}
		#tab-most-sel {background:#fff url(/images/sprite-tabs.jpg) 0 -555px no-repeat; position:absolute; top:6px; left:7px; height:40px;/*needs a bit extra height to cover rounded corner*/ width:144px;}
		#tab-recent-sel {background:#fff url(/images/sprite-tabs.jpg) -144px -555px no-repeat; position:absolute; top:6px; left:158px; height:40px;/*needs a bit extra height to cover rounded corner*/ width:166px;}
			#tabsWrap a b { position:absolute; top:-5000px; left:-5000px;}
			a.tab-rss {background:url(/images/sprite-tabs.jpg) -314px -496px no-repeat;height:26px; width:25px; position:absolute; right:3px; top:3px;}
			a.tab-rss:hover {background-position:-314px -522px;}
			#tabsWrap #tab-recent-sel a.tab-rss { right:4px;}
	#tabs-btm {height:13px; width:428px; background:url(/images/btm-tabs.jpg) no-repeat;}
	
	div.tabsContent {margin:14px 0 0 7px; width:415px; color:#333; background:#f8f8f7 url(/images/bgV-tabs.jpg) repeat-x; position:relative; z-index:1;}
	div.tabResWrap {background:#f3f3f3 url(/images/sprite-tabs.jpg) 0 -683px repeat-x; width:393px; height:135px; border:1px #e4e4e4 solid; margin-bottom:4px; text-align:left; position:relative;}		
		div.tabResWrap:hover {background:#fff; border:1px #ccc solid;}		
	
	a.resThumbWrap {position:absolute; top:9px; left:9px; height:87px; width:126px;background:url(/images/sprite-tabs.jpg) 0 -595px repeat-x;}
	a.resThumbWrap img {border:none; position:absolute; top:5px; left:6px; height:75px; width:114px; overflow:hidden;}	
	a.resThumbWrap:hover img {-moz-opacity:.70; opacity:.70; /*please remove the following 2 properties and place in IE only .css, they apply to IE6-7;IE8*/ filter:alpha(opacity=70);-ms-filter:"alpha(opacity=70)";}		
		
	div.resTextWrap {position:absolute; top:9px; left:146px; width:238px; font-size:11px;}		
	div.resTextWrap a {color:#006699;}
	div.resTextWrap a:hover, div.resTextWrap a.resTitle:hover {color:#009999;}
	
	div.resTextWrap a.resTitle {font-size:12px; color:#015787; font-weight:bold; text-decoration:none; font-weight:bold; display:block;}
	a.more {font-size:11px;}

	div.tabResWrap .credits {position:absolute; top:113px; left:13px; font-size:11px;}
    div.tabResWrap .credits span {color:#009999;}
	
	div.tabResWrap .rating-wrap-static {position:absolute; top:99px; left:12px;}

	
	/*just to be sure there isn't anything tricky coming back in these descriptions*/
	.resTextWrap ul, .resTextWrap img, .resTextWrap object {display:none;}


	/*If you're getting to this before me, go ahead and drop this in a ratings section of the CSS document*/
			/*ratings*/
			.rating-wrap-static {
				height:10px;
				overflow:hidden;
				position:relative;
				width:50px;
				background-image:url(/images/star-ratings-small-bg.jpg);
			}
			
			.rating-wrap-static div.de-ratings {
				height:10px;
				left:0;
				position:absolute;
				top:0;
				background-image:url(/images/star-ratings-small.jpg);
				z-index:1;
			}
	
			.nocursor {
				cursor:default;
			}
						 
	</style>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.1.0/prototype.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.2/scriptaculous.js?load=effects"></script>

	<!--SCRIPT BLOCK SHOULD BE INCLUDED IN GLOBAL JS FILE-->
	<script type="text/javascript" language="javascript">
	
		pSel = "tab-most";
		function landingTabs(el){
			var pnSel = $(pSel);
			var pnSelT = $(pSel+'-sel');
			var pnSelC = $(pSel+'-con');
			pnSelT.hide();
			//pnSel.appear({ duration: .5 });
			pnSel.show();
			pnSelC.hide();
			
			var sel = $(el);
			var selT = $(el+'-sel');
			var selC = $(el+'-con');
		    sel.hide();
			selT.show();
			selC.appear({ duration: .6 });
			
			/*set selected*/
			pSel = el;
		}
		
		
		document.observe("dom:loaded", function() {
		  // initially hide all containers for tab content
		  //new Effect.Scale($('stars1'), 50);
		});

	
	</script>


</head>

<body>
	
    <div id="tabs-landing">
    	<div id="tabsWrap">
        
        	<!--UNSELECTED TABS-->
        	<a href="##" onclick="landingTabs(this.id); return false;" id="tab-most" class="tabs" style="display:none;"><b>Most Viewed STEM Resources</b></a>
            <a href="##" onclick="landingTabs(this.id); return false;" id="tab-recent" class="tabs"><b>Recently Added STEM Resources</b></a>
            
            <!--SELECTED TABS-->
           	<div id="tab-most-sel">
            	<a href="##" class="tab-rss" title="Subscribe to RSS Feed"><b>Subscribe to STEM Most Viewed Resources RSS Feed</b></a>
            </div><!--//tab-most-sel-->
            
            <div id="tab-recent-sel" style="display:none;">
            	<a href="##" class="tab-rss" title="Subscribe to RSS Feed"><b>Subscribe to STEM Recently Added Resources RSS Feed</b></a>
            </div><!--//tab-recent-sel-->
            
        </div><!--//tabs-->	
    
    
    	<div id="tab-most-con" class="tabsContent" align="center">
        	    
            <div class="tabResWrap">
				
                <a href="" class="resThumbWrap">
                	<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#PLACE TITLE HERE#"/>
                </a><!--//resWrap-->
                        
                <div class="resTextWrap">
                	<a href="" class="resTitle">7th Grade Science - Building Bridges</a>
                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque purus et nisi vulputate non scelerisque nulla volutpat. Ut at turpis interdum eros dictum convallis in nec diam. Morbi ut metus sit amet velit scelerisque pharetra ac ut arcu. Ut orci justo, aliquet...<a href="" class="more">more</a>
                </div><!--//resTextWrap-->        
                        
                 <div class="rating-wrap-static">
                    <!---
					<cfset p_Rating = 4.19234>
                    <cfset rating = #p_Rating# * 10>--->
                    <!---<cfoutput>#rating#</cfoutput>--->
                    <div class="de-ratings nocursor" style="width:42.9268px;">
                    </div>
                </div>
                        
                <div class="credits">
                <span>From:</span>  psmith@cardin
                </div>        
                        
            </div><!--//tabResWrap-->
            
            
            
            <div class="tabResWrap">
				
                <a href="" class="resThumbWrap">
                	<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#PLACE TITLE HERE#"/>
                </a><!--//resWrap-->
                        
                <div class="resTextWrap">
                	<a href="" class="resTitle">Fusce nisl lorem -- adipiscing</a>
                	Fusce consectetur leo quis odio blandit at consequat ligula malesuada. In vel est lacus, eget faucibus sem. Curabitur sit amet facilisis leo. Mauris a massa. 	<a href="" class="more">more</a>
                </div><!--//resTextWrap-->        
                        
                 <div class="rating-wrap-static">
                    <!---
					<cfset p_Rating = 4.19234>
                    <cfset rating = #p_Rating# * 10>--->
                    <!---<cfoutput>#rating#</cfoutput>--->
                    <div class="de-ratings nocursor" style="width:42.9268px;">
                    </div>
                </div>
                        
                <div class="credits">
                <span>From:</span>  psmith@cardin
                </div>        
                        
            </div><!--//tabResWrap-->
        
        
        
        	<div class="tabResWrap">
				
                <a href="" class="resThumbWrap">
                	<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#PLACE TITLE HERE#"/>
                </a><!--//resWrap-->
                        
                <div class="resTextWrap">
                	<a href="" class="resTitle">Quisque eu Purus vel Tortor Adipiscing Consequat Duis sit Amet Velit Nisl.</a>
                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque purus et nisi vulputate non scelerisque nulla volutpat. Ut at turpis interdum eros dictum convallis in nec diam. Morbi ut metus sit amet velit scelerisque pharetra ac...<a href="" class="more">more</a>
                </div><!--//resTextWrap-->        
                        
                 <div class="rating-wrap-static">
                    <!---
					<cfset p_Rating = 4.19234>
                    <cfset rating = #p_Rating# * 10>--->
                    <!---<cfoutput>#rating#</cfoutput>--->
                    <div class="de-ratings nocursor" style="width:42.9268px;">
                    </div>
                </div>
                        
                <div class="credits">
                <span>From:</span>  psmith@cardin
                </div>        
                        
            </div><!--//tabResWrap-->
            
            
            
            <div class="tabResWrap">
				
                <a href="" class="resThumbWrap">
                	<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#PLACE TITLE HERE#"/>
                </a><!--//resWrap-->
                        
                <div class="resTextWrap">
                	<a href="" class="resTitle">Quisque eu Purus vel Tortor Adipiscing Consequat Duis sit Amet Velit Nisl.</a>
                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque purus et nisi vulputate non scelerisque nulla volutpat. Ut at turpis interdum eros dictum convallis in nec diam. Morbi ut metus sit amet velit scelerisque pharetra ac...<a href="" class="more">more</a>
                </div><!--//resTextWrap-->        
                        
                 <div class="rating-wrap-static">
                    <!---
					<cfset p_Rating = 4.19234>
                    <cfset rating = #p_Rating# * 10>--->
                    <!---<cfoutput>#rating#</cfoutput>--->
                    <div class="de-ratings nocursor" style="width:42.9268px;">
                    </div>
                </div>
                        
                <div class="credits">
                <span>From:</span>  psmith@cardin
                </div>        
                        
            </div><!--//tabResWrap-->

        </div><!--//tabs-most-content-->
        
        
        
        
        
        
        <div id="tab-recent-con" class="tabsContent" align="center" style="display:none;">
        	
            <div class="tabResWrap">
				
                <a href="" class="resThumbWrap">
                	<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#PLACE TITLE HERE#"/>
                </a><!--//resWrap-->
                        
                <div class="resTextWrap">
                	<a href="" class="resTitle">7th Grade Science - Building Bridges</a>
                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque purus et nisi vulputate non scelerisque nulla volutpat. Ut at turpis interdum eros dictum convallis in nec diam. Morbi ut metus sit amet velit scelerisque pharetra ac ut arcu. Ut orci justo, aliquet...<a href="" class="more">more</a>
                </div><!--//resTextWrap-->        
                        
                 <div class="rating-wrap-static">
                    <!---
					<cfset p_Rating = 4.19234>
                    <cfset rating = #p_Rating# * 10>--->
                    <!---<cfoutput>#rating#</cfoutput>--->
                    <div class="de-ratings nocursor" style="width:42.9268px;">
                    </div>
                </div>
                        
                <div class="credits">
                <span>From:</span>  psmith@cardin
                </div>        
                        
            </div><!--//tabResWrap-->
            
            
            
            <div class="tabResWrap">
				
                <a href="" class="resThumbWrap">
                	<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#PLACE TITLE HERE#"/>
                </a><!--//resWrap-->
                        
                <div class="resTextWrap">
                	<a href="" class="resTitle">7th Grade Science - Building Bridges</a>
                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque purus et nisi vulputate non scelerisque nulla volutpat. Ut at turpis interdum eros dictum convallis in nec diam. Morbi ut metus sit amet velit scelerisque pharetra ac ut arcu. Ut orci justo, aliquet...<a href="" class="more">more</a>
                </div><!--//resTextWrap-->        
                        
                 <div class="rating-wrap-static">
                    <!---
					<cfset p_Rating = 4.19234>
                    <cfset rating = #p_Rating# * 10>--->
                    <!---<cfoutput>#rating#</cfoutput>--->
                    <div class="de-ratings nocursor" style="width:42.9268px;">
                    </div>
                </div>
                        
                <div class="credits">
                <span>From:</span>  psmith@cardin
                </div>        
                        
            </div><!--//tabResWrap-->
        
        
        
        	<div class="tabResWrap">
				
                <a href="" class="resThumbWrap">
                	<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#PLACE TITLE HERE#"/>
                </a><!--//resWrap-->
                        
                <div class="resTextWrap">
                	<a href="" class="resTitle">7th Grade Science - Building Bridges</a>
                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque purus et nisi vulputate non scelerisque nulla volutpat. Ut at turpis interdum eros dictum convallis in nec diam. Morbi ut metus sit amet velit scelerisque pharetra ac ut arcu. Ut orci justo, aliquet...<a href="" class="more">more</a>
                </div><!--//resTextWrap-->        
                        
                 <div class="rating-wrap-static">
                    <!---
					<cfset p_Rating = 4.19234>
                    <cfset rating = #p_Rating# * 10>--->
                    <!---<cfoutput>#rating#</cfoutput>--->
                    <div class="de-ratings nocursor" style="width:42.9268px;">
                    </div>
                </div>
                        
                <div class="credits">
                <span>From:</span>  psmith@cardin
                </div>        
                        
            </div><!--//tabResWrap-->
            
        </div><!--//tabs-most-content-->
    
    	 <div id="tabs-landing-bg">
         	<!--bg image here-->
         </div><!--//tabs-landing-bg-->
    	<div id="tabs-btm">
        	<!--bg img here-->
        </div><!--//tabs-btm-->
    
    
    </div><!--//tabs-landing-->
	


</body>
</html>
