<div>
	<div id="lb-top"><a href="##" id="lb-logout" onclick="clearWin();"></a></div>
	<div id="lb-middle">
	
	<div id="hyperlinks-howto">
	
	<h4>Inserting Hyperlinks</h4>

	<br />

			<img src="/images/hyperlinks-1.jpg" alt="Highlight your text" />


			<p><strong>Highlight some text that you would like to turn into a hyperlink.</strong></p>
			
			<br />
			<img src="/images/hyperlinks-2.jpg" alt="Press the hyperlink button" />
			
			
			<p>In the top right hand corner of the editor, <strong>click the hyperlink button</strong>. It looks like two interconnected chain links.</p>
			
			<br />
			<img src="/images/hyperlinks-3.jpg" alt="Type or paste your link into the URL field" />
			
			<p><strong>Enter your link into the field marked "URL"</strong>. You should also enter a title and any other optional link attributes (anchors, class etc.) In the Target dropdown menu, select "Open in a New Window (_blank).</p>
			
			<p><strong>Click the Insert button</strong>. Congratulations, you have just inserted a link into your content.</p>
	</div>		
			
			
			
	</div>
	
	<div id="lb-bottom"></div>
</div>