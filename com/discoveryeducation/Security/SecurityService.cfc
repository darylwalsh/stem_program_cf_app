<cfcomponent displayname="SecurityService" output="false" hint="SecurityService">

	<cffunction name="init" access="public" output="false" returntype="SecurityService" hint="Constructor">
		<cfreturn this />
	</cffunction>
	
	<!--- dependencies --->
	<cffunction name="setUserService" access="public" output="false" returntype="void">
		<cfargument name="UserService" type="com.redactededucation.User.UserService" required="true" />
		<cfset variables.UserService = arguments.UserService />
	</cffunction>
	<cffunction name="getUserService" access="public" output="false" returntype="com.redactededucation.User.UserService">
		<cfreturn variables.UserService />
	</cffunction>
	
	<cffunction name="setSessionService" access="public" output="false" returntype="void">
		<cfargument name="sessionService" type="com.redactededucation.Session.SessionService" required="true" />
		<cfset variables.sessionService = arguments.sessionService />
	</cffunction>
	<cffunction name="getSessionService" access="public" output="false" returntype="com.redactededucation.Session.SessionService">
		<cfreturn variables.sessionService />
	</cffunction>
	
	<!--- service methods --->
	<cffunction name="getUserByCredentials" access="public" output="false" returntype="com.redactededucation.User.User">
		<cfargument name="uid" type="string" required="true" />
		<cfargument name="pwd" type="string" required="true" />
		
		<cfreturn getUserService().getUserByCredentials(arguments.uid,arguments.pwd) />
	</cffunction>
	
	<cffunction name="getUserById" access="public" output="false" returntype="com.redactededucation.User.User">
		<cfargument name="app_user_guid" type="string" required="true" />
		
		<cfreturn getUserService().getUser(arguments.app_user_guid) />
	</cffunction>
	
	<cffunction name="authenticate" access="public" output="false" returntype="com.redactededucation.User.User">
		<cfargument name="uid" type="string" required="true"/>
		<cfargument name="pwd" type="string" required="true"/>
		
		<cfset var user = getUserByCredentials(arguments.uid,arguments.pwd) />
		
		<cfif user.getApp_user_guid() is not "">
			<!--- store validated user in session --->
			<cfset getSessionService().setUser(user) />
		<cfelse>
			<cfset getSessionService().resetSession() />
		</cfif>
		
		<cfreturn user />
	</cffunction>
	
	<cffunction name="authenticateById" access="public" output="false" returntype="com.redactededucation.user.User">
		<cfargument name="app_user_guid" type="string" required="true" />
		
		<cfset var user = getUserById(arguments.app_user_guid) />
		
		<cfif user.getApp_user_guid NEQ "">
			<!--- store validated user in session --->
			<cfset getSessionService().setUser(user) />
		<cfelse>
			<cfset getSessionService().resetSession() />
		</cfif>
		
		<cfreturn user />
	</cffunction>
	
	<cffunction name="isAuthenticated" access="public" output="false" returntype="boolean">
		<cfif getSessionService().getUser().getApp_user_guid() is not "">
			<cfreturn true />
		<cfelse>
			<cfreturn false />
		</cfif>
	</cffunction>
</cfcomponent>