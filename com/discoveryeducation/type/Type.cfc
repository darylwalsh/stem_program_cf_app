<cfcomponent displayname="Type" output="false">
		<cfproperty name="typeId" type="string" default="" />
		<cfproperty name="siteId" type="string" default="" />
		<cfproperty name="parentId" type="string" default="" />
		<cfproperty name="name" type="string" default="" />
		<cfproperty name="isActive" type="boolean" default="" />
		<cfproperty name="dtCreated" type="date" default="" />
		<cfproperty name="dtUpdated" type="date" default="" />
		<cfproperty name="createdBy" type="string" default="" />
		<cfproperty name="updatedBy" type="string" default="" />
		<cfproperty name="ipCreated" type="string" default="" />
		<cfproperty name="ipUpdated" type="string" default="" />
		<cfproperty name="orderNo" type="numeric" default="" />
		
	<!---
	PROPERTIES
	--->
	<cfset variables.instance = StructNew() />

	<!---
	INITIALIZATION / CONFIGURATION
	--->
	<cffunction name="init" access="public" returntype="com.redactededucation.type.Type" output="false">
		<cfargument name="typeId" type="string" required="false" default="" />
		<cfargument name="siteId" type="string" required="false" default="" />
		<cfargument name="parentId" type="string" required="false" default="" />
		<cfargument name="name" type="string" required="false" default="" />
		<cfargument name="isActive" type="string" required="false" default="" />
		<cfargument name="dtCreated" type="string" required="false" default="" />
		<cfargument name="dtUpdated" type="string" required="false" default="" />
		<cfargument name="createdBy" type="string" required="false" default="" />
		<cfargument name="updatedBy" type="string" required="false" default="" />
		<cfargument name="ipCreated" type="string" required="false" default="" />
		<cfargument name="ipUpdated" type="string" required="false" default="" />
		<cfargument name="orderNo" type="string" required="false" default="" />
		
		<!--- run setters --->
		<cfset settypeId(arguments.typeId) />
		<cfset setsiteId(arguments.siteId) />
		<cfset setparentId(arguments.parentId) />
		<cfset setname(arguments.name) />
		<cfset setisActive(arguments.isActive) />
		<cfset setdtCreated(arguments.dtCreated) />
		<cfset setdtUpdated(arguments.dtUpdated) />
		<cfset setcreatedBy(arguments.createdBy) />
		<cfset setupdatedBy(arguments.updatedBy) />
		<cfset setipCreated(arguments.ipCreated) />
		<cfset setipUpdated(arguments.ipUpdated) />
		<cfset setOrderNo(arguments.orderNo) />
		
		<cfreturn this />
 	</cffunction>

	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="setMemento" access="public" returntype="com.redactededucation.type.Type" output="false">
		<cfargument name="memento" type="struct" required="yes"/>
		<cfset variables.instance = arguments.memento />
		<cfreturn this />
	</cffunction>
	<cffunction name="getMemento" access="public" returntype="struct" output="false" >
		<cfreturn variables.instance />
	</cffunction>

	<cffunction name="validate" access="public" returntype="array" output="false">
		<cfset var errors = arrayNew(1) />
		<cfset var thisError = structNew() />
		
		<!--- typeId --->
		<cfif (NOT len(trim(gettypeId())))>
			<cfset thisError.field = "typeId" />
			<cfset thisError.type = "required" />
			<cfset thisError.message = "typeId is required" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(gettypeId())) AND NOT IsSimpleValue(trim(gettypeId())))>
			<cfset thisError.field = "typeId" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "typeId is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(gettypeId())) GT 0)>
			<cfset thisError.field = "typeId" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "typeId is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- siteId --->
		<cfif (len(trim(getsiteId())) AND NOT IsSimpleValue(trim(getsiteId())))>
			<cfset thisError.field = "siteId" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "siteId is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getsiteId())) GT 0)>
			<cfset thisError.field = "siteId" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "siteId is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- parentId --->
		<cfif (len(trim(getparentId())) AND NOT IsSimpleValue(trim(getparentId())))>
			<cfset thisError.field = "parentId" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "parentId is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getparentId())) GT 0)>
			<cfset thisError.field = "parentId" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "parentId is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- name --->
		<cfif (NOT len(trim(getname())))>
			<cfset thisError.field = "name" />
			<cfset thisError.type = "required" />
			<cfset thisError.message = "name is required" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getname())) AND NOT IsSimpleValue(trim(getname())))>
			<cfset thisError.field = "name" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "name is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getname())) GT 255)>
			<cfset thisError.field = "name" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "name is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- isActive --->
		<cfif (len(trim(getisActive())) AND NOT isBoolean(trim(getisActive())))>
			<cfset thisError.field = "isActive" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "isActive is not boolean" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- dtCreated --->
		<cfif (len(trim(getdtCreated())) AND NOT isDate(trim(getdtCreated())))>
			<cfset thisError.field = "dtCreated" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "dtCreated is not a date" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- dtUpdated --->
		<cfif (len(trim(getdtUpdated())) AND NOT isDate(trim(getdtUpdated())))>
			<cfset thisError.field = "dtUpdated" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "dtUpdated is not a date" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- createdBy --->
		<cfif (len(trim(getcreatedBy())) AND NOT IsSimpleValue(trim(getcreatedBy())))>
			<cfset thisError.field = "createdBy" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "createdBy is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getcreatedBy())) GT 0)>
			<cfset thisError.field = "createdBy" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "createdBy is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- updatedBy --->
		<cfif (len(trim(getupdatedBy())) AND NOT IsSimpleValue(trim(getupdatedBy())))>
			<cfset thisError.field = "updatedBy" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "updatedBy is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getupdatedBy())) GT 0)>
			<cfset thisError.field = "updatedBy" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "updatedBy is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- ipCreated --->
		<cfif (len(trim(getipCreated())) AND NOT IsSimpleValue(trim(getipCreated())))>
			<cfset thisError.field = "ipCreated" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "ipCreated is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getipCreated())) GT 50)>
			<cfset thisError.field = "ipCreated" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "ipCreated is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<!--- ipUpdated --->
		<cfif (len(trim(getipUpdated())) AND NOT IsSimpleValue(trim(getipUpdated())))>
			<cfset thisError.field = "ipUpdated" />
			<cfset thisError.type = "invalidType" />
			<cfset thisError.message = "ipUpdated is not a string" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		<cfif (len(trim(getipUpdated())) GT 50)>
			<cfset thisError.field = "ipUpdated" />
			<cfset thisError.type = "tooLong" />
			<cfset thisError.message = "ipUpdated is too long" />
			<cfset arrayAppend(errors,duplicate(thisError)) />
		</cfif>
		
		<cfreturn errors />
	</cffunction>

	<!---
	ACCESSORS
	--->
	<cffunction name="settypeId" access="public" returntype="void" output="false">
		<cfargument name="typeId" type="string" required="true" />
		<cfset variables.instance.typeId = arguments.typeId />
	</cffunction>
	<cffunction name="gettypeId" access="public" returntype="string" output="false">
		<cfreturn variables.instance.typeId />
	</cffunction>

	<cffunction name="setsiteId" access="public" returntype="void" output="false">
		<cfargument name="siteId" type="string" required="true" />
		<cfset variables.instance.siteId = arguments.siteId />
	</cffunction>
	<cffunction name="getsiteId" access="public" returntype="string" output="false">
		<cfreturn variables.instance.siteId />
	</cffunction>

	<cffunction name="setparentId" access="public" returntype="void" output="false">
		<cfargument name="parentId" type="string" required="true" />
		<cfset variables.instance.parentId = arguments.parentId />
	</cffunction>
	<cffunction name="getparentId" access="public" returntype="string" output="false">
		<cfreturn variables.instance.parentId />
	</cffunction>

	<cffunction name="setname" access="public" returntype="void" output="false">
		<cfargument name="name" type="string" required="true" />
		<cfset variables.instance.name = arguments.name />
	</cffunction>
	<cffunction name="getname" access="public" returntype="string" output="false">
		<cfreturn variables.instance.name />
	</cffunction>

	<cffunction name="setisActive" access="public" returntype="void" output="false">
		<cfargument name="isActive" type="string" required="true" />
		<cfset variables.instance.isActive = arguments.isActive />
	</cffunction>
	<cffunction name="getisActive" access="public" returntype="string" output="false">
		<cfreturn variables.instance.isActive />
	</cffunction>

	<cffunction name="setdtCreated" access="public" returntype="void" output="false">
		<cfargument name="dtCreated" type="string" required="true" />
		<cfset variables.instance.dtCreated = arguments.dtCreated />
	</cffunction>
	<cffunction name="getdtCreated" access="public" returntype="string" output="false">
		<cfreturn variables.instance.dtCreated />
	</cffunction>

	<cffunction name="setdtUpdated" access="public" returntype="void" output="false">
		<cfargument name="dtUpdated" type="string" required="true" />
		<cfset variables.instance.dtUpdated = arguments.dtUpdated />
	</cffunction>
	<cffunction name="getdtUpdated" access="public" returntype="string" output="false">
		<cfreturn variables.instance.dtUpdated />
	</cffunction>

	<cffunction name="setcreatedBy" access="public" returntype="void" output="false">
		<cfargument name="createdBy" type="string" required="true" />
		<cfset variables.instance.createdBy = arguments.createdBy />
	</cffunction>
	<cffunction name="getcreatedBy" access="public" returntype="string" output="false">
		<cfreturn variables.instance.createdBy />
	</cffunction>

	<cffunction name="setupdatedBy" access="public" returntype="void" output="false">
		<cfargument name="updatedBy" type="string" required="true" />
		<cfset variables.instance.updatedBy = arguments.updatedBy />
	</cffunction>
	<cffunction name="getupdatedBy" access="public" returntype="string" output="false">
		<cfreturn variables.instance.updatedBy />
	</cffunction>

	<cffunction name="setipCreated" access="public" returntype="void" output="false">
		<cfargument name="ipCreated" type="string" required="true" />
		<cfset variables.instance.ipCreated = arguments.ipCreated />
	</cffunction>
	<cffunction name="getipCreated" access="public" returntype="string" output="false">
		<cfreturn variables.instance.ipCreated />
	</cffunction>

	<cffunction name="setipUpdated" access="public" returntype="void" output="false">
		<cfargument name="ipUpdated" type="string" required="true" />
		<cfset variables.instance.ipUpdated = arguments.ipUpdated />
	</cffunction>
	<cffunction name="getipUpdated" access="public" returntype="string" output="false">
		<cfreturn variables.instance.ipUpdated />
	</cffunction>
	
	<cffunction name="setOrderNo" access="public" returntype="void" output="false">
		<cfargument name="orderNo" type="string" required="true" />
		<cfset variables.instance.orderNo = arguments.orderNo />
	</cffunction>
	<cffunction name="getOrderNo" access="public" returntype="string" output="false">
		<cfreturn variables.instance.orderNo />
	</cffunction>


	<!---
	DUMP
	--->
	<cffunction name="dump" access="public" output="true" return="void">
		<cfargument name="abort" type="boolean" default="false" />
		<cfdump var="#variables.instance#" />
		<cfif arguments.abort>
			<cfabort />
		</cfif>
	</cffunction>

</cfcomponent>
