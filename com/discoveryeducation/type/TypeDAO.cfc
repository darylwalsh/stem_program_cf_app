<cfcomponent displayname="TypeDAO" hint="table ID column = typeId">

	<cffunction name="init" access="public" output="false" returntype="com.redactededucation.type.TypeDAO">
		<cfargument name="dsn" type="string" required="true">
		<cfset variables.dsn = arguments.dsn>
		<cfreturn this>
	</cffunction>
	
	<cffunction name="create" access="public" output="false" returntype="boolean">
		<cfargument name="Type" type="com.redactededucation.type.Type" required="true" />

		<cfset var qCreate = "" />
		<cftry>
			<cfquery name="qCreate" datasource="#variables.dsn#">
				INSERT INTO cms_types
					(
					typeId,
					siteId,
					parentId,
					name,
					isActive,
					dtCreated,
					dtUpdated,
					createdBy,
					updatedBy,
					ipCreated,
					ipUpdated,
					orderNo
					)
				VALUES
					(
					<cfqueryparam value="#arguments.Type.gettypeId()#" CFSQLType="cf_sql_idstamp" />,
					<cfqueryparam value="#arguments.Type.getsiteId()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Type.getsiteId())#" />,
					<cfqueryparam value="#arguments.Type.getparentId()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Type.getparentId())#" />,
					<cfqueryparam value="#arguments.Type.getname()#" CFSQLType="cf_sql_varchar" />,
					<cfqueryparam value="#arguments.Type.getisActive()#" CFSQLType="cf_sql_bit" null="#not len(arguments.Type.getisActive())#" />,
					<cfqueryparam value="#arguments.Type.getdtCreated()#" CFSQLType="cf_sql_timestamp" null="#not len(arguments.Type.getdtCreated())#" />,
					<cfqueryparam value="#arguments.Type.getdtUpdated()#" CFSQLType="cf_sql_timestamp" null="#not len(arguments.Type.getdtUpdated())#" />,
					<cfqueryparam value="#arguments.Type.getcreatedBy()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Type.getcreatedBy())#" />,
					<cfqueryparam value="#arguments.Type.getupdatedBy()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Type.getupdatedBy())#" />,
					<cfqueryparam value="#arguments.Type.getipCreated()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Type.getipCreated())#" />,
					<cfqueryparam value="#arguments.Type.getipUpdated()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Type.getipUpdated())#" />,
					<cfqueryparam value="#arguments.Type.getOrderNo()#" CFSQLType="cf_sql_integer" null="#not len(arguments.Type.getOrderNo())#" />
					)
			</cfquery>
			<cfcatch type="database">
				<cfreturn false />
			</cfcatch>
		</cftry>
		<cfreturn true />
	</cffunction>

	<cffunction name="read" access="public" output="false" returntype="void">
		<cfargument name="Type" type="com.redactededucation.type.Type" required="true" />

		<cfset var qRead = "" />
		<cfset var strReturn = structNew() />
		<cftry>
			<cfquery name="qRead" datasource="#variables.dsn#">
				SELECT
					typeId,
					siteId,
					parentId,
					name,
					isActive,
					dtCreated,
					dtUpdated,
					createdBy,
					updatedBy,
					ipCreated,
					ipUpdated,
					orderNo
				FROM	cms_types
				WHERE	typeId = <cfqueryparam value="#arguments.Type.gettypeId()#" CFSQLType="cf_sql_idstamp" />
			</cfquery>
			<cfcatch type="database">
				<!--- leave the bean as is and set an empty query for the conditional logic below --->
				<cfset qRead = queryNew("id") />
			</cfcatch>
		</cftry>
		<cfif qRead.recordCount>
			<cfset strReturn = queryRowToStruct(qRead)>
			<cfset arguments.Type.init(argumentCollection=strReturn)>
		</cfif>
	</cffunction>

	<cffunction name="update" access="public" output="false" returntype="boolean">
		<cfargument name="Type" type="com.redactededucation.type.Type" required="true" />

		<cfset var qUpdate = "" />
		<cftry>
			<cfquery name="qUpdate" datasource="#variables.dsn#">
				UPDATE	cms_types
				SET
					siteId = <cfqueryparam value="#arguments.Type.getsiteId()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Type.getsiteId())#" />,
					parentId = <cfqueryparam value="#arguments.Type.getparentId()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Type.getparentId())#" />,
					name = <cfqueryparam value="#arguments.Type.getname()#" CFSQLType="cf_sql_varchar" />,
					isActive = <cfqueryparam value="#arguments.Type.getisActive()#" CFSQLType="cf_sql_bit" null="#not len(arguments.Type.getisActive())#" />,
					dtCreated = <cfqueryparam value="#arguments.Type.getdtCreated()#" CFSQLType="cf_sql_timestamp" null="#not len(arguments.Type.getdtCreated())#" />,
					dtUpdated = <cfqueryparam value="#arguments.Type.getdtUpdated()#" CFSQLType="cf_sql_timestamp" null="#not len(arguments.Type.getdtUpdated())#" />,
					createdBy = <cfqueryparam value="#arguments.Type.getcreatedBy()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Type.getcreatedBy())#" />,
					updatedBy = <cfqueryparam value="#arguments.Type.getupdatedBy()#" CFSQLType="cf_sql_idstamp" null="#not len(arguments.Type.getupdatedBy())#" />,
					ipCreated = <cfqueryparam value="#arguments.Type.getipCreated()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Type.getipCreated())#" />,
					ipUpdated = <cfqueryparam value="#arguments.Type.getipUpdated()#" CFSQLType="cf_sql_varchar" null="#not len(arguments.Type.getipUpdated())#" />,
					orderNo = <cfqueryparam value="#arguments.Type.getOrderNo()#" CFSQLType="cf_sql_integer" null="#not len(arguments.Type.getOrderNo())#" />
				WHERE	typeId = <cfqueryparam value="#arguments.Type.gettypeId()#" CFSQLType="cf_sql_idstamp" />
			</cfquery>
			<cfcatch type="database">
				<cfreturn false />
			</cfcatch>
		</cftry>
		<cfreturn true />
	</cffunction>

	<cffunction name="delete" access="public" output="false" returntype="boolean">
		<cfargument name="Type" type="com.redactededucation.type.Type" required="true" />

		<cfset var qDelete = "">
		<cftry>
			<cfquery name="qDelete" datasource="#variables.dsn#">
				DELETE FROM	cms_types 
				WHERE	typeId = <cfqueryparam value="#arguments.Type.gettypeId()#" CFSQLType="cf_sql_idstamp" />
			</cfquery>
			<cfcatch type="database">
				<cfreturn false />
			</cfcatch>
		</cftry>
		<cfreturn true />
	</cffunction>

	<cffunction name="exists" access="public" output="false" returntype="boolean">
		<cfargument name="Type" type="com.redactededucation.type.Type" required="true" />

		<cfset var qExists = "">
		<cfquery name="qExists" datasource="#variables.dsn#" maxrows="1">
			SELECT count(1) as idexists
			FROM	cms_types
			WHERE	typeId = <cfqueryparam value="#arguments.Type.gettypeId()#" CFSQLType="cf_sql_idstamp" />
		</cfquery>

		<cfif qExists.idexists>
			<cfreturn true />
		<cfelse>
			<cfreturn false />
		</cfif>
	</cffunction>

	<cffunction name="save" access="public" output="false" returntype="boolean">
		<cfargument name="Type" type="com.redactededucation.type.Type" required="true" />
		
		<cfset var success = false />
		<cfif exists(arguments.Type)>
			<cfset success = update(arguments.Type) />
		<cfelse>
			<cfset success = create(arguments.Type) />
		</cfif>
		
		<cfreturn success />
	</cffunction>

	<cffunction name="queryRowToStruct" access="private" output="false" returntype="struct">
		<cfargument name="qry" type="query" required="true">
		
		<cfscript>
			/**
			 * Makes a row of a query into a structure.
			 * 
			 * @param query 	 The query to work with. 
			 * @param row 	 Row number to check. Defaults to row 1. 
			 * @return Returns a structure. 
			 * @author Nathan Dintenfass (nathan@changemedia.com) 
			 * @version 1, December 11, 2001 
			 */
			//by default, do this to the first row of the query
			var row = 1;
			//a var for looping
			var ii = 1;
			//the cols to loop over
			var cols = listToArray(qry.columnList);
			//the struct to return
			var stReturn = structnew();
			//if there is a second argument, use that for the row number
			if(arrayLen(arguments) GT 1)
				row = arguments[2];
			//loop over the cols and build the struct from the query row
			for(ii = 1; ii lte arraylen(cols); ii = ii + 1){
				stReturn[cols[ii]] = qry[cols[ii]][row];
			}		
			//return the struct
			return stReturn;
		</cfscript>
	</cffunction>

</cfcomponent>
