<cfcomponent name="TypeService" output="false">

	<cffunction name="init" access="public" output="false" returntype="com.redactededucation.type.TypeService">
		<cfargument name="TypeDAO" type="com.redactededucation.type.TypeDAO" required="true" />
		<cfargument name="TypeGateway" type="com.redactededucation.type.TypeGateway" required="true" />

		<cfset variables.TypeDAO = arguments.TypeDAO />
		<cfset variables.TypeGateway = arguments.TypeGateway />

		<cfreturn this/>
	</cffunction>

	<cffunction name="createType" access="public" output="false" returntype="com.redactededucation.type.Type">
		<cfargument name="typeId" type="string" required="true" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="parentId" type="string" required="false" />
		<cfargument name="name" type="string" required="false" />
		<cfargument name="isActive" type="boolean" required="false" />
		<cfargument name="dtCreated" type="date" required="false" />
		<cfargument name="dtUpdated" type="date" required="false" />
		<cfargument name="createdBy" type="string" required="false" />
		<cfargument name="updatedBy" type="string" required="false" />
		<cfargument name="ipCreated" type="string" required="false" />
		<cfargument name="ipUpdated" type="string" required="false" />
		<cfargument name="orderNo" type="string" required="false" />
		
			
		<cfset var Type = createObject("component","com.redactededucation.type.Type").init(argumentCollection=arguments) />
		<cfreturn Type />
	</cffunction>

	<cffunction name="getType" access="public" output="false" returntype="Type">
		<cfargument name="typeId" type="string" required="true" />
		
		<cfset var Type = createType(argumentCollection=arguments) />
		<cfset variables.TypeDAO.read(Type) />
		<cfreturn Type />
	</cffunction>

	<cffunction name="getTypes" access="public" output="false" returntype="array">
		<cfargument name="typeId" type="string" required="false" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="parentId" type="string" required="false" />
		<cfargument name="name" type="string" required="false" />
		<cfargument name="isActive" type="boolean" required="false" />
		<cfargument name="dtCreated" type="date" required="false" />
		<cfargument name="dtUpdated" type="date" required="false" />
		<cfargument name="createdBy" type="string" required="false" />
		<cfargument name="updatedBy" type="string" required="false" />
		<cfargument name="ipCreated" type="string" required="false" />
		<cfargument name="ipUpdated" type="string" required="false" />
		<cfargument name="orderNo" type="numeric" required="false" />
		
		<cfreturn variables.TypeGateway.getByAttributes(argumentCollection=arguments) />
	</cffunction>
	
	<cffunction name="getTypesQuery" access="public" output="false" returntype="query">
		<cfargument name="typeId" type="string" required="false" />
		<cfargument name="siteId" type="string" required="false" />
		<cfargument name="parentId" type="string" required="false" />
		<cfargument name="name" type="string" required="false" />
		<cfargument name="isActive" type="boolean" required="false" />
		<cfargument name="dtCreated" type="date" required="false" />
		<cfargument name="dtUpdated" type="date" required="false" />
		<cfargument name="createdBy" type="string" required="false" />
		<cfargument name="updatedBy" type="string" required="false" />
		<cfargument name="ipCreated" type="string" required="false" />
		<cfargument name="ipUpdated" type="string" required="false" />
		<cfargument name="orderNo" type="numeric" required="false" />
		
		<cfreturn variables.TypeGateway.getByAttributesQuery(argumentCollection=arguments) />
	</cffunction>
	
	<cffunction name="getTypesByResourceId" access="public" output="false" returntype="any">
		<cfargument name="resourceId" type="string" required="true" />
		
		<cfreturn variables.TypeGateway.getByResourceId(resourceId=arguments.resourceId) />
	</cffunction>

	<cffunction name="saveType" access="public" output="false" returntype="boolean">
		<cfargument name="Type" type="com.redactededucation.type.Type" required="true" />

		<cfreturn variables.TypeDAO.save(Type) />
	</cffunction>

	<cffunction name="deleteType" access="public" output="false" returntype="boolean">
		<cfargument name="typeId" type="string" required="true" />
		
		<cfset var Type = createType(argumentCollection=arguments) />
		<cfreturn variables.TypeDAO.delete(Type) />
	</cffunction>
</cfcomponent>
