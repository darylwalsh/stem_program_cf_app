<cfcomponent
	displayname="UserListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="User Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setUserService" access="public" returntype="void" output="false">
		<cfargument name="UserService" type="com.redactededucation.user.UserService" required="true" />
		<cfset variables.UserService = arguments.UserService />
	</cffunction>
	<cffunction name="getUserService" access="public" returntype="com.redactededucation.user.UserService" output="false">
		<cfreturn variables.UserService />
	</cffunction>
	
	<cffunction name="setGroupUserService" access="public" returntype="void" output="false">
		<cfargument name="GroupUserService" type="com.redactededucation.groupUser.GroupUserService" required="true" />
		<cfset variables.GroupUserService = arguments.GroupUserService />
	</cffunction>
	<cffunction name="getGroupUserService" access="public" returntype="com.redactededucation.groupUser.GroupUserService" output="false">
		<cfreturn variables.GroupUserService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="getUsersByGroupId" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var groupID = arguments.event.getArg("groupId") />
		
		<cfreturn getUserService().getUsersByGroupId(groupId=groupId) />
	</cffunction>
	
	<cffunction name="getUsersBySiteId" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var siteId = arguments.event.getArg("siteId") />
		
		<cfreturn getUserService().getUsersBySiteId(siteId=siteId) />
	</cffunction>
	
	<cffunction name="getUserByUsername" access="public" returntype="string">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var uid = arguments.event.getArg("uid") />
		
		<cfreturn getUserService().getUserByUsername(uid=uid) />
	</cffunction>
	
	<cffunction name="addGroupUser" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var app_user_guid = arguments.event.getArg("app_user_guid") />
		<cfset var groupId = arguments.event.getArg("groupId") />
		<cfset var groupUser = getGroupUserService().createGroupUser(groupId=groupId,app_user_guid=app_user_guid) />
		
		<cfreturn getGroupUserService().saveGroupUser(groupUser) />
	</cffunction>
	
	<cffunction name="deleteGroupUser" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var app_user_guid = arguments.event.getArg("app_user_guid") />
		<cfset var groupId = arguments.event.getArg("groupId") />
		
		<cfreturn getGroupUserService().deleteGroupUser(groupId=groupId,app_user_guid=app_user_guid) />
	</cffunction>

</cfcomponent>