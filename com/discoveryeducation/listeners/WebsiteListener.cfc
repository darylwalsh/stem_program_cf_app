<cfcomponent
	displayname="WebsiteListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Website Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setWebsiteService" access="public" returntype="void" output="false">
		<cfargument name="WebsiteService" type="com.redactededucation.website.WebsiteService" required="true" />
		<cfset variables.WebsiteService = arguments.WebsiteService />
	</cffunction>
	<cffunction name="getWebsiteService" access="public" returntype="com.redactededucation.website.WebsiteService" output="false">
		<cfreturn variables.WebsiteService />
	</cffunction>
	
	<cffunction name="setWebsiteUserService" access="public" returntype="void" output="false">
		<cfargument name="WebsiteUserService" type="com.redactededucation.websiteUser.WebsiteUserService" required="true" />
		<cfset variables.WebsiteUserService = arguments.WebsiteUserService />
	</cffunction>
	<cffunction name="getWebsiteUserService" access="public" returntype="com.redactededucation.websiteUser.WebsiteUserService" output="false">
		<cfreturn variables.WebsiteUserService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="getWebsitesByUserId" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var app_user_guid = cookie.app_user_guid />
		
		<cfreturn getWebsiteService().getWebsitesByUserId(app_user_guid=app_user_guid) />
	</cffunction>
	<cffunction name="getWebsiteById" access="public" returntype="com.redactededucation.website.Website" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var siteId = arguments.event.getArg("siteId") />
		
		<cfreturn getWebsiteService().getWebsite(siteId=siteId) />
	</cffunction>
	<cffunction name="saveWebsite" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var website = arguments.event.getArg("website") />
		
		<cfreturn getWebsiteService().saveWebsite(website) />
	</cffunction>
	<cffunction name="saveWebsiteUser" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var siteId = arguments.event.getArg("siteId") />
		<cfset var app_user_guid = arguments.event.getArg("app_user_guid") />
		<cfset var websiteUser = getWebsiteUserService().createWebsiteUser(siteId,app_user_guid) />

		<cfif websiteUser.getApp_user_guid() EQ "">
			<cfreturn false />
		</cfif>
		
		<cfreturn getWebsiteUserService().saveWebsiteUser(websiteUser) />
	</cffunction>
	
	<cffunction name="deleteWebsiteUser" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var siteId = arguments.event.getArg("siteId") />
		<cfset var app_user_guid = arguments.event.getArg("app_user_guid") />
		
		<cfreturn getWebsiteUserService().deleteWebsiteUser(siteId,app_user_guid) />
	</cffunction>
	<cffunction name="getWebsites" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
        <cfscript>
			var sites = ArrayNew(1);
        	var temp=StructNew();
			temp.url="http://www.iteea.org/Resources/tewebsites.htm";
        	temp.title="International Technology and Engineering Educators Association Teacher Resources";
			temp.blurb="";
			ArrayAppend(sites, temp);
        	temp=StructNew();
			temp.url="http://www.mn-stem.com/";
        	temp.title="Minnesota STEM Initiative";
			temp.blurb="";
			ArrayAppend(sites, temp);
        	temp=StructNew();
			temp.url="http://www.nsf.gov/news/classroom/";
        	temp.title="National Science Foundation Classroom Resources";
			temp.blurb="";
			ArrayAppend(sites, temp);
        	temp=StructNew();
			temp.url="http://www.nsf.gov/news/classroom/engineering.jsp";
        	temp.title="National Science Foundation Engineering Resources";
			temp.blurb="";
			ArrayAppend(sites, temp);			
			temp=StructNew();
			temp.url="http://www.nsta.org";
        	temp.title="National Science Teachers Associate (NSTA)";
			temp.blurb="";
			ArrayAppend(sites, temp);			
        	temp=StructNew();
			temp.url="http://www.pasteminitiative.org/";
        	temp.title="Pennsylvania STEM Initiative";
			temp.blurb="";
			ArrayAppend(sites, temp);
        	temp=StructNew();
			temp.url="http://www.redactededucation.com/stemconnect/";
        	temp.title="STEM Connect";
			temp.blurb="";
			ArrayAppend(sites, temp);
        	temp=StructNew();
			temp.url="http://stemeduc.blogspot.com/";
        	temp.title="STEM Education Blog";
			temp.blurb="";
			ArrayAppend(sites, temp);
        	temp=StructNew();
			temp.url="http://www.teachscienceandmath.com/";
        	temp.title="Teach Science and Math";
			temp.blurb="";
			ArrayAppend(sites, temp);
        	temp=StructNew();
			temp.url="http://www.epa.gov/teachers/";
        	temp.title="U.S. EPA Educational Resources for Teachers and Students";
			temp.blurb="";
			ArrayAppend(sites, temp);
			return sites;
        </cfscript>
	</cffunction>
</cfcomponent>