<cfcomponent
	displayname="ContentRatingListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="ContentRating Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setContentRatingService" access="public" returntype="void" output="false">
		<cfargument name="ContentRatingService" type="com.redactededucation.ContentRating.ContentRatingService" required="true" />
		<cfset variables.ContentRatingService = arguments.ContentRatingService />
	</cffunction>
	<cffunction name="getContentRatingService" access="public" returntype="com.redactededucation.ContentRating.ContentRatingService" output="false">
		<cfreturn variables.ContentRatingService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="saveContentRating" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var app_user_guid = arguments.event.getArg("app_user_guid") />
		<cfset var rating = arguments.event.getArg("rated") />
		<cfset var notes = arguments.event.getArg("notes") />
		<cfset var contentRating = getContentRatingService().createContentRating(guidAssetId=guidAssetId,app_user_guid=app_user_guid,rating=rating,notes=notes) />
		
		<cfset contentRating.setDt_created(CreateODBCDate(Now())) />
		
		<cfreturn getContentRatingService().saveContentRating(contentRating) />
	</cffunction>
	
	<cffunction name="removeContentRating" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var app_user_guid = arguments.event.getArg("app_user_guid") />
		
		<cfreturn getContentRatingService().deleteContentRating(guidAssetId=guidAssetId,app_user_guid=app_user_guid) />
	</cffunction>
	
	<cffunction name="getContentRating" access="public" returntype="com.redactededucation.contentRating.ContentRating" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var app_user_guid = "00000000-0000-0000-0000-000000000000" />
		
		<cfif structKeyExists(cookie,"app_user_guid")>
			<cfset app_user_guid = cookie.app_user_guid />
		</cfif>
		
		<cfreturn getContentRatingService().getContentRating(guidAssetId=guidAssetId,app_user_guid=app_user_guid) />
	</cffunction>
	
	<cffunction name="getAverageContentRating" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		
		<cfif NOT IsValid("guid",guidAssetId)>
			<cfset guidAssetId = "00000000-0000-0000-0000-000000000000" />
		</cfif>
		
		<cfreturn getContentRatingService().getAverageContentRating(guidAssetId=guidAssetId) />
	</cffunction>

</cfcomponent>