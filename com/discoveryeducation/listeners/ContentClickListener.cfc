<cfcomponent
	displayname="ContentClickListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="ContentClick Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setContentClickService" access="public" returntype="void" output="false">
		<cfargument name="ContentClickService" type="com.redactededucation.ContentClick.ContentClickService" required="true" />
		<cfset variables.ContentClickService = arguments.ContentClickService />
	</cffunction>
	<cffunction name="getContentClickService" access="public" returntype="com.redactededucation.ContentClick.ContentClickService" output="false">
		<cfreturn variables.ContentClickService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="createContentClick" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentClick = getContentClickService().createContentClick(guidAssetId=guidAssetId) />
		
		<cfreturn getContentClickService().saveContentClick(contentClick) />
	</cffunction>

</cfcomponent>