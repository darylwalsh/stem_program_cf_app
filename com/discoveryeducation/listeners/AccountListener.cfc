<cfcomponent
	displayname="AccountListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Account Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setAccountService" access="public" returntype="void" output="false">
		<cfargument name="AccountService" type="com.redactededucation.account.AccountService" required="true" />
		<cfset variables.AccountService = arguments.AccountService />
	</cffunction>
	<cffunction name="getAccountService" access="public" returntype="com.redactededucation.account.AccountService" output="false">
		<cfreturn variables.AccountService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="getAllAccounts" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "name ASC" /> 
		
		<cfreturn getAccountService().getAccounts(orderyBy=orderBy) />
	</cffunction>

</cfcomponent>