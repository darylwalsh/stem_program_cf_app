<cfcomponent
	displayname="GroupUserListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="GroupUser Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setGroupUserService" access="public" returntype="void" output="false">
		<cfargument name="GroupUserService" type="com.redactededucation.groupUser.GroupUserService" required="true" />
		<cfset variables.GroupUserService = arguments.GroupUserService />
	</cffunction>
	<cffunction name="getGroupUserService" access="public" returntype="com.redactededucation.groupUser.GroupUserService" output="false">
		<cfreturn variables.GroupUserService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="getGroupUsersByGroupId" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var groupId = arguments.event.getArg("groupId") />
		
		<cfreturn getGroupService().getGroups(siteId=siteId,orderBy=orderBy) />
	</cffunction>
</cfcomponent>