<cfcomponent
	displayname="ContentFileListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="ContentFile Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setContentFileService" access="public" returntype="void" output="false">
		<cfargument name="contentFileService" type="com.redactededucation.contentFile.ContentFileService" required="true" />
		<cfset variables.contentFileService = arguments.contentFileService />
	</cffunction>
	<cffunction name="getContentFileService" access="public" returntype="com.redactededucation.contentFile.ContentFileService" output="false">
		<cfreturn variables.contentFileService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="processUploadForm" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var contentFile = arguments.event.getArg("contentFile") />
		<cfset var fileExt = "" />
		
		<cfif arguments.event.getArg("fileData") NEQ "">
			<cftry>
				<cffile action="upload" filefield="fileData" destination="#ExpandPath('/assets/sales/')#"
						nameconflict="makeunique" />
				<cfif CFFILE.FileWasSaved>
					<cfset contentFile.setGuidAssetMediaFileId(request.udf.CreateGUID()) />
					<cfset contentFile.setStrFilePath("/assets/sales/") />
					<cfset contentFile.setStrFileName(CFFILE.ServerFile) />
					<cfset contentFile.setIntFileSizeBytes(CFFILE.FileSize) />
					<cfset contentFile.setMediaFileServerUrl(request.baseUrl) />
					<cfset contentFile.setThumbnail(request.udf.createThumbnail("#CFFILE.ServerDirectory#\#CFFILE.ServerFile#",contentFile.getGuidAssetMediaFileId())) />
				</cfif>
				<cfcatch type="application">
					<cfthrow message="#CFCATCH.Message#" detail="#CFCATCH.Detail#" extendedinfo="#CFCATCH.ExtendedInfo#" />
				</cfcatch>
			</cftry>
		</cfif>
			<cfdocument format="PDF" overwrite="yes" filename="c:/errordebug.pdf">
				<cfdump var="#contentFile.getMemento()#"/>
			</cfdocument>
		
		<cfreturn getContentFileService().saveContentFile(contentFile) />
	</cffunction>
	
	<cffunction name="getContentFilesByContentId" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var getContentFilesByContentId = "" />
		
		<cfreturn getContentFileService().getContentFiles(guidAssetId=guidAssetId) />
	</cffunction>
	
	<cffunction name="deleteContentFile" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetMediaFileId = arguments.event.getArg("guidAssetMediaFileId") />
		
		<cfreturn getContentFileService().deleteContentFile(guidAssetMediaFileId=guidAssetMediaFileId) />
	</cffunction>
	
	<cffunction name="createThumbnail" access="public" output="false" returntype="string">
		<cfargument name="event" type="MachII.framework.Event" required="true" />

		<cfset var guidAssetMediaFileId = event.getArg("guidAssetMediaFileId") />
		<cfset var contentFile = getContentFileService().getContentFile(guidAssetMediaFileId) />
		<cfset var assetDirectory = "#GetDirectoryFromPath(ExpandPath(contentFile.getStrFilePath()))#" />
		<cfset var inputFile = "#assetDirectory##contentFile.getStrFileName()#" />
		<cfset var outputFile = "#contentFile.getGuidAssetId()#" />
		<cfset var imagePath = "#contentFile.getStrFilePath()##contentFile.getGuidAssetId()#.jpg" />
			
		<cfset contentFile.setThumbnail(request.udf.createThumbnail(inputFile,outputFile)) />
				
		<cfset getContentFileService().saveContentFile(contentFile) />
		
		<cfreturn imagePath />
	</cffunction>

</cfcomponent>