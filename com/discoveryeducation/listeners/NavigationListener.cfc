<cfcomponent
	displayname="NavigationListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Navigation Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setNavigationService" access="public" returntype="void" output="false">
		<cfargument name="NavigationService" type="com.redactededucation.navigation.NavigationService" required="true" />
		<cfset variables.NavigationService = arguments.NavigationService />
	</cffunction>
	<cffunction name="getNavigationService" access="public" returntype="com.redactededucation.navigation.NavigationService" output="false">
		<cfreturn variables.NavigationService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="getNavigations" output="false" access="public" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidParentId = arguments.event.getArg("guidTaxId") />
		<cfset var orderBy = "intOrder" />
		
		<cfreturn getNavigationService().getNavigations(guidParentId=guidParentId,orderBy=orderBy) />
	</cffunction>
	
	<cffunction name="getRootNavigations" output="false" access="public" returntype="query"
		hint="I return top level taxonomies">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "name" />
		
		<cfreturn getNavigationService().getNavigations(intLevel=0,orderBy=orderBy) />
	</cffunction>
	
	<cffunction name="getAllNavigations" output="false" access="public" returntype="query"
		hint="I return full navigations">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidParentId = request.siteId />
		
		<cfreturn getNavigationService().getAllNavigations(guidParentId=guidParentId) />
	</cffunction>
	
	<cffunction name="getNavigationTree" output="false" access="public" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		
		<cfreturn getNavigationService().getNavigationTree(guidTaxId=guidTaxId) /> 
	</cffunction>
	
	<cffunction name="getChildNavigations" access="public" output="false" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidParentId = arguments.event.getArg("guidTaxId") />
		<cfset var orderBy = "intOrder" />
		
		<cfreturn getNavigationService().getNavigations(guidParentId=guidParentId,orderBy=orderBy) />
	</cffunction>
	
	<cffunction name="getNavigation" access="public" output="false" returntype="com.redactededucation.navigation.Navigation">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidParentId") />
		
		<cfreturn getNavigationService().getNavigation(guidTaxId=guidTaxId) />
	</cffunction>
	
	<cffunction name="updateNavigation" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var navigation = arguments.event.getArg("navigation") />
		
		<cfreturn getNavigationService().saveNavigation(navigation) />
	</cffunction>
	
	<cffunction name="getNavigationById" access="public" output="false" returntype="com.redactededucation.navigation.Navigation">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		
		<cfreturn getNavigationService().getNavigation(guidTaxId=guidTaxId) />
	</cffunction>
	
	<cffunction name="createRootNavigation" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var navigation = arguments.event.getArg("navigation") />
		
		<cfset navigation.setGuidTaxId(request.udf.CreateGUID()) />
		
		<cfreturn getNavigationService().saveNavigation(navigation) />
	</cffunction>

	<cffunction name="createNavigation" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var navigation = arguments.event.getArg("navigation") />
		
		<cfset navigation.setGuidTaxId(request.udf.CreateGUID()) />
		<cfset navigation.setIntOrder(getNavigationService().getMaxOrder(navigation.getGuidParentId()) + 1) />
		<cfset arguments.event.setArg("guidTaxid",navigation.getGuidParentId()) />
		
		<cfreturn getNavigationService().saveNavigation(navigation) />
	</cffunction>
	
	<cffunction name="deleteNavigation" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var guidParentId = arguments.event.getArg("guidParentId") />
		
		<cfset arguments.event.setArg("guidTaxId", guidParentId) />
		
		<cfreturn getNavigationService().deleteNavigation(guidTaxId=guidTaxId) />
	</cffunction>
	
	<cffunction name="getMaxOrder" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var navigation = arguments.event.getArg("navigation") />
		
		<cfreturn getNavigationService().getMaxOrder(navigation) />
	</cffunction>
	
	<cffunction name="moveOrderUp" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var navigation = arguments.event.getArg("navigation") />
		<cfset var guidParentId = arguments.event.getArg("guidParentId") />
		
		<cfset arguments.event.setArg("guidTaxId", guidParentId) />
		
		<cfreturn getNavigationService().moveOrderUp(navigation) />
	</cffunction>
	
	<cffunction name="moveOrderDown" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var navigation = arguments.event.getArg("navigation") />
		<cfset var guidParentId = arguments.event.getArg("guidParentId") />
		
		<cfset arguments.event.setArg("guidTaxId", guidParentId) />
		
		<cfreturn getNavigationService().moveOrderDown(navigation) />
	</cffunction>
	
	<cffunction name="getNavigationsByContentId" access="public" output="false" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		
		<cfreturn getNavigationService().getNavigationsByContentId(guidAssetId) />
	</cffunction>

</cfcomponent>