<cfcomponent
	displayname="MediaFileListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="MediaFile Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="processImageUploadForm" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var success = true />
		
		<cfif arguments.event.getArg("fileData") NEQ "">
			<cftry>
				<cffile action="upload" filefield="fileData" destination="#ExpandPath('/assets/support/img/')#"
						nameconflict="makeunique" />
				<cfcatch type="any">
					<cfset success = false />
					<cfthrow type="application" message="Error!" detail="#CFCATCH.Detail#" />
				</cfcatch>
			</cftry>
		</cfif>
		
		<cfreturn success />
	</cffunction>
</cfcomponent>