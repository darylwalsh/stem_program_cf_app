<cfcomponent displayname="SearchListener" output="false" extends="MachII.framework.Listener" hint="Search Listener for Help">
	
	<cffunction name="configure" access="public" output="false" returntype="void">
		<!--- do nothing for now --->
	</cffunction>
	
	<!--- dependencies --->
	<cffunction name="setSearchService" access="public" output="false" returntype="void">
		<cfargument name="SearchService" type="com.redactededucation.Search.SearchService" required="true" />
		<cfset variables.SearchService = arguments.SearchService />
	</cffunction>
	<cffunction name="getSearchService" access="public" output="false" returntype="com.redactededucation.Search.SearchService">
		<cfreturn variables.SearchService />
	</cffunction>
	
	<!--- listener methods --->
	<cffunction name="processSearchForm" access="public" output="false" returntype="query" hint="Validates a login attempt">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var searchResults = "" />
		<cfset var collname = event.getArg("collname") />
		<cfset var criteria = event.getArg("criteria") />
		<cfset var dtCreated = CreateODBCDateTime(Now()) />
		<cfset var search = getSearchService().createSearch(terms=criteria,dtCreated=dtCreated) />

		<cfset getSearchService().saveSearch(search) />
	
		<cfsearch
			type="internet"
			collection="#collname#"
			name="searchResults"
		   	criteria="#criteria#"
			maxrows = "100" />

		<cfreturn searchResults />
	</cffunction>
</cfcomponent>