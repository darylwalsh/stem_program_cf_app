<cfcomponent
	displayname="ContentListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Content Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setContentService" access="public" returntype="void" output="false">
		<cfargument name="contentService" type="com.redactededucation.content.ContentService" required="true" />
		<cfset variables.contentService = arguments.contentService />
	</cffunction>
	<cffunction name="getContentService" access="public" returntype="com.redactededucation.content.ContentService" output="false">
		<cfreturn variables.contentService />
	</cffunction>
	
	<cffunction name="setNavigationContentService" access="public" returntype="void" output="false">
		<cfargument name="navigationContentService" type="com.redactededucation.navigationContent.navigationContentService" required="true" />
		<cfset variables.navigationContentService = arguments.navigationContentService />
	</cffunction>
	<cffunction name="getNavigationContentService" access="public" returntype="com.redactededucation.navigationContent.navigationContentService" output="false">
		<cfreturn variables.navigationContentService />
	</cffunction>
	
	<cffunction name="setContentFeaturedService" access="public" returntype="void" output="false">
		<cfargument name="ContentFeaturedService" type="com.redactededucation.ContentFeatured.ContentFeaturedService" required="true" />
		<cfset variables.ContentFeaturedService = arguments.ContentFeaturedService />
	</cffunction>
	<cffunction name="getContentFeaturedService" access="public" returntype="com.redactededucation.ContentFeatured.ContentFeaturedService" output="false">
		<cfreturn variables.ContentFeaturedService />
	</cffunction>
	
	<cffunction name="setContentRoleService" access="public" returntype="void" output="false">
		<cfargument name="contentRoleService" type="com.redactededucation.contentRole.ContentRoleService" required="true" />
		<cfset variables.contentRoleService = arguments.contentRoleService />
	</cffunction>
	<cffunction name="getContentRoleService" access="public" returntype="com.redactededucation.contentRole.ContentRoleService" output="false">
		<cfreturn variables.contentRoleService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="processCreateAssetForm" access="public" returntype="void" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var content = arguments.event.getArg("content") />
		<cfset var navigationContent = "" />
		<cfset var intOrder = 0 />
		
		<cfset content.setGuidAssetMetaDataId(request.udf.CreateGUID()) />
		<cfset getContentService().saveContent(content) />
		<cflog file="test" text="#content.getGuidTaxIds()#" />
		<cfloop index="guidTaxId" list="#content.getGuidTaxIds()#">
			<cfset intOrder = getNavigationContentService().getMaxOrder(guidTaxId) + 1 />
			<cfset navigationContent = getNavigationContentService().createNavigationContent(guidTaxId=guidTaxId,guidAssetId=content.getGuidAssetMetaDataId(),intOrder=intOrder) />
			<cfset getNavigationContentService().saveNavigationContent(navigationContent) />
		</cfloop>
		<cfloop index="roleId" list="#content.getRoleIds()#">
			<cfset contentRole = getContentRoleService().createContentRole(roleId=roleId,contentId=content.getGuidAssetMetaDataId()) />
			<cfset getContentRoleService().saveContentRole(contentRole) />
		</cfloop>
		
		<cfset arguments.event.setArg("guidAssetId",content.getGuidAssetMetaDataId()) />
	</cffunction>
	
	<cffunction name="processEditAssetForm" access="public" returntype="void" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var content = arguments.event.getArg("content") />
		<cfset var navigationContent = "" />
		
		<cfset getContentService().saveContent(content) />
		
		<cfset getNavigationContentService().deleteNavigationContents(guidAssetId=content.getGuidAssetMetaDataId()) />
		<cfloop index="guidTaxId" list="#content.getGuidTaxIds()#">
			<cfset navigationContent = getNavigationContentService().createNavigationContent(guidTaxId=guidTaxId,guidAssetId=content.getGuidAssetMetaDataId()) />
			<cfset getNavigationContentService().saveNavigationContent(navigationContent) />
		</cfloop>
		
		<cfset getContentRoleService().deleteContentRoles(contentId=content.getGuidAssetMetaDataId()) />
		<cfloop index="roleId" list="#content.getRoleIds()#">
			<cfset contentRole = getContentRoleService().createContentRole(roleId=roleId,contentId=content.getGuidAssetMetaDataId()) />
			<cfset getContentRoleService().saveContentRole(contentRole) />
		</cfloop>
	</cffunction>
	
	<cffunction name="getPublishedContentByGuidTaxId" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var status = "8" />
		
		<cfreturn getContentService().getContentByGuidTaxId(guidTaxId=guidTaxId,status=status) />
	</cffunction>
	
	<cffunction name="getAllContentsByGuidTaxId" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var status = "7,8" />
		
		<cfreturn getContentService().getContentByGuidTaxId(guidTaxId=guidTaxId,status=status) />
	</cffunction>
	
	<cffunction name="getContentBySiteId" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var siteId = request.siteId />
		
		<cfreturn getContentService().getContentBySiteId(siteId) />
	</cffunction>
	
	<cffunction name="getFeaturedContent" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfreturn getContentService().getFeaturedContent() />
	</cffunction>
	
	<cffunction name="getContentById" access="public" returntype="com.redactededucation.content.Content" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		
		<cfreturn getContentService().getContent(guidAssetId) />
	</cffunction>

	<cffunction name="deleteContent" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		
		<cfreturn getContentService().deleteContent(guidAssetMetaDataId=guidAssetId) />
	</cffunction>

	<cffunction name="createContentFeatured" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentFeatured = getContentFeaturedService().getContentFeatured(guidAssetId=guidAssetId) />
		<cfset var intOrder = contentFeatured.getIntOrder() />

		<cfif intOrder EQ "">
			<cfset contentFeatured.setIntOrder(getContentFeaturedService().getLast() + 1) />
		</cfif>
		
		<cfreturn getContentFeaturedService().saveContentFeatured(contentFeatured) />
	</cffunction>
	
	<cffunction name="deleteContentFeatured" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />

		<cfreturn getContentFeaturedService().deleteContentFeatured(guidAssetId=guidAssetId) />
	</cffunction>
	
	<cffunction name="moveFeaturedOrderUp" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentFeatured = getContentFeaturedService().getContentFeatured(guidAssetId=guidAssetId) />
		
		<cfreturn getContentFeaturedService().moveOrderUp(contentFeatured) />
	</cffunction>
	
	<cffunction name="moveFeaturedOrderDown" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentFeatured = getContentFeaturedService().getContentFeatured(guidAssetId=guidAssetId) />
		
		<cfreturn getContentFeaturedService().moveOrderDown(contentFeatured) />
	</cffunction>
	
	<cffunction name="moveNavContentOrderUp" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />		
		<cfset var navigationContent = getTaxonomyAssetService().getTaxonomyAsset(guidTaxId=guidTaxId,guidAssetId=guidAssetId) />
		
		<cfset navigationContent.setIntOrder(navigationContent.getIntOrder() - 1) />
		
		<cfreturn getTaxonomyAssetService().saveTaxonomyAsset(navigationContent) />
	</cffunction>

	<cffunction name="moveNavContentOrderDown" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />		
		<cfset var navigationContent = getTaxonomyAssetService().getTaxonomyAsset(guidTaxId=guidTaxId,guidAssetId=guidAssetId) />
		
		<cfset navigationContent.setIntOrder(navigationContent.getIntOrder() + 1) />
		
		<cfreturn getTaxonomyAssetService().saveTaxonomyAsset(navigationContent) />
	</cffunction>	
</cfcomponent>