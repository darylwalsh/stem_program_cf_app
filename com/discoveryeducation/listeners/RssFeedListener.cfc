<cfcomponent
	displayname="RssFeedListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="RssFeed Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setRssFeedService" access="public" returntype="void" output="false">
		<cfargument name="rssFeedService" type="com.redactededucation.rssFeed.RssFeedService" required="true" />
		<cfset variables.rssFeedService = arguments.rssFeedService />
	</cffunction>
	<cffunction name="getRssFeedService" access="public" returntype="com.redactededucation.rssFeed.RssFeedService" output="false">
		<cfreturn variables.rssFeedService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="processRssFeedForm" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var rssFeed = arguments.event.getArg("rssFeed") />
		
		<cfset arguments.event.setArg("feedId", rssFeed.getFeedId()) />
		
		<cfreturn getRssFeedService().saveRssFeed(rssFeed) />
	</cffunction>
	
	<cffunction name="getRssFeed" access="public" returntype="com.redactededucation.rssFeed.RssFeed" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var feedId = arguments.event.getArg("feedId") />
		
		<cfreturn getRssFeedService().getRssFeed(feedId) />
	</cffunction>
	
	<cffunction name="getRssFeeds" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var account_guid = arguments.event.getArg("account_guid") />
		
		<cfreturn getRssFeedService().getRssFeeds(account_guid=account_guid) />
	</cffunction>

</cfcomponent>