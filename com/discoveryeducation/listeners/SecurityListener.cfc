<cfcomponent displayname="SecurityListener" output="false" extends="MachII.framework.Listener" hint="Security Listener for Help">
	
	<cffunction name="configure" access="public" output="false" returntype="void">
		<!--- do nothing for now --->
	</cffunction>
	
	<!--- dependencies --->
	<cffunction name="setSecurityService" access="public" output="false" returntype="void">
		<cfargument name="SecurityService" type="com.redactededucation.Security.SecurityService" required="true" />
		<cfset variables.SecurityService = arguments.SecurityService />
	</cffunction>
	<cffunction name="getSecurityService" access="public" output="false" returntype="com.redactededucation.Security.SecurityService">
		<cfreturn variables.SecurityService />
	</cffunction>
	
	<!--- listener methods --->
	<cffunction name="processLoginForm" access="public" output="false" returntype="void" hint="Validates a login attempt">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var username = event.getArg("username") />
		<cfset var password = event.getArg("password") />
		<cfset var user = getSecurityService().authenticate(username,password) />
		<cfset var exitEvent = "failed" />
		<cfset var message = "Your username/password combination was incorrect.<br>Please try again.<br>&nbsp;" />
		
		<cfstoredproc procedure="usp_ServiceAppUserLogonRequest" datasource="EDU_SEC">
    	    <cfprocparam type="In" cfsqltype="CF_SQL_IDSTAMP" dbvarname="@LoggedOnUserID" value="00000000-0000-0000-0000-000000000000" null="yes">
        	<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" dbvarname="@UID" value="#username#">
	        <cfprocparam type="In"cfsqltype="CF_SQL_VARCHAR" dbvarname="@PWD" value="#password#">
    	    <cfprocparam type="In" cfsqltype="CF_SQL_IDSTAMP" dbvarname="@APP_USER_GUID" value="00000000-0000-0000-0000-000000000000" null="yes">
        	<cfprocresult name="APP_USER" resultset="1">
	        <cfprocresult name="LEGACY_XREF" resultset="2">
    	    <cfprocresult name="ACCOUNT" resultset="3">
        	<cfprocresult name="SITE" resultset="4">
	        <cfprocresult name="ROLE" resultset="5">
    	    <cfprocresult name="PermissionBitCode" resultset="6">
        	<cfprocresult name="Products" resultset="7">
	    </cfstoredproc>
	    
	    <cfif IsDefined("APP_USER") AND APP_USER.RecordCount>
			<cfset client.lstRole = ValueList(ROLE.code) />
		
    		<cfcookie name="app_user_guid" value="#APP_USER.APP_USER_GUID#" domain=".redactededucation.com">
        	<cfcookie name="first_name" value="#APP_USER.FIRST_NAME#" domain=".redactededucation.com">
    	    <cfcookie name="last_name" value="#APP_USER.LAST_NAME#" domain=".redactededucation.com">
	        <cfcookie name="UID" value="#APP_USER.UID#" domain=".redactededucation.com">
	        <cfcookie name="lstProducts" value="#ValueList(Products.code)#" domain=".redactededucation.com">
            <cfset strProdSub="">
            <cfloop query="Products">
                <cfset strProdSub="#strProdSub#,#code#:#sub_product_code#">
            </cfloop>
            <cfcookie name="lstSubProducts" value="#strProdSub#" domain=".redactededucation.com">

			<cfset exitEvent = "success" />
			<cfset message = "" />
		</cfif>
		
		<cfset arguments.event.setArg("message", message) />
		<cfset announceEvent(exitEvent, arguments.event.getArgs()) />
	</cffunction>
</cfcomponent>