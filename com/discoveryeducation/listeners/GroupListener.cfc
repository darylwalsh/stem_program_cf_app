<cfcomponent
	displayname="GroupListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Group Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setGroupService" access="public" returntype="void" output="false">
		<cfargument name="GroupService" type="com.redactededucation.group.GroupService" required="true" />
		<cfset variables.GroupService = arguments.GroupService />
	</cffunction>
	<cffunction name="getGroupService" access="public" returntype="com.redactededucation.group.GroupService" output="false">
		<cfreturn variables.GroupService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="getGroupsBySiteId" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var siteId = arguments.event.getArg("siteId") />
		<cfset var orderBy = "name ASC" />
		
		<cfif siteId EQ "">
			<cfset siteId = guidTaxId />
		</cfif>
		<cfreturn getGroupService().getGroups(siteId=siteId,orderBy=orderBy) />
	</cffunction>
	
	<cffunction name="processSiteGroupForm" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var siteGroup = arguments.event.getArg("siteGroup") />
		
		<cfreturn getGroupService().saveGroup(siteGroup) />
	</cffunction>
	
	<cffunction name="getGroup" access="public" returntype="com.redactededucation.group.Group" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var groupId = arguments.event.getArg("groupId") />
		
		<cfreturn getGroupService().getGroup(groupId) />
	</cffunction>
	
	<cffunction name="updateSiteGroup" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var siteGroup = arguments.event.getArg("siteGroup") />
		
		<cfreturn getGroupService().saveGroup(siteGroup) />
	</cffunction>
</cfcomponent>