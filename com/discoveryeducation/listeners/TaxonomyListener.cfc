<cfcomponent
	displayname="TaxonomyListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Taxonomy Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setTaxonomyService" access="public" returntype="void" output="false">
		<cfargument name="TaxonomyService" type="support.model.TaxonomyService" required="true" />
		<cfset variables.TaxonomyService = arguments.TaxonomyService />
	</cffunction>
	<cffunction name="getTaxonomyService" access="public" returntype="support.model.TaxonomyService" output="false">
		<cfreturn variables.TaxonomyService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="getTaxonomys" output="false" access="public" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidLevelOneId = arguments.event.getArg("guidLevelOneId") />
		
		<cfreturn getTaxonomyService().getTaxonomys(guidLevelOneId=guidLevelOneId) />
	</cffunction>
	
	<cffunction name="getAllTaxonomys" output="false" access="public" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfreturn getTaxonomyService().newGetTaxonomys() />
	</cffunction>
	
	<cffunction name="getRootTaxonomys" output="false" access="public" returntype="query"
		hint="I return top level taxonomies">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "name" />
		
		<cfreturn getTaxonomyService().getTaxonomys(intLevel=0,orderBy=orderBy) />
	</cffunction>
	
	<cffunction name="getChildTaxonomys" access="public" output="false" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidParentId = arguments.event.getArg("guidTaxId") />
		<cfset var orderBy = "intOrder" />
		
		<cfreturn getTaxonomyService().getTaxonomys(guidParentId=guidParentId,orderBy=orderBy) />
	</cffunction>
	
	<cffunction name="getTaxonomy" access="public" output="false" returntype="support.model.Taxonomy">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidLevelOneId") />
		
		<cfreturn getTaxonomyService().getTaxonomy(guidTaxId=guidTaxId) />
	</cffunction>
	
	<cffunction name="updateTaxonomy" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var taxonomy = arguments.event.getArg("taxonomy") />
		
		<cfreturn getTaxonomyService().saveTaxonomy(taxonomy) />
	</cffunction>
	
	<cffunction name="getTaxonomyById" access="public" output="false" returntype="support.model.Taxonomy">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		
		<cfreturn getTaxonomyService().getTaxonomy(guidTaxId=guidTaxId) />
	</cffunction>
	
	<cffunction name="createRootTaxonomy" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var taxonomy = arguments.event.getArg("taxonomy") />
		
		<cfset taxonomy.setGuidTaxId(request.udf.CreateGUID()) />
		
		<cfreturn getTaxonomyService().saveTaxonomy(taxonomy) />
	</cffunction>

	<cffunction name="createTaxonomy" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var taxonomy = arguments.event.getArg("taxonomy") />
		
		<cfset taxonomy.setGuidTaxId(request.udf.CreateGUID()) />
		<cfset taxonomy.setIntOrder(getTaxonomyService().getMaxOrder(taxonomy.getGuidParentId()) + 1) />
		<cfset arguments.event.setArg("guidTaxid",taxonomy.getGuidTaxId()) />
		
		<cfreturn getTaxonomyService().saveTaxonomy(taxonomy) />
	</cffunction>
	
	<cffunction name="deleteTaxonomy" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		
		<cfreturn getTaxonomyService().deleteTaxonomy(guidTaxId=guidTaxId) />
	</cffunction>
	
	<cffunction name="getMaxOrder" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var taxonomy = arguments.event.getArg("taxonomy") />
		
		<cfreturn getTaxonomyService().getMaxOrder(taxonomy) />
	</cffunction>
	
	<cffunction name="moveOrderUp" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var taxonomy = arguments.event.getArg("taxonomy") />
		
		<cfreturn getTaxonomyService().moveOrderUp(taxonomy) />
	</cffunction>
	
	<cffunction name="moveOrderDown" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var taxonomy = arguments.event.getArg("taxonomy") />
		
		<cfreturn getTaxonomyService().moveOrderDown(taxonomy) />
	</cffunction>
	
	<cffunction name="getTaxonomysByAssetId" access="public" output="false" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		
		<cfreturn getTaxonomyService().getTaxonomysByAssetId(guidAssetId) />
	</cffunction>

</cfcomponent>