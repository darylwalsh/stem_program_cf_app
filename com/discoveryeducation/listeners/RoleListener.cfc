<cfcomponent
	displayname="RoleListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Role Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setRoleService" access="public" returntype="void" output="false">
		<cfargument name="roleService" type="com.redactededucation.role.RoleService" required="true" />
		<cfset variables.roleService = arguments.roleService />
	</cffunction>
	<cffunction name="getRoleService" access="public" returntype="com.redactededucation.role.RoleService" output="false">
		<cfreturn variables.roleService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="getRoles" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfreturn getRoleService().getRolesQuery() />
	</cffunction>

</cfcomponent>