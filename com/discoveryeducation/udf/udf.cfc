<cfcomponent displayname="" hint="I wrap UDFs">	
	<cffunction name="init" access="public" returntype="udf" output="false">
		<cfreturn this />
	</cffunction>
	
	<cffunction name="UnicodeWin1252" hint="Converts MS-Windows superset characters (Windows-1252) into their XML friendly unicode counterparts" returntype="string">
		<cfargument name="value" type="string" required="yes">
		<cfscript>
      var string = value;
      string = replaceNoCase(string,chr(160),'&amp;##160;','all');      // non-breaking space
      string = replaceNoCase(string,chr(161),'&amp;##161;','all');      // �
      string = replaceNoCase(string,chr(169),'&amp;##169;','all');      // �
      string = replaceNoCase(string,chr(189),'&amp;##189;','all');      // �
      string = replaceNoCase(string,chr(191),'&amp;##191;','all');      // �
      string = replaceNoCase(string,chr(225),'&amp;##225;','all');      // �
      string = replaceNoCase(string,chr(231),'&amp;##231;','all');		// �
      string = replaceNoCase(string,chr(237),'&amp;##237;','all');      // �
      string = replaceNoCase(string,chr(241),'&amp;##241;','all');      // �
      string = replaceNoCase(string,chr(243),'&amp;##243;','all');      // �
      string = replaceNoCase(string,chr(338),'&amp;##338;','all');      // �
      string = replaceNoCase(string,chr(339),'&amp;##339;','all');      // �
      string = replaceNoCase(string,chr(352),'&amp;##352;','all');      // �
      string = replaceNoCase(string,chr(353),'&amp;##353;','all');      // �
      string = replaceNoCase(string,chr(376),'&amp;##376;','all');      // �
      string = replaceNoCase(string,chr(402),'&amp;##402;','all');      // �
      string = replaceNoCase(string,chr(710),'&amp;##710;','all');      // �
      string = replaceNoCase(string,chr(732),'&amp;##732;','all');      // �
      string = replaceNoCase(string,chr(8211),'&amp;##8211;','all');   // �
      string = replaceNoCase(string,chr(8212),'&amp;##8212;','all');   // �
      string = replaceNoCase(string,chr(8216),'&amp;##8216;','all');   // �
      string = replaceNoCase(string,chr(8217),'&amp;##8217;','all');   // �
      string = replaceNoCase(string,chr(8218),'&amp;##8218;','all');   // �
      string = replaceNoCase(string,chr(8220),'&amp;##8220;','all');   // �
      string = replaceNoCase(string,chr(8221),'&amp;##8221;','all');   // �
      string = replaceNoCase(string,chr(8222),'&amp;##8222;','all');   // �
      string = replaceNoCase(string,chr(8224),'&amp;##8224;','all');   // �
      string = replaceNoCase(string,chr(8225),'&amp;##8225;','all');   // �
      string = replaceNoCase(string,chr(8226),'&amp;##8226;','all');   // �
      string = replaceNoCase(string,chr(8230),'&amp;##8230;','all');   // �
      string = replaceNoCase(string,chr(8240),'&amp;##8240;','all');   // �
      string = replaceNoCase(string,chr(8249),'&amp;##8249;','all');   // �
      string = replaceNoCase(string,chr(8250),'&amp;##8250;','all');   // �
      string = replaceNoCase(string,chr(8364),'&amp;##8364;','all');   // �
      string = replaceNoCase(string,chr(8482),'&amp;##8482;','all');   // �
		</cfscript>
		<cfreturn string>
	</cffunction>
	
	<cffunction name="CreateGUID" returntype="string">
		<cfreturn insert("-", CreateUUID(), 23) />
	</cffunction>
	
	<cffunction name="getFileSize">
		<cfargument name="filepath" type="string" required="true" />

		<cfreturn createObject("java","java.io.File").init(arguments.filepath).length() />
	</cffunction>
	
	<cffunction name="FileSize" returntype="string">
		<cfargument name="size" type="string" required="true" />
		
		<cfif arguments.size GTE 1024 AND arguments.size LT 1048576>
			<cfreturn Round(arguments.size/1024) & " KB" />
		<cfelseif arguments.size GTE 1048576>
			<cfreturn DecimalFormat(arguments.size/1048576) & " MB" />
		<cfelse>
			<cfreturn arguments.size & " B" />
		</cfif>
	</cffunction>
	
	<cffunction name="getUserName" returntype="string">
		<cfargument name="app_user_guid" type="string" required="true" />
		
		<cfif arguments.app_user_guid EQ "">
			<cfreturn "No user name" />
		</cfif>
		
		<cfquery name="getUserName" datasource="EDU_SEC">
			SELECT 	uid
			FROM	APP_USER WITH (NOLOCK)
			WHERE 	app_user_guid = <cfqueryparam value="#arguments.app_user_guid#" />
		</cfquery>
		
		<cfreturn getUserName.uid />
	</cffunction>
	
	<cffunction name="getExtension" returntype="string">
		<cfargument name="filename" type="string" required="true" />
		
		<cfif Find(".",arguments.filename)>
			<cfreturn ListLast(arguments.filename,".") />
		<cfelse>
			<cfreturn "" />
		</cfif>		
	</cffunction>

	<cffunction name="parseCsv" returntype="array">
		<cfargument name="filename" type="string" required="true" />
		
		<cfset var fileContents = "" />
		<cfset var parser = CreateObject("java","com.Ostermiller.util.CSVParser") />
		<cfset var parsedFile = "" />
		
		<cffile action="read" file="#arguments.filename#" variable="fileContents" />
		<cfset parsedFile = parser.parse(fileContents) />
		
		<cfreturn parsedFile />
	</cffunction>
	
	<cffunction name="getGrades" returntype="string">
		<cfargument name="minGrade" type="string" required="true" />
		<cfargument name="maxGrade" type="string" required="true" />
		
		<cfset var gradeGuids = "" />
		
		<cfif arguments.minGrade EQ arguments.maxGrade>
			<cfswitch expression="#arguments.minGrade#">
				<cfcase value="Adult">
					<cfset gradeGuids = "16524d65-406f-4f22-bd41-1e482c0696a5" />
				</cfcase>
				<cfcase value="All">
					<cfset gradeGuids = "2ddb51de-4d22-4120-b5f4-e079aea9add6" />
				</cfcase>
				<cfcase value="General">
					<cfset gradeGuids = "cdbf76d5-b32d-4e7c-a22b-6ad0f970af8a" />
				</cfcase>
				<cfcase value="Kindergarten">
					<cfset gradeGuids = "d7fc01bf-a0f3-4011-9704-1a66260a6992" />
				</cfcase>
				<cfcase value="Grade 1">
					<cfset gradeGuids = "d24d43b2-1a43-443f-9cca-aa2680cb6d42" />
				</cfcase>
				<cfcase value="Grade 2">
					<cfset gradeGuids = "342bdef5-1004-4bd3-88a4-d1c81f020842" />
				</cfcase>
				<cfcase value="Grade 3">
					<cfset gradeGuids = "cbb4d2a4-dda3-4f87-bddf-087abe5a8014" />
				</cfcase>
				<cfcase value="Grade 4">
					<cfset gradeGuids = "839aa7df-8a74-40be-90a1-1eff58eeb715" />
				</cfcase>
				<cfcase value="Grade 5">
					<cfset gradeGuids = "bff68b3d-97cd-4ec4-a8c2-617d0b763bb4" />
				</cfcase>
				<cfcase value="Grade 6">
					<cfset gradeGuids = "5d4418a2-d98a-4212-88ad-c7ee11956ddd" />
				</cfcase>
				<cfcase value="Grade 7">
					<cfset gradeGuids = "b10f8967-865e-45d9-af38-25a4308ef9c1" />
				</cfcase>
				<cfcase value="Grade 8">
					<cfset gradeGuids = "650b6512-e320-45a3-b00f-27c7d8d89972" />
				</cfcase>
				<cfcase value="Grade 9">
					<cfset gradeGuids = "08647bc6-cfaa-40c6-bbad-6ff637d43248" />
				</cfcase>
				<cfcase value="Grade 10">
					<cfset gradeGuids = "21f489f4-2f02-4319-a94d-436b87bb1a04" />
				</cfcase>
				<cfcase value="Grade 11">
					<cfset gradeGuids = "02854615-2815-46f8-89d1-d0c2e7e36b59" />
				</cfcase>
				<cfcase value="Grade 12">
					<cfset gradeGuids = "98bf25a0-f17c-451c-9514-bc247a8082f6" />
				</cfcase>
			</cfswitch>
		<cfelse>
			<cfswitch expression="#arguments.minGrade#">
				<cfcase value="Adult">
					<cfset gradeGuids = "16524d65-406f-4f22-bd41-1e482c0696a5" />
				</cfcase>
				<cfcase value="All">
					<cfset gradeGuids = "2ddb51de-4d22-4120-b5f4-e079aea9add6" />
				</cfcase>
				<cfcase value="General">
					<cfset gradeGuids = "cdbf76d5-b32d-4e7c-a22b-6ad0f970af8a" />
				</cfcase>
				<cfcase value="Kindergarten">
					<cfset gradeGuids = "d7fc01bf-a0f3-4011-9704-1a66260a6992" />
				</cfcase>
				<cfcase value="Grade 1">
					<cfset gradeGuids = "d24d43b2-1a43-443f-9cca-aa2680cb6d42" />
				</cfcase>
				<cfcase value="Grade 2">
					<cfset gradeGuids = "342bdef5-1004-4bd3-88a4-d1c81f020842" />
				</cfcase>
				<cfcase value="Grade 3">
					<cfset gradeGuids = "cbb4d2a4-dda3-4f87-bddf-087abe5a8014" />
				</cfcase>
				<cfcase value="Grade 4">
					<cfset gradeGuids = "839aa7df-8a74-40be-90a1-1eff58eeb715" />
				</cfcase>
				<cfcase value="Grade 5">
					<cfset gradeGuids = "bff68b3d-97cd-4ec4-a8c2-617d0b763bb4" />
				</cfcase>
				<cfcase value="Grade 6">
					<cfset gradeGuids = "5d4418a2-d98a-4212-88ad-c7ee11956ddd" />
				</cfcase>
				<cfcase value="Grade 7">
					<cfset gradeGuids = "b10f8967-865e-45d9-af38-25a4308ef9c1" />
				</cfcase>
				<cfcase value="Grade 8">
					<cfset gradeGuids = "650b6512-e320-45a3-b00f-27c7d8d89972" />
				</cfcase>
				<cfcase value="Grade 9">
					<cfset gradeGuids = "08647bc6-cfaa-40c6-bbad-6ff637d43248" />
				</cfcase>
				<cfcase value="Grade 10">
					<cfset gradeGuids = "21f489f4-2f02-4319-a94d-436b87bb1a04" />
				</cfcase>
				<cfcase value="Grade 11">
					<cfset gradeGuids = "02854615-2815-46f8-89d1-d0c2e7e36b59" />
				</cfcase>
				<cfcase value="Grade 12">
					<cfset gradeGuids = "98bf25a0-f17c-451c-9514-bc247a8082f6" />
				</cfcase>
			</cfswitch>
			<cfswitch expression="#arguments.maxGrade#">
				<cfcase value="Adult">
					<cfset gradeGuids = ListAppend(gradeGuids,"16524d65-406f-4f22-bd41-1e482c0696a5") />
				</cfcase>
				<cfcase value="All">
					<cfset gradeGuids = ListAppend(gradeGuids,"2ddb51de-4d22-4120-b5f4-e079aea9add6") />
				</cfcase>
				<cfcase value="General">
					<cfset gradeGuids = ListAppend(gradeGuids,"cdbf76d5-b32d-4e7c-a22b-6ad0f970af8a") />
				</cfcase>
				<cfcase value="Kindergarten">
					<cfset gradeGuids = ListAppend(gradeGuids,"d7fc01bf-a0f3-4011-9704-1a66260a6992") />
				</cfcase>
				<cfcase value="Grade 1">
					<cfset gradeGuids = ListAppend(gradeGuids,"d24d43b2-1a43-443f-9cca-aa2680cb6d42") />
				</cfcase>
				<cfcase value="Grade 2">
					<cfset gradeGuids = ListAppend(gradeGuids,"342bdef5-1004-4bd3-88a4-d1c81f020842") />
				</cfcase>
				<cfcase value="Grade 3">
					<cfset gradeGuids = ListAppend(gradeGuids,"cbb4d2a4-dda3-4f87-bddf-087abe5a8014") />
				</cfcase>
				<cfcase value="Grade 4">
					<cfset gradeGuids = ListAppend(gradeGuids,"839aa7df-8a74-40be-90a1-1eff58eeb715") />
				</cfcase>
				<cfcase value="Grade 5">
					<cfset gradeGuids = ListAppend(gradeGuids,"bff68b3d-97cd-4ec4-a8c2-617d0b763bb4") />
				</cfcase>
				<cfcase value="Grade 6">
					<cfset gradeGuids = ListAppend(gradeGuids,"5d4418a2-d98a-4212-88ad-c7ee11956ddd") />
				</cfcase>
				<cfcase value="Grade 7">
					<cfset gradeGuids = ListAppend(gradeGuids,"b10f8967-865e-45d9-af38-25a4308ef9c1") />
				</cfcase>
				<cfcase value="Grade 8">
					<cfset gradeGuids = ListAppend(gradeGuids,"650b6512-e320-45a3-b00f-27c7d8d89972") />
				</cfcase>
				<cfcase value="Grade 9">
					<cfset gradeGuids = ListAppend(gradeGuids,"08647bc6-cfaa-40c6-bbad-6ff637d43248") />
				</cfcase>
				<cfcase value="Grade 10">
					<cfset gradeGuids = ListAppend(gradeGuids,"21f489f4-2f02-4319-a94d-436b87bb1a04") />
				</cfcase>
				<cfcase value="Grade 11">
					<cfset gradeGuids = ListAppend(gradeGuids,"02854615-2815-46f8-89d1-d0c2e7e36b59") />
				</cfcase>
				<cfcase value="Grade 12">
					<cfset gradeGuids = ListAppend(gradeGuids,"98bf25a0-f17c-451c-9514-bc247a8082f6") />
				</cfcase>
			</cfswitch>
		</cfif>
		
		<cfreturn gradeGuids />
	</cffunction>
	
	<cffunction name="getAssetType" returntype="string">
		<cfargument name="assetType" type="string" required="true" />
		
		<cfset var assetTypeGuid = "" />
		
		<cfswitch expression="#arguments.assetType#">
			<cfcase value="Animation">
				<cfset assetTypeGuid = "f5b3f3a5-5f8f-4032-968f-9a9d05b2d27e" />
			</cfcase>
			<cfcase value="Application/Program">
				<cfset assetTypeGuid = "e29b6be1-4180-457a-ae83-a66398d71d70" />
			</cfcase>
			<cfcase value="Article">
				<cfset assetTypeGuid = "2c8038f9-ed7d-45c1-965a-25f4aa5ff949" />
			</cfcase>
			<cfcase value="Audio">
				<cfset assetTypeGuid = "9c86af9f-5420-4662-abed-910857f7e301" />
			</cfcase>
			<cfcase value="Clip Art">
				<cfset assetTypeGuid = "7859e39f-7300-4a63-86c9-8446e5b32037" />
			</cfcase>
			<cfcase value="Document">
				<cfset assetTypeGuid = "e29b6be1-4180-457a-ae83-a66398d71d70" />
			</cfcase>
			<cfcase value="Event">
				<cfset assetTypeGuid = "71318909-52fa-4af0-b4ef-d90d6f98e45e" />
			</cfcase>
			<cfcase value="Image">
				<cfset assetTypeGuid = "3123b3ad-3c2d-49cf-92ce-9927c3c09e64" />
			</cfcase>
			<cfcase value="Lesson Plan">
				<cfset assetTypeGuid = "0c0ab655-784a-414e-b5d9-e26695741386" />
			</cfcase>
			<cfcase value="Map">
				<cfset assetTypeGuid = "e29b6be1-4180-457a-ae83-a66398d71d70" />
			</cfcase>
			<cfcase value="Podcast">
				<cfset assetTypeGuid = "9c86af9f-5420-4662-abed-910857f7e301" />
			</cfcase>
			<cfcase value="PowerPoint">
				<cfset assetTypeGuid = "3167928d-5a19-4de6-b4c5-4c504fe5a0c0" />
			</cfcase>
			<cfcase value="Quiz">
				<cfset assetTypeGuid = "01cf3c1e-080f-4ce0-a4b6-70460f33a08d" />
			</cfcase>
			<cfcase value="Video">
				<cfset assetTypeGuid = "b373b9c1-fe6d-4891-9ff3-3b59b23550e2" />
			</cfcase>
			<cfcase value="Video Segments">
				<cfset assetTypeGuid = "b2ae1c36-4265-46d4-b8d0-f430d6e1357e" />
			</cfcase>
			<cfcase value="Web Links">
				<cfset assetTypeGuid = "5acf97eb-13f9-4959-a6eb-28c11b4941f3" />
			</cfcase>
			<cfdefaultcase>
				<cfset assetTypeGuid = "e29b6be1-4180-457a-ae83-a66398d71d70" />
			</cfdefaultcase>
		</cfswitch>
		
		<cfreturn assetTypeGuid />
	</cffunction>
	
	<cffunction name="getSubject" returntype="string">
		<cfargument name="subjectAreaId" type="string" required="true" />
		
		<cfset var subjectGuid = "" />
		
		<cfswitch expression="#arguments.subjectAreaId#">
			<cfcase value="Algebra">
				<cfset subjectGuid = "f1b6c5ce-acf9-43ec-94bf-d555dd30fd27" />
			</cfcase>
			<cfcase value="Arithmetic">
				<cfset subjectGuid = "137db89e-df19-4830-9b57-1be059a621aa" />
			</cfcase>
			<cfcase value="Careers">
				<cfset subjectGuid = "0d87b077-0088-457e-838c-62bc557b8033" />
			</cfcase>
			<cfcase value="Chemistry">
				<cfset subjectGuid = "2aef46a3-26e8-4152-878f-17d0ffd93be2" />
			</cfcase>
			<cfcase value="Civics">
				<cfset subjectGuid = "0a714ac0-8e1b-4972-91c3-beb6374d0eb9" />
			</cfcase>
			<cfcase value="Computer Science">
				<cfset subjectGuid = "259d416c-da02-44c2-9b7f-21630e6e6e8e" />
			</cfcase>
			<cfcase value="Controlled Substances">
				<cfset subjectGuid = "3e6a9230-9d23-4551-8cd5-b0604eaf25d6" />
			</cfcase>
			<cfcase value="Dance">
				<cfset subjectGuid = "2272ae17-b7ab-4d70-be0e-ade68ea22714" />
			</cfcase>
			<cfcase value="Earth and Space Science">
				<cfset subjectGuid = "e1154f29-d171-456b-8c44-44277d532b33" />
			</cfcase>
			<cfcase value="Geography and Culture">
				<cfset subjectGuid = "ebd9bad5-9bbc-4670-bd0a-8ebee799b0ef" />
			</cfcase>
			<cfcase value="Geometry">
				<cfset subjectGuid = "4678d016-c4d0-4d8d-8b2d-75da6cb907ce" />
			</cfcase>
			<cfcase value="Grammar and Composition">
				<cfset subjectGuid = "5b2a7570-019f-4f27-9e02-679ad1099d04,4f784d67-7092-49c9-ae62-d98f0c8c48b9" />
			</cfcase>
			<cfcase value="History">
				<cfset subjectGuid = "3e59554a-9096-4bd8-85d7-7ea5b9873c42" />
			</cfcase>
			<cfcase value="Lessons in Life">
				<cfset subjectGuid = "60fbeab1-8a44-4518-842b-6e82237cb021" />
			</cfcase>
			<cfcase value="Life Science/Biology">
				<cfset subjectGuid = "f29f0a94-1c50-4c31-9876-a560eadb5318" />
			</cfcase>
			<cfcase value="Literature">
				<cfset subjectGuid = "3ce3a2b2-816e-4a35-84f6-825dae59d54c" />
			</cfcase>
			<cfcase value="Math AP Topics">
				<cfset subjectGuid = "c9c409e3-470f-42cf-b5c2-bafe623c1001" />
			</cfcase>
			<cfcase value="Measurement">
				<cfset subjectGuid = "81be788c-0e97-4a1d-afa4-652763f09e7b" />
			</cfcase>
			<cfcase value="Music">
				<cfset subjectGuid = "5f63b8f6-894e-446e-a4db-eb0ab58ea5c4" />
			</cfcase>
			<cfcase value="Phonics">
				<cfset subjectGuid = "b7ce70e0-8961-48a1-830f-4e3b9d81cd8e" />
			</cfcase>
			<cfcase value="Physical Health">
				<cfset subjectGuid = "2112f7d2-4e23-4a35-a6a0-9d48999cb74b" />
			</cfcase>
			<cfcase value="Physical Science">
				<cfset subjectGuid = "2aef46a3-26e8-4152-878f-17d0ffd93be2" />
			</cfcase>
			<cfcase value="Physics">
				<cfset subjectGuid = "2aef46a3-26e8-4152-878f-17d0ffd93be2" />
			</cfcase>
			<cfcase value="Probability and Statistics">
				<cfset subjectGuid = "868a2a31-4f66-432b-8b0b-0dfa787d7c20" />
			</cfcase>
			<cfcase value="Problem Solving">
				<cfset subjectGuid = "ee574dbf-763d-474f-993c-6dacda1920e5" />
			</cfcase>
			<cfcase value="Professional Development">
				<cfset subjectGuid = "1cf8af81-ffe4-4df3-920b-c9b2f252b7de" />
			</cfcase>
			<cfcase value="Reading">
				<cfset subjectGuid = "b7ce70e0-8961-48a1-830f-4e3b9d81cd8e" />
			</cfcase>
			<cfcase value="Research Skills">
				<cfset subjectGuid = "c2ff6987-778b-4a8e-91c4-6c00ef6506dd" />
			</cfcase>
			<cfcase value="Rigor and Relevance">
				<cfset subjectGuid = "1a70bd35-aa91-4ddc-869d-182c83b02159" />
			</cfcase>
			<cfcase value="Scientific Inquiry">
				<cfset subjectGuid = "e8854279-169b-4d91-bcf4-fec7f06eaa60" />
			</cfcase>
			<cfcase value="Social Issues in Health">
				<cfset subjectGuid = "60fbeab1-8a44-4518-842b-6e82237cb021" />
			</cfcase>
			<cfcase value="Technology">
				<cfset subjectGuid = "1538b1bc-20f0-4ec1-8b65-d3652ef3b264" />
			</cfcase>
			<cfcase value="Viewing Skills and Media Literacy">
				<cfset subjectGuid = "b7ce70e0-8961-48a1-830f-4e3b9d81cd8e" />
			</cfcase>
			<cfcase value="Visual Arts">
				<cfset subjectGuid = "83732f9e-0280-4f5f-a6ed-a4f2ca12fe9d" />
			</cfcase>
			<cfdefaultcase>
				<cfset subjectGuid = "" />
			</cfdefaultcase>
		</cfswitch>
		
		<cfreturn subjectGuid />
	</cffunction>

	<cffunction name="convertVideo" access="public" output="false" returntype="any">
		<cfargument name="binPath" type="string" required="true" />
		<cfargument name="srcPath" type="string" required="true" />
		<cfargument name="destPath" type="string" required="true" />
		<cfargument name="dimensions" type="string" required="false" default="320x240" />
		
		<cfset var resultLog = "#destPath#.resultLog.txt" />
		<cfset var errorLog = "#destPath#.log" />
		<cfset var results = StructNew() />
		
		<cfscript>
			try {
				runtime = CreateObject("java", "java.lang.Runtime").getRuntime();
				command = '#arguments.binPath# -i "#arguments.srcPath#" -g 300 -y -s #arguments.dimensions# -ar 44100 -ab 64k -b 400k -f flv "#arguments.destPath#"';
				process = runtime.exec(#command#);
				results.errorLog = processStream(process.getErrorStream(), errorLog);
				results.resultLogSuccess = processStream(process.getInputStream());
				results.exitCode = process.waitFor();
			}
			catch(exception e) {
				results.status = e;
			}
		</cfscript>
		
		<cfreturn results />
	</cffunction>
	
	<cffunction name="getVideoInfo" access="public" output="false" returntype="any">
		<cfargument name="binPath" type="string" required="true" />
		<cfargument name="srcPath" type="string" required="true" />
		
		<cfset var results = StructNew() />
		<cfset var runtime = "" />
		<cfset var command = "" />

		<cfset results.errorLog = "" />		
		<cfscript>
			try {
				runtime = CreateObject("java", "java.lang.Runtime").getRuntime();
				command = '#arguments.binPath# -i "#arguments.srcPath#"';
				process = runtime.exec(#command#);
				results.errorLog = processStream(process.getErrorStream());
				results.resultLog = processStream(process.getInputStream());
				results.exitCode = process.waitFor();
			}
			catch(exception e) {
				results.status = e;
			}
		</cfscript>
		
		<cfreturn results.errorLog />
	</cffunction>
	
	<cffunction name="getParentAssetId" access="public" output="false" returntype="string">
		<cfargument name="childAssetId" type="string" required="true" />
		
		<cfset var qRead = "" />
		
		<cfquery name="qRead" datasource="AllProductAssets">
			SELECT
				guidAssetId1
			FROM	tblAssetRelationships tar
			WHERE	guidAssetId2 = <cfqueryparam value="#arguments.childAssetId#" />
		</cfquery>
		
		<cfreturn qRead.guidAssetId1 />
	</cffunction>
	
	<cffunction name="getParentName" returntype="string">
		<cfargument name="guidParentId" type="string" required="true" />
		
		<cfset var qRead = "" />
		
		<cfquery name="qRead" datasource="AllProductAssets">
			SELECT
				name
			FROM	cms_navigation n
			WHERE	0=0
			AND		guidTaxId = <cfqueryparam value="#arguments.guidParentId#" />
		</cfquery>
	
		<cfreturn qRead.name />
	</cffunction>
	
    <cffunction name="getProperties" access="public" returntype="struct" hint="Creates a struct (like java.util.Properties) based on a Java .properties file" output="false">
	    <cfargument name="fileName" type="string" required="true" description="The name of the .properties file, relative to your application root"/>
		
		<cfset var properties = CreateObject("java", "java.util.Properties").init() />
		<cfset var propFile = CreateObject("java", "java.io.FileInputStream").init(javaCast("string", expandPath(arguments.fileName))) />
		
		<cfset properties.load(propFile) />
		<cfset propFile.close() />
		
		<cfreturn properties />
	</cffunction>

	<cffunction name="createThumbnail" access="public" output="false" returntype="string">
		<cfargument name="fileName" type="string" required="true" />
		<cfargument name="fileId" type="string" required="true" />
		
		<cfset var ffmpegPath = "#GetDirectoryFromPath(ExpandPath('/bin/'))#ffmpeg.exe" />
		<cfset var fileExt = ListLast(arguments.fileName,".") />
		<cfset var imageUtils = CreateObject("component", "com.redactededucation.imageUtils.imageUtils") />
		<cfset var thumbPath = "#GetDirectoryFromPath(arguments.fileName)#thumbnails/" />
		<cfset var thumbName = "" />
		
		<cfif NOT DirectoryExists(thumbPath)>
		   <cfdirectory action = "create" directory = "#thumbPath#" />
		</cfif>
		
		<cfswitch expression="#fileExt#">
			<cfcase value="bmp,gif,jpeg,jpg,png,tif,tiff">
			<cftry>
				<cffile action="copy" source="#arguments.fileName#" destination="#thumbPath#temp.jpg" />
				<cfset pic = ImageRead("#thumbPath#temp.jpg") />
				<cfset aspect = ImageNew(pic) />
				<cfset aspect = imageUtils.aspectCrop(aspect,120,90,"center") />
				<cfimage action="write" destination="#thumbPath##arguments.fileId#.jpg" source="#aspect#" overwrite="true" />
				<cfcatch>
					<cfreturn "defaultThumb.gif" />
				</cfcatch>
			</cftry>
				<cftry>
					<cffile action="delete" file="#thumbPath#temp.jpg" />
					<cfcatch><!--- do nothing ---></cfcatch>
				</cftry>
			
				<cfreturn "#arguments.fileId#.jpg" />
			</cfcase>
			<cfcase value="3gp,asf,asx,avi,flv,m4v,mov,mp4,mpeg,mpg,qt,ram,rm,wmv">

				<cfreturn "defaultThumb.gif" />
				<!---
				<cfset inputFile = arguments.fileName />							
				<cfset resultLog = "c:/resultLog.txt" />
				<cfset errorLog = "c:/errorLog.txt" />
				<cfset outputFile = "#thumbPath##arguments.fileId#.jpg" />
				<cfset results = StructNew() />
				<cfscript>
					try {
						runtime = CreateObject("java", "java.lang.Runtime").getRuntime();
						command = '#ffmpegPath# -i "#inputFile#" -an -ss 02 -t 00:00:01 -r 1 -vframes 1 -y -s 120x90 -f mjpeg "#outputFile#"';
						process = runtime.exec(#command#);
						results.errorLogSuccess = processStream(process.getErrorStream(), errorLog);
						results.resultLogSuccess = processStream(process.getInputStream());
						results.exitCode = process.waitFor();
					}
					catch(exception e) {
						results.status = e;
					}
				</cfscript>
								
				<cfif getFileSize(outputFile) NEQ 0>
					<cfreturn "#arguments.fileId#.jpg" />
				<cfelse>
					<cfreturn "defaultThumb.gif" />
				</cfif>
				--->
			</cfcase>
			<cfcase value="pdf">
				<cftry>
					<cfpdf action="thumbnail" source="#arguments.fileName#" 
						destination="#thumbPath#" format="jpeg" imagePrefix="thumbnail"
						overwrite="yes" pages="1" resolution="high" />
					<cfcatch>
						<cfreturn "defaultThumb.gif" />
					</cfcatch>
				</cftry>
				<cfset pic = ImageRead("#thumbPath#thumbnail_page_1.jpg") />
				<cfset aspect = ImageNew(pic) />
				<cfset aspect = imageUtils.aspectCrop(aspect,85,110,"center") />
				<cfimage action="write" destination="#thumbPath##arguments.fileId#.jpg" source="#aspect#" overwrite="true" />
				<cftry>
					<cffile action="delete" file="#thumbPath#thumbnail_page_1.jpg" />
					<cfcatch><!--- do nothing ---></cfcatch>
				</cftry>
				
				<cfreturn "#arguments.fileId#.jpg" />
			</cfcase>
			<cfcase value="ppt">
				<cfreturn "ppt.gif" />
			</cfcase>
			<cfcase value="doc,rtf">
				<cfreturn "doc.gif" />
			</cfcase>
			<cfcase value="xls">
				<cfreturn "xls.gif" />
			</cfcase>
			<cfdefaultcase>	
				<cfreturn "defaultThumb.gif" />
			</cfdefaultcase>
		</cfswitch>
	</cffunction>
	
	<!--- function used to drain the input/output streams. Optionally write the stream to a file --->
	<cffunction name="processStream" access="public" output="false" returntype="any" hint="Returns true if stream was successfully processed">
		<cfargument name="in" type="any" required="true" hint="java.io.InputStream object" />
		<cfargument name="logPath" type="string" required="false" default="" hint="Full path to LogFile" />
	
		<cfset var out = "" />
		<cfset var writer = "" />
		<cfset var reader = "" />
		<cfset var buffered = "" />
		<cfset var line = "" />
		<cfset var sendToFile = false />
		<cfset var errorFound = false />
		<cfset var str = "<p>" />

		<cfscript>
			if ( len(trim(arguments.logPath)) ) {
				out = createObject("java", "java.io.FileOutputStream").init(arguments.logPath);
				writer = createObject("java", "java.io.PrintWriter").init(out);
				sendToFile = true;
			}

			reader = createObject("java", "java.io.InputStreamReader").init(arguments.in);
			buffered = createObject("java", "java.io.BufferedReader").init(reader);
			line = buffered.readLine();
			while ( IsDefined("line") ) {
				if (sendToFile) {
					writer.println(line);
				}
				str = str & '<br />' & line;
				line = buffered.readLine();
			}
				if (sendToFile) {
				errorFound = writer.checkError();
				writer.flush();
				writer.close();
			}
			str = str & '</p>';
		</cfscript>
	
		<!--- return true if no errors found. --->
		<cfreturn str />
	</cffunction>

	<cfscript>
	/**
	* Similar to xmlFormat() but replaces all characters not on the &quot;good&quot; list as opposed to characters specifically on the &quot;bad&quot; list.
	*
	* @param inString      String to format. (Required)
	* @return Returns a string.
	* @author Samuel Neff (sam@serndesign.com)
	* @version 1, January 12, 2004
	*/
	function xmlFormat2(inString) {

		var goodChars = "!@##$%^*()0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`~[]{};:,./?\| -_=+#chr(13)##chr(10)##chr(9)#";
		var i = 1;
		var c = "";
		var s = "";

		for (i=1; i LTE len(inString); i=i+1) {
			c = mid(inString, i, 1);

			if (find(c, goodChars)) {
				s = s & c;
			} else {
				s = s & "&##" & asc(c) & ";";
			}
		}
		
		return s;
	}
	/**
	* Removes potentially nasty HTML text.
	* Version 2 by Lena Aleksandrova - changes include fixing a bug w/ arguments and use of REreplace where REreplaceNoCase should have been used.
	* version 4 fix by Javier Julio - when a bad event is removed, remove the arg too, ie, remove onclick=&quot;foo&quot;, not just onclick.
	*
	* @param text      String to be modified. (Required)
	* @param strip      Boolean value (defaults to false) that determines if HTML should be stripped or just escaped out. (Optional)
	* @param badTags      A list of bad tags. Has a long default list. Consult source. (Optional)
	* @param badEvents      A list of bad HTML events. Has a long default list. Consult source. (Optional)
	* @return Returns a string.
	* @author Nathan Dintenfass (nathan@changemedia.com)
	* @version 4, October 16, 2006
	*/
	function safetext(text) {
    	//default mode is "escape"
	    var mode = "escape";
    	//the things to strip out (badTags are HTML tags to strip and badEvents are intra-tag stuff to kill)
	    //you can change this list to suit your needs
    	var badTags = "SCRIPT,OBJECT,APPLET,EMBED,FORM,LAYER,ILAYER,FRAME,IFRAME,FRAMESET,PARAM,META";
	    var badEvents = "onClick,onDblClick,onKeyDown,onKeyPress,onKeyUp,onMouseDown,onMouseOut,onMouseUp,onMouseOver,onBlur,onChange,onFocus,onSelect,javascript:";
    	var stripperRE = "";
    
	    //set up variable to parse and while we're at it trim white space
    	var theText = trim(text);
		//find the first open bracket to start parsing
	    var obracket = find("<",theText);        
    	//var for badTag
	    var badTag = "";
    	//var for the next start in the parse loop
	    var nextStart = "";
    	//if there is more than one argument and the second argument is boolean TRUE, we are stripping
	    if(arraylen(arguments) GT 1 AND isBoolean(arguments[2]) AND arguments[2]) mode = "strip";
    	if(arraylen(arguments) GT 2 and len(arguments[3])) badTags = arguments[3];
	    if(arraylen(arguments) GT 3 and len(arguments[4])) badEvents = arguments[4];
    	//the regular expression used to stip tags
	    stripperRE = "</?(" & listChangeDelims(badTags,"|") & ")[^>]*>";    
    	//Deal with "smart quotes" and other "special" chars from MS Word
	    theText = replaceList(theText,chr(8216) & "," & chr(8217) & "," & chr(8220) & "," & chr(8221) & "," & chr(8212) & "," & chr(8213) & "," & chr(8230),"',',"","",--,--,...");
    	//if escaping, run through the code bracket by bracket and escape the bad tags.
	    if(mode is "escape"){
    	    //go until no more open brackets to find
        	while(obracket){
	            //find the next instance of one of the bad tags
    	        badTag = REFindNoCase(stripperRE,theText,obracket,1);
        	    //if a bad tag is found, escape it
            	if(badTag.pos[1]){
	                theText = replace(theText,mid(TheText,badtag.pos[1],badtag.len[1]),HTMLEditFormat(mid(TheText,badtag.pos[1],badtag.len[1])),"ALL");
    	            nextStart = badTag.pos[1] + badTag.len[1];
        	    }
            	//if no bad tag is found, move on
	            else{
    	            nextStart = obracket + 1;
        	    }
            	//find the next open bracket
	            obracket = find("<",theText,nextStart);
    	    }
	    }
    	//if not escaping, assume stripping
	    else{
    	    theText = REReplaceNoCase(theText,stripperRE,"","ALL");
	    }
    	//now kill the bad "events" (intra tag text)
	    theText = REReplaceNoCase(theText,'(#ListChangeDelims(badEvents,"|")#)[^ >]*',"","ALL");
    	//return theText
	    return theText;
	}
	/**
	* Limit a string's output to the desired length.
	*
	* @param string      The string to modify.
	* @param length      The max length to use.
	* @return Returns a string.
	* @author John Reed (johnreed1972@yahoo.com.au)
	* @version 1, February 24, 2002
	*/
	function maxLength(string, length) {
    	var padding = 3;
	    var tmp = "&##8230;";
	    var trimmedString = Trim(string);
        
    	if ( len(trimmedString) gte length )
        	return removeChars(trimmedString, length, len(trimmedString) - padding) & tmp;
	    else return trimmedString;
	}
	/**
	* Caesar-cypher encryption that replaces each English letter with the one 13 places forward or back along the alphabet.
	*
	* @param string      String you wish to rot13 encrypt.
	* @return Returns a string.
	* @author Rob Brooks-Bilson (rbils@amkor.com)
	* @version 1.0, August 23, 2001
	*/
	function rot13(string)
	{
	var i=0;
	var j=0;
	var k=0;
	var out="";
	for (i=1; i LTE Len(String); i=i+1){
	j=Asc(Mid(string, i, 1));
	if(j GTE 65 AND j LTE 90){
	j=((J-52) MOD 26)+65;
	}
	else if(j GTE 97 AND j LTE 122){
	j=((j-84) MOD 26)+97;
	}
	out=out&Chr(j);
	}
	return out;
	}
	/**
	* Abbreviates a given string to roughly the given length, stripping any tags, making sure the ending doesn't chop a word in two, and adding an ellipsis character at the end.
	* Fix by Patrick McElhaney
	* v3 by Ken Fricklas kenf@accessnet.net, takes care of too many spaces in text.
	*
	* @param string      String to use. (Required)
	* @param len      Length to use. (Required)
	* @return Returns a string.
	* @author Gyrus (kenf@accessnet.netgyrus@norlonto.net)
	* @version 3, September 6, 2005
	*/
	function abbreviate(string,len) {
	    var newString = REReplace(string, "<[^>]*>", " ", "ALL");
    	var lastSpace = 0;
	    newString = REReplace(newString, " \s*", " ", "ALL");
    	if (len(newString) gt len) {
        	newString = left(newString, len-2);
	        lastSpace = find(" ", reverse(newString));
    	    lastSpace = len(newString) - lastSpace;
        	newString = left(newString, lastSpace) & " &##8230;";
	    }    
    	return newString;
	}
/**
* Removes HTML from the string.
* v2 - Mod by Steve Bryant to find trailing, half done HTML.
*
* @param string      String to be modified. (Required)
* @return Returns a string.
* @author Raymond Camden (ray@camdenfamily.com)
* @version 3, July 9, 2008
*/
function stripHTML(str) {
str = reReplaceNoCase(str, "<.*?>","","all");
//get partial html in front
str = reReplaceNoCase(str, "^.*?>","");
//get partial html at end
str = reReplaceNoCase(str, "<.*$","");
return str;
}
	/**
	* Gets the ip address of the server.
	*
	* @return Returns a string.
	* @author Robert Everland III (reverland@reactivevision.com)
	* @version 1, January 12, 2004
	*/
	function GetServerIP() {
		var iaclass="";
		var addr="";

		// Init class
		iaclass=CreateObject("java", "java.net.InetAddress");

		//Get Local host variable
		addr=iaclass.getLocalHost();

		// Return ip address
		return addr.getHostAddress();
	}
/**
 * Creates a verity &quot;safe&quot; search string.
 * Version 2 rewritten by Raymond Camden
 * Version 3 - made \ into \\ thanks to user comment
 * Version 4 - Fixed bugs identified by John Salonich II (21 JAN 2003), Neal Todd (06 FEB 04), Jeremy Halliwell (01 APR 03). Also added fix for curly brackets, comma, funny quote and plus.
 * v5 bug fixed by Dominic OConnor
 * v6 fix by Mark, when we remove bad chars, replace with space, not nothing
 * 
 * @param input 	 String to Verity clean. (Required)
 * @return Returns a string. 
 * @author Simon Potter (spotter@redbanner.com) 
 * @version 6, April 24, 2009 
 */
function verityClean(input) {
	//Value to return after cleaning
	var cleanText = trim(input);
	// List of special characters to remove
	var reBadChars = "\\|@|#chr(34)#|'|<|>|\(|\)|!|=|\[|\]|\{|\}|\#chr(44)#|`|\#chr(43)#";
	// List of words to watch for
	var arProblemWords = "and,or,not";	
	var x = 1;
	var y = 1;
	var temp = "";
	
	//=-=-=-=-=-=-=-=-=-=-=-=-=-=
	//Turn list into arrays for the loop
	//=-=-=-=-=-=-=-=-=-=-=-=-=-=
	arProblemWords = ListToArray(arProblemWords);	
	
	//=-=-=-=-=-=-=-=-
	// Strip double spaces 
	//=-=-=-=-=-=-=-=-
	cleanText = reReplace(cleanText," {2,}"," ","all");

	//=-=-=-=-=-=-=-=-=-
	// Strip bad characters 
	//=-=-=-=-=-=-=-=-=
	cleanText = reReplace(cleanText,reBadChars," ","all");
	
	//=-=-=-=-=-=-=-=-
	// Clean up sequences of space characters
	//=-=-=-=-=-=-=-=-
	cleanText = reReplace(cleanText,"[[:space:]]+"," ","all");

	//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	// Remove dupes and start/end bad words
	//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	for (x = 1; x lte arraylen(arProblemWords); x = x + 1) {
	
		//remove dupes
		cleanText = trim(reReplace(cleanText,"(#arProblemWords[x]#[[:space:]]+){2,}","","all"));

		//remove arProblemWord[x] + any of the others
		temp = arProblemWords[x] & "[[:space:]]+(";
		for(y = 1; y lte arrayLen(arProblemWords); y=y+1) {
			if(x neq y) {
				temp = temp & arProblemWords[y] & "[[:space:]]+|";
			}
		}
		//remove last |
		temp = left(temp, len(temp)-1);
		//add closing )
		temp = temp & ")";
		cleanText = trim(reReplace(cleanText,temp,"","all"));
		
		//remove from beginning 
		if(
			findNoCase(arProblemWords[x],cleanText) is 1 and 
			reFind("[[:space:]]+",mid(cleanText,len(arProblemWords[x])+1,1)) and 
			mid(cleanText,1,3) NEQ "not"
		) cleanText = trim(right(cleantext,len(cleantext) - len(arProblemWords[x])));
		
		if(
			findNoCase(arProblemWords[x],cleanText) gt 0 and 
			len(cleanText) eq len(arProblemWords[x])
		) cleanText = "";
		
		//remove from end
        if(
            findNoCase(arProblemWords[x],cleanText) gt 0 and
            findNoCase(arProblemWords[x],cleanText,len(cleanText) - len(arProblemWords[x])) is (len(cleanText) - len(arProblemWords[x]) + 1)
            and
            reFind("[[:space:]]+",mid(cleanText,len(cleanText) - len(arProblemWords[x]),1))
        ) cleanText = trim(left(cleanText, len(cleanText) - len(arProblemWords[x])));					
	}	
	
	// Return the cleaned value
	return cleanText;
}
/**
* Applies a simple highlight to a word in a string.
* Original version by Raymond Camden.
*
* @param string      The string to format. (Required)
* @param word      The word to highlight. (Required)
* @param front      This is the HTML that will be placed in front of the highlighted match. It defaults to <span style= (Optional)
* @param back      This is the HTML that will be placed at the end of the highlighted match. Defaults to </span> (Optional)
* @param matchCase      If true, the highlight will only match when the case is the same. Defaults to false. (Optional)
* @return Returns a string.
* @author Dave Forrest (dmf67@yahoo.com)
* @version 2, June 12, 2003
*/
function highLight(source,lookfor) {
    var tmpOn = "[;;^";
    var tmpOff = "^;;]";
    var hilightitem    = "<SPAN STYLE='background-color:yellow;'>";
    var endhilight = "</SPAN>";
    var matchCase = false;
    var obracket = "";
    var tmps        = "";
    var stripperRE = "";
    var badTag        = "";
    var nextStart    = "";
    
    try {
	    if(ArrayLen(arguments) GTE 3) hilightitem = arguments[3];
	    if(ArrayLen(arguments) GTE 4) endhilight = arguments[4];
	    if(ArrayLen(arguments) GTE 5) matchCase = arguments[5];
	    if(NOT matchCase)     source = REReplaceNoCase(source,"(#lookfor#)","#tmpOn#\1#tmpOff#","ALL");
	    else                 source = REReplace(source,"(#lookfor#)","#tmpOn#\1#tmpOff#","ALL");
	    obracket = find("<",source);
	    stripperRE = "<.[^>]*>";    
	    while(obracket){
	        badTag = REFindNoCase(stripperRE,source,obracket,1);
	        if(badTag.pos[1]){
	            tmps      = Replace(Mid(source,badtag.pos[1],badtag.len[1]),"#tmpOn#","","ALL");
	            source      = Replace(source,Mid(source,badtag.pos[1],badtag.len[1]),tmps,"ALL");
	            tmps      = Replace(Mid(source,badtag.pos[1],badtag.len[1]),"#tmpOff#","","ALL");
	            source      = Replace(source,Mid(source,badtag.pos[1],badtag.len[1]),tmps,"ALL");
	            nextStart = badTag.pos[1] + badTag.len[1];
	        }
	        else nextStart = obracket + 1;
	        obracket = find("<",source,nextStart);
	    }
	    source = Replace(source,tmpOn,hilightitem,"ALL");
	    source = Replace(source,tmpOff,endhilight,"ALL");
    } catch (Any e) {
        // do nothing
    }
    return source;
}
</cfscript>
</cfcomponent>