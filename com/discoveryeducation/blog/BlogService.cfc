<cfcomponent displayname="BlogService" hint="Service for Wordpress XML-RPC interface">
	
	<cfset variables.xmlrpc = CreateObject("component","xmlrpc") />
	
	<cffunction name="init" returntype="BlogService" access="public" output="false">
		<cfreturn this/>
	</cffunction>
	
	<cffunction name="metaWeblog_getPost" access="public" output="true" returntype="any">
		<cfargument name="postId" type="string" required="true" />
		<cfargument name="endpointUrl" type="string" required="false" default="http://dev-stemblog.redactededucation.com/xmlrpc.php" />
		<cfargument name="username" type="string" required="false" default="admin" />
		<cfargument name="password" type="string" required="false" default="ef5176c96566" />
	
		<cfset var inputArray = ArrayNew(1) />
		<cfset var requestXml = "" />
		<cfset var resultXml = "" />
		<cfset var response = StructNew() />
	
		<cfset inputArray[1] = "metaWeblog.getPost" />
		<cfset inputArray[2] = arguments.postId />	
		<cfset inputArray[3] = arguments.username />
		<cfset inputArray[4] = arguments.password />
		<cfset requestXml = variables.xmlrpc.CFML2XMLRPC(inputArray,'call') />
	<cftry>
		<cfhttp url="#arguments.endpointUrl#" method="POST" timeout="60" result="resultXml">
			<cfhttpparam name="request_body" value="#requestXml#" type="xml">
		</cfhttp>
		<cfset response = variables.xmlrpc.XMLRPC2CFML(resultXml.filecontent) />
		<cfcatch></cfcatch>
	</cftry>
	
		<cfreturn response />
	</cffunction>
	
	<cffunction name="wp_getComments" access="public" output="true" returntype="any">
		<cfargument name="blogId" type="string" required="false" default="" />
		<cfargument name="postId" type="string" required="true" />
		<cfargument name="numberOfComments" type="string" required="false" default="10" />
		<cfargument name="endpointUrl" type="string" required="false" default="http://dev-stemblog.redactededucation.com/xmlrpc.php" />
		<cfargument name="username" type="string" required="false" default="admin" />
		<cfargument name="password" type="string" required="false" default="ef5176c96566" />
	
		<cfset var inputArray = ArrayNew(1) />
		<cfset var filter = StructNew() />
		<cfset var requestXml = "" />
		<cfset var resultXml = "" />
		<cfset var response = StructNew() />
	
		<cfset filter.post_id = arguments.postId />
		<cfset filter.number = arguments.numberOfComments />
	
		<cfset inputArray[1] = "wp.getComments" />
		<cfset inputArray[2] = arguments.blogId />
		<cfset inputArray[3] = arguments.username />
		<cfset inputArray[4] = arguments.password />
		<cfset inputArray[5] = filter />
		<cfset requestXml = variables.xmlrpc.CFML2XMLRPC(inputArray,'call') />
	<cftry>
		<cfhttp url="#arguments.endpointUrl#" method="POST" timeout="60" result="resultXml">
			<cfhttpparam name="request_body" value="#requestXml#" type="xml">
		</cfhttp>
		<cfset response = variables.xmlrpc.XMLRPC2CFML(resultXml.filecontent) />
		<cfcatch></cfcatch>
	</cftry>
	
		<cfreturn response />
	</cffunction>
	
	<cffunction name="wp_newComment" access="public" output="false" returntype="any">
		<cfargument name="postId" type="string" required="true" />
		<cfargument name="author" type="string" required="false" default="default"/>
		<cfargument name="author_email" type="string" required="false" default="xyz@redacted.com" />
		<cfargument name="content" type="string" required="true" />
		<cfargument name="author_url" type="string" required="false" default="" />
		<cfargument name="endpointUrl" type="string" required="false" default="http://dev-stemblog.redactededucation.com/xmlrpc.php" />
	
		<cfset var inputArray = ArrayNew(1) />
		<cfset var comment = StructNew() />
		<cfset var requestXml = "" />
		<cfset var resultXml = "" />
		<cfset var response = StructNew() />
	
		<cfset comment.comment_parent = "" />
		<cfset comment.content = arguments.content />
		<cfset comment.author = arguments.author />
		<cfset comment.author_url = arguments.author_url />
		<cfset comment.author_email = arguments.author_email />
	
		<cfset inputArray[1] = "wp.newComment" />
		<cfset inputArray[2] = "" />
		<cfset inputArray[3] = "" />
		<cfset inputArray[4] = "" />
		<cfset inputArray[5] = arguments.postId />
		<cfset inputArray[6] = comment />
		<cfset requestXml = variables.xmlrpc.CFML2XMLRPC(inputArray,'call') />
	<cftry>
		<cfhttp url="#arguments.endpointUrl#" method="POST" timeout="60" result="resultXml">
			<cfhttpparam name="request_body" value="#requestXml#" type="xml">
		</cfhttp>
		<cfset response = variables.xmlrpc.XMLRPC2CFML(resultXml.filecontent) />
		<cfcatch></cfcatch>
	</cftry>

		<cfreturn response />
	</cffunction>
	
	<cffunction name="metaWeblog_getRecentPosts" access="public" output="true" returntype="any">
		<cfargument name="numberOfPosts" type="string" required="false" default="10" />
		<cfargument name="endpointUrl" type="string" required="false" default="http://dev-stemblog.redactededucation.com/xmlrpc.php" />
		<cfargument name="username" type="string" required="false" default="admin" />
		<cfargument name="password" type="string" required="false" default="ef5176c96566" />
	
		<cfset var inputArray = ArrayNew(1) />
		<cfset var requestXml = "" />
		<cfset var resultXml = "" />
		<cfset var response = StructNew() />
	
		<cfset inputArray[1] = "metaWeblog.getRecentPosts" />
		<cfset inputArray[2] = "" />	
		<cfset inputArray[3] = arguments.username />
		<cfset inputArray[4] = arguments.password />
		<cfset inputArray[5] = arguments.numberOfPosts />
		<cfset requestXml = variables.xmlrpc.CFML2XMLRPC(inputArray,'call') />
	<cftry>
		<cfhttp url="#arguments.endpointUrl#" method="POST" timeout="60" result="resultXml">
			<cfhttpparam name="request_body" value="#requestXml#" type="xml">
		</cfhttp>
		<cfset response = variables.xmlrpc.XMLRPC2CFML(resultXml.filecontent) />
		<cfcatch></cfcatch>
	</cftry>
	
		<cfreturn response />
	</cffunction>
	
	<cffunction name="mt_getRecentPostTitles" access="public" output="true" returntype="any">
		<cfargument name="numberOfPosts" type="string" required="false" default="5" />
		<cfargument name="endpointUrl" type="string" required="false" default="http://dev-stemblog.redactededucation.com/xmlrpc.php" />
		<cfargument name="username" type="string" required="false" default="admin" />
		<cfargument name="password" type="string" required="false" default="ef5176c96566" />
	
		<cfset var inputArray = ArrayNew(1) />
		<cfset var requestXml = "" />
		<cfset var resultXml = "" />
		<cfset var response = StructNew() />
	
		<cfset inputArray[1] = "mt.getRecentPostTitles" />
		<cfset inputArray[2] = "" />	
		<cfset inputArray[3] = arguments.username />
		<cfset inputArray[4] = arguments.password />
		<cfset inputArray[5] = arguments.numberOfPosts />
		<cfset requestXml = variables.xmlrpc.CFML2XMLRPC(inputArray,'call') />
	<cftry>
		<cfhttp url="#arguments.endpointUrl#" method="POST" timeout="60" result="resultXml">
			<cfhttpparam name="request_body" value="#requestXml#" type="xml">
		</cfhttp>
		<cfset response = variables.xmlrpc.XMLRPC2CFML(resultXml.filecontent) />
		<cfcatch><!--- do nothing for now ---></cfcatch>
	</cftry>
	
		<cfreturn response />
	</cffunction>
	
	<cffunction name="system_listMethods" access="public" output="true" returntype="any">
		<cfargument name="endpointUrl" type="string" required="false" default="http://dev-stemblog.redactededucation.com/xmlrpc.php" />
	
		<cfset var inputArray = ArrayNew(1) />
		<cfset var requestXml = "" />
		<cfset var resultXml = "" />
		<cfset var response = "" />
	
		<cfset inputArray[1] = "system.listMethods" />
		<cfset requestXml = variables.xmlrpc.CFML2XMLRPC(inputArray,'call') />
		<cfhttp url="#arguments.endpointUrl#" method="POST" timeout="60" result="resultXml">
			<cfhttpparam name="request_body" value="#requestXml#" type="xml">
		</cfhttp>
		<cfset response = variables.xmlrpc.XMLRPC2CFML(resultXml.filecontent) />
	
		<cfreturn response />
	</cffunction>
	
</cfcomponent>