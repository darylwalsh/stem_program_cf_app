<cfcomponent
	displayname="MediaFileListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="MediaFile Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setMediaFileService" access="public" returntype="void" output="false">
		<cfargument name="MediaFileService" type="model.MediaFileService" required="true" />
		<cfset variables.MediaFileService = arguments.MediaFileService />
	</cffunction>
	<cffunction name="getMediaFileService" access="public" returntype="model.MediaFileService" output="false">
		<cfreturn variables.MediaFileService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="processUploadForm" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var mediaFile = arguments.event.getArg("mediaFile") />
		
		<cfif arguments.event.getArg("fileData") NEQ "">
			<cftry>
				<cffile action="upload" filefield="fileData" destination="#ExpandPath('/assets/stem/')#"
						nameconflict="makeunique" />
				<cfif CFFILE.FileWasSaved>
					<cfset mediaFile.setGuidAssetMediaFileId(application.udf.CreateGUID()) />
					<cfset mediaFile.setStrFilePath("/assets/stem/") />
					<cfset mediaFile.setStrFileName(CFFILE.ServerFile) />
					<cfset mediaFile.setIntFileSizeBytes(CFFILE.FileSize) />
					<cfset mediaFile.setMediaFileServerUrl(request.baseUrl) />
				</cfif>
				<cfcatch type="any">
					<cfdocument format="PDF" overwrite="yes" filename="c:/errordebug.pdf">
						<cfdump var="#mediafile.getMemento()#"/>
						<cfdump var="#CFCATCH#" />
					</cfdocument>
				</cfcatch>
			</cftry>
		</cfif>
		
		<cfreturn getMediaFileService().saveMediaFile(mediaFile) />
	</cffunction>
	
	<cffunction name="getMediaFilesByAssetId" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var getMediaFilesByAssetId = "" />
		
		<cfif NOT IsValid("guid",guidAssetId)>
			<cfset guidAssetId = "00000000-0000-0000-0000-000000000000" />
		</cfif>
		
		<cfreturn getMediaFileService().getMediaFiles(guidAssetId=guidAssetId) />
	</cffunction>
	
	<cffunction name="deleteMediaFile" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetMediaFileId = arguments.event.getArg("guidAssetMediaFileId") />
		
		<cfreturn getMediaFileService().deleteMediaFile(guidAssetMediaFileId=guidAssetMediaFileId) />
	</cffunction>
	
	<cffunction name="getMediaFile" access="public" returntype="model.MediaFile" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetMediaFileId = arguments.event.getArg("guidAssetMediaFileId") />
		
		<cfreturn getMediaFileService().getMediaFile(guidAssetMediaFileId=guidAssetMediaFileId) />
	</cffunction>
</cfcomponent>