<cfcomponent
	displayname="CategoryListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Category Listener"
	depends="CategoryService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>	
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="createCategory" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var category = arguments.event.getArg("category") />
		
		<cfreturn getCategoryService().saveCategory(category) />
	</cffunction>
	
	<cffunction name="deleteCategory" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var id = arguments.event.getArg("id") />
		
		<cfreturn getCategoryService().deleteCategory(id) />
	</cffunction>
	
	<cffunction name="getCategory" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var id = arguments.event.getArg("categoryId") />
	
		<cfreturn getCategoryService().getCategory(id) />
	</cffunction>
	
	<cffunction name="getCategories" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "orderNo ASC" />
		
		<cfreturn getCategoryService().getCategorys(orderBy=orderBy) />
	</cffunction>
	
	<cffunction name="getCategoriesQuery" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "orderNo ASC" />
		
		<cfreturn getCategoryService().getCategorysQuery(orderBy=orderBy) />
	</cffunction>

</cfcomponent>