<cfcomponent
	displayname="MediaFileListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Email Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="processEmailAssetForm" access="public" returntype="void" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var title = arguments.event.getArg("title") />
		<cfset var sender = arguments.event.getArg("sender") />
		<cfset var copyToSelf = arguments.event.getArg("copyToSelf") />
		<cfset var recipients = arguments.event.getArg("recipients") />
		<cfset var yourMessage = arguments.event.getArg("yourMessage") />
		<cfset var message = "Your e-mail was sent." />
		
		<cfif copyToSelf EQ true>
			<cfset recipients = ListAppend(recipients,sender) />
		</cfif>
		
		<cftry>
			<cfmail from="#sender#" to="#recipients#" subject="help.redactededucation.com: #title#" type="html">
This page was sent to you by: #sender#
<br />
<br />
<a href="http://help.redactededucation.com/support/index.cfm?event=showAsset&guidAssetId=#guidAssetId#">#title#</a>
<br />
<br />
#yourMessage#
			</cfmail>
			<cfcatch type="any">
				<cfset message = "There was an error sending your e-mail." />
			</cfcatch>
		</cftry>
		
		<cfset arguments.event.setArg("message",message) />
	</cffunction>
</cfcomponent>