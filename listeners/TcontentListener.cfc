<cfcomponent
	displayname="TContentListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Tcontent Listener"
	depends="TcontentService,ClickService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>

	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="createTcontent" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var tcontent = arguments.event.getArg("tcontent") />
		
		<cfreturn getTcontentService().saveTcontent(tcontent) />
	</cffunction>
	
	<cffunction name="deleteTcontent" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var id = arguments.event.getArg("id") />
		
		<cfreturn getTcontentService().deleteTcontent(id) />
	</cffunction>
	
	<cffunction name="getTcontent" access="public" returntype="com.redactededucation.tcontent.Tcontent" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />	
		
		<cfset var id = arguments.event.getArg("id") />
		<cfset var c = arguments.event.getArg("c") />
		<cfset var tcontent = "" />
		<cfset var clickType = "view" />
		<cfset var contentType = "page" />
		<cfset var sponsor_user_guid = "" />
		<cfset var contentGuid = "" />
		
		<cfif id EQ "">
			<cfset id = c />
		</cfif>
		
		<cfif NOT IsValid("integer",id)>
			<cfset id = 0 />
		</cfif>
		
		<cfset tcontent = getTcontentService().getTcontent(id) />
				
		<cfif StructKeyExists(COOKIE,"SPONSOR_USER_GUID")>
			<cfset sponsor_user_guid = COOKIE.SPONSOR_USER_GUID />
		</cfif>
		
		<cfset contentGuid = tcontent.getContentId() />
		<cfset click = getClickService().createClick() />
		<cfset click.setClickType(clickType) />
		<cfset click.setContentGuid(contentGuid) />
		<cfset click.setContentType(contentType) />
		<cfset click.setCreatedBy(sponsor_user_guid) />
		<cfset click.setDtCreated(CreateODBCDateTime(Now())) />
		<cfset click.setIpCreated(cgi.remote_host) />
		<cfset click.setAgent(cgi.http_user_agent) />
		
		<cfset getClickService().saveClick(click) />
		
		<cfreturn tcontent />
	</cffunction>
	
	<cffunction name="getByAlias" access="public" returntype="com.redactededucation.tcontent.Tcontent" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />	
		
		<cfset var alias = arguments.event.getArg("p") />
		<cfset var tcontent = "" />
		<cfset var clickType = "view" />
		<cfset var contentType = "page" />
		<cfset var sponsor_user_guid = "" />
		<cfset var contentGuid = "" />
				
		<cfset tcontent = getTcontentService().getTcontentByAlias(alias) />
				
		<cfif StructKeyExists(COOKIE,"SPONSOR_USER_GUID")>
			<cfset sponsor_user_guid = COOKIE.SPONSOR_USER_GUID />
		</cfif>
		
		<cfset contentGuid = tcontent.getContentId() />
		<cfset click = getClickService().createClick() />
		<cfset click.setClickType(clickType) />
		<cfset click.setContentGuid(contentGuid) />
		<cfset click.setContentType(contentType) />
		<cfset click.setCreatedBy(sponsor_user_guid) />
		<cfset click.setDtCreated(CreateODBCDateTime(Now())) />
		<cfset click.setIpCreated(cgi.remote_host) />
		<cfset click.setAgent(cgi.http_user_agent) />
		
		<cfset getClickService().saveClick(click) />
		
		<cfreturn tcontent />
	</cffunction>
	
	<cffunction name="getHomeTcontent" access="public" returntype="com.redactededucation.tcontent.Tcontent" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var tcontents = getTcontentService().getTcontents(parentId=request.siteId) />
		
		<cfreturn tcontents[1] />
	</cffunction>
	
	<cffunction name="getTcontentsBySiteId" access="public" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var siteId = arguments.event.getArg("siteId") />
		
		<cfif siteId EQ "">
			<cfset siteId = request.siteId />
		</cfif>
		
		<cfreturn getTcontentService().getTcontentsBySiteId(siteId=siteId) />
	</cffunction>
	
	<cffunction name="getPagesBySiteId" access="public" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var siteId = arguments.event.getArg("siteId") />
		
		<cfif siteId EQ "">
			<cfset siteId = request.siteId />
		</cfif>
		
		<cfreturn getTcontentService().getPagesBySiteId(siteId=siteId) />
	</cffunction>
	
	<cffunction name="getMaxOrder" access="public" returntype="string">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var parentId = arguments.event.getArg("parentId") />
		
		<cfif NOT IsValid("guid",parentId)>
			<cfset parentId = "00000000-0000-0000-0000-000000000000" />
		</cfif>
		
		<cfreturn getTcontentService().getMaxOrder(parentId) />	
	</cffunction>

</cfcomponent>