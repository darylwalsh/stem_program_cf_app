<cfcomponent
	displayname="AssetListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Asset Listener">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<cffunction name="setAssetService" access="public" returntype="void" output="false">
		<cfargument name="assetService" type="model.AssetService" required="true" />
		<cfset variables.assetService = arguments.assetService />
	</cffunction>
	<cffunction name="getAssetService" access="public" returntype="model.AssetService" output="false">
		<cfreturn variables.assetService />
	</cffunction>
	
	<cffunction name="setTaxonomyAssetService" access="public" returntype="void" output="false">
		<cfargument name="taxonomyAssetService" type="model.TaxonomyAssetService" required="true" />
		<cfset variables.taxonomyAssetService = arguments.taxonomyAssetService />
	</cffunction>
	<cffunction name="getTaxonomyAssetService" access="public" returntype="model.TaxonomyAssetService" output="false">
		<cfreturn variables.TaxonomyAssetService />
	</cffunction>
	
	<cffunction name="setContentFeaturedService" access="public" returntype="void" output="false">
		<cfargument name="ContentFeaturedService" type="com.redactededucation.ContentFeatured.ContentFeaturedService" required="true" />
		<cfset variables.ContentFeaturedService = arguments.ContentFeaturedService />
	</cffunction>
	<cffunction name="getContentFeaturedService" access="public" returntype="com.redactededucation.ContentFeatured.ContentFeaturedService" output="false">
		<cfreturn variables.ContentFeaturedService />
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="processCreateAssetForm" access="public" returntype="void" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var asset = arguments.event.getArg("asset") />
		<cfset var taxonomyAsset = "" />
		<cfset var intOrder = 0 />
		
		<cfset asset.setGuidAssetMetaDataId(application.udf.CreateGUID()) />
		<cfset getAssetService().saveAsset(asset) />
		<cfloop index="guidTaxId" list="#asset.getGuidTaxIds()#">
			<cfset intOrder = getTaxonomyAssetService().getMaxOrder(guidTaxId) + 1 />
			<cfset taxonomyAsset = getTaxonomyAssetService().createTaxonomyAsset(guidTaxId=guidTaxId,guidAssetId=asset.getGuidAssetMetaDataId(),intOrder=intOrder) />
			<cfset getTaxonomyAssetService().saveTaxonomyAsset(taxonomyAsset) />
		</cfloop>
		
		<cfif asset.getStatus() EQ "8">
			<cfif asset.getIsOpen() EQ 1>
				<cfindex
					collection="#request.siteId#"
					action="update"
					type="custom"
					title="#asset.getTitle()#"
					key="#asset.getGuidAssetMetaDataId()#"
					body="#asset.getTitle()#,#asset.getTitle()#,#asset.getTitle_description()#,#asset.getKeywords()#"
					custom1="#asset.getTitle_description()#"
					custom2="#asset.getRegion_code()#" />
				<cfindex
					collection="#request.siteId#_public"
					action="update"
					type="custom"
					title="#asset.getTitle()#"
					key="#asset.getGuidAssetMetaDataId()#"
					body="#asset.getTitle()#,#asset.getTitle()#,#asset.getTitle_description()#,#asset.getKeywords()#"
					custom1="#asset.getTitle_description()#"
					custom2="#asset.getRegion_code()#" />
			<cfelse>
				<cfindex
					collection="#request.siteId#"
					action="update"
					type="custom"
					title="#asset.getTitle()#"
					key="#asset.getGuidAssetMetaDataId()#"
					body="#asset.getTitle()#,#asset.getTitle()#,#asset.getTitle_description()#,#asset.getKeywords()#"
					custom1="#asset.getTitle_description()#"
					custom2="#asset.getRegion_code()#" />
			</cfif>
		</cfif>
		
		<cfset arguments.event.setArg("guidAssetId",asset.getGuidAssetMetaDataId()) />
	</cffunction>
	
	<cffunction name="processEditAssetForm" access="public" returntype="void" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var asset = arguments.event.getArg("asset") />
		<cfset var taxonomyAsset = "" />
		
		<cfset getAssetService().saveAsset(asset) />
		
		<cfset getTaxonomyAssetService().deleteTaxonomyAsset(guidAssetId=guidAssetId) />
		
		<cfloop index="guidTaxId" list="#asset.getGuidTaxIds()#">
			<cfset taxonomyAsset = getTaxonomyAssetService().createTaxonomyAsset(guidTaxId=guidTaxId,guidAssetId=asset.getGuidAssetMetaDataId()) />
			<cfset getTaxonomyAssetService().saveTaxonomyAsset(taxonomyAsset) />
		</cfloop>
		
		<cfif asset.getStatus() EQ "8">
			<cfif asset.getIsOpen() EQ 1>
				<cfindex
					collection="#request.siteId#"
					action="update"
					type="custom"
					title="#asset.getTitle()#"
					key="#asset.getGuidAssetMetaDataId()#"
					body="#asset.getTitle()#,#asset.getTitle()#,#asset.getTitle_description()#,#asset.getKeywords()#"
					custom1="#asset.getTitle_description()#"
					custom2="#asset.getRegion_code()#" />
				<cfindex
					collection="#request.siteId#_public"
					action="update"
					type="custom"
					title="#asset.getTitle()#"
					key="#asset.getGuidAssetMetaDataId()#"
					body="#asset.getTitle()#,#asset.getTitle()#,#asset.getTitle_description()#,#asset.getKeywords()#"
					custom1="#asset.getTitle_description()#"
					custom2="#asset.getRegion_code()#" />
			<cfelse>
				<cfindex
					collection="#request.siteId#"
					action="update"
					type="custom"
					title="#asset.getTitle()#"
					key="#asset.getGuidAssetMetaDataId()#"
					body="#asset.getTitle()#,#asset.getTitle()#,#asset.getTitle_description()#,#asset.getKeywords()#"
					custom1="#asset.getTitle_description()#"
					custom2="#asset.getRegion_code()#" />
			</cfif>
		<cfelseif asset.getStatus() EQ "7">
				<cfindex
					collection="#request.siteId#"
					action="delete"
					key="#asset.getGuidAssetMetaDataId()#" />
				<cfindex
					collection="#request.siteId#_public"
					action="delete"
					key="#asset.getGuidAssetMetaDataId()#" />
		</cfif>
	</cffunction>
	
	<cffunction name="incrementViewCounter" access="public" returntype="void" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var asset = arguments.event.getArg("asset") />
		
		<cfif asset.getCopyright() EQ "">
			<cfset asset.setCopyright(1) />
		<cfelse>
			<cfset asset.setCopyright(asset.getCopyright() + 1) />
		</cfif>
		
		<cfset getAssetService().saveAsset(asset) />
	</cffunction>
	
	<cffunction name="getPublishedAssetsByGuidTaxId" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var status = "8" />
		<cfset var isOpen = 1 />
		<cfset var region_code = "00" />
		
		<cfif StructKeyExists(cookie,"app_user_guid") AND IsValid("guid",cookie.app_user_guid) AND cookie.app_user_guid NEQ "00000000-0000-0000-0000-000000000000">
			<cfif StructKeyExists(client,"region_code")>
				<cfset region_code = ListAppend(region_code,client.region_code) />
			</cfif>
			<cfreturn getAssetService().getAssetsByGuidTaxId(guidTaxId=guidTaxId,status=status,region_code=region_code) />
		<cfelse>
			<cfreturn getAssetService().getAssetsByGuidTaxId(guidTaxId=guidTaxId,status=status,region_code=region_code,isOpen=isOpen) />
		</cfif>
	</cffunction>
	
	<cffunction name="getAllAssetsByGuidTaxId" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var status = "7,8" />
		
		<cfreturn getAssetService().getAssetsByGuidTaxId(guidTaxId=guidTaxId,status=status) />
	</cffunction>
	
	<cffunction name="getAssetsByGuidMediaGroupId" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidMediaGroupId = arguments.event.getArg("guidMediaGroupId") />
		
		<cfreturn getAssetService().getAssetsByGuidMediaGroupId(guidMediaGroupId) />
	</cffunction>
	
	<cffunction name="getFeaturedContent" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfreturn getAssetService().getFeaturedContent() />
	</cffunction>
	
	<cffunction name="getRecentContent" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfreturn getAssetService().getRecentContent() />
	</cffunction>
	
	<cffunction name="getAssetById" access="public" returntype="model.Asset" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		
		<cfif NOT IsValid("guid",guidAssetId)>
			<cfset guidAssetId = "00000000-0000-0000-0000-000000000000" />
		</cfif>
		
		<cfreturn getAssetService().getAsset(guidAssetId) />
	</cffunction>

	<cffunction name="deleteAsset" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var success = false />
		
		<cfset success = getAssetService().deleteAsset(guidAssetId) />
		<cfset success = getTaxonomyAssetService().deleteTaxonomyAsset(guidAssetId=guidAssetId) />
		
		<cfreturn success />
	</cffunction>
	
	<cffunction name="createContentRecent" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentRecent = getContentRecentService().getContentRecent(guidAssetId=guidAssetId) />
		<cfset var intOrder = contentRecent.getIntOrder() />

		<cfif intOrder EQ "">
			<cfset contentRecent.setIntOrder(getContentRecentService().getMaxOrder() + 1) />
		</cfif>
		
		<cfreturn getContentRecentService().saveContentRecent(contentRecent) />
	</cffunction>
	
	<cffunction name="deleteContentRecent" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />

		<cfreturn getContentRecentService().deleteContentRecent(guidAssetId=guidAssetId) />
	</cffunction>
	
	<cffunction name="moveRecentOrderUp" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentRecent = getContentRecentService().getContentRecent(guidAssetId=guidAssetId) />
		
		<cfreturn getContentRecentService().moveOrderUp(contentRecent) />
	</cffunction>
	
	<cffunction name="moveRecentOrderDown" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentRecent = getContentRecentService().getContentRecent(guidAssetId=guidAssetId) />
		
		<cfreturn getContentRecentService().moveOrderDown(contentRecent) />
	</cffunction>

	<cffunction name="createContentFeatured" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentFeatured = getContentFeaturedService().getContentFeatured(guidAssetId=guidAssetId) />
		<cfset var intOrder = contentFeatured.getIntOrder() />

		<cfif intOrder EQ "">
			<cfset contentFeatured.setIntOrder(getContentFeaturedService().getLast() + 1) />
		</cfif>
		
		<cfreturn getContentFeaturedService().saveContentFeatured(contentFeatured) />
	</cffunction>
	
	<cffunction name="deleteContentFeatured" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />

		<cfreturn getContentFeaturedService().deleteContentFeatured(guidAssetId=guidAssetId) />
	</cffunction>
	
	<cffunction name="moveFeaturedOrderUp" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentFeatured = getContentFeaturedService().getContentFeatured(guidAssetId=guidAssetId) />
		
		<cfreturn getContentFeaturedService().moveOrderUp(contentFeatured) />
	</cffunction>
	
	<cffunction name="moveFeaturedOrderDown" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />
		<cfset var contentFeatured = getContentFeaturedService().getContentFeatured(guidAssetId=guidAssetId) />
		
		<cfreturn getContentFeaturedService().moveOrderDown(contentFeatured) />
	</cffunction>
	
	<cffunction name="moveNavContentOrderUp" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />		
		<cfset var navigationContent = getTaxonomyAssetService().getTaxonomyAsset(guidTaxId=guidTaxId,guidAssetId=guidAssetId) />
		
		<cfset navigationContent.setIntOrder(navigationContent.getIntOrder() - 1) />
		
		<cfreturn getTaxonomyAssetService().saveTaxonomyAsset(navigationContent) />
	</cffunction>

	<cffunction name="moveNavContentOrderDown" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var guidTaxId = arguments.event.getArg("guidTaxId") />
		<cfset var guidAssetId = arguments.event.getArg("guidAssetId") />		
		<cfset var navigationContent = getTaxonomyAssetService().getTaxonomyAsset(guidTaxId=guidTaxId,guidAssetId=guidAssetId) />
		
		<cfset navigationContent.setIntOrder(navigationContent.getIntOrder() + 1) />
		
		<cfreturn getTaxonomyAssetService().saveTaxonomyAsset(navigationContent) />
	</cffunction>	
</cfcomponent>