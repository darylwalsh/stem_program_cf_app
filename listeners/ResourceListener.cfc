<cfcomponent
	displayname="ResourceListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Resource Listener"
	depends="ResourceService,ResourceBlockService,ResourceCategoryService,ResourceTypeService,ResourceGradeService,FileService,ClickService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	
	<cffunction name="deleteResource" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var resourceId = arguments.event.getArg("resourceId") />
		<cfset var resource = getResourceService().getResource(resourceId) />
		<cfset var files = resource.getFiles() />
		<cfset var uploadPath = ExpandPath('/assets/stem/') />
		<cfset var thumbPath = ExpandPath('/assets/stem/thumbnails/') />
		
		<cfset getResourceCategoryService().deleteResourceCategories(resourceId=resourceId) />
		<cfset getResourceGradeService().deleteResourceGrades(resourceId=resourceId) />
		<cfloop index="i" from="1" to="#arrayLen(files)#">		
			<cftry>
				<cffile action="delete" file="#uploadPath##file.getFileId()#.#file.getFileExt()#" />
				<cffile action="delete" file="#thumbPath##file.getFileId()#.jpg" />
				<cfcatch><!--- do nothing if delete fails ---></cfcatch>
			</cftry>		
			<cfset getFileService().deleteFile(files[i].getFile_id()) />
		</cfloop>
		
		<cfreturn getResourceService().deleteResource(resourceId=resourceId) />	
	</cffunction>
	<cffunction name="processResourceForm" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var resource = arguments.event.getArg("resource") />
		<cfset var categoryIDs = arguments.event.getArg("categoryIDs") />
		<cfset var typeIDs = arguments.event.getArg("typeIDs") />
		<cfset var gradeIDs = arguments.event.getArg("gradeIDs") />
		
		<cfloop index="i" list="#categoryIDs#">
			<cfset resourceCategory = getResourceCategoryService().createResourceCategory(resourceId=resource.getResourceId(),categoryId=i) />
			<cfset getResourceCategoryService().saveResourceCategory(resourceCategory) />
		</cfloop>
		
		<cfloop index="i" list="#typeIDs#">
			<cfset resourceType = getResourceTypeService().createResourceType(resourceId=resource.getResourceId(),typeId=i) />
			<cfset getResourceTypeService().saveResourceType(resourceType) />
		</cfloop>
        
		<cfloop index="i" list="#gradeIDs#">
			<cfset resourceGrade = getResourceGradeService().createResourceGrade(resourceId=resource.getResourceId(),grade_id=i) />
			<cfset getResourceGradeService().saveResourceGrade(resourceGrade) />
		</cfloop>

		<cfset arguments.event.setArg("r",resource.getResourceId()) />
		<cfreturn getResourceService().saveResource(resource) />
	</cffunction>
	
	<cffunction name="processEditResourceForm" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var resource = arguments.event.getArg("resource") />
		<cfset var categoryIDs = arguments.event.getArg("categoryIDs") />
		<cfset var typeIDs = arguments.event.getArg("typeIDs") />
		<cfset var gradeIDs = arguments.event.getArg("gradeIDs") />
		<cfset var block_id = arguments.event.getArg("block_id") />
		
		<cfset getResourceCategoryService().deleteResourceCategories(resourceId=resource.getResourceId()) />		
		<cfloop index="i" list="#categoryIDs#">
			<cfset resourceCategory = getResourceCategoryService().createResourceCategory(resourceId=resource.getResourceId(),categoryId=i) />
			<cfset getResourceCategoryService().saveResourceCategory(resourceCategory) />
		</cfloop>
		
		<cfset getResourceTypeService().deleteResourceTypes(resourceId=resource.getResourceId()) />		
		<cfloop index="i" list="#typeIDs#">
			<cfset resourceType = getResourceTypeService().createResourceType(resourceId=resource.getResourceId(),typeId=i) />
			<cfset getResourceTypeService().saveResourceType(resourceType) />
		</cfloop>
        
		<cfset getResourceGradeService().deleteResourceGrades(resourceId=resource.getResourceId()) />
		<cfloop index="i" list="#gradeIDs#">
			<cfset resourceGrade = getResourceGradeService().createResourceGrade(resourceId=resource.getResourceId(),grade_id=i) />
			<cfset getResourceGradeService().saveResourceGrade(resourceGrade) />
		</cfloop>
		
		<cfset getResourceBlockService().deleteResourceBlocks(resourceId=resource.getResourceId()) />
		<cfif block_id NEQ "">
			<cfset resourceBlock = getResourceBlockService().createResourceBlock(resourceId=resource.getResourceId(),block_id=block_id) />
			<cfset getResourceBlockService().saveResourceBlock(resourceBlock) />
		</cfif>

		<cfset arguments.event.setArg("r",resource.getResourceId()) />
		<cfreturn getResourceService().saveResource(resource) />
	</cffunction>
	
	<cffunction name="getResource" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var resourceId = arguments.event.getArg("resourceId") />
		<cfset var clickType = "view" />
		<cfset var contentType = "resource" />
		<cfset var sponsor_user_guid = "" />
		<cfset var resource = "" />
		<cfset var contentGuid = "" />
		
		<cfif NOT IsValid("guid",resourceId)>
			<cfset resourceId = "00000000-0000-0000-0000-000000000000" />
		</cfif>
		
		<cfset resource = getResourceService().getResource(resourceId) />
		
		<cfif StructKeyExists(COOKIE,"SPONSOR_USER_GUID")>
			<cfset sponsor_user_guid = COOKIE.SPONSOR_USER_GUID />
		</cfif>
		
		<cfset contentGuid = resource.getResourceId() />
		<cfset click = getClickService().createClick() />
		<cfset click.setClickType(clickType) />
		<cfset click.setContentGuid(contentGuid) />
		<cfset click.setContentType(contentType) />
		<cfset click.setCreatedBy(sponsor_user_guid) />
		<cfset click.setDtCreated(CreateODBCDateTime(Now())) />
		<cfset click.setIpCreated(cgi.remote_host) />
		<cfset click.setAgent(cgi.http_user_agent) />
		
		<cfset getClickService().saveClick(click) />
		
		<cfreturn resource />
	</cffunction>
	
	<cffunction name="getRecentResources" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "dtCreated DESC" />
		<cfset var type = "resource" />
		
		<cfreturn getResourceService().getResources(siteId=request.siteId,orderby=orderBy,numberOfResults=5,numberOfFiles=1,type=type) />
	</cffunction>
	
	<cffunction name="getResourcesBySiteId" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "dtCreated DESC" />
		<cfset var siteId = arguments.event.getArg("siteId") />
		
		<cfif siteId EQ "">
			<cfset siteId = request.siteId />
		</cfif>
		
		<cfreturn getResourceService().getResources(siteId=siteId,orderby=orderBy,numberOfResults=2000) />
	</cffunction>
	
	<cffunction name="getMostViewedResources" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "views DESC" />
		<cfset var type = "resource" />
		
		<cfreturn getResourceService().getResources(siteId=request.siteId,orderby=orderBy,numberOfResults=5,type=type) />
	</cffunction>
	
	<cffunction name="getResourcesByCategoryId" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var categoryId = arguments.event.getArg("categoryId") />
		<cfset var orderBy = "dtCreated DESC" />
		<cfset var siteId = arguments.event.getArg("siteId") />
		<cfset var type = "resource" />
		
		<cfif siteId EQ "">
			<cfset siteId = request.siteId />
		</cfif>		
		<cfif arguments.event.getArg("orderBy") EQ "views">
			<cfset orderBy = "views DESC" />
		</cfif>
		
		<cfreturn getResourceService().getResourcesByCategoryIdQuery(categoryId,siteId,type,orderBy) />
	</cffunction>
	
	<cffunction name="getResourcesByTypeId" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var typeId = arguments.event.getArg("typeId") />
		<cfset var orderBy = "dtCreated DESC" />
		<cfset var siteId = arguments.event.getArg("siteId") />
		<cfset var type = "resource" />
		
		<cfif siteId EQ "">
			<cfset siteId = request.siteId />
		</cfif>		
		<cfif arguments.event.getArg("orderBy") EQ "views">
			<cfset orderBy = "views DESC" />
		</cfif>
		
		<cfreturn getResourceService().getResourcesByTypeIdQuery(typeId,siteId,type,orderBy) />
	</cffunction>
    
	<cffunction name="getResourcesByGradeId" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var grade_id = arguments.event.getArg("g") />
		<cfset var orderBy = "dtCreated DESC" />
		<cfset var siteId = arguments.event.getArg("siteId") />
		<cfset var type = "resource" />
		
		<cfif siteId EQ "">
			<cfset siteId = request.siteId />
		</cfif>		
		<cfif arguments.event.getArg("orderBy") EQ "views">
			<cfset orderBy = "views DESC" />
		</cfif>
		
		<cfreturn getResourceService().getResourcesByGradeIdQuery(grade_id,siteId,type,orderBy) />
	</cffunction>
	
	<cffunction name="getResourcesByUser" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var app_user_guid = arguments.event.getArg("u") />
		<cfset var orderBy = "dtCreated DESC" />
		
		<cfif arguments.event.getArg("orderBy") EQ "views">
			<cfset orderBy = "views DESC" />
		</cfif>
		
		<cfreturn getResourceService().getResourcesQuery(createdBy=app_user_guid,orderBy=orderBy) />
	</cffunction>
	
	<cffunction name="incrementViews" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var resource = arguments.event.getArg("resource") />
		
		<cfif resource.getViews() EQ "">
			<cfset resource.setViews(1) />
		<cfelse>
			<cfset resource.setViews(resource.getViews() + 1) />
		</cfif>
		
		<cfreturn getResourceService().saveResource(resource) />
	</cffunction>
</cfcomponent>