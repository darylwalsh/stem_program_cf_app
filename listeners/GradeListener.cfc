<cfcomponent
	displayname="GradeListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Grade Listener"
	depends="GradeService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>	
	
	<!---
	PUBLIC FUNCTIONS
	--->	
	<cffunction name="getGrades" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "orderNo ASC" />
		
		<cfreturn getGradeService().getGrades(orderBy=orderBy) />
	</cffunction>
</cfcomponent>