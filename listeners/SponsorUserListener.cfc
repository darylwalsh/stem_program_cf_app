<cfcomponent
	displayname="SponsorUserListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Sponsor User Listener"
	depends="SponsorUserService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->	
	<cffunction name="getSponsorUsers" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "last_name ASC, first_name ASC" />
		<cfset var sponsor_guid = arguments.event.getArg("sponsor_guid") />
	
		<cfreturn getSponsorUserService().getSponsorUsers(sponsor_guid=sponsor_guid,orderBy=orderBy) />
	</cffunction>
	<cffunction name="getSponsorUser" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var sponsor_user_guid = arguments.event.getArg("guid") />
		
		<cfreturn getSponsorUserService().getSponsorUser(sponsor_user_guid) />
	</cffunction>
</cfcomponent>