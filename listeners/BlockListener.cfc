<cfcomponent
	displayname="BlockListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Block Listener"
	depends="BlockService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->	
	<cffunction name="processBlockForm" access="public" returntype="string" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var block = arguments.event.getArg("block") />
		
		<cfreturn getBlockService().saveBlock(block) />
	</cffunction>
	<cffunction name="getBlocks" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />	
	
		<cfreturn getBlockService().getBlocks() />
	</cffunction>
	<cffunction name="getBlockById" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />	
	
		<cfset var block_id = arguments.event.getArg("bid") />
		
		<cfif NOT IsNumeric(block_id)>
			<cfset block_id = -1 />
		</cfif>
		
		<cfreturn getBlockService().getBlock(block_id) />
	</cffunction>
</cfcomponent>