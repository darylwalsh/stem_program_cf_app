<cfcomponent
	displayname="FileListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="File Listener"
	depends="ClickService,FileService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="processImageUploadForm" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var path = arguments.event.getArg("path") />
		<cfset var success = true />
		
		<cfif arguments.event.getArg("fileData") NEQ "">
			<cftry>
				<cffile action="upload" filefield="fileData" destination="#ExpandPath('#path#')#"
						nameconflict="overwrite" />
				<cfcatch type="any">
					<cfset success = false />
					<cfthrow type="application" message="#CFCATCH.Message#" detail="#CFCATCH.Detail#" />
				</cfcatch>
			</cftry>
		</cfif>
		
		<cfreturn success />
	</cffunction>
	
	<cffunction name="processResourceUploadForm" access="public" returntype="string" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var file = arguments.event.getArg("file") />
		<cfset var success = false />
		<cfset var uploadPath = ExpandPath('/assets/stem/') />
		<cfset var fileGuid = application.udf.CreateGUID() />
		<cfset var keepGoing = true />
		<cfset var counter = 1 />
		
		<cfif NOT DirectoryExists(uploadPath)>
		   <cfdirectory action = "create" directory = "#uploadPath#" />
		</cfif>
		
		<cfif arguments.event.getArg("fileData") NEQ "">
			<cftry>
				<cffile action="upload" filefield="fileData" destination="#uploadPath#"
						nameconflict="makeunique" />
				<cfif CFFILE.FileWasSaved>
					<cfset file.setFile_id(0) />
					<cfset file.setFileId(fileGuid) />
					<cfset file.setFilename(CFFILE.ServerFile) />
					<cfset file.setFileSize(CFFILE.FileSize) />
					<cfset file.setFileExt(CFFILE.ServerFileExt) />
					<cfset file.setContentType(getContentType(CFFILE.ServerFileExt)) />
					<cfset file.setDtCreated(CreateODBCDateTime(Now())) />
					<cfset file.setIpCreated(CGI.REMOTE_ADDR) />
					<cfset file.setThumbnail(application.udf.createThumbnail("#CFFILE.ServerDirectory#/#CFFILE.ServerFile#",file.getFileId())) />
					<cfset success = getFileService().saveFile(file) />					

					<cfloop condition="keepGoing">
						<cfif fileExists("#CFFILE.ServerDirectory#/#CFFILE.ServerFile#")>
							<cffile action="rename" source="#CFFILE.ServerDirectory#/#CFFILE.ServerFile#" destination = "#CFFILE.ServerDirectory#/#fileGuid#.#CFFILE.ServerFileExt#" />
							<cfset keepGoing = false />
						<cfelse>
							<cfset counter++ />
						</cfif>
					</cfloop>				
				</cfif>
				<cfcatch type="any">
					
					<cflog text="#SERIALIZEJSON(CFCATCH)# #SERIALIZEJSON(file.getMemento())# #SERIALIZEJSON(form)#" file="stem_exception" type="error"/>

				</cfcatch>
			</cftry>
		</cfif>
		
		<cfif success EQ true>
			<cfreturn file.getFilename() />
		<cfelse>
			<cfreturn "nofile.txt" />
		</cfif>
	</cffunction>

	<cffunction name="getFilesByResourceId" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var resourceId = arguments.event.getArg("resourceId") />
		
		<cfreturn getFileService().getFiles(contentId=resourceId) />
	</cffunction>
	
	<cffunction name="getRecentFiles" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "dtCreated DESC" />
		
		<cfreturn getFileService().getFiles(orderBy=orderBy) />	
	</cffunction>
	
	<cffunction name="deleteFile" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var file_id = arguments.event.getArg("f") />
		<cfset var file = getFileService().getFile(file_id) />
		<cfset var uploadDirectory = ExpandPath('/assets/stem/') />
		<cfset var thumbDirectory = ExpandPath('/assets/stem/thumbnails/') />
		<cfset var flvDirectory = ExpandPath('/assets/stem/flvs/') />
		
		<cftry>
			<cffile action="delete" file="#uploadDirectory##file.getFileId()#.#file.getFileExt()#" />
			<cffile action="delete" file="#thumbDirectory##file.getFileId()#.jpg" />
			<cffile action="delete" file="#flvDirectory##file.getFileId()#.flv" />
			<cfcatch><!--- do nothing if delete fails ---></cfcatch>
		</cftry>
		
		<cfreturn getFileService().deleteFile(file_id) />	
	</cffunction>
	
	<cffunction name="getFile" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var file_id = arguments.event.getArg("f") />
		<cfset var file = "" />
		<cfset var contentGuid = "" />
		<cfset var click = "" />
		<cfset var sponsor_user_guid = "" />
		<cfset var clickType = "download" />
		<cfset var contentType = "file" />
		
		<cfif NOT IsValid("integer",file_id)>
			<cfset file_id = 0 />
		</cfif>
		
		<cfif arguments.event.isArgDefined("ct")>
			<cfset clickType = arguments.event.getArg("ct") />
		</cfif>
		
		<cfif StructKeyExists(COOKIE,"SPONSOR_USER_GUID")>
			<cfset sponsor_user_guid = COOKIE.SPONSOR_USER_GUID />
		</cfif>
		
		<cfset file = getFileService().getFile(file_id) />
		
		<cfset contentGuid = file.getFileId() />
		<cfset click = getClickService().createClick() />
		<cfset click.setClickType(clickType) />
		<cfset click.setContentGuid(contentGuid) />
		<cfset click.setContentType(contentType) />
		<cfset click.setCreatedBy(sponsor_user_guid) />
		<cfset click.setDtCreated(CreateODBCDateTime(Now())) />
		<cfset click.setIpCreated(cgi.remote_host) />
		<cfset click.setAgent(cgi.http_user_agent) />
		
		<cfset getClickService().saveClick(click) />
		
		<cfreturn file />
	</cffunction>

	<!--- PRIVATE FUNCTIONS --->
	<cffunction name="getContentType" access="private" returntype="string" output="false">
		<cfargument name="fileExt" type="string" required="true" />
		
		<cfset variables.contentType = "application/unknown" />
		
		<cfswitch expression="#arguments.fileExt#">
			<cfcase value="asf,asx">
				<cfset variables.contentType = "video/x-ms-asf" />
			</cfcase>
			<cfcase value="avi">
				<cfset variables.contentType = "video/x-msvideo" />
			</cfcase>
			<cfcase value="doc">
				<cfset variables.contentType = "application/msword" />
			</cfcase>
			<cfcase value="exe">
				<cfset variables.contentType = "application/octet-stream" />
			</cfcase>
			<cfcase value="gif">
				<cfset variables.contentType = "image/gif" />
			</cfcase>
			<cfcase value="jpg,jpeg">
				<cfset variables.contentType = "image/jpg" />
			</cfcase>
			<cfcase value="mp3">
				<cfset variables.contentType = "audio/mpeg" />
			</cfcase>
			<cfcase value="mov">
				<cfset variables.contentType = "video/quicktime" />
			</cfcase>
			<cfcase value="mp4">
				<cfset variables.contentType = "video/mp4" />
			</cfcase>
			<cfcase value="mpe,mpg,mpeg">
				<cfset variables.contentType = "video/mpeg" />
			</cfcase>
			<cfcase value="pdf">
				<cfset variables.contentType = "application/pdf" />
			</cfcase>
			<cfcase value="png">
				<cfset variables.contentType = "image/png" />
			</cfcase>
			<cfcase value="ppt">
				<cfset variables.contentType = "application/vnd.ms-powerpoint" />
			</cfcase>
			<cfcase value="txt">
				<cfset variables.contentType = "text/plain" />
			</cfcase>
			<cfcase value="wav">
				<cfset variables.contentType = "audio/x-wav" />
			</cfcase>
			<cfcase value="wmv">
				<cfset variables.contentType = "video/x-ms-wmv" />
			</cfcase>
			<cfcase value="xls">
				<cfset variables.contentType = "application/vnd.ms-excel" />
			</cfcase>
			<cfcase value="zip">
				<cfset variables.contentType = "application/zip" />
			</cfcase>
			<cfdefaultcase>
				<cfset variables.contentType = "application/unknown" />
			</cfdefaultcase>
		</cfswitch>

		<cfreturn variables.contentType />
	</cffunction>
</cfcomponent>