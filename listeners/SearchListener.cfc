<cfcomponent displayname="SearchListener" output="false" extends="MachII.framework.Listener" hint="Search Listener for Help">
	
	<cffunction name="configure" access="public" output="false" returntype="void">
		<!--- do nothing for now --->
	</cffunction>
	
	<!--- dependencies --->
	<cffunction name="setSearchService" access="public" output="false" returntype="void">
		<cfargument name="SearchService" type="com.redactededucation.Search.SearchService" required="true" />
		<cfset variables.SearchService = arguments.SearchService />
	</cffunction>
	<cffunction name="getSearchService" access="public" output="false" returntype="com.redactededucation.Search.SearchService">
		<cfreturn variables.SearchService />
	</cffunction>
	
	<!--- listener methods --->
	<cffunction name="processSearchForm" access="public" output="false" returntype="query">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var qSearchResults = QueryNew("AUTHOR,CATEGORY,CATEGORYTREE,CONTEXT,CUSTOM1,CUSTOM2,CUSTOM3,CUSTOM4,KEY,RANK,RECORDSSEARCHED,SCORE,SIZE,SUMMARY,TITLE,TYPE,URL") />
		<cfset var criteria = LCase(Left(HtmlEditFormat(Trim(event.getArg("criteria"))),255)) />
		<cfset var dtCreated = CreateODBCDateTime(Now()) />
		<cfset var search = getSearchService().createSearch(terms=criteria,siteId=request.siteId,dtCreated=dtCreated) />
		
		<cfif Len(search.getTerms())>
			<cfset getSearchService().saveSearch(search) />
		</cfif>
		
		<cfset qSearchResults = getSearchService().search(criteria) />

		<cfreturn qSearchResults />
	</cffunction>
	
	<cffunction name="indexResources" access="public" output="false" returntype="any">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var collectionName = arguments.event.getArg("collectionName") />
		
		<cfreturn getSearchService().index(collectionName=collectionName) />
	</cffunction>
</cfcomponent>