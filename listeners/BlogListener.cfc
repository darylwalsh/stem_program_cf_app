<cfcomponent
	displayname="BlogListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Blog Listener"
	depends="BlogService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
		
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="getRecentPostTitles" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var postTitles = StructNew() />
		<cfset var endpointUrl = "#application.env.stemblog_url#/xmlrpc.php" />
		
		<cfset postTitles = getBlogService().mt_getRecentPostTitles(endpointUrl=endpointUrl,username=application.env.stemblog_username,password=application.env.stemblog_password) />
		
		<cfreturn postTitles />
	</cffunction>
	
	<cffunction name="getRecentPosts" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var numberOfPosts = arguments.event.getArg("n") />
		<cfset var recentPosts = StructNew() />
		<cfset var endpointUrl = "#application.env.stemblog_url#/xmlrpc.php" />
		
		<cfif numberOfPosts EQ "">
			<cfset numberOfPosts = 3 />
		</cfif>
		
		<cfset recentPosts = getBlogService().metaWeblog_getRecentPosts(numberOfPosts=numberOfPosts,endpointUrl=endpointUrl,username=application.env.stemblog_username,password=application.env.stemblog_password) />
		
		<cfreturn recentPosts />
	</cffunction>
	
	<cffunction name="getBlogPost" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var postId = arguments.event.getArg("postId") />
		<cfset var endpointUrl = "#application.env.stemblog_url#/xmlrpc.php" />
		
		<cfreturn getBlogService().metaWeblog_getPost(postId=postId,endpointUrl=endpointUrl,username=application.env.stemblog_username,password=application.env.stemblog_password) />
	</cffunction>
	
	<cffunction name="getBlogPostComments" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var postId = arguments.event.getArg("postId") />
		<cfset var endpointUrl = "#application.env.stemblog_url#/xmlrpc.php" />
		
		<cfreturn getBlogService().wp_getComments(postId=postId,endpointUrl=endpointUrl,username=application.env.stemblog_username,password=application.env.stemblog_password) />
	</cffunction>
	
	<cffunction name="processCommentForm" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var postId = arguments.event.getArg("postId") />
		<cfset var author = arguments.event.getArg("author") />
		<cfset var author_email = arguments.event.getArg("author_email") />
		<cfset var content = arguments.event.getArg("content") />
		<cfset var endpointUrl = "#application.env.stemblog_url#/xmlrpc.php" />
		
		<cfreturn getBlogService().wp_newComment(postId=postId,author=author,author_email=author_email,content=content,endpointUrl=endpointUrl) />
	</cffunction>

</cfcomponent>