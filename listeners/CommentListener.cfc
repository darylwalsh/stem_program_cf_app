<cfcomponent
	displayname="CommentListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Comment Listener"
	depends="CommentService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->	
	<cffunction name="processCommentForm" access="public" returntype="string" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var comment = arguments.event.getArg("comment") />
		<cfset var cffp = CreateObject("component","cfformprotect.cffpVerify").init() />
		
		<!--- now we can test the form submission --->
		<cfif cffp.testSubmission(form)>
			<!--- The submission has passed the form test.  Place processing here --->
			<cftry><cfmail to="#application.comment_email#" from="#comment.getEmail()#" subject="Comment added to #request.siteName#">
Comment added:	#DateFormat(comment.getDtCreated(),"short")# #TimeFormat(comment.getDtCreated(),"short")#
Comment made by:	#comment.getAuthor()#
IP of Poster:	#comment.getIpCreated()#

#comment.getBody()#
			</cfmail><cfcatch></cfcatch></cftry>
			<cfreturn getCommentService().saveComment(comment) />
		<cfelse>
	       <!--- The test failed.  Take appropriate failure action here. --->
	       <cfreturn false />
		</cfif>
	</cffunction>
	
	<cffunction name="deleteComment" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var comment_id = arguments.event.getArg("cid") />
		
		<cfreturn getCommentService().deleteComment(comment_id) />
	</cffunction>
</cfcomponent>