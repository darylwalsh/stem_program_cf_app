<cfcomponent displayname="SecurityListener" output="false" extends="MachII.framework.Listener" hint="Security Listener for Help">
	
	<cffunction name="configure" access="public" output="false" returntype="void">
		<!--- do nothing for now --->
	</cffunction>
	
	<!--- dependencies --->
	
	<!--- listener methods --->
	<cffunction name="processLoginForm" access="public" output="false" returntype="void" hint="Validates a login attempt">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var anonGuid = "00000000-0000-0000-0000-000000000000" />
		<cfset var username = event.getArg("username") />
		<cfset var password = event.getArg("password") />
		<cfset var objFindUser = "" />
		<cfset var exitEvent = "failed" />
		<cfset var message = "Your username/password combination was incorrect.<br>Please try again.<br>&nbsp;" />
			
		<cfset objFindUser = CreateObject("component","commonCFC.GlobalServicesExt").serviceAppUserLogonRequest(anonGuid,username,password,anonGuid) />
	    
		<cfif StructKeyExists(objFindUser, "app_user_guid") AND IsValid("guid", objFindUser.app_user_guid)>
		    <cfquery name="qWebsites" datasource="#getProperty('sDsn')#">
				SELECT
					wu.siteId
				FROM	cms_websiteUsers wu
				WHERE	wu.app_user_guid = <cfqueryparam value="#objFindUser.app_user_guid#" />
			</cfquery>
			
			<cfif qWebsites.RecordCount>
				<cfset StructClear(client) />
				<cfset client.hitcount = 1 />
				<cfloop collection="#objFindUser#" item="key">
					<cfset "client.#key#" = StructFind(objFindUser, key) />
				</cfloop>

    			<cfcookie name="app_user_guid" value="#objFindUser.APP_USER_GUID#"  />
        		<cfcookie name="FIRST_NAME" value="#objFindUser.FIRST_NAME#"  />
	    	    <cfcookie name="LAST_NAME" value="#objFindUser.LAST_NAME#"  />
		        <cfcookie name="lstProducts" value="#objFindUser.productList#"  />
                <cfcookie name="lstSubProducts" value="#objFindUser.lstSubProducts#"  />
				<cfcookie name="blnAuthenticatedStem" value="true"  />

				<cfset exitEvent = "success" />
				<cfset message = "" />			
			<cfelse>
				<cfset message = "You do not have access to this site.<br />" />
			</cfif>
		</cfif>
		
		<cfset arguments.event.setArg("message", message) />
		<cfset announceEvent(exitEvent, arguments.event.getArgs()) />
	</cffunction>
</cfcomponent>