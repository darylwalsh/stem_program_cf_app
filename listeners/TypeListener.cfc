<cfcomponent
	displayname="TypeListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Type Listener"
	depends="TypeService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>	
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="createType" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var type = arguments.event.getArg("type") />
		
		<cfreturn getTypeService().saveType(type) />
	</cffunction>
	
	<cffunction name="deleteType" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var id = arguments.event.getArg("id") />
		
		<cfreturn getTypeService().deleteType(id) />
	</cffunction>
	
	<cffunction name="getType" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var id = arguments.event.getArg("typeId") />
	
		<cfreturn getTypeService().getType(id) />
	</cffunction>
	
	<cffunction name="getTypes" access="public" returntype="array" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "orderNo ASC" />
		
		<cfreturn getTypeService().getTypes(orderBy=orderBy) />
	</cffunction>
	
	<cffunction name="getTypesQuery" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var orderBy = "orderNo ASC" />
		
		<cfreturn getTypeService().getTypesQuery(orderBy=orderBy) />
	</cffunction>

</cfcomponent>