<cfcomponent
	displayname="RatingListener"
	extends="MachII.framework.Listener"
	output="false"
	hint="Rating Listener"
	depends="RatingService">

	<!---
	PROPERTIES
	--->

	<!---
	CONFIGURATION / INITIALIZATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the listener.">
		<!--- Put custom configuration for this listener here. --->
	</cffunction>
	
	<!---
	PUBLIC FUNCTIONS
	--->	
	<cffunction name="saveRating" access="public" returntype="boolean" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var rating = arguments.event.getArg("rating") />
		
		<cfset rating.setDtCreated(CreateODBCDateTime(Now())) />
		
		<cfreturn getRatingService().saveRating(rating) />
	</cffunction>
	
	<cffunction name="getRating" access="public" returntype="any" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var contentId = arguments.event.getArg("resourceId") />
		<cfset var app_user_guid = "00000000-0000-0000-0000-000000000000" />
		
		<cfif structKeyExists(cookie,"app_user_guid")>
			<cfset app_user_guid = cookie.app_user_guid />
		<cfelseif structKeyExists(cookie,"tempGuid")>
			<cfset app_user_guid = cookie.tempGuid />
		</cfif>
		
		<cfreturn getRatingService().getRating(contentId=contentId,app_user_guid=app_user_guid) />
	</cffunction>
	
	<cffunction name="getAverageRating" access="public" returntype="query" output="false">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		
		<cfset var contentId = arguments.event.getArg("resourceId") />
		
		<cfif NOT IsValid("guid",contentId)>
			<cfset contentId = "00000000-0000-0000-0000-000000000000" />
		</cfif>
		
		<cfreturn getRatingService().getAverageRating(contentId=contentId) />
	</cffunction>
</cfcomponent>