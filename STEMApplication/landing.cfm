<cflocation addtoken="no" url="/index.cfm" />


<cfif StructKeyExists(url, "programInstitute") AND application.cfcs.register.isSTEMUserLoggedIn()>
	<cfset form.applicationGUID = applicationGUID />
	
	<cfparam name="url.programInstitute" default="" />
	<cfparam name="url.programSTARsOR" default="" />
	<cfparam name="url.programSTARsPN" default="" />
	<cfparam name="url.programPreference1" default="" />
	<cfparam name="url.programPreference2" default="" />
	
	<cfparam name="form.programInstitute" default="#url.programInstitute#" />
	<cfparam name="form.programSTARsOR" default="#url.programSTARsOR#" />
	<cfparam name="form.programSTARsPN" default="#url.programSTARsPN#" />
	<cfparam name="form.programPreference1" default="#url.programPreference1#" />
	<cfparam name="form.programPreference2" default="#url.programPreference2#" />
	
	<cfset form.fieldNames = "applicationGUID,programInstitute,programSTARsOR,programSTARsPN,programPreference1,programPreference2" />
</cfif>


<cfif StructKeyExists(form, "fieldNames")>
	
	
	<cfquery name="savePage" datasource="Sponsorships">
		UPDATE STEMApplications
		SET	lastUpdated = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
			<cfif StructKeyExists(form, "programInstitute") AND form.programInstitute eq "1">
				programInstitute = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
			<cfelse>
				programInstitute = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
			</cfif>
			
			<cfif StructKeyExists(form, "programSTARsOR") AND form.programSTARsOR eq "1">
				programSTARsOR = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
			<cfelse>
				programSTARsOR = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
			</cfif>
			
			<cfif StructKeyExists(form, "programSTARsPN") AND form.programSTARsPN eq "1">
				programSTARsPN = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
			<cfelse>
				programSTARsPN = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
			</cfif>
			
			<cfif StructKeyExists(form, "programPreference1") AND form.programPreference1 neq "">
				programPreference1 = <cfqueryparam value="#form.programPreference1#" cfsqltype="cf_sql_varchar" />,
			<cfelse>
				programPreference1 = <cfqueryparam value="" cfsqltype="cf_sql_varchar" />,
			</cfif>
			<cfif StructKeyExists(form, "programPreference2") AND form.programPreference2 neq "">
				programPreference2 = <cfqueryparam value="#form.programPreference2#" cfsqltype="cf_sql_varchar" />
			<cfelse>
				programPreference2 = <cfqueryparam value="" cfsqltype="cf_sql_varchar" />
			</cfif>
		WHERE applicationGUID = <cfqueryparam value="#form.applicationGUID#" cfsqltype="cf_sql_varchar" />
	</cfquery>
	
	<cfset qString = ReplaceNoCase(cgi.query_string, "&landing=1", "", "all") />
	
	<cflocation url="#cgi.script_name#?#qString#" addtoken="false" />
</cfif>


<cfset event.setArg("pageTitle", request.siteName & " - Institute Application") />


<cfquery name="getType" datasource="Sponsorships">
	SELECT programInstitute, programSTARsOR, programSTARsPN, programPreference1, programPreference2
	FROM STEMApplications (nolock)
	WHERE applicationGUID = <cfqueryparam value="#applicationGUID#" cfsqltype="cf_sql_varchar">
</cfquery>

<div id="content-single" class="app-landing">
        	<h2 style="text-transform:none;">NOW OPEN: Siemens Summer of Learning Application</h2>
			<em><b style="font-size:1.3em;">A Siemens Foundation Initiative
				<br /><div style='color:#cc4444; font-size:12px; margin:5px 0px 0px 0px;'>Applications due February 5, 2013</div></b></em>
            <br />
            <p style="margin-bottom:20px;">The Siemens Summer of Learning Application is now open! The Siemens STEM Academy offers two one-of-a-kind, all-expenses-paid professional development opportunities that provide unique experiences to middle and high school teachers from across the country looking to bolster STEM learning and gain pragmatic skills that they can translate back into the classroom.
<br><br>
Access the application below to apply to the STARs and Siemens STEM Institute programs. You can apply to one or both programs by completing this application only once. If you apply to both programs, you must complete the application requirements for each program before submitting your application. Please do not submit separate applications for each program.
<br><br>
Apply today for a one-of-a-kind STEM-u-lating summer professional development experience.
</p>

<h3>PROGRAM INFORMATION</h3>
<div class="box institute">
<h4>SIEMENS STEM INSTITUTE</h4>
<p class="image-left">Do you want to learn about new digital tools and technologies that can bolster achievement in the classroom? Do you want to engage with top STEM leaders and scientists and experience behind-the-scenes access of national STEM institutions? The Siemens STEM Institute selects 50 middle and high school educators to attend an all-expenses-paid week at <a href="http://dsc.redacted.com/" target="_blank">redacted's world headquarters</a> outside of Washington, D.C. Fellows will learn about hands-on STEM integration in the classroom, take field trips to leading institutions to observe real-world STEM applications, and network with STEM leaders and peers.</p>
<ul>
<li><a href="/index.cfm?event=showContent&c=36">Program Overview</a></li>
<li><a href="/index.cfm?event=showContent&id=42&c=36">Eligibility Criteria</a></li>
<li><a href="/index.cfm?event=showContent&c=66">2012 Photos and Itinerary</a></li>
<li>Apply Below</li>
</ul>
</div>
<div class="box stars last-box">
<h4>Siemens Teachers as Researchers (STARs)</h4>
<p class="image-left">Have you ever wanted to be part of a scientific research team? Have you ever wanted to bring the excitement of authentic research into the classroom? Offered at <a href="http://www.ornl.gov/" target="_blank">Oak Ridge National Laboratory</a> and 
<a href="http://www.pnnl.gov/" target="_blank">Pacific Northwest National Laboratory</a>, the STARs program gives middle and high school STEM educators the opportunity to spend two weeks, all-expenses-paid, engaging with top scientists and researchers on short-term mentored research projects at one of the U.S. Department of Energy national laboratories.</p>
<ul>
<li><a href="/index.cfm?event=showContent&c=38">Program Overview</a></li>
<li><a href="/index.cfm?event=showContent&id=39&c=38">Eligibility Criteria</a></li>
<li><a href="/index.cfm?event=showContent&c=67">2012 Photos and Itinerary</a></li>
<li>Apply Below</li>
</ul>
</div>



<hr />
<h3>ACCESS APPLICATION<span class="required-legend">Fields with an asterisk(<em>*</em>) are required</span></h3>

<style type="text/css">
	div.row label, div.row select {
		margin-left:20px;
	}
	
	div.row input.submit {
		cursor:pointer;
		margin-left:170px;
	}
</style>

<script type="text/javascript" src="/js/register.js"></script>
<script type="text/javascript">
	function ValidateLandingForm (aform) 
	{
		var numbChecked = 0;
	
		if (aform.programInstitute.checked) {
			numbChecked++;
		}
		if (aform.programSTARsOR.checked) {
			numbChecked++;
		}
		if (aform.programSTARsPN.checked) {
			numbChecked++;
		}
		
		if (numbChecked == 0) {
			alert("Please select at least 1 program.");
			return false;
		}
		
		if (numbChecked >= 2 && aform.programPreference1.value == ""){
			alert("Please select a First Preference.");
			return false;
		}
		
		if (numbChecked >= 3 && aform.programPreference2.value == ""){
			alert("Please select a Second Preference.");
			return false;
		}
		
		
		<cfif StructKeyExists(request, "loginNeeded") AND request.loginNeeded eq true>
			<cfoutput>
			var processURL = "#BuildUrl('xhr.loginForm')#&r=contest";
			</cfoutput>
			if (aform.programInstitute.checked) {
				processURL += "&programInstitute=" + aform.programInstitute.value;
			}
			if (aform.programSTARsOR.checked) {
				processURL += "&programSTARsOR=" + aform.programSTARsOR.value;
			}
			if (aform.programSTARsPN.checked) {
				processURL += "&programSTARsPN=" + aform.programSTARsPN.value;
			}
			if (aform.programPreference1.value != "") {
				processURL += "&programPreference1=" + aform.programPreference1.value;
			}
			if (aform.programPreference2.value != "") {
				processURL += "&programPreference2=" + aform.programPreference2.value;
			}
			
			deGlobalWin({path:processURL, closebtn:false,background:'none'});
			
			return false;
		</cfif>
		
		return true;
	}
</script>

<!---
<cfset length = ListLen(cgi.server_name,".") />
<cfset aDomain = ".#ListGetAt(cgi.server_name,length-2,".")#.#ListGetAt(cgi.server_name,length-1,".")#.#ListGetAt(cgi.server_name,length,".")#" />
<cfoutput>#aDomain#</cfoutput>
--->


<cfoutput>
<form method="post" id="programForm" onsubmit="return ValidateLandingForm(this);">
	
	<div class="row">
		<div style="margin-bottom:5px;">
			<em style="color:red;">*</em> <strong>To begin the application process, please select the program(s) you are applying for:</strong>
		</div>
		
		<label>
			<input type="checkbox" name="programInstitute" value="1" onclick="CheckLandingAppType(this.form);" <cfif getType.programInstitute eq 1>checked="checked"</cfif> /> Institute
		</label>
		<label>
			<input type="checkbox" name="programSTARsOR" value="1" onclick="CheckLandingAppType(this.form);" <cfif getType.programSTARsOR eq 1>checked="checked"</cfif> /> STARs - Oak Ridge
		</label>
		<label>
			<input type="checkbox" name="programSTARsPN" value="1" onclick="CheckLandingAppType(this.form);" <cfif getType.programSTARsPN eq 1>checked="checked"</cfif> /> STARs - Pacific Northwest
		</label>
	</div>
	
	
	<div class="row" id="firstPreference" <cfif getType.programPreference1 eq "">style="display:none;"</cfif>>
		<div style="margin-bottom:5px;">
			<em style="color:red;">*</em> <strong>First Preference</strong>
		</div>
		
		<select name="programPreference1" onchange="CheckLandingPreference2(this.form);">
			<option value="">- Select One -</option>
			<cfif getType.programInstitute eq 1>
				<option value="Institute" <cfif getType.programPreference1 eq "Institute">selected="selected"</cfif>>Institute</option>
			</cfif>
			<cfif getType.programSTARsOR eq 1>
				<option value="STARs - Oak Ridge" <cfif getType.programPreference1 eq "STARs - Oak Ridge">selected="selected"</cfif>>STARs - Oak Ridge</option>
			</cfif>
			<cfif getType.programSTARsPN eq 1>
				<option value="STARs - Pacific Northwest" <cfif getType.programPreference1 eq "STARs - Pacific Northwest">selected="selected"</cfif>>STARs - Pacific Northwest</option>
			</cfif>
		</select>
	</div>
	
	<div class="row" id="secondPreference" <cfif getType.programPreference2 eq "">style="display:none;"</cfif>>
		<div style="margin-bottom:5px;">
			<em style="color:red;">*</em> <strong>Second Preference</strong>
		</div>
		
		<select name="programPreference2">
			<option value="">- Select One -</option>
			<cfif getType.programInstitute eq 1>
				<option value="Institute">Institute</option>
			</cfif>
			<cfif getType.programSTARsOR eq 1>
				<option value="STARs - Oak Ridge" <cfif getType.programPreference2 eq "STARs - Oak Ridge">selected="selected"</cfif>>STARs - Oak Ridge</option>
			</cfif>
			<cfif getType.programSTARsPN eq 1>
				<option value="STARs - Pacific Northwest" <cfif getType.programPreference2 eq "STARs - Pacific Northwest">selected="selected"</cfif>>STARs - Pacific Northwest</option>
			</cfif>
		</select>
	</div>
	
	<div class="row">
		<input type="hidden" name="applicationGUID" value="#applicationGUID#" />
		<input type="submit" class="submit" value="Access Application" />
	</div>
</form>
</cfoutput>


