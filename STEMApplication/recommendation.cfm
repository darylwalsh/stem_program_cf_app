
<cfset displayPage = false />
<cfif StructKeyExists(url, "applicationGUID") AND IsValid("guid", url.applicationGUID)>
	<cfquery name="stemApplication" datasource="Sponsorships">
		SELECT applicationGUID
		FROM STEMApplications (nolock)
		WHERE applicationGUID = <cfqueryparam value="#url.applicationGUID#" cfsqltype="cf_sql_varchar" />
	</cfquery>
	
	<cfif stemApplication.RecordCount gt 0>
		<cfset displayPage = true />
	</cfif>
</cfif>

<cfif StructKeyExists(form, "firstName")>
	<cfset application.cfcs.register.saveRecommendationSheet(form) />
	<cflocation url="#cgi.script_name#?#cgi.query_string#" addtoken="false" />
</cfif>


<cfif displayPage>

	<!--- remove me later --->
	<cftry>
	
	
	<cfquery name="stemApplicantName" datasource="Sponsorships">
		SELECT sr.firstName, sr.lastName
		FROM STEMApplications AS sa (nolock)
			INNER JOIN STEMRegistrations AS sr (nolock)
			ON sr.registrationGUID = sa.registrationGUID
		WHERE sa.applicationGUID = <cfqueryparam value="#url.applicationGUID#" cfsqltype="cf_sql_varchar" />
	</cfquery>
	
	<cfset registrationSheets = application.cfcs.register.getRecommendationSheet(url.applicationGUID) />
	
	<cfquery name="statesUS" dbtype="query">
		SELECT * FROM Application.qCountriesStates
		WHERE COUNTRY_CODE = <cfqueryparam value="US" cfsqltype="cf_sql_varchar" />
    	ORDER BY STATENAME
	</cfquery>
	
	<style type="text/css">
		div.recommendation {
			margin:0px 0px 0px 30px;
			padding:30px 0px 0px 0px;
		}
		
		div.recommendation div.podTitleLeft {
			background-image: url('/cfcs/tabbedForm/images/podTitleLeft.png');
			background-repeat:no-repeat;
			float:left;
			height:29px;
			width:2px;
		}
		
		div.recommendation div.podTitleCenter {
			background-image: url('/cfcs/tabbedForm/images/podTitleCenter.png');
			background-repeat:repeat-x;
			color:#2A6262;
			float:left;
			font-size:1.2em;
			font-weight:bold;
			height:29px;
			line-height:29px;
			width:596px;
		}
		
		div.recommendation div.podTitleCenter div.padding {
			margin-left:10px;
		}
		
		div.recommendation div.podTitleRight {
			background-image: url('/cfcs/tabbedForm/images/podTitleRight.png');
			background-repeat:no-repeat;
			float:left;
			height:29px;
			width:2px;
		}
		
		div.recommendation div.contentWrap {
			background-color:#ffffff;
			border:1px solid #cccccc;
			width:598px;
		}
		
		div.recommendation div.contentWrap div.content {
			margin:15px;
		}
		
		div.recommendation div.row {
			margin:10px 0px 0px 0px;
		}
		
		div.recommendation div.row input, div.recommendation div.row select {
			width:240px;
		}
		
		div.recommendation div.row div.questionFull {
			margin:25px 0px 0px 20px;
			width:560px;
		}
		
		div.recommendation div.row textarea {
			height:140px;
			margin:5px 0px 0px 0px;
			width:510px;
		}
		
		div.recommendation div.row div.question {
			float:left;
			font-weight:bold;
			margin-left:20px;
			width:120px;
		}
		
		div.recommendation div.row div.answer {
			float:right;
			padding:0px;
			text-align:right;
			margin-right:150px;
			width:260px;
		}
		
		div.recommendation div.clear {
			border:0px solid #ff0000;
			clear:both;
			font-size:0px;
			height:0px;
			line-height:0px;
			margin:0px 0px 0px 0px;
			padding:0px 0px 0px 0px;
			width:99%;
		}
		
		div.recommendation div.rule {
			background-color:#cccccc;
			font-size:0px;
			height:1px;
			line-height:0px; 
			margin:10px 0px; 
			padding:0px;
			width:560px;
		}
		
		div.recommendation table.categories {
			border:1px solid #888888; 
			margin-top:8px; 
		}
		
		div.recommendation table.categories td {
			padding:2px;
		}
		
		
		div.recommendation table.categories tr.head, div.recommendation table.categories tr.head td {
			background-color:#eeeeee;
			font-weight:bold;
		}
		
		div.recommendation table.categories tr.row {
			
		}
		
		div.recommendation table.categories tr.row td {
			border-top:1px solid #888888;
		}
		
		div.recommendation table.categories tr.row td input {
			width:12px;
		}
		
		
	</style>
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.1.0/prototype.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js"></script>
	
	<script type="text/javascript" src="/js/simpleValidation.js"></script>
	<script type="text/javascript" src="/js/register.js"></script>
	
	<div class="recommendation">
		<div class="podTitleLeft">&nbsp;</div>
		<div class="podTitleCenter">
			<div class="padding">
				2013 Siemens Summer of Learning Application Recommendation Sheet
			</div>
		</div>
		<div class="podTitleRight">&nbsp;</div>
		<!---<div class="clear">&nbsp;</div>--->
		
		<div class="contentWrap">
			<div class="clear">&nbsp;</div>
			<cfoutput>
			<form method="post" name="recommendationForm" id="recommendationForm">
				<div class="content">
					<cfif registrationSheets.RecordCount gt 0>
						<div style="color:##2222cc; font-size:15px; font-weight:bold; margin-bottom:15px; margin-top:10px;">
							Thank You!  Your recommendation sheet was completed and received on<br />
							 #DateFormat(registrationSheets.created, "m/d/yyyy")# #TimeFormat(registrationSheets.created, "h:mm tt")#.
						</div>
					<cfelse>
						<div>
							Thank you for offering to serve as a recommender. Siemens STEM Academy professional development programs 
							enable educators to have hands-on learning experiences that they can bring back into the classroom immediately 
							to boost STEM learning. It is with support like yours that teachers can truly impact STEM achievement. Please 
							complete the sheet below and submit.  We thank you for your support of educators.
						</div>
						
						<div class="rule">&nbsp;</div>
						
						<div style="font-size:14px;">
							<strong>Recommendation for:</strong> #stemApplicantName.firstName# #stemApplicantName.lastName#
						</div>
						
						<div class="rule">&nbsp;</div>
						
						<div class="row" style="display:none;" id="tblErrMessageTable">
							All fields are required.<br />Required fields have incomplete or incorrect data provided.<br />Please make corrections and submit again.
						</div>
						
						<div class="row">
							<div class="question" id="firstNameLabel">
								First Name
							</div>
							<div class="answer">
								<input type="text" name="firstName" id="firstName" value="" onchange="hideErrorRecommendation(this.name);" maxlength="100" />
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="lastNameLabel">
								Last Name
							</div>
							<div class="answer">
								<input type="text" name="lastName" id="lastName" value="" onchange="hideErrorRecommendation(this.name);" maxlength="100" />
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="titleLabel">
								Position Title
							</div>
							<div class="answer">
								<select name="title" id="title" onchange="hideErrorRecommendation(this.name);"
									style="width:245px;">
									<option value=""> - Choose One -</option>
									<option value="Administrator">Administrator</option>
									<option value="Curriculum Specialist">Curriculum Specialist</option>
									<option value="Department Head">Department Head</option>
									<option value="Higher Education">Higher Education</option>
									<option value="Information Technology Coordinator">Information Technology Coordinator</option>
									<option value="Media Library Specialist">Media Library Specialist</option>
									<option value="Pre-service Student">Pre-service Student</option>
									<option value="Principal">Principal</option>
									<option value="Teacher">Teacher</option>
									<option value="Technology Administrator">Technology Administrator</option>
									<option value="Technology Support">Technology Support </option>
									<option value="Other">Other</option>
								</select>
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="institutionLabel">
								Institution/Employer
							</div>
							<div class="answer">
								<input type="text" name="institution" id="institution" value="" onchange="hideErrorRecommendation(this.name);" maxlength="255" />
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="streetLabel">
								Address
							</div>
							<div class="answer">
								<input type="text" name="street" id="street" value="" onchange="hideErrorRecommendation(this.name);" maxlength="255" />
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="street2Label">
								Address 2
							</div>
							<div class="answer">
								<input type="text" name="street2" id="street2" value="" maxlength="255" />
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="cityLabel">
								 City
							</div>
							<div class="answer">
								<input type="text" name="city" id="city" value="" onchange="hideErrorRecommendation(this.name);" maxlength="255" />
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="stateLabel">
								 State
							</div>
							<div class="answer">
								<select name="state" id="state" onchange="hideErrorRecommendation(this.name);"
									style="width:245px;">
									<option value=""> - Select a State - </option>
									<cfloop query="statesUS">
										<option value="#statesUS.stateCode#">#statesUS.stateName#</option>
									</cfloop>
									
								</select>
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="zipLabel">
								 Zip Code
							</div>
							<div class="answer">
								<input type="text" name="zip" id="zip" value="" onchange="hideErrorRecommendation(this.name);" maxlength="5" />
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="phoneLabel">
								Phone Number
							</div>
							<div class="answer">
								<input type="text" name="phone" id="phone" value="" onchange="hideErrorRecommendation(this.name);" maxlength="20" />
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="question" id="emailLabel">
								E-mail Address
							</div>
							<div class="answer">
								<input type="text" name="email" id="email" value="" onchange="hideErrorRecommendation(this.name);" maxlength="255" />
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						
						<div class="row">
							<div class="questionFull">
								<strong id="capacityKnowApplicantLabel">
									In what capacity have you known the applicant and for how long? (max 100 words)
								</strong>
								<textarea name="capacityKnowApplicant" id="capacityKnowApplicant" onchange="hideErrorRecommendation(this.name);" style="height:100px;"></textarea>
								<div class="clear">&nbsp;</div>
							</div>
						</div>
						
						<div class="row">
							<div class="questionFull">
								<strong id="describeApplicantLabel">
									Please provide substantive comments about the applicant's character, abilities, and educator skills which describe 
									why the applicant would be a good candidate (max 500 words). 
								</strong>
								<textarea name="describeApplicant" id="describeApplicant" onchange="hideErrorRecommendation(this.name);"></textarea>
								<div class="clear">&nbsp;</div>
							</div>
						</div>
						
						<div class="row">
							<div class="questionFull">
								Please rate the applicant on the following in comparison to other students or faculty:
								
								<table border="0" cellpadding="0" cellspacing="0" class="categories">
									<tr class="head">
										<td style="width:160px;">
											Category
										</td>
										<td style="text-align:center; width:60px;">
											Superior
										</td>
										<td style="text-align:center; width:60px;">
											Above<br />
											Average
										</td>
										<td style="text-align:center; width:60px;">
											Average
										</td>
										<td style="text-align:center; width:60px;">
											Below<br />
											Average
										</td>
										<td style="text-align:center; width:130px;">
											Do not have Adequate<br />
											Knowledge to Evaluate
										</td>
									</tr>
									<tr class="row">
										<td id="categoryClassroomInstructionalSkillsLabel">
											Classroom Instructional Skills
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryClassroomInstructionalSkills" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryClassroomInstructionalSkills" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryClassroomInstructionalSkills" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryClassroomInstructionalSkills" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryClassroomInstructionalSkills" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									<tr class="row">
										<td id="categorySubjectMatterKnowledgeLabel">
											Subject Matter Knowledge
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categorySubjectMatterKnowledge" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categorySubjectMatterKnowledge" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categorySubjectMatterKnowledge" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categorySubjectMatterKnowledge" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categorySubjectMatterKnowledge" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									<tr class="row">
										<td id="categoryInitiativeAndSelfRelianceLabel">
											Initiative and Self-Reliance
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInitiativeAndSelfReliance" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInitiativeAndSelfReliance" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInitiativeAndSelfReliance" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInitiativeAndSelfReliance" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInitiativeAndSelfReliance" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									<tr class="row">
										<td id="categoryCreativityLabel">
											Creativity
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryCreativity" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryCreativity" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryCreativity" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryCreativity" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryCreativity" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									<tr class="row">
										<td id="categoryIntegrityLabel">
											Integrity
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryIntegrity" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryIntegrity" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryIntegrity" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryIntegrity" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryIntegrity" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									<tr class="row">
										<td id="categoryLeadershipSkillsLabel">
											Leadership Skills
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryLeadershipSkills" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryLeadershipSkills" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryLeadershipSkills" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryLeadershipSkills" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryLeadershipSkills" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									<tr class="row">
										<td id="categoryInterpersonalSkillsLabel">
											Interpersonal Skills
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInterpersonalSkills" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInterpersonalSkills" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInterpersonalSkills" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInterpersonalSkills" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryInterpersonalSkills" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									<tr class="row">
										<td id="categoryOralCommunicationSkillsLabel">
											Oral Communication Skills
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryOralCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryOralCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryOralCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryOralCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryOralCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									<tr class="row">
										<td id="categoryWrittenCommunicationSkillsLabel">
											Written Communication Skills
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryWrittenCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryWrittenCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryWrittenCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryWrittenCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryWrittenCommunicationSkills" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									<tr class="row">
										<td id="categoryTeamworkSkillsLabel">
											Teamwork/Collaboration Skills
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryTeamworkSkills" onchange="hideErrorRecommendation(this.name);" value="superior" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryTeamworkSkills" onchange="hideErrorRecommendation(this.name);" value="aboveAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryTeamworkSkills" onchange="hideErrorRecommendation(this.name);" value="average" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryTeamworkSkills" onchange="hideErrorRecommendation(this.name);" value="belowAverage" />
										</td>
										<td style="text-align:center;">
											<input type="radio" name="categoryTeamworkSkills" onchange="hideErrorRecommendation(this.name);" value="N/A" />
										</td>
									</tr>
									
								</table>
							</div>
						</div>
						
						
						
						<div class="row">
							<input type="hidden" name="applicationGUID" value="#url.applicationGUID#" />
							<input id="btnSubmit" class="sprite" type="button" title="Submit" value="" style="border:0px; cursor:pointer; margin-left:240px;" onclick="validateRecommendationForm();" />
							<div id="processingForm" style="display:none; margin:10px 0px 10px 225px;"><img src="/images/working.gif" /></div>
						</div>
					</cfif>
				</div>
			</form>
			</cfoutput>
		</div>
	</div>
		
		
		<!--- remove me later --->
		<cfcatch>
			<cfdump var="#cfcatch#" />
		</cfcatch>
	</cftry>
<cfelse>
	<cflocation url="/" addtoken="false" />
</cfif>
