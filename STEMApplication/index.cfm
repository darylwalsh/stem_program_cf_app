
<cfset event.setArg("pageTitle","#request.siteName# - Institute Application") />

<cfif application.cfcs.register.isSTEMUserLoggedIn(true)>
	<cfif not StructKeyExists(application, "tabbedForm") OR StructKeyExists(url, "reinit") OR StructKeyExists(url, "reload") OR StructKeyExists(url, "restart")>
		<cfset application.tabbedForm = CreateObject("component", "cfcs.tabbedForm.tabbedForm").Init() />
	</cfif>

	<cfset applicationGUID = application.tabbedForm.GetApplicationGUID(client.STEMRegistrationGUID) />

	<cfif not IsValid("guid", applicationGUID)>
		<cfquery name="insertRecord" datasource="Sponsorships">
			DECLARE @applicationGUID  varchar(50) = NewId();

			INSERT INTO STEMApplications (applicationGUID, registrationGUID)
			VALUES (
						@applicationGUID,
						<cfqueryparam value="#client.STEMRegistrationGUID#" cfsqltype="cf_sql_varchar" />
					);

			SELECT @applicationGUID AS applicationGUID;
		</cfquery>
		<cfset applicationGUID = insertRecord.applicationGUID />
	</cfif>

	<cfquery name="checkLandingCompleted" datasource="Sponsorships">
		SELECT programInstitute, programSTARsOR, programSTARsPN, programPreference1, programPreference2
		FROM STEMApplications (nolock)
		WHERE applicationGUID = <cfqueryparam value="#applicationGUID#" cfsqltype="cf_sql_varchar" />
	</cfquery>

	<cfset pageLandingCompleted = true />
	<cfif checkLandingCompleted.RecordCount lte 0 OR (checkLandingCompleted.programInstitute neq 1 AND checkLandingCompleted.programSTARsOR neq 1 AND checkLandingCompleted.programSTARsPN neq 1)>
		<cfset pageLandingCompleted = false />
	</cfif>

	<cfif pageLandingCompleted eq false OR StructKeyExists(url, "landing")>
		<cfset request.loginNeeded = false />
		<cfinclude template="/STEMApplication/landing.cfm" />
	<cfelse>
		<cfoutput>#application.tabbedForm.BuildForm(applicationGUID=applicationGUID)#</cfoutput>
	</cfif>

<cfelse>

	<cfsavecontent variable="js">
		<script type="text/javascript" src="/js/loginPopupScripts.js"></script>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.1.0/prototype.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js"></script>
		<script type="text/javascript" src="/js/stemacademy.js"></script>
		<script type="text/javascript" src="/js/deLightBox.min.js"></script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />


	<!--- Need to login --->

	<cfif not StructKeyExists(url, "landing")>
		<cfoutput>
		<script type="text/javascript">
			document.observe("dom:loaded", function() {
				deGlobalWin({path:'#BuildUrl('xhr.loginForm')#', closebtn:false,background:'none'});
			});
		</script>

		<div align="center" style="font-weight:bold; font-size:14px; padding:100px 30px 300px 30px;">
			Please <a href="javascript:void(0);" onclick="deGlobalWin({path:'#BuildUrl('xhr.loginForm')#', closebtn:false,background:'none'});">Login</a> before proceeding.
		</div>
		</cfoutput>
	</cfif>

	<cfset applicationGUID = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" />
	<cfset request.loginNeeded = true />
	<cfinclude template="/STEMApplication/landing.cfm" />

</cfif>

