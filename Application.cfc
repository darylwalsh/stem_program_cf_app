<cfcomponent displayname="Application" extends="MachII.mach-ii" output="false">

	<!---
	PROPERTIES - APPLICATION SPECIFIC
	--->
	<cfset this.name = "cms_#Hash(GetCurrentTemplatePath())#" />
	<cfset this.loginStorage = "session" />
	<cfset this.sessionManagement = true />
	<cfset this.clientManagement = true />
	<cfset this.setClientCookies = true />
	<cfset this.setDomainCookies = false />
	<cfset this.sessionTimeout = CreateTimeSpan(0,1,0,0) />
	<cfset this.applicationTimeout = CreateTimeSpan(0,0,0,10) />
	
	<!---
	PROPERTIES - MACH-II SPECIFIC
	--->
	<!---Set the path to the application's mach-ii.xml file --->
	<cfset MACHII_CONFIG_PATH = ExpandPath("./config/mach-ii.xml") />
	<!--- Set the configuration mode (when to reinit): -1=never, 0=dynamic, 1=always --->
	<cfset MACHII_CONFIG_MODE = -1 />
	<!--- Set the app key for sub-applications within a single cf-application. --->
	<cfset MACHII_APP_KEY =  this.name />
	<!--- Whether or not to validate the configuration XML before parsing. Default to false. --->
	<cfset MACHII_VALIDATE_XML = FALSE />
	
	<!---
	PROPERTIES - APPLICATION SPECIFIC
	--->
	<!--- Increases the request timeout when the framework (re)loads --->
	<cfset REQUEST_TIMEOUT = 240 />

	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="onApplicationStart" returnType="void" output="false" hint="Only runs when the App is started.">
	<cfif ListLen(cgi.SERVER_NAME,".") GTE 2>
         <cfset var hostname = ListGetAt(cgi.SERVER_NAME,2,".") />
    
    <cfelse> 
         <cfset var hostname = cgi.SERVER_NAME />
    </cfif>
	
		<cfif FindNoCase("dev",hostname)>
			<cfset propertyFile = "config/.properties_dev" />
		<cfelseif FindNoCase("stage",hostname)>
			<cfset propertyFile = "config/.properties_stage" />
		<cfelseif FindNoCase("local",hostname) gt 0>
			<cfset propertyFile = "config/.properties_local" />
		<cfelse>
			<cfset var propertyFile = "config/.properties_prod" />
		</cfif>
		
	
		<cfset var globalPropertyFile = "/commonCFC/.properties" />
		<cfset var key = "" />
		
		<cfsetting requesttimeout="#REQUEST_TIMEOUT#" />
		
		
		<cfset application.udf = createObject("component","com.redactededucation.udf.udf").init() />
		<cfset application.env = StructNew() />
		<cfset application.properties = application.udf.getProperties(propertyFile) />
		<cfset application.globalProperties = application.udf.getProperties(globalPropertyFile) />
		<cfset application.siteId = "EE92B302-1F29-3B68-E636-F81E6B003479" />
		<cfset application.startTime = Now() />
		
		<cfloop collection="#application.globalProperties#" item="key">
			<cfset application[key] = StructFind(application.globalProperties,key) />
		</cfloop>
		<cfscript>
			application.env.huburl=TRIM(StructFind(application.properties,"huburl"));			
			application.strEnvironmentType=TRIM(StructFind(application.properties,"strenvironmenttype"));
			application.env.strenvironment=TRIM(StructFind(application.properties,"strenvironment"));
			application.env.searchurl=TRIM(StructFind(application.properties,"searchurl"));
			application.env.strregistrationsite=TRIM(StructFind(application.properties,"strregistrationsite"));
			application.env.toolsurl=TRIM(StructFind(application.properties,"toolsurl"));
			application.env.studentcenterurl=TRIM(StructFind(application.properties,"studentcenterurl"));
			application.env.playerurl=TRIM(StructFind(application.properties,"playerurl"));
			application.env.streaming_url=TRIM(StructFind(application.properties,"streaming_url"));
			application.env.science_url=TRIM(StructFind(application.properties,"science_url"));
			application.env.science_es_url=TRIM(StructFind(application.properties,"science_es_url"));
			application.env.health_url=TRIM(StructFind(application.properties,"health_url"));
			application.env.mediashare_url=TRIM(StructFind(application.properties,"mediashare_url"));
			application.env.help_url=TRIM(StructFind(application.properties,"help_url"));
			application.env.staticurl=TRIM(StructFind(application.properties,"staticurl"));
			application.env.stem_url=TRIM(StructFind(application.properties,"stem_url"));
			application.env.stemblog_url=TRIM(StructFind(application.properties,"stemblog_url"));
			application.env.stemblog_username=TRIM(StructFind(application.properties,"stemblog_username"));
			application.env.stemblog_password=TRIM(StructFind(application.properties,"stemblog_password"));
			application.env.toolsurl=TRIM(StructFind(application.properties,"toolsurl"));
			application.env.classroommanagerurl=TRIM(StructFind(application.properties,"classroommanagerurl"));
			application.env.cfcpath=TRIM(StructFind(application.properties,"cfcpath"));
			application.env.wsurl=TRIM(StructFind(application.properties,"wsurl"));				
			application.env.service_usage_rights=TRIM(StructFind(application.properties,"service_usage_rights"));
			application.endeca_hostIP=Trim(StructFind(application.properties,"endeca_hostIP"));
			application.endeca_hostPort=Trim(StructFind(application.properties,"endeca_hostPort"));
			application.comment_email=Trim(StructFind(application.properties,"comment_email"));
		</cfscript>
		
		<cfset application.dbinfo = StructNew() />
		<cfset application.dbinfo.strGlobalDatasource = "edu_sec" />
		<cfset application.dbinfo.strSponsorshipsDatasource = "Sponsorships" />
        <cfset application.dbinfo.strAPADatasource = "AllProductAssets" />
		
		<cfset application.SPONSOR_GUID = "88888888-8888-8888-0004-888888888888" />
		<cfset application.SPONSOR_CODE = "STEM" />
		<cfset application.SPONSOR_CODE_LAB_COAT = "STEM_TOTE_BAG_2010" /> <!--- STEM_LAB_COAT (2009), STEM_TOTE_BAG_2010 --->
		
		<cfset application.cfcs = StructNew() />
		<cfset application.cfcs.register = CreateObject("component", "cfcs.register") />
		
		<cfinvoke component="cfcs.countrystates" method="getCountries" returnvariable="application.qCountries" />
		<cfinvoke component="cfcs.countrystates" method="getCountriesStates" returnvariable="application.qCountriesStates" />
		<cfinvoke component="cfcs.register" method="getReminderQuestions" returnvariable="application.qReminderQuestions">
			<cfinvokeargument name="SECURITY_QUESTION_GUID" value="" />
		</cfinvoke>
		<cfinvoke component="cfcs.register" method="getUserTitles" returnvariable="application.qUserTitles" />
			
		<cfset loadFramework() />
	</cffunction>
	
	
	<cffunction name="onRequestStart" returnType="void" output="true" hint="Run at the start of a page request.">
		<cfargument name="targetPage" type="string" required="true" />
		
		<cfset var length = 0 />
		
		<!--- Temporarily override the default config mode --->
		<cfif StructKeyExists(url, "reinit") OR StructKeyExists(url, "reload") OR StructKeyExists(url, "restart")>
			<cflock scope="Application" type="exclusive" timeout="60">
				<cfset onApplicationStart() />
			</cflock>
		</cfif>
		
		<cfset request.contestYear = 2013 />
		<cfset request.contestStart=CreateODBCDateTime("2012-12-13 00:00:00")><!--- 2012-12-13 --->
		<cfset request.contestEnd=CreateODBCDateTime("2013-02-05 23:59:59")>
		
		<cfset request.domain = cgi.server_name />
		
		<cfscript>
			request.wsUrl = application.env.wsurl;
			request.hubUrl = application.env.huburl;
			request.baseUrl = application.env.stem_url;
			request.searchUrl = application.env.searchurl;
			request.playerUrl = application.env.playerurl;
			request.strEnvironmentType = application.strenvironmenttype;
		</cfscript>
		
		<cfif StructKeyExists(url, "logout")>		



		    <cfif ListLen(cgi.SERVER_NAME,".") GTE 2>
		         <cfset var hostname = ListGetAt(cgi.SERVER_NAME,2,".") />
		    
		    <cfelse> 
		         <cfset var hostname = cgi.SERVER_NAME />
		    </cfif>
	   
	        <cfif FindNoCase("dev",hostname)>
	            <cfset strCookieDomain = ".dev.redactededucation.com" />
	        <cfelseif FindNoCase("stage",hostname)>
	            <cfset strCookieDomain = ".stage.redactededucation.com" />
	        <cfelseif FindNoCase("local",hostname) gt 0>
	            <cfset strCookieDomain = ".local.redactededucation.com" />
	        <cfelse>
	            <cfset strCookieDomain = ".redactededucation.com" />
	        </cfif>
	
			<!--- A more potent way of clearing out cookies - 09.27.07 - RIS --->
			<!---
			<cfloop collection="#cookie#" item="strCookieItem">
				<cfcookie name="#strCookieItem#" value="" expires="NOW"  />
				<cfcookie name="#strCookieItem#" value="" expires="NOW" domain="#strCookieDomain#" />
			</cfloop>
		    --->
		    <cfloop collection="#COOKIE#" item="strCookieItem">
				<cfcookie name="#strCookieItem#" value="" expires="NOW"  />
				<cfcookie name="#strCookieItem#" value="" expires="NOW" domain="#strCookieDomain#" />
			</cfloop>
		    
			<cfset application.cfcs.register.logoutUser() />
		    <cfset StructClear(client) />
			<cfset client.hitcount = 1 />
		    <cfset StructClear(session) />
		    
		    <cflocation url="/index.cfm" addtoken="false" />
		</cfif>
		
		<!--- Request Scope Variable Defaults --->
		<cfset request.self = "index.cfm" />
		<cfset request.server_name = cgi.server_name />
		<cfif FindNoCase("local-",request.server_name)>
			<cfset request.server_name = Replace(request.server_name,"local-","") />
		<cfelseif FindNoCase("local.",request.server_name)>
			<cfset request.server_name = Replace(request.server_name,"local.","") />
		<cfelseif FindNoCase("dev-",request.server_name)>
			<cfset request.server_name = Replace(request.server_name,"dev-","") />
		<cfelseif FindNoCase("dev.",request.server_name)>
			<cfset request.server_name = Replace(request.server_name,"dev.","") />
		<cfelseif FindNoCase("stage-",request.server_name)>
			<cfset request.server_name = Replace(request.server_name,"stage-","") />
		<cfelseif FindNoCase("stage.",request.server_name)>
			<cfset request.server_name = Replace(request.server_name,"stage.","") />
		<cfelseif FindNoCase("dev-",request.server_name)>
			<cfset request.server_name = Replace(request.server_name,"dev-","") />
		<cfelseif FindNoCase("stage-",request.server_name)>
			<cfset request.server_name = Replace(request.server_name,"stage-","") />
		</cfif>
		<cfquery name="request.qSite" datasource="#getProperty('sDsn')#">
			SELECT TOP 1
				w.name,
				w.siteId,
				w.url
			FROM cms_websites w
			WHERE 0=0
			AND		url = <cfqueryparam value="#request.server_name#" />
		</cfquery>
		<cfif request.qSite.RecordCount EQ 1>
			<cfset request.siteId = request.qSite.siteId />
			<cfset request.siteName = request.qSite.name />
		<cfelse>
			<cfset request.siteId = "EE92B302-1F29-3B68-E636-F81E6B003479" />
			<cfset request.siteName = "Siemens STEM" />
		</cfif>
		
		<!--- toggle debug display --->
		<cfif StructKeyExists(url,"debug")>
			<cfif StructKeyExists(client,"stem_debug") AND client.stem_debug EQ true>
				<cfset client.stem_debug = false />
			<cfelse>
				<cfset client.stem_debug = true />
			</cfif>
		</cfif>
		<!--- Only handle requests from the base index.cfm file --->
		<cfif arguments.targetPage EQ "/index.cfm">
			<cfset handleRequest() />
		</cfif>
	</cffunction>
	
	
</cfcomponent>