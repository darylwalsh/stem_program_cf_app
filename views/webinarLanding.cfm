<cfsilent>
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset event.setArg("pageTitle","#request.siteName# - Webinars") />
</cfsilent>
<style>
.otherbtn {
	width:120px;
	height:20px;
	background-color:black;
	color:white;
	display:inline-block;
	padding:10px;
	text-align:center;
	vertical-align:middle;
	font-size:14px;
	text-decoration:none;
	border: 1px solid #43C5D2;
}
</style>
<cfoutput>
<div id="content-left">

	<h2>Webinars</h2>
	<h3>Upcoming STEM Connect Webinars</h3>




	<!-- <div class="webinar-entry-wrap">
    	<h4>NORTH AMERICA: Explore Glacier National Park</h4>
      <h6>Thursday, May 30, 2013 at 1:00pm EST</h6>
	  <em>For Classrooms</em> <br />
      <p>Join us as we discover some of North America's splendor in Glacier National Park's pristine forests, alpine meadows, rugged mountains and spectacular lakes. Your class will accompany park service specialists for an insider's tour of Glacier to see the animals and landmark features unique to this area; consider why stewardship is so critical to our National Parks; and learn how you can make a difference to ensure these natural resources are available for generations to enjoy.</p>
<a href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=c8122abd%2Dd161%2Dbe6c%2D883c%2D4a414ac3ffdc" 
	class="otherbtn" target="_blank"><nobr><b>Learn More</b></nobr></a>

	</div> -->
	
	<div class="webinar-entry-wrap">
    	<h4>SHARING WITH THE SIEMENS STEM FELLOWS</h4>
      <h6>Tuesday, June 18, 2013 at 7:00pm EST</h6>
	  <em>For Educators</em> <br />
      <p>Join us as we celebrate our 2012 Siemens STEM Institute Fellows and prepare for our 2013 cohort of Fellows at this summer's STEM Institute. You'll hear a variety of ideas on how to bring awareness and impact STEM achievement in your classroom, school, and district as last year's Fellows share their projects and their journeys. Best of all, you'll be sure to learn tips, tools, and resources to spruce up your classroom as you plan for the next school year.</p>
<a href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=c8216b75%2Dacfb%2Dc0d3%2D2be7%2D4d25fc25e898" 
	class="otherbtn" target="_blank"><nobr><b>Learn More</b></nobr></a>

	</div>


	<!--- <a href="/index.cfm?event=showResource&resourceId=45ba104a%2D1321%2D0c71%2D3c2e%2D06fb2e015230" 
	class="otherbtn" target="_blank"><nobr><b>Access Webinar</b></nobr></a>
	--->

</div><!--content-left-->

<!--- ================================================================================================= --->

<div id="content-right">		
	<div style="height:64px;"></div>
	<!---  --->
	<!--- <a href="/index.cfm?event=showPage&p=STEM-archived-webinars&c=30"> --->
	<a href="/index.cfm?event=showWebinarArchive"><img src="/img/webinararchive.png" style="border:none;"></a>

	<h4 class="sidebar-light">Recent Webinars</h4>

	<ul class="linklist">
		<li><a href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=c8122abd%2Dd161%2Dbe6c%2D883c%2D4a414ac3ffdc "
target="_blank">NORTH AMERICA: Explore Glacier National Park</a></li>
	
      	<li><a href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=d0377355%2D1438%2Dbbfa%2D6006%2D804bb20a5afa"
target="_blank">Cache In, Trash Out for Earth Day Week: Geocaching 101</a></li>
          	<li><a href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=d0377355%2D1438%2Dbbfa%2D6006%2D804bb20a5afa" target="_blank">How Podcasting Works</a></li>
    	<li><a href="/index.cfm?event=showResource&resourceId=281b0c2f%2D1321%2D0c71%2D3c70%2D6da77c4333b7" target="_blank">Dynamic Knowledge: Using Wolfram|Alpha in the Classroom</a></li>
		<li><a href="/index.cfm?event=showResource&resourceId=cf6af9e2%2D1321%2D0c71%2D3cb0%2D02f0fb1e919b" target="_blank"
			>Live from the redacted Channel Telescope!</a></li>
		<li><a href="/index.cfm?event=showResource&resourceId=40877950%2D1321%2D0c71%2D3c2a%2D6ed29c1055cd" target="_blank"
			>Understanding the 'E' in STEM: Applying Engineering Standards in the Modern Science Classroom</a></li>
		<li><a href="/index.cfm?event=showResource&resourceId=45ba104a%2D1321%2D0c71%2D3c2e%2D06fb2e015230" target="_blank"
			>Practical Tips for Building the STEM Pipeline in Your School</a></li>
		<li><a href="/index.cfm?event=showResource&resourceId=d203efae%2D1438%2Dbbfa%2D60fb%2D2cd11b32b893" target="_blank"
			>All About Amphibians</a></li>

		
   
	
	
	</ul>

</div>
</cfoutput>