<cfsilent>
	<cfset event.setArg("pageTitle","#request.siteName# - Institute Applications") />
	<cfquery name="qApplications" datasource="#getProperty('sDsn')#">
		SELECT DISTINCT
			su.sponsor_user_guid,
			su.LAST_NAME,
			su.FIRST_NAME,
			su.EMAIL,
			si.applicationStatus,
			si.applicationId
		FROM SPONSOR_FORM AS f INNER JOIN
        SPONSOR_FORM_SECTION AS s ON f.FORM_GUID = s.FORM_GUID INNER JOIN
        SPONSOR_FORM_SECTION_FIELD AS sfs ON s.FORM_SECTION_GUID = sfs.FORM_SECTION_GUID INNER JOIN
        SPONSOR_FORM_RESPONSE AS sfr ON sfs.FORM_SECTION_FIELD_GUID = sfr.FORM_SECTION_FIELD_GUID INNER JOIN
        SPONSOR_USER AS su ON sfr.SPONSOR_USER_GUID = su.SPONSOR_USER_GUID
		LEFT JOIN stemInstitute si on si.sponsor_user_guid = su.sponsor_user_guid
		ORDER BY si.applicationId
	</cfquery>
<cfsavecontent variable="css">
<link rel="stylesheet" type="text/css" href="css/tablekit.css" />
</cfsavecontent>
	<cfsavecontent variable="js">
<script type="text/javascript" src="js/fastinit.js"></script>
<script type="text/javascript" src="js/tablekit.js"></script>
<script type="text/javascript">
			function updateStatus(formName) {
				var form = $(formName);
				form.request({
				onSuccess: function(transport) {
				var successText = String(transport.responseText);
				if (successText.indexOf('false')!=-1){
					alert('Error updating.');
				}else{
					alert('Status updated.');
				}
				}
			});
		}			
</script>
	</cfsavecontent>
	<cfset event.setArg("css",css) />
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
<h1>#qApplications.RecordCount# responses.</h1>
<table border="1" class="sortable" width="100%">
	<tr>
		<th class="nosort">View</th>
		<th>Application ID</th>
		<th>First Name</th>
		<th>Email</th>
		<!--- th>status</th>
		<th class="nosort">Status</th --->
	</tr>
	<cfloop query="qApplications">
	<tr>
		<td><a href="#BuildUrl('judge.showResponse','sponsor_user_guid=#sponsor_user_guid#')#">View</a></td>
		<td>#applicationId#</td>
		<td>#first_name#</td>
		<td>#email#</td>
		<!--- td>#applicationStatus#</td>
		<td>
		<form name="#sponsor_user_guid#Status" id="#sponsor_user_guid#Status" method="post" action="/views/admin/xhr/updateStatus.cfm" onsubmit="updateStatus(this.id); return false;">
			<input type="hidden" name="sponsor_user_guid" value="#sponsor_user_guid#" />
			<select name="applicationStatus" onchange="updateStatus('#sponsor_user_guid#Status');">
			<cfif applicationStatus EQ "">
				<option value="excluded">excluded</option>
				<option value="semi-finalist">semi-finalist</option>
				<option value="finalist">finalist</option>
				<option value="winner">winner</option>
			<cfelseif applicationStatus EQ "excluded">
				<option value="excluded" selected="selected">excluded</option>
				<option value="semi-finalist">semi-finalist</option>
				<option value="finalist">finalist</option>
				<option value="winner">winner</option>
			<cfelseif applicationStatus EQ "semi-finalist">
				<option value="excluded">excluded</option>
				<option value="semi-finalist" selected="selected">semi-finalist</option>
				<option value="finalist">finalist</option>
				<option value="winner">winner</option>
			<cfelseif applicationStatus EQ "finalist">
				<option value="excluded">excluded</option>
				<option value="semi-finalist">semi-finalist</option>
				<option value="finalist" selected="selected">finalist</option>
				<option value="winner">winner</option>			
			<cfelseif applicationStatus EQ "winner">
				<option value="excluded">excluded</option>
				<option value="semi-finalist">semi-finalist</option>
				<option value="finalist">finalist</option>
				<option value="winner" selected="selected">winner</option>
			</cfif>
			</select>
			<!--- <input type="submit" value="Update" /> --->
		</form>
		</td --->
	</tr>
	</cfloop>
</table>
</cfoutput>