<cfsilent>
	<cfsavecontent variable="js">
		
<script type="text/javascript" src="/js/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript" src="/js/datepicker/js/datepicker.packed.js"></script>

<link href="/js/datepicker/css/datepicker.css" rel="stylesheet" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>	

	
        <script type="text/javascript">
			/**
			*
			*  URL encode / decode
			*  http://www.webtoolkit.info/
			*
			**/
			
			var Url = {
			
			// public method for url encoding
			encode : function (string) {
				return escape(this._utf8_encode(string));
			},
			
			// public method for url decoding
			decode : function (string) {
				return this._utf8_decode(unescape(string));
			},
			
			// private method for UTF-8 encoding
			_utf8_encode : function (string) {
				string = string.replace(/\r\n/g,"\n");
				var utftext = "";
				
				for (var n = 0; n < string.length; n++) {
				
					var c = string.charCodeAt(n);
					
					if (c < 128) {
						utftext += String.fromCharCode(c);
					}
					else if((c > 127) && (c < 2048)) {
						utftext += String.fromCharCode((c >> 6) | 192);
						utftext += String.fromCharCode((c & 63) | 128);
					}
					else {
						utftext += String.fromCharCode((c >> 12) | 224);
						utftext += String.fromCharCode(((c >> 6) & 63) | 128);
						utftext += String.fromCharCode((c & 63) | 128);
					}
			
				}
			
			return utftext;
			},
			
			// private method for UTF-8 decoding
			_utf8_decode : function (utftext) {
				var string = "";
				var i = 0;
				var c = c1 = c2 = 0;
				
				while ( i < utftext.length ) {
				
					c = utftext.charCodeAt(i);
					
					if (c < 128) {
						string += String.fromCharCode(c);
						i++;
					}
					else if((c > 191) && (c < 224)) {
						c2 = utftext.charCodeAt(i+1);
						string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
						i += 2;
					}
					else {
						c2 = utftext.charCodeAt(i+1);
						c3 = utftext.charCodeAt(i+2);
						string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
						i += 3;
					}
				
				}
			
			return string;
			}
			
			}
			function redirect(r) {
				window.location = r;
			}
			function ajaxSubmitForm(formName) {
				var form = $(formName);
				var r = Url.decode(form['r'].value);
				$('lb-invalid').hide();
				form.request({
				onSuccess: function(transport) {
				var successText = String(transport.responseText);
				if (successText.indexOf('false')!=-1){
					$('lb-invalid').appear();
				}else{
					//clearWin();
					$('lbLoginForm').hide();
					$('lbLoading').show();
					//setTimeout(function(){redirect(r)},1000);
				}
				}
			});
			}
        </script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
	<cfset event.setArg("pageTitle","#request.siteName# - Institute Application") />
</cfsilent>
<cfoutput>
	<cfif NOT isDefined("COOKIE.SPONSOR_USER_GUID") OR (isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID EQ "00000000-0000-0000-0000-000000000000")>
		<!--- NOT Logged in --->
		<script type="text/javascript">
            document.observe("dom:loaded", function() {
	            deGlobalWin({path:'#BuildUrl('xhr.loginForm')#', closebtn:false,background:'none'});
            });
        </script>        
    	<div align="center" style="font-weight:bold; font-size:14px; padding:100px 30px 300px 30px;">
        	Please <a href="javascript:void(0);" onclick="deGlobalWin({path:'#BuildUrl('xhr.loginForm')#', closebtn:false,background:'none'});">Login</a> before proceeding.
        </div>       
    <cfelse>
    	<!--- LOGGED IN - show content --->
        
        <cfif isDefined("FORM.txtTeachingDates")>
            <cfinvoke component="cfcs.institute" method="saveForm" returnvariable="result">
                <cfinvokeargument name="stcForm" value="#FORM#">
            </cfinvoke>
            
            <cfif result.BLNERROR>
                <!--- Error !!!! --->
                <br /><br />An Error has occurred ...<cfoutput>#result.html#</cfoutput>                       
            </cfif>
        </cfif>
        
        <!--- GAiello - check if this user has already submitted --->
        <cfinvoke component="cfcs.institute" method="checkUserInstituteSubmission" returnvariable="blnResult">
        	<cfinvokeargument name="SPONSOR_USER_GUID" value="#COOKIE.SPONSOR_USER_GUID#">
        </cfinvoke>
        <cfif StructKeyExists(url,'apptarget')>
        	<cfset request.apptarget=url.apptarget>
        </cfif>
        
        <cfif blnResult>
        	<!--- User has Completed the Application --->
            
        	<div id="container" style="padding:0px 30px 0px 30px;">
        		<h2>Siemens Stem Institute</h2>
            	<div id="divThankYou" style="padding: 15px 15px 45px 35px;">
            		<h3>Thank you for your participation.</h3><br /><br />
                    <div align="center">
                    	<p style="width:80%; text-align:left;">Your application has been received.<br />You will be contacted on or about May 7, 2010, if you have been selected to attend the Siemens STEM Academy Institute.</p>
                    </div>
                </div>
            </div>
             
        <cfelseif NOT StructKeyExists(request,'apptarget')>
   			<div id="content-single" class="app-landing">
        	<h2 style="text-transform:none;">Siemens STEM Institute</h2>
			<em><b style="font-size:1.3em;">A Siemens Foundation Initiative</b></em>
            <br />
            <br />
            <p class="bold">Lorem ipsum dolor sit amet, consectetur adipisicing elit!</p>
<p>You've taken the first step towards empowering your students by visiting the Siemens STEM Academy portal. Now apply below 
for the opportunity to attend the Siemens STEM Academy's Institute or STARS summer program, a professional development 
experience promoting hands-on, real-world integration of STEM in the classroom.</p>

<h3>STEM Academy Programs</h3>
<div class="box institute">
<h4>INSTITUTE</h4>
<p class="image-left"><span class="img-fpo"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent facilisis, elit et iaculis laoreet, dui leo posuere neque, nec condimentum arcu sem in sem. Etiam vestibulum libero nec turpis semper feugiat. </p>
<ul>
<li>Lorem ipsum dolor sit amet</li>
<li>Praesent facilisis, elit et iaculis laoreet</li>
<li>Nec condimentum arcu sem in sem.</li>
</ul>
<div class="two-links">
<div class="item-image">
<span class="img-fpo"></span>
<span>Lorem Ipsum</span>
<a href="##">Link somewhere</a>
</div>
<div class="item-image last-box">
<span class="img-fpo"></span>
<span>Lorem Ipsum</span>
<a href="##">Link somewhere</a>
</div></div>
</div>
<div class="box stars last-box">
<h4>STARs</h4>
<p class="image-left"><span class="img-fpo"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent facilisis, elit et iaculis laoreet, dui leo posuere neque, nec condimentum arcu sem in sem. Etiam vestibulum libero nec turpis semper feugiat. </p>
<h5>Lorem Ipsum:</h5>
<div class="five-links">
<a href="##"><span class="img-fpo"></span>link</a>
<a href="##"><span class="img-fpo"></span>link</a>
<a href="##"><span class="img-fpo"></span>link</a>
<a href="##"><span class="img-fpo"></span>link</a>
<a href="##"><span class="img-fpo"></span>link</a>
</div>
</div>
<h3>Application Requirements and Eligibility Criteria</h3>
<ul>
<li>Employed as a full-time professional educator in 2010-2011 school year and intend to continue teaching in the 2011-2012 school year </li>
<li>Instruct science, technology, engineering or mathematics (STEM) courses</li>
<li>Instruct any or a combination of grades 7 through 12</li>
<li>(STARs only): Hold current state teaching certification in middle and/or high school science, technology, engineering or math </li>
<li>U.S. Citizen or a Permanent Resident Alien</li>
<li>Applicants are required to upload a 60 second video in one of the application sections. Acceptable formats: .wmv, .mov, .flv </li>
<li>A letter of recommendation from an administrator or supervisor is required . Details are specified in that section of the application.</li>
</ul>
<h3>Selection Process</h3>
<p>The selection process is based upon responses on the application form, recommendation rating sheet, and school and community demographics. Siemens Foundation's goal is to provide opportunities to educators from a broad spectrum of schools to include diversity in size, private vs. public, rural vs. urban, and socioeconomic backgrounds of student populations and communities. More simply stated, educators from all venues of schools and communities and diverse backgrounds are encouraged to apply.</p>
<hr />
<h3>Access Application <span class="required-legend">Fields with an asterisk(<em>*</em>) are required</span></h3>
<form>
<p><em>*</em> To begin the application process, please select the program(s) you are applying for:</p>
<div class="row">
<label><input type="radio" name="apptarget" value="INSTITUTE" />Institute</label>
<label><input type="radio" name="apptarget" value="STARs" />STARs</label>
<label><input type="radio" name="apptarget" value="INSTITUTE &amp; STARs" />Both</label>
<label class="bold"><em>*</em> Program Preference
<select>
	<option>Institute</option>
	<option>STARs</option>
</select>
</label>
<cfloop collection="#url#" item="j">
	<input type="hidden" name="#j#" value="#url[j]#" />
</cfloop>
<input type="submit" value="Access Application" />
</div>
</form>
            </div>
        <cfelse>
        	<!--- User allowed to fill out the Application --->     
            
<!---            <script>
				// GAiello - 11-23-2009 - added confirmExit to prompt student before they try to close the lightbox or press back button.
				window.onbeforeunload = confirmExit;
				
				function confirmExit()	{
					if (document.getElementById('hidBlnFormStarted').value == 1)
					  return "You have attempted to leave this page.  The changes you have made will be lost if you navigate away, unless you click 'Cancel'.  Please first answer the required fields then click the 'Submit' button at the bottom of the form.";
				}
		
            
                function uploadFinished(filename,type){
                        //type corresponds to the hidden field ... i.e. ApplicationVideo
                        document.getElementById(type).value = type + '_' + filename;
                }
    
            
                function addRow (divID) {
                    if (divID == "TeachingExperience") {
                        var element = document.createElement("input");
                        element.setAttribute("type", "text");
                        element.setAttribute("value", "");
                        element.setAttribute("name", "txtTeachingDates");
                        
                        var element2 = document.createElement("input");
                        element2.setAttribute("type", "text");
                        element2.setAttribute("value", "");
                        element2.setAttribute("name", "txtSchool");
                        
                        var element3 = document.createElement("input");
                        element3.setAttribute("type", "text");
                        element3.setAttribute("value", "");
                        element3.setAttribute("name", "txtGrades");
                        
                        var element4 = document.createElement("input");
                        element4.setAttribute("type", "text");
                        element4.setAttribute("value", "");
                        element4.setAttribute("name", "txtCoursesTaught");
                        
                        var ul = document.createElement("ul");
                            var li = document.createElement("li");
                            li.appendChild(element);
                            li.setAttribute("class", "liTeachingExperience");
                            var li2 = document.createElement("li");
                            li2.appendChild(element2);
                            li2.setAttribute("class", "liTeachingExperience");
                            var li3 = document.createElement("li");
                            li3.appendChild(element3);
                            li3.setAttribute("class", "liTeachingExperience");
                            var li4 = document.createElement("li");
                            li4.appendChild(element4);
                            li4.setAttribute("class", "liTeachingExperience");
                        
                        ul.appendChild(li);
                        ul.appendChild(li2);
                        ul.appendChild(li3);
                        ul.appendChild(li4);
                        ul.setAttribute("class","ulTxtSection");
                        ul.setAttribute("style","padding-top:10px");
                        
                        document.getElementById(divID).appendChild(ul);
                        element.focus();
                    }
                    
                    if (divID == "SecondaryEducational") {
                        var element = document.createElement("input");
                        element.setAttribute("type", "text");
                        element.setAttribute("value", "");
                        element.setAttribute("name", "txtDegreeHours");
                        
                        var element2 = document.createElement("input");
                        element2.setAttribute("type", "text");
                        element2.setAttribute("value", "");
                        element2.setAttribute("name", "txtMajor");
                        
                        var element3 = document.createElement("input");
                        element3.setAttribute("type", "text");
                        element3.setAttribute("value", "");
                        element3.setAttribute("name", "txtSecondarySchool");
                        
                        var ul = document.createElement("ul");
                            var li = document.createElement("li");
                            li.setAttribute("class", "liSecondaryEducational");				
                            li.appendChild(element);
                            var li2 = document.createElement("li");
                            li2.setAttribute("class", "liSecondaryEducational");	
                            li2.appendChild(element2);
                            var li3 = document.createElement("li");
                            li3.setAttribute("class", "liSecondaryEducational");	
                            li3.appendChild(element3);
                        
                        ul.appendChild(li);
                        ul.appendChild(li2);
                        ul.appendChild(li3);
                        ul.setAttribute("class","ulTxtSection");
                        ul.setAttribute("style","padding-top:10px");
                        
                        document.getElementById(divID).appendChild(ul);
                        element.focus();
                    }
                    
                    if (divID == "Leadership") {
                        var element = document.createElement("input");
                        element.setAttribute("type", "text");
                        element.setAttribute("value", "");
                        element.setAttribute("name", "txtLeadershipDates");
                        
                        var element2 = document.createElement("input");
                        element2.setAttribute("type", "text");
                        element2.setAttribute("value", "");
                        element2.setAttribute("name", "txtOrganization");
                        
                        var element3 = document.createElement("input");
                        element3.setAttribute("type", "text");
                        element3.setAttribute("value", "");
                        element3.setAttribute("name", "txtLeadershipRole");
                        
                        var ul = document.createElement("ul");
                            var li = document.createElement("li");
                            li.appendChild(element);
                            li.setAttribute("class", "liLeadership");
                            var li2 = document.createElement("li");
                            li2.appendChild(element2);
                            li2.setAttribute("class", "liLeadership");
                            var li3 = document.createElement("li");
                            li3.appendChild(element3);
                            li3.setAttribute("class", "liLeadership");
                        
                        ul.appendChild(li);
                        ul.appendChild(li2);
                        ul.appendChild(li3);
                        ul.setAttribute("class","ulTxtSection");
                        ul.setAttribute("style","padding-top:10px");
                        
                        document.getElementById(divID).appendChild(ul);
                        element.focus();
                    }
                    
                    if (divID == "Awards") {
                        var element = document.createElement("input");
                        element.setAttribute("type", "text");
                        element.setAttribute("value", "");
                        element.setAttribute("name", "txtAwardDates");
                        
                        var element2 = document.createElement("input");
                        element2.setAttribute("type", "text");
                        element2.setAttribute("value", "");
                        element2.setAttribute("name", "txtAward");
                        
                        var ul = document.createElement("ul");
                            var li = document.createElement("li");
                            li.appendChild(element);
                            li.setAttribute("class","liAwards");				
                            var li2 = document.createElement("li");
                            li2.appendChild(element2);
                            li2.setAttribute("class","liAwards");
                        
                        ul.appendChild(li);
                        ul.appendChild(li2);
                        ul.setAttribute("class","ulTxtSection");
                        ul.setAttribute("style","padding-top:10px");
                        
                        document.getElementById(divID).appendChild(ul);
                        element.focus();
                    }
                    
                }
                
                function setChoices (obj, val) {
                    var arrBoxValues = ['Energy','Climate','Health & Medical Advancement','Biodiversity','Space','Simple Machines','Forces'];
                    
                    if (obj.id == "selFirstChoice") {
                        document.getElementById("selSecondChoice").options.length=0;
                        document.getElementById("selThirdChoice").options.length=0;
                        
                        document.getElementById("selSecondChoice").options.add(new Option("- select one -",""));
                        document.getElementById("selThirdChoice").options.add(new Option("- select one -",""));
                        
                        if (val != "") {
                            for (var dd = 0; dd < arrBoxValues.length; dd++) {
                                if (arrBoxValues[dd] != val) {
                                    document.getElementById("selSecondChoice").options.add(new Option(arrBoxValues[dd],arrBoxValues[dd]));
                                }
                            }
                        }
                    }
                    else {
                        document.getElementById("selThirdChoice").options.length=0;
                        document.getElementById("selThirdChoice").options.add(new Option("- select one -",""));
                        
                        var dd=0;
                        for (var dd = 0; dd < arrBoxValues.length; dd++) {
                            if ((arrBoxValues[dd] != val) && (arrBoxValues[dd] != document.getElementById("selFirstChoice").value)) {
                                document.getElementById("selThirdChoice").options.add(new Option(arrBoxValues[dd],arrBoxValues[dd]));
                            }
                        }
                    }
                }
            </script>
            
            <script>
                function trimFields (objForm) {
                    for(i=0; i<objForm.elements.length; i++)	{
                        if (objForm.elements[i].type == "text") {
                            objForm.elements[i].value = objForm.elements[i].value.replace(/^\s+|\s+$/g,""); // trim
                        }
                    }	
                }
                
                function validateForm (docForm)	{
                    trimFields (docForm);
                    
                    var blnErrors = false;
                    
                    var arrTextFields = ['txtTeachingDates','txtSchool','txtGrades','txtCoursesTaught'];
                    for(i=0; i<arrTextFields.length; i++)	{
                        if (docForm.elements[arrTextFields[i]]) {
                            if (docForm.elements[arrTextFields[i]].value == "")	{
                                document.getElementById("spanTeachingExperience").className= "requiredfield";
                                blnErrors = true;
                            }
                            else if (docForm.elements[arrTextFields[i]].length) {
                                for(j=0; j<docForm.elements[arrTextFields[0]].length; j++)	{
                                    if (docForm.elements[arrTextFields[0]][j].value == "")	{
                                        document.getElementById("spanTeachingExperience").className= "requiredfield";	// only 1st row is required
                                        blnErrors = true;
                                    }
                                }
                            }
                            
                        }
                    }
                    
                    /*
                    var arrTextFields = ['txtDegreeHours','txtMajor','txtSecondarySchool'];
                    for(i=0; i<arrTextFields.length; i++)	{
                        if (docForm.elements[arrTextFields[i]]) {
                            if (docForm.elements[arrTextFields[i]].value == "")	{
                                document.getElementById("spanSecondaryEducational").className= "requiredfield";
                                blnErrors = true;
                            }
                            else if (docForm.elements[arrTextFields[i]].length) {
                                for(j=0; j<docForm.elements[arrTextFields[i]].length; j++)	{
                                    if (docForm.elements[arrTextFields[i]][j].value == "")	{
                                        document.getElementById("spanSecondaryEducational").className= "requiredfield";
                                        blnErrors = true;
                                    }
                                }
                            }
                            
                        }
                    }
                    
                    var arrTextFields = ['txtLeadershipDates','txtOrganization','txtLeadershipRole'];
                    for(i=0; i<arrTextFields.length; i++)	{
                        if (docForm.elements[arrTextFields[i]]) {
                            if (docForm.elements[arrTextFields[i]].value == "")	{
                                document.getElementById("spanLeadership").className= "requiredfield";
                                blnErrors = true;
                            }
                            else if (docForm.elements[arrTextFields[i]].length) {
                                for(j=0; j<docForm.elements[arrTextFields[i]].length; j++)	{
                                    if (docForm.elements[arrTextFields[i]][j].value == "")	{
                                        document.getElementById("spanLeadership").className= "requiredfield";
                                        blnErrors = true;
                                    }
                                }
                            }
                            
                        }
                    }
                    
                    var arrTextFields = ['txtAwardDates','txtAward'];
                    for(i=0; i<arrTextFields.length; i++)	{
                        if (docForm.elements[arrTextFields[i]]) {
                            if (docForm.elements[arrTextFields[i]].value == "")	{
                                document.getElementById("spanAwards").className= "requiredfield";
                                blnErrors = true;
                            }
                            else if (docForm.elements[arrTextFields[i]].length) {
                                for(j=0; j<docForm.elements[arrTextFields[i]].length; j++)	{
                                    if (docForm.elements[arrTextFields[i]][j].value == "")	{
                                        document.getElementById("spanAwards").className= "requiredfield";
                                        blnErrors = true;
                                    }
                                }
                            }
                            
                        }
                    }
                    */
                    
                    if (document.getElementById("selFirstChoice").value =="") {
                        document.getElementById("spanFirstChoice").className= "requiredfield";	
                        blnErrors = true;
                    }
                    if (document.getElementById("selSecondChoice").value =="") {
                        document.getElementById("spanSecondChoice").className= "requiredfield";
                        blnErrors = true;
                    }
                    if (document.getElementById("selThirdChoice").value =="") {
                        document.getElementById("spanThirdChoice").className= "requiredfield";	
                        blnErrors = true;
                    }
                    
                    if (document.getElementById("ApplicationVideo").value =="") {
                        document.getElementById("spanApplicationVideo").className= "requiredfield";	
                        blnErrors = true;
                    }
                    if (document.getElementById("ApplicationEssay").value =="") {
                        document.getElementById("spanApplicationEssay").className= "requiredfield";	
                        blnErrors = true;
                    }
                    if (document.getElementById("RecommendationLetter").value =="") {
                        document.getElementById("spanRecommendationLetter").className= "requiredfield";	
                        blnErrors = true;
                    }
                    
                    if (blnErrors) {
                        alert('Please complete all required fields.');	
                    }
                    else {
                        if (confirm("Are you sure you would like to submit this information?\nInformation cannot be adjusted once your application is submitted.")) {
							document.getElementById('hidBlnFormStarted').value = 0;	// GAiello - turn off prompt
                            docForm.submit();
                        }
                    }
                    
                }
				
				function checkValDef(el){
					var val=el.value;
					if(val=="ex: 06/09-04/10"||val=="ex: 6, 7, 8 or 5, 6"||val=="ex: Biology, Physics, Math"){
						el.value="";
						el.style.color="";
					}
				}
            </script>
--->            
   			<div id="content-single">
        	<h2 style="text-transform:none;">Siemens STEM Institute</h2>
			<em><b style="font-size:1.3em;">A Siemens Foundation Initiative</b></em>
            <br />
            <br />
            
            <div class="application-form">
            	<h3>2010 STEM Academy Application Process</h3>
                <div class="info">Applying for: <strong>#request.apptarget#</strong> <a href="##">change program selection</a>
                <span class="required-legend">Fields with an asterisk(<em>*</em>) are required</span></div>
                <ul class="subnav">
                    <li><a href="##">Application Process</a></li>
                    <li><a href="/index.cfm?event=showContent&id=43">Eligibility Criteria</a></li>
                    <li><a href="##">Judging Guidelines</a></li>
                    <li><a href="/index.cfm?event=showContent&id=44">Program Schedule</a></li>
                    <li><a href="##">Previous Sessions</a></li>
                    <li><a href="/index.cfm?event=showContent&id=36">View Summary</a></li>
                </ul>
                <div class="tab-container">
                <ul class="tabs">
                	<li><a href="##education">Education and Experience</a></li>
                    <li><a href="##achievements">Achievements</a></li>
                    <li><a href="##techskills">Technical Skills</a></li>
                    <li><a href="##reference">Reference</a></li>
                    <li><a href="##upload">Upload</a></li>
                    <li><a href="##essay">Essays</a></li>
                </ul>
                <div class="tab-panel">
                    <div class="tab-pane">
                    <a name="education" class="section-anchor">Education and Experience</a>
                    <h4>Post Secondary Educational Background</h4>
                    <p>Please include additional graduate education even if a degree has not been completed. (Most recent first)</p>
                    <div class="row repeatable">
                    <label><em>*</em>From
                        <input type="text" class="date req" name="txtSchoolStart1" id="txtSchoolStart1" />
                    </label>
                    <label><em>*</em>To
                        <input type="text" class="date req" name="txtSchoolEnd1" id="txtSchoolEnd1" />
                    </label>
                    <label><em>*</em>School/Organization
                        <input type="text" class="mid-text req" name="txtSecondarySchool1" id="txtSecondarySchool1" />
                    </label>
                    <label><em>*</em>Degree
                        <select class="req" name="txtDegree1" id="txtDegree1">
                        	<option value=""> --Select-- </option>
		                   	<option value="MA">Masters</option>
		                   	<option value="PhD">Ph.D</option>
                        </select>
                    </label>
                    <label><em>*</em>Major
                        <input type="text" class="short-text req" name="txtMajor1" id="txtMajor1" />
                    </label>
                    </div>
                    <h4>Teaching Experience <span>Grades 7-12</span></h4>
                    <div class="row">
                    <label><em>*</em>## of years
                        <input id="txtTeachingYears" class="numeric req" />
                    </label>
                    </div>
                    <h5>Current Position</h5>
                    <div class="row">
                    <label><em>*</em>From
                        <input id="txtCurrentStart" class="date req" />
                    </label>
                    <label><em>*</em>School/Organization
                        <input id="txtCurrentSchool" class="long-text req" />
                    </label>
                    <label><em>*</em>Grade(s)
                        <select id="txtCurrentGrade" class="req">
                        	<option value="">--</option>
                        	<option value="6">6</option>
                        	<option value="7">7</option>
                        	<option value="8">8</option>
                        	<option value="9">9</option>
                        	<option value="10">10</option>
                        	<option value="11">11</option>
                        	<option value="12">12</option>
                        </select>
                    </label>
                    <label><em>*</em>Courses Taught
                        <select id="txtCurrentCourses" name="txtCurrentCourses" class="req">
                        	<option value=""> --Select-- </option>
                        	<option value="math">Mathematics</options>
                        	<option value="sci">Science</options>
                        	<option value="eng">Engineering</options>
                        	<option value="tech">Technology</options>
                        	<option value="other">Other</options>
                        </select>
                    </label>
                    <label>If other, please list
                    	<input class="active_when-txtCurrentCourses-other long-text" />
                    </label>
                    </div>
                    <h6><em>*</em>Do you expect to hold the same position for the 2011-12 school year?</h6>
                    <div class="row">
                        <label class="radio"><input type="radio" name="txtSame" value="yes" />Yes</label>
                        <label class="radio"><input type="radio" name="txtSame" value="no" />No</label>
                    </div>
                    <div class="row">
                        <label class="textarea" for="txtCurrentExplain">If no, please explain below</label>
                        <textarea class="large-textarea active_when-txtSame-no" id="txtCurrentExplain"></textarea>
                    </div>
                    <h4>Previous Teaching Position(s)/Experience</h4>
                    <p>Please list all other teaching experience (Most recent first)</p>
                    <div class="row repeatable">
                    <label><em>*</em>From
                        <input id="txtHistoryStart1" class="date req" />
                    </label>
                    <label><em>*</em>To
                        <input id="txtHistoryEnd1" class="date req" />
                    </label>
                    <label><em>*</em>School/Organization
                        <input id="txtHistorySchool1" class="short-text req" />
                    </label>
                    <label><em>*</em>Grade(s)
                        <select id="txtHistoryGrade1" class="req">
                        	<option value="">--</option>
                        	<option value="6">6</option>
                        	<option value="7">7</option>
                        	<option value="8">8</option>
                        	<option value="9">9</option>
                        	<option value="10">10</option>
                        	<option value="11">11</option>
                        	<option value="12">12</option>
                        </select>
                    </label>
                    <label><em>*</em>Courses Taught
                        <select id="txtHistoryCourses1" name="txtHistoryCourses1" class="req" />
                        	<option value=""> --Select-- </option>
                        	<option value="math">Mathematics</options>
                        	<option value="sci">Science</options>
                        	<option value="eng">Engineering</options>
                        	<option value="tech">Technology</options>
                        	<option value="other">Other</options>
                        </select>
                    </label>
                    <label>If other, please list
                    	<input class="active_when-txtHistoryCourses1-other short-text" />
                    </label>
                    </div>
                    <h4>Educational Leadership Responsibilities</h4>
                    <p>Please list your education leadership responsibilities (e.g. school committees, professional organizations, etc). (Most recent first)</p>
                    <div class="row repeatable">
                    <label><em>*</em>From
                        <input id="txtLeadershipStart1" class="date req" />
                    </label>
                    <label><em>*</em>To
                        <input id="txtLeadershipEnd1" class="date req" />
                    </label>
                    <label><em>*</em>Organization
                        <input id="txtLeadershipOrganization1" class="long-text req" />
                    </label>
                    <label><em>*</em>Leadership Role
                        <input id="txtLeadershipRole1" class="mid-text req" />
                    </label>
                    </div>
                    <h4>Extracurricular Activities</h4>
                    <p>Your extracurricular activities with students that are related to science, technology, mathematics, or engineering (STEM) education, including dates (Most recent first)</p>
                    <div class="row repeatable">
                    <label><em>*</em>From
                    <input class="date req" />
                    </label>
                    <label><em>*</em>To
                    <input class="date req" />
                    </label>
                    <label><em>*</em>Activity
                    <input class="long-text req" />
                    </label>
                    <label><em>*</em>Level of Involvement
                    <input class="mid-text req" />
                    </label>
                    </div>
                    <div class="buttons">
                        <button>Save &amp; Continue</button>
                        <button>Save for Later</button>
                        <button>Exit Application</button>
                    </div>
                    </div>
                    <div class="tab-pane">
                    	<a name="achievements" class="section-anchor">Achievements</a>
                    	<h4>STEM Related Professional Development</h4>
                        <p>Include any higher education research as a student. List most recent first.</p>
                        <div class="row repeatable">
                        	<label>From
		                    	<input class="date" />
                        	</label>
                        	<label>To
		                    	<input class="date" />
                        	</label>
                            <label>Professional Development<br />
		                        <textarea class="small-textarea"></textarea>
                            </label>
                        </div>
                    	<h4>Honors and Awards</h4>
                        <h5>List any academic honors/awards you have recieved beginning with the most recent</h5>
                        <div class="row repeatable">
                        	<label>Date Recived
    	                     	<input class="date" />
	                       	</label>
                            <label>Honor / Award
                            	<input class="long-text" />
                           </label>
                        </div>
                        <h5><em>*</em>Have you ever been recognized by the Siemens Foundation or participated in a Siemens Foundation program?</h5>
                        <div class="row">
                            <label class="radio"><input type="radio" name="txtSiemens" value="yes" />Yes</label>
                            <label class="radio"><input type="radio" name="txtSiemens" value="no" />No</label>
                            <span>If yes provide date(s) and program(s)</span>
                        </div>
                        <div class="row repeatable no-label">
                            <label><input class="date active_when-txtSiemens-yes" /></label>
                            <label><input class="long-text active_when-txtSiemens-yes" /></label>
                        </div>
                        <h4>Administrative Support</h4>
                        <h5><em>*</em>Does your principal support your application to partipate in this opportunity?</h5>
                        <div class="row">
                            <label class="radio"><input type="radio" name="txtPrincipal" value="yes" />Yes</label>
                            <label class="radio"><input type="radio" name="txtPrincipal" value="no" />No</label>
                        </div>
                        <div class="row">
                            <label>Prefix
                            <select>
                            	<option value="">--</option>
                            	<option>Mr.</option>
                            	<option>Mrs.</option>
                            	<option>Ms.</option>
                            	<option>Dr.</option>
                            </select>
                            </label>
                            <label>First Name<input class="mid-text" /></label>
                            <label>Last Name<input class="mid-text" /></label>		                        
                            <label>Email<input class="email" /></label>
                        </div>
                        <div class="buttons">
                            <button>Save &amp; Continue</button>
                            <button>Save for Later</button>
                            <button>Exit Application</button>
                        </div>
                        
                    </div>
                    <div class="tab-pane">
                    	<a name="techskills" class="section-anchor">Technical Skills</a>
                    	<h4>Technology Familiarity and Use Gauge</h4>
                        <p>Please assess you familiarity with and use of technology in the classroom by selecting the <strong>most appropriate</strong> response below. Note that this information will not be used in the judging processs but will help us to plan accordingly for the Institute.</p>
                        <h5>Select one:</h5>
                        <label class="radio-row"><input type="radio" name="techFam" />I have advanced knowledge of technology and use it very frequently in the classroom</label>
                        <label class="radio-row"><input type="radio" name="techFam" />I have above-average knowledge of technology and use it somewhat often in the classroom</label>
                        <label class="radio-row"><input type="radio" name="techFam" />I have average knowledge of technology and sometimes use it in the classroom</label>
                        <label class="radio-row"><input type="radio" name="techFam" />I have very limited knowledge of technology and occasionally or rarely use it in the classroom</label>
                        <h5><em>*</em>How often do you use the following tools?</h5>
                        <table class="survey">
                        <thead>
                        <tr><th>&nbsp;</th><th>Very Often</th><th>Sometimes</th><th>Rarely</th><th>Never</th></tr>
                        </thead>
                        <tbody>
                        <tr><td>Wikis</td><td><input type="radio" name="wiki"/></td><td><input type="radio" name="wiki"/></td><td><input type="radio" name="wiki"/></td><td><input type="radio" name="wiki"/></td></tr>
                        <tr><td>Blogs/Forums</td><td><input type="radio" name="blog"/></td><td><input type="radio" name="blog"/></td><td><input type="radio" name="blog"/></td><td><input type="radio" name="blog"/></td></tr>
                        <tr><td>Social Media (Facebook, Twitter, etc.)</td><td><input type="radio" name="social"/></td><td><input type="radio" name="social"/></td><td><input type="radio" name="social"/></td><td><input type="radio" name="social"/></td></tr>
                        <tr><td>Digital Storytelling tools (MovieMaker, etc.)</td><td><input type="radio" name="story"/></td><td><input type="radio" name="story"/></td><td><input type="radio" name="story"/></td><td><input type="radio" name="story"/></td></tr>
                        <tr><td>Web 2.0 Applications</td><td><input type="radio" name="web20"/></td><td><input type="radio" name="web20"/></td><td><input type="radio" name="web20"/></td><td><input type="radio" name="web20"/></td></tr>
                        </tbody>
                        </table>
                        <div class="buttons">
                            <button>Save &amp; Continue</button>
                            <button>Save for Later</button>
                            <button>Exit Application</button>
                        </div>
                    </div>
                    <div class="tab-pane">
                    	<a name="reference" class="section-anchor">Reference</a>
                    	<h4>Letter of Recommendation</h4>
                        <p>Your application requires a minimum of ONE recommendation rating sheet from an administrator or supervisor, specifically addressing how your participation will benefit your school/students/fellow teachers. The individual selected to provide your recomendation letter should be able to evaluate your character, academic and/or teaching abilities.</p>
                        <p>List the contact information of your recommender including name, address, phone number and email address. Please note this person may be contacted for verification purposes.</p>
                        <h6>Please provide the following information for the person you have chosen to recommend you:</h6>
                        <fieldset>
                        <div class="row">
                            <label>Prefix<select>
                            	<option value="">--</option>
                            	<option>Mr.</option>
                            	<option>Mrs.</option>
                            	<option>Ms.</option>
                            	<option>Dr.</option>
                            </select></label>
                            <label><em>*</em>First Name<input class="mid-text" /></label>
                            <label>M.I.<input class="initial req" /></label>
                            <label><em>*</em>Last Name<input class="mid-text" /></label>
                        </div>
                        <div class="row">
                            <label><em>*</em>School/Organization<input class="long-text req" /></label>
                            <label><em>*</em>Title<select name="txtRefTitle" class="mid-text req">
                            	<option val=""> --Select-- </option>
                            	<option>Administrator</option>
                            	<option>Superintendent</option>
                            	<option val="other">Other</option>
                            </select></label>
                            <label>If other, please list<input class="mid-text active_when-txtRefTitle-other" /></label>
                            <label><em>*</em>Relationship to<input class="mid-text req" /></label>
                        </div>
                        <div class="row repeatable">
                            <label><em>*</em>Telephone (w/area code)<input class="phone req" /></label>
                            <label>ext.:<input class="numeric" /></label>
                            <label class="radio"><input type="radio" />Work</label>
                            <label class="radio"><input type="radio" />Home</label>
                        </div>
                        <div class="row repeatable">
                            <label class="long-text"><em>*</em>Email<input class="email req" /></label>
                            <label class="radio"><input type="radio" />Work</label>
                            <label class="radio"><input type="radio" />Personal</label>
                        </div>
                        </fieldset>
                        <div class="notify">-- User Action Required <span>(See Below)</span> --</div>
                        <p>Please send the following URL to the person you have selected to provide a recocommendation letter. It is your responsibility to ensure that they are notified and toy follow up with them to make sure they complete the request. The recommender should upload the letter, not the applicant</p>
                        <p><a href="##">https://www.uniqueurl.com/recommenderlink</a></p>
                        <div class="buttons">
                            <button>Save &amp; Continue</button>
                            <button>Save for Later</button>
                            <button>Exit Application</button>
                        </div>
                    </div>
                    <div class="tab-pane">
                    	<a name="upload" class="section-anchor">Upload</a>
                    	<h4>Upload Video</h4>
                        <p><em>*</em>Please upload a 60 second video responding to <strong>one</strong> of the following:</p>
                        <label class="radio-row"><input type="radio" name="uploadTopic" />Describe any new innovative ideas, materials, instructional strategies or techniques that you utilize in your classroom.</label>
                        <label class="radio-row"><input type="radio" name="uploadTopic" />Describe ways in which you provide student educational STEM experiences outside of the classroom</label>
                        <label class="radio-row"><input type="radio" name="uploadTopic" />What factors influenced your decision to become a teacher? Describe your greatest contributions and accomplishments in STEM education.</label>
                        <label class="radio-row"><input type="radio" name="uploadTopic" />What do you believe are the major issues in education today? Address one in depth, detailing potential causes, effects, and resolutions.</label>
                        
                        <h5><em>*</em>Select a File to Upload <span>(Only the following file formats are acceptable: .wmv, .mov, .flv)</span></h5>
                        <input type="file" class="req" />
                        <div class="buttons">
                            <button>Save &amp; Continue</button>
                            <button>Save for Later</button>
                            <button>Exit Application</button>
                        </div>
                    </div>
                    <div class="tab-pane">
                    <a name="essay" class="section-anchor">Essay</a>
                    <p><em>*</em>1. Describe your teaching philosophy, including your ideas of what makes you a successful teacher. Provide an example of how your teaching philosophy is reflected in a lesson or unit you teach. (100 words max)</p>
                    <textarea class="wysiwyg req"></textarea>
                    <p><em>*</em>2. Why do you think STEM education is important to today's students and how willi it be relevant to their future? (100 words max)</p>
                    <textarea class="wysiwyg req"></textarea>
                    <p><em>*</em>3. Why do you want to attend the Siemens STEM institute? Explain what you hope to learn from the experience, what you can uniquly contribute and/or what you would like to add to your classroom instruction upon completion of the STEM Institute (100 words max)</p>
                    <textarea class="wysiwyg req"></textarea>
                    <p>Additional Information: Please provide any additional information you believe is relevant to your application or for which you needed additional space in any of the above questions. If you provide additional information on a question above, please indicate the question to which the information applies.</p>
                    <textarea class="wysiwyg"></textarea>
                        <div class="buttons">
                            <button>Save &amp; Continue</button>
                            <button>Save for Later</button>
                            <button>Exit Application</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            
<script type="text/javascript" src="/js/stemapplication.js"></script>
            
            <!--- 
        	<div align="left">
            	Welcome to the 2010 <a href="/index.cfm?event=showContent&c=36">Siemens STEM Institute</a> Application!<br /><br />
You've taken the first step towards empowering your students by visiting the Siemens STEM Academy portal. Now apply below for the opportunity to attend the Siemens STEM Institute, a professional development experience promoting hands-on, real-world integration of STEM in the classroom.<br /><br />
Please answer each question below, if applicable. You will need to upload three files before you can submit your application.
<br /><br />
This form will not allow you to save partially completed applications. Bookmark this page to access the application.
                <br /> <br /> 
                Please <a href="mailto:siemens_stem_academy@redacted.com?subject=Contact from Siemens STEM Institure Application" id="contactus">contact us</a> with any questions. Good luck!
            </div>
            
            <br />
            <br />
            
            <div id="divForm">
            	<h3>Tell us more about yourself</h3>
                * <i>All fields marked with an asterisk are required.</i>
                <br /><br />
                
                <form action="" method="post" name="frmTeacher" id="frmTeacher" enctype="multipart/form-data" >
                	<div id="TeachingExperience">
                        <p><b>Teaching experience*</b> <i>(At least one experience required)</i></p>
                        <ul>
                            <li>Dates</li>
                            <li>School/Organization</li>
                            <li>Grade(s)</li>
                            <li>Courses Taught</li>                        
                        </ul>
                         <ul class="ulTxtSection">
                            <li class="liTeachingExperience"><span id="spanTeachingExperience"></span><input name="txtTeachingDates" type="text" id="txtTeachingDates1" value="ex: 06/09-04/10" style="color:##666;" onfocus="checkValDef(this);" /></li>
                            <li class="liTeachingExperience"><input name="txtSchool" type="text" id="txtSchool1" /></li>
                            <li class="liTeachingExperience"><input name="txtGrades" type="text" id="txtGrades1" value="ex: 6, 7, 8 or 5, 6" style="color:##666;" onfocus="checkValDef(this);" /></li>
                            <li class="liTeachingExperience"><input name="txtCoursesTaught" type="text" id="txtCoursesTaught1" value="ex: Biology, Physics, Math" style="color:##666;" onfocus="checkValDef(this);"/></li>
                            <li style="width:40px;"><a href="javascript:void(0);" onclick="addRow('TeachingExperience');" title="add row"> + Add</a></li>
                        </ul>
                        
                    </div>
                    
                    <div id="SecondaryEducational">
                        <p><b>Post Secondary Educational Background</b> - Please include additional graduate education even if a degree has not been completed.	</p>
                        <ul>
                            <li>Degree or Number of Hours</li>
                            <li>Major(s)</li>
                            <li>School</li>
                        </ul>
                         <ul class="ulTxtSection">
                            <li class="liSecondaryEducational"><span id="spanSecondaryEducational"></span><input name="txtDegreeHours" type="text" id="txtDegreeHours1" /></li>
                            <li class="liSecondaryEducational"><input name="txtMajor" type="text" id="txtMajor1" /></li>
                            <li class="liSecondaryEducational"><input name="txtSecondarySchool" type="text" id="txtSecondarySchool1" /></li>
                            <li><a href="javascript:void(0);" onclick="addRow('SecondaryEducational');" title="add row"> + Add</a></li>
                        </ul>
                    
                    
                    </div>
                    
                    <div id="Leadership">
                        <p><b>Education Leadership Responsibilities</b> - Please list your education leadership responsibilities with the most recent first.<br /><i>(Examples: school committees, professional organizations, special projects, etc.)</i></p>
                        <ul>
                            <li>Dates</li>
                            <li>Organization</li>
                            <li>Leadership Role</li>
                        </ul>
                         <ul class="ulTxtSection">
                            <li class="liLeadership"><span id="spanLeadership"></span><input name="txtLeadershipDates" type="text" id="txtLeadershipDates1" value="ex: 06/09-04/10" style="color:##666;" onfocus="checkValDef(this);" /></li>
                            <li class="liLeadership"><input name="txtOrganization" type="text" id="txtOrganization1" /></li>
                            <li class="liLeadership"><input name="txtLeadershipRole" type="text" id="txtLeadershipRole1" /></li>
                            <li><a href="javascript:void(0);" onclick="addRow('Leadership');" title="add row"> + Add</a></li>
                        </ul>
                    
                    </div>
                    
                    <div id="Awards">
                        <p><b>Honors and Awards</b></p>
                        <ul>
                            <li>Dates</li>
                            <li>Honors/Awards</li>
                        </ul>
                         <ul class="ulTxtSection">
                            <li class="liAwards"><span id="spanAwards"></span><input name="txtAwardDates" type="text" id="txtAwardDates1" value="ex: 06/09-04/10" style="color:##666;" onfocus="checkValDef(this);" /></li>
                            <li class="liAwards"><input name="txtAward" type="text" id="txtAward1" /></li>
                            <li><a href="javascript:void(0);" onclick="addRow('Awards');" title="add row"> + Add</a></li>
                        </ul>
                    
                    </div>
                    
                    <div id="Interests">
                        <p><b>Areas of Interest</b> - Please select your top three areas of interest. Each attendee will be assigned to a working group focused on one of these topics for additional deep-dive exposure.</p>
                        <ul>
                            <li>First Choice*</li>
                            <li>Second Choice*</li>
                            <li>Third Choice*</li>                            
                        </ul>
                         <ul class="ulTxtSection">
                            <li><span id="spanFirstChoice"></span><select name="selFirstChoice" id="selFirstChoice" style="width:205px;" onchange="setChoices(this, this.value)">
                            													<option selected value="">- select one-</option>
                                                                                <option value="Energy">Energy</option>
                                                                                <option value="Climate">Climate</option>
                                                                                <option value="Health & Medical Advancement">Health & Medical Advancement</option>
                                                                                <option value="Biodiversity">Biodiversity</option>
                                                                                <option value="Space">Space</option>
                                                                                <option value="Simple Machines">Simple Machines</option>
                                                                                <option value="Forces">Forces</option>                                                                                
                                                                              </select></li>
                            <li><span id="spanSecondChoice"></span><select name="selSecondChoice" id="selSecondChoice" style="width:180px;" onchange="setChoices(this, this.value)">
                            													<option selected value="">- select one -</option>
                                                                              </select></li>
                            <li><span id="spanThirdChoice"></span><select name="selThirdChoice" id="selThirdChoice" style="width:180px;">
                            													<option selected value="">- select one -</option>
                                                                              </select></li>
                        </ul>
                    
                    <br clear="all" />
                    </div>
                    
                   <br /><br />
                   <h3>Why do you want to attend the institute</h3><br />
                   <div id="uploadSection">
                   		<p><b>Application Video*</b>
                   		<br />
                   		Why do you think STEM is important to today's 21st century students and explain its relevance to their future career/life choices?<br />
                   		(Please upload a 60-second video in one of the following file formats; .asf, .avi, .flv, .mov, .mpg, .mpeg, .wmv.  Max video size is 100 MB.)</p>
                        <div id="divApplicationVideo">
                        	<span id="spanApplicationVideo"></span><!---<input type="file" name="ApplicationVideo" id="ApplicationVideo">--->
                                <input type="hidden" name="ApplicationVideo" id="ApplicationVideo" value="">
                            		<div id="flashBox" style="height:50px;">
										<script type="text/javascript">
                                            var flashvars = {};
                                            flashvars.type = "ApplicationVideo";
											flashvars.SPONSOR_USER_GUID = "#COOKIE.SPONSOR_USER_GUID#";
											flashvars.sitePath = "http://#CGI.SERVER_NAME#";
                                            var params = {};
                                            params.menu = "false";
                                            params.wmode = "transparent";
                                            params.allowScriptAccess = 'always';
                                            var attributes = {};
                                            attributes.id = "AppVideo";
                                            swfobject.embedSWF("/swf/stem-upload.swf?super", "altContent0", "290", "50", "8.0.0", false, flashvars, params, attributes);
                                        </script>
                                        <div id="altContent0">
                                            Please upgrade your <a href="http://www.adobe.com/products/flashplayer/" target="_blank">flash player</a>.
                                        </div>
                                    </div>

                        </div>
                        
                        <br />
                        
                        <p><b>Application Essay*</b>
                       
                        <br />Why do you want to attend the Siemens STEM Institute?  How will you uniquely contribute to the experience of your fellow participants? <br />(250-word limit in one of the following file formats; .txt, .doc, .xls, .docx, .rtf.)</p>
                        <div id="divApplicationEssay">
                        	<span id="spanApplicationEssay"></span><!---<input type="file" name="ApplicationEssay" id="ApplicationEssay">--->
                            
                                <input type="hidden" name="ApplicationEssay" id="ApplicationEssay" value="">
                            		<div id="flashBox" style="height:50px;">
										<script type="text/javascript">
                                            var flashvars = {};
                                            flashvars.type = "ApplicationEssay";
											flashvars.SPONSOR_USER_GUID = "#COOKIE.SPONSOR_USER_GUID#";
											flashvars.sitePath = "http://#CGI.SERVER_NAME#";
                                            var params = {};
                                            params.menu = "false";
                                            params.wmode = "transparent";
                                            params.allowScriptAccess = 'always';
                                            var attributes = {};
                                            attributes.id = "AppEssay";
                                            swfobject.embedSWF("/swf/stem-upload.swf?super", "altContent1", "290", "50", "8.0.0", false, flashvars, params, attributes);
                                        </script>
                                        <div id="altContent1">
                                            Please upgrade your <a href="http://www.adobe.com/products/flashplayer/" target="_blank">flash player</a>.
                                        </div>
                                    </div>
                            
                            
                            
                        </div>
                        
                          <br />
                        
                        <p><b>Recommendation Letter*</b>
                        
                        <br />Please submit 1 recommendation letter from an administrator or supervisor, specifically addressing how your participation will benefit<br />your school/students/fellow teachers.<br />(Please use one of the following file formats; .txt, .doc, .xls, .docx, .rtf.)</p>
                        
                        <div id="divRecommendationLetter">
                        	<span id="spanRecommendationLetter"></span><!---<input type="file" name="RecommendationLetter" id="RecommendationLetter">--->
                            
                                <input type="hidden" name="RecommendationLetter" id="RecommendationLetter" value="">
                            		<div id="flashBox" style="height:50px;">
										<script type="text/javascript">
                                            var flashvars = {};
                                            flashvars.type = "RecommendationLetter";
											flashvars.SPONSOR_USER_GUID = "#COOKIE.SPONSOR_USER_GUID#";
											flashvars.sitePath = "http://#CGI.SERVER_NAME#";
                                            var params = {};
                                            params.menu = "false";
                                            params.wmode = "transparent";
                                            params.allowScriptAccess = 'always';
                                            var attributes = {};
                                            attributes.id = "AppRecommendationLetter";
                                            swfobject.embedSWF("/swf/stem-upload.swf?super", "altContent2", "290", "50", "8.0.0", false, flashvars, params, attributes);
                                        </script>
                                        <div id="altContent2">
                                            Please upgrade your <a href="http://www.adobe.com/products/flashplayer/" target="_blank">flash player</a>.
                                        </div>
                                    </div>
                            
                            
                            
                            
                        </div>
                        
                        <br />
                        
                        <div id="btnSection">
                        	<ul>
                        	
                        		<!---input type="button" onclick="validateForm(this.form);" style="border: 0px none ; cursor: pointer; margin-left: 240px;" value="" title="Submit" class="sprite" id="btnSubmit"--->
                        	
                            	<li><input name="btnSubmit" type="button" value="" onclick="validateForm(this.form);" class="sprite" id="btnSubmit" /></li>
                                <li><input name="btnCancel" type="button" value="" class="sprite" id="btn-cancel" /></li>
                            </ul>
                            
                            <br />
                        </div>
                   </div>
                   <input type="hidden" name="hidBlnFormStarted" id="hidBlnFormStarted" value="0" />
					<script type="text/javascript">
						$('frmTeacher').observe('keydown', respondToClick);

						function respondToClick(event) {
							document.getElementById('hidBlnFormStarted').value = 1;	// GAiello - turn ON prompt, someone changed something on the form
						}
					</script>
                </form>                
            </div> ---> <!--- end of divForm --->           
        </div><!--- content single --->            
        </cfif>        
    </cfif>
</cfoutput>
