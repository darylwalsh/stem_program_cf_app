<cfsilent>
	<cfset orderBy = event.getArg("orderBy") />
	<cfif orderBy EQ "">
		<cfset orderBy = "recent" />
	</cfif>
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset qResources = event.getArg("qResources") />
	<cfset g = event.getArg("g") />
	<cfset categoryId = event.getArg("categoryId") />
	<cfset event.setArg("pageTitle","#request.siteName#") />
	<cfset lst3 = "3,4,5" />
	<cfset lst6 = "6,7,8" />
	<cfset lst9 = "9,10,11,12" />
	<cfif event.getArg("pageIndex") EQ "">
		<cfset pageIndex = "0" />
	<cfelse>
		<cfset pageIndex = event.getArg("pageIndex") />
	</cfif>
	<cfif event.getArg("recordsPerPage") EQ "">
		<cfset recordsPerPage = "5" />
	<cfelse>
		<cfset recordsPerPage = event.getArg("recordsPerPage") />
	</cfif>
	<cfset totalPages = ceiling(qResources.RecordCount / recordsPerPage) - 1 />
	<cfset startRow = (pageIndex * recordsPerPage) + 1 />
	<cfset endRow = startRow + recordsPerPage - 1 />
	<cfsavecontent variable="css">
<link rel="stylesheet" type="text/css" href="css/support.css" />
	</cfsavecontent>
	<cfset event.setArg("css",css) />
</cfsilent>
<cfoutput>
<div id="content-single">
	<h2>STEM Resources</h2>
	<div id="resource-lnav">
		<h4 class="sidebar-short">Browse by Grade</h4>
		<ul>
			<li><a title="3-5" href="#BuildUrl('showGrade','c=#c#|g=#lst3#')#" class="wedgelink <cfif ListFindNoCase(lst3,ListFirst(g))>active</cfif>">3-5</a></li>
			<li><a title="6-8" href="#BuildUrl('showGrade','c=#c#|g=#lst6#')#" class="wedgelink <cfif ListFindNoCase(lst6,ListFirst(g))>active</cfif>">6-8</a></li>
			<li><a title="9-12" href="#BuildUrl('showGrade','c=#c#|g=#lst9#')#" class="wedgelink <cfif ListFindNoCase(lst9,ListFirst(g))>active</cfif>">9-12</a></li>
		</ul>
		<h4 class="sidebar-short">Browse by Subject</h4>
		<ul>
		<cfloop index="i" from="1" to="#ArrayLen(categories)#">
			<li><a title="#categories[i].getName()#" href="#BuildUrl('showCategory','c=#c#|categoryId=#categories[i].getCategoryId()#')#" class="wedgelink <cfif categories[i].getCategoryId() EQ categoryId>active</cfif>">#categories[i].getName()#</a></li>
		</cfloop>
		</ul>
	<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
		<h4 class="sidebar-short">My Resources</h4>
		<ul>
			<li><a title="My Resources" href="#BuildUrl('showMyResources','c=#c#|u=#COOKIE.SPONSOR_USER_GUID#')#" class="wedgelink <cfif event.getArg("event") EQ "showMyResources">active</cfif>">My Resources</a></li>
		</ul>
	</cfif>
	</div><!---resource-lnav--->
	<!--BEGIN SEARCH RESULTS UI AND OUTPUT-->
	<div id="searchContent">
		<!--holds search bars and content-->
		<div id="search">
			<!--top bar paging, items per, result count-->
			<div id="topBlock">
				<span class="res">
					Results #startRow#-<cfif endRow LTE qResources.RecordCount>#endRow#<cfelse>#qResources.RecordCount#</cfif> of #qResources.RecordCount#
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span><!--class:res-->
					<span style="color:##565656;display:block;font-size:11px;left:280px;position:absolute;top:7px;">
						<cfif orderBy EQ "recent"><strong>Recently Added</strong><cfelse><a href="#BuildCurrentUrl('orderBy=recent')#">Recently Added</a></cfif> | <cfif orderBy EQ "views"><strong>Most Viewed</strong><cfelse><a href="#BuildCurrentUrl('orderBy=views')#">Most Viewed</a></cfif>
					</span>
				<span class="paging">
					Items per Page:
					<select name="selItemsPerPage" id="selItemsPerPage" class="perPage" onChange="location=this.options[this.selectedIndex].value;" >
						<option value="#BuildCurrentUrl('recordsPerPage=5')#" <cfif recordsPerPage EQ 5>selected</cfif> >5</option>
						<option value="#BuildCurrentUrl('recordsPerPage=10')#" <cfif recordsPerPage EQ 10>selected</cfif> >10</option>
						<option value="#BuildCurrentUrl('recordsPerPage=25')#" <cfif recordsPerPage EQ 25>selected</cfif> >25</option>
					</select>
					<cfloop index="pages" from="0" to="#totalPages#">
						<cfset displayPageNumber = pages + 1 />
						<cfif pageIndex EQ pages>
							<strong>#displayPageNumber#</strong>
						<cfelse>
							<a href="#BuildCurrentUrl('pageIndex=#pages#')#" class="paging_links">#displayPageNumber#</a>
						</cfif>
					</cfloop>
				</span><!--class:paging-->
			</div><!--id:topBlock-->			
			<!--Begin Results-->
			<div id="results-list">
			<cfloop query="qResources">
				<cfif CurrentRow GTE startRow>
				<div class="resultBlock">
					<h3><a href="#BuildUrl('showResource','c=#c#|resourceId=#resourceId#')#">#title#</a></h3>
					<img src="/assets/stem/thumbnails/#thumbnail#" /><br />
					#application.udf.abbreviate(body,500)#
					<span class="subject"></span>
				</div>
				</cfif>
				<cfif CurrentRow EQ endRow>
					<cfbreak />
				</cfif>
			</cfloop>
			<cfif qResources.RecordCount EQ 0>
				<div class="resultBlock">
					No Resources found.
				</div>
			</cfif>
			</div><!---result-list--->				
			<!--End Results-->
			<!--bottom bar paging, items per, result count-->	
			<div id="btmBlock">
				<span class="res">
					Results #startRow#-<cfif endRow LTE qResources.RecordCount>#endRow#<cfelse>#qResources.RecordCount#</cfif> of #qResources.RecordCount#
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span><!--class:res-->			
				<span class="paging">
					Items per Page:
					<select name="selItemsPerPage" id="selItemsPerPage" class="perPage" onChange="location=this.options[this.selectedIndex].value;" >
						<option value="#BuildCurrentUrl('recordsPerPage=5')#" <cfif recordsPerPage EQ 5>selected</cfif> >5</option>
						<option value="#BuildCurrentUrl('recordsPerPage=10')#" <cfif recordsPerPage EQ 10>selected</cfif> >10</option>
						<option value="#BuildCurrentUrl('recordsPerPage=25')#" <cfif recordsPerPage EQ 25>selected</cfif> >25</option>
					</select>
					<cfloop index="pages" from="0" to="#totalPages#">
						<cfset displayPageNumber = pages + 1 />
						<cfif pageIndex EQ pages>
							<strong>#displayPageNumber#</strong>
						<cfelse>
							<a href="#BuildCurrentUrl('pageIndex=#pages#')#" class="paging_links">#displayPageNumber#</a>
						</cfif>
					</cfloop>
				</span><!--class:paging-->			
			</div><!--id:btmBlock-->
		</div><!--id:search-->
	</div><!--id:searchContent-->	
	<!--END SEARCH RESULTS UI AND OUTPUT-->
</div><!---content-single--->
</cfoutput>