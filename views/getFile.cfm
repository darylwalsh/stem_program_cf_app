<!---
<cfif not application.cfcs.register.isSTEMUserLoggedIn(true)>
	<cflocation url="#cgi.HTTP_REFERER#" addtoken="false" />
</cfif>
--->
<cfsilent>
	<cfset resourceFile = event.getArg("file") />
	<cfset fileDirectory = GetDirectoryFromPath(ExpandPath("/assets/stem/")) />
	<cfset theFile = "#resourceFile.getFileId()#.#resourceFile.getFileExt()#" />
	<cfset theFileEXT = "#ListLast(theFile, ".")#" />
	<cfset theFileNAME = "#ListFirst(theFile, ".")#" />
	<cfset theFileNAMEUCASE = "#UCase(theFileNAME)#"/>
	<cfset theFileUCase = theFileNAMEUCASE & "." & theFileEXT />
	<cfset filePath = fileDirectory & theFileUcase />
	<cfset filename = resourceFile.getFilename() />


</cfsilent>

<!---
<cfoutput>
name: #theFileNAME#
<br/>
ext: #theFileEXT#
<br/>
fn: #theFile#
<br/>
UFN: #theFileUcase#
<br/>
#filePath#
<br/>
friendly name: #filename#

</cfoutput>
--->


<cfif fileExists(filePath)>

	
	<cfheader name="content-disposition" value="attachment; filename=""#filename#""" />
	<cfheader name="content-length" value="#resourceFile.getFileSize()#" />
	<cfcontent type="#resourceFile.getContentType()#" file="#filePath#" />
	
<cfelse>
	<a href="javascript:history.go(-1);">Back</a><br />
	Error.  File does not exist.
</cfif>

