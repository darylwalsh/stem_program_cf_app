<cfsilent>
	<cfset Cffp = CreateObject("component","cfformprotect.cffpVerify").init() />
	<cfset email = "" />
	<cfset author = "" />
	<cfif StructKeyExists(COOKIE,"FIRST_NAME") AND StructKeyExists(COOKIE,"LAST_NAME")>
		<cfset author = COOKIE.FIRST_NAME & " " & COOKIE.LAST_NAME />
	</cfif>
	<cfif StructKeyExists(COOKIE,"EMAIL")>
		<cfset email = COOKIE.EMAIL />
	</cfif>
	<cfset resource = event.getArg("resource") />
	<cfset rating = event.getArg("rating") />
	<cfset averageRating = event.getArg("averageRating") />
	<cfset resourceCategories = resource.getCategories() />
	<cfset resourceTypes = resource.getTypes() />
	<cfset resourceGrades = resource.getGrades() />
	<cfset files = resource.getFiles() />
	<cfset comments = resource.getComments() />
	<cfset blocks = resource.getBlocks() />
	<cfset c = event.getArg("c") />
	<cfset event.setArg("pageTitle","#request.siteName# - #resource.getTitle()#") />
	<cfset currentGradeIDs = "" />
	<cfloop index="i" from="1" to="#arrayLen(resourceGrades)#" step="1">
		<cfset grade = resourceGrades[i] />
		<cfset currentGradeIDs = listAppend(currentGradeIDs, grade.getGrade_id(), ",") />
	</cfloop>
	<cfif StructKeyExists(COOKIE,"SPONSOR_USER_GUID")>
		<cfset app_user_guid = COOKIE.SPONSOR_USER_GUID />
	<cfelseif StructKeyExists(COOKIE,"tempGuid")>
		<cfset app_user_guid = COOKIE.tempGuid />
	<cfelse>
		<cfcookie expires="never" name="tempGuid" value="#application.udf.CreateGUID()#" />
		<cfset app_user_guid = COOKIE.tempGuid />
	</cfif>
	<cfsavecontent variable="css">
<link rel="stylesheet" href="http://www.redactededucation.com/media/global/css/de-forms.css" type="text/css" media="all" />
<link href="<cfoutput>#application.env.staticurl#</cfoutput>global/css/ratings.css" rel="stylesheet" type="text/css" />
	</cfsavecontent>
	<cfsavecontent variable="js">
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript" src="http://www.redactededucation.com/media/global/js/validation.js"></script>
<script type="text/javascript" src="http://static.redactededucation.com/global/js/ratings.js"></script>
<script type="text/javascript">
	document.observe('dom:loaded', function() {
		try {
		ratingsInit('http://static.redactededucation.com/global/images/rating-star.jpg');
		new Validation('createCommentForm');
		new UI.Carousel("horizontal_carousel");
		}
		catch (e) {}
	});
</script>
<script type="text/javascript">
/**
*
*  URL encode / decode
*  http://www.webtoolkit.info/
*
**/
 
var Url = {
 
	// public method for url encoding
	encode : function (string) {
		return escape(this._utf8_encode(string));
	},
 
	// public method for url decoding
	decode : function (string) {
		return this._utf8_decode(unescape(string));
	},
 
	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	},
 
	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utftext.length ) {
 
			c = utftext.charCodeAt(i);
 
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 
		}
 
		return string;
	}
 
}
	function redirect(r) {
		window.location = r;
	}
	function ajaxSubmitForm(formName) {
		var form = $(formName);
		var r = Url.decode(form['r'].value);
		$('lb-invalid').hide();
		form.request({
		  onSuccess: function(transport) {
			var successText = String(transport.responseText);
			if (successText.indexOf('false')!=-1){
				$('lb-invalid').appear();
			}else{
				//clearWin();
				$('lbLoginForm').hide();
				$('lbLoading').show();
				//setTimeout(function(){redirect(r)},1000);
		  	}
		  }
		});
	}
</script>
<cfoutput>
<script type="text/javascript">
	function submitForm() {
		var valid = new Validation('createCommentForm', {onSubmit:false});
	    var result = valid.validate();
		if (result != false){
			document.createCommentForm.submit();
		}
	}
function recordRating() {
	var rate = $('my-rating').value;
	new Ajax.Request('#BuildUrl('xhr.saveRating')#', {
	method: 'get',
    parameters: {
    	rate: rate,
    	contentId: '#resource.getResourceId()#',
    	siteId: '#request.siteId#',
    	app_user_guid: '#app_user_guid#'},
    onComplete: function(xhr) {
      // optional callback
      $('rating_panel').toggle();

    }
  });
}
</script>
</cfoutput>
	</cfsavecontent>
	<cfset event.setArg("css",css) />
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
<div id="content-left">
<cfif resource.getType() EQ "resource">
	<h2>Teacher Resources</h2>
<cfelseif resource.getType() EQ "webinar">
	<h2>STEM Connect Webinar</h2>
</cfif>
	<h3 id="res-title">#resource.getTitle()#</h3>
	<!---<h4 id="res-label">description?</h4>--->
	<p id="feed"><strong>Author:</strong> #resource.getCreatedByName()# <!---a href="##"> <img src="/images/rss-feed.gif" alt="Subscribe to this Author's RSS Feed" /></a---></p>
	<br clear="all" />
	<!-- resource start -->
	#resource.getBody()#
	<!-- resource end -->
	<br clear="all" />
	<br />	
	<div class="column">
	<cfif ArrayLen(resourceCategories) GT 0>
		<strong>Subjects:</strong>&nbsp;
		<cfloop index="i" from="1" to="#arrayLen(resourceCategories)#" step="1">
			<cfif i NEQ arrayLen(resourceCategories)>
				<a title="#resourceCategories[i].getName()#" href="#BuildUrl('showCategory','c=#c#|categoryId=#resourceCategories[i].getCategoryId()#')#">#resourceCategories[i].getName()#</a>,&nbsp;
			<cfelse>
				<a title="#resourceCategories[i].getName()#" href="#BuildUrl('showCategory','c=#c#|categoryId=#resourceCategories[i].getCategoryId()#')#">#resourceCategories[i].getName()#</a>
			</cfif>
		</cfloop>
		<br />
	</cfif>
	<cfif ArrayLen(resourceGrades) GT 0>
		<strong>Grade Level:</strong>&nbsp;
		<cfset gradeCounter = 1 />
		<cfloop index="grade" list="#currentGradeIDs#">
			<cfif gradeCounter NEQ ListLen(#currentGradeIDs#)>
				<a title="#grade#" href="#BuildUrl('showGrade','c=#c#|g=#grade#')#">#grade#</a>,&nbsp;
			<cfelse>
				<a title="#grade# "href="#BuildUrl('showGrade','c=#c#|g=#grade#')#">#grade#</a>
			</cfif>
			<cfset gradeCounter++ />
		</cfloop>
	</cfif>
	</div>		
	<div class="column last">
	<cfif resource.getKeywords() NEQ "">
		<strong>Keywords:</strong>&nbsp;
		<cfset keywordCounter = 1 />
		<cfloop index="keyword" list="#resource.getKeywords()#">
			<cfif keywordCounter NEQ ListLen(#resource.getKeywords()#)>
				<a title="#keyword#" href="#BuildUrl('showSearchResults','c=#c#|criteria=#keyword#|collname=#request.siteId#')#">#keyword#</a>,&nbsp;
			<cfelse>
				<a title="#keyword#" href="#BuildUrl('showSearchResults','c=#c#|criteria=#keyword#|collname=#request.siteId#')#">#keyword#</a>
			</cfif>
			<cfset keywordCounter++ />
		</cfloop>
	</cfif>
	<cfif ArrayLen(resourceTypes) GT 0>
		<br /><strong>Types:</strong>&nbsp;
		<cfloop index="i" from="1" to="#arrayLen(resourceTypes)#" step="1">
			<cfif i NEQ arrayLen(resourceTypes)>
				<a title="#resourceTypes[i].getName()#" href="#BuildUrl('showType','c=#c#|typeId=#resourceTypes[i].getTypeId()#')#">#resourceTypes[i].getName()#</a>,&nbsp;
			<cfelse>
				<a title="#resourceTypes[i].getName()#" href="#BuildUrl('showType','c=#c#|typeId=#resourceTypes[i].getTypeId()#')#">#resourceTypes[i].getName()#</a>
			</cfif>
		</cfloop>
		<br />
	</cfif>
	</div>
	<br clear="all" />
	<br clear="all" />
<cfif ArrayLen(comments) GT 0>
	<h4 class="sidebar-light">Comments</h4>
<cfloop index="i" from="1" to="#ArrayLen(comments)#">
	<p><strong class="blog-author">#comments[i].getAuthor()# says:</strong><br />
	<em>#DateFormat(comments[i].getDtCreated())#
	#TimeFormat(comments[i].getDtCreated())#</em></p>
	<p>#comments[i].getBody()#</p>
	<span class="hr-full"></span>
</cfloop>
</cfif>
	<h4 class="sidebar-light">Leave a Comment</h4>
	<cfif event.getArg("message") NEQ "">
		#event.getArg("message")#
	</cfif>
	<div class="comment-form">
		<form id="createCommentForm" name="createCommentForm" action="#BuildUrl('processResourceCommentForm')#" method="post">
			<cfinclude template="/cfformprotect/cffp.cfm" />
				<input type="hidden" name="c" value="#c#" />
				<input type="hidden" name="resourceId" value="#resource.getResourceId()#" />
				<input type="hidden" name="comment_id" value="0" />
				<input type="hidden" name="content_id" value="#resource.getResource_id()#" />
				<input type="hidden" name="contentGuid" value="#resource.getResourceId()#" />
				<input type="hidden" name="dtCreated" value="#CreateODBCDateTime(Now())#" />
				<input type="hidden" name="ipCreated" value="#CGI.REMOTE_ADDR#" />
				<input type="hidden" name="agent" value="#CGI.HTTP_USER_AGENT#" />
				<label for="author">Name</label><br />
				<input id="author" class="required" type="text" name="author" value="#author#" /><br />
				<label for="email">Email (will not be published)</label><br />
				<input id="email" class="required validate-email" type="text" name="email" value="#email#" /><br />
				<label for="body">Comments</label><br />
				<textarea name="body" class="required" id="comments-box"></textarea><br />
				<input type="submit" id="btn-comment" class="sprite" value="" /><br />
				<!---a href="javascript:submitForm();" id="btn-comment" class="sprite"></a--->
		</form>
	</div>
</div><!---content-left--->
<div id="content-right"> 
	<ul id="res-tools">		
		<li id="ratings">		
			<div class="rate-label">Rate it!</div>
			<div id="rating-wrap">
				<ul id="ratings-container" class="de-ratings">
					<li class="f4" id="rate-1"/>
					<li class="f4" id="rate-2"/>
					<li class="f4" id="rate-3"/>
					<li class="f4" id="rate-4"/>
					<li class="f4" id="rate-5"/>
				</ul>
				<div id="rating-status">
				</div>
			</div>
			
			<br clear="all" />
			
			<!--text/elements for dialogue-->
			<div class="rating-subText">#averageRating.ratingCount# Ratings<span id="rating-text"> | Your rating: #rating.getRate()#</span></div>
			
			<!--user guid for recording rating-->
			<input type="hidden" id="my-guid" value="#app_user_guid#" disabled="disabled"/>
			
			<!--we'll fill this when the user clicks a star, you can use this for your ajax call-->
			<input type="hidden" id="my-rating" value="0"/>
			
			<!--fill this in with the assets current rating-->
			<input type="hidden" id="de-rating" value="#averageRating.avgRating#"/>
		
		</li>


	
  		<!---li id="rate">avg rating: #averageRating.avgRating# (#averageRating.ratingCount# votes)My Rating: #rating.getRate()# Rate It <a onClick="saveRating(1);" href="##">1</a>,<a onClick="saveRating(2);" href="##">2</a>,<a onClick="saveRating(3);" href="##">3</a>,<a onClick="saveRating(4);" href="##">4</a>,<a onClick="saveRating(5);" href="##">5</a></li--->
  		<li><a href="javascript:window.print()" class="sprite" id="print">Print</a></li>
  	</ul>  		
  	<br clear="all" />
	<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
		<cfif COOKIE.SPONSOR_USER_GUID EQ resource.getCreatedBy()>
		<a href="#BuildUrl('showEditResourceForm','c=#c#|resourceId=#resource.getResourceId()#')#" class="sprite" id="btn-edit">Edit</a>
		<cfif StructKeyExists(COOKIE, "SPONSOR_USER_GUID") and IsValid("guid", COOKIE.SPONSOR_USER_GUID) and COOKIE.SPONSOR_USER_GUID neq "" 
			and COOKIE.SPONSOR_USER_GUID neq "00000000-0000-0000-0000-000000000000">
			
			<a href="/index.cfm?event=showResourceForm"><img src="/img/uploadresources.png" alt="" /></a>

			<!---
				<cfquery name="getResourceCount" datasource="Sponsorships">
				SELECT * FROM cms_resources (nolock)
				WHERE 	createdBy = <cfqueryparam value="#cookie.SPONSOR_USER_GUID#" cfsqltype="cf_sql_varchar" /> AND
						dtCreated >= <cfqueryparam value="2012-09-21 12:00:00.000" cfsqltype="cf_sql_timestamp" /> <!--- only want entries from this year's giveaway --->
				</cfquery>
				<cfset request.resourcesleft = max(0,2 - getResourceCount.RecordCount) />				
				<script type="text/javascript">
					function AlertMoreResources()
					{
						<cfif request.resourcesleft eq 1>
							alert("Please upload 1 new resource before proceeding to claim your dry erase marker packs.");
						<cfelseif request.resourcesleft eq 2>
							alert("Please upload 2 new resources before proceeding to claim your dry erase marker packs.");
						</cfif>
					}
				</script>
				<br>
			--->
                <!---
                <cfif request.resourcesleft>
					<cfif request.resourcesleft is 1>
						<a href="/index.cfm?event=showResourceForm"><img src="/img/uploadanotherresource.png" alt="" /></a>
					<cfelse>
						<a href="" onclick="AlertMoreResources();return false;"><img src="/img/freedryerasemarkers.png" alt="" /></a>
					</cfif>
				<cfelse>
					<a href="/index.cfm?event=showGiveawayForm&c=37"><img src="/img/freedryerasemarkers.png" alt="" /></a> 
				</cfif>
				--->
		</cfif>
		<br />
		<br />		
		</cfif>
	</cfif>
	<cfif ArrayLen(blocks) GT 0>
		<cfloop from="1" to="#ArrayLen(blocks)#" index="i">
			#blocks[i].getBody()#
		</cfloop>
		<br clear="all" />
	</cfif>
<cfif ArrayLen(files) GT 0>
	<cfif resource.getType() EQ "resource">
		<h4 class="boxtop">Resource Files</h4>
			<div id="resource-roll">
			<cfloop index="i" from="1" to="#ArrayLen(files)#">
				<div class="block">
			<cfset downloadUrl = "index.cfm?event=getFile&f=#files[i].getFile_id()#" />
			<cfset downloadUrl = BuildUrl('getFile','f=#files[i].getFile_id()#') />
			<cfset downloadUrl = BuildCurrentUrl() />
			<cfset downloadUrl = Replace(downloadUrl,"&amp;","&","ALL") />
			<cfset r = UrlEncodedFormat(downloadUrl) />
			<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
				<a title="#files[i].getFilename()#" href="#BuildUrl('getFile','f=#files[i].getFile_id()#')#"><img src="/assets/stem/thumbnails/#files[i].getThumbnail()#" alt="#files[i].getFilename()#" class="thumb" width="100" /></a>
			<cfelse>
  				<a onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;" href="##" title="#files[i].getFilename()#"><img src="/assets/stem/thumbnails/#files[i].getThumbnail()#" alt="#files[i].getFilename()#" class="thumb" width="100" /></a>
			</cfif>
			<p>
				<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
					<a title="#files[i].getFilename()#" href="#BuildUrl('getFile','f=#files[i].getFile_id()#')#" class="file-link">#application.udf.maxLength(files[i].getFileName(),22)#</a><br />
				<cfelse>
	  				<a onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;" href="##" title="#files[i].getFilename()#" class="file-link">#application.udf.maxLength(files[i].getFileName(),22)#</a>
				</cfif>
				File Size:  #application.udf.FileSize(files[i].getFileSize())#<br />
				<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
					<a title="Download" href="#BuildUrl('getFile','f=#files[i].getFile_id()#')#">Download</a><br />
				<cfelse>
					<a title="Download" href="##" onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;">Download</a><br />
				</cfif>
		<cfswitch expression="#files[i].getFileExt()#">
			<cfcase value="3g2,3gp,asf,asx,avi,flv,m4v,mov,mp4,mpeg,mpg,qt,ram,rm,wmv">
				<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
					<a title="View" href="javascript:popupWindow('#BuildUrl('showMediaPlayer','ct=stream|f=#files[i].getFile_id()#')#',520,420);">View</a>
				<cfelse>
					<a title="View" href="##" onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;">View</a>
				</cfif>
			</cfcase>
			<cfcase value="jpeg,jpg,gif,png,bmp">
				<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
					<a title="View" href="/assets/stem/#files[i].getFileId()#.#files[i].getFileExt()#" target="_blank">View</a>
				<cfelse>
					<a title="View" href="##" onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;">View</a>
				</cfif>
			</cfcase>
		</cfswitch>
		</p>
		</div>
	</cfloop>
		<br clear="all" />
	</div><!---resource-roll--->  
	<cfelseif resource.getType() EQ "webinar">
		<h4 class="boxtop">Classroom Connections</h4>
			<div id="resource-roll">				
				 <div id="horizontal_carousel">        
					  <div class="container">
						<ul>
						<cfloop index="i" from="1" to="#ArrayLen(files)#">
							<cfset downloadUrl = BuildCurrentUrl() />
							<cfset downloadUrl = Replace(downloadUrl,"&amp;","&","ALL") />
							<cfset r = UrlEncodedFormat(downloadUrl) />
						  <li>
						<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
							<a style="display:block;float:left;width:100px;height:80px;line-height:80px;overflow:hidden;position:relative;z-index:1;" title="#files[i].getFilename()#" href="#BuildUrl('getFile','f=#files[i].getFile_id()#')#"><img src="/assets/stem/thumbnails/#files[i].getThumbnail()#" alt="#files[i].getFilename()#" class="thumb" width="100" /></a><br />
						<cfelse>
							<a style="display:block;float:left;width:100px;height:80px;line-height:80px;overflow:hidden;position:relative;z-index:1;" title="#files[i].getFilename()#" onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;" href="##"><img src="/assets/stem/thumbnails/#files[i].getThumbnail()#" alt="#files[i].getFilename()#" class="thumb" width="100" /></a><br />
						</cfif>
						<p style="margin:0px;">
							<span class="slide-name">#application.udf.maxLength(files[i].getFileName(),18)#</span>
							File Size:  #application.udf.FileSize(files[i].getFileSize())#<br />
						<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
							<a title="Download" href="#BuildUrl('getFile','f=#files[i].getFile_id()#')#">Download</a><br />
						<cfelse>
							<a title="Download" href="##" onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;">Download</a><br />
						</cfif>
				<cfswitch expression="#files[i].getFileExt()#">
					<cfcase value="3g2,3gp,asf,asx,avi,flv,m4v,mov,mp4,mpeg,mpg,qt,ram,rm,wmv">
						<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
							<a title="View" href="javascript:popupWindow('#BuildUrl('showMediaPlayer','ct=stream|f=#files[i].getFile_id()#')#',520,420);">View</a>
						<cfelse>
							<a title="View" href="##" onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;">View</a>
						</cfif>
					</cfcase>
					<cfcase value="jpeg,jpg,gif,png,bmp">
						<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
							<a title="View" href="/assets/stem/#files[i].getFileId()#.#files[i].getFileExt()#" target="_blank">View</a>
						<cfelse>
							<a title="View" href="##" onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;">View</a>
						</cfif>
					</cfcase>
				</cfswitch>
				</p>
						  </li>
						 </cfloop>
						</ul>
					  </div><!---//container--->					  
					  <br />
					  <div class="previous_button"></div><div id="showing"><!---<strong>1-2</strong> of 10--->&##160;</div><div class="next_button"></div>
					</div><!---//horizontal_carousel--->
				<br clear="all" />
				</div><!-- ///resource-roll/// -->
				
				<br clear="all" />
				
				
				<p>Got feedback on STEM Connect webinar resources? <a href="http://www.zoomerang.com/Survey/WEB22AE4B8EG2J" target="_blank">Click here</a>.</p>
				
				
				
	</cfif>
</cfif>	
</div><!---content-right--->
</cfoutput>