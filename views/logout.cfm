<cftry>
	<!--- A more potent way of clearing out cookies - 09.27.07 - RIS --->
	<cfloop collection="#cookie#" item="strCookieItem">
		<cfcookie name="#strCookieItem#" value="" expires="NOW"  />
	</cfloop>
    
    <cfset structClear(client)>
	<cfset client.hitcount = 1 />
	
	<cfset structClear(session) />
    	
    <cfcatch type="Any">
        <!--- do nothing --->
    </cfcatch>
</cftry>

<cflocation url="index.cfm" addtoken="no">