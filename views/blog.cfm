<cfsilent>
	<cfset blog = event.getArg("blogPosts") />
	<cfif StructKeyExists(blog,"params") AND IsArray(blog.params) AND ArrayLen(blog.params) EQ 1>
		<cfset posts = blog.params[1] />
	<cfelse>
		<cfset posts = ArrayNew(1) />
	</cfif>
	<cfset c = event.getArg("c") />
	<cfset numberOfPosts = event.getArg("n") />
	<cfset perPage = 5 />
	<cfif event.isArgDefined("perPage")>
		<cfset perPage = event.getArg("perPage") />
	</cfif>
	<cfset pageNo = 1 />
	<cfsavecontent variable="js">
<script type="text/javascript">
	function swapPage(hideNo,showNo) {
		var hideDiv = 'page' + hideNo;
		var showDiv = 'page' + showNo;
		$(hideDiv).hide();
		$(showDiv).appear();
	}
</script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
	<cfset event.setArg("pageTitle","#request.siteName# - Blog") />
</cfsilent>
<cfoutput>
<div id="content-left">
	<h2>STEM Blog</h2>
<div id="blogPosts">
<cfloop index="i" from="1" to="#ArrayLen(posts)#">
	<cfif posts[i].post_status EQ "publish">
		<cfif (i MOD perPage) EQ 1>
			<cfif pageNO EQ 1>
				<div id="page#pageNo#">
			<cfelse>
			<div style="text-align:center;font-weight:bold;">
				<cfif pageNO GT 2>
					<a href="##" onclick="swapPage(#pageNo-1#,#pageNo-2#);">PREVIOUS</a>
				</cfif>
				<img border="0" alt="" src="/images/page.png" />
				<a href="##" onclick="swapPage(#pageNo-1#,#pageNo#);">NEXT</a>
			</div>
				</div><!-- page#pageNo-1# -->
				<div id="page#pageNo#" style="display:none;">
			</cfif>
			<cfset pageNo++ />
		</cfif>
		<div id="post#i#">
			<h3 style="border-bottom:1px solid ##999999;padding-bottom:5px;margin-bottom:10px;"><a href="#BuildUrl('showBlogPost','postid=#posts[i].postid#|c=#c#')#">#posts[i].title#</a></h3>
			<p>#posts[i].description#</p>
			<p style="border-top:1px solid ##999999;">
				<em>Posted on #DateFormat(posts[i].date_created_gmt,"long")# by #posts[i].wp_author_display_name#</em>
				<br />
				<a href="#BuildUrl('showBlogPost','postid=#posts[i].postid#|c=#c#')###comments">Comments</a>
			</p>
		</div><!-- post#i# -->
	</cfif>
</cfloop>
<cfif pageNo GT 1>
<div style="text-align:center;font-weight:bold;">
	<a href="##" onclick="swapPage(#pageNo-1#,#pageNo-2#);">PREVIOUS</a>
	<img border="0" alt="" src="/images/page.png" />
</div>
</cfif>
	</div><!-- page#pageNo-1# -->
</div><!-- blogPosts -->
<cfif arrayLen(posts) EQ 0>
	Error retreiving blog or no posts available.
</cfif>
</div><!-- content-left -->
<div id="content-right">
	<cfinclude template="/includes/blog-sidebar.cfm" />
</div><!-- content-right -->
</cfoutput>