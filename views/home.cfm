<cfsilent>
	<cfset c = event.getArg("c") />
	<cfset blog = event.getArg("blog") />
	<cfif StructKeyExists(blog,"params") AND IsArray(blog.params) AND ArrayLen(blog.params) EQ 1>
		<cfset posts = blog.params[1] />
	<cfelse>
		<cfset posts = ArrayNew(1) />
	</cfif>
	<cfset recentResources = event.getArg("recentResources") />
	<cfset fileLoop = 3 />
	<cfif ArrayLen(recentResources) LT fileLoop>
		<cfset fileLoop = ArrayLen(recentResources) />
	</cfif>
	<cfset event.setArg("pageTitle","#request.siteName# - Home") />
</cfsilent>
<cfoutput>
<img id="tagline" src="/images/stem-tagline.jpg" alt="Empowering and Celebrating Excellence in STEM Education" width="578" height="15" />
	<div id="content-left">

    <script type="text/javascript">
			var flashvars = {};
			flashvars.config = "/swf/rotor_box.xml";

			var params = {};
			params.menu = "false";
			params.wmode = "transparent";
			params.allowScriptAccess = 'always'; //this must be here when using ajaxy type stuff

			var attributes = {};
			attributes.id = "index";
			swfobject.embedSWF("/swf/rotor_box.swf", "alt-content", "611", "292", "8.0.0", false, flashvars, params, attributes);
			
			function imageClick(url) {
    window.location = url;
}
		</script>
 

    	<div id="flash-cont" style="height:292px;position:relative; margin-bottom:14px;">
        	<div id="flash-pos" style="position:absolute;top:-6px;left:0;">
                <div id="alt-content">
                    <a id="hotbox-siemens" href="http://www.siemens-foundation.org/en/" target="_blank" title="Siemens Foundation"></a>
                    <a id="hotbox-de" href="http://www.redactededucation.com/" target="_blank" title="redacted Education"></a>
                    <a id="hotbox-orau" href="http://www.orau.org/" target="_blank" title="Oak Ridge Associated Universities"></a>
                    <a id="hotbox-cboard" href="http://siemens.collegeboard.com/" target="_blank" title="College Board"></a>

                    <p><strong>Welcome to the Siemens STEM Academy site</strong>: A premier online community designed exclusively to foster achievement in Science, Technology, Engineering, and Math (STEM) through the collaboration of STEM educators, and sharing of "best practices". <a href="/index.cfm?event=showContent&c=37" title="Browse Teacher Resources">Browse Teacher Resources</a>.</p>


                </div>
            </div>
        </div><!--//flash-cont-->
			<div class="column">


				<h4 class="green-col">STEM TEACHER SPOTLIGHT</h4>
				<a href="?event=showTeacherSpotlight" id="tile-stem-teacher-spotlight">
					<span>Check out these stand-out teachers who are creatively integrating STEM education in their classrooms and making an impact.</span>
					<span>Meat the Teachers</span>
				</a><!---tile-stem-teacher-spotlight--->


			</div>
			<div id="Programs" class="column last">
				<h4 class="green-col">Siemens Foundation Programs</h4>
				<p>Siemens is committed to supporting the next generation of scientists,
				engineers, and business leaders through multiple educational
				initiatives.</p>
				<ul style="padding:0 0 5px 0">
					<li><a href="http://www.siemens-foundation.org/en/competition.htm" target="_blank">Siemens Competition</a></li>
					<li><a href="http://siemensscienceday.redactededucation.com/" target="_blank">Siemens Science Day</a></li>
					<li><a href="http://siemens.redactededucation.com/" target="_blank">Siemens We Can Change the World Challenge</a></li>
					<li><a href="http://www.siemens-foundation.org/en/advanced_placement.htm" target="_blank">Siemens Awards for Advanced Placement</a></li>
					<!---<li><a href="http://www.siemens-foundation.org/en/teachers_scholarships.htm" target="_blank">Teacher Scholarships at HBCUs</a></li>--->
					<!--li><a href="http://www.siemens-foundation.org/en/merit_scholarship.htm" target="_blank">Merit Scholarships</a></li-->
				</ul>
			</div>
		</div><!---content-left--->
  		<div id="content-right" class="home-right">

  			<!---a href="/index.cfm?event=showContent&c=30" id="inst-sol">
      		    <span>Just Added! New Webinars -- View upcoming and access archived webinars.  See webinars.  Danny Foster featured.</span>
  			</a--->
  			<!--- <a href="index.cfm?event=showGiveaway&c=37" id="tile-shark" style="margin-top:26px;">
                <span>Take a bite out of learning -- be one of the first 100 to upload resources and get a free shark USB. (shark usb image)</span>
                <span>Upload Resources</span>
            </a> --->
			<br><br>
			<!--- <a href="/index.cfm?event=showWebsites&c=37"><img src="/img/otherstemsites.png" style="border:none;"></a> --->
			<!--- <a href="/index.cfm?event=showSTEMApplication&landing=1"><img src="/img/starsstemnowopenlink2.png" style="border:none;"></a> --->
         
		<a href="/index.cfm?event=showPage&p=programOverview"><img src="/img/2013STEMSummerOfLearning.png" alt="See Fellows" title="See Fellows"></a>
			<br><br>
		<a href="##" onClick="deGlobalWin({path:'/includes/stemwidget.cfm',closebtn:false,background:'none'}); return false;"><img src="/images/stem_widget_btn.png" alt="Daily STEM Updates" title="Daily STEM Updates"></a>	
			
			
			<div id="blog-roll">
				<h4 class="sidebar-light">Blog</h4>
				<ul class="linklist">
				<cfloop index="i" from="1" to="#ArrayLen(posts)#">
					<cfif posts[i].post_status EQ "publish">
						<li><a href="#BuildUrl('showBlogPost','c=#c#|postid=#posts[i].postid#')#">#posts[i].title#</a></li>
					</cfif>
				</cfloop>
				</ul>
			</div><!---blogroll--->

			<div id="resource-roll-home">
				<h4 class="sidebar-light">Latest Resources</h4>
				<ul>
				<cfloop index="i" from="1" to="#fileLoop#">
					<li>
					<cfset resourceFiles = recentResources[i].getFiles() />
					<cfif arrayLen(resourceFiles) GT 0>
						<a title="#recentResources[i].getTitle()#" href="#BuildUrl('showResource','resourceId=#recentResources[i].getResourceId()#')#"><img src="/assets/stem/thumbnails/#resourceFiles[1].getThumbnail()#" alt="#resourceFiles[1].getFilename()#" class="thumb" width="50" /></a>
					<cfelse>
                		<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#recentResources[i].getTitle()#" class="thumb" width="50" />
					</cfif>
						<p>
							<a title="#recentResources[i].getTitle()#" href="#BuildUrl('showResource','resourceId=#recentResources[i].getResourceId()#')#" class="file-link">#application.udf.maxLength(recentResources[i].getTitle(),22)#</a><br />
							#application.udf.abbreviate(recentResources[i].getBody(),100)#
						</p>
					</li>
				</cfloop>
				</ul>
			</div><!---resource-roll--->
		</div><!---content-right--->
</cfoutput>

<!---
   <script type="text/javascript">
alert(document.cookie);

</script>
--->