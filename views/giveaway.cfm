<cfif StructKeyExists(cookie, "SPONSOR_USER_GUID")>
	<cfquery name="getResourceCount" datasource="Sponsorships">
		SELECT * FROM cms_resources (nolock)
		WHERE 	createdBy = <cfqueryparam value="#cookie.SPONSOR_USER_GUID#" cfsqltype="cf_sql_varchar" /> AND
				dtCreated >= <cfqueryparam value="2012-09-21 12:00:00.000" cfsqltype="cf_sql_timestamp" /> <!--- only want entries from this year's giveaway --->
	</cfquery>

	<!--- <cfif getResourceCount.RecordCount lt 2> --->
		<cfset request.resourcesleft = max(0,2 - getResourceCount.RecordCount) />
	<!--- </cfif> --->
	<script type="text/javascript">
		function AlertMoreResources()
		{
			<cfif request.resourcesleft eq 1>
				alert("Please upload 1 new resource before proceeding to claim your dry erase marker packs.");
			<cfelseif request.resourcesleft eq 2>
				alert("Please upload 2 new resources before proceeding to claim your dry erase marker packs.");
			</cfif>
		}
	</script>
</cfif>

<cfset event.setArg("pageTitle","#request.siteName# - Dry Erase Marker Packs") />


<!--- <cfif StructKeyExists(request, "resourcesleft") AND request.resourcesleft gt 0>
	<cfoutput>
	<script type="text/javascript">
		function AlertMoreResources()
		{
			<cfif request.resourcesleft eq 1>
				alert("Please upload 1 new resource before proceeding to claim your dry erase marker packs.");
			<cfelseif request.resourcesleft eq 2>
				alert("Please upload 2 new resources before proceeding to claim your dry erase marker packs.");
			</cfif>
		}
	</script>
	</cfoutput>
</cfif> --->

<script type="text/javascript">
/**
*
*  URL encode / decode
*  http://www.webtoolkit.info/
*
**/

var Url = {

	// public method for url encoding
	encode : function (string) {
		return escape(this._utf8_encode(string));
	},

	// public method for url decoding
	decode : function (string) {
		return this._utf8_decode(unescape(string));
	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}

}
	function redirect(r) {
		window.location = r;
	}
	function ajaxSubmitForm(formName) {
		var form = $(formName);
		var r = Url.decode(form['r'].value);
		$('lb-invalid').hide();
		form.request({
		  onSuccess: function(transport) {
			var successText = String(transport.responseText);
			if (successText.indexOf('false')!=-1){
				$('lb-invalid').appear();
			}else{
				//clearWin();
				$('lbLoginForm').hide();
				$('lbLoading').show();
				//setTimeout(function(){redirect(r)},1000);
		  	}
		  }
		});
	}
</script>
<cflocation url="/index.cfm?event=event=showHome&c=29" addtoken="false" />
<div id="content-left">
	<h2>Make Your Mark Giveaway</h2>
<h3>Share Your Resources and Receive a Free Dry Erase Marker Pack</h3>
<br />
<h5>How It Works:</h5>
<p>Siemens STEM Academy is proud to host valuable educational resources from curriculum specialists, experts at Siemens, renowned education thought leaders, and educators like you. Your lesson plans, Power Point presentations, worksheets, and other resources help educators around the country bring STEM education to life in their classrooms.</p>
<p>To thank you for uploading resources and making the Siemens STEM Academy even stronger, we're offering free Siemens STEM Academy dry erase marker packs to the first 100 users who upload two or more resources. Get started by uploading your STEM resources today.</p>
<div>

        <h5 style="font-size:1.25em;text-transform:uppercase;">
            Step 1: Upload your resources.
        </h5>
        <p>
		Once you upload your first resource, return to this page to upload at least one 
additional resource. To complete the process, return to this page again and follow the 
instructions in Step 2 to get your dry erase marker packs. 
		</p>
		
		
        <p style="margin-bottom:50px;">
          <cfif StructKeyExists(cookie, "SPONSOR_USER_GUID") and IsValid("guid", cookie.SPONSOR_USER_GUID) and cookie.SPONSOR_USER_GUID neq "" and cookie.SPONSOR_USER_GUID neq "00000000-0000-0000-0000-000000000000">
                <a href="/index.cfm?event=showResourceForm"><img src="/img/uploadresources.png" alt="" /></a>
            <cfelse>
                <a onclick="deGlobalWin({path:'index.cfm?event=xhr.loginForm&amp;r=index%252Ecfm%253Fevent%253DshowResourceForm%2526c%253D37', closebtn:false,background:'none'}); return false;" href="#"><img src="/img/uploadresources.png" alt="" /></a>
            </cfif>
        </p>

        <h5 style="font-size:1.25em;text-transform:uppercase;">
            Step 2: Tell us about your resources.
        </h5>
        <p>
			After you've uploaded your resources, click on the button below to tell us what
			resources you uploaded and where to send your new dry erase marker packs.
        </p>
        <p style="margin-bottom:20px;">
            <cfif StructKeyExists(cookie, "SPONSOR_USER_GUID") and IsValid("guid", cookie.SPONSOR_USER_GUID) and cookie.SPONSOR_USER_GUID neq "" and cookie.SPONSOR_USER_GUID neq "00000000-0000-0000-0000-000000000000">
                <!--- <cfif StructKeyExists(request, "resourcesleft") AND request.resourcesleft lt 2> --->
				<cfif request.resourcesleft>
					<a href="" onclick="AlertMoreResources();return false;"><img src="/img/freedryerasemarkers.png" alt="" /></a>
            	<cfelse>
					<a href="/index.cfm?event=showHome&c=29"><img src="/img/freedryerasemarkers.png" alt="" /></a>
				</cfif>
			<cfelse>
                <a href="#" onclick="deGlobalWin({path:'index.cfm?event=xhr.loginForm&amp;r=index%252Ecfm%253Fevent%253DshowGiveawayForm', closebtn:false,background:'none'}); return false;"><img src="/img/freedryerasemarkers.png" alt="" /></a>
            </cfif>
            <br />
        </p>

        * While supplies last.
     </div>

</div>
<div id="content-right"><img style="padding-top: 30px;" src="/img/makeyourmarklink4.png" alt="" /></div>
