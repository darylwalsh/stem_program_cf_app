<cfsilent>
	<cfset email = "" />
	<cfset author = "" />
	<cfif StructKeyExists(cookie,"first_name") AND StructKeyExists(cookie,"last_name")>
		<cfset author = cookie.first_name & " " & cookie.last_name />
	</cfif>
	<cfif StructKeyExists(cookie,"email")>
		<cfset email = cookie.email />
	</cfif>
	<cfset blog = event.getArg("blogPosts") />
	<cfif StructKeyExists(blog,"params")>
		<cfset posts = blog.params[1] />
	<cfelse>
		<cfset posts = ArrayNew(1) />
	</cfif>
	<cfset c = event.getArg("c") />
	<cfset numberOfPosts = event.getArg("n") />
	<cfset showPosts = event.getArg("s") />
	<cfif showPosts EQ "">
		<cfset showPosts = 0 />
	</cfif>
	<cfset postId = event.getArg("postId") />
	<cfset post = StructNew() />
	<cfset post = event.getArg("blogPost") />
	<cfif ArrayLen(post.params) NEQ 1>
		<cfset pageTitle = "Sorry, no such post." />
	<cfelse>
		<cfset pageTitle = post.params[1].title />
	</cfif>
	<cfset comments = event.getArg("blogPostComments") />
	<cfset event.setArg("pageTitle","#request.siteName# - #pageTitle#") />
	<cfsavecontent variable="css">
<link rel="stylesheet" href="http://www.redactededucation.com/media/global/css/de-forms.css" type="text/css" media="all" />
	</cfsavecontent>
	<cfsavecontent variable="js">
<script type='text/javascript' language="javascript" src='http://www.redactededucation.com/media/global/js/validation.js'></script>
<script type="text/javascript" language="javascript">
	document.observe('dom:loaded', function() {
		new Validation('createCommentForm');
	});
</script>
	</cfsavecontent>
	<cfset event.setArg("css",css) />
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
<div id="content-left">		
		<h2>STEM Blog</h2>
	<cfif ArrayLen(post.params) GT 0>
		<h3 style="border-bottom:1px solid ##999999;padding-bottom:5px;margin-bottom:10px;">#post.params[1].title#</h3>
		#post.params[1].description#
		<p><em>Posted on #DateFormat(post.params[1].date_created_gmt,"long")# by #post.params[1].wp_author_display_name#</em></p>
	</cfif>
		<a name="comments"></a>
<cfif ArrayLen(comments.params[1]) GT 0>		
		<h4 class="sidebar-light">Comments</h4>		
	<cfloop index="i" from="1" to="#ArrayLen(comments.params[1])#">		
		<p><strong class="blog-author">#comments.params[1][i].author# says:</strong><br />
		<em>#DateFormat(comments.params[1][i].date_created_gmt)#
		#TimeFormat(comments.params[1][i].date_created_gmt)#</em></p>
		<p>#comments.params[1][i].content#</p>
		<span class="hr-full"></span>
	</cfloop>
</cfif>
		<h4 class="sidebar-light">Leave a Comment</h4>
		<cfif event.getArg("message") NEQ "">
			#event.getArg("message")#
		</cfif>
		<div class="comment-form">
			<form id="createCommentForm" name="createCommentForm" action="#BuildUrl('processCommentForm')#" method="post">
				<input type="hidden" name="c" value="#c#" />
				<input type="hidden" name="postId" value="#postId#" />
				<label for="author">Name</label><br />
				<input type="text" name="author" value="#author#" class="required" /><br />
				<label for="author_email">Email (will not be published)</label><br />
				<input type="text" name="author_email" value="#email#" class="required validate-email" /><br />
				<label for="content">Comments</label><br />
				<textarea name="content" class="required" id="comments-box"></textarea><br />
				<input type="submit" id="btn-comment" class="sprite" value="" /><br />
				<!---a href="javascript:submitForm();" id="btn-comment" class="sprite"></a--->
			</form>
		</div>
</cfoutput>
</div><!---content-left--->  
<div id="content-right">
	<cfinclude template="/includes/blog-sidebar.cfm" />	
</div><!---content-right--->