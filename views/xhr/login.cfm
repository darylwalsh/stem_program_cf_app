<CFHEADER name="Content-Type" value="text/xml"> 
<cfsilent>
	
<cfparam name="url.strLoginUsername" default="uid" />
<cfparam name="url.strLoginPassword" default="#CreateUUID()#" />
<cfparam name="form.strLoginUsername" default="#url.strLoginUserName#" />
<cfparam name="form.strLoginPassword" default="#url.strLoginPassword#" /> 

<cfset blnValidUser = false />

<cfif form.strLoginUsername NEQ "">
	<cfif application.cfcs.register.loginUser(form.strLoginUsername, form.strLoginPassword)>
		<cfset userData = application.cfcs.register.getSTEMUser(client.STEMRegistrationGUID) />
		<cfset blnValidUser = userData.firstName />

		<cfif ListLen(cgi.SERVER_NAME,".") GTE 2>
         	<cfset hostname = ListGetAt(cgi.SERVER_NAME,2,".") />
    	<cfelse> 
         	<cfset hostname = cgi.SERVER_NAME />
    	</cfif>
   
        <cfif FindNoCase("dev",hostname)>
            <cfset strCookieDomain = ".dev.redactededucation.com" />
        <cfelseif FindNoCase("stage",hostname)>
            <cfset strCookieDomain = ".stage.redactededucation.com" />
        <cfelseif FindNoCase("local",hostname) gt 0>
            <cfset strCookieDomain = ".local.redactededucation.com" />
        <cfelse>
            <cfset strCookieDomain = ".redactededucation.com" />
        </cfif>
		
		<cfcookie name="SPONSOR_USER_GUID" value="#userData.registrationGUID#" domain="#strCookieDomain#" path="/" expires="0.02" />
		<cfcookie name="registrationGUID" value="#userData.registrationGUID#" domain="#strCookieDomain#" path="/" expires="0.02" />
		<cfcookie name="FIRST_NAME" value="#userData.firstName#" domain="#strCookieDomain#" path="/" expires="0.02" />
		<cfcookie name="LAST_NAME" value="#userData.lastName#" domain="#strCookieDomain#" path="/" expires="0.02" />
		<cfcookie name="EMAIL" value="#userData.email#" domain="#strCookieDomain#" path="/" expires="0.02" />
	</cfif>
</cfif>

</cfsilent>


<cfif blnValidUser EQ "false">
	
<cfcontent type="text/xml" reset="yes" /><cfoutput><taconite>  
	<replaceContent select="logins">
	<div id="greeting">Hello, #blnValidUser# | <a href="#BuildUrl('logout')#">Logout</a></div>
	</replaceContent>	
	<eval><![CDATA[ 
			jQry('##login-loader').hide();
			jQry('##strLoginUsername').removeAttr("disabled");
			jQry('##strLoginPassword').removeAttr("disabled");
			jQry('##invalid').show();
         	alert("Content udpated."); 
         	alert(document.cookie);
    		]]> 
    </eval></taconite></cfoutput>

<cfelse>


<cfcontent type="text/xml" reset="yes" /><cfoutput><taconite>  


			
	<eval><![CDATA[ 
         jQry('##login-loader').hide();
        alert("Content udpated."); alert(document.cookie);
    ]]> 
    </eval></taconite></cfoutput>

</cfif>    
<!---
<cfoutput>

<cfif blnValidUser EQ "false">
	#blnValidUser#
<cfelse>
	<!---
	<div id="greeting">Hello, #blnValidUser# | <a href="#BuildUrl('logout')#">Logout</a></div>
	--->
	   <script type="text/javascript">alert(document.cookie);

</script>
</cfif>
</cfoutput>
--->