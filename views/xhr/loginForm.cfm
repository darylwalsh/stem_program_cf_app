<cfsilent>
	<cfset r = event.getArg("r") />
	<cfset programInstitute = event.getArg("programInstitute") />
	<cfset programSTARsOR = event.getArg("programSTARsOR") />
	<cfset programSTARsPN = event.getArg("programSTARsPN") />
	<cfset programPreference1 = event.getArg("programPreference1") />
	<cfset programPreference2 = event.getArg("programPreference2") />
</cfsilent>
<cfoutput>
<div>
	<div id="lb-top"><a href="##" id="lb-logout" onclick="clearWin();"></a></div>
	<div id="lb-middle">
	<div id="lbLoading" style="display:none;"><img alt='activity indicator' src='/images/loading_large.gif' /></div>
	<div id="lbLoginForm">	
	<h4>Please Login or <a href="#BuildUrl('showRegister')#">Sign Up</a> in order to proceed.</h4>	
	<p>This action requires you to be logged in.</p>	
	<br />
	<form id="lb-loginForm" name="lb-loginForm" action="#BuildUrl('xhr.login')#" method="post" onsubmit="ajaxSubmitForm(this.id); return false;">
		<input type="hidden" name="r" value="#r#" /> <!--- used to redirect a page --->
		
		<input type="hidden" name="programInstitute" value="#programInstitute#" /> <!--- used by STEM Application Landing page --->
		<input type="hidden" name="programSTARsOR" value="#programSTARsOR#" /> <!--- used by STEM Application Landing page --->
		<input type="hidden" name="programSTARsPN" value="#programSTARsPN#" /> <!--- used by STEM Application Landing page --->
		<input type="hidden" name="programPreference1" value="#programPreference1#" /> <!--- used by STEM Application Landing page --->
		<input type="hidden" name="programPreference2" value="#programPreference2#" /> <!--- used by STEM Application Landing page --->
		
		
		<div id="lb-invalid" style="display:none;color:red;margin-bottom:10px;">Invalid username or password</div>
		<h4>Email</h4>
		<input type="text" size="36" name="strLoginUsername" id="lbUid" value="" />&nbsp;&nbsp;
		<br /><br />
		<h4>Password</h4>
		<input type="password" size="36" name="strLoginPassword" id="lbPwd" value="" />
		<br /><input type="submit" value="Login" style="position:absolute; top:-5000px; left:-5000px;" />
	</form>
	<br clear="all" />
	
	<a href="##" onclick="ajaxSubmitForm('lb-loginForm'); return false;" class="sprite" id="btn-login"></a>
	<br />
	<a onClick="deGlobalWin({path:'#BuildUrl('xhr.forgotForm')#', closebtn:false,background:'none'}); return false;" href="##" title="Forgot username or password?">Forgot username or password?</a><br />
	If you don't have login, <a href="#BuildUrl('showRegister')#">please sign up now.</a>
	
	</div>
	</div>
	<div id="lb-bottom"></div>
</div>
</cfoutput>