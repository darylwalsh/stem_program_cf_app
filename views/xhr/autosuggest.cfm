<cfsilent>
	<cfset criteria = event.getArg("criteria") />
	<cfquery name="suggestions" datasource="#getProperty('sDsn')#">
		SELECT TOP 10
			r.title,
			r.resourceId
		FROM cms_resources r
		WHERE	0=0
		AND		r.siteId = '#request.siteId#'
		AND		r.title LIKE <cfqueryparam value="%#criteria#%" />
		ORDER BY r.views DESC, r.title ASC
	</cfquery>
</cfsilent>
<cfoutput>
<ul>
	<li>#criteria#</li>
<cfloop query="suggestions">
	<li id="#suggestions.resourceId#">#application.udf.abbreviate(suggestions.title,45)#</li>
</cfloop>
</ul>
</cfoutput>