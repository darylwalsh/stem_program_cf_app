<!--- <cfoutput>dsn: #application.dbinfo.strSponsorshipsDatasource#<br>

logged in: #application.cfcs.register.loginUser('paige_heskamp@redacted.com','redacted')#
<br></cfoutput>
<cfquery name="findUser" datasource="#application.dbinfo.strSponsorshipsDatasource#">
			SELECT * FROM STEMRegistrations (nolock)
			WHERE 	username = <cfqueryparam value="paige_heskamp@redacted.com" cfsqltype="cf_sql_varchar" /> AND
					password = <cfqueryparam value="redacted" cfsqltype="cf_sql_varchar" />
		</cfquery>
		<cfdump var="#findUser#">
		<cfset client.STEMRegistrationGUID=findUser.RegistrationGUID>
<cfoutput><cftry>#client.STEMRegistrationGUID#<cfcatch>#cfcatch.message#<br>#cfcatch.detail#<CFABORT></cfcatch></cftry></cfoutput>
<br>xxx
<CFABORT> --->

<cfif not application.cfcs.register.isSTEMUserLoggedIn(true)>


	<cflocation url="/index.cfm?event=showGiveaway&c=37" addtoken="false" />

</cfif>

<cfquery name="isRegistered" datasource="Sponsorships">
	SELECT * FROM STEMPlushBlanket (nolock)
	WHERE registrationGUID = <cfqueryparam value="#cookie.registrationGUID#" cfsqltype="cf_sql_varchar" />
</cfquery>
<cfif isRegistered.RecordCount gt 0>
	<cflocation url="/index.cfm?event=showGiveawayFormThanks&returning=1" addtoken="false" />
</cfif>


<cfif StructKeyExists(form, "fieldNames")>

	<cfquery name="saveEntry" datasource="Sponsorships">
		INSERT INTO STEMPlushBlanket (registrationGUID, street, city, state, zip, optInDE)
		VALUES (
					<cfqueryparam value="#cookie.registrationGUID#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#form.street#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#form.city#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#form.state#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#form.zip#" cfsqltype="cf_sql_varchar" />,

					<cfif StructKeyExists(form, "optInDE") AND form.optInDE eq "1">
						<cfqueryparam value="1" cfsqltype="cf_sql_bit" />
					<cfelse>
						<cfqueryparam value="0" cfsqltype="cf_sql_bit" />
					</cfif>
				)
	</cfquery>


	<cflocation url="/index.cfm?event=showGiveawayFormThanks" addtoken="false" />
</cfif>


<cfset event.setArg("pageTitle","#request.siteName# - Dry Erase Marker Packs") />

<link type="text/css" rel="stylesheet" href="/css/registerStyle.css" />
<link type="text/css" rel="stylesheet" href="/css/de-ui-style.css" />
<style type="text/css">
	div.registrationSection {
		margin:20px 0px 0px 100px;
	}

	div.registrationSection div.formRow {
		margin:0px 0px 15px 0px;
	}

	div.registrationSection div.err {
		float:left;
		position:relative;
		width:100px;
	}

	div.registrationSection label {
		float:left;
		margin-left:8px;
		width:170px;
	}

	div.registrationSection div.formInput {
		float:left;
		width:200px;
		position:relative;
	}

	div.registrationSection div.formInput select {
		font-size:12px;
	}

	div.registrationSection div.formInput input.topLeft {  // IE workaround
		top:0px;
		left:0px;
		position:absolute;
	}

	div.clear {
		clear:both;
		font-size:0px;
		height:0px;
		line-height:0px;
		margin:0px;
		padding:0px;
		width:100%;
	}
</style>

<script type="text/javascript">
	// Custom Validation - If elements are added/removed from the form then this needs to be updated.
	function ValidateForm (aform)
	{
		var isValid = true;

		if (Trim(aform.street.value) == "") {
			aform.street.onkeyup = function () {
				document.getElementById("streetError").className = "";
				this.onkeyup = function () {}
			}
			document.getElementById("streetError").className = "invalid";
			isValid = false;
		}

		if (Trim(aform.city.value) == "") {
			aform.city.onkeyup = function () {
				document.getElementById("cityError").className = "";
				this.onkeyup = function () {}
			}
			document.getElementById("cityError").className = "invalid";
			isValid = false;
		}
		if (aform.state.selectedIndex == 0) {
			aform.state.onchange = function () {
				document.getElementById("stateError").className = "";
				this.onchange = function () {}
			}
			document.getElementById("stateError").className = "invalid";
			isValid = false;
		}
		if (Trim(aform.zip.value) == "") {
			aform.zip.onkeyup = function () {
				document.getElementById("zipError").className = "";
				this.onkeyup = function () {}
			}
			document.getElementById("zipError").className = "invalid";
			isValid = false;
		}

		if (isValid) {
			document.getElementById("btnSubmit").style.display = "none";
			document.getElementById("processingForm").style.display = "";
			aform.submit();
		}
	}

	function Trim(astring)
	{
		return astring.replace(/^\s*/, "").replace(/\s*$/, ""); //Trim the start and end of string;
	}
</script>




<cfquery name="statesUS" dbtype="query">
	SELECT * FROM Application.qCountriesStates
	WHERE COUNTRY_CODE = <cfqueryparam value="US" cfsqltype="cf_sql_varchar" />
   	ORDER BY STATENAME
</cfquery>


<cfquery name="getResourceCount" datasource="Sponsorships">
	SELECT * FROM cms_resources (nolock)
	WHERE 	createdBy = <cfqueryparam value="#cookie.SPONSOR_USER_GUID#" cfsqltype="cf_sql_varchar" /> AND
			dtCreated >= <cfqueryparam value="2010-11-01 15:00:00.000" cfsqltype="cf_sql_timestamp" /> <!--- only want entries from this year's giveaway --->
</cfquery>



<div id="content-left">
	<h2>DRY ERASE MARKER GIVEAWAY</h2>
	<h3>Share Your Resources and Receive a Pack of Dry Erase Markers For Your Classroom</h3>
	<br />

	<p>
		Please complete the following form after sharing two or more resources. If you are one of the first 100 to do so, we will send you a Siemens STEM Academy dry erase marker pack! 

		<cfif getResourceCount.RecordCount lt 2>
			<br /><br />
			<cfset toDo = 2 - getResourceCount.RecordCount />
			You still need to share <cfoutput>#toDo#</cfoutput> more resource<cfif toDo gt 1>s</cfif>.
		</cfif>
	</p>
	<br />

	<h3 class="icon-title" style="width:430px;">
		<img src="/images/icons-person.gif" alt="icon" />
		<span>Mailing Address</span>
	</h3>
	<div class="clear">&nbsp;</div>

	<cfoutput>
	<form method="post" name="frmLabCoat" id="frmLabCoat">
	<div class="registrationSection">
		<div class="formRow">
			<div class="err">
				<span id="streetError"></span> &nbsp;
			</div>

			<label for="street">Street:</label>
			<div class="formInput">
				<input class="topLeft" type="text" name="street" id="street" value="" maxlength="200" />
			</div>

			<div class="clear">&nbsp;</div>
		</div>

		<div class="formRow">
			<div class="err">
				<span id="cityError"></span> &nbsp;
			</div>

			<label for="city">City:</label>
			<div class="formInput">
				<input class="topLeft" type="text" name="city" id="city" value="" maxlength="100" />
			</div>

			<div class="clear">&nbsp;</div>
		</div>

		<div class="formRow">
			<div class="err">
				<span id="stateError"></span> &nbsp;
			</div>

			<label for="state">State:</label>
			<div class="formInput">
				<select name="state" id="state">
					<option value="">- Choose a State -</option>
					<cfloop query="statesUS">
						<option value="#statesUS.stateCode#">#statesUS.stateName#</option>
					</cfloop>
				</select>
			</div>

			<div class="clear">&nbsp;</div>
		</div>

		<div class="formRow">
			<div class="err">
				<span id="zipError"></span> &nbsp;
			</div>

			<label for="zip">Zip:</label>
			<div class="formInput">
				<input class="topLeft" type="text" name="zip" id="zip" value="" maxlength="5" />
			</div>

			<div class="clear">&nbsp;</div>
		</div>


		<div class="formRow">
			<div class="err">
				&nbsp;
			</div>

			<label for="optInDE">Opt In to receive redacted Education emails:</label>
			<div class="formInput">
				<input class="topLeft" type="checkbox" name="optInDE" id="optInDE" value="1" />
			</div>

			<div class="clear">&nbsp;</div>
		</div>

	</div>
	</cfoutput>

	<div class="registrationSection">
		<div class="formRow">
			<br />
			<input id="btnSubmit" class="sprite" type="button" title="Submit" value="" style="border:0px; cursor:pointer; margin:0px 0px 0px 300px;" onclick="ValidateForm(this.form);" />

			<span id="processingForm" style="display:none;"><div style="margin:10px 0px 9px 338px;"><img src="/images/ajax-greenWait.gif" /></div></span>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
	</form>

</div>
<div id="content-right"><!--- <img src="images/coat2.jpg" alt="" /> ---></div>

