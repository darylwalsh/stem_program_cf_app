<div id="mdrBase"><div id="mdrStage" align="left"><div id="mdrWrap"><div id="mdrWrap2" style="position:relative;">

	<cfinvoke component="cfcs.MDR" method="getStates" returnvariable="qStates"></cfinvoke>

	<cfquery name="qGlobalState" dbtype="query">
		SELECT * FROM Application.qCountriesStates
		WHERE COUNTRY_CODE = 'US'
	    ORDER BY STATENAME
	</cfquery>

	<h2>School Search</h2>

	<cfif NOT isDefined("url.demo")>
		&nbsp;&nbsp;<b>**Note: School change requests, can take up to 5-10 working days. US Territories please manually add your school below. </b><br /><br />
	</cfif>

	<label for="selState">State:</label>
	<select name="selState" id="selState" onChange="getCity(this.value)" class="temp">
		<option value="">- choose one -</option>
		<cfloop query="qStates">
			<cfoutput><option value="#mstate#">#region_name#</option></cfoutput>
		</cfloop>
		<option value="">US Territories</option>
	</select>

	<div id="processing" style="display:none; position:absolute; top:76px; left:118px; width:145px;">
		<div style="position:absolute; top:9px; left:280px; width:145px;">
			<img src="../images/ajax-greenWait.gif" alt="Wait" style="position:relative;top:3px;" /> &nbsp;Please Wait...
		</div>
	</div>

	<div style="clear:both;height:12px;"></div>

	<label for="selCity">City:</label>
	<select name="selCity" id="selCity" disabled onChange="getSchool(this.value)" class="temp">
		<option value="">- N/A -</option>
	</select>
	<div style="clear:both;height:12px;"></div>


	<label for="selCity">School(s):</label>
	<div style="float:left;">
		<div style="width:678px; background:#666;border-right:1px #d8d8d7 solid;">
			<table cellspacing="0" class="MyData">
				<thead>
					<tr class="resHead">
						<td width="260" align="center">School</td>
						<td width="184" align="center">Address</td>
						<td width="144" align="center">City, State</td>
						<td width="63" align="center">ZipCode</td>
					</tr>
				</thead>
			</table>
		</div>

		<div style="width:678px; background:#f6f6f6;border-right:1px #d8d8d7 solid;">
			<div id="tableWrap" style="height:140px; border-bottom:2px #666 solid; overflow-y:auto; overflow-x:hidden;">
				<span id="myTableData">
					<table cellspacing='0' class='MyData' width="100%" style="background:#f6f6f6;">
						<tr>
							<td align="center" style="border:none; width:100%;">
								<br /><br />
								Please select a State/City above...
								<br /><br />
							</td>
						</tr>
					</table>
				</span>
			</div>
		</div>
	</div>

	<div class="clear" style="clear:both;height:12px;"></div>


	<h2>Can't Find Your School or Organization?</h2>
	If you can not find your school or organization after using the search above, then enter it below. Please use a physical address rather than a P.O. box.

	<div id="mdrWrap3">

		<div class="registrationSection" style="margin:20px 0px 0px 0px;">
			<div class="formRow">
				<div class="err">
					<span id="span_txtSchoolName"></span>
					&nbsp;
				</div>
				<label for="txtSchoolName">Name:</label>
				<div class="formInputSmall">
					<input name="txtSchoolName" id="txtSchoolName" type="text" class="temp" size="43" maxlength="255" onChange="hideError(this.name);" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>

			<div class="formRow">
				<div class="err">
					<span id="span_txtSchoolAddress"></span>
					&nbsp;
				</div>
				<label for="txtSchoolAddress">Address:</label>
				<div class="formInputSmall">
					<input name="txtSchoolAddress" id="txtSchoolAddress" type="text" class="temp" size="43" maxlength="255" onChange="hideError(this.name);" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>

			<div class="formRow">
				<div class="err">
					<span id="span_txtSchoolCountry"></span>
					&nbsp;
				</div>
				<label for="txtSchoolCountry">Country:</label>
				<div class="formInputSmall">
					<select name="txtSchoolCountry" id="txtSchoolCountry" class="temp" onchange="hideError(this.name); getRegions();">
						<option value="">- Select One -</option>

						<cfoutput>
						<cfloop query="application.qCountries">
							<option value="#application.qCountries.country_code#">#application.qCountries.country_name#</option>
						</cfloop>
						</cfoutput>
					</select>
				</div>
				<div class="clear">&nbsp;</div>
			</div>

			<div class="formRow">
				<div class="err">
					<span id="span_txtSchoolState"></span>
					&nbsp;
				</div>
				<label for="txtSchoolState">State:</label>
				<div class="formInputSmall">
					<select name="txtSchoolState" id="txtSchoolState" class="temp">
						<option value="">- Select a Country -</option>
					</select>
				</div>
				<div class="clear">&nbsp;</div>
			</div>

			<div class="formRow">
				<div class="err">
					<span id="span_txtSchoolCity"></span>
					&nbsp;
				</div>
				<label for="txtSchoolCity">City:</label>
				<div class="formInputSmall">
					<input name="txtSchoolCity" id="txtSchoolCity" type="text" class="temp" size="43" maxlength="255" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>

			<div class="formRow">
				<div class="err">
					<span id="span_txtSchoolZip"></span>
					&nbsp;
				</div>
				<label for="txtSchoolZip">Postal Code:</label>
				<div class="formInputSmall">
					<input name="txtSchoolZip" id="txtSchoolZip" type="text" class="temp" style="width:50px;" maxlength="20" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>

			<div class="formRow">
				<div class="err">
					<span id="span_txtSchoolPhone"></span>
					&nbsp;
				</div>
				<label for="txtSchoolPhone">Phone:</label>
				<div class="formInputSmall">
					<input name="txtSchoolPhone" id="txtSchoolPhone" type="text" class="temp" size="43" maxlength="50" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		</div>

		<div align="center" style="margin-top:20px;">
			<input name="btnSave" type="button" value="Save" onClick="validateCantFindForm();" class="temp" />
		</div>
	</div>


</div></div></div></div>