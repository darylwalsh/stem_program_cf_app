<cfsilent>
	<cfset pageTitle = "" />
	<cfset orderBy = event.getArg("orderBy") />
	<cfif orderBy EQ "">
		<cfset orderBy = "recent" />
	</cfif>
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset types = event.getArg("types") />
	<cfset qResources = event.getArg("qResources") />
	<cfset g = event.getArg("g") />
	<cfif g NEQ "">
		<cfset pageTitle = "- Grades #g#" />
	</cfif>
	<cfset categoryId = event.getArg("categoryId") />
	<cfset category = event.getArg("category") />
	<cfif IsValid("guid",categoryId)>
		<cfset pageTitle = "- #category.getName()# Resources" />
	</cfif>
	<cfset typeId = event.getArg("typeId") />
	<cfset type = event.getArg("type") />
	<cfif IsValid("guid",typeId)>
		<cfset pageTitle = "- #type.getName()#" />
	</cfif>
	<cfset event.setArg("pageTitle","#request.siteName# #pageTitle#") />
	<cfset lst3 = "3,4,5" />
	<cfset lst6 = "6,7,8" />
	<cfset lst9 = "9,10,11,12" />
	<cfif event.getArg("pageIndex") EQ "">
		<cfset pageIndex = "0" />
	<cfelse>
		<cfset pageIndex = event.getArg("pageIndex") />
	</cfif>
	<cfif event.getArg("recordsPerPage") EQ "">
		<cfset recordsPerPage = "10" />
	<cfelse>
		<cfset recordsPerPage = event.getArg("recordsPerPage") />
	</cfif>
	<cfset totalPages = ceiling(qResources.RecordCount / recordsPerPage) - 1 />
	<cfset startRow = (pageIndex * recordsPerPage) + 1 />
	<cfset endRow = startRow + recordsPerPage - 1 />
	<cfsavecontent variable="css"><link rel="stylesheet" type="text/css" href="css/support.css" /></cfsavecontent>
	<cfset event.setArg("css",css) />
</cfsilent>
<cfoutput>
<div id="content-single">
	<h2>Teacher Resources</h2>
	<div id="resource-lnav">
		<h4 class="sidebar-short">Browse by Grade</h4>
		<ul>
			<li><a title="3-5" href="#BuildUrl('showGrade','c=#c#|g=#lst3#')#" class="wedgelink <cfif ListFindNoCase(lst3,ListFirst(g))>active</cfif>">3-5</a></li>
			<li><a title="6-8" href="#BuildUrl('showGrade','c=#c#|g=#lst6#')#" class="wedgelink <cfif ListFindNoCase(lst6,ListFirst(g))>active</cfif>">6-8</a></li>
			<li><a title="9-12" href="#BuildUrl('showGrade','c=#c#|g=#lst9#')#" class="wedgelink <cfif ListFindNoCase(lst9,ListFirst(g))>active</cfif>">9-12</a></li>
		</ul>
		<h4 class="sidebar-short">Browse by Subject</h4>
		<ul>
		<cfloop index="i" from="1" to="#ArrayLen(categories)#">
			<li><a title="#categories[i].getName()#" href="#BuildUrl('showCategory','c=#c#|categoryId=#categories[i].getCategoryId()#')#" class="wedgelink <cfif categories[i].getCategoryId() EQ categoryId>active</cfif>">#categories[i].getName()#</a></li>
		</cfloop>
		</ul>
		
        <h4 class="sidebar-short">Browse by Type</h4>
		<ul>
		<cfloop index="i" from="1" to="#ArrayLen(types)#">
			<li><a title="#types[i].getName()#" href="#BuildUrl('showType','c=#c#|typeId=#types[i].getTypeId()#')#" class="wedgelink <cfif types[i].getTypeId() EQ typeId>active</cfif>">#types[i].getName()#</a></li>
		</cfloop>
		</ul>
	<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
		<h4 class="sidebar-short">My Resources</h4>
		<ul>
			<li><a title="My Resources" href="#BuildUrl('showMyResources','c=#c#|u=#cookie.sponsor_user_guid#')#" class="wedgelink <cfif event.getArg("event") EQ "showMyResources">active</cfif>">My Resources</a></li>
		</ul>
	</cfif>
	</div><!---resource-lnav--->
	<!--BEGIN SEARCH RESULTS UI AND OUTPUT-->
	<div id="searchContent">
		<!--holds search bars and content-->
		<div id="search">
			<!--top bar paging, items per, result count-->
			<div id="topBlock">
            	<!---<div id="top-btns">
                <a href="#BuildCurrentUrl('orderBy=views')#" id="btn-most" class='sprite <cfif orderBy EQ "views">active</cfif>'></a>&nbsp;<a href="#BuildCurrentUrl('orderBy=recent')#" id="btn-recent" class='sprite <cfif orderBy EQ "recent">active</cfif>'></a>
                	<div class="clear"></div>
                </div><!--//top-btns-->--->
            
            	<div style="position:relative;">
                    <span class="res">
                        Results #startRow#-<cfif endRow LTE qResources.RecordCount>#endRow#<cfelse>#qResources.RecordCount#</cfif> of #qResources.RecordCount#
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                    </span><!--class:res-->	
					<span style="color:##565656;display:block;font-size:11px;left:280px;position:absolute;top:7px;">
						<cfif orderBy EQ "recent"><strong>Recently Added</strong><cfelse><a href="#BuildCurrentUrl('orderBy=recent')#">Recently Added</a></cfif> | <cfif orderBy EQ "views"><strong>Most Viewed</strong><cfelse><a href="#BuildCurrentUrl('orderBy=views')#">Most Viewed</a></cfif>
					</span>
                    <cfsavecontent variable="paging">
                    <span class="paging">
                    Items per Page:
                        <select name="selItemsPerPage" id="selItemsPerPage" class="perPage" onChange="location=this.options[this.selectedIndex].value;" >
                            <option value="#BuildCurrentUrl('recordsPerPage=5')#" <cfif recordsPerPage EQ 5>selected</cfif> >5</option>
                            <option value="#BuildCurrentUrl('recordsPerPage=10')#" <cfif recordsPerPage EQ 10>selected</cfif> >10</option>
                            <option value="#BuildCurrentUrl('recordsPerPage=25')#" <cfif recordsPerPage EQ 25>selected</cfif> >25</option>
                        </select>
                        &nbsp;&nbsp;&nbsp;&nbsp;
						<cfset hellip=true>
                        <cfloop index="pages" from="0" to="#totalPages#">
                            <cfset displayPageNumber = pages + 1 />
                            <cfif pageIndex EQ pages>
                                <strong>#displayPageNumber#</strong>
                                <cfset hellip=true>
                            <cfelseif pages eq 0 OR pages eq totalPages or Abs(pageIndex-pages) lte 2>
                                <a href="#BuildCurrentUrl('pageIndex=#pages#')#" class="paging_links">#displayPageNumber#</a>
                                <cfset hellip=true>
                            <cfelseif hellip>
                            	&hellip;
                                <cfset hellip=false>
                            </cfif>
                        </cfloop>
                    </span><!--class:paging-->
                    </cfsavecontent>
                    #paging#
                </div><!--//position:relative-->
                
			</div><!--id:topBlock-->			
			<!--Begin Results-->
			<div id="results-list">
			<cfloop query="qResources">
				<cfif CurrentRow GTE startRow>
				<div class="resultBlock">
					<h3><a href="#BuildUrl('showResource','c=#c#|resourceId=#resourceId#')#">#title#</a></h3>                    
                    <div class="resBlockLeft">
	                    <a href="#BuildUrl('showResource','c=#c#|resourceId=#resourceId#')#" class="resThumbWrap">
	                        <div class="resThumbCrop">
							<cfif thumbnail NEQ "">
        	                	<img src="/assets/stem/thumbnails/#thumbnail#" alt="#title#" />
							<cfelse>
                	        	<img src="/assets/stem/thumbnails/defaultThumb.gif" alt="#title#" />
							</cfif>
                        	</div><!--//resThumbCrop-->
	                    </a><!--//resWrap-->
                    </div>
                    <div class="resBlockRight">                    
						#application.udf.abbreviate(body,500)#
                    </div>                    
                    <div class="clear"></div>
					<span class="subject" style="color:##0D6296;">Keywords: #keywords#</span>
					<span class="subject" style="color:##0D6296;">Views: #views#</span>
				</div>
				</cfif>
				<cfif CurrentRow EQ endRow>
					<cfbreak />
				</cfif>
			</cfloop>
			</div><!---result-list--->				
			<!--End Results-->
			<!--bottom bar paging, items per, result count-->	
			<div id="btmBlock">
				<span class="res">
					Results #startRow#-<cfif endRow LTE qResources.RecordCount>#endRow#<cfelse>#qResources.RecordCount#</cfif> of #qResources.RecordCount#
				</span><!--class:res-->
				#paging#	
				<br clear="all" />
			</div><!--id:btmBlock-->
		</div><!--id:search-->
	</div><!--id:searchContent-->	
	<!--END SEARCH RESULTS UI AND OUTPUT-->
</div><!---content-single--->

</cfoutput>