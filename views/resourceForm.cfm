<cfsilent>
	<!--- <cfif NOT application.cfcs.register.isSTEMUserLoggedIn(true)>
		<cflocation url="/index.cfm?event=showResourceLanding" addtoken="false" />
	</cfif> --->
	
	<cfset event.setArg("pageTitle", "#request.siteName# - Upload Resource") />
	<cfset resource = event.getArg("resource") />
	<cfset resourceId = application.udf.CreateGUID() />
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset types = event.getArg("types") />
	<cfset grades = event.getArg("grades") />
	<cfset app_user_guid = "00000000-0000-0000-0000-000000000000" />
	<cfif StructKeyExists(COOKIE,"SPONSOR_USER_GUID")>
		<cfset app_user_guid = COOKIE.SPONSOR_USER_GUID />
	</cfif>
	<cfset createdByName = "Anonymous" />
	<cfif StructKeyExists(COOKIE,"FIRST_NAME") AND StructKeyExists(COOKIE,"LAST_NAME")>
		<cfset createdByName = "#COOKIE.FIRST_NAME# #COOKIE.LAST_NAME#" />
	</cfif>
	<cfsavecontent variable="css">
<link rel="stylesheet" href="http://www.redactededucation.com/media/global/css/de-forms.css" type="text/css" media="all" />
	</cfsavecontent>
	<cfsavecontent variable="js">
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	plugins : "safari,advlink,inlinepopups,style",
	
	// Theme Options
	theme_advanced_buttons1 : "link,unlink,|,bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,forecolor,fontselect,fontsizeselect,|,bullist,numlist,|,outdent,indent",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : ""
	
	//content_css : "/css/stemacademy.css"
});
function submitForm() {
tinyMCE.triggerSave();
document.createResourceForm.submit();
}
</script>
<script type='text/javascript' language="javascript" src='http://www.redactededucation.com/media/global/js/validation.js'></script>
<script type="text/javascript" language="javascript">
	document.observe('dom:loaded', function() {
		new Validation('createResourceForm');
	});
</script>
	</cfsavecontent>
	<cfset event.setArg("css",css) />
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
		<div id="content-single">		
			<h2>Upload Teacher Resources</h2>	
			
			<p><strong>Follow these simple steps to upload your favorite STEM related resources to share with your peers</strong>. Please provide as much detail as possible for others to easily identify your asset.</p>		
			#event.getArg("message")#
			<div id="hold-form">		
			<form name="createResourceForm" id="createResourceForm" action="#BuildUrl('processResourceForm','c=#c#')#" method="post">
				<input type="hidden" name="type" value="resource" />
				<input type="hidden" name="siteId" value="#request.siteId#" />
				<input type="hidden" name="resourceId" value="#resourceId#" />
				<input type="hidden" name="dtCreated" value="#CreateODBCDateTime(Now())#" />
				<input type="hidden" name="ipCreated" value="#CGI.REMOTE_ADDR#" />
				<input type="hidden" name="createdBy" value="#app_user_guid#" />
				<input type="hidden" name="createdByName" value="#createdByName#" />
			<table align="center">
			<tr id="top">
				<td>
					<br />
					<table cellpadding="0" cellspacing="0" width="520" align="center" class="dems_forms">
						<tr>
							 <td align="right" valign="top"  width="100" style="padding-right:8px;" class="field_title">
								 *Title:
							 </td>
							 <td align="left">
								 <input class="required" name="title" type="text" style="width:367px;" value="#resource.getTitle()#" id="title" />
							 </td>
						 </tr>
						<tr>
							 <td align="right" valign="top" width="100" style="padding-right:8px;" class="field_title">
								 *Description:
							 </td>
							 <td align="left">
								<textarea class="required" name="body" style="width:400px;height:300px;">#resource.getBody()#</textarea>
							 	<p>Please add any applicable URLs to your description. <a href="##" onClick="deGlobalWin({path:'/includes/hyperlinking.cfm',closebtn:false,background:'none'}); return false;">Find out how to insert URLs into your text</a>.</p>			 
							 </td>			
						 </tr>
						<tr>
							 <td align="right" valign="top"  width="100" style="padding-right:8px;" class="field_title">
								 *Keywords:
							 </td>
							 <td align="left">
								 <input class="required" name="keywords" type="text" style="width:367px;" value="#resource.getKeywords()#" id="keywords" />
							 </td>
						 </tr>
						<tr>
							<td align="right" valign="top" width="100" style="padding-right:8px;" class="field_title">
								*Subject(s):
							</td>
							<td>
							<div>
							<div class="column" id="subjectChecks">
							<cfset variables.newDiv = false />
                            <cfset subCheckRequired = "">
							<cfloop index="i" from="1" to="#arrayLen(categories)#">
								
								<!---set class on last input--->
                                <cfif i eq #arrayLen(categories)#>
                                    <cfset subCheckRequired = "validate-one-required">    
                                </cfif>
							
							<cfif i EQ arrayLen(categories)>
								<input type="checkbox" name="categoryIDs" value="#categories[i].getCategoryId()#" class="#subCheckRequired#" />
							<cfelse>
								<input type="checkbox" name="categoryIDs" value="#categories[i].getCategoryId()#" class="#subCheckRequired#" />
							</cfif>
								<label for="categoryIDs">#categories[i].getName()#</label>
								
                                <!---after 2 sets, clear the floats--->
                                <cfif NOT BitAnd( i, 1 )>
                                <div style="clear:both;"></div>
                                </cfif>
                                
							</cfloop>
							<cfif variables.newDiv EQ false>
							</div>
							</cfif>
							</div>
							</td>
						</tr>
						<tr>
							<td align="right" valign="top" width="100" style="padding-right:8px;" class="field_title">
								*Type(s):
							</td>
							<td>
							<div>
							<div class="column" id="typeChecks">
							<cfset variables.newDiv = false />
                            <cfset subCheckRequired = "">
							<cfloop index="i" from="1" to="#arrayLen(types)#">
								
								<!---set class on last input--->
                                <cfif i eq #arrayLen(types)#>
                                    <cfset subCheckRequired = "validate-one-required">    
                                </cfif>
							
							<cfif i EQ arrayLen(types)>
								<input type="checkbox" name="typeIDs" value="#types[i].getTypeId()#" class="#subCheckRequired#" />
							<cfelse>
								<input type="checkbox" name="typeIDs" value="#types[i].getTypeId()#" class="#subCheckRequired#" />
							</cfif>
								<label for="typeIDs">#types[i].getName()#</label>
								
                                <!---after 2 sets, clear the floats--->
                                <cfif NOT BitAnd( i, 1 )>
                                <div style="clear:both;"></div>
                                </cfif>
                                
							</cfloop>
							<cfif variables.newDiv EQ false>
							</div>
							</cfif>
							</div>
							</td>
						</tr>
						<tr>
							 <td align="right" valign="top" width="100" style="padding-right:8px;" class="field_title">
								 *Grade Level:
							 </td>			
							 <td align="left" valign="top">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td align="left" width="154" valign="top">
											<div>
											<select class="required" name="gradeIDs" multiple="multiple" id="gradeIDs" style="width:100px;" size="5">
											<cfloop index="i" from="1" to="#arrayLen(grades)#">
												<option value="#grades[i].getGrade_id()#">#grades[i].getDescription()#</option>
											</cfloop>
											</select>
											</div>
										</td>
										<td class="gray_10" align="left" valign="middle">
											Hold CTRL and click to select multiple grades.
										</td>
									</tr>
								</table>
							 </td>
						 </tr>
						 <tr>			
							<td colspan="2">
								<img src="/mediashare/images/spacer.gif"  width="1" height="6" border="0" />
							</td>
						</tr>						
					</table>			
				</td>
			</tr>
			<tr id="abbr_top" style="display:none;">
				<td class="gray_light" align="left">
					Complete the required fields to add your resource
				</td>
			</tr>		
			<tr id="sec_3">			
				<td align="right">
					<input type="submit" value="" class="sprite" id="btn-goto-two" title="Continue to Step 2" />
					<!---a href="javascript:submitForm();" class="sprite" id="btn-goto-two" title="Continue to Step 2"></a--->
					<a href="##" class="sprite" id="btn-cancel" title="Cancel"></a>					
				</td>
			</tr>
			</table>
			</form>
			</div><!---hold-form--->
		</div><!---content-single--->
</cfoutput>