<cfsilent>
	<cfset feed = event.getArg("f") />
	<cfswitch expression="#feed#">
		<cfcase value="popular">
			<cfset orderBy = "views" />
		</cfcase>
		<cfcase value="recent">
			<cfset orderBy = "dtCreated" />
		</cfcase>
		<cfdefaultcase>
			<cfset orderBy = "dtCreated" />		
		</cfdefaultcase>
	</cfswitch>
</cfsilent>
<cfquery name="qItems" datasource="#getProperty('sDsn')#">
	SELECT 
		dtCreated AS publishedDate,
		body AS content,
		title AS title,
		'#application.env.stem_url#/index.cfm?event=showResource&c=37&resourceId=' + CAST(resourceId AS VARCHAR(36)) AS rsslink,
		resourceId AS id
	FROM cms_resources
	WHERE 0=0
	AND type = 'resource'
	AND siteId = <cfqueryparam value="#request.siteId#" />
	ORDER BY #orderBy# DESC
</cfquery>
<!--- replace any bad chars --->
<cfloop query="qItems">
   <cfset fixedcontent = replaceList(content, "#chr(19)#,#chr(25)#", "") />
   <cfset querySetCell(qItems, "content", fixedcontent, currentRow) />
</cfloop>
<!--- rss feed metadata --->
<cfset meta = StructNew() />
<cfset meta.version = "rss_2.0" />
<cfset meta.title = "Siemens STEM Academy" />
<cfset meta.link = "http://stem.redactededucation.com" />
<cfset meta.description = "Siemens STEM Academy" />
<cfset meta.pubDate = "#Now()#" />
<cfset meta.image = StructNew() />
<cfset meta.image.title = "Siemens STEM Academy" />
<cfset meta.image.url = "http://www.redactededucation.com/images/de_header/HUB/logo.jpg" />
<cfset meta.image.link = meta.link />
<!--- create the feed --->
<cffeed action="create" properties="#meta#" query="#qItems#" xmlVar="result" />
<cfset result = application.udf.UnicodeWin1252(result) />
<!--- output the xml to the client --->
<cfcontent type="text/xml" reset="true"><cfoutput>#result#</cfoutput>