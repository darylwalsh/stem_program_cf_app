
<cfif not StructKeyExists(cookie, "SPONSOR_USER_GUID") or not IsValid("guid", cookie.SPONSOR_USER_GUID) or cookie.SPONSOR_USER_GUID eq "" or cookie.SPONSOR_USER_GUID eq "00000000-0000-0000-0000-000000000000">
	<cflocation url="/index.cfm?event=showLabCoat" addtoken="false" />
</cfif>


<cfset event.setArg("pageTitle","#request.siteName# - Lab Coat Giveaway") />


<div id="content-left">
	<h2>Lab Coat Giveaway</h2>
	<h3>Share Your Resources and Receive a Free Lab Coat</h3>
	<br />

	<p>
		<cfif not StructKeyExists(url, "returning")>
			Thank you for submitting your resources. <br />
			We will mail your lab coat shortly.
		<cfelse>
			Your lab coat request has been received and we will mail your lab coat shortly. Thank you for your participation.
		</cfif>
	</p>

</div>
<div id="content-right"><img src="images/coat2.jpg" alt="" /></div>
