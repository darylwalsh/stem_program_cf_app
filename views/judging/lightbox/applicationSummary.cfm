
<cfsavecontent variable="cssScripts">
	<link rel="stylesheet" style="text/css" href="/views/judging/css/judging.css" media="all" />
	<style type="text/css">
		html, body {
			height:auto !important;
			height:100%;
			min-height:100%;
			margin:0; padding:0;
			font-size:11px;
			font-family:Tahoma, Arial, Helvetica, sans-serif;margin:0; padding:0;
		}

		div.tabContent div.pod{
			padding-left:25px;
		}
	</style>
</cfsavecontent>
<cfset event.setArg("css", cssScripts) />
<cfset event.setArg("pageTitle", "#request.contestYear# Siemens STEM Academy Institute and STARs Judging") />


<cfset tabs = [] />

<cfset tab = {} />
<cfset tab.title = "EDUCATION AND EXPERIENCE" />
<cfset tab.pods = [] />
<cfset ArrayAppend(tab.pods, "citizenship") />
<cfset ArrayAppend(tab.pods, "education") />
<cfset ArrayAppend(tab.pods, "currentPosition") />
<cfset ArrayAppend(tab.pods, "pastPositions") />
<cfset ArrayAppend(tab.pods, "responsibilities") />
<cfset ArrayAppend(tab.pods, "activities") />
<cfset ArrayAppend(tabs, tab) />

<cfset tab = {} />
<cfset tab.title = "ACHIEVEMENTS" />
<cfset tab.pods = [] />
<cfset ArrayAppend(tab.pods, "honorsAwards") />
<cfset ArrayAppend(tabs, tab) />

<cfset tab = {} />
<cfset tab.title = "REFERENCE" />
<cfset tab.pods = [] />
<cfset ArrayAppend(tab.pods, "letterRecommendation") />
<cfset ArrayAppend(tabs, tab) />

<cfset tab = {} />
<cfset tab.title = "ESSAYS" />
<cfset tab.pods = [] />

<cfset ArrayAppend(tab.pods, "essaysInstitute") />
<cfparam name="url.stemType" default="">
<cfif url.stemType is "STARs">
	<cfset ArrayAppend(tab.pods, "essaysSTARs") />
</cfif>
<!--- <cfif client.STEMJudgeType eq "adminPreInst" OR client.STEMJudgeType eq "adminFinalInst">
	<cfset ArrayAppend(tab.pods, "essaysInstitute") />
<cfelseif client.STEMJudgeType eq "adminPreStar" OR client.STEMJudgeType eq "adminFinalStar">
	<cfset ArrayAppend(tab.pods, "essaysSTARs") />
</cfif> --->

<cfset ArrayAppend(tab.pods, "essaysAdditional") />
<cfset ArrayAppend(tabs, tab) />

<cfset tab = {} />
<cfset tab.title = "VIDEO" />
<cfset tab.pods = [] />
<cfset ArrayAppend(tab.pods, "uploadVideo") />
<cfset ArrayAppend(tabs, tab) />

<cfset settings = StructNew() />
<cfset settings.dsn = "Sponsorships" />


<cfoutput>
<div class="summary">
	<div style="font-size:1.20em; font-weight:bold; margin-bottom:15px;">
		Display ID ###url.displayID#
	</div>
	<cfloop from="1" to="#ArrayLen(tabs)#" index="tabIndex">
		<div class="tabTitleLeft">&nbsp;</div>
		<div class="tabTitleCenter">
			<div class="padding">
				#tabs[tabIndex].title#
			</div>
		</div>
		<div class="tabTitleRight">&nbsp;</div>
		<div class="clear">&nbsp;</div>

		<div class="tabContent">
			<cfloop from="1" to="#ArrayLen(tabs[tabIndex].pods)#" index="i">
				<cfset curSection = CreateObject("component", "cfcs.tabbedForm.pods." & tabs[tabIndex].pods[i]).Init(settings) />

				<div style="color:##323232; font-size:14px; font-weight:bold; margin:10px;">
					#curSection.GetPodTitle()#
				</div>
				<cfif tabs[tabIndex].pods[i] eq "letterRecommendation">

					<cfquery name="getID" datasource="#application.dbinfo.strSponsorshipsDatasource#">
						SELECT approvedGUID FROM STEMAppsJudgingApproved (nolock)
						WHERE applicationGUID = <cfqueryparam value="#url.applicationGUID#" cfsqltype="cf_sql_varchar" />
					</cfquery>
	     			<cfset rs = application.judging.GetRecommendation(getID.approvedGUID) />

					<div style="margin:0px 25px;">
						<strong>Recommendation submitted by:</strong><br />
						#rs.firstName# #rs.lastName#, #rs.title#<br />
						#rs.institution#<br />
						#rs.street#<br />
						<cfif Len(rs.street2)>#rs.street2#<br /></cfif>
						#rs.city#, #rs.state#  #rs.zip#<br />
						#rs.phone#<br />
						#rs.email#<br /><br />

						<p><strong>How do you know applicant?:</strong> <br />#rs.capacityKnowApplicant#</p>
						<p><strong>Describe Applicant:</strong><br />#rs.describeApplicant#</p>
						<p><strong>Classroom Instructional Skills:</strong> #rs.categoryClassroomInstructionalSkills#<br />
						<strong>Subject Matter Knowledge:</strong> #rs.categorySubjectMatterKnowledge#<br />
						<strong>Initiative and Self-reliance:</strong> #rs.categoryInitiativeAndSelfReliance#<br />
						<strong>Creativity:</strong> #rs.categoryCreativity#<br />
						<strong>Integrity:</strong> #rs.categoryIntegrity#<br />
						<strong>Leadership Skills:</strong> #rs.categoryLeadershipSkills#<br />
						<strong>Interpersonal Skills:</strong> #rs.categoryInterpersonalSkills#<br />
						<strong>Oral Communication Skills:</strong> #rs.categoryOralCommunicationSkills#<br />
						<strong>Written Communication Skills:</strong> #rs.categoryWrittenCommunicationSkills#<br />
						<strong>Teamwork Skills:</strong> #rs.categoryTeamworkSkills#</p>
					</div>

				<cfelse>
					#curSection.BuildPod(url.applicationGUID, "summary")#
				</cfif>
			</cfloop>
		</div>
	</cfloop>
</div>
</cfoutput>

