

<cfif StructKeyExists(url, "approvedGUID") AND StructKeyExists(url, "judgeGUID")>

	<link rel="stylesheet" style="text/css" href="/views/judging/css/judging.css" media="all" />

	<cfset judgeScores = application.judging.GetJudgeScores(url.judgeGUID, url.approvedGUID) />

	<cfquery name="getJudge" datasource="Sponsorships">
		SELECT firstName, lastName FROM STEMAppsJudges (nolock)
		WHERE judgeGUID = <cfqueryparam value="#url.judgeGUID#" cfsqltype="cf_sql_varchar" />
	</cfquery>

	<cfquery name="getDisplayID" datasource="Sponsorships">
		SELECT displayID FROM STEMAppsJudgingApproved (nolock)
		WHERE approvedGUID = <cfqueryparam value="#url.approvedGUID#" cfsqltype="cf_sql_varchar" />
	</cfquery>

	<cfoutput>
	<cfset weightedScore = 0 />
	<table class="scoreCard" border="0" cellspacing="0" cellpadding="0">
		<tr class="headRow">
			<td colspan="9">
				#getJudge.firstName# #getJudge.lastName# scores for Display ID ###getDisplayID.displayID#
			</td>
		</tr>
		<tr class="titleRow">
			<td style="width:240px;">&nbsp;</td>
			<!--- <td style="width:74px;">Ineligible</td> --->
			<td style="width:74px;">Poor</td>
			<td style="width:74px;">Weak</td>
			<td style="width:74px;">Good</td>
			<td style="width:74px;">Great</td>
			<td style="width:74px;">Outstanding</td>
			<td style="width:74px;">Weight</td>
			<td style="width:74px; font-size:1.25em; text-decoration:underline;">Score</td>
		</tr>
		
		<tr class="values">
			<td style="text-align:right;"><strong>Letter of Recommendation:</strong></td>
			<cfset score = 0 />
			<cfloop from="1" to="5" index="i">
				<td>
					<input type="radio" name="scoreLetter" value="#i#" <cfif judgeScores.scoreLetter eq i>checked="checked"</cfif> disabled="disabled" />
					#i#
				</td>

				<cfif judgeScores.scoreLetter eq i>
					<cfset score = 6*.15*i />
				</cfif>
			</cfloop>
			<td style="color:##444444;">
				15%
			</td>
			<td id="letterScore" style="font-size:1.20em; text-align:left;">
				&nbsp;&nbsp;&nbsp;
				#score#
				<cfset weightedScore = weightedScore + score />
			</td>
		</tr>

		
		<tr class="values">
			<td style="text-align:right;"><strong>Essay ##1:</strong></td>
			<cfset score = 0 />
			<cfloop from="1" to="5" index="i">
				<td>
					<input type="radio" name="scoreEssay1" value="#i#" <cfif judgeScores.scoreEssay1 eq i>checked="checked"</cfif> disabled="disabled" />
					#i#
				</td>

				<cfif judgeScores.scoreEssay1 eq i>
					<cfset score = 6*.30*i />
				</cfif>
			</cfloop>
			<td style="color:##444444;">
				30%
			</td>
			<td id="essay1Score" style="font-size:1.20em; text-align:left;">
				&nbsp;&nbsp;&nbsp;
				#score#
				<cfset weightedScore = weightedScore + score />
			</td>
		</tr>

		<tr class="values">
			<td style="text-align:right;"><strong>Essay ##2:</strong></td>
			<cfset score = 0 />
			<cfloop from="1" to="5" index="i">
				<td>
					<input type="radio" name="scoreEssay2" value="#i#" <cfif judgeScores.scoreEssay2 eq i>checked="checked"</cfif> disabled="disabled" />
					#i#
				</td>

				<cfif judgeScores.scoreEssay2 eq i>
					<cfset score = 6*.30*i />
				</cfif>
			</cfloop>
			<td style="color:##444444;">
				30%
			</td>
			<td id="essay2Score" style="font-size:1.20em; text-align:left;">
				&nbsp;&nbsp;&nbsp;
				#score#
				<cfset weightedScore = weightedScore + score />
			</td>
		</tr>
		
		<tr class="values">
			<td style="text-align:right;"><strong>Video:</strong></td>
			<cfset score = 0 />
			<cfloop from="1" to="5" index="i">
				<td>
					<input type="radio" name="scoreVideo" value="#i#" <cfif judgeScores.scoreVideo eq i>checked="checked"</cfif> disabled="disabled" />
					#i#
				</td>

				<cfif judgeScores.scoreVideo eq i>
					<cfset score = 6*.25*i />
				</cfif>
			</cfloop>
			<td style="color:##444444;">
				25%
			</td>
			<td id="videoScore" style="font-size:1.20em; text-align:left;">
				&nbsp;&nbsp;&nbsp;
				#score#
				<cfset weightedScore = weightedScore + score />
			</td>
		</tr>

		

		<tr class="results">
			<td colspan="8" style="text-align:right;">APPLICATION SCORE:</td>
			<td id="totalScore" style="font-weight:bold;font-size:1.35em;text-align:center;">
				#weightedScore#
			</td>
		</tr>

	</table>

	<cfif Trim(judgeScores.comment) neq "">
		<div class="question questionSpacer">
			Comments or notes regarding this application:
		</div>
		<div class="question questionSpacer" style="font-size:12px;">
			#judgeScores.comment#
		</div>
	</cfif>

	</cfoutput>


</cfif>

