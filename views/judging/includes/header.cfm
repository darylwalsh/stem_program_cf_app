
<cfif not StructKeyExists(application, "judging") OR StructKeyExists(url, "reinit") OR StructKeyExists(url, "reload") OR StructKeyExists(url, "restart")>
	<cfset application.judging = CreateObject("component", "cfcs.judging").Init() />
</cfif>

<cfif not application.judging.IsLoggedIn()>
	<cflocation url="/index.cfm?event=judge.showLogin" addtoken="false" />
</cfif>
<cfif client.STEMJudgeType eq "Institute" OR client.STEMJudgeType eq "adminPreInst" OR client.STEMJudgeType eq "adminFinalInst">
	<cfset variables.programName='institute'>
<cfelseif client.STEMJudgeType eq "STARs" OR client.STEMJudgeType eq "adminPreStar" OR client.STEMJudgeType eq "adminFinalStar">
	<cfset variables.programName='STARs'>
<cfelse>
	<!--- Should never happen --->
	<cflocation url="/index.cfm?event=judge.showLogin" addtoken="false" />
</cfif>



<cfsavecontent variable="cssScripts">
	<link rel="stylesheet" style="text/css" href="/views/judging/css/judging.css" media="all" />
	<link type="text/css" href="/views/judging/css/redmond/jquery-ui-1.8.6.custom.css" rel="stylesheet" />
	<link type="text/css" href="/views/judging/js/fancybox/jquery.fancybox-1.3.1.css" rel="stylesheet" />
</cfsavecontent>

<cfsavecontent variable="jsScripts">
	<script type="text/javascript" src="/views/judging/js/jquery-1.4.3.min.js"></script>
	<script type="text/javascript" src="/views/judging/js/jquery-ui-1.8.6.custom.min.js"></script>
	<script type="text/javascript" src="/views/judging/js/fancybox/jquery.fancybox-1.3.1.js"></script>


	<script type="text/javascript" src="/views/judging/js/simpleValidation.js"></script>

	<script type="text/javascript" src="/views/judging/js/judging.js"></script>
	<script type="text/javascript">
		// first make sure jQuery doesn't conflict with any other existing APIs.
		jQuery.noConflict();  // now any jQuery scripts must replace '$' with 'jQuery'
	</script>
</cfsavecontent>


<cfset event.setArg("css", cssScripts) />
<cfset event.setArg("js", jsScripts) />
<cfset event.setArg("pageTitle", "#request.contestYear# Siemens STEM Academy Institute and STARs Judging") />

<cfparam name="request.sectionDescription" default="[need to set request.sectionDescription]" />

<cfoutput>
<div class="judge">
	<div id="judgeHead">
		<h2 class="title">
			Siemens STEM Academy Judging
		</h2>
		<div class="titleSub" style="float:left; width:75%;">
			A SIEMENS FOUNDATION INITIATIVE
		</div>
		<cfsavecontent variable="jname">		
		<cfif StructKeyExists(client,"STEMJudgeFirstName")><b>Welcome #client.STEMJudgeFirstName#</b>&nbsp;&nbsp;</cfif>		
		<cfif StructKeyExists(client,"STEMJudgeType")>[#client.STEMJudgeType# Judge]&nbsp;&nbsp;</cfif>		
		Logout
		</cfsavecontent>
		<div style="float:right; text-align:right; margin-right:10px;">
			<a href="/index.cfm?event=judge.showLogin&judgeLogout=1"><nobr>#variables.jname#</nobr></a>
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<div class="formWrap">
		<div class="formWrapTopLeft">&nbsp;</div>
		<div class="formWrapTop">
			<img src="/views/judging/images/judgeTitleCenter.png" />
			<div class="formWrapTopText">&nbsp; #request.ContestYear# SIEMENS STEM ACADEMY: #request.sectionDescription#</div>
		</div>
		<div class="formWrapTopRight">&nbsp;</div>
		<div class="clear">&nbsp;</div>

		<div class="formWrapMiddle">
			<div style="width:100%;">

				<br />

				<div id="judgeNav" class="nav">
					<div class="navLeft">
						<div class="navTopLeft">
							<div class="arrows">
								<div class="arrow">
									<img src="/views/judging/images/navArrow.png" />
								</div>
								<div class="arrow">
									<img src="/views/judging/images/navArrow.png" />
								</div>
								<div class="arrow">
									<img src="/views/judging/images/navArrow.png" />
								</div>
								<div class="arrow">
									<img src="/views/judging/images/navArrow.png" />
								</div>
							</div>
						</div>
						<div id="navMiddleLeft" class="navMiddleLeft navMiddleLeft4">&nbsp;</div>
						<div class="navBottomLeft">&nbsp;</div>
						<div class="clear">&nbsp;</div>
					</div>

					<div class="navRight">

						<div class="navLink navLinkFirst">
							<div class="padding">
								<a href="/index.cfm?event=judge.showLogin">Judging Home</a>
							</div>
						</div>
						<div class="navLink navLinkMiddle" onclick="OpenLightBox('/STEMApplication/lightboxes/#variables.programName#Timeline.html');">
							<div class="padding">
								Timeline
							</div>
						</div>
                        <div class="navLink navLinkMiddle" onclick="OpenLightBox('/STEMApplication/lightboxes/#variables.programName#EligibilityCriteria.html');">
                            <div class="padding">
                                Eligibility Criteria
                            </div>
                        </div>
						<div class="navLink navLinkLast" onclick="OpenLightBoxWH('/STEMApplication/lightboxes/#variables.programName#JudgingRubric.html', 800, 500);">
							<div class="padding">
								Judging Rubric
							</div>
						</div>

						<div class="clear">&nbsp;</div>
					</div>
					<div class="clear">&nbsp;</div>
				</div>
</cfoutput>