<CFPROCESSINGDIRECTIVE SUPPRESSWHITESPACE="Yes">
<cfif Not StructKeyExists(client,"STEMJudgeType")>
	<cflocation url="/index.cfm?event=judge.showLogin" addtoken="0">
</cfif>
<cfif Not listfindnocase("adminPreInst,adminPreStar,adminFinalInst,adminFinalStar",client.STEMJudgeType)>
	<cflocation url="/" addtoken="0">
</cfif>

<cfsetting requesttimeout=1000>
<cfif StructKeyExists(url,"backtopre")>
	<cfquery datasource="Sponsorships">
	DELETE FROM STEMAppsJudgeScores	
	DELETE FROM STEMAppsJudgingApproved	
	DELETE FROM STEMAppsJudgeRecuse
	UPDATE STEMAppsJudges
		SET requiredToScore = 0
		WHERE judgeYear = #request.contestYear#
	</cfquery>
</cfif>
			
<script type="text/javascript">	
	function SubmitForm ()
	{
		jQuery("#saveBtn").css("display", "none");
		jQuery("#saveWorkingImg").css("display", "");
		jQuery("#adminPreApproval").submit();
	}	
</script>
<style>
	.infomsg {
		color:red;
	}
	.bdr {
		border-style:solid;
		border-color:#bbb;
		text-align:center;
		height:26px;
		padding: 0 0 0 5px;
	}	
	.oddrow {
		color:#236d98;
		background-color:#fff;
	}
	.evenrow {
		color:#236d98;
		background-color:#e5e5e5;
	}			
	.oddrow.same {
		color:#6e9823;
	}
	.evenrow.same {
		color:#989023;
	}
</style>

<cfoutput>
<cfset request.sectionDescription = "Administrator Pre-Approval" />
<cfinclude template="../includes/header.cfm" />

<cfif StructKeyExists(form, "fieldNames")><!--- 0 --->
	<cfquery datasource="Sponsorships" name="saveApproval">
	SET NOCOUNT ON;
	<cfloop list="#form.fieldNames#" index="fn">
		<cfif IsValid("guid", fn)>					
			UPDATE STEMAppsJudgingApproved
			SET isApproved = <cfqueryparam value="#form[fn]#" cfsqltype="cf_sql_integer" />,
			isOpen = 0,
			openUser = ''
			WHERE approvedGUID = <cfqueryparam value="#fn#" cfsqltype="cf_sql_varchar" />
			and isOpen = 1
		</cfif>
	</cfloop>
	</cfquery>
	
	<cfquery datasource="Sponsorships" name="approvals">
	SELECT count(*) as count
	FROM STEMAppsJudgingApproved WITH (NOLOCK)
	WHERE applicationYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" />
	and isApproved IN (2,3)<!--- 2=maybe - 3=not graded --->
	</cfquery>
	<cfset msg="Save Complete . . .">
	<cfif Not approvals.count><!--- 1 --->
		<cfset okGo=1>			
		<cfset msg = msg & " . . . And ALL Applications have been assigned to <b>Institute</b> and <b>STARs</b> judges.">
		<cfinclude template="assign_apps_to_judges.cfm">		
	</cfif><!--- 1 --->	
	<cflocation url="/index.cfm?event=judge.approveApplications&savemsg=#variables.msg#&dummy=#CreateUUID()#" addtoken="0">
	<CFABORT>
<cfelse><!--- 0 --->
	<cfquery datasource="Sponsorships" name="getApps">
	SELECT allApps.applicationGUID, allApps.programInstitute, allApps.programSTARsOR, allApps.programSTARsPN
	FROM (
			SELECT sa.applicationGUID, sa.programInstitute, sa.programSTARsOR, sa.programSTARsPN, ja.isApproved, ja.applicationYear, rs.sheetGUID
			FROM STEMApplications AS sa WITH (NOLOCK)
				LEFT JOIN STEMAppsJudgingApproved AS ja WITH (NOLOCK) ON sa.applicationGUID = ja.applicationGUID
				LEFT JOIN STEMAppsRecommendationSheets AS rs WITH (NOLOCK) ON rs.applicationGUID = sa.applicationGUID
			WHERE 	sa.allTabsCompleted IS NOT NULL 
			AND sa.created >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />
			AND sa.allTabsCompleted >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />
			<!--- AND IsDate(rs.created) = 1
			AND sa.videoUTube is not null --->
		) AS allApps
	WHERE allApps.applicationYear IS NULL
	</cfquery>	
	<cfquery datasource="Sponsorships">
	SET NOCOUNT ON;
	<cfset cnt=0>
	<cfloop query="getApps">
		<cfset progType="">
		<cfif getApps.programInstitute is 1>
			<cfset progType="Institute">
		</cfif>
		<cfif getApps.programSTARsOR is 1 or getApps.programSTARsPN is 1>
			<cfset progType=ListAppend(variables.progType,"STARs")>
		</cfif>		
		<cfloop list="#variables.progType#" index="ii">
			<cfset cnt ++>
			INSERT INTO STEMAppsJudgingApproved (applicationGUID, applicationYear, type, displayID)
			VALUES (
					<cfqueryparam value="#getApps.applicationGUID#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" />,				
					'#ii#',
					#variables.cnt#
					)
		</cfloop>
	</cfloop>
	</cfquery>
</cfif><!--- 0 --->

<cfquery datasource="Sponsorships" name="instApps">
<cfif StructKeyExists(url,"openUser")>
	UPDATE STEMAppsJudgingApproved
	SET isOpen = 0
	WHERE openUser = '#url.openUser#'
</cfif>
SELECT applicationGUID,approvedGUID,displayID,type,isApproved	
FROM STEMAppsJudgingApproved WITH (NOLOCK)
WHERE applicationGUID IN (	
	Select Top 10 applicationGUID
	FROM STEMAppsJudgingApproved WITH (NOLOCK)
	WHERE applicationYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer">
	and isApproved IN (2,3)
	and 
		((
		isOpen = 1 and openUser = '#client.STEMJudgeGUID#'
		or
		isOpen = 0		
		))
	Order by isOpen desc,isApproved desc
	)
AND isApproved IN (2,3)
ORDER BY
	isApproved desc,applicationGUID,type
</cfquery>
<cfquery datasource="Sponsorships" name="openApps">
<cfif instApps.recordcount>
	Update STEMAppsJudgingApproved
	SET isOpen = 1,
	openDate = getDate(),
	openUser = '#client.STEMJudgeGUID#'
	WHERE applicationGUID IN (#quotedValueList(instApps.applicationGUID)#)
</cfif>
	SELECT Distinct a.openUser,isNull(b.firstname + ' ' + b.lastname,'Unknown Judge') as judgename
	<!--- a.openDate --->
	FROM STEMAppsJudgingApproved a WITH (NOLOCK)
		left join STEMAppsJudges b WITH (NOLOCK) on convert(varchar(50),b.judgeGUID) = a.openUser
	WHERE a.isOpen = 1 
	and openUser != '#client.STEMJudgeGUID#'
</cfquery>
<cfquery datasource="Sponsorships" name="appCnts">
SELECT
	(select count(*) from STEMAppsJudgingApproved WITH (NOLOCK)) as totalcnt,
	(select count(*) from STEMAppsJudgingApproved WITH (NOLOCK)
		where isApproved = 3) as remainingcnt,
	(select count(*) from STEMAppsJudgingApproved WITH (NOLOCK)
		where isApproved = 1) as approvedcnt,
	(select count(*) from STEMAppsJudgingApproved WITH (NOLOCK)
		where isApproved = 0) as deniedcnt,
	(select count(*) from STEMAppsJudgingApproved WITH (NOLOCK)
		where isApproved = 2) as maybecnt
</cfquery>
<cfif listfindnocase("adminPreStar,adminFinalStar",client.STEMJudgeType)>
	<cfset appType = "STARs" />
<cfelse>
	<cfset appType = "Institute" />
</cfif>

	<div style="margin-bottom:12px; font-size:14px; color:##000000;">
		<b>#request.contestYear# #UCase(variables.appType)# APPLICATIONS</b>
	</div>
	<span style="float:right;margin:-28px 20px 10px 0;"><a href="/index.cfm?event=judge.setupFinalists">Go to Finalists</a></span>
	
<form id="adminPreApproval" action="#cgi.script_name#?event=judge.approveApplications" method="post">
<div class="content">
	<div class="section">
		<div class="left">&nbsp;</div>
		<div class="middle">			
				#request.contestYear# STARs & Institute Applicants						
		</div>
		<div class="right">&nbsp;</div>
		<div class="clear">&nbsp;</div>		
		<cfif StructKeyExists(url,"savemsg")>
			<br><span class="infomsg">&nbsp;&nbsp;&nbsp;#url.savemsg#</span>
		</cfif>		
		<div class="pods">
			<div class="clear">&nbsp;</div>
			
			
			<div class="pod">				
				<div class="clear">&nbsp;</div>
		<span style="color:##005284;font-weight:bold;">
		<span>Remaining: #appCnts.remainingcnt#</span>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<span>Approved: #appCnts.approvedcnt#</span>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<span>Denied: #appCnts.deniedcnt#</span>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<span>Maybe: #appCnts.maybecnt#</span>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<span>Total: #appCnts.totalcnt#</span>
		</span>
		<table border="0" cellpadding="0" cellspacing="0" style="width:673px;margin:0px;">
		<tr style="font-weight:bold;font-size:11px;color:##fff;background-color:##555;">
			<td class="bdr" style="border-width:1px 1px 1px 1px;width:310px;text-align:left;">									
				APPLICANT
				<!--- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span class="oddrow same">&nbsp;This Color&nbsp;</span> & <span class="evenrow same">&nbsp;This Color&nbsp;</span> = Dual Submission --->
			</td>
			<td class="bdr" style="border-width:1px 1px 1px 0px;width:70px;">									
				TYPE
			</td>
			<td class="bdr" style="border-width:1px 1px 1px 0px;width:70px;">									
				APPROVE
			</td>
			<td class="bdr" style="border-width:1px 1px 1px 0px; width:70px;">									
				DENY
			</td>
			<td class="bdr" style="border-width:1px 1px 1px 0px; width:70px;">									
				MAYBE
			</td>
		</tr>	
		</table>
		<div class="podContent" style="width:671px; height:240px; overflow:scroll; overflow-x:hidden;">
		<cfif Not instApps.recordcount and Not openApps.recordcount>
			<br><span class="infomsg">&nbsp;&nbsp;&nbsp;There are Zero applications remaining for pre-approval!</span>
		</cfif>
		<table border="0" cellpadding="0" cellspacing="0" style="width:671px;margin:0px;">
		<cfif Not instApps.recordcount and openApps.recordcount>
		<tr>
		<td>&nbsp;</td>
		<td>
		<br><br>Listed below are judges who have <b>Pre Approval Applications</b> open.<br>
		All applications must be Approved / Denied before judging can move to phase 2,<br>
		which is the assigning of the applications to Institute & STARs judges for scoring.<br>
		To open these applications click the link next to desired judge.
		<br><br>
		</td></tr>		
		<cfloop query="openApps">
		<tr>
			<td>&nbsp;</td>
			<td>
			<a href="/index.cfm?event=judge.approveApplications&openUser=#openApps.openUser#">Click Here</a> 
			to open the applications of #openApps.judgename#
			</td>			
		</tr>
		</cfloop>
		</cfif>
					<cfset cnt=0>
					<cfset reget=1>				
						<cfloop query="instApps">							
							<cfif reget>
								<cfset cnt++>
								<cfif variables.cnt mod 2>					
									<cfset classname="oddrow">
								<cfelse>
									<cfset classname="evenrow">
								</cfif>
							</cfif>				
							<cfif instApps.applicationGUID is instApps.applicationGUID[instApps.currentrow+1]>
								<cfset showhr=0>
								<cfset reget=0>
								<cfset classname="#variables.classname# same">
							<cfelse>
								<cfset showhr=1>
								<cfset reget=1>
								<cfif instApps.currentrow gt 1 and instApps.applicationGUID is instApps.applicationGUID[instApps.currentrow-1]>
									<cfset classname="#variables.classname# same">
								</cfif>								
							</cfif>
							
		<tr class="#variables.classname#">
			<td class="bdr" style="border-width:0px 1px <cfif variables.showhr>1<cfelse>0</cfif>px 0px;width:310px;text-align:left;">									
				<a href="javascript:Void();" onclick="OpenLightBoxWH('/index.cfm?event=judge.displayApplicationSummary&applicationGUID=#instApps.applicationGUID#&displayID=#instApps.displayID#&stemType=#instApps.type#', 760, 600);" class="#variables.classname#"><nobr>Display ID ###numberformat(instApps.displayID,"0000")#
<small>&nbsp;||&nbsp;&nbsp;#instApps.applicationGUID#</small></nobr></a> 				
			</td>
			<td class="bdr" style="border-width:0px 1px <cfif variables.showhr>1<cfelse>0</cfif>px 0px;width:70px;">
				#instApps.type#	
			</td>
			<td class="bdr" style="border-width:0px 1px <cfif variables.showhr>1<cfelse>0</cfif>px 0px;width:70px;">
				<input type="radio" name="#instApps.approvedGUID#" value="1" checked />										
			</td>
			<td class="bdr" style="border-width:0px 1px <cfif variables.showhr>1<cfelse>0</cfif>px 0px;width:70px;">				
				<input type="radio" name="#instApps.approvedGUID#" value="0" <cfif instApps.isApproved is 0>checked</cfif> />					
			</td>
			<td class="bdr" style="border-width:0px 0px <cfif variables.showhr>1<cfelse>0</cfif>px 0px;width:70px;">				
				<input type="radio" name="#instApps.approvedGUID#" value="2" <cfif instApps.isApproved is 2>checked</cfif> />					
			</td>
		</tr>
						</cfloop>
					</table>
					
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div class="clear">&nbsp;</div>
			
			<cfif instApps.recordcount>
			<div style="height:50px; margin:5px auto 0px auto; width:80px;">
				#application.judging.BuildButton("saveBtn", "Save", "SubmitForm();")#
				<img id="saveWorkingImg" src="/views/judging/images/formProcessing2.gif" style="display:none; margin:4px 0px 0px 15px;" />
			</div>
			</cfif>
		</div>
	</div>
	
	<div class="clear">&nbsp;</div>
</div>
</form>

<cfinclude template="../includes/footer.cfm" />

<cfif StructKeyExists(url,"backtopre")>
	<script>alert("Judging has been reset and the Pre-Approval Silo has been loaded!");</script>
</cfif>

</cfoutput></CFPROCESSINGDIRECTIVE>