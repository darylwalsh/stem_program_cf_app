<CFPROCESSINGDIRECTIVE SUPPRESSWHITESPACE="Yes"><cfoutput>
<cfif Not listfindnocase("adminPreInst,adminPreStar,adminFinalInst,adminFinalStar",client.STEMJudgeType)>
	<CFABORT>
</cfif>
<cfparam name="url.string" default="">
<cfparam name="url.action" default="">
<cfparam name="url.stemType" default="">
<cfparam name="url.newJudge" default="">
<cfparam name="url.oldJudge" default="">
<cfparam name="url.infomsg" default="">
<cfif StructKeyExists(form,"fieldnames") and url.action is "update"><!--- 1 --->	
	<cfif IsValid("guid", form.newJudge) and IsValid("guid", form.oldJudge) and 
			form.newJudge neq form.oldJudge><!--- 2 --->
		<cfparam name="form.moveAmt" default="">
		<cfif Not isNumeric(form.moveAmt)>
			<cfset form.moveAmt=1>
		</cfif>
		<cfquery datasource="Sponsorships" name="getEmail">
		WITH j AS
        (
	        SELECT TOP (#form.moveAmt#) judgeGUID, status
	        FROM STEMAppsJudgeScores WITH (NOLOCK)
	        WHERE judgeGUID = '#form.oldJudge#'
			AND judgeGUID != '#form.newJudge#'
			AND status != 'Scored'
        )
		UPDATE j
		SET judgeGUID = '#form.newJudge#',
			status = 'ToBeJudged'
		
			UPDATE STEMAppsJudges
				SET requiredToScore = isNull(
										(
										Select count(*) From STEMAppsJudgeScores WITH (NOLOCK)
										Where judgeGUID = '#form.newJudge#'
										),
									0)
				WHERE judgeGUID = '#form.newJudge#'
			UPDATE STEMAppsJudges
				SET requiredToScore = isNull(
										(
										Select count(*) From STEMAppsJudgeScores WITH (NOLOCK)
										Where judgeGUID = '#form.oldJudge#'
										),
									0)
				WHERE judgeGUID = '#form.oldJudge#'
			
			<cfif StructKeyExists(form,"disable")>
				UPDATE STEMAppsJudges
					SET isEnabled = 0
					WHERE judgeGUID = '#form.oldJudge#'
					and Not Exists (Select 1 From STEMAppsJudgeScores a WITH (NOLOCK) Where a.judgeGUID = '#form.oldJudge#' and status != 'Scored')
			</cfif>
			
			SELECT email
			FROM STEMAppsJudges
			WHERE judgeGUID = '#form.newJudge#'
		</cfquery>
		
		<cfset infomsg="Reassign Complete!">
		<cfset emailTo = getEmail.email />
		<cfif FindNoCase("stage", cgi.server_name)>
			<cfset emailTo = "daryl_walsh@redacted.com,daryl_walsh@redacted.com" />
		</cfif>
		<cftry>
			<cfmail subject="Siemens STEM Academy Judging - New Applications Assigned" 
				from="Siemens STEM Academy <stemJudging@redacted.com>" 
				to="#emailTo#" 
				type="html">
					<div style="margin:10px; width:600px;">
					<cfif FindNoCase("stage", cgi.server_name)>
					This email would normally go to <b>#getEmail.email#</b><br><br>
					</cfif>
					New Applications have been assigned to you.<br>
					<a href="http://#cgi.server_name#/index.cfm?event=judge.showLogin">Log-in</a> to view your applications. <br />
					</div>
			</cfmail>
		<cfcatch></cfcatch></cftry>
		<cfset turl="/index.cfm?event=judge.reassignJudgemulti&string=#url.string#&stemType=#url.stemType#">
		<cfset turl=turl & "&newJudge=#form.newJudge#&oldJudge=#form.oldJudge#&infomsg=#variables.infomsg#">
		<cflocation url="#variables.turl#" addtoken="0">
	</cfif><!--- 2 --->
</cfif><!--- 1 --->

<style>
body {	
	padding:20px;
	text-align:center;
	color:##323232;
	font-family:Tahoma;
	font-size:12px;
}
a:link,a:visited {
	color:##0d9ebc;
	text-decoration:none;
}
a:hover {
	text-decoration:underline;
}
span {
	display:inline-block;
	position:relative;
	text-align:left;
}
input {
	font-family:arial;
	font-size:13px;
}
.maincontent {
	border:1px solid ##999;
	background-color: ##eee;
	width:auto;
	margin:auto;
	padding:10px;
	text-align:center;
}
.infomsg {
	color:##900;
}
</style>

<cfquery datasource="Sponsorships" name="oldJudges">
SELECT firstname, lastname, judgeGUID, type, isNull(
													(Select count(*) From STEMAppsJudgeScores WITH (NOLOCK) 
													Where judgeGUID = STEMAppsJudges.judgeGUID
													and status IN ('Pending','ToBeJudged')),
												0) as remaining
FROM STEMAppsJudges WITH (NOLOCK)
WHERE judgeYear = #request.contestYear#
and isEnabled = 1
and type = '#url.stemType#'
and judgeGUID IN 
	(
		SELECT distinct judgeGUID
		FROM STEMAppsJudgeScores
		WHERE (status IN ('Pending','ToBeJudged'))	
	)
Order by lastname, firstname
</cfquery>
<cfquery datasource="Sponsorships" name="newJudges">
SELECT firstname, lastname, judgeGUID, type, isNull(
													(Select count(*) From STEMAppsJudgeScores WITH (NOLOCK) 
													Where judgeGUID = STEMAppsJudges.judgeGUID
													and status IN ('Pending','ToBeJudged')),
												0) as remaining
FROM STEMAppsJudges WITH (NOLOCK)
WHERE judgeYear = #request.contestYear#
and isEnabled = 1
and type = '#url.stemType#'
Order by lastname, firstname
</cfquery>

<span class="maincontent"><br>

	<span style="float:left;">
		<a href="" onclick="parent.location.href='#url.string#'; return false;"
			>
			<cfif Not oldJudges.recordcount>
				There are Zero judges with outstanding applications - CLICK HERE to return
			<cfelse>
				CLICK HERE when you are finished reassigning
			</cfif>
			</a>
	</span>
	<span class="infomsg">#url.infomsg#</span>
	<br><br>
<cfif oldJudges.recordcount><!--- 2 --->
	<script type="text/javascript" src="/views/judging/js/jquery-1.4.3.min.js"></script>
	<script>
	jQuery.noConflict();
	jQuery(document).ready(function() {		
		jQuery('##oldJudge').live('change', function() {	
			remval=jQuery('##oldJudge option:selected').attr('remval');
			jQuery('##moveAmt').empty();
			var list = [];
			for (var ii = remval; ii >= 1; ii--) {					
			    list.push('<option value="'+ ii +'">'+ ii +'</option>');
			}			
			jQuery('##moveAmt').html(list.join(''));
		});
		jQuery('##oldJudge').change();
	});
	</script>
	<b>#ucase(url.stemType)#</b><br><br>
	<form action="/index.cfm?event=judge.reassignJudgemulti&string=#url.string#&stemType=#url.stemType#&action=update" 
		method="post">
		<nobr>
		<b>DISABLE</b><input type="checkbox" name="disable" onclick="window.focus()"
						style="vertical-align:middle;" title="Disable this Judge">
		<b>& MOVE</b>
		<select name="moveAmt" id="moveAmt"></select>
		<b>OF</b>
		<select name="oldJudge" id="oldJudge">
		<cfloop query="oldJudges">
			<option value="#judgeGUID#"
				remval="#remaining#"
				<cfif oldJudges.judgeGUID is url.oldJudge>selected</cfif>
				>#lastname#, #firstname#'s remaining #remaining# application<cfif remaining neq 1>s</cfif>
		</cfloop>
		</select>		
		<b>TO</b>
		<select name="newJudge">
		<cfloop query="newJudges">
			<option value="#judgeGUID#"
				<cfif newJudges.judgeGUID is url.newJudge>selected</cfif>
				>#lastname#, #firstname# who has #remaining# remaining application<cfif remaining neq 1>s</cfif>
		</cfloop>
		</select>
		</nobr>
		<br><br>		
		
		<input type="image" src="/img/buttonsubmit.png">
		
	</form>
</cfif><!--- 2 --->
</span>

</cfoutput></CFPROCESSINGDIRECTIVE>