<cfif Not StructKeyExists(client,"STEMJudgeType")>
	<cflocation url="/index.cfm?event=judge.showLogin" addtoken="0">
</cfif>
<cfif Not listfindnocase("adminPreInst,adminPreStar,adminFinalInst,adminFinalStar",client.STEMJudgeType)>
	<cflocation url="/" addtoken="0">
</cfif>

<cfset request.sectionDescription = "Administrator Finalists" />
<cfinclude template="../includes/header.cfm" />

<cfif listfindnocase("adminPreStar,adminFinalStar",client.STEMJudgeType)>
	<cfset appType = "STARs" />
<cfelse>
	<cfset appType = "Institute" />
</cfif>

<cfif StructKeyExists(form, "emailReminders")>
	<cfset application.judging.SendReminders(appType) />
	<cfset request.alertMsgEmailed = true />
</cfif>

<cfif StructKeyExists(form, "adminFinalists")>
	<cfset application.judging.SaveFinalists(appType, form) />
	<cfset request.alertMsgSaved = true />
</cfif>

<cfparam name="url.col" default="displayID" />
<cfparam name="url.dir" default="asc" />

<cfset adminCounts = application.judging.GetAdministratorCounts(appType) />
<cfset apps = application.judging.GetFinalists(appType) />
<cfset application.judging.AttachJudgeScoresToQuery(apps, appType) />

<cfif url.col neq "displayID" OR url.dir neq "asc">
	<cfquery name="apps" dbtype="query">
		SELECT * FROM apps
		ORDER BY #url.col# #url.dir#
	</cfquery>
</cfif>


<script type="text/javascript">
	function AlertEmailsSent ()
	{
		alert("Reminders to Judges who still need to submit scores have been sent successfully.");
	}


	function AlertSaved ()
	{
		alert("Application finalists and alternates settings have saved successfully.");
	}


	function ConfirmSendEmails ()
	{
		if (confirm("Are you sure you want to send reminder emails to all judges who need to submit scores right now?\n\nClick OK to send.\nClick Cancel to cancel.")) {
			jQuery("#emailRemindersForm").submit();
		}
	}


	function ExportReport ()
	{
		<cfoutput>location.href = "/cfcs/reports/reports.cfc?method=GetCSVReport&report=judgeResults&appType=#appType#";</cfoutput>
	}


	function SaveForm ()
	{
		jQuery("#adminFinalists").submit();
	}


	function Sort (columnName, dir)
	{
		location.href = "/index.cfm?event=judge.setupFinalists&col=" + columnName + "&dir=" + dir;
	}

	<cfif StructKeyExists(request, "alertMsgSaved")>
		window.setTimeout("AlertSaved()", 500);
	<cfelseif StructKeyExists(request, "alertMsgEmailed")>
		window.setTimeout("AlertEmailsSent()", 500);
	</cfif>
</script>


<form id="emailRemindersForm" method="post" style="display:none;">
	<input type="hidden" name="emailReminders" value="1" />
</form>


<form id="adminFinalists" method="post">
	<input type="hidden" name="adminFinalists" value="1" />
<div class="content">

	<div style="margin-bottom:12px; font-size:14px; color:#000000;">
		<cfoutput><b>#request.contestYear# #UCase(appType)# APPLICATIONS</b></cfoutput>
	</div>
	<span style="float:right;margin:-28px 20px 10px 0;"><a href="/index.cfm?event=judge.approveApplications">Go to Pre-Approval</a></span>
	<!---
	<div style="margin-bottom:20px; font-size:12px;">
		Welcome to the Siemens STEM <cfoutput>#client.STEMJudgeType#</cfoutput> Judging Site. You will find a list of
		applications to judge below. Please click on the links to the left for more information on the judging process.
	</div>
	--->

	<div class="section">
		<div class="left">&nbsp;</div>
		<div class="middle">
			LIST OF APPLICANTS
		</div>
		<div class="right">&nbsp;</div>
		<div class="clear">&nbsp;</div>

		<div class="pods">
			<div class="clear">&nbsp;</div>

			<cfoutput>
			<div class="pod" style="margin-top:2px;">
				<div class="podTitleLeft">&nbsp;</div>
				<div class="podTitleCenter" style="width:680px;">
					<div class="padding">
						APPLICATION SCORING SUMMARY: #variables.appType#

						<div style="color:##000000; float:right; font-size:0.90em; font-weight:normal;">
							Total Applications: #adminCounts.totalAppsCnt#
							&nbsp;&nbsp;&nbsp;
						</div>

					</div>
				</div>
				<div class="podTitleRight">&nbsp;</div>
				<div class="clear">&nbsp;</div>


				<div class="podContent" style="width:682px;">
					<div class="clear">&nbsp;</div>

					<div style="margin:10px;">
						<div style="float:left;  width:180px;">
							Number of Judges: #adminCounts.judgesCnt#
						</div>
						<div style="float:left; width:160px;">
							Need Scores:
							<cfif variables.appType is "STARs">
								#adminCounts.totalAppsCnt - adminCounts.submittedCnt#
							<cfelse>
								#(2*adminCounts.totalAppsCnt) - adminCounts.submittedCnt#
							</cfif>							
						</div>
						<div style="float:left; width:120px; color: ##F00; font-weight: bold;">
							Pending: #adminCounts.pendingCnt#
						</div>
						<div style="float:left; width:120px;">
							Submitted: #adminCounts.submittedCnt#
						</div>
						<div class="clear">&nbsp;</div>

						<div style="margin-top:20px;">
							<img src="/views/judging/images/reminder.png" />
							<a href="javascript: return false;" onclick="ConfirmSendEmails();">Send a reminder</a> to Judges who still need to submit scores.
						</div>
					</div>
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div class="clear">&nbsp;</div>
			
			<cfif len(trim(variables.appType)) and client.STEMAdmin>
				<a href="javascript:Void();" 
					onclick="OpenLightBoxWH('/index.cfm?event=judge.reassignJudgemulti&string=#cgi.script_name#?#cgi.query_string#&stemType=#variables.appType#', 930, 300);" 
					style="color:##f00;float:left;margin:0 0 0 100px;">Reassign Multiple Applications</a>
			</cfif>
			<div style="margin:5px 0px 0px 605px;width:500px;">			
				#application.judging.BuildButton("reportBtn", "Export Report", "ExportReport();")#
			</div>
			<a name="reassign">&nbsp;</a>
				
<script>
jQuery(document).ready(function() {
	jQuery('.reassigner').live('click', function() {	
		jQuery.get(
				jQuery(this).attr('vurl'), '', 
				function(data) { 
								location.replace('#cgi.script_name#?#cgi.query_string#&dummy=#CreateUUID()###reassign');
								/*console.log(data.trim()) */
								}, 
				"html"
			);	
		return false;
	});
});
</script>

					
			</cfoutput>
			
			
			<div class="pod">
				<div class="podTitleLeft">&nbsp;</div>
				<div class="podTitleCenter" style="border-right:1px solid #cccccc; width:150px;">
					<cfif url.col eq "displayID" AND url.dir eq "asc">
						<div style="float:left; width:15px;" onclick="Sort('displayID', 'desc');">
							<img src="/views/judging/images/arrow_up.png" style="cursor:pointer; margin:3px 0px 0px 0px;" />
						</div>
					<cfelse>
						<div style="float:left; width:15px;" onclick="Sort('displayID', 'asc');">
							<img src="/views/judging/images/arrow_down.png" style="cursor:pointer; margin:3px 0px 0px 0px;" />
						</div>
					</cfif>

					<div class="padding" style="float:left;">
						APPLICANT
					</div>
					<div class="clear">&nbsp;</div>
				</div>

				<div class="podTitleCenter" style="border-right:1px solid #cccccc; width:200px;">
					<cfif url.col eq "judge1LastName" AND url.dir eq "asc">
						<div style="float:left; width:15px;" onclick="Sort('judge1LastName', 'desc');">
							<img src="/views/judging/images/arrow_up.png" style="cursor:pointer; margin:3px 0px 0px 0px;" />
						</div>
					<cfelse>
						<div style="float:left; width:15px;" onclick="Sort('judge1LastName', 'asc');">
							<img src="/views/judging/images/arrow_down.png" style="cursor:pointer; margin:3px 0px 0px 0px;" />
						</div>
					</cfif>

					<div class="padding" style="float:left;">
						JUDGE
					</div>
					<div class="clear">&nbsp;</div>
				</div>

				<div class="podTitleCenter" style="border-right:1px solid #cccccc; width:100px;">
					<cfif url.col eq "judgeScoreAvg" AND url.dir eq "asc">
						<div style="float:left; width:15px;" onclick="Sort('judgeScoreAvg', 'desc');">
							<img src="/views/judging/images/arrow_up.png" style="cursor:pointer; margin:3px 0px 0px 0px;" />
						</div>
					<cfelse>
						<div style="float:left; width:15px;" onclick="Sort('judgeScoreAvg', 'asc');">
							<img src="/views/judging/images/arrow_down.png" style="cursor:pointer; margin:3px 0px 0px 0px;" />
						</div>
					</cfif>

					<div class="padding" style="float:left;">
						SCORE
					</div>
					<div class="clear">&nbsp;</div>
				</div>

				<div class="podTitleCenter" style="border-right:1px solid #cccccc; text-align:center; width:100px;">
					<div class="padding">
						FINALIST
					</div>
				</div>
				<div class="podTitleCenter" style="text-align:center; width:100px;">
					<div class="padding">
						ALTERNATE
					</div>
				</div>
				<div class="podTitleRight">&nbsp;</div>
				<div class="clear">&nbsp;</div>

				<div class="podContent" style="width:673px; height:240px; overflow:scroll; overflow-x:hidden;">

<cfoutput>

					<table border="0" cellpadding="0" cellspacing="0">
						<cfloop query="apps">
							<tr <cfif appType eq "Institute">style="height:42px;"</cfif> <cfif apps.CurrentRow mod 2 eq 0>class="even"</cfif>>
								<td style="border-right:1px solid ##cccccc; width:151px;">
									<div style="margin:3px 0px 3px 0px;">
										<a href="javascript:Void();" onclick="OpenLightBoxWH('/index.cfm?event=judge.displayApplicationSummary&applicationGUID=#apps.applicationGUID#&displayID=#apps.displayID#&stemType=#variables.appType#', 760, 600);" style="margin-left:5px;">Display ID ###numberformat(apps.displayID,"0000")#</a><br />
									</div>
								</td>

								<td style="border-right:1px solid ##cccccc; width:200px;">
									<div style="margin:3px 0px 3px 0px;">
										<cfif appType eq "Institute">
											<div style="border-bottom:1px solid ##cccccc; height:20px; line-height:20px; width:100%;">
												&nbsp;&nbsp;
												<cfif Trim(apps.judge1FirstName) neq "">
													<cfif apps.judge1AppStatus IS "Pending">
														<span style="color: ##cc0000; font-weight: bold;">
															#apps.judge1FirstName# #apps.judge1LastName#
														</span>
	<a href="" class="reassigner"
		vurl="/index.cfm?event=judge.reassignJudge&judgeGUID=#apps.judge1GUID#&approvedGUID=#apps.approvedGUID#"
		>Reassign</a>
													<cfelse>
														#apps.judge1FirstName# #apps.judge1LastName#
													</cfif>
												<cfelse>
													N/A
												</cfif>
											</div>
											<div style="height:21px; line-height:21px;">
												&nbsp;&nbsp;
												<cfif Trim(apps.judge2FirstName) neq "">
													<cfif apps.judge2AppStatus IS "Pending">
														<span style="color: ##cc0000; font-weight: bold;">
															#apps.judge2FirstName# #apps.judge2LastName#
														</span>
	<a href="" class="reassigner"
		vurl="/index.cfm?event=judge.reassignJudge&judgeGUID=#apps.judge2GUID#&approvedGUID=#apps.approvedGUID#"
		>Reassign</a>
													<cfelse>
														#apps.judge2FirstName# #apps.judge2LastName#
													</cfif>
												<cfelse>
													N/A
												</cfif>
											</div>
										<cfelseif appType eq "STARs">
											&nbsp;&nbsp;
											<cfif Trim(apps.judge1FirstName) neq "">
												<cfif apps.judge1AppStatus IS "Pending">
													<span style="color: ##cc0000; font-weight: bold;">
														#apps.judge1FirstName# #apps.judge1LastName#
													</span>
	<a href="" class="reassigner"
		vurl="/index.cfm?event=judge.reassignJudge&judgeGUID=#apps.judge1GUID#&approvedGUID=#apps.approvedGUID#"
		>Reassign</a>
												<cfelse>
													#apps.judge1FirstName# #apps.judge1LastName#
												</cfif>
											<cfelse>
												N/A
											</cfif>
										</cfif>
									</div>
								</td>


								<td style="border-right:1px solid ##cccccc; width:100px;">
									<div style="margin:3px 0px 3px 0px;">
										<cfif appType eq "Institute">
											<div style="border-bottom:1px solid ##cccccc; height:20px; line-height:20px; width:100%;">
												<cfif IsNumeric(apps.judge1Score)>
													<div style="float:left;width:45px;">
														&nbsp;&nbsp;
														<strong>#apps.judge1Score#</strong>
													</div>

													<div style="cursor:pointer; float:left; width:22px;" onclick="OpenLightBoxWH('/index.cfm?event=judge.displayJudgeScores&approvedGUID=#apps.approvedGUID#&judgeGUID=#apps.judge1GUID#',800,345);">
														<img src="/views/judging/images/icon_view.png" />
													</div>

													<a href="javascript:Void();" onclick="OpenLightBoxWH('/index.cfm?event=judge.displayJudgeScores&approvedGUID=#apps.approvedGUID#&judgeGUID=#apps.judge1GUID#',800,345);">View</a>
												<cfelse>
													&nbsp;
												</cfif>
											</div>
											<div style="height:21px; line-height:21px;">
												<cfif IsNumeric(apps.judge2Score)>
													<div style="float:left;width:45px;">
														&nbsp;&nbsp;
														<strong>#apps.judge2Score#</strong>
													</div>

													<div style="cursor:pointer; float:left; width:22px;" onclick="OpenLightBoxWH('/index.cfm?event=judge.displayJudgeScores&approvedGUID=#apps.approvedGUID#&judgeGUID=#apps.judge2GUID#',800,345);">
														<img src="/views/judging/images/icon_view.png" />
													</div>

													<a href="javascript:Void();" onclick="OpenLightBoxWH('/index.cfm?event=judge.displayJudgeScores&approvedGUID=#apps.approvedGUID#&judgeGUID=#apps.judge2GUID#',800,345);">View</a>
												<cfelse>
													&nbsp;
												</cfif>
											</div>
										<cfelseif appType eq "STARs">
											<cfif IsNumeric(apps.judge1Score)>
												<div style="float:left;width:45px;">
													&nbsp;&nbsp;
													<strong>#apps.judge1Score#</strong>
												</div>

												<div style="cursor:pointer; float:left; width:22px;" onclick="OpenLightBoxWH('/index.cfm?event=judge.displayJudgeScores&approvedGUID=#apps.approvedGUID#&judgeGUID=#apps.judge1GUID#',800,345);">
													<img src="/views/judging/images/icon_view.png" />
												</div>

												<a href="javascript:Void();" onclick="OpenLightBoxWH('/index.cfm?event=judge.displayJudgeScores&approvedGUID=#apps.approvedGUID#&judgeGUID=#apps.judge1GUID#',800,345);">View</a>
											<cfelse>
												&nbsp;
											</cfif>
										</cfif>
									</div>
								</td>

								<td style="border-right:1px solid ##cccccc; text-align:center; width:100px;">
									<div style="margin:3px 0px 3px 0px;">
										<input type="checkbox" name="isFinalist" id="Finalist#apps.approvedGUID#" onclick="if (this.checked) {document.getElementById('Alternate#apps.approvedGUID#').checked=false;}" value="#apps.approvedGUID#" <cfif apps.isFinalist eq true>checked="checked"</cfif> />
									</div>
								</td>

								<td style="text-align:center; width:100px;">
									<div style="margin:3px 0px 3px 0px;">
										<input type="checkbox" name="isAlternate" id="Alternate#apps.approvedGUID#" onclick="if (this.checked) {document.getElementById('Finalist#apps.approvedGUID#').checked=false;}" value="#apps.approvedGUID#" <cfif apps.isAlternate eq true>checked="checked"</cfif> />
									</div>
								</td>
							</tr>
						</cfloop>
					</table>
					</cfoutput>


				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div class="clear">&nbsp;</div>


			<div style="height:50px; margin:5px auto 0px auto; width:80px;">
				<cfoutput>#application.judging.BuildButton("saveBtn", "Save", "SaveForm();")#</cfoutput>
				<img id="saveWorkingImg" src="/views/judging/images/formProcessing2.gif" style="display:none; margin:4px 0px 0px 15px;" />
			</div>
		</div>
	</div>

	<div class="clear">&nbsp;</div>
</div>
</form>



<cfinclude template="../includes/footer.cfm" />
