<CFPROCESSINGDIRECTIVE SUPPRESSWHITESPACE="Yes"><cfoutput>
<cfparam name="url.judgeGUID" default="">
<cfparam name="url.approvedGUID" default="">
<cfif IsValid("guid", url.judgeGUID) and IsValid("guid", url.approvedGUID)>
	<cfquery datasource="Sponsorships" name="getEmail">
	Declare @Judge uniqueidentifier
	SELECT Top (1) @Judge = judgeGUID
				FROM STEMAppsJudges with (nolock) 
				WHERE judgeYear = #request.contestYear#
				and isEnabled = 1
				and type = (
								Select Top 1 type From STEMAppsJudgingApproved WITH (NOLOCK)
								Where approvedGUID = '#url.approvedGUID#'
							)
				and Not Exists(select 1 from STEMAppsJudgeScores a with (nolock) where a.judgeGUID = STEMAppsJudges.judgeGUID
										and a.approvedGUID = '#url.approvedGUID#')
		Order by requiredToScore
	
	IF @Judge is not null
		begin
			UPDATE STEMAppsJudgeScores
			SET judgeGUID = @Judge,
			status = 'ToBeJudged'
			WHERE approvedGUID = '#url.approvedGUID#'
			and judgeGUID = '#url.judgeGUID#'
			
			UPDATE STEMAppsJudges
			SET requiredToScore = requiredToScore + 1
			WHERE judgeGUID = @Judge
					
			UPDATE STEMAppsJudges
			SET requiredToScore = requiredToScore - 1
			WHERE judgeGUID = '#url.judgeGUID#'
			
			INSERT INTO STEMAppsJudgeRecuse
			(oldJudge,newJudge,approvedGUID)
			VALUES
			('#url.judgeGUID#',@Judge,'#url.approvedGUID#')
			
			SELECT email
			FROM STEMAppsJudges
			WHERE judgeGUID = @Judge
		end
	</cfquery>
	
		<cftry>
			<cfset emailTo = getEmail.email />
			<cfif FindNoCase("stage", cgi.server_name)>
				<cfset emailTo = "daryl_walsh@redacted.com,daryl_walsh@redacted.com" />
			</cfif>
		
			<cfmail subject="Siemens STEM Academy Judging - New Applications Assigned" 
				from="Siemens STEM Academy <stemJudging@redacted.com>" 
				to="#emailTo#" 
				type="html">
					<div style="margin:10px; width:600px;">
					<cfif FindNoCase("stage", cgi.server_name)>
					This email would normally go to <b>#getEmail.email#</b><br><br>
					</cfif>
					New Applications have been assigned to you.<br>
					<a href="http://#cgi.server_name#/index.cfm?event=judge.showLogin">Log-in</a> to view your applications. <br />
					</div>
			</cfmail>
		<cfcatch></cfcatch></cftry>
</cfif>
</cfoutput></CFPROCESSINGDIRECTIVE>