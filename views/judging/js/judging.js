
function CloseLightBox ()
{
	jQuery.fancybox.close();
}

function OpenLightBox (aLocation)
{
	OpenLightBoxWH(aLocation, 600, 400);
}

function OpenLightBoxWH (aLocation, w, h)
{
	jQuery.fancybox({
		'href' : aLocation,
		'width' : w,
		'height' : h,
		'autoScale' : false,
		'transitionIn' : 'none',
		'transitionOut' : 'none',
		'type' : 'iframe'
	});
}

function RefreshAdminPage ()
{
	location.href = "/index.cfm?event=judge.setupFinalists";
}

function Void ()
{
	
}
