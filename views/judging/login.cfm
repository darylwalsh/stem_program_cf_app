
<cfif not StructKeyExists(application, "judging") OR StructKeyExists(url, "reinit") OR StructKeyExists(url, "reload") OR StructKeyExists(url, "restart")>
	<cfset application.judging = CreateObject("component", "cfcs.judging").Init() />
</cfif>


<cfif application.judging.IsLoggedIn() AND StructKeyExists(url, "judgeLogout")>
	<cfif StructKeyExists(client,"STEMJudgeGUID") and len(trim(client.STEMJudgeGUID))>
	<cftry>
		<cfquery datasource="Sponsorships">
		Update STEMAppsJudgingApproved
		SET isOpen = 0,
		openUser = ''
		WHERE openUser = '#client.STEMJudgeGUID#'
		</cfquery>
	<cfcatch></cfcatch></cftry>
	</cfif>
	<cfset application.judging.Logout() />
</cfif>

<cfif application.judging.IsLoggedIn()>
	<cfif StructKeyExists(client, "STEMJudgeType") AND (client.STEMJudgeType eq "adminPreInst" OR client.STEMJudgeType eq "adminPreStar")>
		<cflocation url="/index.cfm?event=judge.approveApplications" addtoken="false" /> <!--- will get chagned to judge.setupFinalists --->
	<cfelseif StructKeyExists(client, "STEMJudgeType") AND (client.STEMJudgeType eq "STARs" OR client.STEMJudgeType eq "Institute")>
		<cflocation url="/index.cfm?event=judge.showApplications" addtoken="false" />
	<cfelseif StructKeyExists(client, "STEMJudgeType") AND (client.STEMJudgeType eq "adminFinalInst" OR client.STEMJudgeType eq "adminFinalStar")>
		<cflocation url="/index.cfm?event=judge.setupFinalists" addtoken="false" />
	</cfif>
</cfif>

<cfif StructKeyExists(form, "strJudgeLoginUsername") and StructKeyExists(form, "strJudgeLoginPassword")>
	<cfif application.judging.Login(form.strJudgeLoginUsername, form.strJudgeLoginPassword)>
		<cfif StructKeyExists(client, "STEMJudgeType") AND (client.STEMJudgeType eq "adminPreInst" OR client.STEMJudgeType eq "adminPreStar")>
			<cflocation url="/index.cfm?event=judge.approveApplications" addtoken="false" /> <!--- will get chagned to judge.setupFinalists --->
		<cfelseif StructKeyExists(client, "STEMJudgeType") AND (client.STEMJudgeType eq "STARs" OR client.STEMJudgeType eq "Institute")>
			<cflocation url="/index.cfm?event=judge.showApplications" addtoken="false" />
		<cfelseif StructKeyExists(client, "STEMJudgeType") AND (client.STEMJudgeType eq "adminFinalInst" OR client.STEMJudgeType eq "adminFinalStar")>
			<cflocation url="/index.cfm?event=judge.setupFinalists" addtoken="false" />
		<cfelse>
			<cfset request.errorMsg = true />
		</cfif>

	<cfelse>
		<cfset request.errorMsg = true />
	</cfif>
</cfif>




<cfset pageTitle = "#request.contestYear# Siemens STEM Academy Institute and STARs Judging" />
<cfset event.setArg("pageTitle", pageTitle) />


<cfsavecontent variable="jScripts">
	<script type="text/javascript">
		function PreSubmitLogin()
		{
			document.getElementById("lb-loginForm").style.display = "none";
			document.getElementById("lbLoading").style.display = "";
			window.setTimeout("SubmitLogin()", 500);
		}

		function SubmitLogin()
		{
			document.getElementById("lb-loginForm").submit();
		}
	</script>
</cfsavecontent>
<cfset event.setArg("js",jScripts) />


<cfparam name="form.strJudgeLoginUsername" default="" />


<cfoutput>
<h3 style="margin:0px 0px 0px 25px; padding:15px 0px 0px 0px;">
	#pageTitle#
</h3>

<div style="margin:0px 0px 20px 290px;">
	<div id="lb-top2"></div>
	<div id="lb-middle">
		<div id="lbLoginForm">

			<h4 style="font-size:">Please Login</h4>
			<p>This action requires you to be logged in.</p>

			<cfif StructKeyExists(request, "errorMsg")>
				<div id="lb-invalid" style="color:red;font-weight:bold;margin-bottom:10px;">Invalid username and/or password</div>
			</cfif>

			<br />

			<div id="form-container" style="height:125px;">
				<div id="lbLoading" style="display:none; padding:40px 0px 0px 20px;"><img alt="activity indicator" src="/images/loading_large.gif" /></div>

				<form id="lb-loginForm" name="lb-loginForm" method="post">
					<div style="margin-left:10px;">
						<h4>Username</h4>
						<input type="text" size="36" name="strJudgeLoginUsername" id="lbUid" value="#form.strJudgeLoginUsername#" />&nbsp;&nbsp;
						<br /><br />

						<h4>Password</h4>
						<input type="password" size="36" name="strJudgeLoginPassword" id="lbPwd" value="" />
						<br /><br />
						<input type="submit" value="Login" style="position:absolute; top:-5000px; left:-5000px;" />
						<br clear="all" />
					</div>

					<a href="##" onclick="PreSubmitLogin(); return false;" class="sprite" id="btn-login"></a>
				</form>
				<br clear="all" />

			</div>

		</div>
	</div>
	<div id="lb-bottom"></div>
</div>
</cfoutput>
