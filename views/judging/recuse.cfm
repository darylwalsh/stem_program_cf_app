<cflocation url="/" addtoken="no">
<cfif not StructKeyExists(application, "judging") OR StructKeyExists(url, "reinit") OR StructKeyExists(url, "reload") OR StructKeyExists(url, "restart")>
	<cfset application.judging = CreateObject("component", "cfcs.judging").Init() />
</cfif>


<cfif StructKeyExists(url, "approve")>
	<cfinvoke component="#application.judging#" method="JudgeRecuseApplication" argumentcollection="#url#" />
<cfelse>
	<cflocation url="/" addtoken="no">
</cfif>

<cfset pageTitle = "2012 Siemens STEM Academy Institute and STARs Judging" />
<cfset event.setArg("pageTitle", pageTitle) />

<cfoutput>
<h3 style="margin:0px 0px 0px 25px; padding:15px 0px 0px 0px;">
	#pageTitle#
</h3>

<div style="margin:0px 0px 20px 290px;">
	<div id="lb-top2"></div>
	<div id="lb-middle">
        <h4>Success</h4>	
        <p>Recusal request
        <cfif url.approve>
	        accepted.
        <cfelse>
        	rejected.
        </cfif>
        </p>	
        <br clear="all" />
	</div>
	<div id="lb-bottom"></div>
</div>
</cfoutput>
