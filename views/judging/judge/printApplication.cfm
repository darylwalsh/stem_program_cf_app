

<script type="text/javascript" src="/views/judging/js/jquery-1.4.3.min.js"></script>
<script type="text/javascript">
	// first make sure jQuery doesn't conflict with any other existing APIs.
	jQuery.noConflict();  // now any jQuery scripts must replace '$' with 'jQuery'


	jQuery().ready(function () {
		window.print();
	})

</script>


<link rel="stylesheet" style="text/css" href="/views/judging/css/judging.css" media="all" />
<style type="text/css">
	html, body {
		height:auto !important;
		height:100%;
		min-height:100%;
		margin:0; padding:0;
		font-size:11px;
		font-family:Tahoma, Arial, Helvetica, sans-serif;margin:0; padding:0;
	}

	div.tabContent div.pod{
		padding-left:25px;
	}
</style>


<cfif not StructKeyExists(url, "approvedGUID") OR not IsValid("guid", url.approvedGUID)>
	<cflocation url="/index.cfm?event=judge.showLogin" addtoken="false" />
</cfif>


<cfif StructKeyExists(form, "comment")>
	<cfset application.judging.SaveJudgeScores(client.STEMJudgeGUID, url.approvedGUID, form) />
	<cflocation url="/index.cfm?event=judge.showApplications&scored=#form.displayID#" addtoken="false" />

<cfelseif StructKeyExists(form, "pendingComment")>
	<!--- <cfset application.judging.CommentToCollegeBoard(client.STEMJudgeGUID, url.approvedGUID, form) /> --->
	<cflocation url="/index.cfm?event=judge.showApplications&commented=#form.displayID#" addtoken="false" />

</cfif>


<cfset approvedApp = application.judging.GetApprovedApplication(url.approvedGUID) />
<cfif approvedApp.RecordCount lte 0>
	<cflocation url="/index.cfm?event=judge.showLogin" addtoken="false" />
</cfif>

<cfset app = application.judging.GetApplication(approvedApp.applicationGUID) />
<cfset judgeScores = application.judging.GetJudgeScores(client.STEMJudgeGUID, url.approvedGUID) />


<script type="text/javascript">
	<cfif StructKeyExists(url, "callPrint")>
		jQuery().ready(function() {
			window.print();
		})
	</cfif>
</script>



<div class="content" style="margin:10px 0px 0px 10px;">
	<div style="margin-bottom:12px; font-size:18px; font-weight:bold; color:#000000;">
		<cfoutput>
			#client.STEMJudgeType# Display ID ###approvedApp.displayID#
		</cfoutput>
	</div>

	<cfset tabs = [] />

	<cfset tab = {} />
	<cfset tab.title = "EDUCATION AND EXPERIENCE" />
	<cfset tab.pods = [] />
	<cfset ArrayAppend(tab.pods, "citizenship") />
	<cfset ArrayAppend(tab.pods, "education") />
	<cfset ArrayAppend(tab.pods, "currentPosition") />
	<cfset ArrayAppend(tab.pods, "pastPositions") />
	<cfset ArrayAppend(tab.pods, "responsibilities") />
	<cfset ArrayAppend(tab.pods, "activities") />
	<cfset ArrayAppend(tabs, tab) />

	<cfset tab = {} />
	<cfset tab.title = "ACHIEVEMENTS" />
	<cfset tab.pods = [] />
	<cfset ArrayAppend(tab.pods, "honorsAwards") />
	<cfset ArrayAppend(tabs, tab) />

	<cfset tab = {} />
	<cfset tab.title = "REFERENCE" />
	<cfset tab.pods = [] />
	<cfset ArrayAppend(tab.pods, "letterRecommendation") />
	<cfset ArrayAppend(tabs, tab) />

	<cfset tab = {} />
	<cfset tab.title = "VIDEO" />
	<cfset tab.pods = [] />
	<cfset ArrayAppend(tab.pods, "uploadVideo") />
	<cfset ArrayAppend(tabs, tab) />

	<cfset tab = {} />
	<cfset tab.title = "ESSAYS" />
	<cfset tab.pods = [] />

	<cfset settings = StructNew() />
	<cfset settings.dsn = "Sponsorships" />

	<cfoutput>
	<div class="section">
		<div style="font-size:16px; font-weight:bold;">
			SUMMARY
		</div>


		<div class="pods">
			<div class="clear">&nbsp;</div>

			<div class="summary" id="summaryContent">
				<cfloop from="1" to="#ArrayLen(tabs)#" index="tabIndex">
					<div class="tabTitleLeft">&nbsp;</div>
					<div class="tabTitleCenter">
						<div class="padding">
							#tabs[tabIndex].title#
						</div>
					</div>
					<div class="tabTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>

					<div class="tabContent">
						<cfloop from="1" to="#ArrayLen(tabs[tabIndex].pods)#" index="i">
							<cfset curSection = CreateObject("component", "cfcs.tabbedForm.pods." & tabs[tabIndex].pods[i] ).Init(settings) />

							<div style="color:##323232; font-size:14px; font-weight:bold; margin:10px;">
								#curSection.GetPodTitle()#
							</div>
							#curSection.BuildPod(approvedApp.applicationGUID, "summary")#
						</cfloop>
					</div>
				</cfloop>
			</div>

		</div>

	</div>


	<div class="section" style="margin-top:40px;">

		<div style="font-size:16px; font-weight:bold;">
			REVIEW & SCORE APPLICATION
		</div>


		<div class="pods">
			<div class="clear">&nbsp;</div>

			<div class="summary" id="reviewContent">

				<div style="margin:0px 0px 8px 4px;">
					<strong>NOTE: You cannot save your entries until scoring has been completed for all sections</strong>
				</div>


				<div class="tabTitleLeft">&nbsp;</div>
				<div class="tabTitleCenter">
					<div class="padding">
						Video Guidelines
					</div>
				</div>
				<div class="tabTitleRight">&nbsp;</div>
				<div class="clear">&nbsp;</div>

				<div class="tabContent">
					<div style="margin:10px;">
						<!---
						<div class="question questionSpacer">
							&bull; If video exceeds 60 seconds, please deduct 1 point from the total score for every 10 seconds it exceeds 60 seconds.
						</div>
						--->
						<div class="question questionSpacer">
							&bull; Refer to the rubrics below to score the submitted video (scoring range is 1 - 5).  <!--- In the event that a video is ineligible, you may assign a score of zero (0). --->
						</div>

						<div class="question questionSpacer">
							SELECTED TOPIC:
							<strong>
								<cfif app.videoType eq "UtilizeInYourClassroom">
									Describe any new innovative ideas, materials, instructional strategies or techniques that you utilize in your classroom.
								<cfelseif app.videoType eq "ProvideSTEMOutside">
									Describe ways in which you provide student educational STEM experiences outside of the classroom.
								<cfelseif app.videoType eq "InfluencedToBecomeTeacher">
									What factors influenced your decision to become a teacher? Describe your greatest contributions and accomplishments in STEM education.
								<cfelseif app.videoType eq "IssuesEducationToday">
									What do you believe are the major issues in education today? Address one in depth, detailing potential causes, effects, and resolutions.
								</cfif>
							</strong>
						</div>

						<div class="question questionSpacer">
							VIDEO:

							<a href="http://static.redactededucation.com/feeds/stem-application-videos/#app.videoFileName#" target="_STEM">#app.videoFileName#</a>
						</div>
					</div>

				</div>



				<div class="tabTitleLeft">&nbsp;</div>
				<div class="tabTitleCenter">
					<div class="padding">
						Essay Guidelines
					</div>
				</div>
				<div class="tabTitleRight">&nbsp;</div>
				<div class="clear">&nbsp;</div>

				<div class="tabContent">
					<div style="margin:10px;">

						<!---
						<div class="question questionSpacer">
							&bull; If the essay is over 350 words it is ineligible.  Do not review it.
						</div>
						--->

						<div class="question questionSpacer">
							&bull; Use the rubrics to score each part of the essay submission.  The scoring range is 1 to 5.
						</div>

						<div class="question questionSpacer">
							<strong>INSTITUTE ESSAY QUESTIONS:</strong>
						</div>

						<div class="question questionSpacer">
							<strong>ESSAY ##1:</strong>
							<em>
								<cfif approvedApp.type eq "Institute">
									Describe your teaching philosophy, including your ideas of what makes you a successful teacher.  Provide an example
									of how your teaching philosophy is reflected in a lesson or unit you teach.
								<cfelseif approvedApp.type eq "STARs">
									Please describe your laboratory skills related to using equipment such as balances, fume hoods, oscilloscopes,
									spectrometers, etc.
								</cfif>

							</em>
						</div>

						<div class="question questionSpacer" id="essay1Content">
							<cfif approvedApp.type eq "Institute">
								#app.essaySTEMInstitute1#
							<cfelseif approvedApp.type eq "STARs">
								#app.essaySTARs1#
							</cfif>
						</div>


						<div class="question questionSpacer">
							<strong>ESSAY ##2:</strong>
							<em>
								<cfif approvedApp.type eq "Institute">
									Why do you think STEM is important to today's students and how will it be relevant to their future?
								<cfelseif approvedApp.type eq "STARs">
									Describe a classroom activity/lesson which you have conducted that you believe illustrates good
									inquiry-based instruction.
								</cfif>
							</em>
						</div>

						<div class="question questionSpacer" id="essay2Content">
							<cfif approvedApp.type eq "Institute">
								#app.essaySTEMInstitute2#
							<cfelseif approvedApp.type eq "STARs">
								#app.essaySTARs2#
							</cfif>
						</div>


						<div class="question questionSpacer">
							<strong>ESSAY ##3:</strong>
							<em>
								<cfif approvedApp.type eq "Institute">
									Explain what you hope to learn from the experience, what you can uniquely contribute and what you would like to add to
									your classroom instruction upon completion of the STEM Institute program.
								<cfelseif approvedApp.type eq "STARs">
									Why do you want to attend the STARs program? Describe three ways in which you believe the Siemens Teachers as Researchers
									professional development program will improve teaching and learning in your classroom.
								</cfif>
							</em>
						</div>

						<div class="question questionSpacer" id="essay3Content">
							<cfif approvedApp.type eq "Institute">
								#app.essaySTEMInstitute3#
							<cfelseif approvedApp.type eq "STARs">
								#app.essaySTARs3#
							</cfif>
						</div>
					</div>
				</div>


				<div class="tabTitleLeft">&nbsp;</div>
				<div class="tabTitleCenter">
					<div class="padding">
						Recommendation Rating Sheet
					</div>
				</div>
				<div class="tabTitleRight">&nbsp;</div>
				<div class="clear">&nbsp;</div>

				<div class="tabContent">
					<div style="margin:10px;">
						<div class="question questionSpacer">
							The application requires a minimum of 1 recommendation rating sheet from an administrator or supervisor, specifically
							addressing how participation will benefit the school/students/fellow teachers.  The individual writing the
							recommendation rating sheet should be able to evaluate their character, academic and/or teaching abilities.  This
							letter must be on official letterhead.
						</div>
<cfif fileExists(expandPath('/views/judging/stem-application-recommendations/#approvedApp.applicationGUID#.docx'))>						
						<div class="question questionSpacer">
							<!--- <a href="http://static.redactededucation.com/feeds/stem-application-recommendations/#app.recommendationFileName#" target="_STEM">View Letter of Recommendation</a> --->
<a href="/views/judging/stem-application-recommendations/#approvedApp.applicationGUID#.docx" target="_blank">View Letter of Recommendation</a>
						</div>
</cfif>						
					</div>
				</div>



				<div class="tabTitleLeft">&nbsp;</div>
				<div class="tabTitleCenter">
					<div class="padding">
						Application
					</div>
				</div>
				<div class="tabTitleRight">&nbsp;</div>
				<div class="clear">&nbsp;</div>

				<div class="tabContent">
					<div style="margin:10px;">
						<div class="question questionSpacer">
							After reviewing the overall application, rate based on the education leadership responsibilities, extracurricular
							activities, STEM related professional development and honors and awards.  Please note, length of teaching should not
							be a factor when scoring the teacher as the goal of this program is to reach teachers with a variety of teaching experience.
						</div>

					</div>
				</div>



				<div class="tabTitleLeft">&nbsp;</div>
				<div class="tabTitleCenter">
					<div class="padding">
						SCORE
					</div>
				</div>
				<div class="tabTitleRight">&nbsp;</div>
				<div class="clear">&nbsp;</div>

				<div class="tabContent">
					<div style="margin:10px;">
						<input type="hidden" name="displayID" value="#approvedApp.displayID#" />
						<table class="scoreCard" border="0" cellspacing="0" cellpadding="0">
							<tr class="headRow">
								<td colspan="7">
									<!--- <strong>SCORING CRITERIA</strong> ---> &nbsp;
								</td>
							</tr>
							<tr class="titleRow">
								<td style="width:167px;">&nbsp;</td>
								<!--- <td style="width:74px;">Ineligible</td> --->
								<td style="width:74px;">Poor</td>
								<td style="width:74px;">Weak</td>
								<td style="width:74px;">Good</td>
								<td style="width:74px;">Great</td>
								<td style="width:74px;">Outstanding</td>
								<td style="width:74px; font-size:1.25em; text-decoration:underline;">Score</td>
							</tr>
							
							<tr class="values">
								<td style="text-align:right;"><strong>Letter of Recommendation:</strong></td>
								<cfloop from="1" to="5" index="i">
									<td>
										<input type="radio" name="scoreLetter" value="#i#" onclick="CalcScore();" <cfif judgeScores.scoreLetter eq i>checked="checked"</cfif> <cfif IsDate(judgeScores.submitted)>disabled="disabled"</cfif> />
										#i#
									</td>
								</cfloop>
								<td id="letterScore" style="font-size:1.20em;">
									#judgeScores.scoreLetter#
								</td>
							</tr>
							

							<tr class="values">
								<td style="text-align:right;"><strong>Essay ##1:</strong></td>
								<cfloop from="1" to="5" index="i">
									<td>
										<input type="radio" name="scoreEssay1" value="#i#" onclick="CalcScore();" <cfif judgeScores.scoreEssay1 eq i>checked="checked"</cfif> <cfif IsDate(judgeScores.submitted)>disabled="disabled"</cfif> />
										#i#
									</td>
								</cfloop>
								<td id="essay1Score" style="font-size:1.20em;">
									#judgeScores.scoreEssay1#
								</td>
							</tr>

							<tr class="values">
								<td style="text-align:right;"><strong>Essay ##2:</strong></td>
								<cfloop from="1" to="5" index="i">
									<td>
										<input type="radio" name="scoreEssay2" value="#i#" onclick="CalcScore();" <cfif judgeScores.scoreEssay2 eq i>checked="checked"</cfif> <cfif IsDate(judgeScores.submitted)>disabled="disabled"</cfif> />
										#i#
									</td>
								</cfloop>
								<td id="essay2Score" style="font-size:1.20em;">
									#judgeScores.scoreEssay2#
								</td>
							</tr>

							<tr class="values">
								<td style="text-align:right;"><strong>Video:</strong></td>
								<cfloop from="1" to="5" index="i">
									<td>
										<input type="radio" name="scoreVideo" value="#i#" onclick="CalcScore();" <cfif judgeScores.scoreVideo eq i>checked="checked"</cfif> <cfif IsDate(judgeScores.submitted)>disabled="disabled"</cfif> />
										#i#
									</td>
								</cfloop>
								<td id="videoScore" style="font-size:1.20em;">
									#judgeScores.scoreVideo#
								</td>
							</tr>

							<tr class="results">
								<td colspan="6" style="text-align:right;">APPLICATION SCORE:</td>
								<td id="totalScore" style="font-weight:bold;font-size:1.35em;text-align:center;">
									<cfset totalScore = 0 />
									<cfif IsNumeric(judgeScores.scoreVideo)>
										<cfset totalScore += judgeScores.scoreVideo />
									</cfif>
									<cfif IsNumeric(judgeScores.scoreEssay1)>
										<cfset totalScore += judgeScores.scoreEssay1 />
									</cfif>
									<cfif IsNumeric(judgeScores.scoreEssay2)>
										<cfset totalScore += judgeScores.scoreEssay2 />
									</cfif>
									<cfif IsNumeric(judgeScores.scoreLetter)>
										<cfset totalScore += judgeScores.scoreLetter />
									</cfif>

									#totalScore#
								</td>
							</tr>

						</table>

						<div class="question questionSpacer">
							Enter any comments or notes you may have below regarding this application.
						</div>
						<div class="question questionSpacer">
							<textarea name="comment" style="height:75px; width:695px; <cfif IsDate(judgeScores.submitted)>color:##000000;</cfif>" <cfif IsDate(judgeScores.submitted)>disabled="disabled"</cfif>>#judgeScores.comment#</textarea>
						</div>

					</div>
				</div>

			</div>

		</div>
	</div>
	</cfoutput>
</div>


