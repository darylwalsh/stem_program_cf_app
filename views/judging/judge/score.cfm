
<cfif not StructKeyExists(url, "approvedGUID") OR not IsValid("guid", url.approvedGUID)>
	<cflocation url="/index.cfm?event=judge.showLogin" addtoken="false" />
</cfif>

<cfif listfindnocase("adminPreStar,adminFinalStar,Stars",client.STEMJudgeType)> 
	<cfset appType = "STARs" />
<cfelse>
	<cfset appType = "Institute" />
</cfif>
<cfset request.sectionDescription = UCase(variables.appType) & " JUDGING PHASE" />
<cfinclude template="../includes/header.cfm" />

	<cfif StructKeyExists(form, "comment")>
     	<cfif form.newStatus EQ "Pending" OR form.newStatus EQ "PartiallyScored">
			<cfset application.judging.SavePendingScores(client.STEMJudgeGUID, url.approvedGUID, form) />
          <cfelse>
			<cfset application.judging.SaveJudgeScores(client.STEMJudgeGUID, url.approvedGUID, form) />
          </cfif>
		<cflocation url="/index.cfm?event=judge.showApplications&scored=#form.displayID#" addtoken="false" />

	<cfelseif StructKeyExists(form, "pendingComment")>
		<!--- <cfset application.judging.CommentToCollegeBoard(client.STEMJudgeGUID, url.approvedGUID, form) /> --->
		<cflocation url="/index.cfm?event=judge.showApplications&commented=#form.displayID#" addtoken="false" />

	</cfif>


	<cfset approvedApp = application.judging.GetApprovedApplication(url.approvedGUID) />
	<cfif approvedApp.RecordCount lte 0>
		<cflocation url="/index.cfm?event=judge.showLogin" addtoken="false" />
	</cfif>

	<cfset app = application.judging.GetApplication(approvedApp.applicationGUID) />
     <cfset rs = application.judging.GetRecommendation(url.approvedGUID) />
	<cfset judgeScores = application.judging.GetJudgeScores(client.STEMJudgeGUID, url.approvedGUID) />

	<cfif FindNoCase("MSIE", cgi.http_user_agent) gt 0>
		<style type="text/css">
			div.plusMinus {
				margin-top:2px;
			}
		</style>
	</cfif>

	<script type="text/javascript">
		jQuery().ready(function () {
			CalcScore ();
		});

		function CalcScore ()
		{
			var scoreCard = document.getElementById("scoreCard");
			var totalScore = 0;

			for (var i = 0; i < scoreCard.scoreVideo.length; i++) {
				if (scoreCard.scoreVideo[i].checked) {
					jQuery("#videoScore").html(scoreCard.scoreVideo[i].value);
					totalScore += Number(scoreCard.scoreVideo[i].value);
					break;
				}
			}

			for (var i = 0; i < scoreCard.scoreEssay1.length; i++) {
				if (scoreCard.scoreEssay1[i].checked) {
					jQuery("#essay1Score").html(scoreCard.scoreEssay1[i].value);
					totalScore += Number(scoreCard.scoreEssay1[i].value);
					break;
				}
			}

			for (var i = 0; i < scoreCard.scoreEssay2.length; i++) {
				if (scoreCard.scoreEssay2[i].checked) {
					jQuery("#essay2Score").html(scoreCard.scoreEssay2[i].value);
					totalScore += Number(scoreCard.scoreEssay2[i].value);
					break;
				}
			}

			/*for (var i = 0; i < scoreCard.scoreEssay3.length; i++) {
				if (scoreCard.scoreEssay3[i].checked) {
					jQuery("#essay3Score").html(scoreCard.scoreEssay3[i].value);
					totalScore += Number(scoreCard.scoreEssay3[i].value);
					break;
				}
			} */

			for (var i = 0; i < scoreCard.scoreLetter.length; i++) {
				if (scoreCard.scoreLetter[i].checked) {
					jQuery("#letterScore").html(scoreCard.scoreLetter[i].value);
					totalScore += Number(scoreCard.scoreLetter[i].value);
					break;
				}
			}

			jQuery("#totalScore").html(totalScore);
		}


		function PlusMinus(idImg, idContent)
		{
			var pm = jQuery("#" + idImg);
			var con = jQuery("#" + idContent);

			if (pm.attr("src").toLowerCase() == "/views/judging/images/collapse3.png") {
				pm.attr("src", "/views/judging/images/collapse2.png");
				con.hide();
			}
			else if (pm.attr("src").toLowerCase() == "/views/judging/images/collapse2.png") {
				pm.attr("src", "/views/judging/images/collapse3.png");
				con.show();
			}
		}


		function SaveForm ()
		{
			var isValid = true;

			jQuery("#saveBtn").css("display", "none");
			jQuery("#pendingBtn").css("display", "none");
			jQuery("#saveWorkingImg").css("display", "");

			// run some validation
			var scoreCard = document.getElementById("scoreCard");
			var videoChecked = false;
			var essay1Checked = false;
			var essay2Checked = false;
			var letterChecked = false;

			for (var i = 0; i < scoreCard.scoreVideo.length; i++) {
				if (scoreCard.scoreVideo[i].checked) {
					videoChecked = true;
					break;
				}
			}
			for (var i = 0; i < scoreCard.scoreEssay1.length; i++) {
				if (scoreCard.scoreEssay1[i].checked) {
					essay1Checked = true;
					break;
				}
			}
			for (var i = 0; i < scoreCard.scoreEssay2.length; i++) {
				if (scoreCard.scoreEssay2[i].checked) {
					essay2Checked = true;
					break;
				}
			}
			/*for (var i = 0; i < scoreCard.scoreEssay3.length; i++) {
				if (scoreCard.scoreEssay3[i].checked) {
					essay3Checked = true;
					break;
				}
			}*/
			for (var i = 0; i < scoreCard.scoreLetter.length; i++) {
				if (scoreCard.scoreLetter[i].checked) {
					letterChecked = true;
					break;
				}
			}

			/*if (!videoChecked) {
				alert("Please provide a score for Video.");
			}
			else if (!essay1Checked) {
				alert("Please provide a score for Essay #1.");
			}
			else if (!essay2Checked) {
				alert("Please provide a score for Essay #2.");
			}
			else if (!essay3Checked) {
				alert("Please provide a score for Essay #3.");
			}
			else if (!letterChecked) {
				alert("Please provide a score for Letter of Recommendation.");
			}
			else if (!applicationChecked) {
				alert("Please provide a score for Application.");
			}
			*/

			if (!videoChecked || !essay1Checked || !essay2Checked || !letterChecked) {
				jQuery("#saveWorkingImg").css("display", "none");
				jQuery("#saveBtn").css("display", "");
				jQuery("#pendingBtn").css("display", "");
				alert("Application will be saved in the Saved Score box until all scores are entered.");
				window.setTimeout("SubmitForm('PartiallyScored')", 500);
			}
			else {
				// lets pretent we are doing something for a while
				window.setTimeout("SubmitForm('Scored')", 500);
			}
		}


		function SavePending ()
		{
			var isValid = true;

			jQuery("#pendingBtn").css("display", "none");
			jQuery("#saveBtn").css("display", "none");
			jQuery("#saveWorkingImg").css("display", "");

			// run some validation
			var scoreCard = document.getElementById("scoreCard");
			var videoChecked = false;
			var essay1Checked = false;
			var essay2Checked = false;
			var letterChecked = false;

			for (var i = 0; i < scoreCard.scoreVideo.length; i++) {
				if (scoreCard.scoreVideo[i].checked) {
					videoChecked = true;
					break;
				}
			}
			for (var i = 0; i < scoreCard.scoreEssay1.length; i++) {
				if (scoreCard.scoreEssay1[i].checked) {
					essay1Checked = true;
					break;
				}
			}
			for (var i = 0; i < scoreCard.scoreEssay2.length; i++) {
				if (scoreCard.scoreEssay2[i].checked) {
					essay2Checked = true;
					break;
				}
			}
			for (var i = 0; i < scoreCard.scoreLetter.length; i++) {
				if (scoreCard.scoreLetter[i].checked) {
					letterChecked = true;
					break;
				}
			}


			if (!videoChecked || !essay1Checked || !essay2Checked || !letterChecked) {
				jQuery("#saveWorkingImg").css("display", "none");
				jQuery("#saveBtn").css("display", "");
				jQuery("#pendingBtn").css("display", "");
				alert("Application will be saved in Pending box.");
				window.setTimeout("SubmitForm('Pending')", 500);
			}
			else {
				// lets pretent we are doing something for a while
				window.setTimeout("SubmitForm('Pending')", 500);
			}
		}


		function SubmitAppIssue (pendingComment)
		{
			var pendingCommentForm = document.getElementById("pendingCommentForm");

			pendingCommentForm.pendingComment.value = pendingComment;
			pendingCommentForm.submit();
		}


		function SubmitForm (status)
		{
			var scoreCard = document.getElementById("scoreCard");
			if(!jQuery('#reqcomment').val().length) {
				alert("Enter any comments or notes is Required");
				jQuery("#saveWorkingImg").css("display", "none");
				jQuery("#saveBtn").css("display", "");
				jQuery("#pendingBtn").css("display", "");
				return false;
			}
			scoreCard.newStatus.value = status;
			jQuery("#scoreCard").submit();
		}

		<cfif StructKeyExists(url, "callPrint")>
			jQuery().ready(function() {
				window.print();
			})
		</cfif>

	</script>

	<cfoutput>
	<form method="post" id="pendingCommentForm" style="display:none;">
		<input type="hidden" name="displayID" value="#approvedApp.displayID#" />
		<input type="hidden" name="pendingComment" value="" />
	</form>
	</cfoutput>

	<div class="content">
		<div style="margin-bottom:12px; font-size:14px; font-weight:bold; color:#000000;">
			<cfoutput>
				#UCase(variables.appType)# Display ID ###numberformat(approvedApp.displayID,"0000")#
			</cfoutput>
		</div>

		<cfset tabs = [] />

		<cfset tab = {} />
		<cfset tab.title = "EDUCATION AND EXPERIENCE" />
		<cfset tab.pods = [] />
		<cfset ArrayAppend(tab.pods, "education") />
		<cfset ArrayAppend(tab.pods, "currentPosition") />
		<cfset ArrayAppend(tab.pods, "pastPositions") />
		<cfset ArrayAppend(tab.pods, "responsibilities") />
		<cfset ArrayAppend(tab.pods, "activities") />
		<cfset ArrayAppend(tab.pods, "citizenship") />

		<cfset ArrayAppend(tabs, tab) />

		<cfset tab = {} />
		<cfset tab.title = "ACHIEVEMENTS" />
		<cfset tab.pods = [] />

		<cfset ArrayAppend(tab.pods, "honorsAwards") />

		<cfset ArrayAppend(tabs, tab) />


		<cfset settings = StructNew() />
		<cfset settings.dsn = "Sponsorships" />

		<cfoutput>
		<div class="section">
			<div class="left">&nbsp;</div>
			<div class="middle">

				<div class="plusMinus" onclick="PlusMinus('pmSummaryImg','summaryContent');">
					<img id="pmSummaryImg" src="/views/judging/images/collapse3.png" />
				</div>

				SUMMARY
			</div>
			<div class="right">&nbsp;</div>
			<div class="clear">&nbsp;</div>


			<div class="pods">
				<div class="clear">&nbsp;</div>

				<div class="summary" id="summaryContent">
<cfloop from="1" to="#ArrayLen(tabs)#" index="tabIndex">
						<div class="tabTitleLeft">&nbsp;</div>
						<div class="tabTitleCenter">
							<div class="padding">
								#tabs[tabIndex].title#
							</div>
						</div>
						<div class="tabTitleRight">&nbsp;</div>
						<div class="clear">&nbsp;</div>

						<div class="tabContent">
							<cfloop from="1" to="#ArrayLen(tabs[tabIndex].pods)#" index="i">
								<cfset curSection = CreateObject("component", "cfcs.tabbedForm.pods." & tabs[tabIndex].pods[i] ).Init(settings) />

								<div style="color:##323232; font-size:14px; font-weight:bold; margin:10px;">
									#curSection.GetPodTitle()#
								</div>
								#curSection.BuildPod(approvedApp.applicationGUID, "summary")#
							</cfloop>
						</div>
					</cfloop>
				</div>

			</div>

		</div>


		<div class="section" style="margin-top:15px;">
			<div class="left">&nbsp;</div>
			<div class="middle">

				<div class="plusMinus" onclick="PlusMinus('pmReviewImg','reviewContent');">
					<img id="pmReviewImg" src="/views/judging/images/collapse3.png" />
				</div>

				REVIEW & SCORE APPLICATION
			</div>
			<div class="right">&nbsp;</div>
			<div class="clear">&nbsp;</div>


			<div class="pods">
				<div class="clear">&nbsp;</div>

				<div class="summary" id="reviewContent">

					<div style="margin:0px 0px 8px 4px;">
						<strong>NOTE: You cannot save your entries until scoring has been completed for all sections</strong>
					</div>


					<div class="tabTitleLeft">&nbsp;</div>
					<div class="tabTitleCenter">
						<div class="padding">
							Recommendation Rating Sheet
						</div>
					</div>
					<div class="tabTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>

					<div class="tabContent">
						<div style="margin:10px;">
							<div class="question questionSpacer">
								The application requires a minimum of 1 recommendation rating sheet from an administrator or supervisor, specifically
								addressing how participation will benefit the school/students/fellow teachers.  The individual writing the
								recommendation rating sheet should be able to evaluate their character, academic and/or teaching abilities.  This
								letter must be on official letterhead.
							</div>
			<cfif rs.recordCount>
                                   <div class="question questionSpacer">
								<a href="javascript:return false;" onclick="PlusMinus('pmrecommendationContent','recommendationContent');">
									<div class="plusMinus" style="margin-top:0px;">
										<img id="pmrecommendationContent" src="/views/judging/images/collapse2.png"  />
									</div>
									View Recommendation Sheet
								</a>
							</div>
                                   <div class="question questionSpacer" id="recommendationContent" style="display:none;"><!---  --->
								<strong>Recommendation submitted by:</strong><br />
                                        #rs.firstName# #rs.lastName#, #rs.title#<br />
                                        #rs.institution#<br />
                                        #rs.street#<br />
                                        <cfif Len(rs.street2)>rs.street2<br /></cfif>
                                        #rs.city#, #rs.state#  #rs.zip#<br />
                                        #rs.phone#<br />
                                        #rs.email#<br /><br />

                                        <p><strong>How do you know applicant?:</strong> <br />#rs.capacityKnowApplicant#</p>
                                        <p><strong>Describe Applicant:</strong><br />#rs.describeApplicant#</p>
                                        <p><strong>Classroom Instructional Skills:</strong> #rs.categoryClassroomInstructionalSkills#<br />
                                        <strong>Subject Matter Knowledge:</strong> #rs.categorySubjectMatterKnowledge#<br />
                                        <strong>Initiative and Self-reliance:</strong> #rs.categoryInitiativeAndSelfReliance#<br />
                                        <strong>Creativity:</strong> #rs.categoryCreativity#<br />
                                        <strong>Integrity:</strong> #rs.categoryIntegrity#<br />
                                        <strong>Leadership Skills:</strong> #rs.categoryLeadershipSkills#<br />
                                        <strong>Interpersonal Skills:</strong> #rs.categoryInterpersonalSkills#<br />
                                        <strong>Oral Communication Skills:</strong> #rs.categoryOralCommunicationSkills#<br />
                                        <strong>Written Communication Skills:</strong> #rs.categoryWrittenCommunicationSkills#<br />
                                        <strong>Teamwork Skills:</strong> #rs.categoryTeamworkSkills#</p>
							</div></cfif>
<cfif fileExists(expandPath('/views/judging/stem-application-recommendations/#approvedApp.applicationGUID#.docx'))>						
						<div class="question questionSpacer">
							<!--- <a href="http://static.redactededucation.com/feeds/stem-application-recommendations/#app.recommendationFileName#" target="_STEM">View Letter of Recommendation</a> --->
<a href="/views/judging/stem-application-recommendations/#approvedApp.applicationGUID#.docx" target="_blank">View Letter of Recommendation</a>
						</div>
</cfif>
							
						</div>
					</div>


					<div class="tabTitleLeft">&nbsp;</div>
					<div class="tabTitleCenter">
						<div class="padding">
							Essay Guidelines
						</div>
					</div>
					<div class="tabTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>

					<div class="tabContent">
						<div style="margin:10px;">

						<div class="question questionSpacer">
							&bull; Use the rubrics to score each part of the essay submission.  The scoring range is 1 to 5.
						</div>

						<div class="question questionSpacer">
							<strong>Institute & STARs ESSAY QUESTIONS:</strong>
						</div>
						
						<div class="question questionSpacer">
							<strong>ESSAY ##1:</strong>
							<em>Why do you think STEM education is important to today's students and give two examples of 
							how you have enhanced STEM education?</em>
							<br><br>
							#app.essaySTEMInstitute1#
						</div>

						<div class="question questionSpacer">
							<strong>ESSAY ##2:</strong>
							<em>Explain what you hope to learn from attending the Siemens Academy (STARs and/or STEM Institute), 
							what you can uniquely contribute and what you would like to add to your classroom instruction upon 
							completing the experience. How will you share this information with your students and/or peers?</em>
							<br><br>
							#app.essaySTEMInstitute2#
						</div>
						
						<cfparam name="url.stemType" default="">
						<cfif url.stemType is "STARs">
						<div class="question questionSpacer">
						<b>STARs Essay Questions</b> (these two questions will not be scored):<br><br>
							<em>Please describe your laboratory skills related to using equipment such as balances, 
								fume hoods, oscilloscopes, spectrometers, etc.</em>
								<br><br>
							#app.essaySTARs1#
						</div>
						<div class="question questionSpacer">
							<em>Have you previously worked in a science profession other than teaching 
								(e.g. lab researcher, engineer, chemist)?<br>
								If yes, indicate the number of years you worked, your title, and a brief description of the position(s) below.</em>
								<br><br>
							#app.essaySTARs2#
						</div>
						</cfif>
						
                        <div class="question questionSpacer">
							<strong>Additional Information:</strong>
                        </div>
						<div class="question questionSpacer" id="essayAdditionalInformation">
                        	#app.essayAdditionalInformation#
                        </div>
						
						</div>
					</div>
					
					
					<div class="tabTitleLeft">&nbsp;</div>
					<div class="tabTitleCenter">
						<div class="padding">
							Video Guidelines
						</div>
					</div>
					<div class="tabTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>

					<div class="tabContent">
						<div style="margin:10px;">
							<!---
							<div class="question questionSpacer">
								&bull; If video exceeds 60 seconds, please deduct 1 point from the total score for every 10 seconds it exceeds 60 seconds.
							</div>
							--->

							<div class="question questionSpacer">
								&bull; Refer to the rubrics below to score the submitted video (scoring range is 1 - 5).  <!--- In the event that a video is ineligible, you may assign a score of zero (0). --->
							</div>

							<div class="question questionSpacer">
								SELECTED TOPIC:
								<strong>
		<cfif app.videoType eq "UtilizeInYourClassroom">
										Describe any new innovative ideas, materials, instructional strategies or techniques that you utilize in your classroom.
									<cfelseif app.videoType eq "ProvideSTEMOutside">
										Describe ways in which you provide student educational STEM experiences outside of the classroom.
									<cfelseif app.videoType eq "InfluencedToBecomeTeacher">
										What factors influenced your decision to become a teacher? Describe your greatest contributions and accomplishments in STEM education.
									<cfelseif app.videoType eq "IssuesEducationToday">
										What do you believe are the major issues in education today? Address one in depth, detailing potential causes, effects, and resolutions.
									</cfif>
								</strong>
							</div>

							<div class="question questionSpacer">
								VIDEO:

	<cfif Len(app.videoUTube) GT 1>
	YouTube: 
	<a href="<cfif NOT FindNoCase('http://', app.videoUTube) AND NOT FindNoCase('https://', app.videoUTube)>https://</cfif>#app.videoUTube#"
		target="_blank">#app.videoUTube#</a>
	<cfelseif Len(app.videoFileName) GT 1>
	<a href="http://static.redactededucation.com/feeds/stem-application-videos/#app.videoFileName#"
		target="_blank">#app.videoFileName#</a>
	</cfif>
							</div>
						</div>

					</div>



					<div class="tabTitleLeft">&nbsp;</div>
					<div class="tabTitleCenter">
						<div class="padding">
							SCORE
						</div>
					</div>
					<div class="tabTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>

					<div class="tabContent">
						<div style="margin:10px;">
							<form method="post" id="scoreCard">
							<input type="hidden" name="displayID" value="#approvedApp.displayID#" />
                                   <input type="hidden" name="newStatus" value="" />
							<table class="scoreCard" border="0" cellspacing="0" cellpadding="0">
								<tr class="headRow">
									<td colspan="7">
										<!--- <strong>SCORING CRITERIA</strong> ---> &nbsp;
									</td>
								</tr>
								<tr class="titleRow">
									<td style="width:190px;">&nbsp;</td>
									<!---<td style="width:74px;">Ineligible</td>--->
									<td style="width:84px;">Poor</td>
									<td style="width:84px;">Weak</td>
									<td style="width:84px;">Good</td>
									<td style="width:84px;">Great</td>
									<td style="width:84px;">Outstanding</td>
									<td style="width:74px; font-size:1.25em; text-decoration:underline;">Score</td>
								</tr>
								
								<tr class="values">
									<td style="text-align:right;"><strong>Letter of Recommendation:</strong></td>
									<cfloop from="1" to="5" index="i">
										<td>
											<input type="radio" name="scoreLetter" value="#i#" onclick="CalcScore();" <cfif judgeScores.scoreLetter eq i>checked="checked"</cfif> <cfif IsDate(judgeScores.submitted) OR judgeScores.status eq "Pending">disabled="disabled"</cfif> />
											#i#
										</td>
									</cfloop>
									<td id="letterScore" style="font-size:1.20em;">&nbsp;

									</td>
								</tr>

								
								<tr class="values">
									<td style="text-align:right;"><strong>Essay ##1:</strong></td>
									<cfloop from="1" to="5" index="i">
										<td>
											<input type="radio" name="scoreEssay1" value="#i#" onclick="CalcScore();" <cfif judgeScores.scoreEssay1 eq i>checked="checked"</cfif> <cfif IsDate(judgeScores.submitted) OR judgeScores.status eq "Pending">disabled="disabled"</cfif> />
											#i#
										</td>
									</cfloop>
									<td id="essay1Score" style="font-size:1.20em;">&nbsp;

									</td>
								</tr>

								<tr class="values">
									<td style="text-align:right;"><strong>Essay ##2:</strong></td>
									<cfloop from="1" to="5" index="i">
										<td>
											<input type="radio" name="scoreEssay2" value="#i#" onclick="CalcScore();" <cfif judgeScores.scoreEssay2 eq i>checked="checked"</cfif> <cfif IsDate(judgeScores.submitted) OR judgeScores.status eq "Pending">disabled="disabled"</cfif> />
											#i#
										</td>
									</cfloop>
									<td id="essay2Score" style="font-size:1.20em;">&nbsp;

									</td>
								</tr>
								
								<tr class="values">
									<td style="text-align:right;"><strong>Video:</strong></td>
									<cfloop from="1" to="5" index="i">
										<td>
											<input type="radio" name="scoreVideo" value="#i#" onclick="CalcScore();" <cfif judgeScores.scoreVideo eq i>checked="checked"</cfif> <cfif IsDate(judgeScores.submitted) OR judgeScores.status eq "Pending">disabled="disabled"</cfif> />
											#i#
										</td>
									</cfloop>
									<td id="videoScore" style="font-size:1.20em;">&nbsp;

									</td>
								</tr>

								<!--- <tr class="values">
									<td style="text-align:right;"><strong>Essay ##3:</strong></td>
									<cfloop from="0" to="5" index="i">
										<td>
											<input type="radio" name="scoreEssay3" value="#i#" onclick="CalcScore();" <cfif judgeScores.scoreEssay3 eq i>checked="checked"</cfif> <cfif IsDate(judgeScores.submitted)>disabled="disabled"</cfif> />
											#i#
										</td>
									</cfloop>
									<td id="essay3Score" style="font-size:1.20em;">&nbsp;

									</td>
								</tr> --->

								

								<tr class="results">
									<td colspan="6" style="text-align:right;">APPLICATION SCORE:</td>
									<td id="totalScore" style="font-weight:bold;font-size:1.35em;text-align:center;">&nbsp;

									</td>
								</tr>
							</table>

							<div class="question questionSpacer">
								<b>Enter any comments or notes</b> you may have below regarding this application.
							</div>
							<div class="question questionSpacer">
								<textarea name="comment" id="reqcomment" style="height:75px; width:695px; <cfif IsDate(judgeScores.submitted) OR judgeScores.status eq "Pending">color:##000000;</cfif>" <cfif IsDate(judgeScores.submitted) OR judgeScores.status eq "Pending">disabled="disabled"</cfif>>#judgeScores.comment#</textarea>
							</div>
							</form>

							<div style="height:30px; margin:15px auto 0px auto; width:60px;">
							<cfif not IsDate(judgeScores.submitted) AND judgeScores.status neq "Pending">
									#application.judging.BuildButton("saveBtn", "Save", "SaveForm();")#<!--- <br />
									#application.judging.BuildButton("pendingBtn", "Save to Pending", "SavePending();")# --->
								<cfelse>
									#application.judging.BuildButton("backBtn", "Back", "location.href='/index.cfm?event=judge.showApplications';")#
								</cfif>
								<img id="saveWorkingImg" src="/views/judging/images/formProcessing2.gif" style="display:none; margin:4px 0px 0px 15px;" />
							</div>


							<cfif not IsDate(judgeScores.submitted) AND judgeScores.status neq "Pending">
<script>
jQuery(document).ready(function() {
	jQuery('.reassigner').live('click', function() {
		var msg = jQuery(this).attr('msg');
		var agree=confirm(msg);
		if (!agree) {				
			return false;				
		}
		jQuery.get(
				jQuery(this).attr('vurl'), '', 
				function(data) {
								location.replace('/index.cfm?event=judge.showApplications');
								}, 
				"html"
			);
		return false;
	});
});
</script>
								<div style="text-align:center; margin:20px;">
									If you cannot judge this application 
									<a href="" class="reassigner"
		vurl="/index.cfm?event=judge.reassignJudge&judgeGUID=#client.STEMJudgeGUID#&approvedGUID=#url.approvedGUID#"
		msg="Are you sure you want to reassign this application?"
		>CLICK HERE</a>									 
									and it will be removed from your queue and reassigned.
									
									<!--- Cannot judge this application? Notify the <a href="javascript:Void();" onclick="OpenLightBoxWH('/views/judging/lightbox/contactCollegeBoard.html',405,200);">College Board here</a> --->								</div>
							</cfif>

						</div>
					</div>

				</div>

			</div>
		</div>
		</cfoutput>
	</div>

<cfinclude template="../includes/footer.cfm" />

