<CFPROCESSINGDIRECTIVE SUPPRESSWHITESPACE="Yes"><cfoutput>
<cftry>
<cfif listfindnocase("adminPreStar,adminFinalStar,Stars",client.STEMJudgeType)> 
	<cfset appType = "STARs" />
<cfelse>
	<cfset appType = "Institute" />
</cfif>
<cfset request.sectionDescription = ucase(variables.appType) & " JUDGING PHASE" />
<cfinclude template="../includes/header.cfm" />
<cftry>
<cfif (StructKeyExists(form, "submitAll") AND form.submitAll EQ "yes") AND (Len(form.displayID) EQ 0 AND Len(form.scoreGUID) EQ 0)>
	<cfset application.judging.SubmitAllScores(client.STEMJudgeGUID) />	
<cfelseif StructKeyExists(form, "displayID") AND StructKeyExists(form, "scoreGUID")>
	<cfset application.judging.SubmitScore(client.STEMJudgeGUID, form.scoreGUID) />
</cfif>
<cfcatch></cfcatch></cftry>
<cfparam name="url.col" default="displayID" />
<cfparam name="url.dir" default="asc" />


<script type="text/javascript">
	function AlertCommented(displayID)
	{
		alert("Your comment about Display ID ##" + displayID + " has successfully been submitted to the College Board.");
	}


	function AlertScored(displayID)
	{
		alert("Display ID ##" + displayID + "'s score has saved successfully.");
	}


	function AlertSubmitted(displayID)
	{
		alert("Display ID ##" + displayID + " has been submitted.");
	}


	function AlertAllSubmitted()
	{
		alert("Scored applicants have been submitted.");
	}


	function PrintApplication (approvedGUID)
	{
		document.getElementById("printFrame").src = "/index.cfm?event=judge.printApplication&approvedGUID=" + approvedGUID + "&callPrint=1";
	}


	function Sort (columnName, dir)
	{
		location.href = "/index.cfm?event=judge.showApplications&col=" + columnName + "&dir=" + dir;
	}


	function SubmitScore (scoreGUID, displayID)
	{
		if (confirm("Are you sure you want to submit Display ID ##" + displayID + "?\nOnce submitted you will not be able to update scores for this application.\n\nClick OK to submit.\nClick Cancel to cancel.")) {
			var submitForm = document.getElementById("submitForm");
			submitForm.displayID.value = displayID;
			submitForm.scoreGUID.value = scoreGUID;
			submitForm.submit();
		}
	}


	function SubmitAllScore ()
	{
		if (confirm("Are you sure you want to submit all scored applications?\nOnce submitted you will not be able to update scores for these applications.\n\nClick OK to submit.\nClick Cancel to cancel.")) {
			var submitForm = document.getElementById("submitForm");
			submitForm.submitAll.value = 'yes';
			submitForm.submit();
		}
	}

	<cfif (StructKeyExists(form, "submitAll") AND form.submitAll EQ "yes") AND (Len(form.displayID) EQ 0 AND Len(form.scoreGUID) EQ 0)>		
		window.setTimeout("AlertAllSubmitted()", 500);
	<cfelseif StructKeyExists(form, "displayID") AND StructKeyExists(form, "scoreGUID")>
		window.setTimeout("AlertSubmitted('#form.displayID#')", 500);
	<cfelseif StructKeyExists(url, "scored")>
		window.setTimeout("AlertScored('#url.scored#')", 500);
	<cfelseif StructKeyExists(url, "commented")>
		window.setTimeout("AlertCommented('#url.commented#')", 500);
	</cfif>	
</script>

<style>	
	.bdr {
		border-style:solid;
		border-color:##bbb;
		text-align:left;
		width:100%;		
		padding:4px;
	}
	.altrow1 {
		background-color:##e5e5e5;
	}
</style>

<cfset structDelete(url,"scored")>
<form method="post" id="submitForm" style="display:none;">
	<input type="hidden" name="displayID" value="" />
	<input type="hidden" name="scoreGUID" value="" />
	<input type="hidden" name="submitAll" value="" />
</form>

<div class="content">
	<div style="margin-bottom:12px; font-size:14px; font-weight:bold; color:##000;">
		 #request.contestyear# #ucase(variables.appType)# APPLICATIONS
	</div>

	<div style="margin-bottom:20px; font-size:12px;">
		Welcome to the Siemens STEM #client.STEMJudgeType# Judging Site. You will find a list of
		applications to judge below. Please click on the links to the left for more information on the judging process.
	</div>


	<div class="section">
		<div class="left">&nbsp;</div>
		<div class="middle">
			LIST OF APPLICANTS
		</div>
		<div class="right">&nbsp;</div>
		<div class="clear">&nbsp;</div>

		<div class="pods">
			<div class="clear">&nbsp;</div>
			<div class="rule">&nbsp;</div>

			<div style="font-size:1.20em; font-weight:bold; margin:8px; width:600px;">
				APPLICATION SUMMARY
			</div>

			<cfset counts = application.judging.GetApplicationsCounts(client.STEMJudgeGUID) />
			<div style="margin:0px 0px 8px 25px; font-weight:bold;">
				<div style="float:left;  width:200px;">
					Required to Score:  #counts.requiredToScore#
				</div>
				<div style="float:left; width:120px;">
					Scored: #counts.toSubmitCnt + counts.submittedCnt#
				</div>
				<div style="float:left; width:120px;">
					Remaining: #(counts.remainingCnt)#
					<!--- Pending: #counts.pendingCnt# --->
				</div>
				<div style="float:left; width:120px;">
					Submitted: #counts.submittedCnt#
				</div>
				<div class="clear">&nbsp;</div>
			</div>

			<div class="rule">&nbsp;</div>




			<div style="width:80px; margin:12px 0px 0px 640px;">
				<a href="javascript:Void();" onclick="OpenLightBoxWH('/STEMApplication/lightboxes/#variables.programName#JudgingRubric.html', 800, 500);">View Rubric</a>
			</div>

			<cfset toBeScored = application.judging.GetApplicationToBeScored(client.STEMJudgeGUID, client.STEMJudgeType) />

			<div class="pod" style="margin-top:2px;">
				<div class="podTitleLeft">&nbsp;</div>
				<div class="podTitleCenter" style="width:680px;">
					<div class="padding">
						APPLICATION TO BE SCORED
					</div>
				</div>
				<div class="podTitleRight">&nbsp;</div>
				<div class="clear">&nbsp;</div>


				<div class="podContent" style="width:682px; height:240px; overflow:scroll; overflow-x:hidden;">
					<div class="clear">&nbsp;</div>
					<cfif toBeScored.RecordCount gt 0>
						<cfloop query="toBeScored">
						<div style="border-width:0px 0px 1px 0px;"
							class="bdr<cfif Not (toBeScored.currentrow mod 2)> altrow1</cfif>">						
								<a href="/index.cfm?event=judge.score&approvedGUID=#toBeScored.approvedGUID#&stemType=#variables.appType#"
								><strong>Display ID ###numberformat(toBeScored.displayID,"0000")#</strong></a>
								<!--- #approvedGUID# --->							
						</div>
						</cfloop>
					<cfelse>
						<span style="color:##cc0000;">No new applications to be scored.</span>
					</cfif>
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div class="clear">&nbsp;</div>


			<cfset partiallyscored = application.judging.GetApplications(client.STEMJudgeGUID, client.STEMJudgeType, "PartiallyScored") />
			<cfif partiallyscored.RecordCount gt 0>
				<div class="pod" style="margin-top:2px;">
					<div class="podTitleLeft">&nbsp;</div>
					<div class="podTitleCenter" style="border-right:1px solid ##cccccc; width:580px;">
						<div class="padding">
							SAVED SCORE APPLICATIONS
						</div>
					</div>
					<div class="podTitleCenter" style="text-align:center; width:100px;">
						<div class="padding">
							DATE
						</div>
					</div>
					<div class="podTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>


					<div class="podContent" style="width:682px;">
						<div class="clear">&nbsp;</div>

						<table border="0" cellpadding="0" cellspacing="0">
							<cfloop query="partiallyscored">
								<tr <cfif partiallyscored.CurrentRow mod 2 eq 0>class="even"</cfif>>
									<td style="border-right:1px solid ##cccccc; width:581px;">
										<div style="margin:3px 0px 3px 0px;">
											&nbsp;&nbsp;
											<a href="/index.cfm?event=judge.score&approvedGUID=#partiallyscored.approvedGUID#&stemType=#variables.appType#">Display ID ###numberformat(partiallyscored.displayID,"0000")#</a>
										</div>
									</td>
									<td style="width:100px;">
										<div style="margin:3px 0px 3px 0px; text-align:center; width:100%;">
											#DateFormat(partiallyscored.created, "mmm d")#
										</div>
									</td>
								</tr>
							</cfloop>
						</table>

					</div>
					<div class="clear">&nbsp;</div>
				</div>
				<div class="clear">&nbsp;</div>
			</cfif>



			<cfset scored = application.judging.GetApplications(client.STEMJudgeGUID, client.STEMJudgeType, "Scored") />
			<cfif scored.RecordCount gt 0>
				<cfif url.col neq "displayID" OR url.dir neq "asc">
					<cfquery name="scored" dbtype="query">
						SELECT * FROM scored
						ORDER BY #url.col# #url.dir#
					</cfquery>
				</cfif>

				<div class="pod" style="margin-top:2px;">
					<div class="podTitleLeft">&nbsp;</div>
					<div class="podTitleCenter" style="border-right:1px solid ##cccccc; width:260px;">
						<cfif url.col eq "displayID" AND url.dir eq "asc">
							<div style="float:left; width:15px;" onclick="Sort('displayID', 'desc');">
								<img src="/views/judging/images/arrow_up.png" style="cursor:pointer; margin:3px 0px 0px 0px;" />
							</div>
						<cfelse>
							<div style="float:left; width:15px;" onclick="Sort('displayID', 'asc');">
								<img src="/views/judging/images/arrow_down.png" style="cursor:pointer; margin:3px 0px 0px 0px;" />
							</div>
						</cfif>
						<div class="padding" style="float:left;">
							SCORED APPLICATIONS
						</div>
						<div class="clear">&nbsp;</div>
					</div>
					<div class="podTitleCenter" style="border-right:1px solid ##cccccc; text-align:center; width:100px;">
						<cfif url.col eq "created" AND url.dir eq "asc">
							<div style="float:left; width:15px;" onclick="Sort('created', 'desc');">
								<img src="/views/judging/images/arrow_up.png" style="cursor:pointer; margin:3px 0px 0px 3px;" />
							</div>
						<cfelse>
							<div style="float:left; width:15px;" onclick="Sort('created', 'asc');">
								<img src="/views/judging/images/arrow_down.png" style="cursor:pointer; margin:3px 0px 0px 3px;" />
							</div>
						</cfif>
						<div class="padding" style="float:left;">
							DATE
						</div>
						<div class="clear">&nbsp;</div>
					</div>
					<div class="podTitleCenter" style="border-right:1px solid ##cccccc; text-align:center; width:100px;">
						<cfif url.col eq "scoreTotal" AND url.dir eq "asc">
							<div style="float:left; width:15px;" onclick="Sort('scoreTotal', 'desc');">
								<img src="/views/judging/images/arrow_up.png" style="cursor:pointer; margin:3px 0px 0px 3px;" />
							</div>
						<cfelse>
							<div style="float:left; width:15px;" onclick="Sort('scoreTotal', 'asc');">
								<img src="/views/judging/images/arrow_down.png" style="cursor:pointer; margin:3px 0px 0px 3px;" />
							</div>
						</cfif>

						<div class="padding" style="float:left;">
							SCORE
						</div>
						<div class="clear">&nbsp;</div>
					</div>
					<div class="podTitleCenter" style="width:220px;">
						<div class="padding">
							ACTIONS
						</div>
					</div>
					<div class="podTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>


					<div class="podContent" style="width:685px;">
						<div class="clear">&nbsp;</div>

						<table border="0" cellpadding="0" cellspacing="0">
							<cfloop query="scored">
								<tr <cfif scored.CurrentRow mod 2 eq 0>class="even"</cfif>>
									<td style="border-right:1px solid ##cccccc; width:261px;">
										<div style="margin:3px 0px 3px 0px;">
											&nbsp;&nbsp;
											Display ID ###numberformat(Scored.displayID,"0000")#
										</div>
									</td>
									<td style="border-right:1px solid ##cccccc; width:100px;">
										<div style="margin:3px 0px 3px 0px; text-align:center; width:100%;">
											#DateFormat(scored.created, "mmm d")#
										</div>
									</td>
									<td style="border-right:1px solid ##cccccc; width:100px;">
										<div style="margin:3px 0px 3px 0px; text-align:center; width:100%;">
											#scored.scoreTotal#
										</div>
									</td>

									<td style="width:220px;">
										<div style="margin:3px 0px 3px 0px;  width:100%;">
											<div style="float:left;margin-left:10px;width:65px;">
												<a href="/index.cfm?event=judge.score&approvedGUID=#scored.approvedGUID#&stemType=#variables.appType#">
													<img src="/views/judging/images/edit.png" />
													<cfif IsDate(scored.submitted)>
														View
													<cfelse>
														Edit
													</cfif>
												</a>
											</div>

											<div style="float:left;width:65px;">
												<a href="javascript:Void();" onclick="PrintApplication('#scored.approvedGUID#');">
													<img src="/views/judging/images/printer.png" />
													Print
												</a>
											</div>

											<div style="float:left;width:80px;">
												<cfif IsDate(scored.submitted)>
													<img src="/views/judging/images/check.png" />
													Submitted
												<cfelse>
													<a href="javascript:Void();" onclick="SubmitScore('#scored.scoreGUID#', '#scored.displayID#');">
														<img src="/views/judging/images/submit.png" />
														Submit
													</a>
												</cfif>
											</div>
											<div class="clear">&nbsp;</div>
										</div>
									</td>
								</tr>
							</cfloop>
						</table>

					</div>
                         <br />
                         <a href="javascript:Void();" onclick="SubmitAllScore();">
                              <img src="/views/judging/images/submit.png" />
                              Submit All Scored Applications
                         </a>
					<div class="clear">&nbsp;</div>
				</div>
				<div class="clear">&nbsp;</div>
			</cfif>
			<cfset pending = application.judging.GetApplications(client.STEMJudgeGUID, client.STEMJudgeType, "Pending") />
			<cfif pending.RecordCount gt 0>
				<div class="pod" style="margin-top:2px;">
					<div class="podTitleLeft">&nbsp;</div>
					<div class="podTitleCenter" style="border-right:1px solid ##cccccc; width:580px;">
						<div class="padding">
							PENDING APPLICATIONS
						</div>
					</div>
					<div class="podTitleCenter" style="text-align:center; width:100px;">
						<div class="padding">
							DATE
						</div>
					</div>
					<div class="podTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>


					<div class="podContent" style="width:682px;">
						<div class="clear">&nbsp;</div>

						<table border="0" cellpadding="0" cellspacing="0">
							<cfloop query="pending">
								<tr <cfif pending.CurrentRow mod 2 eq 0>class="even"</cfif>>
									<td style="border-right:1px solid ##cccccc; width:581px;">
										<div style="margin:3px 0px 3px 0px;">
											&nbsp;&nbsp;
											<a href="/index.cfm?event=judge.score&approvedGUID=#pending.approvedGUID#&stemType=#variables.appType#">Display ID ###numberformat(pending.displayID,"0000")#</a>
										</div>
									</td>
									<td style="width:100px;">
										<div style="margin:3px 0px 3px 0px; text-align:center; width:100%;">
											#DateFormat(pending.created, "mmm d")#
										</div>
									</td>
								</tr>
							</cfloop>
						</table>

					</div>
					<div class="clear">&nbsp;</div>
				</div>
				<div class="clear">&nbsp;</div>
			</cfif>
		</div>

	</div>

	<div class="clear">&nbsp;</div>
</div>

<iframe src="/views/judging/empty.html" id="printFrame" name="printFrame" style="height:1px; visibility:hidden; width:1px;"></iframe>


<cfinclude template="../includes/footer.cfm" />


<cfcatch>

#cfcatch.message#<br>
#cfcatch.detail#<br>
#cfcatch.tagcontext[1].template# #cfcatch.tagcontext[1].line#

</cfcatch></cftry>

</cfoutput></CFPROCESSINGDIRECTIVE>