<cfif not StructKeyExists(application, "reports") OR StructKeyExists(url, "reinit") OR StructKeyExists(url, "reload") OR StructKeyExists(url, "restart")>
	<cfset application.reports = CreateObject("component", "cfcs.reports.reports").Init(StructNew()) />
</cfif>

<cfoutput>#application.reports.DisplayReportsPage()#</cfoutput>
