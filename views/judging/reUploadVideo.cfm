
<cfsavecontent variable="cssScripts">
	
	<link rel="stylesheet" style="text/css" href="/cfcs/tabbedForm/css/tabbedForm.css" media="all" />
	<!--- This is an IE workaround, wouldn't need to do this if our header was STRICT XHTML.
			IE CSS expression doesn't seem to work, so CF calculates if it is IE.  --->
	<cfif FindNoCase("MSIE", cgi.http_user_agent) gt 0>
		<style type="text/css" media="all"> 
			div.app div.clear {
				margin:-1px 0px 0px 0px !important;
			}
			
			table.survey tr.alt td {
				background-color: ##f2f8ff;
			}
		</style>
	</cfif>
	
	<link type="text/css" href="/cfcs/tabbedForm/css/redmond/jquery-ui-1.8.6.custom.css" rel="stylesheet" />
	<link type="text/css" href="/cfcs/tabbedForm/js/fancybox/jquery.fancybox-1.3.1.css" rel="stylesheet" />
	
	
	
	<style type="text/css">
		html, body {
			height:auto !important;
			height:100%;
			min-height:100%;
			margin:0; padding:0;
			font-size:11px;
			font-family:Tahoma, Arial, Helvetica, sans-serif;margin:0; padding:0;
		}
		
		div.tabContent div.pod{
			padding-left:25px;
		}
	</style>
</cfsavecontent>


<cfsavecontent variable="jsScripts">
	<script type="text/javascript" src="/cfcs/tabbedForm/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
	<script type="text/javascript" src="/cfcs/tabbedForm/js/jquery-1.4.3.min.js"></script>
	<script type="text/javascript" src="/cfcs/tabbedForm/js/jquery-ui-1.8.6.custom.min.js"></script>
	
	<script type="text/javascript" src="/cfcs/tabbedForm/js/simpleValidation.js"></script>
	
	<script type="text/javascript" src="/cfcs/tabbedForm/js/tabbedForm.js"></script>
	<script type="text/javascript">
		// first make sure jQuery doesn't conflict with any other existing APIs.
		jQuery.noConflict();  // now any jQuery scripts must replace '$' with 'jQuery'
		
		var remoteURL = "/cfcs/tabbedForm/tabbedForm.cfc";
		jQuery().ready(function () {
			
		});
	</script>
	
	
</cfsavecontent>


<cfset event.setArg("css", cssScripts) />
<cfset event.setArg("js", jsScripts) />
<cfset event.setArg("pageTitle", "2011 Siemens STEM Academy Institute and STARs Video Upload") />

<cfset tabs = [] />
			
<cfset tab = {} />
<cfset tab.title = "VIDEO" />
<cfset tab.pods = [] />
<cfset ArrayAppend(tab.pods, "uploadVideoReUpload") />
<cfset ArrayAppend(tabs, tab) />


<cfset settings = StructNew() />
<cfset settings.dsn = "Sponsorships" />


<cfoutput>
<div class="app">
	#CreateObject("component", "cfcs.tabbedForm.pods.uploadVideoReUpload" ).Init(settings).BuildPod(url.applicationGUID, "edit")#
</div>
</cfoutput>

