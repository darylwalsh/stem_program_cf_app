<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h3> STEM STARS</h3>

<b>Dates:</b>

<p><b>STARS:Oak Ridge:June 16-28,2013 </b></p>
<p><b>STARS:Pacific Northwest:July 14-26,2013 </b></p>
<p>Congratulations to the 2013 Siemens Teachers as Researchers (STARs) Fellows! Two groups of twenty educators have been chosen to attend an all-expenses paid, two-week professional development program at one of the U.S. Department of Energy's national laboratories this summer – Oak Ridge National Laboratory in Tennessee or Pacific Northwest National Laboratory in Washington.
STARs provides middle school and high school STEM teachers the opportunity to engage with top scientists and researchers on short-term research projects about current topics of national interest that are related to ongoing research at the laboratory. Working in small teams, teachers will collaborate with laboratory scientific teams to conduct assigned research projects on current topics of national interest. ORNL or PNNL will provide additional leadership and resources to help you incorporate research into your science and math classrooms.
In addition to the research immersion, each lab will provide additional educational enhancements throughout the program in the form of seminars, mini-workshops, tours, and field trips to focus and expand the research experience.




</p>
<h3>2013 SIEMENS STEM STARS FELLOWS</h3>
            <img src="../img/learn_stars.PNG" />
			<UL>
<ul>
				<li>
				Beth Allcox,New Holstein, WI 
					<br />
					New Holstein High School 
				</li>
				<li>
	Enrique Arce-Larreta Salt Lake City, UT 
					<br />
 West High School 
				</li>
				<li>
			 David Beckman,Minnetonka, MN 
					<br />
	 Hopkins North Jr. High School 
				</li>
				<li>
		 Bonnie Bourgeous Layton, UT 
					<br />
		 North Layton Jr. High School 
			  </li>
			  <li>
					Daniel Babauta Brooklyn, NY 
					<br />
			Sunset Park High School 
			  </li>
				<li>
				Brian Cole,Garner, NC 
					<br />
East Garner Magnet School 
			  </li>
				<li>
		Jerry De Carlo,Cleveland, OH 
					<br />
St. Ignatius High School 
			  </li>
				<li>
			Gary Duquette,Jackson, WY 
					<br />
		Jackson Hole High School 
			  </li>
				<li>
Donna Earle,Brentwood, CA 
					<br />
				Heritage High School 
			  </li>
				<li>
				 April Ho,Bronx, NY 
					<br />
				New World High School 
  </li>
				<li>
				Eve Kersey,Harrodsburg, KY 
					<br />
	 Kenneth D. King Middle School 
			  </li>
				<li>
Denise Magrini ,Mendham, NJ 
					<br />
		Mountain View Middle School
			  </li>
				<li>
	Amanda Peretich,Frederick, MD 
					<br />
				 Calvert High School 
			  </li>
				<li>
			Lorraine Plageman,Jupiter, FL <br />Jupiter Community High School 
				</li>
				<li>
				Diane Ripollone,Raleigh, NC 
					<br />
Cardinal Gibbons High School 
				</li>
				<li>
				 Desmond Rowe,Baltimore, MD 
					<br />
				 West Baltimore Middle School 80 
				</li>
				<li>
				Brenda Talbert,Whippany, NJ Somerset 
					<br>
	Memorial Junior High School 
				</li>
		
                         <li>
	 Melanie Tucker,Haines City, FL 
					<br />
		 Daniel Jenkins Academy 
				</li>
				<li>
				Amanda Whaley,Houston, TX 
					<br />
	Kipp Academy Middle School 
				</li>
</UL>
			&nbsp;&nbsp;&nbsp;&nbsp; 


</body>
</html>