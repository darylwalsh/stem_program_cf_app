
<cfif not StructKeyExists(cookie, "SPONSOR_USER_GUID") or not IsValid("guid", cookie.SPONSOR_USER_GUID) or cookie.SPONSOR_USER_GUID eq "" or cookie.SPONSOR_USER_GUID eq "00000000-0000-0000-0000-000000000000">
	<cflocation url="/index.cfm?event=showLabCoat" addtoken="false" />
</cfif>


<!--- Still need to check if they already have registered for a lab coat --->

<cfquery name="isRegistered" datasource="Sponsorships">
	SELECT * FROM SPONSOR_PROF_RESP with (nolock)
	WHERE 	SPONSOR_USER_GUID = <cfqueryparam value="#cookie.SPONSOR_USER_GUID#" cfsqltype="cf_sql_varchar" /> AND
			SPONSOR_PROF_SETTING_GUID = <cfqueryparam value="CCCCCCCC-CCCC-CCCC-0001-DDDDDDDDDDDD" cfsqltype="cf_sql_varchar" /> <!--- labCoatStreet --->
</cfquery>
<cfif isRegistered.RecordCount gt 0>
	<cflocation url="/index.cfm?event=showLabCoatFormThanks&returning=1" addtoken="false" />
</cfif>



<cfinvoke component="cfcs.register" method="getSettingsQuestions" returnvariable="qGetSettingsQuestions">
	<cfinvokeargument name="Code" value="#application.SPONSOR_CODE_LAB_COAT#" />
</cfinvoke>

<cfif StructKeyExists(form, "fieldNames")>
	<cfloop query="qGetSettingsQuestions">
		<cfset formVal = "" />
		<cfif StructKeyExists(form, qGetSettingsQuestions.CODE)>
			<cfset formVal = form[qGetSettingsQuestions.CODE] />
		</cfif>
		
		<cfquery name="qInsertSponsorASSNUser" datasource="Sponsorships">
			EXECUTE Sponsorships.dbo.usp_SPONSOR_PROF_RESP_Insert 
						@LoggedOnUserID = '00000000-0000-0000-0000-000000000000'
						,@SPONSOR_USER_GUID = '#cookie.SPONSOR_USER_GUID#'
						,@SETTING_VALUE = '#trim(formVal)#'
						,@SPONSOR_PROF_SETTING_GUID = '#qGetSettingsQuestions.SPONSOR_PROF_SETTING_GUID#'
						,@DOLOG = '~'
		</cfquery>
	</cfloop>
	
	<cflocation url="/index.cfm?event=showLabCoatFormThanks" addtoken="false" />
</cfif>


<cfset event.setArg("pageTitle","#request.siteName# - Lab Coat Giveaway") />

<link type="text/css" rel="stylesheet" href="/css/registerStyle.css" />
<link type="text/css" rel="stylesheet" href="/css/de-ui-style.css" />
<style type="text/css">
	div.registrationSection {
		margin:20px 0px 0px 100px;
	}
	
	div.registrationSection div.formRow {
		margin:0px 0px 15px 0px;
	}
	
	div.registrationSection div.err {
		float:left;
		position:relative;
		width:100px;
	}
	
	div.registrationSection label {
		float:left;
		margin-left:8px;
		width:170px;
	}
	
	div.registrationSection div.formInput {
		float:left;
		width:200px;
		position:relative;
	}
	
	div.registrationSection div.formInput select {
		font-size:12px;
	}
	
	div.registrationSection div.formInput input.topLeft {  // IE workaround
		top:0px;
		left:0px;
		position:absolute;
	}
	
	div.clear {
		clear:both;
		font-size:0px;
		height:0px;
		line-height:0px;
		margin:0px;
		padding:0px;
		width:100%;
	}
</style>

<script type="text/javascript">
	// Custom Validation - If elements are added/removed from the form then this needs to be updated.
	function ValidateForm (aform) 
	{
		var isValid = true;
		
		if (Trim(aform.labCoatStreet.value) == "") {
			aform.labCoatStreet.onkeyup = function () {
				document.getElementById("spanlabCoatStreet").className = "";
				this.onkeyup = function () {}
			}
			document.getElementById("spanlabCoatStreet").className = "invalid";
			isValid = false;
		}
		if (Trim(aform.labCoatCity.value) == "") {
			aform.labCoatCity.onkeyup = function () {
				document.getElementById("spanlabCoatCity").className = "";
				this.onkeyup = function () {}
			}
			document.getElementById("spanlabCoatCity").className = "invalid";
			isValid = false;
		}
		if (aform.labCoatState.selectedIndex == 0) {
			aform.labCoatState.onchange = function () {
				document.getElementById("spanlabCoatState").className = "";
				this.onchange = function () {}
			}
			document.getElementById("spanlabCoatState").className = "invalid";
			isValid = false;
		}
		if (Trim(aform.labCoatZip.value) == "") {
			aform.labCoatZip.onkeyup = function () {
				document.getElementById("spanlabCoatZip").className = "";
				this.onkeyup = function () {}
			}
			document.getElementById("spanlabCoatZip").className = "invalid";
			isValid = false;
		}
		if (aform.labCoatSize.selectedIndex == 0) {
			aform.labCoatSize.onchange = function () {
				document.getElementById("spanlabCoatSize").className = "";
				this.onchange = function () {}
			}
			document.getElementById("spanlabCoatSize").className = "invalid";
			isValid = false;
		}
		
		if (isValid) {
			document.getElementById("btnSubmit").style.display = "none";
			document.getElementById("processingForm").style.display = "";
			aform.submit();
		}
	}
	
	function Trim(astring)
	{
		return astring.replace(/^\s*/, "").replace(/\s*$/, ""); //Trim the start and end of string;
	}
</script>


<cfquery name="getResourceCount" datasource="Sponsorships">
	SELECT * FROM cms_resources with (nolock)
	WHERE createdBy = <cfqueryparam value="#cookie.SPONSOR_USER_GUID#" cfsqltype="cf_sql_varchar" />
</cfquery>


<div id="content-left">
	<h2>Lab Coat Giveaway</h2>
	<h3>Share Your Resources and Receive a Free Lab Coat</h3>
	<br />
	
	<p>
		<cfif getResourceCount.RecordCount gte 2>
			To receive your free lab coat complete the following form.
		<cfelse>
			<cfset toDo = 2 - getResourceCount.RecordCount />
			To receive your free lab coat share <cfoutput>#toDo#</cfoutput> more resource<cfif toDo gt 1>s</cfif> and complete the following form.
		</cfif>
	</p>
	<br />
	
	<h3 class="icon-title" style="width:430px;">
		<img src="/images/icons-person.gif" alt="icon" />
		<span>Mailing Address and Coat Size</span>
	</h3>
	<div class="clear">&nbsp;</div>
	
	<cfoutput>
	<form method="post" name="frmLabCoat" id="frmLabCoat">
	<div class="registrationSection">
		<cfloop query="qGetSettingsQuestions">
			
			<div class="formRow">
				<div class="err">
					<span id="span#qGetSettingsQuestions.CODE#"></span>
					&nbsp;
				</div>
				<label for="#qGetSettingsQuestions.CODE#">#qGetSettingsQuestions.DESCRIPTION#:</label>
				<div class="formInput"> 
					<cfif qGetSettingsQuestions.DATATYPE eq "text">
						<input class="topLeft" type="text" name="#qGetSettingsQuestions.CODE#" id="#qGetSettingsQuestions.CODE#" value="" />
					<cfelseif qGetSettingsQuestions.DATATYPE eq "checkbox">
						<input type="checkbox" name="#qGetSettingsQuestions.CODE#" id="#qGetSettingsQuestions.CODE#" value="1" />
					<cfelseif qGetSettingsQuestions.DATATYPE eq "select">
						<cfinvoke component="cfcs.register" method="getSettings" returnvariable="qGetSettingsDomainValues">
							<cfinvokeargument name="Code" value="#application.SPONSOR_CODE_LAB_COAT#" />
							<cfinvokeargument name="SPONSOR_PROF_SETTING_GUID" value="#qGetSettingsQuestions.SPONSOR_PROF_SETTING_GUID#" />
						</cfinvoke>
						
						<select name="#qGetSettingsDomainValues.CODE#" id="#qGetSettingsDomainValues.CODE#">
							<option value=""> - Choose a #qGetSettingsQuestions.DESCRIPTION# - </option>
							<cfloop query="qGetSettingsDomainValues">
								<option value="#qGetSettingsDomainValues.VALUE#">#qGetSettingsDomainValues.DISPLAY_TEXT#</option>
							</cfloop>
						</select>
					</cfif>
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		</cfloop>
	</div>
	</cfoutput>
	
	<div class="registrationSection">
		<div class="formRow">
			<br />
			<input id="btnSubmit" class="sprite" type="button" title="Submit" value="" style="border:0px; cursor:pointer; margin:0px 0px 0px 300px;" onclick="ValidateForm(this.form);" />
			
			<span id="processingForm" style="display:none;"><div style="margin:10px 0px 9px 338px;"><img src="/images/ajax-greenWait.gif" /></div></span>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
	</form>
</div>
<div id="content-right"><img src="images/coat2.jpg" alt="" /></div>
<!-- ///content-right/// -->
