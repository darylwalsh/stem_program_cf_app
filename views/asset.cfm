<cfsilent>
	<cfset currentURL = CGI.SERVER_NAME & CGI.SCRIPT_NAME & "?" & CGI.QUERY_STRING />
	<cfset asset = event.getArg("asset") />
	<cfset taxonomy = event.getArg("taxonomy") />
	<cfset mediaFiles = event.getArg("mediaFiles") />
	<cfset assetTaxonomys = event.getArg("assetTaxonomys") />
	<cfset event.setArg("pageTitle", "#request.siteName# - #asset.getTitle()#") />
	<cfset contentRating = 0 />
	<cfset contentRatingCount = 0 />
	<cfset averageRating = event.getArg("averageRating") />
	<cfif averageRating.recordCount>
		<cfset contentRating = averageRating.avgRating />
		<cfset contentRatingCount = averageRating.ratingCount />
	</cfif>
	<cfset userContentRating = 0 />
	<cfset userRating = event.getArg("userRating") />
	<cfif userRating.getRating() NEQ "">
		<cfset userContentRating = userRating.getRating() />
	</cfif>
	<cfset app_user_guid = "00000000-0000-0000-0000-000000000000" />
	<cfset isLocked = true />
	<cfif structKeyExists(cookie,"app_user_guid")>
		<cfset app_user_guid = cookie.app_user_guid />
		<cfset isLocked = false />
	</cfif>
	<cfset lstRoles = "STUDENT" />
	<cfif StructKeyExists(client,"rolelist")>
		<cfset lstRoles = client.rolelist />
	</cfif>
	<cfset account_guid = "10000000-0000-1DC8-5997-000000000000" />
	<cfif StructKeyExists(client,"account_guid")>
		<cfset account_guid = client.account_guid />
	</cfif>
	<cfset message = event.getArg("message") />
</cfsilent>
<cfoutput>
<script type="text/javascript">
<!--
function saveStar(event) {
  new Ajax.Request('index.cfm?event=ajax.saveStar&guidAssetId=#asset.getGuidAssetMetaDataId()#&app_user_guid=#app_user_guid#', {
    parameters: event.memo,
    onComplete: function(xhr) {
      // optional callback
    }
  });
}
// observing all starboxes
document.observe('starbox:rated', saveStar);
-->
</script>
</cfoutput>
<cfoutput>
	<cfif message NEQ "">
		<div id="message" style="color:red;font-size:18px;">#message#</div>
		<br />
	</cfif>
	<h1 class="asset">#asset.getTitle()#</h1>	
	<div id="assetContent">
		#asset.getTitle_description()#
	<cfif NOT isLocked>
		<div id="contentComment" style="border-top:##BCBCBC solid 1px; padding:5px;margin:5px;">
			<form name="contentRatingForm" action="#BuildUrl('saveContentRating')#" method="post">
				<input type="hidden" name="guidAssetId" value="#asset.getGuidAssetMetaDataId()#" />
				<input type="hidden" name="app_user_guid" value="#app_user_guid#" />				
				<!---amaro 081309: removed per ticket BTS-9377
				Rating: <select name="rated">
							<option value="5" <cfif userRating.getRating()EQ 5>selected</cfif>>5</option>
							<option value="4" <cfif userRating.getRating()EQ 4>selected</cfif>>4</option>
							<option value="3" <cfif userRating.getRating()EQ 3>selected</cfif>>3</option>
							<option value="2" <cfif userRating.getRating()EQ 2>selected</cfif>>2</option>
							<option value="1" <cfif userRating.getRating()EQ 1>selected</cfif>>1</option>
						</select>
				<br />--->
				Leave a comment:
				<br />
				<textarea name="notes" style="width:450px;height:100px;">#userRating.getNotes()#</textarea>
				<br />
				<input type="submit" value="Send" />
			</form>
		</div>
	</cfif>
	</div><!--id:assetContent-->
	<div id="assetRightCol">
		<div id="email-print-box" class="option-boxes">
			<a title="E-mail" href="#BuildUrl('showEmailAsset','guidAssetId=#guidAssetId#')#" class="email-link">E-mail</a>
            <br />
			<a title="Print" href="#BuildUrl('printAsset','guidAssetId=#guidAssetId#')#" class="print-link" target="_blank">Print</a>
		</div>
		<div id="rating-box" class="option-boxes">
			<div>
				Average Rating:<br />
				<div id="averageRating"><img src="img/loading.gif" /></div>
				<script type="text/javascript">
					new Starbox('averageRating',
								#contentRating#,
								{
								color: '##1761A5',
								identity: '#asset.getGuidAssetMetaDataId()#',
								indicator: '(#contentRatingCount# ratings)',
								locked: true,
								total: #contentRatingCount#
								});
				</script>
			</div>
	<cfif NOT ListFindNoCase(lstRoles,"STUDENT")>
		<cfif account_guid NEQ "10000000-0000-1DC8-5997-000000000000">
			<div style="margin-top:5px;">
				Rate it:
				<div id="userRating"><img src="img/loading.gif" /></div>
				<script type="text/javascript">
					new Starbox('userRating',
								#userContentRating#,
								{
								color: 'gold',
								identity: '#asset.getGuidAssetMetaDataId()#',
								locked: #isLocked#,
								rerate: true
								});
				</script>
				<cfif isLocked>
					<a href="#request.hubUrl#/login.cfm?returnUrl=#URLEncodedFormat(currentUrl)#">Please sign in to rate.</a>
				</cfif>
			</div>
		</cfif>
	</cfif>
		</div>
	<cfif arrayLen(mediaFiles) NEQ 0>
		<div class="resBox">
		<h3><b>Resources</b></h3>
		<p>
		<cfloop index="i" from="1" to="#arrayLen(mediaFiles)#">
			<a target="_blank" href="#mediaFiles[i].getMediaFileServerUrl()##mediaFiles[i].getStrFilePath()##mediaFiles[i].getStrFileName()#" title="#mediaFiles[i].getStrFileName()#">#application.udf.maxLength(mediaFiles[i].getStrFileName(),20)#</a>
			<br />
			<b class="gr">File Size:</b> #application.udf.fileSize(mediaFiles[i].getIntFileSizeBytes())#
			<br /><br />
			&nbsp;&nbsp;
			<!--Download Link-->
			<a title="Download #mediaFiles[i].getStrFileName()#" href="#BuildUrl('getFile','guidAssetMediaFileId=#mediaFiles[i].getGuidAssetMediaFileId()#')#"><img src="img/download.gif" alt="Download File" /></a>
			<a title="Download #mediaFiles[i].getStrFileName()#" href="#BuildUrl('getFile','guidAssetMediaFileId=#mediaFiles[i].getGuidAssetMediaFileId()#')#" class="subLinks">Download</a>
			<br />
		</cfloop>
		</p>
		<span class="resBox_btm"></span>
		</div><!--class:resBox-->
	</cfif>
	<cfif assetTaxonomys.recordCount>
		<span id="subjects">
		<h5>Subjects:</h5>
		<cfloop query="assetTaxonomys">
			<a href="#BuildUrl('showNode','guidTaxId=#guidTaxId#')#">#parentName# &gt; #name#</a><br />
		</cfloop>
		</span>
	</cfif>
		<div style="margin-left:14px;margin-top:16px;color:##CCCCCC">(#asset.getCopyright()# views)</div>
	</div><!--assetRightCol-->
</cfoutput>