<cfsilent>
	<cfset tcontent = event.getArg("tcontent") />
	<cfset event.setArg("pageTitle", "#request.siteName# - #tcontent.getTitle()#") />
	<cfsavecontent variable="js">
<script type="text/javascript" src="/js/lightbox.min.js"></script>
<script type="text/javascript" src="/js/carousel.min.js"></script>
<script type="text/javascript">
function createCarousels() { 
	$(document.body).select('.carousel-container').each(function(e) { 
		new Carousel(e.down('.carousel-wrapper'), e.select('.carousel-content .slide'), e.select('a.carousel-control'), {
			duration: 1,
			visibleSlides: 3,
			circular: false
		});
	});
}
document.observe("dom:loaded", function() {
	createCarousels();
});
</script>
	</cfsavecontent>
	<cfsavecontent variable="css">
<link rel="stylesheet" href="/css/lightbox.css" type="text/css" media="screen" />
	</cfsavecontent>
	<cfset event.setArg("js",js) />
	<cfset event.setArg("css",css) />
</cfsilent>
<cfoutput>
	<cfif tcontent.getType() EQ "custom">
		<cflocation url="#tcontent.getFileName()#&c=#tcontent.getTcontent_id()#" addtoken="false" />
	<cfelseif tcontent.getType() EQ "page">
 		#tcontent.getBody()#
	</cfif>
</cfoutput>