<cfsilent>
	<cfset currentURL = CGI.SERVER_NAME & CGI.SCRIPT_NAME & "?" & CGI.QUERY_STRING />
	<cfset asset = event.getArg("asset") />
	<cfset event.setArg("pageTitle", "#request.siteName# - Email this article - #asset.getTitle()#") />
	<cfset email = "" />
	<cfif StructKeyExists(client,"email")>
		<cfset email = client.email />
	</cfif>
</cfsilent>
<cfoutput>
	<div style="border:1px solid ##CCCCCC;background-color:##F0F4F5;">
		<form action="#BuildUrl('emailAsset')#" method="post" name="emailAssetForm" id="emailAssetForm">
			<input type="hidden" name="guidAssetId" id="guidAssetId" value="#asset.getGuidAssetMetaDataId()#" />
			<input type="hidden" name="title" id="title" value="#asset.getTitle()#" />
			<h1 class="asset">#asset.getTitle()#</h1>	
			<div style="margin:4px 0 0 8px;">
				#application.udf.abbreviate(trim(asset.getTitle_description()),255)#
			</div><!--id:assetContent-->
			<div style="margin:4px;border-top:1px solid ##CCCCCC;border-bottom:1px solid ##CCCCCC;">
				<table>
					<tr>
						<td><label for="sender"><strong>Your E-mail:</strong></label></td>
						<td><input type="text" value="#email#" name="sender" id="sender" style="width:300px;" /><label for="copytoself"><input type="checkbox" value="1" name="copytoself" id="copytoself" />Send me a copy</label></td>
					</tr>
					<tr>
						<td><label for="recipients"><strong>Recipient's E-Mail:</strong></label></td>
						<td><input type="text" value="" name="recipients" id="recipients" style="width:400px;" /></td>
					</tr>
					<tr>
						<td>Your message (optional):</td>
						<td><textarea name="yourMessage" style="width:400px;"></textarea></td>
					</tr>
				</table>
				<p>(Separate multiple e-mail addresses with commas. Limited to 20 addresses.  The e-mail addresses that you supply to use this service will not be shared with any third parties.)</p>
			</div>
			<p style="text-align:right;margin:4px;"><input type="submit" value="Send" /></p>
		</form>
	</div>
</cfoutput>