<cfset event.setArg("pageTitle","#request.siteName# - Lab Coat Giveaway") />

<script type="text/javascript">
/**
*
*  URL encode / decode
*  http://www.webtoolkit.info/
*
**/
 
var Url = {
 
	// public method for url encoding
	encode : function (string) {
		return escape(this._utf8_encode(string));
	},
 
	// public method for url decoding
	decode : function (string) {
		return this._utf8_decode(unescape(string));
	},
 
	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	},
 
	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utftext.length ) {
 
			c = utftext.charCodeAt(i);
 
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 
		}
 
		return string;
	}
 
}
	function redirect(r) {
		window.location = r;
	}
	function ajaxSubmitForm(formName) {
		var form = $(formName);
		var r = Url.decode(form['r'].value);
		$('lb-invalid').hide();
		form.request({
		  onSuccess: function(transport) {
			var successText = String(transport.responseText);
			if (successText.indexOf('false')!=-1){
				$('lb-invalid').appear();
			}else{
				//clearWin();
				$('lbLoginForm').hide();
				$('lbLoading').show();
				//setTimeout(function(){redirect(r)},1000);
		  	}
		  }
		});
	}
</script>

<div id="content-left">
	<h2>Lab Coat Giveaway</h2>
	<h3>Share Your Resources and Receive a Free Lab Coat</h3>
	<br />
	
	
	<p>
		The Siemens STEM Academy proudly hosts a wide range of valuable educational resources created by educators like you. Your lesson plans, multi-media presentations, worksheets, and other resources help other educators from around the country bring STEM education to life in their classrooms. 
	</p>
    
    
    <p>
		We'd like to thank you for sharing your resources and encourage you to make the site even stronger by offering a free lab coat  to those who upload two or more resources. Be among the first 100 teachers to upload these resources and receive a FREE Siemens STEM Academy lab coat to wear in your classroom.
	</p>



    <p>
		Get started by uploading your STEM resources today.
	</p>
	
	<h5 style="margin:20px 0 20px 0;font-size:1.6em;">How It Works:</h5>
	
    <div style="position:relative;margin-left:10px;">
    
        
        <h5 style="font-size:1.25em;text-transform:uppercase;">
            Step 1: Upload your resources.
        </h5>
        <p>
            Upload two or more resources to the Siemens STEM Academy resource page. 
        </p>
        <p style="margin-bottom:50px;">
            <cfif StructKeyExists(cookie, "SPONSOR_USER_GUID") and IsValid("guid", cookie.SPONSOR_USER_GUID) and cookie.SPONSOR_USER_GUID neq "" and cookie.SPONSOR_USER_GUID neq "00000000-0000-0000-0000-000000000000">
                <a id="btn-upload" class="sprite" title="Upload STEM Resources" href="/index.cfm?event=showResourceForm"></a>
            <cfelse>
                <a id="btn-upload" class="sprite" title="Upload STEM Resources" onclick="deGlobalWin({path:'index.cfm?event=xhr.loginForm&amp;r=index%252Ecfm%253Fevent%253DshowResourceForm%2526c%253D37', closebtn:false,background:'none'}); return false;" href="#"></a>
            </cfif>
        </p>
        
        
        <h5 style="font-size:1.25em;text-transform:uppercase;">
            Step 2: Tell us about your resources.
        </h5>
        <p>
            Once you've uploaded your resources, click on the button below to tell us what<Br /> resources you uploaded and where to send your new limited edition lab coat.
        </p>
        <p style="margin-bottom:20px;">
            <cfif StructKeyExists(cookie, "SPONSOR_USER_GUID") and IsValid("guid", cookie.SPONSOR_USER_GUID) and cookie.SPONSOR_USER_GUID neq "" and cookie.SPONSOR_USER_GUID neq "00000000-0000-0000-0000-000000000000">
                <a href="/index.cfm?event=showLabCoatForm"><img src="/images/stem-free-lab-coat.jpg" alt="Click here to receive your free lab coat." /></a>
            <cfelse>
                <a title="Upload STEM Resources" onclick="deGlobalWin({path:'index.cfm?event=xhr.loginForm&amp;r=index%252Ecfm%253Fevent%253DshowLabCoatForm', closebtn:false,background:'none'}); return false;" href="#"><img src="/images/stem-free-lab-coat.jpg" alt="Click here to receive your free lab coat." /></a>
            </cfif>
            <br />
        </p>
        
        * While supplies last.
     </div>
	
</div>
<div id="content-right"><img src="images/coat2.jpg" alt="" /></div>
