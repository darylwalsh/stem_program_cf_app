<CFPROCESSINGDIRECTIVE SUPPRESSWHITESPACE="Yes">
<cfsilent>
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset event.setArg("pageTitle","#request.siteName# - Webinar Archive") />
</cfsilent>

<script src="/js/jquery-1.7.2.min.js"></script>

<style>
span {
	display:inline-block;
	position:relative;
}
.yearcol { 
	float: left; 
	width: 285px; 
	margin: 0 22px 10px 0;
}
.colhead { 
	display:inline-block;
	width:265px;
	border-bottom:1px solid #099;
	background: #CEEEEF; 
	color: #099; 
	font-size: 14px; 
	font-weight: bold; 
	padding: 6px 8px;
    -webkit-border-radius: 8px 8px 0 0;
    border-radius: 8px 8px 0 0;
    -moz-background-clip: padding; 
	-webkit-background-clip: padding-box; 
	background-clip: padding-box;
	text-align: center;
}
.collist { 
	background-color: #F3F3F3;
	overflow: auto;	
	width: 265px; 
	list-style-type: none; 
	padding: 8px; 
	margin: 0; 
	font-size: 12px;
	min-height: 125px;
	-webkit-border-radius: 0 0 8px 8px;
    border-radius: 0 0 8px 8px;
	border-top: 2px solid #DEDFDF;
}
a.thumblink { 
	border: 1px solid #fff; 
	width: 46px; 
	height: 46px; 	
	overflow: hidden; 
	float: left;
}
a.thumblink:hover { 
	border: 1px solid #999;
}
.webinar-content-wrap { 
	float: right; 
	width: 200px; 
	padding-right: 10px; 
}
a.webinar-title { 
	color:#015787; 
	font-weight: bold; 
	display: block; 
	padding-bottom: 4px; 
}
.webinar-details { 
	font-weight: bold; 
	padding-bottom: 4px; 
}
.webinar-details,.webinar-date { 
	font-size: 11px; 
}

.nobr	{ 
	white-space:nowrap; 
}

.xlist {
	height: auto;
	width: 300px;
	white-space:normal;
	/*overflow: auto;*/
	margin: 0px 0px 0px -5px;
	vertical-align:top;
}
.yholder {
	height: auto;
	width: 930px;
}
.ybutton {
	width:50px;
	height:20px;
	line-height:20px;
	border-radius:5px;
	background-color:#cef;
	color:#099;
	font-weight: bold;
	border:1px solid #099;
	display:inline-block;
	text-align:center;
	font-size:14px;
	margin:2px;
	
}
.ybutton:hover {
	background-color:#165378;
	color:#fff;
	text-decoration:none;
}
.xactive {
	background-color:#165378;
	color:#fff;
}
</style>

<script>
var selectedYear = 2013;
jQuery.noConflict();
jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".clicker", function() {
		jQuery('.yearcol').hide();				
		jQuery('.clicker').removeClass('xactive');		
		jQuery(this).addClass('xactive');		
		jQuery('#c' + jQuery(this).attr('elem')).show();
		return false;
	});
	jQuery('.yearcol').hide();	
	jQuery('.ybutton').filter('a[elem='+selectedYear+']').addClass('xactive');
	jQuery('#c'+selectedYear).show();
});
</script>
<cfoutput>
<div id="content-single"><!--- 1 --->
	<h2>Webinars</h2>
	<h3>Archived STEM Connect Webinars</h3>
	<p>The Siemens STEM Academy website hosts monthly webinars for teachers and students. 
	All events are archived and have supporting materials that help you integrate the content into your classroom. 
	Access the webinar archive below.</p>
	<p><img title="Webinar Categories" src="img/webinar_cats.png" alt="Webinar Categories" /></p>
	
	
	<a href="" class="ybutton clicker"
		elem="2013">2013</a>
	<a href="" class="ybutton clicker"
		elem="2012">2012</a>
	<a href="" class="ybutton clicker"
		elem="2011">2011</a>
	<a href="" class="ybutton clicker"
		elem="2010">2010</a>
	
	<br><br>	
	
	<div class="yholder"><!--- 3 --->				
		
		<div class="yearcol" id="c2013">			
		
		<nobr>
		<span class="xlist">
			<span class="colhead">January 2013</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&resourceId=40877950%2D1321%2D0c71%2D3c2a%2D6ed29c1055cd"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&resourceId=40877950%2D1321%2D0c71%2D3c2a%2D6ed29c1055cd">Understanding the "E" in STEM: Applying Engineering Standards in the Modern Science Classroom</a>
			<div class="webinar-details">Presented by Lawrence Perretto<br>Siemens STARs Fellow, NASA Fellow</div>
			<div class="webinar-date">January 29, 2013</div>
			</div>
			</span>
		</span>
			
		<span class="xlist">
			<span class="colhead">February 2013</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&resourceId=cf6af9e2%2D1321%2D0c71%2D3cb0%2D02f0fb1e919b"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&resourceId=cf6af9e2%2D1321%2D0c71%2D3cb0%2D02f0fb1e919b">A Behind the Scenes Look at the redacted Channel Telescope</a>
			<div class="webinar-details">Presented by Jeffrey Hall,<br>Director, Rotunda at Lowell Observatory</div>
			<div class="webinar-date">February 26, 2013</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">March 2013</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&resourceId=281b0c2f%2D1321%2D0c71%2D3c70%2D6da77c4333b7"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&resourceId=281b0c2f%2D1321%2D0c71%2D3c70%2D6da77c4333b7">Dynamic Knowledge: Using Wolfram|Alpha in the Classroom</a>
			<div class="webinar-details">Presented by Crystal Fantry<br>
			Senior Educational Outreach Specialist, Wolfram|Alpha</div>
			<div class="webinar-date">March 19, 2013</div>
			</div>
			</span>
		</span>		
		</nobr>
		
		<br><br>
		
		<nobr>		
		<span class="xlist">
			<span class="colhead">April 2013</span><br>
			<span class="collist"> <a class="thumblink" href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=d0377355%2D1438%2Dbbfa%2D6006%2D804bb20a5afa"><img width="46" height="46" alt="webinar" src="/img/legend_development.png"></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=d0377355%2D1438%2Dbbfa%2D6006%2D804bb20a5afa">Cache In, Trash Out for Earth Day Week: Geocaching 101</a>
			<div class="webinar-details">Presented by Conni Mulligan,Instructional Technology Teacher</div>
			<div class="webinar-date">Tuesday April 23, 2013</div>
			</div>
			</span>
			
			<span class="collist"> <a class="thumblink" href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=d0377355%2D1438%2Dbbfa%2D6006%2D804bb20a5afa"><img width="46" height="46" alt="webinar" src="/img/legend_engineering.png"></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=d0377355%2D1438%2Dbbfa%2D6006%2D804bb20a5afa">How Podcasting Works</a>
			<div class="webinar-details">Presented by Josh Clark and Chuck Bryant, Co-hosts of <em>"Stuff You Should Know"</em></div>
			<div class="webinar-date">Thursday April 25, 2013</div>
			</div>
			</span>
			
		</span>
		
		<span class="xlist">
			<span class="colhead">May 2013</span><br>	
			<span class="collist"> <a class="thumblink" href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=c8122abd%2Dd161%2Dbe6c%2D883c%2D4a414ac3ffdc"><img width="46" height="46" alt="webinar" src="/img/legend_science.png"></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="http://siemensstemacademy.com/index.cfm?event=showResource&resourceId=c8122abd%2Dd161%2Dbe6c%2D883c%2D4a414ac3ffdc">NORTH AMERICA: Explore Glacier National Park</a>
			<div class="webinar-details">Presented by Glacier National Park & redacted Education</div>
			<div class="webinar-date">Thursday May 30, 2013</div>
			</div>
			</span>
			
		</span>
		
		<span class="xlist">
			<span class="colhead">June 2013</span><br>	
			
		</span>
		</nobr>
		
		<br><br>
		
		<nobr>
		<span class="xlist">
			<span class="colhead">July 2013</span><br>		
			
		</span>
		
		<span class="xlist">
			<span class="colhead">August 2013</span><br>
			
		</span>
		
		<span class="xlist">
			<span class="colhead">September 2013</span><br>
			
		</span>
		</nobr>
		
		<br><br>
		
		<nobr>
		<span class="xlist">
			<span class="colhead">October 2013</span><br>
			
		</span>
		
		<span class="xlist">
			<span class="colhead">November 2013</span><br>
			
		</span>
		
		<span class="xlist">
			<span class="colhead">December 2013</span><br>
			
		</span>
		</nobr>
		
		</div>
		
		<!--- =============================================== --->	
		
		<div class="yearcol" id="c2012">			
		
		<nobr>
		<span class="xlist">
			<span class="colhead">January 2012</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=c4fd755c-1438-bbfa-6093-5b766fb4994a"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=c4fd755c-1438-bbfa-6093-5b766fb4994a">How to Apply to the 2012 Siemens Summer of Learning Professional Development Programs</a>
			<div class="webinar-details">Presented by Jannita Demian</div>
			<div class="webinar-date">January 18, 2012</div>
			</div>
			</span>
		</span>
			
		<span class="xlist">
			<span class="colhead">February 2012</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=f6fb9b22-1438-bbfa-6071-578af5531f85"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=f6fb9b22-1438-bbfa-6071-578af5531f85">The Science and Technology behind Storm Chasing!</a>
			<div class="webinar-details">Presented by redacted Channel&rsquo;s Reed Timmer</div>
			<div class="webinar-date">February 13, 2012</div>
			</div>
			</span>
			
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=c8adaf70-1438-bbfa-60d8-9b9ca9349467"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=c8adaf70-1438-bbfa-60d8-9b9ca9349467">Making Mobile Media Meaningful in (and Beyond!) Your STEM Classroom</a>
			<div class="webinar-details">Presented by Hall Davidson</div>
			<div class="webinar-date">February 1, 2012</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">March 2012</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=10a7c7a8-1321-0c71-3c19-8e63a69bbfbe"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=10a7c7a8-1321-0c71-3c19-8e63a69bbfbe">Looking at Atoms through the World&rsquo;s Biggest Microscope</a>
			<div class="webinar-details">Presented by Dr. Ian Anderson of Oak Ridge National Laboratory</div>
			<div class="webinar-date">March 21, 2012</div>
			</div>
			</span>
				
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=f70e2980-1438-bbfa-6047-7b09d149367f"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=f70e2980-1438-bbfa-6047-7b09d149367f">Flipping Your Classroom with FIZZ</a>
			<div class="webinar-details">Presented by Dr. Lodge McCammon</div>
			<div class="webinar-date">March 1, 2012</div>
			</div>
			</span>
		</span>		
		</nobr>
		
		<br><br>
		
		<nobr>		
		<span class="xlist">
			<span class="colhead">April 2012</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=758eed10-1438-bbfa-6025-3cbe51e5276a"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=758eed10-1438-bbfa-6025-3cbe51e5276a">Educator Webinar with NASA&rsquo;s Leland Melvin</a>
			<div class="webinar-details">Educator Webinar with NASA&rsquo;s Leland Melvin</div>
			<div class="webinar-date">April 24, 2012</div>
			</div>
			</span>
		
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=edcbfc4e-1438-bbfa-6024-cecf51636fce"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=edcbfc4e-1438-bbfa-6024-cecf51636fce">Earth Day: What the Ice and Penguins Tell Us About Our Changing Planet</a>
			<div class="webinar-date">April 12, 2012</div>
			</div>
			</span>		
		</span>
		
		<span class="xlist">
			<span class="colhead">May 2012</span><br>	
		<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=f57c10ed%2D1321%2D0c71%2D3cd8%2Dd07fd4574559"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
		<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=f57c10ed%2D1321%2D0c71%2D3cd8%2Dd07fd4574559">Changing the Equation in K-12 STEM Learning</a>
			<div class="webinar-details">Presented by Linda Rosen, CEO, Changing the Equation</div>
			<div class="webinar-date">May 22, 2012</div>
		</div>
		</span>
		</span>
		<span class="xlist">
			<span class="colhead">July 2012</span><br>		
		<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=e1cc07de%2D1321%2D0c71%2D3c0e%2D68b0629b251c"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
		<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=e1cc07de%2D1321%2D0c71%2D3c0e%2D68b0629b251c"> 'Round the Horn with STEM-alicous Resources Webinar </a>
			<div class="webinar-details">Presented by Kyle Schutt, redacted Educator Network</div>
			<div class="webinar-date">July 17, 2012</div>
		</div>
		</span>
		</span>
        </nobr>
		
		<br>
		<nobr>
        <span class="xlist">
			<span class="colhead">September 2012</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=53a00e5e%2D1321%2D0c71%2D3c70%2D99ea1f4ea75c"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=53a00e5e%2D1321%2D0c71%2D3c70%2D99ea1f4ea75c"> The Science and Technology behind Green Architecture </a>
			<div class="webinar-details">Presented by Danny Forster</div>
			<div class="webinar-date">September 18, 2012</div>
			</div>
			</span>
		</span>
	
		<span class="xlist">
			<span class="colhead">October 2012</span><br>
		<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=72d50c16%2D1438%2Dbbfa%2D6016%2D7cde5262a94e"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
		<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=72d50c16%2D1438%2Dbbfa%2D6016%2D7cde5262a94e">Challenge-Based Learning 101</a>
			<div class="webinar-details">Presented by Cynthia Treichler</div>
			<div class="webinar-date">October 16, 2012</div>
		</div>
		</span>
		</span><span class="xlist">
			<span class="colhead">December 2012</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=45ba104a%2D1321%2D0c71%2D3c2e%2D06fb2e015230"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=45ba104a%2D1321%2D0c71%2D3c2e%2D06fb2e015230">Practical Steps for Building the STEM Pipeline in Your School</a>
			<div class="webinar-details">Presented by Dr. Cindy Moss</div>
			<div class="webinar-date">December 10, 2012</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=d203efae%2D1438%2Dbbfa%2D60fb%2D2cd11b32b893"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=d203efae%2D1438%2Dbbfa%2D60fb%2D2cd11b32b893">All About Amphibians</a>
			<div class="webinar-details">Presented by the National Zoo</div>
			<div class="webinar-date">December 4, 2012</div>
			</div>
			</span>
		</span>
		</nobr>
		
		</div>
		
		<!--- =============================================== --->		
		
		<div class="yearcol" id="c2011">
		
				<nobr>
		<span class="xlist">
			<span class="colhead">January 2011</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=d175093a-1438-bbfa-6092-4cb966e74964"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=d175093a-1438-bbfa-6092-4cb966e74964">Get Started: How to Apply to STARs and Siemens STEM Institute Professional Development Programs</a>
			<div class="webinar-date">January 19, 2011</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=f5bd2bdd-1321-0c71-3cb7-eaae15d49715"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=f5bd2bdd-1321-0c71-3cb7-eaae15d49715">A Look at the World of Parasites with Animal Planet&rsquo;s Dan Riskin</a>
			<div class="webinar-details">Animal Planet's Dan Riskin</div>
			<div class="webinar-date">January 11, 2011</div>
			</div>
			</span>
		</span>			
		
		<span class="xlist">
			<span class="colhead">February 2011</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=800a1ca4-1321-0c71-3c3a-272ff0055da7"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=800a1ca4-1321-0c71-3c3a-272ff0055da7">Top Ten STEM Resources of 2011</a>
			<div class="webinar-details">Presented by Patti Duncan</div>
			<div class="webinar-date">February 23, 2011</div>
			</div>
			</span>
		</span>
		
		
			
		<span class="xlist">
			<span class="colhead">April 2011</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=92df5707-1438-bbfa-602f-53fe705d6390"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=92df5707-1438-bbfa-602f-53fe705d6390">Earth Day: The Impact of the Gulf Oil Spill One Year Later with Jeff Corwin</a>
			<div class="webinar-details">Presented by Jeff Corwin</div>
			<div class="webinar-date">April 19, 2011</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=a9bbea1d-1321-0c71-3c03-19056281e358"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=a9bbea1d-1321-0c71-3c03-19056281e358">Getting them There: Recruitment and Retention of Girls in STEM</a>
			<div class="webinar-date">April 14, 2011</div>
			</div>
			</span>
		</span>
		</nobr>
		
		<br><br>
		
		<nobr>	
		<span class="xlist">
			<span class="colhead">May 2011</span><br>	
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=a1462659-1321-0c71-3c0d-44946a0900c4"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=a1462659-1321-0c71-3c0d-44946a0900c4">Closing the STE&hellip;M Gap</a>
			<div class="webinar-details">Presented by Peggy Hartwig, a 2010 Siemens STEM Institute Fellow</div>
			<div class="webinar-date">May 4, 2011</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">June 2011</span><br>	
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=579944e3-1321-0c71-3c35-396c1aab21e5"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=579944e3-1321-0c71-3c35-396c1aab21e5">Innovative STEM Projects for Your Classroom - Featuring 2010 Institute Fellows</a>
			<div class="webinar-details">Presented by 2010 Siemens STEM Institute Fellows</div>
			<div class="webinar-date">June 21, 2011</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=b7502243-1438-bbfa-602e-14dd314455e8"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=b7502243-1438-bbfa-602e-14dd314455e8">Adventure to the Arctic Circle, the Earth's Air Conditioner</a>
			<div class="webinar-details">Presented by Philippe Cousteau and Dr. Eduard Sarukhanian</div>
			<div class="webinar-date">June 2, 2011</div>
			</div>
			</span>
		</span>
	
		<span class="xlist">
			<span class="colhead">July 2011</span><br>		
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=b33c0eb1-1321-0c71-3c51-4b9012d4460b"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=b33c0eb1-1321-0c71-3c51-4b9012d4460b">More Innovative STEM Projects</a>
			<div class="webinar-details">Presented by 2010 Siemens STEM Institute Fellows</div>
			<div class="webinar-date">July 25, 2011</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;c=37&amp;resourceId=b9a15a23-1321-0c71-3ce3-c266a67c68d0"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;c=37&amp;resourceId=b9a15a23-1321-0c71-3ce3-c266a67c68d0">Communicate, Collaborate and Create: Building Dynamic Classroom Projects</a>
			<div class="webinar-details">Presented by Jannita Demian</div>
			<div class="webinar-date">July 20, 2011</div>
			</div>
			</span>
		</span>
			</nobr>
		
		<br><br>
		
		<nobr>
		<span class="xlist">
			<span class="colhead">September 2011</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=3f43446a-1438-bbfa-6000-66a5cd31569c"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=3f43446a-1438-bbfa-6000-66a5cd31569c">A Scientific Approach to Science and Engineering Education</a>
			<div class="webinar-details">Presented by Dr. Carl Wieman, Associate Director for Science at the White House Office of Science and Technology Policy</div>
			<div class="webinar-date">September 20, 2011</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=e29d1a15-1438-bbfa-609b-46d45ac87adc"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=e29d1a15-1438-bbfa-609b-46d45ac87adc">Sharks: Myth vs. Reality</a>
			<div class="webinar-details">Presented by Andy Dehart, redacted Channel Shark Advisor</div>
			<div class="webinar-date">September 16, 2011</div>
			</div>
			</span>
		</span>
	
		<span class="xlist">
			<span class="colhead">October 2011</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=a77b5480-1321-0c71-3c28-20a054dabe0a"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=a77b5480-1321-0c71-3c28-20a054dabe0a">STEMulate Student Minds with Creative Web 2.0 Sites</a>
			<div class="webinar-details">Presented by Brad Fountain</div>
			<div class="webinar-date">October 19, 2011</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=93eba3f9-1321-0c71-3ca2-1c36a53ab7d3"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=93eba3f9-1321-0c71-3ca2-1c36a53ab7d3">Making A Difference Through Conservation</a>
			<div class="webinar-details">Presented by Animal Planet&rsquo;s Dave Salmoni</div>
			<div class="webinar-date">October 19, 2011</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">November 2011</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=3c0f04b5-1438-bbfa-607f-16bad33959c8"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=3c0f04b5-1438-bbfa-607f-16bad33959c8">iThink iNeed iPads in the Classroom</a>
			<div class="webinar-details">Presented by Steve Dembo</div>
			<div class="webinar-date">November 30, 2011</div>
			</div>
			</span>
		</span>
			</nobr>
		
		<br><br>
		
		<nobr>
		<span class="xlist">
			<span class="colhead">December 2011</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=7ffea464-1321-0c71-3cb8-fe6427a8e4cf"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=7ffea464-1321-0c71-3cb8-fe6427a8e4cf">How to Incorporate Sustainability into Your Schools and Communities</a>
			<div class="webinar-details">Presented by Juan Sostheim, Founder of Rancho Margot, Costa Rica</div>
			<div class="webinar-date">December 6, 2011</div>
			</div>
			</span>
		</span>
		</nobr>	
			
		</div>
		
		<!--- =============================================== --->			
		
		<div class="yearcol" id="c2010">
		
		<nobr>
		<span class="xlist">
			<span class="colhead">January 2010</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=e6b5a8ea-1321-0c71-3c37-b7215fe415d4"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=e6b5a8ea-1321-0c71-3c37-b7215fe415d4">Top Ten STEM Resources Webinar</a>
			<div class="webinar-details">Presented by Patti Duncan</div>
			</div>
			</span>
		</span>			
		
		<span class="xlist">
			<span class="colhead">February 2010</span><br>			
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;c=37&amp;resourceId=0586cf60-237d-0718-f456-e9afd12f6da4"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;c=37&amp;resourceId=0586cf60-237d-0718-f456-e9afd12f6da4">STEM Connect with Michio Kaku</a>
			<div class="webinar-details">Presented by Dr. Michio Kaku</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">March 2010</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=bec50e4c-1438-bbfa-60fb-eebd5e61fdca"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=bec50e4c-1438-bbfa-60fb-eebd5e61fdca">How Math Can Solve Everyday Problems</a>
			<div class="webinar-details">Presented by STEM Academy</div>
			<div class="webinar-date">March 9, 2010</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=1fbec959-1321-0c71-3c58-4dce6f7f7854"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=1fbec959-1321-0c71-3c58-4dce6f7f7854">Using Technology To Create New Knowledge</a>
			<div class="webinar-details">Presented by Hall Davidson</div>
			<div class="webinar-date">March 8, 2010</div>
			</div>
			</span>
		</span>		
		</nobr>
		
		<br><br>
		
		<nobr>		
		<span class="xlist">
			<span class="colhead">April 2010</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=ab1ef416-1321-0c71-3c9e-35cda75bc34d"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=ab1ef416-1321-0c71-3c9e-35cda75bc34d">Earth Day: Celebrating Our Past and Future Efforts to Conserve Our Oceans</a>
			<div class="webinar-details">Presented by Philippe Cousteau</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=1fc04e32-1321-0c71-3c11-417638329c95"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=1fc04e32-1321-0c71-3c11-417638329c95">Getting Started with Project-Based Learning</a>
			<div class="webinar-details">Presented by Jennifer Dorman</div>
			<div class="webinar-date">April 8, 2010</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">May 2010</span><br>	
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=46341d43-1438-bbfa-60bd-372566172a5f"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=46341d43-1438-bbfa-60bd-372566172a5f">The Science of Future Food</a>
			<div class="webinar-details">Presented by Homaro Cantu and Ben Roche</div>
			<div class="webinar-date">May 13, 2010</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=0c5b4653-1321-0c71-3c4b-04e85586f860"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=0c5b4653-1321-0c71-3c4b-04e85586f860">Layers of Learning with Google Earth</a>
			<div class="webinar-details">Presented by Brad Fountain</div>
			<div class="webinar-date">May 4, 2010</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">June 2010</span><br>	
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=2601d5ff-1438-bbfa-6032-46f677809950"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=2601d5ff-1438-bbfa-6032-46f677809950">Authentic Intellectual Work</a>
			<div class="webinar-details">Presented by Mike Bryant</div>
			<div class="webinar-date">June 17, 2010</div>
			</div>
			</span>
		</span>
		</nobr>
		
		<br><br>
		
		<nobr>
		<span class="xlist">
			<span class="colhead">July 2010</span><br>		
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=2609bab1-1438-bbfa-60f2-56f0ebda8ee9"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=2609bab1-1438-bbfa-60f2-56f0ebda8ee9">Start Thinking in 3D: Google SketchUp</a>
			<div class="webinar-details">Brent Jacobsen, Master of Landscape Architecture</div>
			<div class="webinar-date">July 13, 2010</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">August 2010</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=26527f08-1438-bbfa-605f-705c7f171230"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=26527f08-1438-bbfa-605f-705c7f171230">Project-ing Your Class Globally</a>
			<div class="webinar-date">August 17, 2010</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=b4675a76-1321-0c71-3cd1-6f3ebdfbd8d9"><img src="/img/legend_science.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=b4675a76-1321-0c71-3cd1-6f3ebdfbd8d9">Create Impact and Win Big with Science</a>
			<div class="webinar-details">Presented by Siemens Foundation, redacted Education</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">September 2010</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=80645e90-1438-bbfa-6049-367d3719ed20"><img src="/img/legend_development.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=80645e90-1438-bbfa-6049-367d3719ed20">Get Ready for 3D Learning: More with Google SketchUp</a>
			<div class="webinar-details">Presented by Brent Jacobsen</div>
			<div class="webinar-date">September 28, 2010</div>
			</div>
			</span>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=91493d6f-1438-bbfa-60be-48d65f9ec16d"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=91493d6f-1438-bbfa-60be-48d65f9ec16d">A New Way to Look at Architecture</a>
			<div class="webinar-details">Presented by Danny Forster</div>
			<div class="webinar-date">September 13, 2010</div>
			</div>
			</span>
		</span>
		</nobr>
		
		<br><br>
		
		<nobr>
		<span class="xlist">
			<span class="colhead">October 2010</span><br>
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=8053b168-1438-bbfa-60a3-4a737bb2cebc"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=8053b168-1438-bbfa-60a3-4a737bb2cebc">Exploding Stars and Atom Smashers: Solving the Mysteries of the Sky with Science, Math and Technology</a>
			<div class="webinar-date">October 27, 2010</div>
			</div>
			</span>
		</span>
		
		<span class="xlist">
			<span class="colhead">November 2010</span><br>			
			<span class="collist"> <a class="thumblink" href="/index.cfm?event=showResource&amp;resourceId=e3e69cd8-1321-0c71-3cd7-d3d8ae5437ab"><img src="/img/legend_engineering.png" alt="webinar" width="46" height="46" /></a>
			<div class="webinar-content-wrap"><a class="webinar-title" href="/index.cfm?event=showResource&amp;resourceId=e3e69cd8-1321-0c71-3cd7-d3d8ae5437ab">Learn, Create, and Innovate with Scratch!</a>
			<div class="webinar-date">November 30, 2010</div>
			</div>
			</span>
		</span>
		
	</nobr>
		
		</div>
		
	</div><!--- 3 --->
	
</div><!--- 1 --->
<br><br>
</cfoutput>
</CFPROCESSINGDIRECTIVE>