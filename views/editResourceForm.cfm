<cfsilent>
	<cfset resource = event.getArg("resource") />
	<cfset files = resource.getFiles() />
	<cfset categories = event.getArg("categories") />
	<cfset types = event.getArg("types") />
	<cfset grades = event.getArg("grades") />
	<cfset resourceCategories = resource.getCategories() />
	<cfset resourceTypes = resource.getTypes() />
	<cfset resourceGrades = resource.getGrades() />
	<cfset c = event.getArg("c") />
	<cfset event.setArg("pageTitle","#request.siteName# - #resource.getTitle()#") />
	<cfset app_user_guid = "00000000-0000-0000-0000-000000000000" />
	<cfif StructKeyExists(COOKIE,"SPONSOR_USER_GUID")>
		<cfset app_user_guid = COOKIE.SPONSOR_USER_GUID />
	</cfif>
	<cfset currentCategoryIDs = "" />
	<cfloop index="i" from="1" to="#arrayLen(resourceCategories)#" step="1">
		<cfset category = resourceCategories[i] />
		<cfset currentCategoryIDs = listAppend(currentCategoryIDs, category.getCategoryId(), ",") />
	</cfloop>
	<cfset currentTypeIDs = "" />
	<cfloop index="i" from="1" to="#arrayLen(resourceTypes)#" step="1">
		<cfset type = resourceTypes[i] />
		<cfset currentTypeIDs = listAppend(currentTypeIDs, type.getTypeId(), ",") />
	</cfloop>
	<cfset currentGradeIDs = "" />
	<cfloop index="i" from="1" to="#arrayLen(resourceGrades)#" step="1">
		<cfset grade = resourceGrades[i] />
		<cfset currentGradeIDs = listAppend(currentGradeIDs, grade.getGrade_id(), ",") />
	</cfloop>
	<cfsavecontent variable="js">
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	plugins : "safari,advlink,inlinepopups,style",
	
	// Theme Options
	theme_advanced_buttons1 : "link,unlink,|,bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,forecolor,fontselect,fontsizeselect,|,bullist,numlist,|,outdent,indent",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : ""
	
	//content_css : "/css/stemacademy.css"
});
</script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
<div id="content-left">

<div id="edit-form">

				<h2>STEM Resources</h2>
			<form name="editResourceForm" id="editResourceForm" action="#BuildUrl('processEditResourceForm','c=#c#')#" method="post">
				<input type="hidden" name="type" value="#resource.getType()#" />
				<input type="hidden" name="siteId" value="#resource.getSiteId()#" />
				<input type="hidden" name="resourceId" value="#resource.getResourceId()#" />
				<input type="hidden" name="dtCreated" value="#resource.getDtCreated()#" />
				<input type="hidden" name="ipCreated" value="#resource.getIpCreated()#" />
				<input type="hidden" name="createdBy" value="#resource.getCreatedBy()#" />
				<input type="hidden" name="createdByName" value="#resource.getCreatedByName()#" />
				<input type="hidden" name="views" value="#resource.getViews()#" />
				<input type="hidden" name="dtUpdated" value="#CreateODBCDateTime(Now())#" />
				<input type="hidden" name="ipUpdated" value="#CGI.REMOTE_ADDR#" />
				<input type="hidden" name="updatedBy" value="#app_user_guid#" />
				<h3 id="res-title"><input type="text" style="width:600px;" name="title" value="#resource.getTitle()#" /></h3>
				<br clear="all" />				
				<textarea name="body" style="width:600px;height:400px;">#resource.getBody()#</textarea>
				
				<br />
				<br />
				
				<div class="last">
					<strong class="field_title" style="vertical-align:middle;">Keywords:</strong> <input type="text" name="keywords" value="#resource.getKeywords()#" /><br />
					<strong class="field_title">Subjects:</strong><br />
					<cfloop query="categories">
						<input type="checkbox" name="categoryIDs" value="#categoryId#" <cfif listFindNoCase(currentCategoryIDs, categories.categoryId, ",") NEQ 0>checked="true"</cfif>/> <span style="display:inline-block;margin-top:-10px;vertical-align:middle;">#name#</span><br />
					</cfloop>
					<strong class="field_title">Types:</strong><br />
					<cfloop query="types">
						<input type="checkbox" name="typeIDs" value="#typeId#" <cfif listFindNoCase(currentTypeIDs, types.typeId, ",") NEQ 0>checked="true"</cfif>
						/> <span style="display:inline-block;margin-top:-10px;vertical-align:middle;">#name#</span><br />
					</cfloop>
					<strong class="field_title">Grade Level:</strong>
					<br />
											<select name="gradeIDs" multiple="multiple" id="gradeIDs" style="width:190px;" size="#arrayLen(grades)#" class="left">
											<cfloop index="i" from="1" to="#arrayLen(grades)#">
												<option value="#grades[i].getGrade_id()#" <cfif ListFindNoCase(currentGradeIDs,grades[i].getGrade_id(),",") NEQ 0>selected="selected"</cfif>>#grades[i].getDescription()#</option>
											</cfloop>	
											</select>
											
											<p class="left">Hold CTRL and click to select multiple grades.</p>
						
						
					
					<br clear="all" />
					<br />	
					<input type="submit" value="" id="btn-save-resource" class="sprite" />
				</div>
				<br />
			</form>
				<br clear="all" />
				
</div><!---editform--->	
				
				
</div><!---content-left--->
<div id="content-right">  		
  				<br clear="all" />
  				<h4 class="boxtop">Teacher Resources</h4>
				<div id="resource-roll">
				<p><a href="#BuildUrl('showResourceUpload','c=#c#|resourceId=#resource.getResourceId()#')#" id="btn-addfiles" class="sprite">Add Files</a></p>
			<cfloop index="i" from="1" to="#ArrayLen(files)#">
				<img src="/assets/stem/thumbnails/#files[i].getThumbnail()#" alt="#files[i].getFilename()#" class="thumb" width="100" />
				<p><a title="#files[i].getFilename()#" href="##" class="file-link">#application.udf.maxLength(files[i].getFileName(),22)#</a><br />
				File Size:  #application.udf.FileSize(files[i].getFileSize())#<br />
				<a title="Download" href="#BuildUrl('getFile','f=#files[i].getFile_id()#')#">Download</a><br />
				<a title="Delete" href="#BuildUrl('deleteFile','c=#c#|f=#files[i].getFile_id()#|resourceId=#resource.getResourceId()#')#" onClick="return confirm('Are you sure you want to delete this file?');">DELETE</a></p>
			</cfloop>
				<br clear="all" />
				</div><!---resource-roll--->  
		<br />
</div><!---content-right--->
</cfoutput>