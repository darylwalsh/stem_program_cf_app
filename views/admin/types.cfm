<cfsilent>
	<cfset event.setArg("pageTitle", "#request.siteName# admin - Types") />
	<cfset types = event.getArg("types") />
	<cfset newId = application.udf.CreateGUID() />
</cfsilent>
<cfoutput>
	<form name="createType" action="#BuildUrl('admin.processTypeForm')#" method="post">
		<input type="hidden" name="dtCreated" value="#CreateODBCDateTime(Now())#" />
		<input type="hidden" name="createdBy" value="#cookie.app_user_guid#" />
		<input type="hidden" name="ipCreated" value="#cgi.REMOTE_ADDR#" />
		<input type="hidden" name="typeId" value="#newId#" />
		<input type="hidden" name="siteId" value="#request.siteId#" />
		<input type="hidden" name="parentId" value="" />
		<input type="hidden" name="isActive" value="1" />
		Name: <input type="text" name="name" value="" /><br />
		orderNo: <input type="text" name="orderNo" value="" /><br />
		<input type="submit" name="create" value="Create" />
	</form>
	<table border="1" width="100%">
		<tr>
			<th>Order</th>
			<th>Type</th>
			<th>Active</th>
			<th>Actions</th>
		</tr>
		<cfloop index="i" from="1" to="#arrayLen(types)#">
		<tr>
			<td>#types[i].getOrderNo()#</td>
			<td>#types[i].getName()#</td>
			<td>#types[i].getIsActive()#</td>
			<td>Edit | <a href="#BuildUrl('admin.deleteType','id=#types[i].getTypeId()#')#">Delete</a></td>
		</tr>
		</cfloop>
	</table>
</cfoutput>