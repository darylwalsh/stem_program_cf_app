<cfoutput>
 	<!--Begin Menu-->
	<div style="background:url(img/leftNav_cap_blank.jpg);">
		<div style="padding:5px;"><a href="#BuildUrl('admin.showHome')#">CMS Admin</a></div>	
	</div>
	<div id="menus">
		<dl id="menu">
			<dt class="xmenu" id="a000"><a id="la000" href="#BuildUrl('admin.showSite','siteId=#request.siteId#')#">Site Manager</a></dt>
			<dt class="xmenu" id="a001"><a id="la001" href="#BuildUrl('admin.editWebsite','siteId=#request.siteId#')#">Site Members</a></dt>
			<dt class="xmenu" id="a201"><a id="la201" href="#BuildUrl('admin.showImages')#">File Manager</a></dt>
			<dt class="xmenu" id="a202"><a id="la202" href="#BuildUrl('admin.showCategories')#">Categories</a></dt>
			<dt class="xmenu" id="a100"><a id="la100" href="#BuildUrl('admin.showCreateAsset')#">Create Asset</a></dt>
			<dt class="xmenu" id="a200"><a id="la200" href="#BuildUrl('admin.showAllAssets')#">View All Assets</a></dt>
			<dt class="xmenu" id="a300"><a id="la300" href="#BuildUrl('admin.refreshSearchIndex')#">Update Search</a></dt>
			<dt class="xmenu" id="a400"><a id="la400" href="#BuildUrl('admin.showReports')#">Reports</a></dt>
			<dt class="xmenu" id="a500"><a id="la500" href="#BuildUrl('admin.showSearchReports')#">Search Reports</a></dt>
			<dt class="xmenu" id="a600"><a id="la600" href="#BuildUrl('admin.showRatingReports')#">Rating Reports</a></dt>
		</dl>
	</div>
	<img src="img/leftNav_cap_btm.jpg" />
</cfoutput>