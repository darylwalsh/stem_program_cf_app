<cfsilent>
	<cfset event.setArg("pageTitle","#request.siteName# Admin - Rating Detail") />
	<cfset resourceId = event.getArg("resourceId") />
	<cfquery name="contentRatings" datasource="#getProperty('sDsn')#">
		SELECT
			cr.*,
			au.uid,
			au.app_user_guid
		FROM cms_contentRatings cr
		LEFT JOIN EDU_SEC.dbo.app_user au (NOLOCK) ON au.app_user_guid = cr.app_user_guid
		WHERE	0=0
		AND		cr.contentId = <cfqueryparam value="#resourceId#" />
		ORDER BY cr.dtCreated DESC
	</cfquery>
</cfsilent>
<cfoutput>
<a href="#BuildUrl('admin.showRatingReports')#">&lt;--Back</a>
<br />
<table border="1" width="100%">
	<tr>
		<th>Usernmae</th>
		<th>Rating</th>
		<th>Date</th>
	</tr>
	<cfloop query="contentRatings">
	<tr>
		<td><cfif uid EQ "">anonymous<cfelse>#contentRatings.uid#</cfif></td>
		<td>#contentRatings.rate#</td>
		<td>#DateFormat(contentRatings.dtCreated,"short")#</td>
	</tr>
	</cfloop>
</table>
</cfoutput>