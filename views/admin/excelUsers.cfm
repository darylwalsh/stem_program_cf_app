<cfsilent>
	<cfset users = event.getArg("users") />
</cfsilent>
<cfsavecontent variable="report">
<cfoutput>
        <table border="1">
                <thead align="center">
                        <th>ADDRESS1</th>
                        <th>ADDRESS2</th>
                        <th>CITY</th>
                        <th>EMAIL</th>
                        <th>FIRST_NAME</th>
                        <th>LAST_NAME</th>
						<th>PHONE</th>
						<th>PID</th>
						<th>SADDRESS1</th>
						<th>SADDRESS2</th>
						<th>SCITY</th>
						<th>SNAME</th>
						<th>SPHONE</th>
						<th>SZIPCODE</th>
						<th>TITLE</th>
						<th>UID</th>
                </thead>
				<cfloop from="1" to="#ArrayLen(users)#" index="i">
                <tr align="left">
                        <td>#users[i].getAddress1()#</td>
                        <td>#users[i].getAddress2()#</td>
                        <td>#users[i].getCity()#</td>
                        <td>#users[i].getEmail()#</td>
                        <td>#users[i].getFirst_name()#</td>
                        <td>#users[i].getLast_name()#</td>
						<td>#users[i].getPhone()#</td>
						<td>#users[i].getPid()#</td>
						<td>#users[i].getSaddress1()#</td>
						<td>#users[i].getSaddress2()#</td>
						<td>#users[i].getScity()#</td>
						<td>#users[i].getSname()#</td>
						<td>#users[i].getSphone()#</td>
						<td>#users[i].getSzipcode()#</td>
						<td>#users[i].getTitle()#</td>
						<td>#users[i].getUid()#</td>
                </tr>
                </cfloop>
        </table>
</cfoutput>
</cfsavecontent>
<cfheader name="content-disposition" value="attachment; filename=Users.xls">
<cfcontent type="application/vnd.ms-excel" reset="true">
<cfoutput>#report#</cfoutput>