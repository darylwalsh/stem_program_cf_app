<cfsilent>
	<cfset event.setArg("pageTitle", "#request.siteName# - Site Manager") />
	<cfset siteId = event.getArg("siteId") />
	<cfset tcontents = event.getArg("tcontents") />
	<cfset pages = event.getArg("pages") />
	<cfset newId = application.udf.CreateGUID() />
</cfsilent>
	<cfif tcontents.RecordCount GT 0>
	<h3>Navigation</h3>
		<table border="1" width="100%">
			<tr>
				<th>Add Page</th>
				<th>Name</th>
				<th>Order</th>
				<th>Active</th>
				<th>Actions</th>
			</tr>
		<cfoutput query="tContents" group="lev1contentId">
			<tr>
				<td><a href="#BuildUrl('admin.showCreateTcontent','parentId=#lev1contentId#|siteId=#siteId#')#">Add</a></td>
				<td>#lev1title#</td>
				<td>#lev1orderNo#</td>
				<td>#lev1isActive#</td>
				<td><a href="#BuildUrl('admin.showEditTcontent','id=#lev1id#')#">Edit</a> | <a href="#BuildUrl('showContent','id=#lev1id#')#" target="_blank">Preview</a><cfif lev2id EQ ""><a href="#BuildUrl('admin.deleteTcontent','id=#lev1id#|siteId=#lev1siteId#')#" onClick="return confirm('Are you sure you want to delete this page?');"> | Delete</a></cfif></td>
			</tr>
		<cfif lev2contentId NEQ "">
			<cfoutput group="lev2contentId">
			<tr>
				<td><a href="#BuildUrl('admin.showCreateTcontent','parentId=#lev2contentId#|siteId=#siteId#')#">Add</a></td>
				<td>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;#lev2title#</td>
				<td>#lev2orderNo#</td>
				<td>#lev2isActive#</td>
				<td><a href="#BuildUrl('admin.showEditTcontent','id=#lev2id#')#">Edit</a> | <a href="#BuildUrl('showContent','id=#lev2id#')#" target="_blank">Preview</a><cfif lev3id EQ ""> | <a href="#BuildUrl('admin.deleteTcontent','id=#lev2id#|siteId=#lev2siteId#')#" onClick="return confirm('Are you sure you want to delete this page?');">Delete</a></cfif></td>
			</tr>
		<cfif lev3contentId NEQ "">
			<cfoutput>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;&nbsp;&nbsp;&raquo;&nbsp;&nbsp;#lev3title#</td>
				<td>#lev3orderNo#</td>
				<td>#lev3isActive#</td>
				<td><a href="#BuildUrl('admin.showEditTcontent','id=#lev3id#')#">Edit</a> | <a href="#BuildUrl('showContent','id=#lev3id#')#" target="_blank">Preview</a> | <a href="#BuildUrl('admin.deleteTcontent','id=#lev3id#|siteId=#lev3siteId#')#" onClick="return confirm('Are you sure you want to delete this page?');">Delete</a></td>
			</tr>
			</cfoutput>
		</cfif>
			</cfoutput>
		</cfif>
		</cfoutput>
		</table>
	<cfelseif tcontents.RecordCount EQ 0>
	<cfoutput>
	<div>
		Add Page:
		<form action="#BuildUrl('admin.createTcontent')#" method="post">
			<input type="hidden" name="siteId" value="#request.siteId#" />
			<input type="hidden" name="parentId" value="#request.siteId#" />
			<input type="hidden" name="contentId" value="#newId#" />
			<input type="hidden" name="template" value="page.cfm" />
			<input type="hidden" name="type" value="page" />
			<input type="hidden" name="isActive" value="1" />
			<input type="hidden" name="orderNo" value="0" />
			<input type="hidden" name="title" value="Home Page" />
			<input type="hidden" name="menuTitle" value="Home" />
			<input type="hidden" name="dtCreated" value="#CreateODBCDateTime(Now())#" />
			<input type="hidden" name="createdBy" value="#cookie.app_user_guid#" />
			<input type="hidden" name="ipCreated" value="#cgi.REMOTE_ADDR#" />
			<input type="hidden" name="approved" value="1" />
			<input type="hidden" name="isNav" value="0" />
			<input type="submit" name="submit" value="Create Home Page" />
		</form>
	</div>
	</cfoutput>
	</cfif>
<cfoutput>
	<h3>Ad-hoc pages</h3>
	<a href="#BuildUrl('admin.showCreateTcontent','siteId=#siteId#')#">Create Page</a>
	<table border="1" width="100%">
		<tr>
			<th>Name</th>
			<th>Alias</th>
			<th>Active</th>
			<th>Actions</th>
		</tr>
	<cfloop query="pages">
		<tr>
			<td>#pages.title#</td>
			<td>#pages.alias#</td>
			<td>#pages.isActive#</td>
			<td><a href="#BuildUrl('admin.showEditTcontent','id=#pages.tcontent_id#')#">Edit</a> | <a href="#BuildUrl('showPage','p=#pages.alias#')#" target="_blank">Preview</a> | <a href="#BuildUrl('admin.deleteTcontent','id=#pages.tcontent_id#|siteId=#pages.siteId#')#" onClick="return confirm('Are you sure you want to delete this page?');">Delete</a></td>
		</tr>
	</cfloop>
	</table>
</cfoutput>