<cfsilent>
	<cfset event.setArg("pageTitle","#request.siteName# Admin - Rating Reports") />
	<cfquery name="contentRatings" datasource="#getProperty('sDsn')#">
		SELECT
			r.title,
			xyz.*
		FROM cms_resources r
		JOIN (SELECT
				contentId,
				AVG(rate) as avgRating,
				COUNT(1) as ratingCount
			FROM	cms_contentRatings
			GROUP BY contentId) as xyz
		ON r.resourceId = xyz.contentId
		WHERE r.siteId = <cfqueryparam value="#request.siteId#" />
		ORDER BY xyz.ratingCount DESC, xyz.avgRating DESC
	</cfquery>
</cfsilent>
<cfoutput>
<table border="1" width="100%">
	<tr>
		<th>Title</th>
		<th>Rating</th>
		<th>Count</th>
	</tr>
	<cfloop query="contentRatings">
	<tr>
		<td><a href="#BuildUrl('showResource','resourceId=#contentId#')#" target="_blank">#contentRatings.title#</a></td>
		<td>#DecimalFormat(contentRatings.avgRating)#</td>
		<td><a href="#BuildUrl('admin.showRatingReportsDetail','resourceId=#contentRatings.contentId#')#">#contentRatings.ratingCount#</a></td>
	</tr>
	</cfloop>
</table>
</cfoutput>