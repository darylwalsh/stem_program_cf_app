<cfsilent>
	<cfset event.setArg("pageTitle","#request.siteName# - Institute Applications") />
	<cfquery name="qApplications" datasource="#getProperty('sDsn')#">
		SELECT DISTINCT
			su.sponsor_user_guid,
			su.LAST_NAME,
			su.FIRST_NAME,
			su.EMAIL,
			si.applicationStatus,
			si.applicationId
		FROM SPONSOR_FORM AS f INNER JOIN
        SPONSOR_FORM_SECTION AS s ON f.FORM_GUID = s.FORM_GUID INNER JOIN
        SPONSOR_FORM_SECTION_FIELD AS sfs ON s.FORM_SECTION_GUID = sfs.FORM_SECTION_GUID INNER JOIN
        SPONSOR_FORM_RESPONSE AS sfr ON sfs.FORM_SECTION_FIELD_GUID = sfr.FORM_SECTION_FIELD_GUID INNER JOIN
        SPONSOR_USER AS su ON sfr.SPONSOR_USER_GUID = su.SPONSOR_USER_GUID
		LEFT JOIN stemInstitute si on si.sponsor_user_guid = su.sponsor_user_guid
		ORDER BY si.applicationId
	</cfquery>
	<cfsavecontent variable="js">
<script type="text/javascript">
			function updateStatus(formName) {
				var form = $(formName);
				form.request({
				onSuccess: function(transport) {
				var successText = String(transport.responseText);
				if (successText.indexOf('false')!=-1){
					alert('Error updating.');
				}else{
					alert('Status updated.');
				}
				}
			});
		}			
</script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
<h1>#qApplications.RecordCount# responses.</h1>
<table border="1" width="100%">
	<tr>
		<th>View</th>
		<th>Application ID</th>
		<th>Last Name</th>
		<th>First Name</th>
		<th>Email</th>
		<th>status</th>
		<th>Status</th>
	</tr>
	<cfloop query="qApplications">
	<tr>
		<td><a href="#BuildUrl('admin.showResponse','sponsor_user_guid=#sponsor_user_guid#')#">View</a></td>
		<td>#applicationId#</td>
		<td>#last_name#</td>
		<td>#first_name#</td>
		<td>#email#</td>
		<td>#applicationStatus#</td>
		<td>
		<form name="#sponsor_user_guid#Status" id="#sponsor_user_guid#Status" method="post" action="/views/admin/xhr/updateStatus.cfm" onsubmit="updateStatus(this.id); return false;">
			<input type="hidden" name="sponsor_user_guid" value="#sponsor_user_guid#" />
			<select name="applicationStatus" onchange="updateStatus('#sponsor_user_guid#Status');">
			<cfif applicationStatus EQ "To Be Reviewed">
				<option value="To Be Reviewed" selected="selected">To Be Reviewed</option>
				<option value="Not Finalist">Not Finalist</option>
				<option value="Not Selected">Not Selected</option>
				<option value="Finalist">Finalist</option>
				<option value="Selected">Selected</option>
			<cfelseif applicationStatus EQ "Not Selected">
				<option value="To Be Reviewed">To Be Reviewed</option>
				<option value="Not Finalist">Not Finalist</option>
				<option value="Not Selected" selected="selected">Not Selected</option>
				<option value="Finalist">Finalist</option>
				<option value="Selected">Selected</option>
			<cfelseif applicationStatus EQ "Finalist">
				<option value="To Be Reviewed">To Be Reviewed</option>
				<option value="Not Finalist">Not Finalist</option>
				<option value="Not Selected">Not Selected</option>
				<option value="Finalist" selected="selecte">Finalist</option>
				<option value="Selected">Selected</option>			
			<cfelseif applicationStatus EQ "Selected">
				<option value="To Be Reviewed">To Be Reviewed</option>
				<option value="Not Finalist">Not Finalist</option>
				<option value="Not Selected">Not Selected</option>
				<option value="Finalist">Finalist</option>
				<option value="Selected" selected="selected">Selected</option>
			<cfelse>
				<option value="To Be Reviewed">To Be Reviewed</option>
				<option value="Not Finalist">Not Finalist</option>
				<option value="Not Selected">Not Selected</option>
				<option value="Finalist">Finalist</option>
				<option value="Selected">Selected</option>
			</cfif>
			</select>
			<!--- <input type="submit" value="Update" /> --->
		</form>
		</td>
	</tr>
	</cfloop>
</table>
</cfoutput>