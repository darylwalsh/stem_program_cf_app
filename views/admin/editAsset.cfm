<cfsilent>
	<cfset asset = event.getArg("asset") />
	<cfset taxonomys = event.getArg("taxonomys") />
	<cfset mediaFiles = event.getArg("mediaFiles") />
	<cfset siteNavigations = event.getArg("siteNavigations") />
	<cfset siteId = request.siteId />
	<cfset guidAssetMediaFileId = application.udf.CreateGUID() />
	<cfset event.setArg("pageTitle", "#request.siteName# Admin - #asset.getTitle()# - Edit") />
	<cfquery name="regions" datasource="EDU_SEC">
		SELECT
			scr.region_code,
			scr.region_name
		FROM	system_country_region scr
		WHERE	0=0
		AND	scr.country_code = 'US'
		ORDER BY scr.region_name
	</cfquery>
</cfsilent>
<link href="css/swfupload.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/swfupload.js"></script>
<script type="text/javascript" src="js/swfupload.queue.js"></script>
<script type="text/javascript" src="js/fileprogress.js"></script>
<script type="text/javascript" src="js/handlers.js"></script>
<cfoutput>
<script type="text/javascript">
	<!--
		var upload1;

		window.onload = function() {
			upload1 = new SWFUpload({
				// Backend Settings
				upload_url: "/index.cfm?event=admin.processUploadForm",	// Relative to the SWF file (or you can use absolute paths)
				post_params: {
					"guidAssetMediaFileId" : "#guidAssetMediaFileId#",
					"guidAssetId" : "#asset.getGuidAssetMetaDataId()#",
					"intFormatId" : "1"
				},

				// File Upload Settings
				file_size_limit : "307200",	// 300MB
				file_types : "*.*",
				file_types_description : "All Files",
				file_upload_limit : "5",
				file_queue_limit : "0",

				// Event Handler Settings (all my handlers are in the Handler.js file)
				file_dialog_start_handler : fileDialogStart,
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,

				// Button Settings
				button_image_url : "/img/XPButtonUploadText_61x22.png",	// Relative to the SWF file
				button_placeholder_id : "spanButtonPlaceholder1",
				button_width: 61,
				button_height: 22,
				
				// Flash Settings
				flash_url : "swf/swfupload.swf",
				

				custom_settings : {
					progressTarget : "fsUploadProgress1",
					cancelButtonId : "btnCancel1"
				},
				
				// Debug Settings
				debug: false
			});
	     }
	     -->
	</script>
</cfoutput>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/de_tinyMCE.js"></script>
<cfoutput>
<div style="padding-left:5px;">
	<div style="padding:3px;margin:3px;">
	<form name="updateAsset" action="#BuildUrl('admin.updateAsset')#" method="post">
		<input name="guidAssetId" type="hidden" value="#asset.getGuidAssetMetaDataId()#" />
		<input name="guidAssetMetaDataId" type="hidden" value="#asset.getGuidAssetMetaDataId()#" />
		<input name="country_code" type="hidden" value="#asset.getCountry_code()#" />
		<input type="hidden" name="updated_by" value="#cookie.app_user_guid#" />
		<input type="hidden" name="siteId" value="#request.siteId#" />
		<table>
			<tr>
				<td>Id</td>
				<td>#asset.getGuidAssetMetaDataId()#</td>
			</tr>
			<tr>
				<td>Create Date</td>
				<td>#asset.getCreate_date()#</td>
			</tr>
			<tr>
				<td>Updated Date</td>
				<td>#asset.getUpdated_date()#</td>
			</tr>
			<tr>
				<td>Content Type</td>
				<td>
					<select name="content_type">
						<option value="page" <cfif asset.getContent_type() EQ "page">selected</cfif>>Page</option>
						<option value="resource" <cfif asset.getContent_type() EQ "resource">selected</cfif>>Resource</option>
						<option value="webinar" <cfif asset.getContent_type() EQ "webinar">selected</cfif>>Webinar</option>
						<option value="custom" <cfif asset.getContent_type() EQ "custom">selected</cfif>>Custom</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>
					<select name="status">
						<option value="7" <cfif asset.getStatus() EQ 7>selected</cfif>>Pending</option>
						<option value="8" <cfif asset.getStatus() EQ 8>selected</cfif>>Published</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Title</td>
				<td><input name="title" type="text" value="#asset.getTitle()#" size="50" /> <a target="_blank" href="#BuildUrl('showAsset','guidAssetId=#asset.getGuidAssetMetaDataId()#')#">Preview</a></td>
			</tr>
			<tr>
				<td>Keywords</td>
				<td><input name="keywords" type="text" value="#asset.getKeywords()#" size="50" /></td>
			</tr>
			<tr>
				<td>Public</td>
				<td>
				<cfif asset.getIsOpen() EQ true>
					<input name="isOpen" type="checkbox" value="1" checked />
				<cfelse>
					<input name="isOpen" type="checkbox" value="1" />
				</cfif>
				</td>
			</tr>
			<tr>
				<td>Region</td>
				<td>
					<select name="region_code">
						<option value="00" <cfif asset.getRegion_code() EQ "00">selected</cfif>>All Regions</option>
						<cfloop query="regions">
							<option value="#region_code#" <cfif asset.getRegion_code() EQ region_code>selected</cfif>>#region_name#</option>
						</cfloop>
					</select>
				</td>
			</tr>
		</table>
		Content:<br />
		<textarea name="title_description" style="width:600px;height:400px;">
			#asset.getTitle_description()#
		</textarea><br />
		</cfoutput>
		<cfoutput query="siteNavigations" group="lev1id">
			<ul>
				<li>
					<cfif lev2name NEQ "">
						<a href="##" onClick="$('#lev1id#').toggle();return false;">#lev1name#</a>
						<div id="#lev1id#" style="display:none;">
						<ul>
						<cfoutput>
							<li>
							<cfif ListFindNoCase(asset.getGuidTaxIds(), lev2id)>
								<input id="guidTaxIds" name="guidTaxIds" type="checkbox" value="#lev2id#" checked />
							<cfelse>
								<input id="guidTaxIds" name="guidTaxIds" type="checkbox" value="#lev2id#" />
							</cfif>
								#lev2name#
							</li>
						</cfoutput>
						</ul>
						</div>
					<cfelse>
						<cfif ListFindNoCase(asset.getGuidTaxIds(), lev1id)>
							<input id="guidTaxIds" name="guidTaxIds" type="checkbox" value="#lev1id#" checked />
						<cfelse>
							<input id="guidTaxIds" name="guidTaxIds" type="checkbox" value="#lev1id#" />
						</cfif>
						#lev1name#
					</cfif>
				</li>
			</ul>
		</cfoutput>
		<cfoutput>
		<input type="submit" value="Save" />
	</form>
	</div>
<!---	<div style="border:1px solid;padding:1px;margin:1px;">
		Attach resources to asset:
		<form name="createMediaFile" action="#BuildUrl('admin.processUploadForm')#" method="post" enctype="multipart/form-data">
			<input type="hidden" name="guidAssetMediaFileId" value="#application.udf.CreateGUID()#" />
			<input type="hidden" name="guidAssetId" value="#asset.getGuidAssetMetaDataId()#" />
			<input type="hidden" name="intFormatId" value="1" />
			<input name="fileData" type="file" /><br />
			<input type="submit" value="Upload File" />
		</form>
	</div>--->
	<div style="padding-top:10px;">
		<div class="fieldset flash" id="fsUploadProgress1">
			<span class="legend">Upload Files:</span>
		</div>
		<div style="padding-left: 5px;">
			<span id="spanButtonPlaceholder1"></span>
			<input id="btnCancel1" type="button" value="Cancel Uploads" onclick="cancelQueue(upload1);" disabled="disabled" style="margin-left: 2px; height: 22px; font-size: 8pt; margin-botton: 10px;" />
			<br />
		</div>
	</div>
	
	<div>
	<cfif arrayLen(mediaFiles) GT 0>
		<table border="1" width="100%">
			<tr>
				<th>url</th>
				<th>path</th>
				<th>file name</th>
				<th>file size</th>
				<th>delete</th>
			</tr>
			<cfloop index="i" from="1" to="#arrayLen(mediaFiles)#">
			<tr>
				<td>#mediaFiles[i].getMediaFileServerUrl()#</td>
				<td>#mediaFiles[i].getStrFilePath()#</td>
				<td>#mediaFiles[i].getStrFileName()#</td>
				<td>#application.udf.FileSize(mediaFiles[i].getIntFileSizeBytes())#</td>
				<td><a href="#BuildUrl('admin.deleteMediaFile','guidAssetMediaFileId=#mediaFiles[i].getGuidAssetMediaFileId()#|guidAssetId=#mediaFiles[i].getGuidAssetId()#')#">delete</a></td>
			</tr>
			</cfloop>		
		</table>
	</cfif>
	</div>
</div>
</cfoutput>