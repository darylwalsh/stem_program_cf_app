<cfsilent>
	<cfquery name="deleteAndInsert" datasource="#getProperty('sDsn')#">
		DELETE FROM cms_searches_aggregate

		INSERT INTO cms_searches_aggregate
		(
		terms,
		theDate,
		numSearches,
		siteId
		)
		SELECT
			LOWER(terms) as terms,
			DATEADD(DD, -DATEDIFF(DD, dtCreated, 1), 1) as theDate,
			COUNT(1) AS numSearches,
			s.siteId
		FROM	cms_searches s
		GROUP BY LOWER(terms), DATEADD(DD, -DATEDIFF(DD, dtCreated, 1), 1), s.siteId
	</cfquery>
</cfsilent>