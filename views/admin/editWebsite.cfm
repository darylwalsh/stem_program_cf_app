<cfsilent>
	<cfset event.setArg("pageTitle", "#request.siteName# - Site Members") />
	<cfset website = event.getArg("website") />
	<cfset success = event.getArg("success") />
	<cfset websiteUsers = event.getArg("websiteUsers") />
	<cfset app_user_guid = "00000000-0000-0000-0000-000000000000" />
	<cfif StructKeyExists(cookie,"app_user_guid") AND IsValid("guid",cookie.app_user_guid)>
		<cfset app_user_guid = cookie.app_user_guid />
	</cfif>
</cfsilent>
<cfoutput>
	<cfif success EQ true>
		Operation successful.<br />
	</cfif>
		<div style="border:1px solid;padding: 3px;margin: 3px;">
			Edit website:
			<form action="#BuildUrl('admin.saveWebsite')#" method="post">
				<input type="hidden" name="siteId" value="#website.getSiteId()#" />
				Url: <input type="text" name="url" value="#website.getUrl()#" />
				<br />
				Name: <input type="text" name="name" value="#website.getName()#" />
				<br />
				<input type="hidden" name="created_by" value="#website.getCreated_by()#" />
				<input type="hidden" name="create_date" value="#website.getCreate_date()#" />
				<input type="hidden" name="isActive" value="#website.getIsActive()#" />
				<input type="hidden" name="updated_by" value="#app_user_guid#" />
				<input type="hidden" name="updated_date" value="#CreateODBCDateTime(Now())#" />
				<input type="submit" name="submit" value="Submit" />
			</form>
		</div>
		<div style="border:1px solid;padding: 3px;margin: 3px;">
			Add user:
			<form action="#BuildUrl('admin.saveWebsiteUser')#" method="post">
				<input type="hidden" name="siteId" value="#website.getSiteId()#" />
				username: <input type="text" name="uid" value="" />
				<br />
				<input type="submit" name="submit" value="Submit" />
			</form>
		</div>
		<div style="border:1px solid;padding: 3px;margin: 3px;">
			<table border="1">
				<tr>
					<td>Remove</td>
					<td>first name</td>
					<td>last name</td>
					<td>username</td>
				</tr>
				<cfloop query="websiteUsers">
				<tr>
					<td><a href="#BuildUrl('admin.removeWebsiteUser','app_user_guid=#app_user_guid#|siteId=#siteId#')#">Remove</a></td>
					<td>#first_name#</td>
					<td>#last_name#</td>
					<td>#uid#</td>
				</tr>
				</cfloop>
			</table>
		</div>
</cfoutput>