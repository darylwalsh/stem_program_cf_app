<cfsilent>
	<cfset event.setArg("pageTitle", "#request.siteName# admin - Categories") />
	<cfset categories = event.getArg("categories") />
	<cfset newId = application.udf.CreateGUID() />
</cfsilent>
<cfoutput>
	<form name="createCategory" action="#BuildUrl('admin.processCategoryForm')#" method="post">
		<input type="hidden" name="dtCreated" value="#CreateODBCDateTime(Now())#" />
		<input type="hidden" name="createdBy" value="#cookie.app_user_guid#" />
		<input type="hidden" name="ipCreated" value="#cgi.REMOTE_ADDR#" />
		<input type="hidden" name="categoryId" value="#newId#" />
		<input type="hidden" name="siteId" value="#request.siteId#" />
		<input type="hidden" name="parentId" value="" />
		<input type="hidden" name="isActive" value="1" />
		Name: <input type="text" name="name" value="" /><br />
		orderNo: <input type="text" name="orderNo" value="" /><br />
		<input type="submit" name="create" value="Create" />
	</form>
	<table border="1" width="100%">
		<tr>
			<th>Order</th>
			<th>Category</th>
			<th>Active</th>
			<th>Actions</th>
		</tr>
		<cfloop index="i" from="1" to="#arrayLen(categories)#">
		<tr>
			<td>#categories[i].getOrderNo()#</td>
			<td>#categories[i].getName()#</td>
			<td>#categories[i].getIsActive()#</td>
			<td>Edit | <a href="#BuildUrl('admin.deleteCategory','id=#categories[i].getCategoryId()#')#">Delete</a></td>
		</tr>
		</cfloop>
	</table>
</cfoutput>