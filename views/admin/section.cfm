<cfsilent>
	<cfset taxonomy = event.getArg("taxonomy") />
	<cfset childTaxonomys = event.getArg("childTaxonomys") />
	<cfset assets = event.getArg("assets") />
	<cfset root = false />
	<cfif taxonomy.getGuidLevelOneId() EQ "">
		<cfset guidLevelOneId = taxonomy.getGuidTaxId() />
		<cfset root = true />
	<cfelse>
		<cfset guidLevelOneId = taxonomy.getGuidLevelOneId() />
	</cfif>
	<cfset event.setArg("pageTitle","DE Help Admin - #taxonomy.getName()#") />
</cfsilent>
<cfoutput>
	<cfif taxonomy.getGuidParentId() NEQ "">
		<a href="#BuildUrl('admin.showSection','guidTaxId=#taxonomy.getGuidParentId()#')#">Up</a><br />
	</cfif>
	<form name="updateNode" action="#BuildUrl('admin.updateSection')#" method="post">
		<input type="hidden" name="guidTaxId" value="#taxonomy.getGuidTaxId()#" />
		<input type="hidden" name="intLevel" value="#taxonomy.getIntLevel()#" />
		<input type="hidden" name="guidParentId" value="#taxonomy.getGuidParentId()#" />
		<input type="hidden" name="guidLevelOneId" value="#taxonomy.getGuidLevelOneId()#" />
		<input type="hidden" name="intOrder" value="#taxonomy.getIntOrder()#" />
		<input type="hidden" name="isActive" value="#taxonomy.getIsActive()#" />
		Name: <input type="text" name="name" value="#taxonomy.getName()#" /><br />
		Link: <input type="text" name="link" value="#taxonomy.getLink()#" />
		<input type="submit" value="Save" />
	</form>
		<table border="1" width="100%">
			<tr>
				<th>Order</th>
				<th>Name</th>
				<th>Delete</th>
				<th>Move Up</th>
				<th>Move Down</th>
			</tr>
		<cfloop query="childTaxonomys">
			<tr>
				<td>#intOrder#</td>
				<td>
					<a href="#BuildUrl('admin.showSection','guidTaxId=#childTaxonomys.guidTaxId#')#">#name#</a>
				</td>
				<td><a href="#BuildUrl('admin.deleteSection','guidTaxId=#guidTaxId#')#">Delete</a></td>
				<td><a href="#BuildUrl('admin.moveNavOrderUp','guidTaxId=#guidTaxId#|guidParentId=#guidLevelOneId#')#">Move Up</a></td>
				<td><a href="#BuildUrl('admin.moveNavOrderDown','guidTaxId=#guidTaxId#|guidParentId=#guidLevelOneId#')#">Move Down</a></td>
			</tr>
		</cfloop>
		</table>
	Add Node:
					<form action="#BuildUrl('admin.createSection')#" method="post">
						<input type="hidden" name="intLevel" value="#taxonomy.getIntLevel() + 1#" />
						<input type="hidden" name="guidParentId" value="#taxonomy.getGuidTaxId()#" />
						<input type="hidden" name="guidLevelOneId" value="#guidLevelOneId#" />
						<input type="text" name="name" value="" />
						<input type="submit" name="submit" value="Submit" />
					</form>
	Assets:<br />
	<table border="1" width="100%">
		<tr>
			<th>Views</th>
			<th>Status</th>
			<th>Title</th>
			<th>Edit</th>
			<!--- <td>Move Up</td>
			<td>Move Down</td> --->
		</tr>
	<cfloop query="assets">
		<tr>
			<td>#copyright#</td>
			<td><cfif status EQ 7>Pending<cfelseif status EQ 8>Published</cfif></td>
			<td><a href="#BuildUrl('showAsset','guidAssetId=#assets.guidAssetMetaDataId#|guidTaxId=#guidTaxId#')#" target="_blank">#title#</a></td>
			<td><a href="#BuildUrl('admin.showEditAsset','guidAssetId=#guidAssetMetaDataId#')#">edit</a></td>
			<!---<td><a href="#BuildUrl('moveNavContentOrderUp','guidTaxId=#guidTaxId#|guidAssetId=#guidAssetId#')#">Move Up</a></td>
			<td><a href="#BuildUrl('moveNavContentOrderDown','guidTaxId=#guidTaxId#|guidAssetId=#guidAssetId#')#">Move Down</a></td>--->
		</tr>
	</cfloop>
	</table>
</cfoutput>