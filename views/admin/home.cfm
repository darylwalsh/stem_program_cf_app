<cfsilent>
	<cfset event.setArg("pageTitle", "#request.siteName# Admin - Home") />
	<cfset userWebsites = event.getArg("userWebsites") />
	<cfset newGuid = application.udf.CreateGUID() />
</cfsilent>
<cfoutput>
	<div style="padding-left:5px;">
		<table border="1">
			<tr>
				<th>Name</th>
				<th>url</th>
				<th>Manage Site</th>
			</tr>
			<cfloop query="userWebsites">
				<tr>
					<td>
						<a href="#BuildUrl('admin.showSite','siteId=#siteId#')#">#name#</a>
					</td>
					<td>#url#</td>
					<td>
						<a href="#BuildUrl('admin.editWebsite','siteId=#siteId#')#">Edit</a>
					</td>
				</tr>
			</cfloop>
		</table>
	<cfif StructKeyExists(url,"create")>
		<div>
			Create website:
			<form action="#BuildUrl('admin.saveWebsite')#" method="post">
				siteId: <input type="text" name="siteId" value="#newGuid#" />
				<br />
				Url: <input type="text" name="url" value="" />
				<br />
				Name: <input type="text" name="name" value="" />
				<br />
				<input type="hidden" name="created_by" value="#cookie.app_user_guid#" />
				<input type="hidden" name="create_date" value="#CreateODBCDateTime(Now())#" />
				<input type="hidden" name="isActive" value="1" />
				<input type="submit" name="submit" value="Submit" />
			</form>
		</div>
	</cfif>
	</div>
</cfoutput>