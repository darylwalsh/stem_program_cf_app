<cfsilent>
	<cfset event.setArg("pageTitle","#request.siteName# Admin - All Assets") />
	<cfset resources = event.getArg("resources") />
<cfsavecontent variable="css">
<link rel="stylesheet" type="text/css" href="css/tablekit.css" />
</cfsavecontent>
<cfsavecontent variable="js">
<script type="text/javascript" src="js/fastinit.js"></script>
<script type="text/javascript" src="js/tablekit.js"></script>
</cfsavecontent>
<cfset event.setArg("css",css) />
<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
<div style="padding:5px;">
	<div><a href="#BuildUrl('admin.showResourceForm')#">Create Resource</a></div>
	<table border="1" class="sortable" width="100%">
		<tr>
			<th>Views</th>
			<th>Type</th>
			<th>Name</th>
			<th>Files</th>
			<th>Date</th>
			<th class="nosort">Actions</th>
		</tr>
	<cfloop index="i" from="1" to="#arrayLen(resources)#">
		<tr>
			<td><cfif resources[i].getViews() EQ "">0<cfelse>#resources[i].getViews()#</cfif></td>
			<td>#resources[i].getType()#</td>
			<td><a href="#BuildUrl('showResource','resourceId=#resources[i].getResourceId()#')#" target="_blank">#resources[i].getTitle()#</a></td>
			<td>#arrayLen(resources[i].getFiles())#</td>
			<td>#resources[i].getDtCreated()#</td>
			<td>
				<a href="#BuildUrl('admin.showResourceForm','resourceId=#resources[i].getResourceId()#')#">edit</a>
				|
				<a href="#BuildUrl('admin.deleteResource','resourceId=#resources[i].getResourceId()#')#" onClick="return confirm('Are you sure you want to delete this resource and all associated files?');">delete</a>
			</td>
		</tr>
	</cfloop>
	</table>
</div>
</cfoutput>