<cfsilent>
	<cfset event.setArg("pageTitle", "#request.siteName# - Edit Content") />
	<cfset tcontent = event.getArg("tcontent") />
	<cfset newId = application.udf.CreateGUID() />
	<cfsavecontent variable="js">
<!---
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckfinder/ckfinder.js"></script>
--->
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
// General options
mode : "textareas",
theme : "advanced",
plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager",
extended_valid_elements : "link[type|rel|href],p[style]",
// Theme options
theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
theme_advanced_toolbar_location : "top",
theme_advanced_toolbar_align : "left",
theme_advanced_statusbar_location : "bottom",
theme_advanced_resizing : true,
	template_templates : [
		{
			title : "Image Gallery",
			src : "/templates/imageGallery.htm",
			description : "Adds Image Gallery"
		}
	],


// Example content CSS (should be your site CSS)
//content_css : "/css/stemacademy.css",

// Drop lists for link/image/media/template dialogs
//template_external_list_url : "js/template_list.js",
//external_link_list_url : "js/link_list.js",
external_image_list_url : "js/imageList.cfm"
//media_external_list_url : "js/media_list.js",
 
// Replace values for the template plugin
//template_replace_values : {
//username : "Some User",
//staffid : "991234"
//}
});
</script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
	<div>
		<form action="#BuildUrl('admin.updateTcontent')#" method="post">
			<input type="hidden" name="id" value="#tcontent.getTcontent_id()#" />
			<input type="hidden" name="tcontent_id" value="#tcontent.getTcontent_id()#" />
			<input type="hidden" name="siteId" value="#tcontent.getSiteId()#" />
			<input type="hidden" name="parentId" value="#tcontent.getParentId()#" />
			<input type="hidden" name="contentId" value="#tcontent.getContentId()#" />
			<input type="hidden" name="template" value="#tcontent.getTemplate()#" />
			<input type="hidden" name="isActive" value="#tcontent.getIsActive()#" />
			<input type="hidden" name="dtCreated" value="#tcontent.getDtCreated()#">
			<input type="hidden" name="dtUpdated" value="#CreateODBCDateTime(Now())#" />
			<input type="hidden" name="createdBy" value="#tcontent.getCreatedBy()#" />
			<input type="hidden" name="updatedBy" value="#cookie.app_user_guid#" />
			<input type="hidden" name="ipCreated" value="#tcontent.getIpCreated()#" />
			<input type="hidden" name="ipUpdated" value="#cgi.REMOTE_ADDR#" />
		<table>
			<tr>
				<td>Content Type</td>
				<td>
					<select name="type">
						<option value="page" <cfif tcontent.getType() EQ "page">selected</cfif>>Page</option>
						<option value="custom" <cfif tcontent.getType() EQ "custom">selected</cfif>>Custom</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>
					<select name="approved">
						<option value="0" <cfif tcontent.getApproved() EQ 0>selected</cfif>>Pending</option>
						<option value="1" <cfif tcontent.getApproved() EQ 1>selected</cfif>>Published</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Navigation?</td>
				<td>
					<select name="isNav">
						<option value="1" <cfif tcontent.getIsNav() EQ 1>selected</cfif>>Yes</option>
						<option value="0" <cfif tcontent.getIsNav() EQ 0>selected</cfif>>No</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Title</td>
				<td><input name="title" type="text" value="#tcontent.getTitle()#" size="50" /></td>
			</tr>
			<tr>
				<td>Alias</td>
				<td><input name="alias" type="text" value="#tcontent.getAlias()#" size="50" /></td>
			</tr>
			<tr>
				<td>Navigation Title (optional)</td>
				<td><input name="menuTitle" type="text" value="#tcontent.getMenuTitle()#" size="50" /></td>
			</tr>
			<tr>
				<td>Custom page</td>
				<td><input name="filename" type="text" value="#tcontent.getFileName()#" size="50" /></td>
			</tr>
			<tr>
				<td>Order</td>
				<td><input type="text" name="orderNo" value="#tcontent.getOrderNo()#" /></td>
			</tr>
		</table>
		<br />
	<cfif tcontent.getType() EQ "page">
		Content:
		<textarea id="editor1" name="body" style="width:100%;height:400px;">#tcontent.getBody()#</textarea>
		<!---
		<script type="text/javascript">
			var editor = CKEDITOR.replace( 'editor1');
			CKFinder.SetupCKEditor(editor);
		</script>
		--->
	</cfif>
		<input type="submit" value="Save" />
		</form>
	</div>
</cfoutput>