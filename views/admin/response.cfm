<cfsilent>
	<cfset sponsor_user_guid = event.getArg("sponsor_user_guid") />
	<cfset event.setArg("pageTitle","#request.siteName# - Institute Applications") />
	<cfquery name="qApplication" datasource="#getProperty('sDsn')#">
SELECT     su.sponsor_user_guid,su.LAST_NAME, su.FIRST_NAME, su.EMAIL, su.SNAME, s.DESCRIPTION, s.SORT_ORDER section_order
			, sfs.DESCRIPTION AS field_description, sfs.SORT_ORDER AS field_order
			,  sfr.RESPONSE_VALUE, sfr.SORT_ORDER AS response_order
FROM         SPONSOR_FORM AS f INNER JOIN
                      SPONSOR_FORM_SECTION AS s ON f.FORM_GUID = s.FORM_GUID INNER JOIN
                      SPONSOR_FORM_SECTION_FIELD AS sfs ON s.FORM_SECTION_GUID = sfs.FORM_SECTION_GUID INNER JOIN
                      SPONSOR_FORM_RESPONSE AS sfr ON sfs.FORM_SECTION_FIELD_GUID = sfr.FORM_SECTION_FIELD_GUID INNER JOIN
                      SPONSOR_USER AS su ON sfr.SPONSOR_USER_GUID = su.SPONSOR_USER_GUID
WHERE 0=0
AND su.sponsor_user_guid = <cfqueryparam value="#sponsor_user_guid#" />
ORDER BY su.LAST_NAME, su.FIRST_NAME, s.SORT_ORDER, field_order, response_order
	</cfquery>
</cfsilent>
<cfoutput>
<table border="1" width="100%">
	<thead>
		<th>Question</th>
		<th>Response</th>
	</thead>
	<cfloop query="qApplication">
	<tr>
		<td>#description#</td>
	<cfif description EQ "Application Video" OR description EQ "Application Essay" OR description EQ "Recommendation Rating Sheet">
		<td><a href="http://static.redactededucation.com/feeds/stemacademy/#sponsor_user_guid#/#UrlEncodedFormat(response_value)#">#response_value#</a></td>
	<cfelse>
		<td>#response_value#</td>
	</cfif>
	</tr>
	</cfloop>
</table>
</cfoutput>