<cfsilent>
	<cfparam name="url.sponsor_user_guid" default="00000000-0000-0000-0000-00000000" />
	<cfparam name="url.applicationStatus" default="semi-finalist" />
	<cfparam name="form.sponsor_user_guid" default="#url.sponsor_user_guid#" />
	<cfparam name="form.applicationStatus" default="#url.applicationStatus#" />
	<cfset variables.success = true />
	<cfset variables.dsn = "Sponsorships" />

	<cftry>
	<cfquery name="qExists" datasource="#variables.dsn#">
		SELECT COUNT(1) AS idexists
		FROM stemInstitute
		WHERE sponsor_user_guid = <cfqueryparam value="#form.sponsor_user_guid#" />
	</cfquery>
	
	<cfif qExists.idexists>
		<cfquery name="qUpdate" datasource="#variables.dsn#">
			UPDATE stemInstitute
			SET applicationStatus = <cfqueryparam value="#form.applicationStatus#" />
			WHERE sponsor_user_guid = <cfqueryparam value="#form.sponsor_user_guid#" />
		</cfquery>
	<cfelse>
		<cfquery name="qInsert" datasource="#variables.dsn#">
			INSERT INTO stemInstitute
			(sponsor_user_guid,applicationStatus)
			VALUES
			(<cfqueryparam value="#form.sponsor_user_guid#">,<cfqueryparam value="#form.applicationStatus#">)
		</cfquery>	
	</cfif>
		<cfcatch>
			<cfset variables.success = false />
		</cfcatch>
	</cftry>
</cfsilent>
<cfoutput>#variables.success#</cfoutput>