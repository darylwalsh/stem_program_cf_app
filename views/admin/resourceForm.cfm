<cfsilent>
	<cfset resource = event.getArg("resource") />
	<cfset files = resource.getFiles() />
	<cfset categories = event.getArg("categories") />
	<cfset types = event.getArg("types") />
	<cfset grades = event.getArg("grades") />
	<cfset blocks = event.getArg("blocks") />
	<cfset resourceCategories = resource.getCategories() />
	<cfset resourceTypes = resource.getTypes() />
	<cfset resourceGrades = resource.getGrades() />
	<cfset resourceBlocks = resource.getBlocks() />
	<cfset c = event.getArg("c") />
	<cfset event.setArg("pageTitle","#request.siteName# - #resource.getTitle()#") />
	<cfset app_user_guid = "00000000-0000-0000-0000-000000000000" />
	<cfif StructKeyExists(cookie,"app_user_guid")>
		<cfset app_user_guid = cookie.app_user_guid />
	</cfif>
	<cfset currentTypeIDs = "" />
	<cfloop index="i" from="1" to="#arrayLen(resourceTypes)#" step="1">
		<cfset type = resourceTypes[i] />
		<cfset currentTypeIDs = listAppend(currentTypeIDs, type.getTypeId(), ",") />
	</cfloop>
	<cfset currentCategoryIDs = "" />
	<cfloop index="i" from="1" to="#arrayLen(resourceCategories)#" step="1">
		<cfset category = resourceCategories[i] />
		<cfset currentCategoryIDs = listAppend(currentCategoryIDs, category.getCategoryId(), ",") />
	</cfloop>
	<cfset currentGradeIDs = "" />
	<cfloop index="i" from="1" to="#arrayLen(resourceGrades)#" step="1">
		<cfset grade = resourceGrades[i] />
		<cfset currentGradeIDs = listAppend(currentGradeIDs, grade.getGrade_id(), ",") />
	</cfloop>
	<cfset currentBlockIds = "" />
	<cfloop index="i" from="1" to="#arrayLen(resourceBlocks)#" step="1">
		<cfset block = resourceBlocks[i] />
		<cfset currentBlockIds = listAppend(currentBlockIds, block.getBlock_id(), ",") />
	</cfloop>
	<cfif resource.getResourceId() EQ "" OR resource.getResourceId() EQ "00000000-0000-0000-0000-000000000000">
		<cfset resource.setSiteId(request.siteId) />
		<cfset resource.setResourceId(application.udf.CreateGUID()) />
		<cfset resource.setDtCreated(Now()) />
		<cfset resource.setIpCreated(CGI.REMOTE_ADDR) />
		<cfset resource.setCreatedBy(app_user_guid) />
		<cfset resource.setViews(0) />
		<cfset resource.setCreatedByName("#cookie.first_name# #cookie.last_name#") />
	</cfif>
<cfsavecontent variable="js">
<script type="text/javascript" src="js/popupWindow.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
// General options
mode : "textareas",
theme : "advanced",
plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager,media",
 
// Theme options
theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,advhr,|,print,|,ltr,rtl,|,fullscreen",
theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,media",
theme_advanced_toolbar_location : "top",
theme_advanced_toolbar_align : "left",
theme_advanced_statusbar_location : "bottom",
theme_advanced_resizing : true,

// Example content CSS (should be your site CSS)
content_css : "/css/stemacademy.css",

// Drop lists for link/image/media/template dialogs
//template_external_list_url : "js/template_list.js",
//external_link_list_url : "js/link_list.js",
external_image_list_url : "js/imageList.cfm"
//media_external_list_url : "js/media_list.js",
 
// Replace values for the template plugin
//template_replace_values : {
//username : "Some User",
//staffid : "991234"
//}
});
</script>
</cfsavecontent>
<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
<div style="float:left;">
				<h2>STEM Resources</h2>
				<cfif event.getArg("message") NEQ "">
					#event.getArg("message")#
				</cfif>
			<form name="editResourceForm" id="editResourceForm" action="#BuildUrl('admin.saveResource','c=#c#')#" method="post">
				<input type="hidden" name="siteId" value="#resource.getSiteId()#" />
				<input type="hidden" name="resourceId" value="#resource.getResourceId()#" />
				<input type="hidden" name="dtCreated" value="#resource.getDtCreated()#" />
				<input type="hidden" name="ipCreated" value="#resource.getIpCreated()#" />
				<input type="hidden" name="createdBy" value="#resource.getCreatedBy()#" />
				<input type="hidden" name="views" value="#resource.getViews()#" />
			<cfif resource.getResource_id() NEQ "">
				<input type="hidden" name="dtUpdated" value="#CreateODBCDateTime(Now())#" />
				<input type="hidden" name="ipUpdated" value="#CGI.REMOTE_ADDR#" />
				<input type="hidden" name="updatedBy" value="#app_user_guid#" />
			</cfif>
				<p><input type="text" style="width:500px;" name="title" value="#resource.getTitle()#" /> | <a href="#BuildUrl('showResource','resourceId=#resource.getResourceId()#')#" target="_blank">Preview</a></p>
				<p>
					<select name="type">
						<option value="resource" <cfif resource.getType() EQ "resource">selected</cfif> >Resource</option>
						<option value="webinar"  <cfif resource.getType() EQ "webinar">selected</cfif> >Webinar</option>
				<br clear="all" />				
				<textarea name="body" style="width:600px;height:400px;">#resource.getBody()#</textarea>
				<div class="last">
					<strong>Created By:</strong> <input type="text" name="createdByName" value="#resource.getCreatedByName()#" /><br />
					<strong>Keywords:</strong> <input type="text" name="keywords" value="#resource.getKeywords()#" /><br />
					<strong>Subjects:</strong><br />
					<cfloop query="categories">
						<input type="checkbox" name="categoryIDs" value="#categoryId#" <cfif listFindNoCase(currentCategoryIDs, categories.categoryId, ",") NEQ 0>checked="true"</cfif>/> #name#<br />
					</cfloop>
					
					
					<strong>Types:</strong><br />
					<cfloop index="i" from="1" to="#arrayLen(types)#">
						<input type="checkbox" name="typeIDs" value="#types[i].getTypeID()#" <cfif ListFindNoCase(currentTypeIDs,types[i].getTypeID(),",") NEQ 0>checked="checked"</cfif>> #types[i].getName()#<br />
					</cfloop>
					
					<strong>Grade Level:</strong><br />
											<select name="gradeIDs" multiple="multiple" id="gradeIDs" style="width:190px;" size="#arrayLen(grades)#" >
											<cfloop index="i" from="1" to="#arrayLen(grades)#">
												<option value="#grades[i].getGrade_id()#" <cfif ListFindNoCase(currentGradeIDs,grades[i].getGrade_id(),",") NEQ 0>selected="selected"</cfif>>#grades[i].getDescription()#</option>
											</cfloop>	
											</select>
					<br />
					<strong>Content Block:</strong><br />
					<select name="block_id">
						<option value="">None</option>
					<cfloop from="1" to="#ArrayLen(blocks)#" index="i">
						<option value="#blocks[i].getBlock_id()#" <cfif ListFindNoCase(currentBlockIds,blocks[i].getBlock_id(),",") NEQ 0>selected="selected"</cfif>>#blocks[i].getTitle()#</option>
					</cfloop>
					</select>
					<br />
					<input type="submit" value="Save" />
				</div>
				<br />
			</form>
				<br clear="all" />
</div><!---content-left--->
<div style="float:left;">
	<br clear="all" />
	<h4 class="boxtop">Teacher Resources</h4>
	<div id="resource-roll">
		<p>
			<a href="#BuildUrl('admin.showResourceUpload','c=#c#|resourceId=#resource.getResourceId()#')#">Upload Files</a>
			<br />
			<a href="/tasks/convertVideos.cfm" target="_blank">Run Encoder</a>
		</p>
	<cfloop index="i" from="1" to="#ArrayLen(files)#">
		<img src="/assets/stem/thumbnails/#files[i].getThumbnail()#" alt="#files[i].getFilename()#" class="thumb" width="100" />
		<p>
			<a title="#files[i].getFilename()#" href="##" class="file-link">#application.udf.maxLength(files[i].getFileName(),22)#</a><br />
			File Size:  #application.udf.FileSize(files[i].getFileSize())#<br />
			<a title="Download" href="#BuildUrl('getFile','f=#files[i].getFile_id()#')#">Download</a><br />
		<cfswitch expression="#files[i].getFileExt()#">
			<cfcase value="3g2,3gp,asf,asx,avi,flv,m4v,mov,mp4,mpeg,mpg,qt,ram,rm,wmv">
			<a title="View" href="javascript:popupWindow('#BuildUrl('showMediaPlayer','f=#files[i].getFile_id()#')#',500,400);">View</a>
			</cfcase>
			<cfcase value="jpeg,jpg,gif,png,bmp">
			<a title="View" href="/assets/stem/#files[i].getFileId()#.#files[i].getFileExt()#" target="_blank">View</a>
			</cfcase>
		</cfswitch>
			<br />
			<a title="Delete" href="#BuildUrl('admin.deleteFile','c=#c#|f=#files[i].getFile_id()#|resourceId=#resource.getResourceId()#')#" onClick="return confirm('Are you sure you want to delete this file?');">DELETE</a>
		</p>
	</cfloop>
				<br clear="all" />
				</div><!---resource-roll--->  
		<br />
</div><!---content-right--->
</cfoutput>