<cfsilent>
	<cfset event.setArg("pageTitle","#request.siteName# Admin - Search Reports") />
	<cfset currentDateTime = Now() />
	<cfset currentDate = DateFormat(currentDateTime,'mm/dd/yyyy') />
	<cfif event.getArg("date") NEQ "">
		<cfset currentDate = DateFormat(event.getArg("date"),'mm/dd/yyyy') />
	</cfif>
	<cfset prevDate = currentDate - 1 />
	<cfset nextDate = currentDate + 1 />
	
	<cfset currentMonth = DatePart("m",currentDate) />
	<cfif event.getArg("month") NEQ "">
		<cfset currentMonth = event.getArg("month") />
	</cfif>
	<cfset prevMonth = dateAdd("m", -1, currentDate) />
	<cfset nextMonth = dateAdd("m", 1, currentDate) />
	
	<cfset currentYear = DatePart("yyyy", currentDate) />
	<cfif event.getArg("year") NEQ "">
		<cfset currentYear = event.getArg("year") />
	</cfif>
	<cfset prevYear = dateAdd("yyyy", -1, currentDate) />
	<cfset nextYear = dateAdd("yyyy", 1, currentDate) />
	
	<cfset period = "day" />	
	<cfif event.getArg("period") NEQ "">
		<cfset period = event.getArg("period") />
	</cfif>
	
<cfif period EQ "day">
	<cfquery name="daily" datasource="#getProperty('sDsn')#">
		SELECT
			terms,
			numSearches
		FROM	cms_searches_aggregate
		WHERE	theDate = <cfqueryparam value="#currentDate#" />
		AND		siteId = '#request.siteId#'
		ORDER BY numSearches DESC
	</cfquery>
<cfelseif period EQ "month">
	<cfquery name="monthly" datasource="#getProperty('sDsn')#">
		SELECT
			terms,
			SUM(numSearches) AS numSearches
		FROM	cms_searches_aggregate
		WHERE	DatePart(month, theDate) = <cfqueryparam value="#currentMonth#" />
		AND		DatePart(year, theDate) = <cfqueryparam value="#currentYear#" />
		AND		siteId = '#request.siteId#'
		GROUP BY terms
		ORDER BY numSearches DESC
	</cfquery>
<cfelseif period EQ "year">
	<cfquery name="yearly" datasource="#getProperty('sDsn')#">
		SELECT
			terms,
			SUM(numSearches) AS numSearches
		FROM	cms_searches_aggregate
		WHERE	DatePart(year, theDate) = <cfqueryparam value="#currentYear#" />
		AND		siteId = '#request.siteId#'
		GROUP BY terms
		ORDER BY numSearches DESC
	</cfquery>
</cfif>
</cfsilent>
<cfoutput>
<div>
	<a href="#BuildUrl('admin.refreshSearchReports')#">Update Reports</a><br />
	<cfif period EQ "day">
		<strong>Daily</strong>
	<cfelse>
		<a href="#BuildUrl('admin.showSearchReports','period=day')#">Daily</a>
	</cfif> | 
	<cfif period EQ "month">
		<strong>Monthly</strong>
	<cfelse>
		<a href="#BuildUrl('admin.showSearchReports','period=month')#">Monthly</a>
	</cfif> | 
	<cfif period EQ "year">
		<strong>Yearly</strong>
	<cfelse>
		<a href="#BuildUrl('admin.showSearchReports','period=year')#">Yearly</a>
	</cfif>
	<br />
	<cfif period EQ "day">
		<a href="#BuildUrl('admin.showSearchReports','date=#prevDate#')#">#DateFormat(prevDate)#</a> | <strong>#DateFormat(currentDate)#</strong> | <a href="#BuildUrl('admin.showSearchReports','date=#nextDate#')#">#DateFormat(nextDate)#</a><br />
		<table border="1">
			<tr>
				<td>Terms</td>
				<td>Searches</td>
			</tr>
			<cfloop query="daily">
			<tr>
				<td>#terms#</a></td>
				<td>#numSearches#</td>
			</tr>
			</cfloop>
		</table>
	<cfelseif period EQ "month">
		<a href="#BuildUrl('admin.showSearchReports','period=month|date=#prevMonth#')#">#DateFormat(prevMonth, "mm/yyyy")#</a> | <strong>#DateFormat(currentDate, "mm/yyyy")#</strong> | <a href="#BuildUrl('admin.showSearchReports','period=month|date=#nextMonth#')#">#DateFormat(nextMonth, "mm/yyyy")#</a><br />
		<table border="1">
			<tr>
				<td>Terms</td>
				<td>Searches</td>
			</tr>
			<cfloop query="monthly">
			<tr>
				<td>#terms#</a></td>
				<td>#numSearches#</td>
			</tr>
			</cfloop>
		</table>
	<cfelseif period EQ "year">
		<a href="#BuildUrl('admin.showSearchReports','period=year|date=#prevYear#')#">#DateFormat(prevYear, "yyyy")#</a> | <strong>#DateFormat(currentDate, "yyyy")#</strong> | <a href="#BuildUrl('admin.showSearchReports','period=year|date=#nextYear#')#">#DateFormat(nextYear, "yyyy")#</a><br />
		<table border="1">
			<tr>
				<td>Terms</td>
				<td>Searches</td>
			</tr>
			<cfloop query="yearly">
			<tr>
				<td>#terms#</a></td>
				<td>#numSearches#</td>
			</tr>
			</cfloop>
		</table>
	</cfif>
</div>
</cfoutput>