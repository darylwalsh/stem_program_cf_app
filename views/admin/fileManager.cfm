<cfsilent>
	<cfset subDir = event.getArg("subDir") />
	<cfset guidAssetMediaFileId = application.udf.CreateGUID() />
	<cfset event.setArg("pageTitle", "#request.siteName# admin - File Manager") />
	<cfif subDir NEQ "">
		<cfset imgPath = "/#subDir#/" />
	<cfelseif subDir EQ "/">
		<cfset imgPath = subDir />
	<cfelse>
		<cfset imgPath = "/" />
	</cfif>
	<cfset imgDirectory = GetDirectoryFromPath(ExpandPath(imgPath)) />
	<cfset filters = "" />
	<cfif FindNoCase("/",subDir)>
		<cfset replaceString = "/#ListLast(imgPath,"/")#/" />
	<cfelseif subDir NEQ "">
		<cfset replaceString = imgPath />
	</cfif>
	<cfif subDir NEQ "">
		<cfset parentDir = Replace(imgPath,replaceString,"") />
		<cfif Left(parentDir,1) EQ "/">
			<cfset parentDir = Mid(parentDir,2,Len(parentDir)) />
		</cfif>
	</cfif>
	
	<cfdirectory action="list" directory="#imgDirectory#" name="dirQuery" filter="#filters#" sort="type ASC, name ASC" />
</cfsilent>
<cfsavecontent variable="css">
<link href="css/swfupload.css" rel="stylesheet" type="text/css" />
</cfsavecontent>
<cfsavecontent variable="js">
<script type="text/javascript" src="js/swfupload.js"></script>
<script type="text/javascript" src="js/swfupload.queue.js"></script>
<script type="text/javascript" src="js/fileprogress.js"></script>
<script type="text/javascript" src="js/handlers.js"></script>
<cfoutput>
<script type="text/javascript">
		var upload1;

		window.onload = function() {
			upload1 = new SWFUpload({
				// Backend Settings
				upload_url: "/index.cfm?event=admin.processImageUploadForm&path=#imgPath#",	// Relative to the SWF file (or you can use absolute paths)

				// File Upload Settings
				file_size_limit : "307200",	// 300MB
				file_types : "*.*",
				file_types_description : "All Files",
				file_upload_limit : "5",
				file_queue_limit : "0",

				// Event Handler Settings (all my handlers are in the Handler.js file)
				file_dialog_start_handler : fileDialogStart,
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,

				// Button Settings
				button_image_url : "/img/XPButtonUploadText_61x22.png",	// Relative to the SWF file
				button_placeholder_id : "spanButtonPlaceholder1",
				button_width: 61,
				button_height: 22,
				
				// Flash Settings
				flash_url : "swf/swfupload.swf",
				

				custom_settings : {
					progressTarget : "fsUploadProgress1",
					cancelButtonId : "btnCancel1"
				},
				
				// Debug Settings
				debug: false
			});
	     }
</script>
</cfoutput>
</cfsavecontent>
<cfset event.setArg("css",css) />
<cfset event.setArg("js",js) />
<cfoutput>
</cfoutput>
<cfoutput>
	<div style="padding-top:10px;">
		<div class="fieldset flash" id="fsUploadProgress1">
			<span class="legend">Upload Files:</span>
		</div>
		<div style="padding-left: 5px;">
			<span id="spanButtonPlaceholder1" style="width:61px;height:21px;"></span>
			<input id="btnCancel1" type="button" value="Cancel Uploads" onclick="cancelQueue(upload1);" disabled="disabled" style="margin-left: 2px; height: 22px; font-size: 8pt; margin-botton: 10px;" />
			<br />
		</div>
	</div>
	<div>
		<table border="1" width="100%">
			<tr>
				<th>&nbsp;</th>
				<th>#imgPath#</th>
				<th>Size</th>
			</tr>
		<cfif imgPath NEQ "/">
			<tr>
				<td>
					<a href="#BuildUrl('admin.showFileManager','subDir=#parentDir#')#">Up</a>
				</td>
				<td>
					<a href="#BuildUrl('admin.showFileManager','subDir=#parentDir#')#">Parent Directory</a>
				</td>
				<td>&nbsp;</td>
			</tr>
		</cfif>
	<cfloop query="dirQuery">
			<tr>
				<td>#type#</td>
				<td>
				<cfif type EQ "dir">
					<cfif subDir EQ "">
					<a href="#BuildUrl('admin.showFileManager','subDir=#name#')#">#name#</a>
					<cfelse>
					<a href="#BuildUrl('admin.showFileManager','subDir=#subDir#/#name#')#">#name#</a>
					</cfif>
				<cfelse>
					<a href="#imgPath##name#" target="_blank">#name#</a>
				</cfif>
				</td>
				<td>#application.udf.fileSize(size)#</td>
			</tr>
	</cfloop>
		</table>
	</div>
</cfoutput>