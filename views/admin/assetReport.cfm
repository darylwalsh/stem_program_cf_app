<cfsilent>
	<cfset guidAssetId = event.getArg("guidAssetId") />
	
	<cfquery name="assetReport" datasource="AllProductAssets">
		SELECT
			c_a.guidAssetMetaDataId,
			c_a.siteId,
			c_a.theDate,
			CONVERT(datetime, theDate) as dt,
			c_a.title,
			c_a.views
		FROM	contentClickLog_aggregate c_a
		WHERE	guidAssetMetaDataId = <cfqueryparam value="#guidAssetId#" />
		ORDER BY c_a.dt ASC
	</cfquery>
</cfsilent>
<cfoutput>
	#assetReport.title#<br />
	<cfchart showborder="true" chartheight="600" chartwidth="600" yaxistitle="Clicks" xaxistitle="Date">
		<cfchartseries type="area" serieslabel="#assetReport.title#">
			<cfloop query="assetReport">
				<cfchartdata item="#theDate#" value="#views#">
			</cfloop>
		</cfchartseries>
	</cfchart>
</cfoutput>