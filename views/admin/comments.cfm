<cfsilent>
	<cfquery name="qComments" datasource="#getProperty('sDsn')#">
		SELECT
			[comment_id]
			,[content_id]
      ,[contentGuid]
      ,[author]
      ,[email]
      ,[url]
      ,[body]
      ,[dtCreated]
      ,[ipCreated]
      ,[approved]
      ,[agent]
      ,[app_user_guid]
      ,[type]
      ,[parent]
		FROM cms_comments
		ORDER BY dtCreated DESC
	</cfquery>
	<cfsavecontent variable="js">
<script type="text/javascript">
	function deleteComment(comment_id) {
		var url = <cfoutput>'#BuildUrl('admin.xhr.deleteComment')#'</cfoutput>;
		var commentDiv = 'comment-' + comment_id;
		new Ajax.Request(url, {
			method: 'get',
			parameters: 'cid=' + comment_id,
			onSuccess: function(transport) {
				var result = String(transport.responseText).toLowerCase();
				if (result.indexOf('true') >= 0){
					$(commentDiv).fade();
				}
				else {
					alert('Error deleting comment.');
					return;
				}
			}
		});
	}
</script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
	<cfset event.setArg("pageTitle","#request.siteName# - Admin - Comments")>
</cfsilent>
<cfoutput>
<table border="1" width="100%">
	<th>Action</th>
	<th>Author</th>
	<th>Comment</th>
	<th>Resource</th>
	<cfloop query="qComments">
	<tr id="comment-#comment_id#">
		<td style="width:5%"><a href="##" onclick="deleteComment(#comment_id#); return false;">Delete</a></td>
		<td style="width:20%;">
			<strong>#author#</strong><br />
			<a href="mailto:#email#">#email#</a><br />
			#ipCreated#
		</td>
		<td style="width:60%;">
			#dtCreated#<br />
			#body#
		</td>
		<td style="width:15%"><a href="#BuildUrl('showResource','resourceId=#contentGuid#')#" target="_blank">View</a></td>
	</tr>
	</cfloop>
</table>
</cfoutput>