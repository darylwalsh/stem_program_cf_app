<cfsilent>
	<cfset block = event.getArg("block") />
	<cfif NOT IsValid("guid",block.getBlockGuid())>
		<cfset block.setBlockGuid(application.udf.CreateGUID())>
	</cfif>
</cfsilent>
<cfoutput>
<cfif event.isArgDefined("message")>
	#event.getArg("message")#
</cfif>
<form action="#BuildUrl('admin.processBlockForm')#" method="post">
	<input name="block_id" type="hidden" value="#block.getBlock_id()#" />
	<input name="blockGuid" type="hidden" value="#block.getBlockGuid()#" />
	<input name="siteId" type="hidden" value="#request.siteId#" />
	Name: <input name="name" type="text" value="#block.getName()#" /><br />
	Title: <input name="title" type="text" value="#block.getTitle()#" /><br />
	Content:<br />
	<textarea name="body" style="width:600px;height:300px;">#block.getBody()#</textarea><br />
	<input type="submit" value="Save" />
</form>
</cfoutput>