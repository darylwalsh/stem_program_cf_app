<cfsilent>
	<cfset users = event.getArg("users") />
	<cfset sponsor_guid = event.getArg("sponsor_guid") />
	<cfset event.setArg("pageTitle","#request.siteName# - Registered Users") />
</cfsilent>
<cfoutput>
	<a href="#BuildUrl('admin.excelUsers','sponsor_guid=#sponsor_guid#')#">Export to Excel</a>
	<h1>#ArrayLen(users)# registered users.</h1>
<table width="100%" border="1">
	<tr>
		<th>View</th>
		<th>Last Name</th>
		<th>First Name</th>
		<th>E-mail</th>
	</tr>
<cfloop from="1" to="#ArrayLen(users)#" index="i">
	<tr>
		<td><a href="#BuildUrl('admin.showUser','guid=#users[i].getSponsor_user_guid()#')#">View</a></td>
		<td>#users[i].getLast_name()#</td>
		<td>#users[i].getFirst_name()#</td>
		<td>#users[i].getEmail()#</td>
	</tr>
</cfloop>
</table>
</cfoutput>