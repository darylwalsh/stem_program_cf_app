<cfsilent>
	<cfset c = event.getArg("c") />
	<cfset resourceId = event.getArg("resourceId") />
	<cfset fileId = application.udf.CreateGUID() />
	<cfset event.setArg("pageTitle", "#request.siteName# - Upload Resource") />
	<cfset app_user_guid = "00000000-0000-0000-0000-000000000000" />
	<cfif StructKeyExists(cookie,"app_user_guid")>
		<cfset app_user_guid = cookie.app_user_guid />
	</cfif>
</cfsilent>
<cfsavecontent variable="css">
<link href="css/swfupload.css" rel="stylesheet" type="text/css" />
</cfsavecontent>
<cfsavecontent variable="js">
<script type="text/javascript" src="js/swfupload.js"></script>
<script type="text/javascript" src="js/swfupload.queue.js"></script>
<script type="text/javascript" src="js/fileprogress.js"></script>
<script type="text/javascript" src="js/resource_handlers.js"></script>
<cfoutput>
<script type="text/javascript">
	var upload1;

	document.observe("dom:loaded", function() {
			upload1 = new SWFUpload({
				// Backend Settings
				upload_url: "/index.cfm?event=processResourceUploadForm",	// Relative to the SWF file (or you can use absolute paths)
				post_params: {
					"fileId" : "#fileId#",
					"contentId" : "#resourceId#",
					"siteId" : "#request.siteId#",
					"createdBy" : "#app_user_guid#"
				},

				// File Upload Settings
				file_size_limit : "307200",	// 300MB
				file_types : "*.*",
				file_types_description : "All Files",
				file_upload_limit : "0",
				file_queue_limit : "0",

				// Event Handler Settings (all my handlers are in the Handler.js file)
				file_dialog_start_handler : fileDialogStart,
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,

				// Button Settings
				button_image_url : "/images/add-files-btn.gif",	// Relative to the SWF file
				button_placeholder_id : "spanButtonPlaceholder1",
				button_width: 136,
				button_height: 34,
				
				// Flash Settings
				flash_url : "swf/swfupload.swf",				

				custom_settings : {
					progressTarget : "fsUploadProgress1",
					cancelButtonId : "btn-cancel-attach",
					completeId : "complete"
				},
				
				// Debug Settings
				debug: false
			});
	     });
</script>
</cfoutput>
</cfsavecontent>
<cfset event.setArg("css",css) />
<cfset event.setArg("js",js) />
<cfoutput>

<div id="content-single">

<h2>Upload STEM Resources</h2>



<div style="width:650px;margin-left:auto;margin-right:auto;">

<p>Please continue with your uploading process by selecting your asset's name and media type.</p>

	<img src="/images/bg-upload-top.gif" alt="curved edge" />
	
	<div class="upload-bg">
		<div class="fieldset flash" id="fsUploadProgress1">
			<span class="legend">Upload Status:</span>
		</div>
		<div>
			<div class="left"><span id="spanButtonPlaceholder1"></span></div>
			<a href="##" id="btn-cancel-attach" class="sprite" onclick="cancelQueue(upload1); return false;" style="display:none;"></a>
			<!---
			<input id="btnCancel1" type="button" value="Cancel Uploads" onclick="cancelQueue(upload1);" disabled="disabled" style="margin-left: 2px; height: 22px; font-size: 8pt; margin-botton: 10px;" />
			--->
			<br clear="all" />
		</div>
	</div>
	<div class="upload-bg">
	<h4 class="label">Attached Files:</h4>
	<div id="complete"></div>
	</div>
	<div class="upload-bg">
		<a href="#BuildUrl('admin.showResourceForm','resourceId=#resourceId#|c=#c#')#" id="btn-save-resource" class="sprite"></a>
	</div>
	<img src="/images/bg-upload-bottom.gif" alt="curved edge" />
</div>

</div><!---content-single--->


</cfoutput>