<cfsilent>
	<cfset event.setArg("pageTitle", "#request.siteName# - Create Content") />
	<cfset parentId = event.getArg("parentId") />
	<cfset siteId = event.getArg("siteId") />
	<cfset orderNo = event.getArg("maxOrder") + 1 />
	<cfset newId = application.udf.CreateGUID() />
	<cfsavecontent variable="js">
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
// General options
mode : "textareas",
theme : "advanced",
plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager",
extended_valid_elements : "link[type|rel|href],p[style]",
// Theme options
theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
theme_advanced_toolbar_location : "top",
theme_advanced_toolbar_align : "left",
theme_advanced_statusbar_location : "bottom",
theme_advanced_resizing : true,

// Example content CSS (should be your site CSS)
content_css : "/css/stemacademy.css",

// Drop lists for link/image/media/template dialogs
//template_external_list_url : "js/template_list.js",
//external_link_list_url : "js/link_list.js",
external_image_list_url : "js/imageList.cfm"
//media_external_list_url : "js/media_list.js",
 
// Replace values for the template plugin
//template_replace_values : {
//username : "Some User",
//staffid : "991234"
//}
});
</script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
	<div>
		Add Page:
		<form action="#BuildUrl('admin.createTcontent')#" method="post">
			<input type="hidden" name="guidParentId" value="#siteId#" />
			<input type="hidden" name="siteId" value="#siteId#" />
			<input type="hidden" name="parentId" value="#parentId#" />
			<input type="hidden" name="contentId" value="#newId#" />
			<input type="hidden" name="template" value="page.cfm" />
			<input type="hidden" name="isActive" value="1" />
			<input type="hidden" name="orderNo" value="#orderNo#" />
			<input type="hidden" name="dtCreated" value="#CreateODBCDateTime(Now())#" />
			<input type="hidden" name="createdBy" value="#cookie.app_user_guid#" />
			<input type="hidden" name="ipCreated" value="#cgi.REMOTE_ADDR#" />
		<table>
			<tr>
				<td>Content Type</td>
				<td>
					<select name="type">
						<option value="page" selected>Page</option>
						<option value="custom">Custom</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>
					<select name="approved">
						<option value="0" selected>Pending</option>
						<option value="1">Published</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Navigation?</td>
				<td>
					<select name="isNav">
						<option value="1" selected>Yes</option>
						<option value="0">No</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Title</td>
				<td><input name="title" type="text" value="" size="50" /></td>
			</tr>
			<tr>
				<td>Alias</td>
				<td><input name="alias" type="text" value="" size="50" /></td>
			</tr>
			<tr>
				<td>Navigation Title (optional)</td>
				<td><input name="menuTitle" type="text" value="" size="50" /></td>
			</tr>
		</table>
		<br />Content:
		<textarea name="body" style="width:100%;height:400px;">
		</textarea>
		<input type="submit" value="Submit" />
		</form>
	</div>
</cfoutput>