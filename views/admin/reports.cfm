<cfsilent>
	<cfset event.setArg("pageTitle","#request.siteName# Admin - Reports") />
	<cfset currentDateTime = Now() />
	<cfset currentDate = DateFormat(currentDateTime,'mm/dd/yyyy') />
	<cfif event.getArg("date") NEQ "">
		<cfset currentDate = DateFormat(event.getArg("date"),'mm/dd/yyyy') />
	</cfif>
	<cfset prevDate = currentDate - 1 />
	<cfset nextDate = currentDate + 1 />
	
	<cfset currentMonth = DatePart("m",currentDate) />
	<cfif event.getArg("month") NEQ "">
		<cfset currentMonth = event.getArg("month") />
	</cfif>
	<cfset prevMonth = dateAdd("m", -1, currentDate) />
	<cfset nextMonth = dateAdd("m", 1, currentDate) />
	
	<cfset currentYear = DatePart("yyyy", currentDate) />
	<cfif event.getArg("year") NEQ "">
		<cfset currentYear = event.getArg("year") />
	</cfif>
	<cfset prevYear = dateAdd("yyyy", -1, currentDate) />
	<cfset nextYear = dateAdd("yyyy", 1, currentDate) />
	
	<cfset period = "day" />	
	<cfif event.getArg("period") NEQ "">
		<cfset period = event.getArg("period") />
	</cfif>
<cfif period EQ "day">
	<cfquery name="daily" datasource="#getProperty('sDsn')#">
		SELECT
			contentId,
			title,
			views,
			type
		FROM	cms_clicks_aggregate
		WHERE	theDate = <cfqueryparam value="#currentDate#" />
		AND		siteId = '#request.siteId#'
		ORDER BY views DESC
	</cfquery>
<cfelseif period EQ "month">
	<cfquery name="monthly" datasource="#getProperty('sDsn')#">
		SELECT DISTINCT
			ca.contentId,
			ca.title,
			xyz.views,
			ca.type
		FROM cms_clicks_aggregate ca
		JOIN
		(SELECT
			ca2.contentId,
			SUM(ca2.views) AS views
		FROM	cms_clicks_aggregate ca2
		WHERE	DatePart(month, theDate) = <cfqueryparam value="#currentMonth#" />
		AND		DatePart(year, theDate) = <cfqueryparam value="#currentYear#" />
		AND		siteId = '#request.siteId#'
		GROUP BY ca2.contentId) as xyz
		ON ca.contentId = xyz.contentId
		ORDER BY xyz.views DESC
	</cfquery>
<cfelseif period EQ "year">
	<cfquery name="yearly" datasource="#getProperty('sDsn')#">
		SELECT DISTINCT
			ca.contentId,
			ca.title,
			xyz.views,
			ca.type
		FROM cms_clicks_aggregate ca
		JOIN
		(SELECT
			ca2.contentId,
			SUM(ca2.views) AS views
		FROM	cms_clicks_aggregate ca2
		WHERE	DatePart(year, theDate) = <cfqueryparam value="#currentYear#" />
		AND		siteId = '#request.siteId#'
		GROUP BY ca2.contentId) as xyz
		ON ca.contentId = xyz.contentId
		ORDER BY xyz.views DESC
	</cfquery>
</cfif>
</cfsilent>
<cfoutput>
<div><a href="#BuildUrl('admin.showExcelReports')#" target="_blank">Excel Export</a></div>
<div>
	<a href="#BuildUrl('admin.refreshReports')#">Update Reports</a><br />
	<cfif period EQ "day">
		<strong>Daily</strong>
	<cfelse>
		<a href="#BuildUrl('admin.showReports','period=day')#">Daily</a>
	</cfif> | 
	<cfif period EQ "month">
		<strong>Monthly</strong>
	<cfelse>
		<a href="#BuildUrl('admin.showReports','period=month')#">Monthly</a>
	</cfif> | 
	<cfif period EQ "year">
		<strong>Yearly</strong>
	<cfelse>
		<a href="#BuildUrl('admin.showReports','period=year')#">Yearly</a>
	</cfif>
	<br />
	<cfif period EQ "day">
		<a href="#BuildUrl('admin.showReports','date=#prevDate#')#">#DateFormat(prevDate)#</a> | <strong>#DateFormat(currentDate)#</strong> | <a href="#BuildUrl('admin.showReports','date=#nextDate#')#">#DateFormat(nextDate)#</a><br />
		<table border="1">
			<tr>
				<td>Title</td>
				<td>Views</td>
				<td>Type</td>
			</tr>
			<cfloop query="daily">
			<tr>
				<td>#title#</td>
				<td>#views#</td>
				<td>#type#</td>
			</tr>
			</cfloop>
		</table>
	<cfelseif period EQ "month">
		<a href="#BuildUrl('admin.showReports','period=month|date=#prevMonth#')#">#DateFormat(prevMonth, "mm/yyyy")#</a> | <strong>#DateFormat(currentDate, "mm/yyyy")#</strong> | <a href="#BuildUrl('admin.showReports','period=month|date=#nextMonth#')#">#DateFormat(nextMonth, "mm/yyyy")#</a><br />
		<table border="1">
			<tr>
				<td>Title</td>
				<td>Views</td>
				<td>Type</td>
			</tr>
			<cfloop query="monthly">
			<tr>
				<td>#title#</td>
				<td>#views#</td>
				<td>#type#</td>
			</tr>
			</cfloop>
		</table>
	<cfelseif period EQ "year">
		<a href="#BuildUrl('admin.showReports','period=year|date=#prevYear#')#">#DateFormat(prevYear, "yyyy")#</a> | <strong>#DateFormat(currentDate, "yyyy")#</strong> | <a href="#BuildUrl('admin.showReports','period=year|date=#nextYear#')#">#DateFormat(nextYear, "yyyy")#</a><br />
		<table border="1">
			<tr>
				<td>Title</td>
				<td>Views</td>
				<td>Type</td>
			</tr>
			<cfloop query="yearly">
			<tr>
				<td>#title#</td>
				<td>#views#</td>
				<td>#type#</td>
			</tr>
			</cfloop>
		</table>
	</cfif>
</div>
</cfoutput>