<cfsilent>
	<cfset resourceFile = event.getArg("file") />
	<cfset swf = "#application.env.stem_url#/swf/media_player.swf" />
	<cfset flv = "#application.env.stem_url#/assets/stem/flvs/#UCase(resourceFile.getFileId())#.mp4" />
	<cfset image = "#application.env.stem_url#/assets/stem/thumbnails/#resourceFile.getThumbnail()#" />
	<cfset event.setArg("pageTitle",resourceFile.getFilename()) />
</cfsilent>

<cfif FileExists(flv)>   	
<cfoutput>
	<cfsavecontent variable="embedCode">
<object width="400" height="300"><param name="movie" value="#swf#"></param><param name="flashvars" value="autoStart=true&amp;video=&amp;vol=&amp;rtmp=&amp;imagePath=#image#&amp;localHostURL=#flv#"><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="#swf#" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="400" height="300" flashvars="autoStart=true&amp;video=&amp;vol=&amp;rtmp=&amp;imagePath=#image#&amp;localHostURL=#flv#"></embed></object>
	</cfsavecontent>
<div style="width:480px;margin-left:auto;margin-right:auto;text-align:center;">
	<div>
		#embedCode#
	</div>
	<div style="text-align:left;">
		Embed:<br/>
		<input type="text" id="embedCode" style="width:470px;" onclick="javascript:this.focus();this.select();" value="#XMLFormat(embedCode)#" />
	</div>
</div>
</cfoutput>
<cfelse>
	<br/>
Your file is still being encoded into a viewable format.  <br/>
Please <a href="javascript:window.close()">Close</a>  this window and click view again in 2 minutes.<br/>
Thank you.<br/>
<div style="display:none;">
	<cfoutput>#flv#</cfoutput>
</div>

</cfif>