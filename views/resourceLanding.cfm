<cfsilent>
	<cfset c = event.getArg("c") />
	<cfset recentResources = event.getArg("recentResources") />
	<cfset mostViewedResources = event.getArg("mostViewedResources") />
	<cfset categories = event.getArg("categories") />
	<cfset types = event.getArg("types") />
    <cfset tab = event.getArg("tab") eq 'recent' />
    <cfset display=StructNew() />
    <cfset display[1 eq 1]='' />
    <cfset display[0 eq 1]=' style="display: none;"' />
	<cfset event.setArg("pageTitle","#request.siteName# - Resources")>
	<cfsavecontent variable="js">
<script type="text/javascript">
/**
*
*  URL encode / decode
*  http://www.webtoolkit.info/
*
**/

var Url = {

	// public method for url encoding
	encode : function (string) {
		return escape(this._utf8_encode(string));
	},

	// public method for url decoding
	decode : function (string) {
		return this._utf8_decode(unescape(string));
	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}

}
	function redirect(r) {
		window.location = r;
	}
	function ajaxSubmitForm(formName) {
		var form = $(formName);
		var r = Url.decode(form['r'].value);
		$('lb-invalid').hide();
		form.request({
		  onSuccess: function(transport) {
			var successText = String(transport.responseText);
			if (successText.indexOf('false')!=-1){
				$('lb-invalid').appear();
			}else{
				//clearWin();
				$('lbLoginForm').hide();
				$('lbLoading').show();
				//setTimeout(function(){redirect(r)},1000);
		  	}
		  }
		});
	}
</script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
<div id="content-single">
	<h2>Teacher Resources</h2>
	<p id="resource-intro"><b>Join your fellow educators by sharing your favorite lesson plans, tips, tricks, ideas, presentations, websites, videos and other STEM related resources</b> that may prove useful for others who want to expand their classroom materials. Looking to grow your own library of resources to inspire your students? Browse this intriguing and ever-broadening collection of resources already submitted by your peers. Visit the STEM resources area often, as this area will continue to expand as more and more educators upload multimedia tools each day.</p>
</div>
<div id="content-left">
	<div id="resource-lnav">
		<h4 class="sidebar-short">Browse by Grade</h4>
		<ul>
			<li><a title="3-5" href="#BuildUrl('showGrade','c=#c#|g=3,4,5')#" class="wedgelink">3-5</a></li>
			<li><a title="6-8" href="#BuildUrl('showGrade','c=#c#|g=6,7,8')#" class="wedgelink">6-8</a></li>
			<li><a title="9-12" href="#BuildUrl('showGrade','c=#c#|g=9,10,11,12')#" class="wedgelink">9-12</a></li>
		</ul>
		<h4 class="sidebar-short">Browse by Subject</h4>
		<ul>
		<cfloop index="i" from="1" to="#ArrayLen(categories)#">
			<li><a title="#categories[i].getName()#" href="#BuildUrl('showCategory','c=#c#|categoryId=#categories[i].getCategoryId()#')#" class="wedgelink">#categories[i].getName()#</a></li>
		</cfloop>
		</ul>

		<h4 class="sidebar-short">Browse by Type</h4>
		<ul>
		<cfloop index="i" from="1" to="#ArrayLen(types)#">
			<li><a title="#types[i].getName()#" href="#BuildUrl('showType','c=#c#|typeId=#types[i].getTypeId()#')#" class="wedgelink">#types[i].getName()#</a></li>
		</cfloop>
		</ul>

	<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
		<h4 class="sidebar-short">My Resources</h4>
		<ul>
			<!---<li><a title="My Resources" href="#BuildUrl('showMyResources','c=#c#|u=#cookie.app_user_guid#')#" class="wedgelink">My Resources</a></li> --->
			<li><a title="My Resources" href="#BuildUrl('showMyResources','c=#c#|u=#cookie.SPONSOR_USER_GUID#')#" class="wedgelink">My Resources</a></li>
		</ul>
	</cfif>
	</div><!---resource-lnav--->
	<div id="tab-browser">
    <div id="tabs-landing">
    	<div id="tab-overlay" style="display:none;"></div><!--//tab-overlay-->


    	<div id="tabsWrap">
        	<!--UNSELECTED TABS-->
        	<a href="##" onclick="landingTabs(this.id); return false;" id="tab-recent" class="tabs" #display['no']#><b>Recently Added STEM Resources</b></a>
        	<a href="##" onclick="landingTabs(this.id); return false;" id="tab-most" class="tabs"><b>Most Viewed STEM Resources</b></a>
        	<a href="#BuildUrl('showWebsites')#&c=37" id="tab-web" class="tabs"><b>STEM Websites</b></a>
            <!--SELECTED TABS-->
           	<div id="tab-recent-sel">
            	<!---
				<a href="#BuildUrl('showFeed','f=recent')#" target="_blank" class="tab-rss" title="Subscribe to RSS Feed"><b>Subscribe to STEM Recently Added Resources RSS Feed</b></a>
            	--->
			</div><!--//tab-recent-sel-->
           	<div id="tab-most-sel" #display['no']#>
            	<!---
				<a href="#BuildUrl('showFeed','f=popular')#" target="_blank" class="tab-rss" title="Subscribe to RSS Feed"><b>Subscribe to STEM Most Viewed Resources RSS Feed</b></a>
            	--->
			</div><!--//tab-most-sel-->
           	<div id="tab-web-sel" #display['no']#>
            	<!---
				<a href="#BuildUrl('showFeed','f=popular')#" target="_blank" class="tab-rss" title="Subscribe to RSS Feed"><b>Subscribe to STEM Websites RSS Feed</b></a>
            	--->
			</div>

        </div><!--//tabs-->
    	<div id="tab-most-con" class="tabsContent" align="center" #display['no']#>
		<cfloop index="i" from="1" to="#ArrayLen(mostViewedResources)#">
			<cfset resourceFiles = mostViewedResources[i].getFiles() />
            <div class="tabResWrap">
                <a href="#BuildUrl('showResource','resourceId=#mostViewedResources[i].getResourceId()#|c=#c#')#" class="resThumbWrap">
				<div class="resThumbCrop">
				<cfif ArrayLen(resourceFiles) GT 0>
                	<img src="/assets/stem/thumbnails/#resourceFiles[1].getThumbnail()#" alt="#mostViewedResources[i].getTitle()#" />
				<cfelse>
                	<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#mostViewedResources[i].getTitle()#"/>
				</cfif>
                </div><!--//resThumbCrop-->
                </a><!--//resWrap-->
                <div class="resTextWrap">
                	<a href="#BuildUrl('showResource','resourceId=#mostViewedResources[i].getResourceId()#|c=#c#')#" class="resTitle">#mostViewedResources[i].getTitle()#</a>
                	#application.udf.abbreviate(mostViewedResources[i].getBody(),200)#<cfif Len(mostViewedResources[i].getBody()) GT 200><a href="#BuildUrl('showResource','resourceId=#mostViewedResources[i].getResourceId()#|c=#c#')#" class="more">more</a></cfif>
                </div><!--//resTextWrap-->
                 <!---<div class="rating-wrap-static">
					<cfset p_Rating = 4.19234>
                    <cfset rating = #p_Rating# * 10>
                    #rating#
                    <div class="de-ratings nocursor" style="width:42.9268px;">
                    </div>
                </div>--->
                <!---<div class="credits">
                <span>From:</span>  psmith@cardin
                </div>--->
            </div><!--//tabResWrap-->
		</cfloop>
        </div><!--//tabs-most-content-->
        <div id="tab-recent-con" class="tabsContent" align="center">
		<cfloop index="i" from="1" to="#ArrayLen(recentResources)#">
			<cfset resourceFiles = recentResources[i].getFiles() />
            <div class="tabResWrap">
                <a href="#BuildUrl('showResource','resourceId=#recentResources[i].getResourceId()#|c=#c#')#" class="resThumbWrap">
                <div class="resThumbCrop">
				<cfif ArrayLen(resourceFiles) GT 0>
                	<img src="/assets/stem/thumbnails/#resourceFiles[1].getThumbnail()#" alt="#recentResources[i].getTitle()#" />
				<cfelse>
                	<img src="http://www.unitedstreaming.com/videos/images/search/13c250c1-1aac-4af0-aa8d-e1c8f2f98b40.jpg" alt="#recentResources[i].getTitle()#"/>
				</cfif>
                </div><!--//resThumbCrop-->
                </a><!--//resWrap-->
                <div class="resTextWrap">
                	<a href="#BuildUrl('showResource','resourceId=#recentResources[i].getResourceId()#|c=#c#')#" class="resTitle">#recentResources[i].getTitle()#</a>
                	#application.udf.abbreviate(recentResources[i].getBody(),200)#<cfif Len(recentResources[i].getBody()) GT 200><a href="#BuildUrl('showResource','resourceId=#recentResources[i].getResourceId()#|c=#c#')#" class="more">more</a></cfif>
                </div><!--//resTextWrap-->
                 <!---<div class="rating-wrap-static">
					<cfset p_Rating = 4.19234>
                    <cfset rating = #p_Rating# * 10>
                    #rating#
                    <div class="de-ratings nocursor" style="width:42.9268px;">
                    </div>
                </div>--->
                <!---<div class="credits">
                <span>From:</span>  psmith@cardin
                </div>--->
            </div><!--//tabResWrap-->
		</cfloop>
        </div><!--//tabs-recent-content-->
    	 <div id="tabs-landing-bg">
         	<!--bg image here-->
         </div><!--//tabs-landing-bg-->
    	<div id="tabs-btm">
        	<!--bg img here-->
        </div><!--//tabs-btm-->
    </div><!--//tabs-landing-->
    </div><!---tab-browser--->
		</div><!---content-left--->
  		<div id="content-right" style="padding-top:8px;">

      		<cfset downloadUrl = BuildUrl('showResourceForm','c=#c#') />
			<cfset downloadUrl = Replace(downloadUrl,"&amp;","&","ALL") />
			<cfset r = UrlEncodedFormat(downloadUrl) />
			<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
  				<a href="#BuildUrl('showResourceForm','c=#c#')#" id="btn-upload" class="sprite" title="Upload STEM Resources"></a>
			<cfelse>
  				<a onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;" href="##" id="btn-upload" class="sprite" title="Upload STEM Resources"></a>
			</cfif>
			
		<!---	<a href="/index.cfm?event=showGiveaway&c=37">
				<img src="/img/makeyourmarklink.png" style="border:none;">
          		 <span>Click Here for more stem websites and resources (image of keyboard)</span>
          		<span>SEE MORE</span> 
      		</a>--->
			
			<a href="##" onClick="deGlobalWin({path:'/includes/stemwidget.cfm',closebtn:false,background:'none'}); return false;"><img src="/images/stem_widget_btn.png" alt="Daily STEM Updates" title="Daily STEM Updates"></a>	
			<br><br>
			
      		<a href="#BuildUrl('showWebsites')#&c=37" id="tile-website-page">
          		<span>Click Here for more stem websites and resources (image of keyboard)</span>
          		<span>SEE MORE</span>
      		</a>

      		<!--- <a href="index.cfm?event=showGiveaway&c=37" id="tile-shark">
                <span>Take a bite out of learning -- be one of the first 100 to upload resources and get a free shark USB. (shark usb image)</span>
                <span>Upload Resources</span>
            </a> --->

			<h4 class="sidebar-dark"><a href="/index.cfm?event=showWebsites&c=37">Top STEM Links + Organizations</a></h4>
			<ul class="linklist" style="line-height: 18px;">
				<li><a title="Minnesota STEM Initiative" href="http://www.mn-stem.com/" target="_blank">Minnesota STEM Initiative</a></li>
				<li><a title="National Science Foundation Classroom Resources" href="http://www.nsf.gov/news/classroom/" target="_blank">National Science Foundation Classroom Resources</a></li>
				<li><a title="Pennsylvania STEM Initiative" href="http://www.pasteminitiative.org/" target="_blank">Pennsylvania STEM Initiative</a></li>
				<li><a title="STEM Connect " href="http://www.redactededucation.com/stemconnect/" target="_blank">STEM Connect </a></li>
				<li><a title="U.S. EPA Educational Resources for Teachers and Students" href="http://www.epa.gov/teachers/" target="_blank">U.S. EPA Educational Resources for Teachers and Students</a></li>
				<li><a title="Society for Science &amp; the Public" href="http://apps.societyforscience.org/science_training_programs/" target="_blank">Society for Science &amp; the Public</a></li>
				<li><a title="More Links" href="/index.cfm?event=showWebsites&c=37">Click here for more links.</a></li>


				<!---
				<li><a title="International Technology and Engineering Educators Association Teacher Resources" href="http://www.iteea.org/Resources/tewebsites.htm" target="_blank">International Technology and Engineering Educators Association Teacher Resources</a></li>
				<li><a title="http://www.nsf.gov/news/classroom/engineering.jsp" href="http://www.nsf.gov/news/classroom/engineering.jsp" target="_blank">National Science Foundation Engineering Resources</a></li>
				<li><a title="STEM Education Blog" href="http://stemeduc.blogspot.com/" target="_blank">STEM Education Blog</a></li>
				<li><a title="Teach Science and Math" href="http://www.teachscienceandmath.com/" target="_blank">Teach Science and Math</a></li>
				--->
			</ul>

		</div><!---content-right--->
</cfoutput>