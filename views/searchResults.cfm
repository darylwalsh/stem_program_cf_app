<cfsilent>
	<cfset qResources = event.getArg("searchResults") />
	<cfset criteria = event.getArg("criteria") />
	<cfset collname = event.getArg("collname") />
	<cfif event.getArg("pageIndex") EQ "">
		<cfset pageIndex = "0" />
	<cfelse>
		<cfset pageIndex = event.getArg("pageIndex") />
	</cfif>
	<cfif event.getArg("recordsPerPage") EQ "">
		<cfset recordsPerPage = "5" />
	<cfelse>
		<cfset recordsPerPage = event.getArg("recordsPerPage") />
	</cfif>
	<cfset totalPages = ceiling(qResources.RecordCount / recordsPerPage) - 1 />
	<cfset startRow = (pageIndex * recordsPerPage) + 1 />
	<cfset endRow = startRow + recordsPerPage - 1 />
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset event.setArg("pageTitle", "#request.siteName# Search - #criteria#") />
	<cfsavecontent variable="css">
<link rel="stylesheet" type="text/css" href="css/support.css" />
	</cfsavecontent>
	<cfset event.setArg("css",css) />
</cfsilent>
<cfoutput>
<div id="content-single">
	<h2>STEM Resources</h2>
	<div id="resource-lnav">
		<h4 class="sidebar-short">Browse by Grade</h4>
		<ul>
			<li><a title="3-5" href="#BuildUrl('showGrade','c=#c#|g=3,4,5')#" class="wedgelink">3-5</a></li>
			<li><a title="6-8" href="#BuildUrl('showGrade','c=#c#|g=6,7,8')#" class="wedgelink">6-8</a></li>
			<li><a title="9-12" href="#BuildUrl('showGrade','c=#c#|g=9,10,11,12')#" class="wedgelink">9-12</a></li>
		</ul>
		<h4 class="sidebar-short">Browse by Subject</h4>
		<ul>
		<cfloop index="i" from="1" to="#ArrayLen(categories)#">
			<li><a title="#categories[i].getName()#" href="#BuildUrl('showCategory','c=#c#|categoryId=#categories[i].getCategoryId()#')#" class="wedgelink">#categories[i].getName()#</a></li>
		</cfloop>
		</ul>
	<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
		<h4 class="sidebar-short">My Resources</h4>
		<ul>
			<li><a title="My Resources" href="#BuildUrl('showMyResources','c=#c#|u=#COOKIE.SPONSOR_USER_GUID#')#" class="wedgelink <cfif event.getArg("event") EQ "showMyResources">active</cfif>">My Resources</a></li>
		</ul>
	</cfif>
	</div><!---resource-lnav--->
	<div id="searched">You searched for: <b>#criteria#</b></div>
	<!--BEGIN SEARCH RESULTS UI AND OUTPUT-->
	<div id="searchContent">
		<!--holds search bars and content-->
		<div id="search">
			<!--top bar paging, items per, result count-->
			<div id="topBlock">
				<span class="res">
					Results #startRow#-<cfif endRow LTE qResources.RecordCount>#endRow#<cfelse>#qResources.RecordCount#</cfif> of #qResources.RecordCount#
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Items per Page:
					<select name="selItemsPerPage" id="selItemsPerPage" class="perPage" onChange="location=this.options[this.selectedIndex].value;" >
						<option value="#BuildCurrentUrl('recordsPerPage=5|criteria=#criteria#|collname=#collname#')#" <cfif recordsPerPage EQ 5>selected</cfif> >5</option>
						<option value="#BuildCurrentUrl('recordsPerPage=10|criteria=#criteria#|collname=#collname#')#" <cfif recordsPerPage EQ 10>selected</cfif> >10</option>
						<option value="#BuildCurrentUrl('recordsPerPage=25|criteria=#criteria#|collname=#collname#')#" <cfif recordsPerPage EQ 25>selected</cfif> >25</option>
					</select>
				</span><!--class:res-->			
				<span class="paging">					
					<cfloop index="pages" from="0" to="#totalPages#">
						<cfset displayPageNumber = pages + 1 />
						<cfif pageIndex EQ pages>
							<strong>#displayPageNumber#</strong>
						<cfelse>
							<a href="#BuildCurrentUrl('pageIndex=#pages#|criteria=#criteria#|collname=#collname#')#" class="paging_links">#displayPageNumber#</a>
						</cfif>
					</cfloop>
				</span><!--class:paging-->
			</div><!--id:topBlock-->			
			<!--Begin Results-->
			<div id="results-list">
			<cfloop query="qResources">
				<cfset hTitle = title />
				<cfloop index="i" list="#criteria#" delimiters=" ">
					<cfset hTitle = application.udf.highlight(hTitle,i) />
				</cfloop>
				<cfset hBody = application.udf.abbreviate(custom1,500) />
				<cfloop index="i" list="#criteria#" delimiters=" ">
					<cfset hbody = application.udf.highlight(hbody,i) />
				</cfloop>
				<cfset hKeywords = custom2 />
				<cfloop index="i" list="#criteria#" delimiters=" ">
					<cfset hKeywords = application.udf.highlight(hKeywords,i) />
				</cfloop>
				<cfif CurrentRow GTE startRow>
				<div class="resultBlock">
					<h3><a href="#BuildUrl('showResource','c=#c#|resourceId=#key#')#">#hTitle#</a></h3>
                    <div class="resBlockLeft">
	                    <a href="#BuildUrl('showResource','c=#c#|resourceId=#key#')#" class="resThumbWrap">
	                        <div class="resThumbCrop">
								<cfif custom4 NEQ "">
									<img src="/assets/stem/thumbnails/#custom4#" alt="#title#" /><br />
								<cfelse>
									<img src="/assets/stem/thumbnails/defaultThumb.gif" alt="title" /><br />
								</cfif>
                        	</div><!--//resThumbCrop-->
	                    </a><!--//resWrap-->
                    </div>
                    <div class="resBlockRight">                    
						#hBody#
                    </div>                    
                    <div class="clear"></div>
					<span class="subject" style="color:##0D6296;">Keywords: #hKeywords#</span>
					<span class="subject" style="color:##0D6296;">Views: #custom3#</span>
				</div>
				</cfif>
				<cfif CurrentRow EQ endRow>
					<cfbreak />
				</cfif>
			</cfloop>
			</div><!---result-list--->				
			<!--End Results-->
			<!--bottom bar paging, items per, result count-->	
			<div id="btmBlock">
				<span class="res">
					Results #startRow#-<cfif endRow LTE qResources.RecordCount>#endRow#<cfelse>#qResources.RecordCount#</cfif> of #qResources.RecordCount#
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Items per Page:
					<select name="selItemsPerPage" id="selItemsPerPage" class="perPage" onChange="location=this.options[this.selectedIndex].value;" >
						<option value="#BuildCurrentUrl('recordsPerPage=5|criteria=#criteria#|collname=#collname#')#" <cfif recordsPerPage EQ 5>selected</cfif> >5</option>
						<option value="#BuildCurrentUrl('recordsPerPage=10|criteria=#criteria#|collname=#collname#')#" <cfif recordsPerPage EQ 10>selected</cfif> >10</option>
						<option value="#BuildCurrentUrl('recordsPerPage=25|criteria=#criteria#|collname=#collname#')#" <cfif recordsPerPage EQ 25>selected</cfif> >25</option>
					</select>
				</span><!--class:res-->			
				<span class="paging">					
					<cfloop index="pages" from="0" to="#totalPages#">
						<cfset displayPageNumber = pages + 1 />
						<cfif pageIndex EQ pages>
							<strong>#displayPageNumber#</strong>
						<cfelse>
							<a href="#BuildCurrentUrl('pageIndex=#pages#|criteria=#criteria#|collname=#collname#')#" class="paging_links">#displayPageNumber#</a>
						</cfif>
					</cfloop>
				</span><!--class:paging-->
			</div><!--id:btmBlock-->
		</div><!--id:search-->
	</div><!--id:searchContent-->	
	<!--END SEARCH RESULTS UI AND OUTPUT-->
</div><!---content-single--->
</cfoutput>