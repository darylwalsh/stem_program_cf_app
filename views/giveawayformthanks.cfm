<cfif not application.cfcs.register.isSTEMUserLoggedIn(true)>
	<cflocation url="/index.cfm?event=showGiveaway" addtoken="false" />
</cfif>

<cfset event.setArg("pageTitle","#request.siteName# - Dry Erase Marker Packs") />

<div id="content-left">
	<h2>Dry Erase Marker Packs Giveaway</h2>
	<h3>Share Your Resources and Receive Free Dry Erase Marker Packs</h3>
	<br />

	<p style="margin-bottom:100px;">
		<cfif not StructKeyExists(url, "returning")>
			Thank you for submitting your resources. <br />
			If you are verified as being one of the first 100 to submit resources, we will mail your dry erase marker packs shortly.
		<cfelse>
			Your dry erase marker packs request has been received and if you are verified as being one of the first 100 to submit resources, we
			will mail your dry erase marker packs shortly.  Thank you for your participation.
		</cfif>
	</p>

</div>
<div id="content-right"><img style="padding-top: 30px;" src="img/makeyourmarklink2.png" alt="" /></div>
