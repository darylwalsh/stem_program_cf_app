<cfsilent>
	<cfset variables.exception = event.getArg("exception") />
	<!---<cfset server.tools.DumpToFile(variables.exception.getMessage(), "__EXCEPTION_ROBERT") />--->
	<cfset pageNotFound = "MachII.framework.EventHandlerNotDefined,MachII.framework.EventHandlerNotAccessible" />
	<cfif ListFindNoCase(pageNotFound,variables.exception.getCaughtException().type)>
		<cflocation addtoken="false" url="#BuildUrl('')#" />
	</cfif>
</cfsilent>
<cfoutput>
<cfsavecontent variable="errortext">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>#variables.exception.getMessage()# #cgi.server_name# #application.udf.getServerIp()#</title>
</head>
<body>
		<h3>Mach-II Exception</h3>
		<h3>Server: #application.udf.getServerIp()#</h3>
		<h3>Requested url: http://#cgi.server_name##cgi.script_name#?#cgi.query_string#</h3>
		<table>
			<tr>
				<td valign="top"><h4>Message</h4></td>
				<td valign="top"><p>#variables.exception.getMessage()#</p></td>
			</tr>
			<tr>
				<td valign="top"><h4>Detail</h4></td>
				<td valign="top"><p>#variables.exception.getDetail()#</p></td>
			</tr>
			<tr>
				<td valign="top"><h4>Extended Info</h4></td>
				<td valign="top"><p>#variables.exception.getExtendedInfo()#</p></td>
			</tr>
			<tr>
				<td valign="top"><h4>Tag Context</h4></td>
				<td valign="top">
					<cfset variables.tagCtxArr = variables.exception.getTagContext() />
					<cfloop index="i" from="1" to="#ArrayLen(variables.tagCtxArr)#">
						<cfset variables.tagCtx = variables.tagCtxArr[i] />
						<p>#variables.tagCtx['template']# (#variables.tagCtx['line']#)</p>
					</cfloop>
				</td>
			</tr>
			<tr>
				<td valign="top"><h4>Caught Exception</h4></td>
				<td valign="top"><cfdump var="#variables.exception.getCaughtException()#" /></td>
			</tr>
			<tr>
				<td valign="top"><h4>Exception Event</h4></td>
				<td valign="top">
					<cfif event.isArgDefined('exceptionEvent')>
			    		<cfdump var="#event.getArg('exceptionEvent').getArgs()#" />
					<cfelse>
		    			<cfdump var="#event.getArgs()#" />
					</cfif>
				</td>
			</tr>
		</table>
		Debug Info:<br />
		<cfdump var="#cgi#" label="cgi" />
		<cfdump var="#request#" label="request" />
		<cfdump var="#variables#" label="variables" />
</body>
</html>
</cfsavecontent>
<cfif application.strenvironmenttype EQ "prod">
	<cftry><cfmail to="daryl_walsh@redacted.com" from="daryl_walsh@redacted.com" subject="[Error] - [#cgi.server_name#] [#variables.exception.getMessage()#]" type="html">
#errortext#
	</cfmail><cfcatch></cfcatch></cftry>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Unexpected Error</title>
</head>
<body>
<div style="margin-left: auto;margin-right: auto; margin-top: 40px; border: solid 1px grey; width: 400px; height: 200px; font-size: 13px;">
	<div style="margin-left: 10px; margin-top: 15px; margin-right: 10px;">
		An unexpected error has occurred.
	</div>
	<div style="margin-left: 10px; margin-top: 15px; margin-right: 10px;">
		Please go <a href="javascript:history.go(-1);">BACK</a> to the previous page.
	</div>
	<div style="margin-left: 10px; margin-top: 15px; margin-right: 10px;">
		If you feel you have reached this page as an error,
		<a href="http://www.redactededucation.com/aboutus/contactus.cfm">please click here to contact Technical Support</a> for assistance.
	</div>
</div>
</body>
</html>
<cfelseif application.strenvironmenttype EQ "local">
	#errortext#
<cfelse>
	#errortext#
	<cftry><cfmail to="daryl_walsh@redacted.com" from="daryl_walsh@redacted.com" subject="[Error] - [#cgi.server_name#] [#variables.exception.getMessage()#]" type="html">
#errortext#
	</cfmail><cfcatch></cfcatch></cftry>
</cfif>
</cfoutput>