<cfsilent>
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset types = event.getArg("types") />
	<cfset event.setArg("pageTitle","#request.siteName# - STEM Websites")>
	<cfsavecontent variable="js">
<script type="text/javascript">
/**
*
*  URL encode / decode
*  http://www.webtoolkit.info/
*
**/

var Url = {

	// public method for url encoding
	encode : function (string) {
		return escape(this._utf8_encode(string));
	},

	// public method for url decoding
	decode : function (string) {
		return this._utf8_decode(unescape(string));
	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}

}
	function redirect(r) {
		window.location = r;
	}
	function ajaxSubmitForm(formName) {
		var form = $(formName);
		var r = Url.decode(form['r'].value);
		$('lb-invalid').hide();
		form.request({
		  onSuccess: function(transport) {
			var successText = String(transport.responseText);
			if (successText.indexOf('false')!=-1){
				$('lb-invalid').appear();
			}else{
				//clearWin();
				$('lbLoginForm').hide();
				$('lbLoading').show();
				//setTimeout(function(){redirect(r)},1000);
		  	}
		  }
		});
	}
</script>
	</cfsavecontent>
	<cfset event.setArg("js",js) />
</cfsilent>
<cfoutput>
<div id="content-single" style="width:600px; float:left; ">
	<h2>Teacher Resources <a name="anchor-top">&nbsp;</a> </h2>
	<p id="resource-intro">
		<b>Check out these top STEM websites and organizations!</b>
		<br>
		To return to the Resources library and to access additional resources, <a href="/index.cfm?event=showResourceLanding">click here</a>.
	</p>
</div>

<cfset downloadUrl = BuildUrl('showResourceForm','c=#c#') />
<cfset downloadUrl = Replace(downloadUrl,"&amp;","&","ALL") />
<cfset r = UrlEncodedFormat(downloadUrl) />
<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
		<a href="#BuildUrl('showResourceForm','c=#c#')#" id="btn-upload" style="float:right; margin:42px 80px 0 0;" class="sprite" title="Upload STEM Resources"></a>
<cfelse>
		<a onClick="deGlobalWin({path:'#BuildUrl('xhr.loginForm','r=#r#')#', closebtn:false,background:'none'}); return false;" style="float:right; margin:42px 80px 0 0;" href="##" id="btn-upload" class="sprite" title="Upload STEM Resources"></a>
</cfif>

<div class="clear"></div>

<div style="padding:10px 20px 20px 20px;">
	<div id="resource-lnav">
		<h4 class="sidebar-short">Browse by Grade</h4>
		<ul>
			<li><a title="3-5" href="#BuildUrl('showGrade','c=#c#|g=3,4,5')#" class="wedgelink">3-5</a></li>
			<li><a title="6-8" href="#BuildUrl('showGrade','c=#c#|g=6,7,8')#" class="wedgelink">6-8</a></li>
			<li><a title="9-12" href="#BuildUrl('showGrade','c=#c#|g=9,10,11,12')#" class="wedgelink">9-12</a></li>
		</ul>
		<h4 class="sidebar-short">Browse by Subject</h4>
		<ul>
		<cfloop index="i" from="1" to="#ArrayLen(categories)#">
			<li><a title="#categories[i].getName()#" href="#BuildUrl('showCategory','c=#c#|categoryId=#categories[i].getCategoryId()#')#" class="wedgelink">#categories[i].getName()#</a></li>
		</cfloop>
		</ul>

        <h4 class="sidebar-short">Browse by Type</h4>
		<ul>
		<cfloop index="i" from="1" to="#ArrayLen(types)#">
			<li><a title="#types[i].getName()#" href="#BuildUrl('showType','c=#c#|typeId=#types[i].getTypeId()#')#" class="wedgelink">#types[i].getName()#</a></li>
		</cfloop>
		</ul>
	<cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
		<h4 class="sidebar-short">My Resources</h4>
		<ul>
			<!---<li><a title="My Resources" href="#BuildUrl('showMyResources','c=#c#|u=#cookie.app_user_guid#')#" class="wedgelink">My Resources</a></li> --->
			<li><a title="My Resources" href="#BuildUrl('showMyResources','c=#c#|u=#cookie.SPONSOR_USER_GUID#')#" class="wedgelink">My Resources</a></li>
		</ul>
	</cfif>
	</div><!---resource-lnav--->
	<div>

	<div id="tab-web-con">

    	<div class="anchor-links">
        	<a href="##anchor-tools">Tools</a>
        	<span>|</span>
        	<!--- <a href="##anchor-blogs">Blogs</a>
        	<span>|</span> --->
        	<a href="##anchor-sites">Sites &amp; Initiatives</a>
        	<span>|</span>
        	<a href="##anchor-orgs">Organizations &amp; Associations</a>
    	</div>

    	<h3>Tools <a name="anchor-tools">&nbsp;</a></h3>
		
		<div class="site-entry-wrapper">
			<a href="http://www.aurasma.com/##/whats-your-aura" target="_blank">
				Aurasma
			</a>
			<div style="margin-top:2px;">
				Create fun, "augmented reality" interactive content on your iPhones, iPads and high-powered Android devices using Aurasma. Aurasma uses advanced image and pattern recognition to blend the real-world with rich content such as videos and animations called "Auras". 
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://chirp.io/" target="_blank">
				Chirp
			</a>
			<div style="margin-top:2px;">
				Chirp is an incredible new way to share information - using sound. Chirp sings information from one iPhone to another.
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://www.classdojo.com" target="_blank">
				Class Dojo
			</a>
			<div style="margin-top:2px;">
				Helps teachers improve behavior in their classrooms quickly and easily. It also captures and generates data on behavior that teachers can share with parents and administrators.
			</div>
		</div>
		
		
		<div class="site-entry-wrapper">
			<a href="https://itunes.apple.com/us/app/classroom-timer/id570172806?mt=8" target="_blank">
				Classroom Timer
			</a>
			<div style="margin-top:2px;">
				A Simple timer, designed for teachers or anyone else who needs to set timed tasks for groups of children in a classroom or similar environment. Displays an alarm clock graphic that counts down time.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.coachseye.com/" target="_blank">
				Coach's Eye
			</a>
			<div style="margin-top:2px;">
				Analyze video on your computer, iPhone or iPad with Coach's Eye.  Coach's Eye allows you to zoom, slow-motion and draw on videos to highlight small details to your students.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="https://itunes.apple.com/us/app/decide-now!/id383718755?mt=8" target="_blank">
				Decide Now!
			</a>
			<div style="margin-top:2px;">
				An app for iPhone and iPad that allows students to "spin the wheel" and the app will choose from a variety of categories that you input into the app. It can be used to add a fun element to quizzing students, by allowing them to "spin the wheel" and then answer questions on the category the wheel lands on.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://about.edmodo.com/" target="_blank">
				Edmodo
			</a>
			<div style="margin-top:2px;">
				Provides teachers and students a secure place to connect and collaborate, share content and educational applications, and access homework, grades, class discussions and notifications.
			</div>
		</div>
		
		
		
		<div class="site-entry-wrapper">
			<a href="http://www.educreations.com/" target="_blank">
				EduCreations
			</a>
			<div style="margin-top:2px;">
				Educreations is a recordable interactive whiteboard that captures your voice and handwriting to produce amazing video lessons that you can share online. Students and colleagues can replay your lessons in any web browser, or from within our app on their iPads.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="https://evernote.com/evernote/" target="_blank">
				Evernote
			</a>
			<div style="margin-top:2px;">
				Evernote makes it easy to remember things big and small using your computer, phone, tablet and the web.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="https://itunes.apple.com/us/app/free-game-show-soundboard/id377333262?mt=8" target="_blank">
				Game Show Soundboard
			</a>
			<div style="margin-top:2px;">
				An app for iPhone and iPad that produces game show sounds.  This app can make quizzing students fun, allowing you to reward correct answers with a "DING DING DING!" sound.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.google.com/educators/tools.html" target="_blank">
				Google Tools
			</a>
			<div style="margin-top:2px;">
				Google offers a wide range of free tools that educators can use to enhance their teachings including Google Maps, Earth, Book Search, SketchUp, etc.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="https://itunes.apple.com/us/app/green-screen-movie-fx/id445285983?mt=8" target="_blank">
				GreenScreen Movie Fx
			</a>
			<div style="margin-top:2px;">
				Easily create a movie with your students using your iPhone, iPad or iPod touch. GreenScreen Movie FX puts the power of a Hollywood special effects studio in your pocket (well, almost!).
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="https://www.appcertain.com/apple-guided-access-mode-parents/" target="_blank">
				Guided Access
			</a>
			<div style="margin-top:2px;">
				Guided Access helps students with disabilities such as autism remain on task and focused on content while using an iOS device, such as an iPad. It allows a parent, teacher, or administrator to limit an iOS device to one app by disabling the Home button, as well as restrict touch input on certain areas of the screen.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="https://itunes.apple.com/us/app/ibanner-hd-for-ipad-led-scrolling/id365602542?mt=8" target="_blank">
				iBanner HD for iPad
			</a>
			<div style="margin-top:2px;">
				iBanner HD produces a scrolling marquee allowing you to input data and deliver fun messages to your students on your iPad.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.icab-mobile.de/" target="_blank">
				iCab Mobile
			</a>
			<div style="margin-top:2px;">
				iCab Mobile is a web browser for the iPhone&reg;, iPod Touch&reg; and the iPad&reg;. It provides many features which makes surfing in the web much easier.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.apple.com/ilife/imovie/" target="_blank">
				iMovie
			</a>
			<div style="margin-top:2px;">
				iMovie makes it easy to turn your home videos into your all-time favorite films. You can also store and organize your videos by event, similar to photo albums.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://liveminutes.com" target="_blank">
				LiveMinutes
			</a>
			<div style="margin-top:2px;">
				Free collaborative meeting service focused on Document Sharing. When sharing, everyone can videochat, look at the same slide at the same time and annotate documents in real time.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://k12videos.mit.edu/" target="_blank">
				MIT+K12
			</a>
			<div style="margin-top:2px;">
				K12 educators and MIT partner to make educational movies. Educators can make requests and search for a variety of STEM-related videos.
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://blossoms.mit.edu/" target="_blank">
				MIT Blossoms
			</a>
			<div style="margin-top:2px;">
				Video lessons enrich students' learning experiences in high school classrooms from Brooklyn to Beirut to Bangalore. The Video Library contains over 50 math and science lessons, all freely available to teachers as streaming video and Internet downloads and as DVDs and videotapes.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.mosamack.org/" target="_blank">
				Mosa Mack: Science Detective
			</a>
			<div style="margin-top:2px;">
				Mosa Mack: Science Detective is a series of short animated science mysteries that redefines the image of a scientist. In each episode, Mosa uses critical thinking skills to solve each mystery and shows students that scientific thinking is a part of their everyday lives.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.nearpod.com/" target="_blank">
				NearPod
			</a>
			<div style="margin-top:2px;">
				This free app for iOS devices allow teachers to create digital presentations and digitally interact with their students in real time. 
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://buzzers.com/products/pickme-buzzer-mobile" target="_blank">
				PickMe!Buzzer
			</a>
			<div style="margin-top:2px;">
				This app allows you and your students to use your iOS devices to operate as buzzers. Your students can use their iPhone or iPad as a remote to "buzz in" to answer questions, similar to Jeopardy.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.pulseplanet.com/" target="_blank">
				Pulse of the Planet
			</a>
			<div style="margin-top:2px;">
				Each weekday, this radio series provides listeners with a two-minute sound portrait of Planet Earth, tracking the rhythms of nature, culture and science worldwide, blending interviews with extraordinary natural sound.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="https://itunes.apple.com/us/app/puppet-pals-hd/id342076546?mt=8" target="_blank">
				Puppet Pals HD
			</a>
			<div style="margin-top:2px;">
				Create your own unique shows with animation and audio in real time on your iPad! Simply pick out your puppet actors and backdrops, drag them on to the stage, and tap record. Your movements and audio will be recorded in real time for playback later.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.socrative.com/" target="_blank">
				Socrative
			</a>
			<div style="margin-top:2px;">
				Socrative allows teachers to digitally play games and ask questions to their students in real time using tablets, smart phones and laptops. The teacher controls the flow of a variety of questions and games available on Socrative.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.sonicpics.com/public/index.php" target="_blank">
				SonicPics
			</a>
			<div style="margin-top:2px;">
				Create and record custom photo slideshows on your iPhone, iPod Touch or iPad and share them with your students.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.mexircus.com/Strip_Designer/" target="_blank">
				Strip Designer
			</a>
			<div style="margin-top:2px;">
				With Strip Designer you can create your own personal comic strip right on your iPhone or iPad.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.symbaloo.com/login.form" target="_blank">
				Symbaloo
			</a>
			<div style="margin-top:2px;">
				A unique visual bookmarking tool that helps users keep their favorite links in order.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://teacherkit.net/" target="_blank">
				TeacherKit
			</a>
			<div style="margin-top:2px;">
				An app for iPhone, iPad & iPod touch, TeacherKit is a personal organizer for the teacher. It enables the teacher to organize classes, and students. It's simple and intuitive interface enables teachers to track the attendance, grades and behavior of students.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="https://itunes.apple.com/us/app/too-noisy/id499844023?mt=8" target="_blank">
				Too Noisy
			</a>
			<div style="margin-top:2px;">
				An app for iPhone and iPad that is a noise level meter built for the classroom. The app has been designed to assist a teacher to keep control of general noise levels in a classroom using a visual stimulus.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.wikispaces.com/content/teacher" target="_blank">
				Wikispaces
			</a>
			<div style="margin-top:2px;">
				Wikispaces makes managing class a breeze, with tools to handle day-to-day work and features to tackle the special activities. Educators can get free, private, secure space for their class.
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://www.wolframalpha.com/about.html" target="_blank">
				Wolfram Alpha
			</a>
			<div style="margin-top:2px;">
				Introduces a fundamentally new way to get knowledge and answers-not by searching the web, but by doing dynamic computations based on a vast collection of built-in data, algorithms, and methods.
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://www.wordle.net/" target="_blank">
				Wordle
			</a>
			<div style="margin-top:2px;">
				Tool used for generating "word clouds" which give greater prominence to words that appear more frequently in the source text. Educators can tweak their clouds with different fonts, layouts, and color schemes for their classroom.
			</div>
		</div>

		<a class="back-to-top" href="##anchor-top">Back to Top</a>



    	<!--- <h3>Blogs <a name="anchor-blogs">&nbsp;</a></h3>

		<div class="site-entry-wrapper">
			<a href="http://www.bestcollegesonline.com/blog/2011/12/14/50-essential-twitter-feeds-for-stem-educators/" target="_blank">
				50 Essential Twitter Feeds for STEM Educators
			</a>
			<div style="margin-top:2px;">
				50 of the greatest STEM-related feeds that inspire and build a love of all things math and science.
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://blog.redactededucation.com/" target="_blank">
				redacted Education Blog
			</a>
			<div style="margin-top:2px;">
				Blog site of redacted Education with the latest STEM tools, tips and event information.
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://stemeduc.blogspot.com/" target="_blank">
				STEM Education Blog
			</a>
			<div style="margin-top:2px;">
				Blog site of Joe Marencik designed to disseminate curriculum ideas, projects, problems and thoughts about STEM education in grades K-12.
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://www.teachscienceandmath.com/" target="_blank">
				Teach Science and Math
			</a>
			<div style="margin-top:2px;">
				Blog site of David R. Wetzel, Ph.D., that provides resources and tools for integrating STEM into the classroom.
			</div>
		</div>

		<a class="back-to-top" href="##anchor-top">Back to Top</a> --->



		<h3>Sites &amp; Initiatives <a name="anchor-sites">&nbsp;</a></h3>
<div class="site-entry-wrapper">
			<a href="http://cainesarcade.com/" target="_blank">
				Caine's Arcade
			</a>
			<div style="margin-top:2px;">
				Caine's Arcade is a short film about a 9 year old boy's cardboard arcade, located in his dad's used auto parts store in East LA. An amazing use of engineering and imagination!
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.c-spanclassroom.org/" target="_blank">
				C-SPAN
			</a>
			<div style="margin-top:2px;">
				Enhances the teaching of social studies through C-SPAN's primary source programming and websites.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.wired.com/magazine/2011/05/ff_jobsessay" target="_blank">
				The Economic Rebound: It Isn't What You Think
			</a>
			<div style="margin-top:2px;">
				An article from Wired magazine describing the emergence of a new sector of "smart" jobs, focusing on computer engineering. 
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.gearthblog.com/" target="_blank">
				Google Earth Blog
			</a>
			<div style="margin-top:2px;">
				Google Earth Blog is dedicated to sharing the best news, interesting sights, technology, and happenings for Google Earth.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.iteea.org/Resources/tewebsites.htm" target="_blank">
				International Technology and Engineering Educators Association Teacher Resources
			</a>
			<div style="margin-top:2px;">
				Professional organization for technology, innovation, design and engineering educators; features a wide range of resources and links to teacher resources sites.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.mn-stem.com/" target="_blank">
				Minnesota STEM Initiative
			</a>
			<div style="margin-top:2px;">
				Statewide initiative that provides resources, links, interactives and more to encourage students to explore STEM topics and teachers to inspire students.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.mn-stem.com/" target="_blank">
				mn-stem.com
			</a>
			<div style="margin-top:2px;">
				An education website to help students redacted firsthand how their participation in certain STEM coursework can lead directly to exciting and rewarding careers.
			</div>
		</div>		
		

		
		<div class="site-entry-wrapper">
			<a href="http://nacase.org/" target="_blank">
				National Center for the Advancement of STEM Education
			</a>
			<div style="margin-top:2px;">
				Provides resources to schools, teachers and students to increase the interest and engagement of young people in STEM.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.nctm.org" target="_blank">
				National Council of Teachers of Mathematics
			</a>
			<div style="margin-top:2px;">
				The National Council of Teachers of Mathematics is a public voice of mathematics education, supporting teachers to ensure equitable mathematics learning of the highest quality for all students through vision, leadership, professional development, and research
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.eweek.org/Home.aspx" target="_blank">
				National Engineers Week 2013
			</a>
			<div style="margin-top:2px;">
				Engineers Week celebrates the positive contributions engineers make to society and is a catalyst for outreach across the country to kids and adults alike.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.nationalmathandscience.org" target="_blank">
				National Math and Science Initiative:  Designing for Scale through Replication of Existing Programs (NMSI)
			</a>
			<div style="margin-top:2px;">
				Identifies proven projects related to math and science with the potential for wide-spread success, particularly those focused on producing strong math and science teachers, strengthening existing teachers' skills, and expanding the pipeline of STEM capable students.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.nsdl.org" target="_blank">
				National Science Digital Library
			</a>
			<div style="margin-top:2px;">
				NSDL is the National Science Foundation's online library of resources and collections for science, technology, engineering, and mathematics education and research.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.nsf.gov/news/classroom/" target="_blank">
				National Science Foundation Classroom Resources
			</a>
			<div style="margin-top:2px;">
				A diverse collection of lessons and web resources for classroom teachers, students, and families from the National Science Digital Library.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.nsf.gov/news/classroom/engineering.jsp" target="_blank">
				National Science Foundation Engineering Resources
			</a>
			<div style="margin-top:2px;">
				Lessons and web resources aimed at classroom teachers, students and families to promote interest in engineering.
			</div>
		</div>
		
				<div class="site-entry-wrapper">
			<a href="http://www.nsta.org" target="_blank">
				National Science Teachers Associate (NSTA)
			</a>
			<div style="margin-top:2px;">
				Professional organization committed to science education and promoting excellence and innovation in science
				teaching with thousands of products and services available to science teachers nationwide.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.n-stem.org" target="_blank">
				National Stem Foundation
			</a>
			<div style="margin-top:2px;">
				The National Stem Foundation is committed to promoting STEM education by serving educational institutions and organizations.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.microsoft.com/en-us/news/presskits/citizenship/docs/STEM-IG.pdf" target="_blank">
				Our Future Demands - STEM
			</a>
			<div style="margin-top:2px;">
				An article by Microsoft promoting the education of STEM. The article illustrates that, over the next decade, there will be a signi?cant shortage of quali?ed college graduates to ?ll STEM careers in the United States.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.pasteminitiative.org/" target="_blank">
				Pennsylvania STEM Initiative
			</a>
			<div style="margin-top:2px;">
				Statewide effort designed to establish a network of partners and programs that support the development and deployment of STEM education and workforce development.
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://www.ssma.org" target="_blank">
				School Science and Mathematics Association (SSMA)
			</a>
			<div style="margin-top:2px;">
				An inclusive professional community of researchers and teachers who promote research, scholarship, and practice that improves school science and mathematics and advances the integration of science and mathematics
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://www.sciencenetlinks.com" target="_blank">
				Science Netlinks
			</a>
			<div style="margin-top:2px;">
				Providing a wealth of resources for k-12 science educators, Science NetLinks of the American Association for the Advancement of Science provides a meaningful standards-based Internet experiences for students.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.stemedcoalition.org" target="_blank">
				Science, Technology, Engineering and Mathematics (STEM) Education Coalition
			</a>
			<div style="margin-top:2px;">
				The STEM Education Coalition works aggressively to raise awareness in Congress, the Administration, and other organizations about the critical role that STEM education plays in enabling the U.S. to remain the economic and technological leader of the global marketplace of the 21st century.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://screencastle.com/" target="_blank">
				ScreenCastle
			</a>
			<div style="margin-top:2px;">
				A free site that allows you to create and save screencasts to use in your class. A screencast is a digital recording of a computer screen.
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://apps.societyforscience.org/science_training_programs/" target="_blank">
				Society for Science & the Public: Student and Teacher Program Index
			</a>
			<div style="margin-top:2px;">
				With more than 300 programs listed, this site offers a comprehensive catalog of science, mathematics and engineering enrichment programs for pre-college students and teachers.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://eeweek.org/pdf/Infographic_STEM.pdf" target="_blank">
				STEM and Our Planet
			</a>
			<div style="margin-top:2px;">
				The environment is a compelling context for teaching and engaging our students in science, technology, engineering and math (STEM).
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.redactededucation.com/stemconnect/" target="_blank">
				STEM Connect
			</a>
			<div style="margin-top:2px;">
				Real-world STEM curriculum and career development resources from redacted Education highlighting the connections among science, technology, engineering and math.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://stemconnector.org/" target="_blank">
				Stemconnector.org
			</a>
			<div style="margin-top:2px;">
				With more than 5000 STEM stakeholders' profiles, its purpose is to map the STEM Education activity of organizations and all states.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://awesome.good.is/transparency/web/1107/stem-education/flat.html" target="_blank">
				The STEM Dilemma
			</a>
			<div style="margin-top:2px;">
				Promoting STEM subjects is a national priority to keep America competitive in the 21st century. This infographic displays the dilemma.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="https://www.facebook.com/STEMSchools" target="_blank">
				STEM Education on Facebook
			</a>
			<div style="margin-top:2px;">
				An online Facebook community for the sharing of STEM school resources for educators, parents, and students.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://stem4all.edc.org/" target="_blank">
				STEM for ALL
			</a>
			<div style="margin-top:2px;">
				A project dedicated to expanding the integration of science, technology, engineering, and math (STEM) content into out-of-school time programs across the country.
			</div>
		</div>
		
		
		<div class="site-entry-wrapper">
			<a href="http://si.edu/Educators" target="_blank">
				Smithsonian
			</a>
			<div style="margin-top:2px;">
				Serves as a laboratory to create models and methods of innovative informal education and link them to the formal education system.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.redactededucation.com/STEMConnect/" target="_blank">
				Teacher Center - STEM Connect
			</a>
			<div style="margin-top:2px;">
				STEM Connect is the captivating real-world STEM curriculum and career development resource from redacted Education.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.teachersfirst.com" target="_blank">
				Teachers First
			</a>
			<div style="margin-top:2px;">
				Serves as a laboratory to create models and methods of innovative informal education and link them to the formal education system.
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://www.thinkfinity.org" target="_blank">
				Thinkfinity
			</a>
			<div style="margin-top:2px;">
				Offers lesson plans, activities and interactives to help educators build a cross-curricular classroom.
			</div>
		</div>
		<div class="site-entry-wrapper">
			<a href="http://www.epa.gov/teachers/" target="_blank">
				U.S. EPA Educational Resources for Teachers and Students
			</a>
			<div style="margin-top:2px;">
				Collection of information, resources and publications, and links to awards, grants, workshops, conferences and other programs.
			</div>
		</div>


		<a class="back-to-top" href="##anchor-top">Back to Top</a>



		<h3 style="margin-top:20px;">Organizations &amp; Associations <a name="anchor-orgs">&nbsp;</a></h3>

		<div class="site-entry-wrapper">
			<a href="http://www.asis.org/" target="_blank">
    			Information Society for the Information Age
			</a>
			<div style="margin-top:2px;">
    			Society for information professionals leading the search for new and better theories, techniques, and technologies to improve access to information.
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://www.iteaconnect.org/" target="_blank">
    			International Technology and Engineering Educators Association
			</a>
			<div style="margin-top:2px;">
    			Professional organization for technology, innovation, design, and engineering educators.  Mission is to promote technological literacy for all by supporting the teaching of technology and engineering and promoting the professionalism of those engaged in these pursuits
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://www.nsf.gov/" target="_blank">
    			National Science Foundation
			</a>
			<div style="margin-top:2px;">
    			Independent federal agency created by Congress in 1950 "to promote the progress of science; to advance the national health, prosperity, and welfare; to secure the national defense."
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://www.nctm.org/" target="_blank">
    			National Council of Teachers of Mathematics
			</a>
			<div style="margin-top:2px;">
    			Public voice of mathematics education supporting teachers to ensure equitable mathematics learning of the highest quality for all students through vision, leadership, professional development and research.
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://www.nsta.org/" target="_blank">
    			National Science Teachers Association
			</a>
			<div style="margin-top:2px;">
    			Member-driven organization that publishes books and journals for science teachers from kindergarten through college. Additionally, NSTA provides ways for science teachers to connect with one another, while informing Congress and the public on vital questions affecting science literacy and a well-educated workforce
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://www.orau.org" target="_blank">
    			Oak Ridge Associated Universities
			</a>
			<div style="margin-top:2px;">
    			University consortium with a strategic partnership with Oak Ridge National Laboratory (ORNL), that brings together university faculty and students to collaborate on major scientific initiatives that help keep America on the leading edge of science and technology.
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://www.siemens-foundation.org" target="_blank">
    			Siemens Foundation
			</a>
			<div style="margin-top:2px;">
    			The Siemens Foundation provides more than $7 million annually in support of educational initiatives in the areas of science, technology, engineering and math in the United States. Additionally, Siemens supports outstanding students and recognizes the teachers and schools that inspire their excellence. The Foundation helps nurture tomorrow's scientists and engineers. The Foundation's mission is based on the culture of innovation, research and educational support that is the hallmark of Siemens' U.S. companies and its parent company, Siemens AG.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://www.mnh.si.edu/" target="_blank">
    			Smithsonian National Museum of Natural History
			</a>
			<div style="margin-top:2px;">
    			The Museum is dedicated to inspiring curiosity, redacted, and learning about the natural world through its unparalleled research, collections, exhibitions, and education outreach programs.
			</div>
		</div>
		
		<div class="site-entry-wrapper">
			<a href="http://k12s.phast.umass.edu/stem/stem.html" target="_blank">
    			STEM Ed
			</a>
			<div style="margin-top:2px;">
    			STEM Ed is the STEM Education Institute at the University of Massachusetts Amherst. It was founded in 1995; its predecessors began in 1986 to work to improve STEM education in K12 and higher education.
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://www.collegeboard.org" target="_blank">
    			The College Board
			</a>
			<div style="margin-top:2px;">
    			A not-for-profit membership organization committed to excellence and equity in education. Our mission is to connect students to college success and opportunity.
			</div>
		</div>

		<div class="site-entry-wrapper">
			<a href="http://epa.gov/" target="_blank">
    			U.S. Environmental Protection Agency
			</a>
			<div style="margin-top:2px;">
    			An agency that houses a variety of federal research, monitoring, standard-setting and enforcement activities to ensure environmental protection. Since its inception, EPA has been working for a cleaner, healthier environment for the American people.
			</div>
		</div>

		<a class="back-to-top" href="##anchor-top">Back to Top</a>

    </div><!--//tabs-web-content-->


    <div class="clear"></div>
</div><!---content-left--->

</cfoutput>