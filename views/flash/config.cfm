<cfsilent>
	<cfquery name="mostRecent" datasource="AllProductAssets" maxrows="5">
		SELECT
			guidAssetMetaDataId,
			title,
			title_description
		FROM	cms_content c
		WHERE	status = '8'
		AND c.siteId = '#request.siteId#'
		ORDER BY c.create_date DESC
	</cfquery>
	<cfquery name="topIssues" datasource="AllProductAssets" maxrows="5" cachedWithin="#CreateTimeSpan(1,0,0,0)#">
		SELECT
			c.guidAssetMetaDataId,
			title,
			title_description,
			views
		FROM	cms_content c
		JOIN	(SELECT 
					guidassetmetadataid,
					SUM(views) AS views
				FROM cms_contentClicks_aggregate
				WHERE theDate >= DATEADD(DAY,DATEDIFF(DAY,0,GetDate())- 7,0)
				AND		siteId = '#request.siteId#'
				GROUP BY guidassetmetadataid
				) ccl
		ON	c.guidAssetMetaDataId = ccl.guidassetmetadataid
		WHERE	status = '8'
		AND		isOpen = 1
		AND c.siteId = '#request.siteId#'
		ORDER BY views DESC
	</cfquery>
	<cfquery name="topGuides" datasource="AllProductAssets" maxrows="5">
		SELECT
			guidAssetMetaDataId,
			title,
			title_description,
			intOrder
		FROM	cms_content c
		JOIN	cms_contentFeatured cf
		ON	c.guidAssetMetaDataId = cf.guidAssetId
		WHERE	status = '8'
		AND c.siteId = '#request.siteId#'
		ORDER BY cf.intOrder ASC
	</cfquery>
</cfsilent>
<cfcontent type="text/xml" reset="true">
<cfoutput>
<HELP>
	<Tab title="Featured">
		<cfloop query="topGuides">
		<item desc="#xmlFormat(trim(application.udf.abbreviate(title_description,290)))#" subject="" guid="#guidAssetMetaDataId#">#xmlFormat(application.udf.abbreviate(title,60))#</item>
		</cfloop>
	</Tab>
	<Tab title="Most Viewed">
		<cfloop query="topIssues">
		<item desc="#xmlFormat(trim(application.udf.abbreviate(title_description,290)))#" subject="" guid="#guidAssetMetaDataId#">#xmlFormat(application.udf.abbreviate(title,60))#</item>
		</cfloop>
	</Tab>
	<Tab title="Most Recent">
		<cfloop query="mostRecent">
		<item desc="#xmlFormat(trim(application.udf.abbreviate(title_description,290)))#" subject="" guid="#guidAssetMetaDataId#">#xmlFormat(application.udf.abbreviate(title,60))#</item>
		</cfloop>
	</Tab>
</HELP>
</cfoutput>