<cfsilent>
	<cfparam name="form.criteria" default="" type="string">
	<!---<cfquery name="suggestions" datasource="AllProductAssets" maxRows="10" cachedwithin="#CreateTimeSpan(1,0,0,0)#">
		SELECT
			LOWER(terms) as terms,
			COUNT(1) as numSearches
		FROM searches
		WHERE LOWER(terms) LIKE '#LCase(form.criteria)#%'
		GROUP BY LOWER(terms)
		ORDER BY numSearches DESC
	</cfquery>--->
	<cfquery name="suggestions1" datasource="AllProductAssets" maxrows="10" cachedwithin="#CreateTimeSpan(1,0,0,0)#">
		SELECT
			guidAssetMetaDataId,
			title
		FROM content
		WHERE LOWER(title) LIKE '%#LCase(form.criteria)#%'
		AND siteId = '#request.siteId#'
		ORDER BY copyright DESC
	</cfquery>
</cfsilent>
<ul>
<cfoutput query="suggestions1">
  <li>#application.udf.maxLength(suggestions1.title,45)#</li>
</cfoutput>
</ul>