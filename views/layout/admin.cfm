<cfsilent>
	<cfset content = event.getArg("layout.content") />
</cfsilent>
<style type="text/css">
#admintabs ul {
	margin-left: 0;
	padding-left: 0;
	display: inline;
	font-size: 0.80em;
	} 

#admintabs ul li {
	margin-left: 0;
	margin-bottom: 0;
	padding: 2px 15px 5px;
	border: 1px solid #000;
	list-style: none;
	display: inline;
	background: white;
	}
	
		
#amdintabs ul li.here {
	border-bottom: 1px solid #ffc;
	list-style: none;
	display: inline;
	}
</style>
<cfoutput>
<div id="admintabs">
	<ul>
		<li><a id="la002" href="#BuildUrl('admin.showHome')#">Admin Home</a></li>
		<li><a id="la000" href="#BuildUrl('admin.showSite','siteId=#request.siteId#')#">Site Manager</a></li>
		<li><a id="la001" href="#BuildUrl('admin.editWebsite','siteId=#request.siteId#')#">Site Members</a></li>
		<li><a id="la201" href="#BuildUrl('admin.showFileManager')#">File Manager</a></li>
		<li><a id="la202" href="#BuildUrl('admin.showCategories')#">Categories</a></li>
		<li><a id="la200" href="#BuildUrl('admin.showAllAssets')#">Resources</a></li>
		<li><a id="la300" href="#BuildUrl('admin.refreshSearchIndex','collectionName=#request.siteId#')#">Update Search</a></li>
		<li><a id="la400" href="#BuildUrl('admin.showReports')#">Reports</a></li>
		<li><a id="la500" href="#BuildUrl('admin.showSearchReports')#">Search Reports</a></li>
		<li><a id="la600" href="#BuildUrl('admin.showRatingReports')#">Rating Reports</a></li>
		<li><a id="la700" href="#BuildUrl('admin.showComments')#">Comments</a></li>
		<li><a id="la800" href="#BuildUrl('admin.showBlocks')#">Blocks</a></li>
	</ul>
</div>
<div style="padding:10px;">
	#content#
</div>
</cfoutput>