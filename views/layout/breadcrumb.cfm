<cfsilent>
	<cfset isSearch = false />
	<cfset isNav = false />
	<cfset isAsset = false />
	<cfset asset = event.getArg("asset") />
	<cfset taxonomy = event.getArg("taxonomy") />
	<cfset criteria = event.getArg("criteria") />
	<cfset guidTaxId = event.getArg("guidTaxId") />
	<cfset collname = "#request.siteId#_public" />
	<cfif StructKeyExists(cookie,"app_user_guid") and Len(trim(cookie.app_user_guid)) EQ 36 AND cookie.app_user_guid NEQ "00000000-0000-0000-0000-000000000000">
		<cfset collname = "#request.siteId#" />
	</cfif>
	<cfif criteria NEQ "">
		<cfset isSearch = true />
	</cfif>
	<cfif guidTaxId NEQ "">
		<cfset isNav = true />
	</cfif>
	<cfif IsObject(asset)>
		<cfset isAsset = true />
	</cfif>
</cfsilent>
<cfoutput>
	<cfif isSearch EQ true AND isAsset EQ true>
		<span class="breadcrumbs"><a href="#BuildUrl('showHome')#"><b>#request.siteName#</b></a> &gt; <a href="#BuildUrl('showSearchResults','collname=#collname#|criteria=#criteria#')#">Search: <i>#criteria#</i></a> &gt; #asset.getTitle()#</span>
	<cfelseif isSearch EQ true AND isAsset EQ false>
		<span class="breadcrumbs"><a href="#BuildUrl('showHome')#"><b>#request.siteName#</b></a> &gt; <a href="#BuildUrl('showSearchResults','collname=#collname#|criteria=#criteria#')#">Search: <i>#criteria#</i></a> &gt;</span>
	<cfelseif isNav EQ true AND isAsset EQ true>
		<cfif taxonomy.getLink() EQ "">
			<span class="breadcrumbs"><a href="#BuildUrl('showHome')#"><b>#request.siteName#</b></a> &gt; <a href="#BuildUrl('showNode','guidTaxId=#guidTaxId#')#">#application.udf.getParentName(taxonomy.getGuidParentId())# &gt; #taxonomy.getName()#</a> &gt; #asset.getTitle()#</span>
		<cfelse>
			<span class="breadcrumbs"><a href="#BuildUrl('showHome')#"><b>#request.siteName#</b></a> &gt; <a href="#taxonomy.getLink()#">#application.udf.getParentName(taxonomy.getGuidParentId())# &gt; #taxonomy.getName()#</a> &gt; #asset.getTitle()#</span>
		</cfif>
	<cfelseif isNav EQ true AND isAsset EQ false AND taxonomy.getGuidTaxId() NEQ "c24b4d49-e957-ec82-bd74-3831e99c4399">
				<span class="breadcrumbs"><a href="#BuildUrl('showHome')#"><b>#request.siteName#</b></a> &gt; <a href="#BuildUrl('showNode','guidTaxId=#guidTaxId#')#">#application.udf.getParentName(taxonomy.getGuidParentId())# &gt; #taxonomy.getName()#</a> &gt;</span>
	<cfelseif isNav EQ false AND isAsset EQ true>
		<span class="breadcrumbs"><a href="#BuildUrl('showHome')#"><b>#request.siteName#</b></a> &gt; #asset.getTitle()#</span>
	<cfelse>
		<span class="breadcrumbs"><a href="#BuildUrl('showHome')#"><b>#request.siteName#</b></a> &gt;</span>
	</cfif>
</cfoutput>