<cfoutput><cfcontent reset="true"><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>#event.getArg("pageTitle")#</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
#event.getArg("css")#
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.1.0/prototype.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js"></script>
#event.getArg("js")#
<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-12418696-1']);
_gaq.push(['_setDomainName', 'redactededucation.com']);
_gaq.push(['_setAllowLinker', true]);
_gaq.push(['_trackPageview']);

(function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })();

</script>
<script type="text/javascript">
if (typeof jQuery != 'undefined') {
jQuery(document).ready(function($) {
var filetypes = /\.(zip|exe|pdf|doc*|xls*|ppt*|mp3|mp4|flv|f4v|mov|wmv|avi)$/i;
var baseHref = '';
if (jQuery('base').attr('href') != undefined)
baseHref = jQuery('base').attr('href');
jQuery('a').each(function() {
var href = jQuery(this).attr('href');
if (href && (href.match(/^https?\:/i)) && (!href.match(document.domain))) {
jQuery(this).click(function() {
var extLink = href.replace(/^https?\:\/\//i, '');
_gaq.push(['_trackEvent', 'External', 'Click', extLink]);
if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
setTimeout(function() { location.href = href; }, 200);
return false;
}
});
}
else if (href && href.match(/^mailto\:/i)) {
jQuery(this).click(function() { var mailLink = href.replace(/^mailto\:/i, ''); _gaq.push(['_trackEvent', 'Email', 'Click', mailLink]); });
}
else if (href && href.match(filetypes)) {
jQuery(this).click(function() {
var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
var filePath = href;
_gaq.push(['_trackEvent', 'Download', 'Click-' + extension, filePath]);
if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
setTimeout(function() { location.href = baseHref + href; }, 200);
return false;
}
});
}
});
});
}
</script>
</head>
<body>
<div>#event.getArg("layout.content")#</div>
<cfif StructKeyExists(url,"debug")>
<div id="debug">
	Debug Info:<br />
	<cfdump var="#event.getArgs()#" expand="false" label="eventArgs" />
	<cfdump var="#cgi#" expand="false" label="cgi" />
	<cfdump var="#cookie#" expand="false" label="cookie" />
	<cfdump var="#client#" expand="false" label="client" />
	<cfdump var="#request#" expand="false" label="request" />
	<cfdump var="#session#" expand="false" label="session" />
	<cfdump var="#variables#" expand="false" label="variables" />
	<cfdump var="#application#" expand="false" label="application" />
</div>
</cfif>
</body>
</html>
</cfoutput>