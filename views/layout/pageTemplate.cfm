<cfcontent reset="true"><cfoutput><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Siemens STEM Academy</title>
	<META NAME="DESCRIPTION" CONTENT="The nation's first online community designed exclusively to foster achievement in science, technology, engineering and math through the collaboration of STEM educators and the sharing of "best practices."">
	<META NAME="KEYWORDS" CONTENT="Science, technology, engineering, math, STEM, STEM Education">
	<link href="css/stemacademy.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		function IsEmail(object_value, required)
		{
			object_value = object_value.replace(/^\s+/,'').replace(/\s+$/,'');

			if(required) {
				if(object_value.length == 0) {
					return false;
				}
			} else {
				if( object_value.length == 0 ) {
					return true;
				}
			}
			return IsValidRegex(object_value, /^[a-zA-Z_0-9-]+(\.[a-zA-Z_0-9-]+)*@([a-zA-Z_0-9-]+\.)+[a-zA-Z]{2,7}$/, required);
		}

		function IsValidRegex(object_value, regexPattern, required)
		{
			if(required) {
				if(object_value.length == 0) {
					return false;
				}
			} else {
				if(object_value.length == 0) {
					return true;
				}
			}

			return regexPattern.test(object_value);
		}

		function Trim(astring)
		{
			return astring.replace(/^\s*/, "").replace(/\s*$/, ""); //Trim the start and end of string;
		}

		function ValidateForm ()
		{
			var signupForm = document.getElementById("signupForm");

			if (Trim(signupForm.name.value) == "") {
				alert("Please enter YOUR NAME to complete sign up.");
				return false;
			}
			else if (signupForm.title.selectedIndex == 0) {
				alert("Please enter YOUR TITLE to complete sign up.");
				return false;
			}
			else if (Trim(signupForm.email.value) == "" || !IsEmail(signupForm.email.value)) {
				alert("Please enter YOUR EMAIL in a valid email format to complete sign up.");
				return false;
			}
			else {
				signupForm.submit();
			}
		}
	</script>
<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-12418696-1']);
_gaq.push(['_setDomainName', 'redactededucation.com']);
_gaq.push(['_setAllowLinker', true]);
_gaq.push(['_trackPageview']);

(function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })();

</script>
<script type="text/javascript">
if (typeof jQuery != 'undefined') {
jQuery(document).ready(function($) {
var filetypes = /\.(zip|exe|pdf|doc*|xls*|ppt*|mp3|mp4|flv|f4v|mov|wmv|avi)$/i;
var baseHref = '';
if (jQuery('base').attr('href') != undefined)
baseHref = jQuery('base').attr('href');
jQuery('a').each(function() {
var href = jQuery(this).attr('href');
if (href && (href.match(/^https?\:/i)) && (!href.match(document.domain))) {
jQuery(this).click(function() {
var extLink = href.replace(/^https?\:\/\//i, '');
_gaq.push(['_trackEvent', 'External', 'Click', extLink]);
if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
setTimeout(function() { location.href = href; }, 200);
return false;
}
});
}
else if (href && href.match(/^mailto\:/i)) {
jQuery(this).click(function() { var mailLink = href.replace(/^mailto\:/i, ''); _gaq.push(['_trackEvent', 'Email', 'Click', mailLink]); });
}
else if (href && href.match(filetypes)) {
jQuery(this).click(function() {
var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
var filePath = href;
_gaq.push(['_trackEvent', 'Download', 'Click-' + extension, filePath]);
if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
setTimeout(function() { location.href = baseHref + href; }, 200);
return false;
}
});
}
});
});
}
</script>
</head>
#event.getArg("layout.header")#
<cfif not findNoCase('redactededucation',cgi.http_host)>
	<style type="text/css">
		##shell {display:none;}
	</style>
    <br />
</cfif>
<div id="MainContain">
	<div id="Header"><img src="images/Header.jpg" width="971" height="114" alt="STEM Academy" title="STEM Header" />
		<div id="Nav">
			<!--When this site goes live this is where the links will go. The image is a back ground image.-->
		</div><!--Nav ENDS-->
		<div>#event.getArg("layout.content")#</div>
	</div><!--Header ENDS-->
	<div id="Clear"><p>&nbsp;</p></div>
	<div id="Footer">
		<a href="http://www.siemens-foundation.org/en/about.htm" target="_blank">Siemens Foundation</a>  |
		<a href="/index.cfm?event=showPage&p=about-STEM-academy" target="_blank">About STEM Academy</a>  |
		<a href="http://www.redactededucation.com" target="_blank">redacted Education </a><!--  |
		<a href="http://siemens.collegeboard.com/" target="_blank">College Board</a>  |
		<a href="http://www.energy.gov/" target="_blank">Department of Energy</a>-->
	</div><!--Footer ENDS-->
</div><!--MainContain ENDS-->
#event.getArg("layout.footer")#
</html>
	<div id="debug">
		Server: #application.udf.getServerIp()#<br />
		Debug Info:<br />
		<cfdump var="#event.getArgs()#" expand="false" label="eventArgs" />
		<cfdump var="#cookie#" expand="false" label="cookie" />
		<cfdump var="#client#" expand="false" label="client" />
		<cfdump var="#request#" expand="false" label="request" />
		<cfdump var="#session#" expand="false" label="session" />
		<cfdump var="#variables#" expand="false" label="variables" />
		<cfdump var="#application#" expand="false" label="application" />
		<cfdump var="#cgi#" expand="false" label="cgi" />
	</div>
</cfoutput>