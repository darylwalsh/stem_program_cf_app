<cfsilent>
	<cfset tcontents = event.getArg("tcontents") />
	<cfset id = event.getArg("c") />
</cfsilent>
<cfoutput><cfcontent reset="true">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>#event.getArg("pageTitle")#</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="DESCRIPTION" content="The nation's first online community designed exclusively to foster achievement in science, technology, engineering and math through the collaboration of STEM educators and the sharing of best practices." />
<meta name="KEYWORDS" content="Science, technology, engineering, math, STEM, STEM Education" />
<link href="/css/stemacademy.css" rel="stylesheet" type="text/css" />
<link href="/css/deLightBox.css" rel="stylesheet" type="text/css" />
<link href="/css/jquery_select_plugin.css" rel="stylesheet" type="text/css" />	
<!-- insert page css -->
#event.getArg("css")#

<!-- end page css -->
<cfif FindNoCase("redactededucation.com", request.domain) lte 0>
	<style type="text/css">
		##shell {display:none;}
		##global-footer {display:none;}
	</style>
</cfif>
<!--[if lte IE 7]>
    <style type="text/css" media="all">
        @import url("/css/stem-ie.css");
    </style>
<![endif]-->
<!--[if IE 6]>
    <style type="text/css" media="all">
        @import url("/css/stem-ie6.css");
    </style>
<![endif]-->
<!--[if IE 7]>
    <style type="text/css" media="all">
        @import url("/css/stem-ie7.css");
    </style>
<![endif]-->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.1.0/prototype.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js"></script>

<script type="text/javascript" src="/js/stemacademy.js"></script>

<script type="text/javascript" src="/js/deLightBox.min.js"></script>

<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/js/jquery.taconite.js"></script>
<script type="text/javascript" src="/js/login2013.js"></script>


<!--- <script type="text/javascript" src="/js/swfobject.js"></script> --->
<!--- <script type="text/javascript" src="http://static.redactededucation.com/de/js/swfobject.js"></script> --->
<!-- insert page js -->
#event.getArg("js")#
<!-- end page js -->
<!--::  Insert Header  ::::::::::::::::::::-->
<!-- apr 24, 2012 10:42am et -->
<cfsavecontent variable="xyz">http://static.redactededucation.com/www/js/swfobject.js|http://static.redactededucation.com/de/js/swfobject.js|http://dev-static.redactededucation.com/www/js/swfobject.js|http://stage-static.redactededucation.com/www/js/swfobject.js</cfsavecontent>
#rereplacenocase(event.getArg("layout.header"),xyz,"/js/swfobject.js")#
<!--::  End Header  :::::::::::::::::::::::-->
<div id="MainContain">
	<div id="Header">
  		<a id="home-link" href="/" title="Siemens STEM Academy"></a>
  		<div id="utility">
			<div id="logins">
			<cfif application.cfcs.register.isSTEMUserLoggedIn(true)>
				<cfset logoutURL = cgi.script_name />
				<cfif Find("?", logoutURL) gt 0>
					<cfset logoutURL = logoutURL & "&" />
				<cfelse>
					<cfset logoutURL = logoutURL & "?" />
				</cfif>
				<cfset logoutURL = logoutURL & "logout=true" />
				<div id="greeting">Hello, #cookie.first_name# | <a href="#logoutURL#">Logout</a></div>
				<br clear="all" />
			<cfelse>
				<form action="##" method="post" onsubmit="loginAjax(); return false;">
					<div id="invalid" style="display:none;">Invalid username or password</div>
					<div id="login-loader" style="display:none;"><img src="/images/homeLoader_small.gif" alt="" width="16" height="16" /></div>
					<img src="/images/label-login.gif" width="56" height="22" />
					<input type="text" size="15" name="strLoginUsername" id="strLoginUsername" value="Email" onFocus="focusUsername(this.id);" onblur="blurUsername(this.id);"/>
					<input type="password" size="15" name="strLoginPassword" id="strLoginPassword" value="" onBlur="checkField(this.id,'strLoginPassword_hidden');" style="display:none;"/>
                    <input type="text" size="15" name="strLoginPassword" id="strLoginPassword_hidden" value="Password" onFocus="swapFields(this.id,'strLoginPassword');" />
					<a href="##" onclick="loginAjax(); return false;" class="sprite" id="btn-submit"></a>
					<br clear="all" />
					New User?  <a href="#BuildUrl('showRegister')#">Register.</a>
	  				<a onClick="deGlobalWin({path:'#BuildUrl('xhr.forgotForm')#', closebtn:false,background:'none'}); return false;" href="##" title="Forgot username or password?">Forgot username or password?</a>
					<input type="submit" style="position:absolute; top:-5000px; left:-5000px;"  />
					<a href="##" id="SignIn" class="btnSignup"><!--bg image here--></a>
				</form>
			</cfif>
			</div><!---end logins--->
			<div id="nav-search">
				<form name="searchForm" method="get" action="#BuildUrl('')#" onSubmit="return verify();">
					<img src="/images/label-search.gif" width="66" height="22" />
					<input type="hidden" name="event" value="showSearchResults" />
					<!--- input type="hidden" name="collname" value="#request.siteId#" / --->
					<input type="text" id="criteria" name="criteria" size="20" />
					<!---input type="submit" id="submit" name="submt" value="Search" /--->
					<a href="javascript:document.searchForm.submit();" class="sprite" id="btn-search" onClick="return verify();"></a>
					<div style="z-index:3000;"><div id="criteriaSuggestions" class="autocomplete" style="display:none;"></div></div>
				</form>
			</div><!---end nav-search--->
			<br clear="all" />
			<div id="share">
				<div id="ShareBtnContain">
					<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pub=xa-4a81650f3516c8d3"><img src="http://s7.addthis.com/static/btn/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0" /></a>
					<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js?pub=xa-4a81650f3516c8d3"></script>
				</div><!--ShareBtnContain ENDS-->
				<a href="http://twitter.com/SiemensSTEMAcad" class="sprite" id="btn-twitter" title="Follow Us on Twitter" target="_blank"></a>
				<a href="http://www.facebook.com/home.php?##/pages/Siemens-STEM-Academy/268535066939?ref=nf" class="sprite" id="btn-facebook" title="Become a Friend on Facebook" target="_blank"></a>
	  		</div> <!---end share--->
  		</div> <!---end utility--->
	</div><!--Header ENDS-->
	<!--Nav Begins-->
	<div id="Nav">
		<ul id="sitenav">
</cfoutput>
<cfoutput query="tcontents" group="lev1id">
			<li><a href="#BuildUrl('showContent','c=#lev1id#')#" <cfif id EQ lev1id OR id EQ "">class="active"</cfif>>#lev1title#</a></li>
		<cfif lev2id NEQ "">
			<cfoutput group="lev2id">
			<li><a href="#BuildUrl('showContent','c=#lev2id#')#" <cfif id EQ lev2id>class="active"</cfif>>#lev2title#</a></li>
			</cfoutput>
		</cfif>
</cfoutput>
<cfoutput>
		</ul>
	</div>
	<!--Nav ENDS-->
	<div id="MiddleContain">
		<!-- Insert Content -->
		#event.getArg("layout.content")#
		<!-- End Content -->
		<br clear="all" />
	</div><!--MiddleContain-->
	<div id="Footer">
		<div id="sponsors">
			<a href="http://www.siemens-foundation.org/en/" target="_blank"><img src="/images/LogoSiemens-Foundation.png" alt="Siemens" title="Siemens" /></a>
			<a href="http://www.redactededucation.com" target="_blank"><img src="/images/dsc-edu-logo.png" alt="redacted Education" title="redacted Education" /></a>
			<a href="http://www.orau.org/" target="_blank"><img src="/images/ORAUlogo.jpg" alt="Oak Ridge" title="Oak Ridge" width="100" height="34" /></a>
			<a href="http://siemens.collegeboard.com/" target="_blank"><img src="/images/LogoCollegeBoard.png" alt="College Board" title="College Board" /></a>
		</div>
		<br clear="all" />
		<a href="http://www.siemens-foundation.org/en/" target="_blank">Siemens Foundation</a>  |
		<a href="/index.cfm?event=showPage&p=about-STEM-academy" target="_blank">About STEM Academy</a>  |
	    <a href="http://www.redactededucation.com/" target="_blank">redacted Education </a>  |
		<a href="#BuildUrl('showPage','p=media')#">Media</a>  |
		<a href="http://www.redactededucation.com/aboutus/privacypolicy.cfm" target="_blank">Privacy Policy</a>
	</div><!--Footer ENDS-->
</div><!--MainContain ENDS-->


<!--::  Insert Footer  ::::::::::::::::::::-->
#event.getArg("layout.footer")#
<!--::  End Footer  :::::::::::::::::::::::-->
<cfif StructKeyExists(client,"stem_debug") AND client.stem_debug EQ true>
	<div id="debug">
		Server: #application.udf.getServerIp()#<br />
		Debug Info:<br />
		<cfdump var="#event.getArgs()#" expand="true" label="eventArgs" />
		<cfdump var="#variables#" expand="true" label="variables" />
		<cfdump var="#request#" expand="true" label="request" />
		<cfdump var="#cgi#" expand="true" label="cgi" />
		<!---cfdump var="#getPageContext().getBuiltInScopes()#" expand="false" label="SCOPES" /--->
	</div>
</cfif>
</cfoutput>