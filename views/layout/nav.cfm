<cfsilent>
	<cfset tcontents = event.getArg("tcontents") />
</cfsilent>
 	<!--Begin Menu-->
	<a href="index.cfm" title="Home"><img src="img/leftNav_cap_blank.jpg" width="156" height="11" /></a>
	<div id="menus">
		<dl id="menu">
		<cfoutput query="tcontents" group="lev1id">
			<dt class="smenu" id="a#lev1id#"><a class="aro_b" id="la#lev1id#" href="##" onclick="return false;">#lev1title#</a></dt>
			<cfif lev2id NEQ "">
				<dd id="smenu#lev1id#">
					<ul>
					<cfoutput group="lev2id">
						<li><a id="t#lev2id#" href="#BuildUrl('showContent','id=#lev2id#')#">#lev2title#</a></li>
					</cfoutput>
					</ul>
				</dd>
			</cfif>
		</cfoutput>
		</dl>
	</div>
	<img src="img/leftNav_cap_btm.jpg" width="156" height="11" />