<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><cfoutput>#event.getArg("pageTitle")#</cfoutput></title>
<link href="/css/admin.css" type="text/css" media="screen" rel="stylesheet" />
<!-- insert page css -->
<cfoutput>#event.getArg("css")#</cfoutput>
<!-- end page css -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.1.0/prototype.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js"></script>
<!-- insert page js -->
<cfoutput>#event.getArg("js")#</cfoutput>
<!-- end page js -->
<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-12418696-1']);
_gaq.push(['_setDomainName', 'redactededucation.com']);
_gaq.push(['_setAllowLinker', true]);
_gaq.push(['_trackPageview']);

(function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })();

</script>
<script type="text/javascript">
if (typeof jQuery != 'undefined') {
jQuery(document).ready(function($) {
var filetypes = /\.(zip|exe|pdf|doc*|xls*|ppt*|mp3|mp4|flv|f4v|mov|wmv|avi)$/i;
var baseHref = '';
if (jQuery('base').attr('href') != undefined)
baseHref = jQuery('base').attr('href');
jQuery('a').each(function() {
var href = jQuery(this).attr('href');
if (href && (href.match(/^https?\:/i)) && (!href.match(document.domain))) {
jQuery(this).click(function() {
var extLink = href.replace(/^https?\:\/\//i, '');
_gaq.push(['_trackEvent', 'External', 'Click', extLink]);
if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
setTimeout(function() { location.href = href; }, 200);
return false;
}
});
}
else if (href && href.match(/^mailto\:/i)) {
jQuery(this).click(function() { var mailLink = href.replace(/^mailto\:/i, ''); _gaq.push(['_trackEvent', 'Email', 'Click', mailLink]); });
}
else if (href && href.match(filetypes)) {
jQuery(this).click(function() {
var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
var filePath = href;
_gaq.push(['_trackEvent', 'Download', 'Click-' + extension, filePath]);
if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
setTimeout(function() { location.href = baseHref + href; }, 200);
return false;
}
});
}
});
});
}
</script>
</head>
<body>
<div id="header">
	<h1><cfoutput>#request.siteName# Admin</cfoutput></h1>
</div>
<div id="menu">
	<h1>menu</h1>
<cfoutput>
	<ul>
		<li><a href="#BuildUrl('admin.showHome')#">Admin Home</a></li>
		<li><a href="#BuildUrl('admin.showSite','siteId=#request.siteId#')#">Site Manager</a></li>
		<li><a href="#BuildUrl('admin.editWebsite','siteId=#request.siteId#')#">Site Members</a></li>
		<li><a href="#BuildUrl('admin.showFileManager')#">File Manager</a></li>
		<li><a href="#BuildUrl('admin.showCategories')#">Categories</a></li>
		<li><a href="#BuildUrl('admin.showTypes')#">Types</a></li>
		<li><a href="#BuildUrl('admin.showAllAssets')#">Resources</a></li>
		<li><a href="#BuildUrl('admin.refreshSearchIndex','collectionName=#request.siteId#')#">Update Search</a></li>
		<li><a href="#BuildUrl('admin.showReports')#">Reports</a></li>
		<li><a href="#BuildUrl('admin.showSearchReports')#">Search Reports</a></li>
		<li><a href="#BuildUrl('admin.showRatingReports')#">Rating Reports</a></li>
		<li><a href="#BuildUrl('admin.showComments')#">Comments</a></li>
		<li><a href="#BuildUrl('admin.showBlocks')#">Blocks</a></li>
		<li><a href="#BuildUrl('admin.showUsers','sponsor_guid=#application.sponsor_guid#')#">Registered Users</a></li>
		<li><a href="#BuildUrl('admin.showInstitute')#">Institute Applications</a></li>
		<li><a href="#BuildUrl('')#" target="_blank">Open site in New Window</a></li>
	</ul>
</cfoutput>
</div>
<div id="content">
	<div><cfoutput>#event.getArg("layout.content")#</cfoutput></div>
</div>
<div id="footer">
	<h1><cfoutput>#request.siteName# Admin</cfoutput></h1>
</div>
</body>
</html>