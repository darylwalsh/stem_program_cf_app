<cfoutput><cfcontent reset="true"><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>#event.getArg("pageTitle")#</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="robots" content="noindex, nofollow" />
<link rel="stylesheet" type="text/css" href="css/support.css" />
<link rel="stylesheet" type="text/css" href="css/starbox.css" />
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js'></script>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/prototype/1.6.0.3/prototype.js'></script>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.2/scriptaculous.js'></script>
<script type="text/javascript" src="#application.env.huburl#/js/wddx.js"></script>
<script type='text/javascript' src='js/starbox.js'></script>
<script type="text/javascript" src="js/nav.js"></script>
<script type="text/javascript" src="js/popUpWindow.js"></script>
<script type="text/javascript" src="js/titleList.cfm"></script>
<script type="text/javascript">
	<!--
	function verify() {
		var themessage = "none";
		if ($('criteria').value == "") {
			themessage = "Please enter one or more search terms.";
		}
		//alert if fields are empty and cancel form submit
		if (themessage == "none") {
			return true;
		}
		else {
			alert(themessage);
			return false;
		}
	}
	-->
</script>
<!--::  Insert Header  ::::::::::::::::::::-->
#event.getArg("layout.header")#
<!--::  End Header  :::::::::::::::::::::::-->
<div id="#layout#SupportBase" align="center"><!--align content in center regardless of containing width-->
	<div id="#layout#SupportStage" align="left"><!--set stage to 780px-->
	<cfif layout EQ "pre">
		<div id="preTier1">
			#event.getArg("layout.Breadcrumb")#
			<div style="position: absolute; right: 0pt; top: -3px; height: 22px; width: 300px;">
				<form name="searchForm" method="post" action="#BuildUrl('showSearchResults')#" onSubmit="return verify();">
					<input type="hidden" name="collname" value="#request.siteId#_public" />
					<input type="text" id="criteria" name="criteria" size="20" />
					<input type="submit" id="submit" name="submt" value="Search" />
					<div style="z-index:3000;"><div id="criteriaSuggestions" class="autocomplete"></div></div>
					<script type="text/javascript" language="javascript">
						var myAutoCompleter = new Autocompleter.Local('criteria', 'criteriaSuggestions', titleList, { });
					</script>
				</form>
			</div>
		</div><!--tier1-->
	<cfelse>
		<div id="#layout#Tier1">
			#event.getArg("layout.Breadcrumb")#
			<div style="position: absolute; right: 0pt; top: -3px; height: 22px; width: 300px;">
				<form name="searchForm" method="post" action="#BuildUrl('showSearchResults')#" onSubmit="return verify();">
					<input type="hidden" name="collname" value="#request.siteId#" />
					<input type="text" id="criteria" name="criteria" size="20" />
					<input type="submit" id="submit" name="submt" value="Search" />
					<div style="z-index:3000;"><div id="criteriaSuggestions" class="autocomplete"></div></div>
					<script type="text/javascript" language="javascript">
						var myAutoCompleter = new Autocompleter.Local('criteria', 'criteriaSuggestions', titleList, { });
					</script>
				</form>
			</div>
		</div>
	</cfif>
	<cfif event.isArgDefined("layout.Nav") AND event.getArg("layout.Nav") NEQ "">
		<!-- two column layout -->
		<div id="supportNav">
			#event.getArg("layout.Nav")#
		</div><!--id:supportNav-->
		<div id="supportContent">
			#event.getArg("layout.Content")#
		</div><!--id:supportContent-->
	<cfelse>
		<!-- one column layout -->
		<div>
			#event.getArg("layout.Content")#
		</div>
	</cfif>
		<div class="clear"></div>
	</div><!--id:supportStage-->
</div><!--id:supportBase-->
<!--::  Insert Footer  ::::::::::::::::::::-->
#event.getArg("layout.footer")#
<!--::  End Footer  :::::::::::::::::::::::-->
<cfif StructKeyExists(url,"debug")>
	<div id="debug">
		Server: #application.udf.getServerIp()#<br />
		Debug Info:<br />
		<cfdump var="#event.getArgs()#" expand="false" label="eventArgs" />
		<cfdump var="#cookie#" expand="false" label="cookie" />
		<cfdump var="#client#" expand="false" label="client" />
		<cfdump var="#request#" expand="false" label="request" />
		<cfdump var="#session#" expand="false" label="session" />
		<cfdump var="#variables#" expand="false" label="variables" />
		<cfdump var="#application#" expand="false" label="application" />
		<cfdump var="#cgi#" expand="false" label="cgi" />
	</div>
</cfif>
</cfoutput>