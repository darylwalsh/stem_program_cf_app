<cfsilent>
	<cfset orderBy = event.getArg("orderBy") />
	<cfif orderBy EQ "">
		<cfset orderBy = "recent" />
	</cfif>
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset qResources = event.getArg("qResources") />
	<cfset event.setArg("pageTitle","#request.siteName#") />
</cfsilent>
<link rel="stylesheet" type="text/css" href="css/support.css" />
<cfoutput>
<div id="content-single">
	<h2>STEM Resources</h2>
	<div id="resource-lnav">
		<h4 class="sidebar-short">Browse by Grade</h4>
		<ul>
			<li><a title="3-5" href="#BuildUrl('showGrade','c=#c#|g=3,4,5')#" class="wedgelink">3-5</a></li>
			<li><a title="6-8" href="#BuildUrl('showGrade','c=#c#|g=6,7,8')#" class="wedgelink">6-8</a></li>
			<li><a title="9-12" href="#BuildUrl('showGrade','c=#c#|g=9,10,11,12')#" class="wedgelink">9-12</a></li>
		</ul>
		<h4 class="sidebar-short">Browse by Subject</h4>
		<ul>
		<cfloop index="i" from="1" to="#ArrayLen(categories)#">
			<li><a title="#categories[i].getName()#" href="#BuildUrl('showCategory','c=#c#|categoryId=#categories[i].getCategoryId()#')#" class="wedgelink">#categories[i].getName()#</a></li>
		</cfloop>
		</ul>				
	</div><!---resource-lnav--->
	<!--BEGIN SEARCH RESULTS UI AND OUTPUT-->
	<div id="searchContent">
		<!--holds search bars and content-->
		<div id="search">
			<!--top bar paging, items per, result count-->
			<div id="topBlock">
				<span class="res">
					Results 1-10 of #qResources.RecordCount#
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Items per Page:
					<select name="selItemsPerPage" id="selItemsPerPage" class="perPage" onChange="location=this.options[this.selectedIndex].value;" >
						<option value="#BuildCurrentUrl('itemsPerPage=10')#" selected>10</option>
						<option value="#BuildCurrentUrl('itemsPerPage=25')#">25</option>
						<option value="#BuildCurrentUrl('itemsPerPage=50')#">50</option>
					</select>
				</span><!--class:res-->			
				<span class="paging">
					<cfif orderBy EQ "views"><strong></cfif><a href="#BuildCurrentUrl('orderBy=views')#">Most Viewed</a><cfif orderBy EQ "views"></strong></cfif> | <cfif orderBy EQ "recent"><strong></cfif><a href="#BuildCurrentUrl('orderBy=recent')#">Recently Added</a><cfif orderBy EQ "recent"></strong></cfif>
					<strong>1</strong>
					<a href="" class="paging_links">2</a>						
				</span><!--class:paging-->
			</div><!--id:topBlock-->			
			<!--Begin Results-->
			<div id="results-list">
			<cfloop query="qResources">
				<div class="resultBlock">
					<h3><a href="#BuildUrl('showResource','c=#c#|resourceId=#resourceId#')#">#title#</a></h3>
					<img src="/assets/stem/thumbnails/#thumbnail#" /><br />
					#application.udf.abbreviate(body,500)#
					<span class="subject"></span>
				</div>
			</cfloop>
			</div><!---result-list--->				
			<!--End Results-->
			<!--bottom bar paging, items per, result count-->	
			<div id="btmBlock">
				<span class="res">
					Results 1-10 of #qResources.RecordCount#
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Items per Page:
					<select name="selItemsPerPage" id="selItemsPerPage" class="perPage" onChange="location=this.options[this.selectedIndex].value;" >
					   <option value="index.cfm?event=showSearchResults&amp;collname=ee92b302%2D1f29%2D3b68%2De636%2Df81e6b003479%5Fpublic&amp;criteria=science&amp;recordsPerPage=10" selected >10</option>
					</select>
				</span><!--class:res-->			
				<span class="paging">
					<strong>1</strong>
					<a href="" class="paging_links">2</a>
				</span><!--class:paging-->			
			</div><!--id:btmBlock-->
		</div><!--id:search-->
	</div><!--id:searchContent-->	
	<!--END SEARCH RESULTS UI AND OUTPUT-->
</div><!---content-single--->
</cfoutput>