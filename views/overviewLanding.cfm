<cfsilent>
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset event.setArg("pageTitle","#request.siteName# - Webinars") />
</cfsilent>
<style>
	.otherbtn { width:120px; height:20px; background-color:black; color:white; display:inline-block; padding:10px; text-align:center; vertical-align:middle; font-size:14px; text-decoration:none; } 
</style>
<cfoutput>
	<div id="content-left">
		<h2>EMPOWERING AND CELEBRATING EXCELLENCE IN STEM EDUCATION</h2>
<h3>PROGRAM OVERVIEW</h3>
	The Institute is back for 2013! Fifty educators from across the country will take part in the Siemens STEM Institute this summer, a one-of-a-kind program that promotes hands-on, real-world integration of science, technology, engineering, and math (STEM) in the classroom.
A diverse group of Fellows have been selected to attend this all-expenses-paid, week-long professional development experience, hosted at the world headquarters of redacted Communications, located outside of Washington D.C. Fellows will be exposed to leaders, personalities and innovators whose work across STEM disciplines shape and define our world today.
The week will be filled with guest speakers, field trips to leading institutions where Fellows will observe real-world applications of STEM subject matter, and networking opportunities with like-minded peers from across the nation. In addition to broad-based STEM applications, each Fellow will be assigned to a thematic working group that will provide additional deep-dive exposure

		<div class="frame">
		<iframe id="frame1" src="opnl.cfm" frameborder="0"></iframe>
	</div>
	<div class="frame">
		<iframe id="frame2" src="pnnl.cfm"  frameborder="0"></iframe>
	</div>