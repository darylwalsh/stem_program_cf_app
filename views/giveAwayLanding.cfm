<cfsilent>
	<cfset r = event.getArg("r") />
	<cfset categories = event.getArg("categories") />
	<cfset event.setArg("pageTitle","#request.siteName# - Webinars") />
</cfsilent>
<cfoutput>
<div id="content-left">
<h2>Dry Erase Marker Packs Giveaway</h2>
<h3>Share Your Resources and Receive Free Dry Erase Marker Packs</h3>
<br />
<h5>How It Works:</h5>
<p class="p1">Siemens STEM Academy is proud to host valuable educational resources from curriculum specialists, experts at Siemens, renowned education thought leaders, and educators like you. Your lesson plans, Power Point presentations, worksheets, and other resources help educators around the country bring STEM education to life in their classrooms.</p>
<p class="p1">To thank you for uploading resources and making the Siemens STEM Academy even stronger, we're offering free Dry Erase Marker Packs to users who upload two or more videos until December 1, 2012.&nbsp; Get started by uploading your STEM resources today.</p>
<p style="text-align: center;">
    <cfif isDefined("COOKIE.SPONSOR_USER_GUID") AND COOKIE.SPONSOR_USER_GUID NEQ "00000000-0000-0000-0000-000000000000">
		<a id="btn-upload" class="sprite" title="Upload STEM Resources" href="/index.cfm?event=showResourceForm&c=37"></a>
	<cfelse>
		<a id="btn-upload" class="sprite" title="Upload STEM Resources" onclick="deGlobalWin({path:'index.cfm?event=xhr.loginForm&amp;r=index%252Ecfm%253Fevent%253DshowResourceForm%2526c%253D37', closebtn:false,background:'none'}); return false;" href="##"></a>
	</cfif>
</p>
<p>&nbsp;</p>
<h5>Already Uploaded Your Resources:</h5>
<p>Click here to receive your free lab coat.</p>
</div>
<div id="content-right"><img style="padding-top: 30px;" src="img/makeyourmarklink2.png" alt="" /></div>
<!-- ///content-right/// -->
</cfoutput>