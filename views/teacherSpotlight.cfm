<cfsilent>
	<cfset c = event.getArg("c") />
	<cfset categories = event.getArg("categories") />
	<cfset event.setArg("pageTitle","#request.siteName# - Webinars") />


</cfsilent>

<script src="/js/jquery-1.4.4.min.js"></script>
<script src="/js/jquery_select_plugin.js"></script>

<script>
jQuery.noConflict();
	jQuery(document).ready( function() {
		var w = window.location.search;
		jQuery(".customselect").custSelectBox(	
			{selectwidth: '243px'}		
		);
		jQuery('.hover').live('click', function() {
			jQuery.each(jQuery(this), function(key, element) {			
			/*jQuery.each(element, function(ii, jj) {
				alert('key: ' + ii + '\n' + 'value: ' + jj);
			});	*/				
				x = jQuery(element).find('.elmValue').text();				
				if (x.length) {
					location.href = w + '#' + x;
				}
			});
		});
	});
</script>
<cfoutput>
<div id="content-left">

<h2>Teacher Spotlight</h2>
	<a name="joe-slifka"></a>
    <div class="teacher-photo-col">
    
    	<img src="/img/teacher-spotlight-joe-slifka.png" class="teacher-photo"/>
    

        <blockquote>
       "[The Siemens STEM Institute] has taught me that education is not me standing there collecting data as students push buttons; it is teaching me to guide students to solutions and work with them to discover the world around them."
        </blockquote>
        <cite>
            --Joe Slifka
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Joe Slifka</h3>
        <div class="teacher-details">
            <b>2011 STEM Institute Fellow</b><br>
        STEM/Technology<br>
		LaBrae High School, Leavittsburg, OH
        </div>
		


		<p class="question">What is the importance of STEM education in the U.S.? </p>
				
		<p>STEM education teaches students how to think. It takes what was learned and applies it in a way that a "traditional" teacher would say, "Huh. I never looked at it that way... I guess that does work!" Fostering creativity and applying multidisciplinary knowledge to solve a problem are two critical skills learned in my STEM classes.
<br><br>
I believe there needs to be a sweeping reform of the way we think about education and the way we teach our students. Students are not benefitting from recitation of facts. We are doing a disservice to our kids when we do not make them apply what they learn to something more than just a paper and pencil test. Real world skills, innovation, and creative problem solving are needed to be successful, and you will find all of them in just about every STEM education course.


</p>
		    
		<p class="question">How has participating in the STAR's/STEM Institute impacted the way you teach in the classroom?</p>
		
		<p>
		Before attending the institute, I felt I knew what STEM was: I used clickers; I had a projector; I posted my homework on a website. Little did I know... this is not STEM! STEM is all about interactivity. It's about creating, collaborating, designing, re-designing, thinking, and problem solving. STEM has taught me that education is not me standing there collecting data as students push buttons; it is teaching me to guide students to solutions and work with them to discover the world around them. From Shakespeare to trigonometry and government to Newton's Laws, STEM brings learning to life. I have been able to enhance all of my lessons thanks to my STEM training I received while at the weeklong Siemens STEM Institute.  </p>
		
		<p class="question">Please describe one way that you have become an ambassador for Siemens Foundation and shared your experience with other teachers.</p>
		<p>After attending the Siemens STEM Institute, I knew that my future as a 9th grade science teacher would soon change. I'd learned too much to be confined to just one discipline. I am now a STEM teacher for LaBrae Middle and High Schools in Leavittsburg, OH. I was hired as a technology teacher with the intent of starting new STEM programs for my district. I have been working with both the administrators and fellow teachers here to design and implement new classes focusing on design, creative thinking, environmental sustainability, and project-based learning. My students enjoy coming to class and I love seeing their successes as they work through problems and various projects. Without the Institute, I don't think I would have the skills or the way of thinking that I have today.
</p>
       

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>
	<br><br>

	<a name="adam-robb"></a>
    <div class="teacher-photo-col">
    
    	<img src="/img/teacher-spotlight-adam-robb.png" class="teacher-photo"/>
    

        <blockquote>
       "Attending the Siemens STEM Institute provided me with the expertise and confidence that I needed to apply to become a National Board Certified Teacher."
        </blockquote>
        <cite>
            --Adam Robb
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Adam Robb</h3>
        <div class="teacher-details">
            <b>2012 STARs Fellow</b><br>
        Mathematics & Science<br>
		Moundridge High School, Moundridge, KS
        </div>
		


		<p class="question">What is the importance of STEM education in the U.S.? </p>
				
		<p>As our daily lives shift into more high-tech routines, STEM education is at the forefront. Not only are we teaching students how to succeed in math, science, and technology related fields (and learning from them, as they are digitally native), we are also teaching them to think critically, be creative in problem solving, and work well with others.

</p>
		    
		<p class="question">How has participating in the STAR's/STEM Institute impacted the way you teach in the classroom?</p>
		
		<p>
		The STAR's program has made me more aware of the nature of problem solving in math and science. Prior to participating in the STAR's program, I used what I thought was a good deal of inquiry and student-guided learning. Now, looking back through the STAR's lens, I realize that my lecture-heavy method wasn't getting the job done. I've tweaked my classes to include more student redacted, inquiry, simulation, and problem solving, and am seeing wonderful results! Class is more enjoyable for me as well, which is an added bonus! </p>
		
		<p class="question">Please describe one way that you have become an ambassador for Siemens Foundation and shared your experience with other teachers.</p>
		<p>I took the opportunity to host a Siemens Science Day at our local Moundridge Elementary School last fall, where I was able to share about my STARS experience and bring some sweet interactive science to our elementary students. This got them all fired up for science! Professionally, I have been working on recruiting my colleagues to take advantage of this amazing experience! I have several teaching friends and colleagues that would benefit greatly from this experience, and contribute immensely to the experience of others, and I have been working to encourage them to apply (and accept, if offered, of course!).
</p>
       

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>
	<br><br>
	<a name="bradley-lands"></a>
    <div class="teacher-photo-col">
        <img src="/img/teacher-spotlight-bradley-lands.png" class="teacher-photo"/>

        <blockquote>
       "Attending the Siemens STEM Institute provided me with the expertise and confidence that I needed to apply to become a National Board Certified Teacher."
        </blockquote>
        <cite>
            -- Bradley Lands
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Bradley Lands</h3>
        <div class="teacher-details">
            <b>2011 STEM Institute Fellow</b><br>
        Instructional Technology Coach<br>
		Mary Ellen Henderson Middle School, Falls Church City, VA
        </div>
		


		<p class="question">What is the importance of STEM education in the U.S.? </p>
				
		<p>STEM education has never been as important for U.S. students as it is today. Compared with other students around the world, U.S. students are particularly falling behind in math and science. As a result, fewer students are graduating college with math and science degrees and thus, failing to fill math and science positions in the workforce.  Simultaneously, as the U.S. demand for technology increases exponentially, the supply of innovators steadily decreases, jeopardizing our economy, environment, and national security.
As U.S. students enter the workforce, they may face the nation’s most difficult challenges to date. The United States thrives on creativity and ingenuity; therefore, our U.S. students will need to continue to surpass other countries in innovation and technological advancement. As an educator, it is my responsibility to inspire students to pursue STEM education so America continues to persevere in the future.

</p>
		    
		<p class="question">How has participating in the STAR's/STEM Institute impacted the way you teach in the classroom?</p>
		
		<p>
		Participating in the Siemens STEM Institute has significantly impacted the way that I teach.  As a fellow, I had the once-in-a-lifetime opportunity to participate with 49 other educators from across the U.S. in a unique professional development program.  Focused on providing participating educators with new tools, skills and strategies for improving student achievement in science, technology, engineering and math (STEM) disciplines, I was able to learn, share and grow from other outstanding educators from across the nation.
Lastly, I have applied what I have learned from the Siemens STEM Institute in order to reach National Board Certification in the field of Career and Technical Education. I used several of the best and next practice teaching strategies, coupled with powerful technology tools with my students in order to demonstrate my teaching ability.  Attending the Siemens STEM Institute provided me with the expertise and confidence that I needed to apply to become a National Board Certified Teacher.
</p>
		
		<p class="question">Please describe one way that you have become an ambassador for Siemens Foundation and shared your experience with other teachers.</p>
		<p>Following the Institute, I am currently serving as a STEM ambassador in my school and community as I continue working with my colleagues using the tools and knowledge gained from the Institute.  I returned to my district, empowered with new knowledge to share with my colleagues with a goal of improving student achievement in the critical disciplines of STEM.  Now all of my colleagues have the opportunity to use the resources and tools that I acquired at the institute with their students.  By sharing what I learned from the institute with my colleagues, I am creating a ripple effect that will positively impact the education of all our students. I am also documenting and sharing my successes to my professional learning community via my blog and my Twitter account.</p>
       

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>
	<br><br>
	


	
	<a name="tim-kubinak"></a>
    <div class="teacher-photo-col">
        <img src="/img/teacher-spotlight-tim-kubinak.png" class="teacher-photo"/>

        <blockquote>
        "The Siemens STEM Institute opened my eyes to a wealth of strategies, technologies, and projects that otherwise I wouldn't be able to access."
        </blockquote>
        <cite>
            -- Tim Kubinak
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Tim Kubinak</h3>
        <div class="teacher-details">
            <b>2012 STEM Institute Fellow</b><br>
            Mathematics<br>
			John Yeates Middle School, Suffolk, Virginia
        </div>
		
		<p class="question">What is the importance of STEM education in the U.S.? </p>
				
		<p>While the three R's have always been important in the formative education of students, there has never been a time that STEM education has been more needed, and for as many different reasons as there are students in our classrooms. Our societal shift to technology-based manufacturing, government, economy, and even education compels even the most Luddite of our field to engage students with, and in, new educational technology. The need for competent scientific, mathematical minds has never been greater; our field is tasked with maintaining a knowledge pool to maintain the pace of society's hunger for time- and labor-saving devices, as well as addressing a variety of environmental and man-made problems. In addressing those problems, the role of engineering professionals goes hand-in-hand. As a competitive issue, we want to ensure our children are a part of the knowledge base, and not reliant upon others to solve our problems.</p>
		    
		<p class="question">How has participating in the STAR's/STEM Institute impacted the way you teach in the classroom?</p>
		
		<p>
		The Siemens STEM Institute opened my eyes to a wealth of strategies, technologies, and projects that otherwise I wouldn't be able to access. The speakers and coaches provided me the inspiration, feedback, and resources to teach the way my students should be taught- good content, thoughtful use of technology, and high engagement. I've also learned that, with the sheer volume of technologies and gadgets available for use in the classroom, it is best to incorporate them slowly and wisely into an instructional program. While I am still the same teacher, teaching the same math content, I now have a toolbox of strategies, hardware, and software that will bring out the best in my students. And, it will be FUN!</p>
		
		<p class="question">Please describe one way that you have become an ambassador for Siemens Foundation and shared your experience with other teachers.</p>
		<p>I have tried to be an ambassador for Siemens Foundation not only by bringing my experience (and a cool t-shirt) back to my district and region, but by shedding light on the strategies and tools I've learned via their wise use in my classroom. While incorporating these into my instruction, I have had the opportunity to share with my colleagues throughout my school district during curriculum development sessions, email sharing groups, and videoconferencing. As a result of my desired project focus, I was able to partner with my school's science teachers, improving student understanding of scientific investigation and problem solving via the use of gameplay. In addition, I've been able to share my instruction on my website and my Edmodo class sites, providing my school's stakeholders with opportunities not only to witness instruction taking place, but they can also share the learning experience, learning with, and sometimes from, their children.</p>
       

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>
	<br><br>
	



	<a name="terrence-bissoondial"></a>
    <div class="teacher-photo-col">
        <img src="/img/teacher-spotlight-terrence-bissoondial.png" class="teacher-photo"/>

        <blockquote>
            "Conducting research at the high school level helps students to start asking the appropriate questions, 
			without which research is not possible."
        </blockquote>
        <cite>
            -- Dr. Terrence Bissoondial
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Dr. Terrence Bissoondial</h3>
        <div class="teacher-details">
            <b>2010 STAR's Fellow (Oak Ridge National Laboratory)</b><br>
            Science Research<br>
			George W. Hewlett High School, Hewlett, NY
        </div>
		
		<p class="question">What is the importance of STEM education in the U.S.?</p>
				
		<p>It is estimated that by 2015, hundreds of thousands of students in the United States and Europe will leave education without basic skills in STEM.  This deficiency will further stall the recovery of local and global economies as enterprises cannot be properly staffed.  Part of the solution to this problem is to foster interest in STEM education during the early school years. How can STEM education then be re-invigorated?  "Therein lies the rub."  Multiple means of teaching should be developed. 
For me, doing research is not an occupation but a vocation.   Like the manifestation of many traits or phenotypes, the appropriate volition and environment is required.  Ability is of little consequence without the opportunity to try them. Conducting research at the high school helps students to starting asking the appropriate questions, without which research is not possible.  It fosters their concerns for particulars and the mechanisms of things.  Moreover, it helps to develop meticulous and systematic approach for procedures and analyses.  In short, implementing the scientific method is a lifestyle that should be developed as early as possible.</p>
		    
		<p class="question">How has participating in the STAR's/STEM Institute impacted the way you teach in the classroom?</p>
		
		<p>
		During my fellowship at Oak Ridge National Laboratory, I was introduced to Panicum virgatum or switchgrass. Currently, switchgrass is explored by the US Department of Energy (DOE) as one of the possible feedstock for biofuel (ethanol) production. Ethanol is being developed as one alternative to fossil fuels since it is renewable and produces less atmospheric pollutants. Currently, ethanol is produced primarily through the saccharification and fermentation of starch (using corn or gain crops as feedstock). Since the summer of 2010, my students have been exploring ways to increase biomass production in switchgrass. One of the problems being studied is the lengthy period it takes for the seeds of switchgrass to be competent to germinate (2-4yrs). After two years of research, five of students isolated and characterized genes that regulate germination in switchgrass. Their studies can lead to development of strains of switchgrass that can grow faster!  
The research projects of these students were submitted Siemens 2012-2013 Competition. On Oct. 19th, 2012, all five students were named semifinalists in the premier Siemens Competition for Math, Science and Technology. In addition to studying switchgrass, other students are exploring new feedstocks for ethanol production using the enzymes and research techniques I acquired at Oak Ridge. In short, my high school research lab has been inundated with research ideas because of my research experience as a STAR's Fellow.</p>
		
		<p class="question">How have you become an Ambassador for Siemens Foundation and shared your experience with other teachers.</p>
		<p>Many schools are currently developing their research programs and seek guidance on the implementations.  Having achieved immense success with the research program at George W. Hewlett High School (especially since three of my students were recently named the 2012-2013 Siemens's Team Grand Winners for STEM), I have been approached for ideas concerning research projects that can be developed in the high school setting. I constantly refer to the STAR's Fellowship Program as an excellent start.</p>
       

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>
	<br><br>
	
	
	<a name="judy-stucky"></a>
    <div class="teacher-photo-col">
        <img src="/img/teacher-spotlight-judy-stucky.png" class="teacher-photo"/>

        <blockquote>
            "I use more technology than ever and have stories of what I did and learned [at STARs] to share with my students to inspire them.... [I hope] this leads to improved academic achievement and success, as well as more young people in STEM careers."
        </blockquote>
        <cite>
            -- Judy Stucky
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Judy Stucky</h3>
        <div class="teacher-details">
            <b>2011 STAR's Fellow (Oak Ridge National Laboratory)</b><br>
            Physics<br>
			Westside High School, Omaha, NE
        </div>
		
		<p class="question">What is the importance of STEM education in the U.S.?</p>
				
		<P>Science and Technology aren't going away. The impact it has on our lives will continue to grow exponentially. We need qualified personnel ready to use and create new technology. STEM education is absolutely vital. Nearly every job anyone will have will require some knowledge of science, technology, engineering and/or mathematics. Growing these fields in the U.S. will be important for our future economy and for our place in global society. </P>
		    
		<p class="question">How has participating in the STAR's/STEM Institute impacted the way you teach in the classroom?</p>
		
		<p>
		I use more technology than ever and have stories of what I did and learned to share with my students to inspire them. I have a better idea of what types of jobs are out there, what requirements it takes to fill these jobs and new technologies and inventions that will be coming down the road. I hope this has made my classroom more interesting and relevant and that this leads to improved academic achievement and success as well as interesting more young people in STEM careers.
		</P>
		
		<p class="question">Please describe one way that you have become an ambassador for Siemens Foundation and shared your experience with other teachers.</p>
		<p>
		I mentor new science teachers and share my experiences with them. I hope that one day soon some of them will be a part of the program. I have told members of professional organizations that I am involved in to apply as well. I believe that experiences like this are vital to avoid burnout.	
		</P>
       

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>
	<br><br>
	

	<a name="kendall-morton"></a>
    <div class="teacher-photo-col">
        <img src="/img/teacher-spotlight-kendall-morton.png" class="teacher-photo"/>

        <blockquote>
            "I have more tools to excite my students about STEM because of my experiences with the STARs program."
        </blockquote>
        <cite>
            -- Kendall Morton
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Kendall Morton</h3>
        <div class="teacher-details">
            <b>2010 STAR's Fellow (Oak Ridge National Laboratory)</b><br>
            High School Science ITL,<br>
			Chemistry and Environmental Science<br>
			Glenelg High School, Glenelg, MD

        </div>
		
		<p class="question">What is the importance of STEM education in the U.S.?</p>
				
		<P>STEM education is essential to the future success of the United States.  We need students to be informed about science, technology, engineering, and math so that they can use their innovation to create solutions to local, regional, and international problems. </P>
		    
		<p class="question">How has participating in the STAR's/STEM Institute impacted the way you teach in the classroom?</p>
		
		<p>
		Since completing the STARs program I am much more knowledgeable of the realm of scientific research.  I have more tools to excite my students about STEM because of my experiences with the STARs program.
		</P>
		
		<p class="question">Please describe one way that you have become an ambassador for Siemens Foundation and shared your experience with other teachers.</p>
		<p>
		I have told many science teachers of the great experiences I had with STARs at ORAU and encouraged them to apply.  While at ORAU I became aware of Siemens programs like We Can Change the World Challenge.  I have shared this information with the science teachers I work with.
		</P>
       

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>
	<br><br>
	
	

	<a name="darryl-richards"></a>
    <div class="teacher-photo-col">
        <img src="/img/teacher-spotlight-darryl-richards.png" class="teacher-photo"/>

        <blockquote>
            "Since participating in the STARs program, I infused more research, inquiry and technology-based activities into my lessons.... I give [my students] the chance to have that "A-ha!" moment ..."
        </blockquote>
        <cite>
            -- Darryl Richards
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Darryl Richards</h3>
        <div class="teacher-details">
            <b>2010 STAR's Fellow (Oak Ridge National Laboratory)</b><br>
            Middle & High School Science<br>
			New Era Academy, Baltimore, MD
        </div>

		
<p class="question">What is the importance of STEM education in the U.S.?</p>
		
<P>STEM education in the US is crucial to our country remaining a viable competitor in STEM-related fields with other world powers. A STEM education affords a student the opportunity to pursue many career pathways that they otherwise would probably not be exposed to.</P>
    
<p class="question">How has participating in STAR's enhanced your classroom instruction?</p>

<P>Since participating in the STARs program, I infused more research, inquiry and technology-based activities into my lessons.  Above all us, I experienced a paradigm shift in the way I approach my lessons.  I now allow my students more time to take more ownership of their learning by giving them the opportunity and time for redacted rather than telling the answer.  I give them the chance to have that "A-ha!" moment.</P>

<p class="question">Describe a way that you have become an Ambassador for Siemens Foundation and shared your experience with other teachers.</P>

<P>After I returned home after spending two weeks in Oak Ridge, TN at the Oak Ridge National Laboratory, I was so excited and inspired to share my incredible research experience.  When the 2010-2011 school year began, I spoke with my administrator about getting the opportunity to share my research experience with the science department.  Throughout that year I would share the research I conducted while at ORNL and also shared the supplies I acquired from the classroom stipend I received.  I also, encouraged them to apply to the program because it was such a worthwhile professional development experience in authentic research.  As an educator, you have to expect visitors to come in your classroom from time and time to see you teach a lesson. I often get compliments about the way I decorate my room and the equipment I have my students using.  It truly makes me very proud to tell them that I was able to get the needed supplies for my classroom courtesy of the STARs program.</P>
       

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>
	<br><br>

	<a name="eric-regh"></a>
    <div class="teacher-photo-col">
        <img src="/img/teacher-spotlight-eric-regh.png" class="teacher-photo"/>

        <blockquote>
            "The STEM Institute will continue my path of life-long learning.... My students deserve to see the "real world" application of STEM and I will be better prepared to offer this to them ..."
        </blockquote>
        <cite>
            -- Eric Regh
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Eric Regh</h3>
        <div class="teacher-details">
            <b>2012 STEM Institute Fellow</b><br>
            High School Science<br>
			St. George's School, Spokane, WA
        </div>

        <p class="question">
          Why do you think STEM education is important to today's students and how will it be relevant to their future?  
        </p>

        <p>
           STEM education is important to today's students because every one of them is born into a rapidly evolving technological world.  Many of the problems of today, as well as the problems of the future, will rely on technological solutions.  Careers that my grandparents and even my parents could rely on are disappearing or morphing into jobs that require the critical thinking skills that STEM education develops.  Even if a student has a desire to pursue a career outside of the STEM field, the knowledge and skills gained through a quality stem education will foster their desire for life-long learning as well as an ability to adapt and overcome challenges that a complex world will most certainly present to them.  As our economy has become ever more global, we do a disservice to our students if we do not prepare them to compete with their international peers.  With a majority of the job growth predicted to be in the medical and technological industry, we have to do a better job of developing our STEM programs in the K-12 setting.  When we have career discussions in class, my students continually point out their interest in biotech and medicine.  At times they have even pleaded with me to do the labs and activities that will most closely resemble their career after college.  We have an amazing university system in the US, but students are entering these institutions lacking the skills needed to reach their full potential.  My students are great at accessing information from places like Google and Wikipedia, but they are lacking the "thinking outside the box" skills that STEM programs develop.  An emphasis on STEM will allow my students to not only utilize the technologies available to them, but potentially develop the next Google or Wikipedia.   
        </p>

        <p class="question">
           How would you like to enhance your classroom instruction from experiences gained by participating in the Siemens STEM Institute? 
        </p>

        <p>
           I consider myself to be a life-long learner, who feels incredibly lucky to work in a field that is constantly evolving.  I have grown exponentially over the last two and a half years because of my desire to attend professional development workshops in the biotech/stem field.  The STEM Institute will continue my path of life-long learning.  Spokane, Washington can be a very isolated place that lacks the growth and development that biotech can offer.  There is not a lot of funding for STEM development and professional development opportunities like the STEM Institute can really benefit a city like Spokane.  I hope to bring the knowledge and skills that I can gain from a program like the STEM Institute and share these with not only my students, but my peers as well.  My students deserve to see the "real world" application of STEM and I will be better prepared to offer this to them after I have had a chance to network with scientists and other educators who are using STEM in the "real world."
        </p>

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>
	<br><br>

	<a name="brooke-fischels"></a>
	<div class="teacher-photo-col">
        <img src="/img/teacher-spotlight-brooke-fischels.png" class="teacher-photo" alt="Photo of Brooke Fischels"/>

        <blockquote>
            "The Siemens STARs program allowed me to experience open-ended research, high-level collaborative work, and intense problem solving situations. I attempt to replicate that sort of experience on a small scale in my classroom ..."
        </blockquote>
        <cite>
            -- Brooke Fischels
        </cite>

    </div><!--//teacher-photo-col-->
	
	 <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Brooke Fischels</h3>
        <div class="teacher-details">
            <b>2010 STAR's  Fellow (Oak Ridge National Laboratory)</b><br>
            High School Mathematics<br>
			Ottumwa High School, Ottumwa, Iowa

        </div>

        <p class="question">
            Why is STEM education important to students in the US?
        </p>

        <p>
            Investment in STEM education in the United States is critical to job growth and creation in engineering and technological fields.  Without a heavy and concerted effort to organize, propel, and improve STEM education in the United States, our students and companies will be out-innovated by emerging countries who are already solidifying footholds in the technological marketplace.  Only comprehensive implementation of STEM education that pushes our STEM curriculum beyond cross-curricular exchanges and moves to innovative ways to create trans-curricular reform will put the United States in a superior position to lead world efforts in engineering and technology.
        </p>

        <p class="question">
            How has participating in the STAR's/STEM Institute enhanced your classroom instruction?
        </p>

        <p>
            The Siemens STARs program allowed me to experience open-ended research, high-level collaborative work, and intense problem solving situations.  I attempt to replicate that sort of experience on a small scale in my classroom when I create problem based instructional tasks.  While students are exploring STEM concepts in my mathematics courses, they are learning how to work together, developing reasoning skills, and generalizing the underlying mathematics that makes things work.
        </p>
		
		<p class="question">
           How have you become an Ambassador for Siemens Foundation and shared your experience with other teachers.
        </p>
		
		<p>I have been able to share my Siemens STARs experience with colleagues during professional development.  In doing so, we have been able to collaborate about ways we can incorporate authentic problem solving experiences for students in our classrooms.  Students are receiving so many more opportunities that break away from the textbook and encourage higher-order thinking skills via productive group work and real world problem solving.
		</p>

    </div><!--//teacher-bio-col-->
	
    <div class="clear"></div>
	<br><br>
	<a name="channa-comer"></a>
    <div class="teacher-photo-col">
        <img src="/img/teacher-spotlight-channa-comer.png" class="teacher-photo" alt="Photo of Channa Comer"/>

        <blockquote>
            "[STEM education] will enable our students to make informed choices, be better citizens and to provide them with opportunities for employment and growth as they enter into adulthood."
        </blockquote>
        <cite>
            -- Channa Comer
        </cite>

    </div><!--//teacher-photo-col-->

    <div class="teacher-bio-col">
        <h2>Meet Siemens STEM Academy Featured Teacher</h2>
        <h3>Channa Comer</h3>
        <div class="teacher-details">
            <b>2012 STAR's  Fellow (Pacific Northwest Laboratory)</b><br>
            Middle School Science<br>
            Baychester Middle School, Bronx, NY
        </div>

        <p class="question">
            Why is STEM education important to students in the US?
        </p>

        <p>
            STEM education is important and relevant to the future of my students. As nations in the world become more interconnected and increasingly technological, it is more important than ever that our children are prepared to be leaders and problem solvers. STEM education is a critical component in this preparation. Not only will STEM education serve to provide solutions, new discoveries and new products, it will provide technical skills and knowledge that will enable our students to make informed choices, be better citizens and to provide them with opportunities for employment and growth as they enter into adulthood.
        </p>

        <p class="question">
            How would you like to enhance your classroom instruction from experiences gained by participating in the Siemens STEM Institute?
        </p>

        <p>
            My classroom theme for this year is "Discovering your inner scientist." I can think of no better way to help my students do this than to make my own inner scientist even more vibrant. One of the greatest gifts of being an educator is to have the summer months to immerse myself in study, deepen my own scientific knowledge, and then bring that knowledge back to my students in the classroom. It is through my learning experiences that I can help them see the relevance in science through inquiry, hands on experiences, and lively discussion and debate. And it is this relevance that will transform their experience from one of rote learning of information to true learning and understanding.
        </p>

    </div><!--//teacher-bio-col-->
    <div class="clear"></div>


</div><!--content-left-->
<div id="content-right" style="position:relative;">

	<!--- <img src="/img/stemstars.png"> --->
	
	<div style="height:25px;">
    <ul class="cssMenu">
        <li>
            <!a href="##">&nbsp;</a>           
            <ul>
            	<li><a href="##adam-robb">June 2013 - Adam Robb</a></li>
            	<li><a href="##bradley-lands">May 2013 - Bradley Lands </a></li>
                <li><a href="##tim-kubinak">April 2013 - Tim Kubinak </a></li>
                <li><a href="##terrence-bissoondial">March 2013 - Dr. Terrence Bissoondial</a></li>
                <li><a href="##judy-stucky">February 2013 - Judy Stucky</a></li>
				<li><a href="##kendall-morton">January 2013 - Kendall Morton</a></li>
				<li><a href="##darryl-richards">December 2012 - Darryl Richards</a></li>
				<li><a href="##eric-regh">November 2012 - Eric Regh</a></li>
				<li><a href="##brooke-fischels">October 2012 - Brooke Fischels</a></li>
				<li><a href="##channa-comer">September 2012 - Channa Comer</a></li>
            </ul>
        </li>
        
        
    </ul>
    	</div>
	
	<!-- <div class="select_wrap">
		<select class="customselect">
		<option>&nbsp;
       <!--- <option value="bradley-lands">May 2013 -Bradley Lands--->
		<option value="tim-kubinak">April 2013 - Tim Kubinak 
        <option value="terrence-bissoondial">March 2013 - Dr. Terrence Bissoondial
		<option value="judy-stucky">February 2013 - Judy Stucky
		<option value="kendall-morton">January 2013 - Kendall Morton
		<option value="darryl-richards">December 2012 - Darryl Richards
		<option value="eric-regh">November 2012 - Eric Regh
		<option value="brooke-fischels">October 2012 - Brooke Fischels
		<option value="channa-comer">September 2012 - Channa Comer
		</select>
	</div> -->
	<div style="height:30px;"></div>
	
   <!--- <a href="index.cfm?event=showGiveaway&c=37" id="tile-shark">
        <span>Take a bite out of learning -- be one of the first 100 to upload resources and get a free shark USB. (shark usb image)</span>
        <span>Upload Resources</span>
    </a> --->

	<a href="/index.cfm?event=showWebsites&c=37"><img src="/img/otherstemsites.png" style="border:none;"></a>
	<div style="height:20px;"></div>
	<a href="##featured_video" id="tile-featured-video">
		<span>Featured Video</span>
		<span>See how the Siemens STEM Academy is bolstering STEM learning</span>
	</a>

</div>
</cfoutput>