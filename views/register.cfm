<cfsilent>
	<cfset event.setArg("pageTitle","#request.siteName# - Register")>
</cfsilent>
<cfif FindNoCase("register.cfm", cgi.HTTP_REFERER) lte 0 and FindNoCase("redactededucation", cgi.HTTP_REFERER) gt 0>
	<cfif not StructKeyExists(url, "forwardURL")>
		<cfset url.forwardURL = cgi.HTTP_REFERER />
	</cfif>
</cfif>
<cfparam name="URL.forwardURL" default="/index.cfm" />

<cfif StructKeyExists(cookie, "APP_USER_GUID")>
	<cfset request.APP_USER_GUID = cookie.APP_USER_GUID />
</cfif>
<cfparam name="request.APP_USER_GUID" default="" />

<cfif not StructKeyExists(url, "up") AND application.cfcs.register.isSTEMUserLoggedIn()>
    <cflocation url="#URL.forwardURL#" addtoken="false" /> <!--- user already has an account and is logged in, so throw them to index page --->
    <cfabort>
</cfif>


<cfif StructKeyExists(client, "STEMRegistrationGUID") AND IsValid("guid", client.STEMRegistrationGUID)>
	<cfset reg = application.cfcs.register.getRegistration(client.STEMRegistrationGUID) />
	
	<cfset form.username = reg.username />
	<cfset form.password = reg.password />
	<cfset form.firstName = reg.firstName />
	<cfset form.lastName = reg.lastName />
	<cfset form.title = reg.title />
	<cfset form.email = reg.email />
	
	<cfif ListLen(reg.phone, "-") eq 3>
		<cfset form.areaPhone = ListFirst(reg.phone, "-") />
		<cfset form.prePhone = ListGetAt(reg.phone, 2, "-") />
		<cfset form.phone = ListLast(reg.phone, "-") />
	</cfif>
	
	<cfset form.PID = reg.PID />
	<cfset form.schoolName = reg.schoolName />
	<cfset form.schoolAddress = reg.schoolAddress />
	<cfset form.schoolAddress2 = reg.schoolAddress2 />
	<cfset form.schoolCity = reg.schoolCity />
	<cfset form.schoolState = reg.schoolState />
	<cfset form.schoolZipCode = reg.schoolZipCode />
	<cfset form.schoolPhone = reg.schoolPhone />
	<cfset form.schoolCountry = reg.schoolCountry />
	<cfset form.locationType = reg.locationType />
	<cfset form.gradeK = reg.gradeK />
	<cfset form.grade1 = reg.grade1 />
	<cfset form.grade2 = reg.grade2 />
	<cfset form.grade3 = reg.grade3 />
	<cfset form.grade4 = reg.grade4 />
	<cfset form.grade5 = reg.grade5 />
	<cfset form.grade6 = reg.grade6 />
	<cfset form.grade7 = reg.grade7 />
	<cfset form.grade8 = reg.grade8 />
	<cfset form.grade9 = reg.grade9 />
	<cfset form.grade10 = reg.grade10 />
	<cfset form.grade11 = reg.grade11 />
	<cfset form.grade12 = reg.grade12 />
	<cfset form.subjectScience = reg.subjectScience />
	<cfset form.subjectTechnology = reg.subjectTechnology />
	<cfset form.subjectEngineering = reg.subjectEngineering />
	<cfset form.subjectMath = reg.subjectMath />
	<cfset form.subjectIntegrated = reg.subjectIntegrated />
	<cfset form.subjectOther = reg.subjectOther />
	<cfset form.studentCount = reg.studentCount />
	<cfset form.yearsTeaching = reg.yearsTeaching />
	<cfset form.SECURITY_QUESTION_GUID = reg.SECURITY_QUESTION_GUID />
	<cfset form.SECURITY_QUESTION_ANSWER = reg.SECURITY_QUESTION_ANSWER />
	<cfset form.de_updates = reg.de_updates />
</cfif>


<cfparam name="form.username" default="" />
<cfparam name="form.password" default="" />
<cfparam name="form.firstName" default="" />
<cfparam name="form.lastName" default="" />
<cfparam name="form.title" default="" />
<cfparam name="form.email" default="" />
<cfparam name="form.areaPhone" default="" />
<cfparam name="form.prePhone" default="" />
<cfparam name="form.phone" default="" />
<cfparam name="form.PID" default="" />
<cfparam name="form.schoolName" default="" />
<cfparam name="form.schoolAddress" default="" />
<cfparam name="form.schoolAddress2" default="" />
<cfparam name="form.schoolCity" default="" />
<cfparam name="form.schoolState" default="" />
<cfparam name="form.schoolZipCode" default="" />
<cfparam name="form.schoolPhone" default="" />
<cfparam name="form.schoolCountry" default="" />
<cfparam name="form.locationType" default="" />
<cfparam name="form.gradeK" default="" />
<cfparam name="form.grade1" default="" />
<cfparam name="form.grade2" default="" />
<cfparam name="form.grade3" default="" />
<cfparam name="form.grade4" default="" />
<cfparam name="form.grade5" default="" />
<cfparam name="form.grade6" default="" />
<cfparam name="form.grade7" default="" />
<cfparam name="form.grade8" default="" />
<cfparam name="form.grade9" default="" />
<cfparam name="form.grade10" default="" />
<cfparam name="form.grade11" default="" />
<cfparam name="form.grade12" default="" />
<cfparam name="form.subjectScience" default="" />
<cfparam name="form.subjectScience" default="" />
<cfparam name="form.subjectTechnology" default="" />
<cfparam name="form.subjectEngineering" default="" />
<cfparam name="form.subjectMath" default="" />
<cfparam name="form.subjectIntegrated" default="" />
<cfparam name="form.subjectOther" default="" />
<cfparam name="form.studentCount" default="" />
<cfparam name="form.yearsTeaching" default="" />
<cfparam name="form.SECURITY_QUESTION_GUID" default="" />
<cfparam name="form.SECURITY_QUESTION_ANSWER" default="" />
<cfparam name="form.de_updates" default="" />



<link type="text/css" rel="stylesheet" href="/css/registerStyle.css" />
<link type="text/css" rel="stylesheet" href="/css/de-ui-style.css" />

<cfoutput>
<script type="text/javascript">
	_ajaxConfig = {'feedbackAjaxCFC':'/cfcs/feedback.cfc','send2friendAjaxCFC':'/cfcs/send2friend.cfc','registerAjaxCFC':'/cfcs/register.cfc','userAjaxCFC':'/cfcs/userAjax.cfc','mdrCFC':'/cfcs/MDR.cfc','_jsscriptFolder':'/js/ajax','debug':false};	
</script>
<script type="text/javascript" src="/js/ajax/ajax.js"></script>
<script  type="text/javascript" src="/js/dimmingDIV.js"></script>
<script type="text/javascript" src="/js/simpleValidation.js"></script>
<script type="text/javascript" src="/js/register.js"></script>
<script type="text/javascript" src="/js/de-ui.js"></script>
<script type="text/javascript" language="javascript">
	fwdURL = '#url.forwardURL#';
</script>

<form method="post" name="registrationForm" id="registrationForm">
<div id="content-single">
	<h2>Siemens Stem Academy Registration</h2>
	
	<cfif StructKeyExists(client, "STEMRegistrationGUID") AND IsValid("guid", client.STEMRegistrationGUID)>
		<div style="color:##2222cc; font-size:16px; font-weight:bold; margin-bottom:10px;">
			Please confirm that the below information is correct.
		</div>
	</cfif>
	
	<div style="display:none;" id="tblErrMessageTable">
		All fields are required.<br />Required fields have incomplete or incorrect data provided.<br />Please make corrections and submit again.
	</div>
	<br /><br />

	<h3 class="icon-title">
		<img src="/images/icons-person.gif" alt="icon" />
		<span>Tell Us About Yourself</span>
	</h3>
	<div class="registrationSection">
		<div class="formRow">
			<div class="err">
				<span id="span_firstName"></span>
				&nbsp;
			</div>
			<label for="firstName">First Name:</label>
			<div class="formInput"> 
				<input type="text" class="topLeft" name="firstName" id="firstName" value="#form.firstName#" style="width:160px;" onChange="hideError(this.name);" />
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="formRow">
			<div class="err">
				<span id="span_lastName"></span>
				&nbsp;
			</div>
			<label for="lastName">Last Name:</label>
			<div class="formInput"> 
				<input type="text" class="topLeft" name="lastName" id="lastName" value="#form.lastName#" style="width:160px;" onChange="hideError(this.name)" />
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="formRow">
			<div class="err">
				<span id="span_title"></span>
				&nbsp;
			</div>
			<label for="title">Title:</label>
			<div class="formInput"> 
				
				<cfset titles = [] />
				<cfset ArrayAppend(titles, "Administrator") />
				<cfset ArrayAppend(titles, "Curriculum Specialist") />
				<cfset ArrayAppend(titles, "Department Head") />
				<cfset ArrayAppend(titles, "Higher Education") />
				<cfset ArrayAppend(titles, "Information Technology Coordinator") />
				<cfset ArrayAppend(titles, "Media Library Specialist") />
				<cfset ArrayAppend(titles, "Pre-service Student") />
				<cfset ArrayAppend(titles, "Principal") />
				<cfset ArrayAppend(titles, "Teacher") />
				<cfset ArrayAppend(titles, "Technology Administrator") />
				<cfset ArrayAppend(titles, "Technology Support") />
				<cfset ArrayAppend(titles, "Other") />
				
				<select class="topLeft" name="title" id="title" style="width:160px;" onChange="hideError(this.name);">
					<option value=""> - choose one -</option>
					<cfloop from="1" to="#ArrayLen(titles)#" index="i">
						<option value="#titles[i]#" <cfif form.title eq titles[i]>selected="selected"</cfif>>#titles[i]#</option>
					</cfloop>
				</select>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="formRow">
			<div class="err">
				<span id="span_phone"></span>
				&nbsp;
			</div>
			<label for="areaPhone">Phone Number:</label>
			<div class="formInput"> 
				<input type="text" class="topLeft" name="areaPhone" id="areaPhone" style="width:30px;" value="#form.areaPhone#" maxlength="3" onKeyUp="tabPhoneNext(this,'prePhone')" />
				<input type="text" style="position:absolute;top:0px;left:40px;width:30px;" name="prePhone" id="prePhone" value="#form.prePhone#" maxlength="3" onKeyUp="tabPhoneNext(this,'phone')" />
				<input type="text" style="position:absolute;top:0px;left:80px;width:80px;" name="phone" id="phone" value="#form.phone#" maxlength="10" onChange="hideError(this.name);" />
			</div>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
	<br /><br />
	
	<h3 class="icon-title">
		<img src="/images/icons-apple.gif" alt="icon" />
		<span>Tell Us About Your Teaching Experience</span>
	</h3>
	<div class="registrationSection">
		
		<div class="formRow">
			<div class="err">
				<span id="span_locationType"></span>
				&nbsp;
			</div>
			<div class="formInput">
				Location of your school:
				<div class="teachingFields">
					<div class="locationType">
						<input type="radio" name="locationType" id="locationType" onclick="hideError(this.name);" value="Urban" <cfif form.locationType eq "Urban">checked="checked"</cfif>  />
						Urban
					</div>
					<div class="locationType">
						<input type="radio" name="locationType" onclick="hideError(this.name);" value="Suburban" <cfif form.locationType eq "Suburban">checked="checked"</cfif> />
						Suburban
					</div>
					<div class="locationType">
						<input type="radio" name="locationType" onclick="hideError(this.name);" value="Rural" <cfif form.locationType eq "Rural">checked="checked"</cfif> />
						Rural
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		
		<div class="formRow">
			<div class="err">
				<span id="span_grades"></span>
				&nbsp;
			</div>
			<div class="formInput">
				Which grade level(s) do you teach? (Select all that apply) 
				<div class="teachingFields">
					<div class="grades">
						<input type="checkbox" name="grades" id="grades" onclick="hideError(this.name);" value="gradeK" <cfif form.gradeK eq true>checked="checked"</cfif> />
						K
					</div>
					<cfloop from="1" to="12" index="i">
						<div class="grades">
							<input type="checkbox" name="grades" onclick="hideError(this.name);" value="grade#i#" <cfif form["grade" & i] eq true>checked="checked"</cfif> />
							#i#
						</div>
					</cfloop>
					
					<div class="clear">&nbsp;</div>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		
		<div class="formRow">
			<div class="err">
				<span id="span_subjects"></span>
				&nbsp;
			</div>
			<div class="formInput">
				Which subject(s) do you teach? (Select all that apply)
				<div class="teachingFields">
					<div class="subjects">
						<input type="checkbox" name="subjects" id="subjects" onclick="hideError(this.name);" value="Science" <cfif form.subjectScience eq true>checked="checked"</cfif> />
						Science
					</div>
					<div class="subjects">
						<input type="checkbox" name="subjects" onclick="hideError(this.name);" value="Technology" <cfif form.subjectTechnology eq true>checked="checked"</cfif> />
						Technology
					</div>
					<div class="subjects">
						<input type="checkbox" name="subjects" onclick="hideError(this.name);" value="Engineering" <cfif form.subjectEngineering eq true>checked="checked"</cfif> />
						Engineering
					</div>
					<div class="subjects">
						<input type="checkbox" name="subjects" onclick="hideError(this.name);" value="Math" <cfif form.subjectMath eq true>checked="checked"</cfif> />
						Math
					</div>
					<div class="subjects">
						<input type="checkbox" name="subjects" onclick="hideError(this.name);" value="Integrated" <cfif form.subjectIntegrated eq true>checked="checked"</cfif> />
						Integrated
					</div>
					<div class="subjects">
						<input type="checkbox" name="subjects" onclick="hideError(this.name);" value="Other" <cfif form.subjectOther eq true>checked="checked"</cfif> />
						Other
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		
		<div class="formRow">
			<div class="err">
				<span id="span_studentCount"></span>
				&nbsp;
			</div>
			<div class="formInput">
				Total number of students you teach: 
				<div class="teachingFields">
					<div class="studentCount">
						<input type="radio" name="studentCount" id="studentCount" onclick="hideError(this.name);" value="1-5" <cfif form.studentCount eq "1-5">checked="checked"</cfif> />
						1-5
					</div>
					<div class="studentCount">
						<input type="radio" name="studentCount" onclick="hideError(this.name);" value="6-10" <cfif form.studentCount eq "6-10">checked="checked"</cfif> />
						6-10
					</div>
					<div class="studentCount">
						<input type="radio" name="studentCount" onclick="hideError(this.name);" value="11-15" <cfif form.studentCount eq "11-15">checked="checked"</cfif> />
						11-15
					</div>
					<div class="studentCount">
						<input type="radio" name="studentCount" onclick="hideError(this.name);" value="16-20" <cfif form.studentCount eq "16-20">checked="checked"</cfif> />
						16-20
					</div>
					<div class="studentCount">
						<input type="radio" name="studentCount" onclick="hideError(this.name);" value="21+" <cfif form.studentCount eq "21+">checked="checked"</cfif> />
						21 or more
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		
		<div class="formRow">
			<div class="err">
				<span id="span_yearsTeaching"></span>
				&nbsp;
			</div>
			<div class="formInput">
				Total number of years of teaching experience: 
				<div class="teachingFields">
					<div class="yearsTeaching">
						<input type="radio" name="yearsTeaching" id="yearsTeaching" onclick="hideError(this.name);" value="1-5" <cfif form.yearsTeaching eq "1-5">checked="checked"</cfif> />
						1-5
					</div>
					<div class="yearsTeaching">
						<input type="radio" name="yearsTeaching" onclick="hideError(this.name);" value="6-10" <cfif form.yearsTeaching eq "6-10">checked="checked"</cfif> />
						6-10
					</div>
					<div class="yearsTeaching">
						<input type="radio" name="yearsTeaching" onclick="hideError(this.name);" value="11-15" <cfif form.yearsTeaching eq "11-15">checked="checked"</cfif> />
						11-15
					</div>
					<div class="yearsTeaching">
						<input type="radio" name="yearsTeaching" onclick="hideError(this.name);" value="16-20" <cfif form.yearsTeaching eq "16-20">checked="checked"</cfif> />
						16-20
					</div>
					<div class="yearsTeaching">
						<input type="radio" name="yearsTeaching" onclick="hideError(this.name);" value="21-24" <cfif form.yearsTeaching eq "21-24">checked="checked"</cfif> />
						21-24
					</div>
					<div class="yearsTeaching">
						<input type="radio" name="yearsTeaching" onclick="hideError(this.name);" value="25+" <cfif form.yearsTeaching eq "25+">checked="checked"</cfif> />
						25 or more
					</div>
					<div class="clear">&nbsp;</div>
				</div>                    
			</div>
		</div>
		
	</div>
	
	<h3 class="icon-title">
		<img src="/images/icons-building.gif" alt="icon" />
		<span>Tell Us About Your School</span>
	</h3>
	<div class="registrationSection">
		<div class="formRow" style="margin-bottom:0px;">
			<div class="err">
				<span id="span_schoolName"></span>
				&nbsp;
			</div>
			<div class="formInput" style="margin-left:8px;">
				<a href="##" id="btn-findschool" class="sprite" onClick="getSchoolInfo(); return false;"></a>&nbsp;&nbsp;
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="formRow" style="margin-bottom:0px;">
			<div class="err">&nbsp;</div>
			<div class="formInput" style="margin-left:8px;">
				<div id="tblSchoolInfo" <cfif form.schoolName eq "">style="display:none;"</cfif>>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="2"><span id="span_schoolName_text" style="font-weight:bold;">
								<cfif form.schoolName eq "">
									<i>- none -</i></span>
								<cfelse>
									#form.schoolName#
								</cfif>
							</td>
						</tr>
						<tr>
							<td width="98">Country:</td>
							<td><b><span id="span_schoolCountry">#form.schoolCountry#</span></b></td>
						</tr>
						<tr>
							<td>Address:</td>
							<td><b><span id="span_schoolAddress">#form.schoolAddress#</span></b></td>
						</tr>
						<tr>
							<td>City: </td>
							<td><b><span id="span_schoolCity">#form.schoolCity#</span></b></td>
						</tr>
						<tr>
							<td>State/Province:</td>
							<td><b><span id="span_schoolState">#form.schoolState#</span></b></td>
						</tr>
						<tr>
							<td>Zip/Postal Code: </td>
							<td><b><span id="span_schoolZipCode">#form.schoolZipCode#</span></b></td>
						</tr>
						<tr>
							<td>Phone Number:</td>
							<td><b><span id="span_schoolPhone">#form.schoolPhone#</span></b></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
	<br /><br />
	
	<h3 class="icon-title">
		<img src="/images/icons-computer.gif" alt="icon" />
		<span>Create Your Username/Password to Login</span>
	</h3>
	<div class="registrationSection">
		<div class="formRow">
			<div class="err">&nbsp;</div>
			<em>Please use a valid email address as your username.</em>
		</div>
		<div class="formRow">
			<div class="err">
				<span id="span_email"></span>
				&nbsp;
			</div>
			<label for="email">Email Address:</label>
			<div class="formInput"> 
				<input type="text" class="topLeft" name="email" id="email" value="#form.email#" style="width:160px;" maxlength="255" onchange="hideError(this.name); validateRegistrationEmail();" /> <!--- onChange="emailChanged(this);" --->
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="formRow">
			<div class="err">
				<span id="span_confirmEmail"></span>
				&nbsp;
			</div>
			<label for="confirmEmailirmEmail">Confirm Email Address:</label>
			<div class="formInput"> 
				<input type="text" class="topLeft" name="confirmEmail" id="confirmEmail" value="#form.email#" style="width:160px;" maxlength="255" onchange="hideError(this.name); validateRegistrationEmail();" /> <!--- onChange="confirmEmails();" onBlur="confirmEmails();" --->
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="formRow">
			<div class="err">
				<span id="span_password"></span>
				&nbsp;
			</div>
			<label for="password">Password:</label>
			<div class="formInput"> 
				<input type="password" class="topLeft" name="password" id="passwordField" value="" style="width:160px;" maxlength="255" onChange="hideError(this.name); validateRegistrationPassword();" /> <!--- validatePassword(this.value) --->
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="formRow">
			<div class="err">
				<span id="span_confirmPassword"></span>
				&nbsp;
			</div>
			<label for="confirmPassword">Confirm Password:</label>
			<div class="formInput"> 
				<input type="password" class="topLeft" name="confirmPassword" id="confirmPassword" value="" style="width:160px;" maxlength="255" onChange="hideError(this.name); validateRegistrationPassword();" /> <!--- validateConfirmPassword(); --->
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="formRow">
			<div class="err">
				<span id="span_SECURITY_QUESTION_GUID"></span>
				&nbsp;
			</div>
			<label for="SECURITY_QUESTION_GUID">Security Reminder Question:</label>
			<div class="formInput">
				<select name="SECURITY_QUESTION_GUID" id="SECURITY_QUESTION_GUID" size="1" onChange="hideError(this.name);">
					<option value="">- choose one -</option>
					<cfloop query="application.qReminderQuestions">
						<option value="#application.qReminderQuestions.SECURITY_QUESTION_GUID#" <cfif form.SECURITY_QUESTION_GUID eq application.qReminderQuestions.SECURITY_QUESTION_GUID>selected="selected"</cfif>>#application.qReminderQuestions.QUESTION#</option>
					</cfloop>
				</select>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="formRow">
			<div class="err">
				<span id="span_SECURITY_QUESTION_ANSWER"></span>
				&nbsp;
			</div>
			<label for="SECURITY_QUESTION_ANSWER">Security Reminder Answer:</label>
			<div class="formInput">
				<input type="text" class="topLeft" name="SECURITY_QUESTION_ANSWER" id="SECURITY_QUESTION_ANSWER"  value="#form.SECURITY_QUESTION_ANSWER#" style="width:160px;" maxlength="100" onChange="hideError(this.name);" />
			</div>
			<div class="clear">&nbsp;</div>
		</div>
          <!--- opt-in --->
          <div class="formRow">
			<div class="err">
				<span id="span_de_updates"></span>
				&nbsp;
			</div>
			<label for="de_updates">Please check this box if you would like to receive communications from redacted Education and its partners.</label>
			<div class="formInput">
				<input class="topLeft" type="checkbox" name="de_updates" id="de_updates" onclick="hideError(this.name);" value="1" <cfif form.de_updates eq true>checked="checked"</cfif> />
			</div>
			<div class="clear">&nbsp;</div>
		</div>
          <!--- end opt-in --->
	</div>
	<br /><br />
	
	<div class="registrationSection">
		<div class="formRow">
            <cfif StructKeyExists(client, "STEMRegistrationGUID") AND IsValid("guid", client.STEMRegistrationGUID)>
				<input type="hidden" name="registrationGUID" value="#client.STEMRegistrationGUID#" />
			</cfif>
			
			<input type="hidden" name="APP_USER_GUID" id="APP_USER_GUID" value="#request.APP_USER_GUID#" />
			<input type="hidden" name="PID" id="PID" value="#form.PID#" />
			<input type="hidden" name="schoolCountry" id="schoolCountry" value="#form.schoolCountry#" />
			<input type="hidden" name="schoolName" id="schoolName"  value="#form.schoolName#" />
			<input type="hidden" name="schoolAddress" id="schoolAddress" value="#form.schoolAddress#" />
			<input type="hidden" name="schoolCity" id="schoolCity" value="#form.schoolCity#" />
			<input type="hidden" name="schoolState" id="schoolState" value="#form.schoolState#" />
			<input type="hidden" name="schoolZipCode" id="schoolZipCode" value="#form.schoolZipCode#" />
			<input type="hidden" name="schoolPhone" id="schoolPhone" value="#form.schoolPhone#" />
			
			<input id="btnSubmit" class="sprite" type="button" title="Submit" value="" style="border:0px; cursor:pointer; margin-left:240px;" onclick="validateRegistrationForm();" />
			<span id="processingForm" style="display:none; margin:10px 0px 10px 225px;"><img src="/images/working.gif" /></span>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
	
</div>
<br clear="all" />
</form>
</cfoutput>