<!---
Copyright 2008-2010 Barney Boisvert (bboisvert@gmail.com).

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--->
<cfcomponent output="false">

    <cffunction name="init" access="public" output="false" returntype="amazons3">
        <cfargument name="awsKey" type="string" required="true" />
        <cfargument name="awsSecret" type="string" required="true" />
        <cfargument name="localCacheDir" type="string" required="false"
            hint="If omitted, no local caching is done.  If provided, this directory is used for local caching of S3 assets.  Note that if local caching is enabled, this CFC assumes it is the only entity managing the S3 storage and therefore that S3 never needs to be checked for updates (other than those made though this CFC).  If you update S3 via other means, you cannot safely use the local cache." />
        <cfset variables.awsKey = awsKey />
        <cfset variables.awsSecret = awsSecret />
        <cfset variables.useLocalCache = structKeyExists(arguments, "localCacheDir") />
        <cfset variables.simpleDateFormat = createObject("java", "java.text.SimpleDateFormat").init("yyyy-MM-dd'T'HH:mm:ss.SSSZ") />
        <cfif useLocalCache>
            <cfset variables.localCacheDir = localCacheDir />
            <cfif NOT directoryExists(localCacheDir)>
                <cfdirectory action="create"
                    directory="#localCacheDir#" />
            </cfif>
        </cfif>
        <cfreturn this />
    </cffunction>



    <cffunction name="s3Url" access="public" output="false" returntype="string">
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="objectKey" type="string" required="true" />
        <cfargument name="requestType" type="string" default="vhost"
            hint="Must be one of 'regular', 'ssl', 'vhost', or 'cname'.  'Vhost' and 'cname' are only valid if your bucket name conforms to the S3 virtual host conventions, and cname requires a CNAME record configured in your DNS." />
        <cfargument name="timeout" type="numeric" default="900"
            hint="The number of seconds the URL is good for.  Defaults to 900 (15 minutes)." />
        <cfargument name="verb" type="string" default="GET"
            hint="The HTTP verb to use.  Only GET (the default) and HEAD make sense." />
        <cfscript>
            var expires = int(getTickCount() / 1000) + timeout;
            var signature = "";
            var destUrl = "";

            signature = getRequestSignature(
                uCase(verb),
                bucket,
                objectKey,
                expires
            );
            if (requestType EQ "ssl" OR requestType EQ "regular") {
                destUrl = "http" & iif(requestType EQ "ssl", de("s"), de("")) & "://s3.amazonaws.com/#listAppend(bucket, objectKey, '/')#?AWSAccessKeyId=#variables.awsKey#&Signature=#urlEncodedFormat(signature)#&Expires=#expires#";
            } else if (requestType EQ "cname") {
                destUrl = "http://#bucket#/#objectKey#?AWSAccessKeyId=#variables.awsKey#&Signature=#urlEncodedFormat(signature)#&Expires=#expires#";
            } else { // vhost
                destUrl = "http://#bucket#.s3.amazonaws.com/#objectKey#?AWSAccessKeyId=#variables.awsKey#&Signature=#urlEncodedFormat(signature)#&Expires=#expires#";
            }

            return destUrl;
        </cfscript>
    </cffunction>



    <cffunction name="listBuckets" access="public" output="false" returntype="query">
        <cfset var result = "" />
        <cfset var xml = "" />
        <cfset var n = "" />
        <cfset var md = structNew() />
        <cfset var name = "" />

        <cfset md.executionTime = getTickCount() />
        <cfhttp url="#s3Url('', '', 'regular')#"
            result="result" />
        <cfset xml = xmlParse(result.fileContent) />
        <cfset result = queryNew("name,directory,size,type,dateLastModified,attributes,mode,bucket,objectKey", "varchar,varchar,integer,varchar,date,varchar,varchar,varchar,varchar") />
        <cfloop array="#xml.xmlRoot.Buckets.Bucket#" index="n">
            <cfset name = n.Name.xmlText />
            <cfset queryAddRow(result) />
            <cfset querySetCell(result, "type", "Dir") />
            <cfset querySetCell(result, "name", name) />
            <cfset querySetCell(result, "bucket", name) />
            <cfset querySetCell(result, "directory", '/') />
            <cfset querySetCell(result, "dateLastModified", ripDate(n.CreationDate.xmlText)) />
        </cfloop>
        <cftry>
            <cfset result.getMetadata().setExtendedMetadata(md) />
            <cfset md.cached = false />
            <cfset md.executionTime = getTickCount() - md.executionTime />
            <cfset md.columnList = result.columnList />
            <cfset md.recordCount = result.recordCount />
            <cfcatch type="any">
                <!--- no worries.  This is a bonus feature where it is supported, which isn't everywhere. --->
            </cfcatch>
        </cftry>
        <cfreturn result />
    </cffunction>



    <cffunction name="listObjects" access="public" output="false" returntype="query">
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="prefix" type="string" default="" />
        <cfargument name="delimiter" type="string" default="/" />
        <cfargument name="startAfter" type="string" required="false"
            hint="If provided, the listing will start with the first object key after this one.  If omitted, the listing will start with the first key.  Keys are ordered alphabetically.  This corresponds to the 'marker' parameter in the raw S3 API." />
        <cfargument name="maxResults" type="numeric" default="1000"
            hint="Limits the number of results returned.  The S3 default is 1000 rows.  This corresponds to the 'max-keys' parameter in the raw S3 API." />
        <cfset var result = "" />
        <cfset var xml = "" />
        <cfset var n = "" />
        <cfset var name = "" />
        <cfset var dirs = createObject("java", "java.util.HashSet").init() />
        <cfset var dir = delimiter & listPrepend(prefix, bucket, delimiter) />
        <cfset var prefixLacksTrailingDelimiter = false />
        <cfset var requestUrl = s3Url(bucket, '', 'regular') />
        <cfset var md = structNew() />

        <cfset md.executionTime = getTickCount() />
        <cfif prefix EQ delimiter>
            <cfset prefix = "" />
        <cfelseif right(prefix, 1) EQ delimiter>
            <cfset prefix = left(prefix, len(prefix) - 1) />
        </cfif>
        <cfset prefixLacksTrailingDelimiter = len(prefix) GT 0 AND right(prefix, 1) NEQ delimiter />
        <cfset requestUrl &= "&prefix=" & prefix />
        <cfif prefixLacksTrailingDelimiter>
            <cfset requestUrl &= delimiter />
        </cfif>
        <cfset requestUrl &= "&delimiter=" & delimiter />
        <cfset requestUrl &= "&max-keys=" & maxResults />
        <cfif structKeyExists(arguments, "startAfter") AND len(trim(startAfter)) GT 0>
            <cfif prefix NEQ '' AND left(startAfter, len(prefix)) NEQ prefix>
                <cfset startAfter = prefix & iif(prefixLacksTrailingDelimiter, 'delimiter', de('')) & startAfter />
            </cfif>
            <cfset requestUrl &= "&marker=" & startAfter />
        </cfif>
        <cfhttp url="#requestUrl#"
            result="result" />
        <cfset xml = xmlParse(result.fileContent) />
        <cfset result = queryNew("name,directory,size,type,dateLastModified,attributes,mode,bucket,objectKey", "varchar,varchar,integer,varchar,date,varchar,varchar,varchar,varchar") />
        <cfloop array="#xml.xmlRoot.xmlChildren#" index="n">
            <cfif n.xmlName EQ "Contents">
                <cfset name = n.Key.xmlText />
                <cfif len(prefix) GT 0>
                    <cfset name = removeChars(name, 1, len(prefix) + 1) />
                </cfif>
                <cfif NOT dirs.contains(name) AND right(name, 9) NEQ "_$folder$">
                    <cfset queryAddRow(result) />
                    <cfset querySetCell(result, "type", "File") />
                    <cfset querySetCell(result, "name", name) />
                    <cfset querySetCell(result, "bucket", bucket) />
                    <cfset querySetCell(result, "objectKey", n.Key.xmlText) />
                    <cfset querySetCell(result, "directory", dir) />
                    <cfset querySetCell(result, "size", n.Size.xmlText) />
                    <cfset querySetCell(result, "dateLastModified", ripDate(n.LastModified.xmlText)) />
                </cfif>
            <cfelseif n.xmlName EQ "CommonPrefixes">
                <cfset name = n.Prefix.xmlText />
                <cfset name = left(name, len(name) - 1) />
                <cfif len(prefix) GT 0>
                    <cfset name = removeChars(name, 1, len(prefix) + 1) />
                </cfif>
                <cfset dirs.add(name) />
                <cfset queryAddRow(result) />
                <cfset querySetCell(result, "type", "Dir") />
                <cfset querySetCell(result, "name", name) />
                <cfset querySetCell(result, "bucket", bucket) />
                <cfset querySetCell(result, "directory", dir) />
            <cfelse>
                <!--- add this to the metadata structure --->
                <cfset md[n.xmlName] = n.xmlText />
            </cfif>
        </cfloop>
        <cftry>
            <cfset result.getMetadata().setExtendedMetadata(md) />
            <cfset md.cached = false />
            <cfset md.executionTime = getTickCount() - md.executionTime />s
            <cfset md.columnList = result.columnList />
            <cfset md.recordCount = result.recordCount />
            <cfcatch type="any">
                <!--- no worries.  This is a bonus feature where it is supported, which isn't everywhere. --->
            </cfcatch>
        </cftry>
        <cfreturn result />
    </cffunction>

    <cffunction name="asyncPutFileOnS3" returntype="void" output="false">
        <cfset var thread = "" />

        <cfthread action="run" name="asyncPutFileOnS3_#arguments.localFilePath#" priority="HIGH" args="#arguments#">
            <cfset thread.s3Result = putFileOnS3(argumentCollection=attributes.args) />
            <cflog text="#serializeJSON(thread)#" file="amazons3" type="information" />
        </cfthread>

    </cffunction>

    <cffunction name="putFileOnS3" access="public" output="false" returntype="struct"
            hint="I put a file on S3, and return the HTTP response from the PUT">
        <cfargument name="localFilePath" type="string" required="true" />
        <cfargument name="contentType" type="string" required="true"  />
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="objectKey" type="string" required="true" />
        <cfargument name="isPublic" type="boolean" default="false" />
        <cfset var gmtNow = dateAdd("s", getTimeZoneInfo().utcTotalOffset, now()) />
        <cfset var dateValue = dateFormat(gmtNow, "ddd, dd mmm yyyy") & " " & timeFormat(gmtNow, "HH:mm:ss") & " GMT" />
        <cfset var signature = getRequestSignature(
            "PUT",
            bucket,
            objectKey,
            dateValue,
            contentType,
            '',
            iif(isPublic, de('x-amz-acl:public-read'), '')
        ) />
        <cfset var content = "" />
        <cfset var result = "" />
        <cffile action="readbinary"
            file="#localFilePath#"
            variable="content" />
        <cfhttp url="http://s3.amazonaws.com/#bucket#/#objectKey#"
            method="PUT"
            result="result">
            <cfhttpparam type="header" name="Date" value="#dateValue#" />
            <cfhttpparam type="header" name="Authorization" value="AWS #variables.awsKey#:#signature#" />
            <cfhttpparam type="header" name="Content-Type" value="#contentType#" />
            <cfif isPublic>
                <cfhttpparam type="header" name="x-amz-acl" value="public-read" />
            </cfif>
            <cfhttpparam type="body" value="#content#" />
        </cfhttp>
        <cfset deleteCacheFor(bucket, objectKey) />
        <cfreturn result />
    </cffunction>



    <cffunction name="s3FileExists" access="public" output="false" returntype="boolean">
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="objectKey" type="string" required="true" />
        <cfset var result = "" />
        <cfif useLocalCache>
            <cfif fileExists(cacheFilenameFromBucketAndKey(bucket, objectKey))>
                <cfreturn true />
            </cfif>
        </cfif>
        <cfhttp url="#s3Url(bucket, objectKey, 'regular', 30, 'HEAD')#"
            method="HEAD"
            timeout="30"
            throwonerror="false"
            result="result" />
        <cfreturn val(trim(result.statusCode)) EQ 200 />
    </cffunction>



    <cffunction name="deleteS3File" access="public" output="false" returntype="void">
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="objectKey" type="string" required="true" />
        <cfset deleteS3FileInternal(bucket, objectKey, 0) />
    </cffunction>



    <cffunction name="deleteS3FileInternal" access="private" output="false" returntype="void">
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="objectKey" type="string" required="true" />
        <cfargument name="attemptCount" type="numeric" required="true" />
        <cfset var result = "" />
        <cfset var retry = false />
        <cfhttp url="#s3Url(bucket, objectKey, 'regular', 30, 'DELETE')#"
            method="DELETE"
            timeout="15"
            throwonerror="false"
            result="result">
            <cfhttpparam type="header" name="Content-Type" value="" />
        </cfhttp>
        <cfif result.statusCode LT 200 OR result.statusCode GTE 300>
            <cfset retry = attemptCount LT 3 AND (
                (isXml(result.fileContent)
                    AND result.fileContent CONTAINS "<Code>InternalError</Code>"
                )
                OR (result.fileContent EQ "Connection Timeout"
                )
            )>
            <cfif retry>
                <cfset deleteS3FileInternal(bucket, objectKey, attemptCount + 1) />
            <cfelse>
                <cfdump var="#arguments#" />
                <cfdump var="#result#" />
                <cfabort showerror="error deleting file from S3" />
            </cfif>
        </cfif>
        <cfset deleteCacheFor(bucket, objectKey) />
    </cffunction>



    <cffunction name="getFileFromS3" access="public" output="false" returntype="string"
            hint="Brings a file from S3 down local, and returns the fully qualified local path">
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="objectKey" type="string" required="true" />
        <cfargument name="localFilePath" type="string" required="false"
            hint="If omitted a temp file will be created" />
        <cfset var filepath = "" />
        <cfset var cachePath = "" />
        <cfif structKeyExists(arguments, "localFilePath")>
            <cfset filepath = localFilePath />
        <cfelse>
            <cfset filepath = getTempFile(getTempDirectory(), "s3") />
        </cfif>
        <cfif useLocalCache>
            <cfset cachePath = cacheFilenameFromBucketAndKey(bucket, objectKey) />
            <cfif fileExists(cachePath)>
                <cfset fileCopy(cachePath, filepath) />
                <cfreturn filepath />
            </cfif>
        </cfif>
        <cftry>
            <cfhttp url="#s3Url(bucket, objectKey, 'regular', 30)#"
                timeout="30"
                throwonerror="true"
                getasbinary="yes"
                file="#getFileFromPath(filepath)#"
                path="#getDirectoryFromPath(filepath)#" />
            <cfcatch type="any">
                <!--- try again, exactly the same --->
                <cfhttp url="#s3Url(bucket, objectKey, 'regular', 30)#"
                    timeout="30"
                    throwonerror="true"
                    getasbinary="yes"
                    file="#getFileFromPath(filepath)#"
                    path="#getDirectoryFromPath(filepath)#" />
            </cfcatch>
        </cftry>
        <cfif useLocalCache>
            <cfset fileCopy(filepath, cachePath) />
        </cfif>
        <cfreturn filepath />
    </cffunction>



    <cffunction name="deleteCacheFor" access="public" output="false" returntype="void">
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="objectKey" type="string" required="true" />
        <cfset var cachePath = "" />
        <cfif useLocalCache>
            <cfset cachePath = cacheFilenameFromBucketAndKey(bucket, objectKey) />
            <cfif fileExists(cachePath)>
                <cfset fileDelete(cachePath) />
            </cfif>
        </cfif>
    </cffunction>



    <cffunction name="getRequestSignature" access="private" output="false" returntype="string">
        <cfargument name="verb" type="string" required="true" />
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="objectKey" type="string" required="true" />
        <cfargument name="dateOrExpiration" type="string" required="true" />
        <cfargument name="contentType" type="string" default="" />
        <cfargument name="contentMd5" type="string" default="" />
        <cfargument name="canonicalizedAmzHeaders" type="string" default=""
            hint="A newline-delimited list of headers, in lexographical order, duplicates collapsed, and no extraneous whitespace.  See Amazon's description of 'CanonicalizedAmzHeaders' for specifics." />
        <cfscript>
            var stringToSign = "";
            var algo = "HmacSHA1";
            var signingKey = "";
            var mac = "";
            var signature = "";

            stringToSign = uCase(verb) & chr(10)
                & contentMd5 & chr(10)
                & contentType & chr(10)
                & dateOrExpiration & chr(10)
                & iif(len(canonicalizedAmzHeaders) GT 0, de(canonicalizedAmzHeaders & chr(10)), de(''))
                & "/" & listAppend(bucket, objectKey, "/");
            signingKey = createObject("java", "javax.crypto.spec.SecretKeySpec").init(variables.awsSecret.getBytes(), algo);
            mac = createObject("java", "javax.crypto.Mac").getInstance(algo);
            mac.init(signingKey);
            signature = toBase64(mac.doFinal(stringToSign.getBytes()));

            return signature;
        </cfscript>
    </cffunction>



    <cffunction name="cacheFilenameFromBucketAndKey" access="private" output="false" returntype="string">
        <cfargument name="bucket" type="string" required="true" />
        <cfargument name="objectKey" type="string" required="true" />
        <cfreturn localCacheDir & "/" & REReplace(bucket & "/" & objectKey, "[^a-zA-Z0-9.-]+", "_", "all") />
    </cffunction>



    <cffunction name="ripDate" access="private" output="false" returntype="date">
        <cfargument name="amzDate" type="string" required="true" />
        <cfreturn simpleDateFormat.parse(replace(amzDate, 'Z', '-0000')) />
    </cffunction>

</cfcomponent>