<cfsilent>

	<cfset counter = 0 />
	
	<cfset results = StructNew() />
	
	<cfset filters = "*.3g2|*.3gp|*.asf|*.asx|*.avi|*.flv|*.m4v|*.mov|*.mp4|*.mpeg|*.mpg|*.qt|*.ram|*.rm|*.wmv" />
	
	<cfset srcDirectory = ExpandPath('/assets/stem/') />
	<cfset dstDirectory = ExpandPath('/assets/stem/flvs/') />

	<cfdirectory action="list" directory="#srcDirectory#" name="qFiles" filter="#filters#" />

	<cfif NOT DirectoryExists(dstDirectory)>
	
		<cfdirectory action="create" directory="#dstDirectory#" />
	
	</cfif>

		
</cfsilent>

<cfoutput>

<cfloop query="qFiles">
	       
	<cfset guid = ListFirst(name,".") />
	<cfset fileExt = ListLast(name,".") />
	<cfset mp4 = "#guid#.mp4" />
	<cfset srcPath = "#qFiles.directory#/#qFiles.name#" />
	<cfset dstPath = "#dstDirectory##mp4#" />

	<cfif IsValid("guid",guid)>
	
		<cfif NOT FileExists(dstPath)>

			<cfquery name="qryCheck" datasource="Sponsorships">
								
				SELECT fileGuid, result, encoding_id
				FROM cms_encodings
				WHERE fileGuid = <cfqueryparam value="#guid#" CFSQLType="cf_sql_idstamp" />				
								
			</cfquery>

			<cfif NOT qryCheck.RecordCount>

			
				<cfinclude template="encode.cfm"/>

						<cfquery name="qCreate" datasource="Sponsorships">
						INSERT INTO cms_encodings
							(
							fileGuid,
							source,
							destination,
							result,
							dtCreated,
							ipCreated
							)
						VALUES
							(
							<cfqueryparam value="#guid#" CFSQLType="cf_sql_idstamp" null="#not len(guid)#" />,
							<cfqueryparam value="#srcPath#" CFSQLType="cf_sql_varchar" null="#not len(srcPath)#" />,
							<cfqueryparam value="#dstPath#" CFSQLType="cf_sql_varchar" null="#not len(dstPath)#" />,
							<cfqueryparam value="Submitted" CFSQLType="cf_sql_longvarchar" />,
							getDate(),
							<cfqueryparam value="#CGI.REMOTE_ADDR#" />
							)
						</cfquery>

		    <cfelse>

		    	<cfif qryCheck.result IS 'Submitted'>
		    		
		    		<cfinclude template="encode.cfm"/>

						<cfquery name="qCreate" datasource="Sponsorships">
							UPDATE cms_encodings
							SET result = <cfqueryparam value="ReSubmitted" CFSQLType="cf_sql_longvarchar" />
							WHERE encoding_id = <cfqueryparam value="#qryCheck.encoding_id#" cfsqltype="cf_sql_integer"/>
						</cfquery>
		    	
		    	</cfif>  
			</cfif>
		</cfif>
	</cfif>
</cfloop>

</cfoutput>