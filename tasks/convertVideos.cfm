<cfsilent>
	<cfset counter = 0 />
	<cfset results = StructNew() />
	<cfset filters = "*.3g2|*.3gp|*.asf|*.asx|*.avi|*.flv|*.m4v|*.mov|*.mp4|*.mpeg|*.mpg|*.qt|*.ram|*.rm|*.wmv" />
	<cfset srcDirectory = ExpandPath('/assets/stem/') />
	<cfset dstDirectory = ExpandPath('/assets/stem/flvs/') />
	
	<cfdirectory action="list" directory="#srcDirectory#" name="qFiles" filter="#filters#" />
	<cfif NOT DirectoryExists(dstDirectory)>
		<cfdirectory action="create" directory="#dstDirectory#" />
	</cfif>
</cfsilent>
<cfoutput>
	<cfloop query="qFiles">
		<cfset guid = ListFirst(name,".") />
		<cfset flv = "#guid#.flv" />
		<cfset srcPath = "#qFiles.directory#/#qFiles.name#" />
		<cfset dstPath = "#dstDirectory##flv#" />
		<cfif IsValid("guid",guid)>
			<cfset movie = CreateObject("component","ffmpeg.ffmpeg").init(srcPath) />
			<cfif NOT FileExists(dstPath)>
				<cfset results = movie.transcodeToFlv(dstPath) />
				Converted #srcPath# to #dstPath#<br />
				#results.errorLog#<br/>
				<cfset counter++ />
				<cfquery name="qCreate" datasource="Sponsorships">
				INSERT INTO cms_encodings
					(
					fileGuid,
					source,
					destination,
					result,
					dtCreated,
					ipCreated
					)
				VALUES
					(
					<cfqueryparam value="#guid#" CFSQLType="cf_sql_idstamp" null="#not len(guid)#" />,
					<cfqueryparam value="#srcPath#" CFSQLType="cf_sql_varchar" null="#not len(srcPath)#" />,
					<cfqueryparam value="#dstPath#" CFSQLType="cf_sql_varchar" null="#not len(dstPath)#" />,
					<cfqueryparam value="#results.errorlog#" CFSQLType="cf_sql_longvarchar" null="#not len(results.errorlog)#" />,
					getDate(),
					<cfqueryparam value="#CGI.REMOTE_ADDR#" />
					)
				</cfquery>
			</cfif>
		</cfif>
	</cfloop>
	#qFiles.RecordCount# files scanned. <br />
	#counter# files converted.
</cfoutput>