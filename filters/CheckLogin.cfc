<cfcomponent extends="MachII.framework.EventFilter" displayname="Security Filter" output="false">
	
	<!---
	PROPERTIES
	--->
	
	<!---
	INITIALIZATION / CONFIGURATION
	--->
	<cffunction name="configure" access="public" returntype="void" output="false"
		hint="Configures the filter.">
		<!--- Put custom configuration for this filter here. --->
	</cffunction>	
	
	<!---
	PUBLIC FUNCTIONS
	--->
	<cffunction name="filterEvent" access="public" output="false" returntype="boolean">
		<cfargument name="event" type="MachII.framework.Event" required="true" />
		<cfargument name="eventContext" type="MachII.framework.EventContext" required="true" />
		
		<cfset var proceed = false />
		<cfset var objFindUser = "" />
		<cfset var message = "" />
		<cfset var loggedOnUserId = "00000000-0000-0000-0000-000000000000" />
		<cfset var uid = "" />
		<cfset var pwd = "" />
		
		<cfif StructKeyExists(cookie,"app_user_guid") AND IsValid("guid",cookie.app_user_guid) AND cookie.app_user_guid NEQ "00000000-0000-0000-0000-000000000000">
			<cfif StructKeyExists(cookie, "blnAuthenticatedStem")>
				<cfset proceed = true />
			<cfelse>
				<cfset objFindUser = CreateObject("component","commonCFC.GlobalServicesExt").serviceAppUserLogonRequest(loggedOnUserId,uid,pwd,cookie.app_user_guid) />

				<cfif StructKeyExists(objFindUser, "app_user_guid") AND IsValid("guid", objFindUser.app_user_guid)>
				    <cfquery name="qWebsites" datasource="#getProperty('sDsn')#">
						SELECT
							wu.siteId
						FROM	cms_websiteUsers wu
						WHERE	wu.app_user_guid = <cfqueryparam value="#objFindUser.app_user_guid#" />
					</cfquery>
					
					<cfset client.siteList = ValueList(qWebsites.siteId) />
			
					<cfif qWebsites.RecordCount GT 0>
						<cfloop collection="#objFindUser#" item="key">
							<cfset "client.#key#" = StructFind(objFindUser, key) />
						</cfloop>
						<cfset client.hitcount = 1 />
					
				        <cfcookie name="first_name" value="#objFindUser.FIRST_NAME#"  />
        				<cfcookie name="last_name" value="#objFindUser.LAST_NAME#"  />
						<cfcookie name="blnAuthenticatedStem" value="true"  />
					
						<cfset proceed = true />
					<cfelse>
						<cfset message = "You do not have access to this site.<br />" />
						<cfset arguments.event.setArg("message", message) />
						<cfset announceEvent("showLoginForm", arguments.event.getArgs()) />
					</cfif>
				<cfelse>
					<cfset arguments.event.setArg("message", message) />
					<cfset announceEvent("showLoginForm", arguments.event.getArgs()) />
				</cfif>
			</cfif>
		<cfelse>
			<cfset arguments.event.setArg("message", message) />
			<cfset announceEvent("showLoginForm", arguments.event.getArgs()) />
		</cfif>
		
		<cfreturn proceed />
	</cffunction>
</cfcomponent>