
<cfcomponent displayname="judging" output="false">

	<!----------------------------->
	<!--- Initialize variables  --->
	<!----------------------------->

	<cfset variables = {} />
	<cfset variables.settings = {} />



	<!----------------------------->
	<!--- Setup Functions       --->
	<!----------------------------->

	<cffunction name="Init" access="public" returntype="any" output="true">

		<cfset variables.settings.dsn = GetDSN() />

		<cfreturn this />
	</cffunction>



	<!----------------------------->
	<!--- Public Functions      --->
	<!----------------------------->

	<cffunction name="AttachJudgeScoresToQuery" access="public" returntype="query" output="false" hint="Would be nicer if it was a query but this does work">
		<cfargument name="apps" type="query" required="true" hint="Must have approvedGUID defined in query" />
		<cfargument name="appType" type="string" required="true" />

		<cfset var appScores = "" />
		<cfset var avg = 0 />
		<cfset var scores = GetFinalistSubmittedScores(appType) />

		<cfset QueryAddColumn(arguments.apps, "judge1GUID", "VarChar", ArrayNew(1)) />
		<cfset QueryAddColumn(arguments.apps, "judge1FirstName", "VarChar", ArrayNew(1)) />
		<cfset QueryAddColumn(arguments.apps, "judge1LastName", "VarChar", ArrayNew(1)) />
		<cfset QueryAddColumn(arguments.apps, "judge1Score", "Double", ArrayNew(1)) />
		<cfset QueryAddColumn(arguments.apps, "judge1Comment", "VarChar", ArrayNew(1)) />
        <cfset QueryAddColumn(arguments.apps, "judge1AppStatus", "VarChar", ArrayNew(1)) />

		<cfif arguments.appType eq "Institute"  >
			<cfset QueryAddColumn(arguments.apps, "judge2GUID", "VarChar", ArrayNew(1)) />
			<cfset QueryAddColumn(arguments.apps, "judge2FirstName", "VarChar", ArrayNew(1)) />
			<cfset QueryAddColumn(arguments.apps, "judge2LastName", "VarChar", ArrayNew(1)) />
			<cfset QueryAddColumn(arguments.apps, "judge2Score", "Double", ArrayNew(1)) />
			<cfset QueryAddColumn(arguments.apps, "judge2Comment", "VarChar", ArrayNew(1)) />
			<cfset QueryAddColumn(arguments.apps, "judge2AppStatus", "VarChar", ArrayNew(1)) />
		</cfif>

		<cfset QueryAddColumn(arguments.apps, "judgeScoreAvg", "Double", ArrayNew(1)) />

		<cfloop query="arguments.apps">
			<cfquery name="appScores" dbtype="query">
				SELECT * FROM scores
				WHERE approvedGUID = <cfqueryparam value="#arguments.apps.approvedGUID#" cfsqltype="cf_sql_varchar" />
				ORDER BY submitted
			</cfquery>

			<cfif appType eq "Institute">
				<cfif appScores.RecordCount eq 0>
					<cfset QueryAddRow(appScores) />
					<cfset QueryAddRow(appScores) />
				<cfelseif appScores.RecordCount eq 1>
					<cfset QueryAddRow(appScores) />
				</cfif>

				<cfset avg = 0 />

				<cfset arguments.apps.judge1GUID[apps.CurrentRow] = appScores.judgeGUID[1] />
				<cfset arguments.apps.judge1FirstName[apps.CurrentRow] = appScores.firstName[1] />
				<cfset arguments.apps.judge1LastName[apps.CurrentRow] = appScores.lastName[1] />
				<cfset arguments.apps.judge1Score[apps.CurrentRow] = appScores.scoreTotalWeighted[1] />
				<cfset arguments.apps.judge1Comment[apps.CurrentRow] = appScores.comment[1] />
				<cfset arguments.apps.judge1AppStatus[apps.CurrentRow] = appScores.status[1] />
				<cfif IsNumeric(appScores.scoreTotalWeighted[1])>
					<cfset avg = appScores.scoreTotalWeighted[1] />
				</cfif>

				<cfset arguments.apps.judge2GUID[apps.CurrentRow] = appScores.judgeGUID[2] />
				<cfset arguments.apps.judge2FirstName[apps.CurrentRow] = appScores.firstName[2] />
				<cfset arguments.apps.judge2LastName[apps.CurrentRow] = appScores.lastName[2] />
				<cfset arguments.apps.judge2Score[apps.CurrentRow] = appScores.scoreTotalWeighted[2] />
				<cfset arguments.apps.judge2Comment[apps.CurrentRow] = appScores.comment[2] />
				<cfset arguments.apps.judge2AppStatus[apps.CurrentRow] = appScores.status[2] />
				<cfif IsNumeric(appScores.scoreTotalWeighted[2])>
					<cfset avg = (avg + appScores.scoreTotalWeighted[2])/2 />
				</cfif>

				<cfif IsNumeric(appScores.scoreTotalWeighted[1])>
					<cfset arguments.apps.judgeScoreAvg[apps.CurrentRow] = avg />
				</cfif>

			<cfelseif appType eq "STARs">
				<cfif appScores.RecordCount eq 0>
					<cfset QueryAddRow(appScores) />
				</cfif>

				<cfset arguments.apps.judge1GUID[apps.CurrentRow] = appScores.judgeGUID[1] />
				<cfset arguments.apps.judge1FirstName[apps.CurrentRow] = appScores.firstName[1] />
				<cfset arguments.apps.judge1LastName[apps.CurrentRow] = appScores.lastName[1] />
				<cfset arguments.apps.judge1Score[apps.CurrentRow] = appScores.scoreTotalWeighted[1] />
				<cfset arguments.apps.judge1Comment[apps.CurrentRow] = appScores.comment[1] />
				<cfset arguments.apps.judge1AppStatus[apps.CurrentRow] = appScores.status[1] />
				<cfif IsNumeric(appScores.scoreTotalWeighted[1])>
					<cfset arguments.apps.judgeScoreAvg[apps.CurrentRow] = appScores.scoreTotalWeighted[1] />
				<cfelse>
					<cfset arguments.apps.judgeScoreAvg[apps.CurrentRow] = 0 />
				</cfif>
			</cfif>

		</cfloop>


		<cfreturn arguments.apps />
	</cffunction>


	<cffunction name="BuildButton" access="public" returntype="string" output="false">
		<cfargument name="id" type="string" required="true">
		<cfargument name="displayText" type="string" required="true" />
		<cfargument name="onClickJSRun" type="string" required="true" hint="run this javascript when clicked.  Pass in a function to call." />

		<cfset var buttonHTML = "" />

		<cfset arguments.onClickJSRun = Replace(arguments.onClickJSRun, """", "'", "all") />

		<cfoutput>
		<cfsavecontent variable="buttonHTML">
			<div class="button" id="#arguments.id#" onmouseover="this.className='button buttonHover';" onmouseout="this.className='button';" onclick="#arguments.onClickJSRun#">
				<div class="buttonLeft">
					&nbsp;
				</div>
				<div class="buttonMiddle">
					#arguments.displayText#
				</div>
				<div class="buttonRight">
					&nbsp;
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div class="clear">&nbsp;</div>
		</cfsavecontent>
		</cfoutput>

		<cfreturn buttonHTML />
	</cffunction>


	<cffunction name="CommentToCollegeBoard" access="public" returntype="void" output="false">
		<cfargument name="judgeGUID" type="string" required="false" />
		<cfargument name="approvedGUID" type="string" required="false" />
		<cfargument name="aform" type="struct" required="false" />

		<cfset var emailTo = "" />
		<cfset var getAdmins = "" />
		<cfset var getDisplayID = "" />
		<cfset var getJudge = "" />
		<cfset var savePending = "" />
	<cftry>

		<cfquery name="getJudge" datasource="#GetDSN()#">
			SELECT * FROM STEMAppsJudges (nolock)
			WHERE judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfquery name="getDisplayID" datasource="#GetDSN()#">
			SELECT displayID FROM STEMAppsJudgingApproved (nolock)
			WHERE approvedGUID = <cfqueryparam value="#arguments.approvedGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfquery name="savePending" datasource="#GetDSN()#">
			UPDATE STEMAppsJudgeScores
			SET status = <cfqueryparam value="Pending" cfsqltype="cf_sql_varchar" />,
				pendingComment = <cfqueryparam value="#arguments.aform.pendingComment#" cfsqltype="cf_sql_longvarchar" />
			WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
					approvedGUID = <cfqueryparam value="#arguments.approvedGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfquery name="getAdmins" datasource="#GetDSN()#">
			SELECT email FROM STEMAppsJudges (nolock)
			WHERE 	judgeYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" /> 
			AND
				<cfif client.STEMJudgeType eq "STARs">
						type IN ('adminPreStar','adminFinalStar')
				<cfelse>
						type IN ('adminPreInst','adminFinalInst')
				</cfif>
            AND email is not null
		</cfquery>

		<cfloop query="getAdmins">
			<cfset emailTo = getAdmins.email />
			<cfif FindNoCase("stage", cgi.server_name)>
				<cfset emailTo = "daryl_walsh@redacted.com,daryl_walsh@redacted.com" />
			</cfif>

			<cfmail subject="Siemens STEM Academy Judging - New Pending Application" from="stemJudging@redacted.com" to="#emailTo#" type="html">
				<div style="margin:10px; width:600px;">
				<cfif FindNoCase("stage", cgi.server_name)>
				This email would normally go to <b>#getAdmins.email#</b><br><br>
				</cfif>
				
					A new pending application has been created: <br /><br />
					Display ID ###getDisplayID.displayID#<br />
					Judge Name: #getJudge.firstName# #getJudge.lastName#<br />
					Judge Comment: #arguments.aform.pendingComment#<br /><br />

					<a href="http://#cgi.server_name#/index.cfm?event=judge.setupFinalists">Login</a>
					<!---
                    <a href="http://#cgi.server_name#/index.cfm?event=judge.recuseApplication&approve=1&appID=#getDisplayID.displayID#&judgeGUID=#getJudge.judgeGUID#">Approve request for recusal</a><br><br>
                    <a href="http://#cgi.server_name#/index.cfm?event=judge.recuseApplication&approve=0&appID=#getDisplayID.displayID#&judgeGUID=#getJudge.judgeGUID#">Deny request for recusal</a>
					--->
				</div>
			</cfmail>
	
		</cfloop>
<cfcatch></cfcatch></cftry>

	</cffunction>


	<cffunction name="ReturntoOriginalJudge" access="public" returntype="void" output="false">
		<cfargument name="aform" type="struct" required="true" />

		<cfset var changeJudge = "" />
		<cfset var getJudge = "" />


		<cfquery name="changeJudge" datasource="#GetDSN()#">
          UPDATE STEMAppsJudgeScores
             SET status = <cfqueryparam value="ToBeJudged" cfsqltype="cf_sql_varchar" />
           WHERE approvedGUID = <cfqueryparam value="#arguments.aform.approvedGUID#" cfsqltype="cf_sql_varchar" />
           AND judgeGUID = <cfqueryparam value="#arguments.aform.oldJudge#" cfsqltype="cf_sql_varchar" />
		</cfquery>

          <cfquery name="getJudge" datasource="#GetDSN()#">
          SELECT *
          FROM STEMAppsJudges
          WHERE judgeGUID = <cfqueryparam value="#arguments.aform.oldJudge#" cfsqltype="cf_sql_varchar" />
		</cfquery>

          <cfset emailTo = getJudge.email />
          <cfif FindNoCase("stage", cgi.server_name)>
			<cfset emailTo = "daryl_walsh@redacted.com,daryl_walsh@redacted.com" />
		  </cfif>
          <cfmail subject="Siemens STEM Academy Judging - Application Reassigned Back to You" from="Siemens STEM Academy Judging <stemJudging@redacted.com>" to="#emailTo#" type="html">
               <div style="margin:10px; width:600px;">
			   <cfif FindNoCase("stage", cgi.server_name)>
				This email would normally go to <b>#getJudge.email#</b><br><br>
				</cfif>
                    This application has been sent back to you: <br /><br />

                    Display ID: ###arguments.aform.displayID#<br />
                    Judge Name: #getJudge.firstName# #getJudge.lastName#<br />
                    <p>Comment from Admin:<br>
                    #arguments.aform.message#</p>

               <a href="http://#cgi.server_name#/index.cfm?event=judge.showLogin">Login</a><br><br>
               </div>
          </cfmail>

	</cffunction>

	<cffunction name="GetAdministratorCounts" access="public" returntype="query" output="false">
		<cfargument name="appType" type="string" required="true" hint="Institute OR STARs" />

		<cfset var getCounts = "" />


		<cfquery name="getCounts" datasource="#GetDSN()#">
			SELECT totalApps.cnt AS totalAppsCnt, judges.cnt AS judgesCnt, pending.cnt AS pendingCnt, submitted.cnt AS submittedCnt
			FROM	(
						SELECT COUNT (*) AS cnt, 1 AS link
						FROM STEMAppsJudgingApproved (nolock)
						WHERE 	type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
								isApproved = <cfqueryparam value="1" cfsqltype="cf_sql_integer" /> AND
								applicationYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" />
					) AS totalApps
				LEFT JOIN (

							SELECT COUNT(*) AS cnt, 1 AS link
							FROM STEMAppsJudges (nolock)
							WHERE 	type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
									judgeYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" />
						) AS judges
				ON judges.link = totalApps.link
					LEFT JOIN (
									SELECT COUNT(js.status) AS cnt, 1 AS link
									FROM STEMAppsJudgeScores AS js (nolock)
										INNER JOIN STEMAppsJudgingApproved AS ja (nolock)
										ON js.approvedGUID = ja.approvedGUID
									WHERE 	ja.type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
											ja.isApproved = <cfqueryparam value="1" cfsqltype="cf_sql_integer" /> AND
											js.status = <cfqueryparam value="Pending" cfsqltype="cf_sql_varchar" /> AND
											ja.applicationYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" />
							) AS pending
					ON pending.link = totalApps.link
						LEFT JOIN (
										SELECT COUNT(js.status) AS cnt, 1 AS link
										FROM STEMAppsJudgeScores AS js (nolock)
											INNER JOIN STEMAppsJudgingApproved AS ja (nolock)
											ON js.approvedGUID = ja.approvedGUID
										WHERE 	ja.type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
												ja.isApproved = <cfqueryparam value="1" cfsqltype="cf_sql_integer" /> AND
												js.status = <cfqueryparam value="Scored" cfsqltype="cf_sql_varchar" /> AND
												js.submitted IS NOT NULL AND
												ja.applicationYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" />
								) AS submitted
						ON submitted.link = totalApps.link
		</cfquery>


		<cfreturn getCounts />
	</cffunction>


	<cffunction name="GetApplication" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />

		<cfset var app = "" />

		<cfquery name="app" datasource="#GetDSN()#">
			SELECT 	videoType,
					(
						SELECT TOP 1 videoFileName FROM STEMAppsVideoFiles (nolock)
						WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
						ORDER BY created DESC
					) AS videoFileName, videoUTube,
					essaySTARs1, essaySTARs2, essaySTARs3,
					essaySTEMInstitute1, essaySTEMInstitute2, essaySTEMInstitute3, essayAdditionalInformation,
					(
						SELECT TOP 1 fileName FROM STEMAppsRecommendationFiles (nolock)
						WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
						ORDER BY created DESC
					) AS recommendationFileName
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfreturn app />
	</cffunction>

     <cffunction name="GetRecommendation" access="public" returntype="query" output="false">
		<cfargument name="approvedGUID" type="string" required="true" />

		<cfset var recSheet = "" />

		<cfquery name="recSheet" datasource="#GetDSN()#">
			SELECT TOP 1 rs.*, ja.applicationGUID, ja.approvedGUID
			FROM STEMAppsRecommendationSheets (nolock) AS rs INNER JOIN STEMAppsJudgingApproved (nolock) AS ja
               ON rs.applicationGUID = ja.applicationGUID AND ja.approvedGUID = <cfqueryparam value="#arguments.approvedGUID#" cfsqltype="cf_sql_varchar" />
               ORDER BY rs.created DESC
		</cfquery>

		<cfreturn recSheet />
	</cffunction>


	<cffunction name="GetApplicationToBeScored" access="public" returntype="query" output="false">
		<cfargument name="judgeGUID" type="string" required="true" />
		<cfargument name="appType" type="string" required="true" />

		<cfset var createScore = "" />
		<cfset var getApp = GetApplications(arguments.judgeGUID, arguments.appType, "ToBeJudged") />
		<cfset var getCounts = "" />
		<cfset var getNext = "" />		

		<cfreturn getApp />
	</cffunction>


	<cffunction name="GetApplications" access="public" returntype="query" output="false">
		<cfargument name="judgeGUID" type="string" required="true" />
		<cfargument name="appType" type="string" required="true" />
		<cfargument name="appStatus" type="string" required="true" />

		<cfset var getApps = "" />


		<cfquery name="getApps" datasource="#GetDSN()#"> <!--- adding AS displayID as workaround - db caching something? --->
			SELECT js.*, ja.displayID AS displayID
			FROM STEMAppsJudgeScores AS js (nolock)
				INNER JOIN STEMAppsJudgingApproved AS ja (nolock)
				ON ja.approvedGUID = js.approvedGUID
			WHERE	js.judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
					js.status = <cfqueryparam value="#arguments.appStatus#" cfsqltype="cf_sql_varchar" /> AND
					ja.type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
                         ja.applicationYear = <cfqueryparam cfsqltype="cf_sql_integer" value="#request.contestYear#" />
			ORDER BY cast(ja.displayID as int)
		</cfquery>


		<cfreturn getApps />
	</cffunction>


	<cffunction name="GetApplicationsCounts" access="public" returntype="query" output="false">
		<cfargument name="judgeGUID" type="string" required="true" />

		<cfset var getCounts = "" />

		<cfquery name="getCounts" datasource="#GetDSN()#">
			SELECT 	requiredToScore,
					<!--- (
						SELECT COUNT(*)
						FROM STEMAppsJudgeScores (nolock)
						WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
								status = <cfqueryparam value="Pending" cfsqltype="cf_sql_varchar" /> AND
                                        created >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date">
					) AS pendingCnt, --->
					(
						Select count(*)
						From STEMAppsJudgeScores with (nolock)
						Where judgeGUID = '#arguments.judgeGUID#'
						and status != 'Scored'
					) as remainingCnt,
					(
						SELECT COUNT(*)
						FROM STEMAppsJudgeScores (nolock)
						WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
								status = <cfqueryparam value="Scored" cfsqltype="cf_sql_varchar" /> AND
                                        created >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date"> AND
								submitted IS NULL
					) AS toSubmitCnt,
					(
						SELECT COUNT(*)
						FROM STEMAppsJudgeScores (nolock)
						WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
								status = <cfqueryparam value="Scored" cfsqltype="cf_sql_varchar" /> AND
                                        created >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date"> AND
								submitted IS NOT NULL
					) AS submittedCnt
			FROM STEMAppsJudges (nolock)
			WHERE judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfreturn getCounts />
	</cffunction>


	<cffunction name="GetApprovedApplication" access="public" returntype="query" output="false">
		<cfargument name="approvedGUID" type="string" required="true" />

		<cfset var approvedApp = "" />

		<cfquery name="approvedApp" datasource="#GetDSN()#">
			SELECT applicationGUID, displayID, type
			FROM STEMAppsJudgingApproved (nolock)
			WHERE approvedGUID = <cfqueryparam value="#arguments.approvedGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfreturn approvedApp />
	</cffunction>


	<cffunction name="GetFinalists" access="public" returntype="query" output="false">
		<cfargument name="appType" type="string" required="true" />

		<cfset var apps = "" />

		<cfquery name="apps" datasource="#GetDSN()#">
			SELECT approvedGUID, applicationGUID, displayID, isFinalist, isAlternate
			FROM STEMAppsJudgingApproved (nolock)
			WHERE 	isApproved = <cfqueryparam value="1" cfsqltype="cf_sql_integer" /> AND
					type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
                         applicationYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" />
			ORDER BY cast(displayID as int)
		</cfquery>

		<cfreturn apps />
	</cffunction>


	<cffunction name="GetFinalistsFull" access="public" returntype="query" output="false">
		<cfargument name="appType" type="string" required="true" />

		<cfset var app = "" />

		<cfquery name="apps" datasource="#GetDSN()#">
SELECT 
ja.displayID AS Applicant_ID,
a.programInstitute, a.programSTARsOR, a.programSTARsPN, a.programPreference1, a.programPreference2, su.FIRSTNAME AS First_Name,
su.LASTNAME AS Last_Name,SchoolName,SchoolAddress,SchoolAddress2,schoolCity,schoolState,schoolZipCode,su.schoolPhone AS School_Phone_Number,
su.EMAIL AS Primary_Email,su.gradeK,su.grade1,su.grade2,su.grade3,su.grade4,su.grade5,su.grade6,su.grade7,su.grade8,su.grade9,su.grade10,su.grade11,su.grade12,su.subjectScience,su.subjectTechnology,su.subjectEngineering,su.subjectMath,su.subjectIntegrated,su.subjectOther,su.studentCount,
				 '="' + su.yearsTeaching + '"' AS YearsTeaching,				 
CASE WHEN a.appliedSTEMInPast = 0 THEN NULL ELSE 'X' END AS AppliedSTEMInPast,
CASE WHEN mdr.ENROLLMENT is NULL
THEN 'n/a'
ELSE CAST(CAST(mdr.ENROLLMENT AS Integer) AS varchar(10))
END AS Total_number_of_students,
CASE WHEN mdr.LOWGRADE is NULL
THEN 'n/a'
ELSE
CASE WHEN mdr.LOWGRADE = mdr.HIGHGRADE
THEN
CASE WHEN mdr.HIGHGRADE = 'PK'
THEN 'Pre K'
WHEN mdr.HIGHGRADE = 'K'
THEN 'K'
WHEN mdr.HIGHGRADE = '01'
THEN '1st'
WHEN mdr.HIGHGRADE = '02'
THEN '2nd'
WHEN mdr.HIGHGRADE = '03'
THEN '3rd'
WHEN mdr.HIGHGRADE = '04'
THEN '4th'
WHEN mdr.HIGHGRADE = '05'
THEN '5th'
WHEN mdr.HIGHGRADE = '06'
THEN '6th'
WHEN mdr.HIGHGRADE = '07'
THEN '7th'
WHEN mdr.HIGHGRADE = '08'
THEN '8th'
WHEN mdr.HIGHGRADE = '09'
THEN '9th'
WHEN mdr.HIGHGRADE = '10'
THEN '10th'
WHEN mdr.HIGHGRADE = '11'
THEN '11th'
WHEN mdr.HIGHGRADE = '12'
THEN '12th'
WHEN mdr.HIGHGRADE = '13'
THEN '13th'
WHEN mdr.HIGHGRADE = '14'
THEN '14th'
WHEN mdr.HIGHGRADE = '15'
THEN '15th'
WHEN mdr.HIGHGRADE = '16'
THEN '16th'
WHEN mdr.HIGHGRADE = '17'
THEN '17th'
ELSE CAST(mdr.HIGHGRADE AS varchar(10))
END
ELSE
CASE WHEN mdr.LOWGRADE = 'PK'
THEN 'Pre K'
WHEN mdr.LOWGRADE = 'K'
THEN 'K'
WHEN mdr.LOWGRADE = '01'
THEN '1st'
WHEN mdr.LOWGRADE = '02'
THEN '2nd'
WHEN mdr.LOWGRADE = '03'
THEN '3rd'
WHEN mdr.LOWGRADE = '04'
THEN '4th'
WHEN mdr.LOWGRADE = '05'
THEN '5th'
WHEN mdr.LOWGRADE = '06'
THEN '6th'
WHEN mdr.LOWGRADE = '07'
THEN '7th'
WHEN mdr.LOWGRADE = '08'
THEN '8th'
WHEN mdr.LOWGRADE = '09'
THEN '9th'
WHEN mdr.LOWGRADE = '10'
THEN '10th'
WHEN mdr.LOWGRADE = '11'
THEN '11th'
WHEN mdr.LOWGRADE = '12'
THEN '12th'
WHEN mdr.LOWGRADE = '13'
THEN '13th'
WHEN mdr.LOWGRADE = '14'
THEN '14th'
WHEN mdr.LOWGRADE = '15'
THEN '15th'
WHEN mdr.LOWGRADE = '16'
THEN '16th'
WHEN mdr.LOWGRADE = '17'
THEN '17th'
ELSE CAST(mdr.LOWGRADE AS varchar(10))
END
+ ' - ' +
CASE WHEN mdr.HIGHGRADE = 'PK'
THEN 'Pre K'
WHEN mdr.HIGHGRADE = 'K'
THEN 'K'
WHEN mdr.HIGHGRADE = '01'
THEN '1st'
WHEN mdr.HIGHGRADE = '02'
THEN '2nd'
WHEN mdr.HIGHGRADE = '03'
THEN '3rd'
WHEN mdr.HIGHGRADE = '04'
THEN '4th'
WHEN mdr.HIGHGRADE = '05'
THEN '5th'
WHEN mdr.HIGHGRADE = '06'
THEN '6th'
WHEN mdr.HIGHGRADE = '07'
THEN '7th'
WHEN mdr.HIGHGRADE = '08'
THEN '8th'
WHEN mdr.HIGHGRADE = '09'
THEN '9th'
WHEN mdr.HIGHGRADE = '10'
THEN '10th'
WHEN mdr.HIGHGRADE = '11'
THEN '11th'
WHEN mdr.HIGHGRADE = '12'
THEN '12th'
WHEN mdr.HIGHGRADE = '13'
THEN '13th'
WHEN mdr.HIGHGRADE = '14'
THEN '14th'
WHEN mdr.HIGHGRADE = '15'
THEN '15th'
WHEN mdr.HIGHGRADE = '16'
THEN '16th'
WHEN mdr.HIGHGRADE = '17'
THEN '17th'
ELSE CAST(mdr.HIGHGRADE AS varchar(10))
END
END
END AS What_grades_does_your_school_serve,
CASE WHEN ft.FileTypeDescription is NULL
THEN 'n/a'
ELSE ft.FileTypeDescription
END AS Type_of_school,

CASE WHEN nces.LUNCHPRGM is NULL
THEN 'n/a'
WHEN CAST(nces.TOTSTNDTS AS Integer) = 0
THEN 'n/a'
WHEN nces.LUNCHPRGM = '999999'
THEN 'n/a'
ELSE CAST((100*CAST(nces.LUNCHPRGM AS float)/CAST(nces.TOTSTNDTS AS float)) AS varchar(20))
END AS Percent_free_or_reduced_lunch,

CASE WHEN nces.WHITE is NULL
THEN 'n/a'
WHEN CAST(nces.TOTSTNDTS AS Integer) = 0
THEN 'n/a'
WHEN nces.WHITE = '999999'
THEN 'n/a'
ELSE CAST((100*CAST(nces.WHITE AS float)/CAST(nces.TOTSTNDTS AS float)) AS varchar(20))
END AS Percent_White,

CASE WHEN nces.BLACK is NULL
THEN 'n/a'
WHEN CAST(nces.TOTSTNDTS AS Integer) = 0
THEN 'n/a'
WHEN nces.BLACK = '999999'
THEN 'n/a'
ELSE CAST((100*CAST(nces.BLACK AS float)/CAST(nces.TOTSTNDTS AS float)) AS varchar(20))
END AS Percent_Black,

CASE WHEN nces.HISPANIC is NULL
THEN 'n/a'
WHEN CAST(nces.TOTSTNDTS AS Integer) = 0
THEN 'n/a'
WHEN nces.HISPANIC = '999999'
THEN 'n/a'
ELSE CAST((100*CAST(nces.HISPANIC AS float)/CAST(nces.TOTSTNDTS AS float)) AS varchar(20))
END AS Percent_Hispanic,

CASE WHEN nces.ASIANPACF is NULL
THEN 'n/a'
WHEN CAST(nces.TOTSTNDTS AS Integer) = 0
THEN 'n/a'
WHEN nces.ASIANPACF = '999999'
THEN 'n/a'
ELSE CAST((100*CAST(nces.ASIANPACF AS float)/CAST(nces.TOTSTNDTS AS float)) AS varchar(20))
END AS Percent_Asian,

CASE WHEN nces.INDALASK is NULL
THEN 'n/a'
WHEN CAST(nces.TOTSTNDTS AS Integer) = 0
THEN 'n/a'
WHEN nces.INDALASK = '999999'
THEN 'n/a'
ELSE CAST((100*CAST(nces.INDALASK AS float)/CAST(nces.TOTSTNDTS AS float)) AS varchar(20))
END AS Percent_Native_American,

CASE WHEN mdr.DISTSIZE is NULL
THEN 'n/a'
ELSE
CASE mdr.DISTSIZE
WHEN 'A' THEN '1 - 599'
WHEN 'B' THEN '600 - 1,199'
WHEN 'C' THEN '1,200 - 2,499'
WHEN 'D' THEN '2,500 - 4,999'
WHEN 'E' THEN '5,000 - 9,999'
WHEN 'F' THEN '10,000 - 24,999'
WHEN 'G' THEN '25,000 Or More'
WHEN 'H' THEN 'Unclassified'
ELSE 'n/a'
     END
END AS Total_Number_of_Students_in_District,
ja.isFinalist, ja.isAlternate,ja.approvedGUID,
a.teachingYears,a.currentPositionFrom,a.currentPositionTo,a.currentPositionSchool,a.currentPositionGrades,a.currentPositionGradesInSchool,a.currentPositionCourse,a.currentPositionNextYear,a.currentPositionNextYearNoExplain,a.siemensRecognized,a.principalSupport,a.principalSupportWhyNo,a.principalSupportPrefix,a.principalSupportFirstName,a.principalSupportLastName,a.principalSupportEmail,a.technologyKnowledge,a.technologyUseWikis,a.technologyUseBlogs,a.technologyUseSocialMedia,a.technologyUseStorytelling,a.technologyUseWeb2_0,a.referencePrefix,a.referenceFirstName,a.referenceMI,a.referenceLastName,a.referenceSchool,a.referenceTitle,a.referenceTitleOther,a.referenceRelationship,a.referenceRelationshipOther,a.videoType,a.essaySTARs1,a.essaySTARs2YN,a.essaySTARs2,a.essaySTARs3,a.essaySTEMInstitute1,a.essaySTEMInstitute2,a.essaySTEMInstitute3,a.essayAdditionalInformation,a.confirmRecommendation,a.confirmTermsConditions,a.confirmNoPlagiarize,a.programInstitute,a.programSTARsOR,a.programSTARsPN,a.programPreference1,a.programPreference2,a.areYouLegalResident,a.currentPositionFullTime,a.appliedSTEMInPast,a.appliedSTEMInPastValue,a.appliedSTEMInPastInstitute2010,a.appliedSTEMInPastInstitute2011,a.appliedSTEMInPastSTARs2010,a.appliedSTEMInPastSTARs2011,a.videoUTube,a.optionalDemographicsGender,a.optionalDemographicsEthnicity,
isDate((select top 1 created from STEMAppsRecommendationSheets with (nolock) where applicationguid = a.applicationguid)) AS hasRecommendationSheet

FROM STEMAppsJudgingApproved AS ja (nolock)
INNER JOIN STEMApplications AS a (nolock)
ON a.applicationGUID = ja.applicationGUID
     LEFT JOIN STEMRegistrations AS su (nolock)
     ON su.registrationGUID = a.registrationGUID
          LEFT JOIN [MDR].[dbo].MDR_MASTER_US_CURR AS mdr (nolock)
          ON mdr.PID = su.PID
               LEFT JOIN [MDR].[dbo].FileType AS ft (nolock)
               ON ft.FileTypeCode = mdr.FILETYPE
                    LEFT JOIN [MDR].[dbo].NCES AS nces (nolock)
                    ON nces.PID = mdr.pid
WHERE 	ja.isApproved = <cfqueryparam value="1" cfsqltype="cf_sql_integer" /> AND
     ja.applicationYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" /> AND
     ja.type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" />
ORDER BY cast(ja.displayID as int)
		</cfquery>

		<cfreturn apps />
	</cffunction>


	<cffunction name="GetFinalistSubmittedScores" access="public" returntype="query" output="false">
		<cfargument name="appType" type="string" required="true" />

		<cfset var apps = "" />

		<cfquery name="apps" datasource="#GetDSN()#">
			SELECT js.approvedGUID, js.judgeGUID, js.scoreTotalWeighted, js.submitted, js.comment, js.status, j.firstName, j.lastName
			FROM STEMAppsJudgeScores AS js (nolock)
				INNER JOIN STEMAppsJudgingApproved AS ja
				ON ja.approvedGUID = js.approvedGUID
					INNER JOIN STEMAppsJudges AS j (nolock)
					ON j.judgeGUID = js.judgeGUID
			WHERE	ja.type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" />
			ORDER BY js.submitted
		</cfquery>

		<cfreturn apps />
	</cffunction>


	<cffunction name="GetJudgeScores" access="public" returntype="query" output="false">
		<cfargument name="judgeGUID" type="string" required="true" />
		<cfargument name="approvedGUID" type="string" required="true" />

		<cfset var scores = "" />

		<cfquery name="scores" datasource="#GetDSN()#">
			SELECT * FROM STEMAppsJudgeScores AS js (nolock)
			WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
					approvedGUID = <cfqueryparam value="#arguments.approvedGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>


		<cfreturn scores />
	</cffunction>

	<cffunction name="IsLoggedIn" access="public" returntype="boolean" output="false">

		<cfset var retVal = false />

		<cfif StructKeyExists(client, "STEMJudgeType") AND client.STEMJudgeType neq "">
			<cfset retVal = true />
		</cfif>

		<cfreturn retVal />
	</cffunction>


	<cffunction name="Login" access="public" returntype="boolean" output="true">
		<cfargument name="strJudgeLoginUsername" type="string" required="true" />
		<cfargument name="strJudgeLoginPassword" type="string" required="true" />

		<cfset var checkLogin = "" />
		<cfset var loggedIn = false />

		<cfquery name="checkLogin" datasource="#GetDSN()#">
			SELECT judgeGUID, type, stemAdmin, firstName
			FROM STEMAppsJudges (nolock)
			WHERE judgeYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" /> AND
					isEnabled = <cfqueryparam value="1" cfsqltype="cf_sql_bit" /> AND
					userName = <cfqueryparam value="#arguments.strJudgeLoginUsername#" cfsqltype="cf_sql_varchar" /> AND
					password = <cfqueryparam value="#arguments.strJudgeLoginPassword#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfif checkLogin.RecordCount gt 0>
			<cfset client.STEMJudgeGUID = checkLogin.judgeGUID />
			<cfset client.STEMJudgeType = checkLogin.type />
			<cfset client.STEMAdmin = checkLogin.stemAdmin />
			<cfset client.STEMJudgeFirstName = checkLogin.firstName />
			<cfset loggedIn = true />
		</cfif>

		<cfreturn loggedIn />
	</cffunction>


	<cffunction name="Logout" access="public" returntype="void" output="false">

		<cfset client.STEMJudgeGUID = "" />
		<cfset client.STEMJudgeType = "" />
		<cfset client.STEMAdmin = "" />
		<cfset client.STEMJudgeFirstName = "" />

		<cfset StructKeyExists(client, "STEMJudgeGUID") />
		<cfset StructKeyExists(client, "STEMJudgeType") />
		<cfset StructKeyExists(client, "STEMAdmin") />
		<cfset StructKeyExists(client, "STEMJudgeFirstName") />

		<!--- A more potent way of clearing out cookies - 09.27.07 - RIS --->
		<cfloop collection="#cookie#" item="strCookieItem">
			<cfcookie name="#strCookieItem#" value="" expires="NOW"  />
		</cfloop>

	    <cfset StructClear(client) />
		<cfset client.hitcount = 1 />
	    <cfset StructClear(session) />

	</cffunction>


	<cffunction name="SaveJudgeScores" access="public" returntype="void" output="false">
		<cfargument name="judgeGUID" type="string" required="true" />
		<cfargument name="approvedGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />

		<cfset var saveScores = "" />
		<cfset var scoreTotal = arguments.aform.scoreVideo + arguments.aform.scoreEssay1 + arguments.aform.scoreEssay2 + arguments.aform.scoreLetter />
		<cfset var scoreTotalWeighted = 6*((0.25*arguments.aform.scoreVideo) + (0.30*arguments.aform.scoreEssay1) + (0.30*arguments.aform.scoreEssay2) + (0.15*arguments.aform.scoreLetter)) />

		<cfquery name="saveScores" datasource="#GetDSN()#">
			UPDATE STEMAppsJudgeScores
			SET status = <cfqueryparam value="Scored" cfsqltype="cf_sql_varchar" />,
				scoreVideo = <cfqueryparam value="#arguments.aform.scoreVideo#" cfsqltype="cf_sql_integer" />,
				scoreEssay1 = <cfqueryparam value="#arguments.aform.scoreEssay1#" cfsqltype="cf_sql_integer" />,
				scoreEssay2 = <cfqueryparam value="#arguments.aform.scoreEssay2#" cfsqltype="cf_sql_integer" />,
				scoreLetter = <cfqueryparam value="#arguments.aform.scoreLetter#" cfsqltype="cf_sql_integer" />,
				comment = <cfqueryparam value="#arguments.aform.comment#" cfsqltype="cf_sql_longvarchar" />,
				scoreTotal = <cfqueryparam value="#scoreTotal#" cfsqltype="cf_sql_integer" />,
				scoreTotalWeighted = <cfqueryparam value="#scoreTotalWeighted#" cfsqltype="cf_sql_float" />
			WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
					approvedGUID = <cfqueryparam value="#arguments.approvedGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

	</cffunction>


	<cffunction name="SavePendingScores" access="public" returntype="void" output="false">
		<cfargument name="judgeGUID" type="string" required="true" />
		<cfargument name="approvedGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />

		<cfset var saveScores = "" />
          <cfset var scoreTotal = "" />
          <cfset var scoreTotalWeighted = "" />

          <cfif structCount(aform) EQ 6>
          	<cfif isDefined("arguments.aform.scoreVideo") AND isDefined("arguments.aform.scoreEssay1") AND isDefined("arguments.aform.scoreEssay2") AND isDefined("arguments.aform.scoreLetter")>
				<cfset scoreTotal = arguments.aform.scoreVideo + arguments.aform.scoreEssay1 + arguments.aform.scoreEssay2 + arguments.aform.scoreLetter />
                    <cfset scoreTotalWeighted = 6*((0.25*arguments.aform.scoreVideo) + (0.30*arguments.aform.scoreEssay1) + (0.30*arguments.aform.scoreEssay2) + (0.15*arguments.aform.scoreLetter)) />
               <cfelse>
               	<cfset scoreTotal = 0 />
	               <cfset scoreTotalWeighted = 0 />
               </cfif>
          </cfif>

		<cfquery name="saveScores" datasource="#GetDSN()#">
			UPDATE STEMAppsJudgeScores
			SET status = <cfqueryparam value="#arguments.aform.newStatus#" cfsqltype="cf_sql_varchar" />,
				scoreVideo = <cfif isDefined("arguments.aform.scoreVideo")><cfqueryparam value="#arguments.aform.scoreVideo#" cfsqltype="cf_sql_integer" /><cfelse>NULL</cfif>,
				scoreEssay1 = <cfif isDefined("arguments.aform.scoreEssay1")><cfqueryparam value="#arguments.aform.scoreEssay1#" cfsqltype="cf_sql_integer" /><cfelse>NULL</cfif>,
				scoreEssay2 = <cfif isDefined("arguments.aform.scoreEssay2")><cfqueryparam value="#arguments.aform.scoreEssay2#" cfsqltype="cf_sql_integer" /><cfelse>NULL</cfif>,
				scoreLetter = <cfif isDefined("arguments.aform.scoreLetter")><cfqueryparam value="#arguments.aform.scoreLetter#" cfsqltype="cf_sql_integer" /><cfelse>NULL</cfif>,
				comment = <cfif isDefined("arguments.aform.comment")><cfqueryparam value="#arguments.aform.comment#" cfsqltype="cf_sql_longvarchar" /><cfelse>NULL</cfif>,
				scoreTotal = <cfif Len(scoreTotal)><cfqueryparam value="#scoreTotal#" cfsqltype="cf_sql_integer" /><cfelse>NULL</cfif>,
				scoreTotalWeighted = <cfif Len(scoreTotalWeighted)><cfqueryparam value="#scoreTotalWeighted#" cfsqltype="cf_sql_float" /><cfelse>NULL</cfif>
			WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
					approvedGUID = <cfqueryparam value="#arguments.approvedGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

	</cffunction>


	<cffunction name="SaveFinalists" access="public" returntype="void" output="false">
		<cfargument name="appType" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />

		<cfset var i = 0 />
		<cfset var update = "" />

		<cfquery name="update" datasource="#GetDSN()#">
			UPDATE STEMAppsJudgingApproved
			SET isFinalist = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
				isAlternate = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />
			WHERE type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfif StructKeyExists(arguments.aform, "isFinalist") AND arguments.aform.isFinalist neq "">
			<cfquery name="update" datasource="#GetDSN()#">
				UPDATE STEMAppsJudgingApproved
				SET isFinalist = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />
				WHERE 	type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
						approvedGUID IN (
											<cfloop from="1" to="#ListLen(arguments.aform.isFinalist)#" index="i">'#ListGetAt(arguments.aform.isFinalist,i)#'<cfif ListLen(arguments.aform.isFinalist) neq i>,</cfif></cfloop>
										)
			</cfquery>
		</cfif>

		<cfif StructKeyExists(arguments.aform, "isAlternate") AND arguments.aform.isAlternate neq "">
			<cfquery name="update" datasource="#GetDSN()#">
				UPDATE STEMAppsJudgingApproved
				SET isAlternate = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />
				WHERE 	type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
						approvedGUID IN (
											<cfloop from="1" to="#ListLen(arguments.aform.isAlternate)#" index="i">'#ListGetAt(arguments.aform.isAlternate,i)#'<cfif ListLen(arguments.aform.isAlternate) neq i>,</cfif></cfloop>
										)
			</cfquery>
		</cfif>

	</cffunction>

	<cffunction name="SendReminders" access="public" returntype="void" output="false">
		<cfargument name="appType" type="string" required="true" hint="Institute or STARs" />

		<cfset var emailTo = "" />
		<cfset var getJudgesToSend = "" />
		<cfset var submitCount = 0 />

		<cfquery name="getJudgesToSend" datasource="#GetDSN()#">
			SELECT j.firstName, j.lastName, j.email, j.requiredToScore, submitted.cnt AS submitCnt
			FROM STEMAppsJudges AS j (nolock)
				LEFT JOIN 	(
								SELECT judgeGUID, COUNT(*) AS cnt
								FROM STEMAppsJudgeScores AS js (nolock)
									INNER JOIN STEMAppsJudgingApproved AS ja (nolock)
									ON ja.approvedGUID = js.approvedGUID
								WHERE 	ja.type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
										js.status = <cfqueryparam value="Scored" cfsqltype="cf_sql_varchar" /> AND
										js.submitted IS NOT NULL AND
										ja.created >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />
								GROUP BY judgeGUID
							) AS submitted
				ON submitted.judgeGUID = j.judgeGUID
			WHERE 	j.type = <cfqueryparam value="#arguments.appType#" cfsqltype="cf_sql_varchar" /> AND
					j.judgeYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" />
		</cfquery>


		<cfloop query="getJudgesToSend">
			<cfset submitCount = 0 />
			<cfif IsNumeric(getJudgesToSend.submitCnt)>
				<cfset submitCount = getJudgesToSend.submitCnt />
			</cfif>

			<cfif submitCount lt getJudgesToSend.requiredToScore>
				<cfset emailTo = getJudgesToSend.email />
				 <cfif FindNoCase("stage", cgi.server_name)>
					<cfset emailTo = "daryl_walsh@redacted.com,daryl_walsh@redacted.com" />
				  </cfif>
          
			   
			<cftry>
				<cfmail subject="Siemens STEM Academy Judging Email Reminder" to="#emailTo#" from="Siemens STEM Academy Judging <stemJudging@redacted.com>" type="html">
					<div style="margin:10px; width:600px;">
					<cfif FindNoCase("stage", cgi.server_name)>
					This email would normally go to <b>#getJudgesToSend.email#</b><br><br>
					</cfif>
					
						Hello #getJudgesToSend.firstName#,
						<br /><br />

						This is a friendly reminder from the Siemens STEM Academy. We see that you have not finished submitting scores for the required number of STEM applications.
						<a href="http://#cgi.HTTP_HOST#/index.cfm?event=judge.showLogin">Click here</a> to submit your saved applications or to begin judging.


						<br /><br /><br />
						Thanks,<br />
						Siemens STEM Academy
					</div>
				</cfmail>
			<cfcatch></cfcatch></cftry>
			</cfif>

		</cfloop>

	</cffunction>


	<cffunction name="SubmitScore" access="public" returntype="void" output="false">
		<cfargument name="judgeGUID" type="string" required="true" />
		<cfargument name="scoreGUID" type="string" required="true" />

		<cfset var submitIt = "" />
          <cfset var sendtoAdmins = "" />

		<cfquery name="submitIt" datasource="#GetDSN()#">
			UPDATE STEMAppsJudgeScores
			SET submitted = getDate()
			WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
					scoreGUID = <cfqueryparam value="#arguments.scoreGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

          <cfquery name="chkunSubmitted" datasource="#GetDSN()#">
			SELECT scoreGUID
               FROM STEMAppsJudgeScores
			WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
                         submitted IS NULL
		</cfquery>

          <cfif chkunSubmitted.recordCount EQ 0>
               <cfset InfoToAdmins(arguments.judgeGUID) />
          </cfif>

	</cffunction>


	<cffunction name="SubmitAllScores" access="public" returntype="void" output="false">
		<cfargument name="judgeGUID" type="string" required="true" />

		<cfset var submitIt = "" />
          <cfset var chkunSubmitted = "" />

		<cfquery name="submitIt" datasource="#GetDSN()#">
			UPDATE STEMAppsJudgeScores
			SET submitted = getDate()
			WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
					status = <cfqueryparam value="Scored" cfsqltype="cf_sql_varchar" /> AND
                         submitted IS NULL
		</cfquery>

          <cfquery name="chkunSubmitted" datasource="#GetDSN()#">
			SELECT scoreGUID
               FROM STEMAppsJudgeScores
			WHERE 	judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /> AND
                         submitted IS NULL
		</cfquery>

          <cfif chkunSubmitted.recordCount EQ 0>
          	<cfset InfoToAdmins(arguments.judgeGUID) />
          </cfif>

	</cffunction>

	<cffunction name="InfoToAdmins" access="public" returntype="void" output="false">
		<cfargument name="judgeGUID" type="string" required="false" />
		<cfargument name="approvedGUID" type="string" required="false" />

		<cfset var emailTo = "" />
		<cfset var getAdmins = "" />
		<cfset var getJudge = "" />


		<cfquery name="getJudge" datasource="#GetDSN()#">
			SELECT * FROM STEMAppsJudges (nolock)
			WHERE judgeGUID = <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfquery name="getAdmins" datasource="#GetDSN()#">
			SELECT email FROM STEMAppsJudges (nolock)
			WHERE judgeYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" /> 
			AND
				<cfif client.STEMJudgeType eq "STARs">
						type IN ('adminPreStar','adminFinalStar')
				<cfelse>
						type IN ('adminPreInst','adminFinalInst')
				</cfif>
		</cfquery>

		<cfloop query="getAdmins">
			<cfset emailTo = getAdmins.email />
			<cfif FindNoCase("stage", cgi.server_name)>
				<cfset emailTo = "daryl_walsh@redacted.com,daryl_walsh@redacted.com" />
			</cfif>
			<cftry>
			<cfmail subject="Siemens STEM Academy Judging - New Applications Submitted" 
				from="Siemens STEM Academy <stemJudging@redacted.com>" to="#emailTo#" type="html">
				<div style="margin:10px; width:600px;">
					<cfif FindNoCase("stage", cgi.server_name)>
					This email would normally go to <b>#getAdmins.email#</b><br><br>
					</cfif>
					#getJudge.firstName# #getJudge.lastName# has now completed application judging.  <a href="http://#cgi.server_name#/index.cfm?event=judge.showLogin">Log-in</a> to view scores. <br />
				</div>
			</cfmail>
			<cfcatch></cfcatch></cftry>
		</cfloop>

	</cffunction>

     <cffunction name="getJudgesList" access="public" returntype="query" output="false">
		<cfargument name="judgeGUID" type="string" required="false" />

		<cfset var getJudge = "" />

		<cfquery name="getJudge" datasource="#GetDSN()#">
			SELECT * FROM STEMAppsJudges (nolock)
			WHERE judgeYear = <cfqueryparam value="#request.contestYear#" cfsqltype="cf_sql_integer" />
               AND 
			   <cfif listfindnocase("adminPreStar,adminFinalStar",client.STEMJudgeType)> 
			   type = <cfqueryparam value="STARs" cfsqltype="cf_sql_varchar" />
			   <cfelse>
               type = <cfqueryparam value="Institute" cfsqltype="cf_sql_varchar" />
               </cfif>
               <cfif Len(arguments.judgeGUID)>AND judgeGUID != <cfqueryparam value="#arguments.judgeGUID#" cfsqltype="cf_sql_varchar" /></cfif>
               AND type NOT LIKE <cfqueryparam value="admin%" cfsqltype="cf_sql_varchar" />
               ORDER BY firstName
		</cfquery>

          <cfreturn getJudge />

	</cffunction>

     <cffunction name="changeJudges" access="public" returntype="void" output="false">
		<cfargument name="aform" type="struct" required="true" />

		<cfset var changeJudge = "" />
		<cfset var getNewJudge = "" />
		<cfset var getOldJudge = "" />
		<cfset var newRequiredToScore = 0 />
		<cfset var saveJudge = "" />


		<cfquery name="getOldJudge" datasource="#GetDSN()#">
			SELECT * FROM STEMAppsJudges with (nolock)
			WHERE judgeGUID = <cfqueryparam value="#arguments.aform.oldJudge#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfquery name="getNewJudge" datasource="#GetDSN()#">
			SELECT * FROM STEMAppsJudges with (nolock)
			WHERE judgeGUID = <cfqueryparam value="#arguments.aform.newJudge#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfset newRequiredToScore = getOldJudge.requiredToScore - 1 />
		<cfquery name="saveJudge" datasource="#GetDSN()#">
			UPDATE STEMAppsJudges
			SET requiredToScore = <cfqueryparam value="#newRequiredToScore#" cfsqltype="cf_sql_integer" />
			WHERE judgeGUID = <cfqueryparam value="#arguments.aform.oldJudge#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfset newRequiredToScore = getNewJudge.requiredToScore + 1 />
		<cfquery name="saveJudge" datasource="#GetDSN()#">
			UPDATE STEMAppsJudges
			SET requiredToScore = <cfqueryparam value="#newRequiredToScore#" cfsqltype="cf_sql_integer" />
			WHERE judgeGUID = <cfqueryparam value="#arguments.aform.newJudge#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfquery name="changeJudge" datasource="#GetDSN()#">
			UPDATE STEMAppsJudgeScores
			SET judgeGUID = <cfqueryparam value="#arguments.aform.newJudge#" cfsqltype="cf_sql_varchar" />,
				status = <cfqueryparam value="ToBeJudged" cfsqltype="cf_sql_varchar" />
			WHERE 	approvedGUID = <cfqueryparam value="#arguments.aform.approvedGUID#" cfsqltype="cf_sql_varchar" /> AND
					judgeGUID = <cfqueryparam value="#arguments.aform.oldJudge#" cfsqltype="cf_sql_varchar" />
		</cfquery>


		<cfset emailTo = getNewJudge.email />
		<cfif FindNoCase("stage", cgi.server_name)>
			<cfset emailTo = "daryl_walsh@redacted.com,daryl_walsh@redacted.com" />
		</cfif>

		<cfmail subject="Siemens STEM Academy Judging - Application Reassigned to You" from="Siemens STEM Academy Judging <stemJudging@redacted.com>" to="#emailTo#" type="html">
			<div style="margin:10px; width:600px;">
				<cfif FindNoCase("stage", cgi.server_name)>
				This email would normally go to <b>#getNewJudge.email#</b><br><br>
				</cfif>
				An application has been reassigned to you: <br /><br />

				Display ID: ###arguments.aform.displayID#<br />
				Previous Judge Name: #getoldJudge.firstName# #getoldJudge.lastName#<br />
				<cfif Len(Trim(arguments.aform.message)) gt 0>
					<p>
						Comment from Admin:<br>
						#arguments.aform.message#
					</p><br />
				</cfif>
				<a href="http://#cgi.server_name#/index.cfm?event=judge.showLogin">Login</a><br><br>
			</div>
		</cfmail>


	</cffunction>

	<!----------------------------->
	<!--- Private Functions     --->
	<!----------------------------->

	<cffunction name="GetDSN" access="private" returntype="String" output="false">
		<cfreturn "Sponsorships" />
	</cffunction>


</cfcomponent>
