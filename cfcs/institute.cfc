<cfcomponent displayname="institute">

	<cffunction name="saveForm" access="public" returntype="struct">
		<cfargument name="stcForm" type="struct" required="yes">
        
        <cfset theForm = arguments.stcForm>
        
		<cfset stcData = structNew()>
        <cfset stcData.blnError = false>
        <cfset stcData.html = "">
        
        
        
        <cfset tempGuid="">
        <cfset Counter = 0>
        <cfloop index="item" list="#ListSort(theForm.FIELDNAMES,'Text')#">
            <cfquery name="qGetFieldGuid" datasource="Sponsorships">
                SELECT ff.FORM_SECTION_FIELD_GUID
                FROM SPONSOR AS s
                    INNER JOIN SPONSOR_FORM AS f ON s.SPONSOR_GUID = f.SPONSOR_GUID AND s.SPONSOR_GUID = '88888888-8888-8888-0004-888888888888' /*SIEMENS STEM ACADEMY*/
                    INNER JOIN SPONSOR_FORM_SECTION AS ss ON f.FORM_GUID = ss.FORM_GUID AND f.FORM_GUID = 'AAAAAAAA-AAAA-AAAA-0001-AAAAAAAAAAAA' /* Siemens Stem Institute*/
                    INNER JOIN SPONSOR_FORM_SECTION_FIELD AS ff ON ss.FORM_SECTION_GUID = ff.FORM_SECTION_GUID
                WHERE ff.NAME = '#item#'
            </cfquery>
            
            <cfif qGetFieldGuid.recordcount>
                <cfset fieldguid = qGetFieldGuid.FORM_SECTION_FIELD_GUID>
                
                <cfif tempGuid EQ fieldguid>
                    <cfset Counter = Counter + 1>	<!--- next row increment counter --->
                <cfelse>
                    <cfset Counter = 1>	<!--- reset counter --->
                </cfif>
                
                <cfset tempGuid = qGetFieldGuid.FORM_SECTION_FIELD_GUID>
                
                <cfset formvalue = "theForm.#item#" >
                <cfif ListLen(evaluate(formvalue)) GTE Counter>
                    <cfset formvalue = ListGetAt(evaluate(formvalue), Counter)>
                <cfelse>
                    <cfset formvalue = evaluate(formvalue)>
                </cfif>
                
                <cfif formvalue NEQ "">
                    <cftry>
                        <cfquery name="qSaveValue" datasource="Sponsorships">
                            EXECUTE usp_SPONSOR_FORM_RESPONSE_INSERT
                               @SPONSOR_USER_GUID = '#COOKIE.SPONSOR_USER_GUID#'
                              ,@FORM_SECTION_FIELD_GUID = '#fieldguid#'
                              ,@RESPONSE_VALUE = '#formvalue#'
                              ,@SORT_ORDER = '#Counter#'
                        </cfquery>
                        <cfcatch>
                            <cfset stcData.blnError = true>
                            <cfsavecontent variable="stcData.html"><cfdump var=#cfcatch#></cfsavecontent>
                        </cfcatch>
                    </cftry>
                </cfif>
            </cfif>
        	
        </cfloop>        
        
		<cfreturn stcData>
	</cffunction>
    
    <cffunction name="checkUserInstituteSubmission" access="public" returntype="boolean">
    	<cfargument name="SPONSOR_USER_GUID" type="string" required="yes">
        
        <cfset blnSubmitted = false>
        
        <cfquery name="qCheckSubmission" datasource="Sponsorships">
        	SELECT s.DESCRIPTION, sf.DESCRIPTION AS Expr1, r.RESPONSE_VALUE
            FROM SPONSOR_FORM AS f
                INNER JOIN SPONSOR_FORM_SECTION AS s ON f.FORM_GUID = s.FORM_GUID AND f.FORM_GUID = 'AAAAAAAA-AAAA-AAAA-0001-AAAAAAAAAAAA' /* Siemens Stem Institute - FORM */
                INNER JOIN SPONSOR_FORM_SECTION_FIELD AS sf ON s.FORM_SECTION_GUID = sf.FORM_SECTION_GUID
                INNER JOIN SPONSOR_FORM_RESPONSE AS r ON sf.FORM_SECTION_FIELD_GUID = r.FORM_SECTION_FIELD_GUID
            WHERE  r.SPONSOR_USER_GUID = '#arguments.SPONSOR_USER_GUID#'
            ORDER BY s.SORT_ORDER, sf.SORT_ORDER, r.SORT_ORDER
        </cfquery>
        
        <cfif qCheckSubmission.recordcount>
        	<cfset blnSubmitted = true>
        </cfif>
        
        <cfreturn blnSubmitted>
    </cffunction>


</cfcomponent>