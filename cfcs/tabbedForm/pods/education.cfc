<cfcomponent displayname="education.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var dateIds = "" />
		<cfset var i = "required" />
		<cfset var id = "" />
		<cfset var option = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		<cfif podValues.RecordCount lte 0>
			<cfset QueryAddRow(podValues) />
		</cfif>
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div id="educationPOD">
				<div class="question questionSpacer">
					List your undergraduate degree, major, graduation year and institution. 
					Also, please include additional graduate education even if a degree has not been completed. (Most recent first)
				</div>
				
				<cfif arguments.podType eq "edit">
					<cfloop query="podValues">
						<cfif podValues.CurrentRow eq 1>
							<cfset i = "required" />
						<cfelse>
							<cfset i = CreateUUID() />
						</cfif>
						
						<cfset dateIds = ListAppend(dateIds, "educationDateFrom-" & i) />
						<cfset dateIds = ListAppend(dateIds, "educationDateTo-" & i) />
						
						<div id="educationRow-#i#" class="questionRow">
							<div class="questionSmall floatLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										<span class="required">*</span> From:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.dateFrom />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="educationDateFrom-#i#" id="educationDateFrom-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										<span class="required">*</span> To:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.dateTo />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="educationDateTo-#i#" id="educationDateTo-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										<span class="required">*</span> School/Organization:
									</div>
								</cfif>
								<div class="floatLeft">
									<input type="text" class="large" name="educationSchool-#i#" id="educationSchool-#i#" value="#podValues.school#" maxlength="100">
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										<span class="required">*</span> Degree/Status:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.degree />
									<cfset options = "In Progress,Associates,Bachelors,Masters,Ph.D" />
									
									<select name="educationDegree-#i#" id="educationDegree-#i#">
										<option value="">- Select -</option>
										<cfloop list="#options#" index="option">
											<option value="#option#" <cfif option eq value>selected="selected"</cfif>>#option#</option>
										</cfloop>
									</select>
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										<span class="required">*</span> Major/Concentration:
									</div>
								</cfif>
								<div class="floatLeft">
									<input type="text" class="large" name="educationMajor-#i#" id="educationMajor-#i#" value="#podValues.major#" maxlength="100">
								</div>
							</div>
							
							<div class="floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">&nbsp;</div>
								</cfif>
								<div class="floatLeft">
									<cfif podValues.CurrentRow eq 1>
										#BuildButton("Add Another", "AddEducationRow();")#
									<cfelse>
										#BuildButton("Delete", "RemoveEducationRow('educationRow-#i#');")#
									</cfif>
								</div>
							</div>
							
							<div class="clear">&nbsp;</div>
						</div>
					</cfloop>
					
				<cfelse>
					
					<table class="summary">
						<tr class="head">
							<td style="width:90px;">
								<span class="required">*</span>
								From:
							</td>
							<td style="width:90px;">
								<span class="required">*</span>
								To:
							</td>
							<td style="width:160px;">
								<span class="required">*</span>
								School/Organization:
							</td>
							<td style="width:150px;">
								<span class="required">*</span>
								Degree:
							</td>
							<td style="width:150px;">
								<span class="required">*</span>
								Major/Concentration:
							</td>
						</tr>
						<cfif podValues.RecordCount gt 0>
							<cfloop query="podValues">
								<tr class="row">
									<td>
										<cfset value = podValues.dateFrom />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfset value = podValues.dateTo />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.school neq "">
											#podValues.school#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
										
									</td>
									<td>
										<cfif podValues.degree neq "">
											#podValues.degree#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.major neq "">
											#podValues.major#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
								</tr>
							</cfloop>
						<cfelse>
							<tr>
								<td colspan="5">
									<em>0 entries entered</em>
								</td>
							</tr>
						</cfif>
					</table>
					
				</cfif>
			</div>
			</cfoutput>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					function AddEducationRow ()
					{
						jQuery.post(remoteURL + "?method=AJAXCall", {componentName: "education", functionName: "GetEmptyFormRow"}, function(html){
							jQuery("#educationPOD").append(html);
						});
					}
					
					function RemoveEducationRow (rowId)
					{
						jQuery("#" + rowId).remove();
					}
					
					<cfoutput>
					jQuery().ready(function () {
						<cfloop list="#dateIds#" index="id">
							datePickerController.createDatePicker({formElements:{"#id#":"m-sl-d-sl-Y"}});
						</cfloop>
					});
					</cfoutput>
				</script>
			</cfif>
		</cfsavecontent>
		
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Post Secondary Educational Background" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT dateFrom, dateTo, school, degree, major
			FROM STEMAppsEducation (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY dateTo DESC
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = false />
		
		
		<cfif podValues.RecordCount gt 0>
			<cfset retVal = true />
			
			<cfloop query="podValues">
				<cfif NOT IsDate(podValues.dateFrom) OR NOT IsDate(podValues.dateTo) OR podValues.school eq "" OR podValues.degree eq "" OR podValues.major eq "">
					<cfset retVal = false />
				</cfif>
			</cfloop>
			
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var key = "" />
		<cfset var keyWordCheck = "educationDateFrom" />
		<cfset var postfix = "" />
		<cfset var removeAll = "" />
		<cfset var save = "" />
		
		<cfquery name="removeAll" datasource="#variables.settings.dsn#">
			DELETE FROM STEMAppsEducation
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfloop collection="#arguments.aform#" item="key">
			<cfif Left(key, Len(keyWordCheck)) eq keyWordCheck>
				<cfset postfix = Right(key, Len(key) - Len(keyWordCheck))>
				
				<!--- only save if at least 1 value is set --->
				
				<cfif IsDate(arguments.aform['educationDateFrom' & postfix]) OR IsDate(arguments.aform['educationDateTo' & postfix]) OR arguments.aform['educationSchool' & postfix] neq "" OR
						arguments.aform['educationDegree' & postfix] neq "" OR arguments.aform['educationMajor' & postfix] neq "">
					<cfquery name="save" datasource="#variables.settings.dsn#">
						INSERT INTO STEMAppsEducation (applicationGUID, dateFrom, dateTo, school, degree, major)
						VALUES (
									<cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />,
									<cfif IsDate(DateToDBFormat(arguments.aform['educationDateFrom' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['educationDateFrom' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfif IsDate(DateToDBFormat(arguments.aform['educationDateTo' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['educationDateTo' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfqueryparam value="#arguments.aform['educationSchool' & postfix]#" cfsqltype="cf_sql_varchar" />,
									<cfqueryparam value="#arguments.aform['educationDegree' & postfix]#" cfsqltype="cf_sql_varchar" />,
									<cfqueryparam value="#arguments.aform['educationMajor' & postfix]#" cfsqltype="cf_sql_varchar" />
								)
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	<cffunction name="GetEmptyFormRow" access="public" returntype="string" output="false">
		
		<cfset var emptyFormRow = "" />
		<cfset var i = CreateUUID() />
		
		<cfoutput>
		<cfsavecontent variable="emptyFormRow">
		<div id="educationRow-#i#" class="questionRow">
			<div class="questionSmall floatLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="educationDateFrom-#i#" id="educationDateFrom-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="educationDateTo-#i#" id="educationDateTo-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="large" name="educationSchool-#i#" id="educationSchool-#i#" value="" maxlength="100">
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<select name="educationDegree-#i#" id="educationDegree-#i#">
						<option value="">- Select -</option>
						<option value="Associates">Associates</option>
						<option value="Bachelors">Bachelors</option>
						<option value="Masters">Masters</option>
						<option value="Ph.D">Ph.D</option>
					</select>
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="large" name="educationMajor-#i#" id="educationMajor-#i#" value="" maxlength="100">
				</div>
			</div>
			
			<div class="floatLeft paddingLeft">
				<div class="floatLeft">
					#BuildButton("Delete", "RemoveEducationRow('educationRow-#i#');")#
				</div>
			</div>
			
			<div class="clear">&nbsp;</div>
		</div>
		
		<script type="text/javascript">
			jQuery().ready(function () {
				datePickerController.createDatePicker({formElements:{"educationDateFrom-#i#":"m-sl-d-sl-Y"}});
				datePickerController.createDatePicker({formElements:{"educationDateTo-#i#":"m-sl-d-sl-Y"}});
			});
		</script>
		</cfsavecontent>
		</cfoutput>
		
		
		<cfreturn emptyFormRow />
	</cffunction>
	
	
	
	
	
</cfcomponent>