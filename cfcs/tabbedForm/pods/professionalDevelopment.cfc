<cfcomponent displayname="professionalDevelopment.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var dateIds = "" />
		<cfset var i = "" />
		<cfset var id = "" />
		<cfset var option = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		<cfif arguments.podType eq "edit" AND podValues.RecordCount lte 0>
			<cfset QueryAddRow(podValues) />
		</cfif>
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div id="professionalDevelopmentPOD">
				<div class="question questionSpacer">
					 List your STEM related professional development: (Most recent first)
				</div>
				
				<cfif arguments.podType eq "edit">
					<cfloop query="podValues">
						<cfset i = CreateUUID() />
						<cfset dateIds = ListAppend(dateIds, "professionalDevelopmentDateFrom-" & i) />
						<cfset dateIds = ListAppend(dateIds, "professionalDevelopmentDateTo-" & i) />
						
						<div id="professionalDevelopmentRow-#i#" class="questionRow">
							<div class="questionSmall floatLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										From:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.dateFrom />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="professionalDevelopmentDateFrom-#i#" id="professionalDevelopmentDateFrom-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										To:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.dateTo />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="professionalDevelopmentDateTo-#i#" id="professionalDevelopmentDateTo-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										 Professional Development:
									</div>
								</cfif>
								<div class="floatLeft">
									<textarea name="professionalDevelopmentProfessionalDevelopment-#i#" id="professionalDevelopmentProfessionalDevelopment-#i#" style="width:350px;">#podValues.professionalDevelopment#</textarea>
								</div>
							</div>
							
							<div class="floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">&nbsp;</div>
								</cfif>
								<div class="floatLeft">
									<cfif podValues.CurrentRow eq 1>
										#BuildButton("Add Another", "AddProfessionalDevelopmentRow();")#
									<cfelse>
										#BuildButton("Delete", "RemoveProfessionalDevelopmentRow('professionalDevelopmentRow-#i#');")#
									</cfif>
								</div>
							</div>
							
							<div class="clear">&nbsp;</div>
						</div>
					</cfloop>
					
				<cfelse>
					
					<table class="summary">
						<tr class="head">
							<td style="width:90px;">
								From:
							</td>
							<td style="width:90px;">
								To:
							</td>
							<td style="width:400px;">
								Professional Development:
							</td>
						</tr>
						<cfif podValues.RecordCount gt 0>
							<cfloop query="podValues">
								<tr class="row">
									<td>
										<cfset value = podValues.dateFrom />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfset value = podValues.dateTo />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.professionalDevelopment neq "">
											#podValues.professionalDevelopment#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
								</tr>
							</cfloop>
						<cfelse>
							<tr class="rowEmpty">
								<td colspan="3">
									<em>0 entries entered</em>
								</td>
							</tr>
						</cfif>
					</table>
					
				</cfif>
			</div>
			</cfoutput>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					function AddProfessionalDevelopmentRow ()
					{
						jQuery.post(remoteURL + "?method=AJAXCall", {componentName: "professionalDevelopment", functionName: "GetEmptyFormRow"}, function(html){
							jQuery("#professionalDevelopmentPOD").append(html);
						});
					}
					
					
					function RemoveProfessionalDevelopmentRow (rowId)
					{
						jQuery("#" + rowId).remove();
					}
					
					
					jQuery().ready(function () {
						<cfoutput>
						<cfloop list="#dateIds#" index="id">
						datePickerController.createDatePicker({formElements:{"#id#":"m-sl-d-sl-Y"}});
						</cfloop>
						</cfoutput>
					});
				</script>
			</cfif>
		</cfsavecontent>
		
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "STEM Related Professional Development" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT dateFrom, dateTo, professionalDevelopment
			FROM STEMAppsProfessionalDevelopment (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY dateTo DESC
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = true />
		
		<cfloop query="podValues">
			<cfif NOT IsDate(podValues.dateFrom) OR NOT IsDate(podValues.dateTo) OR podValues.professionalDevelopment eq "">
				<cfset retVal = false />
			</cfif>
		</cfloop>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var key = "" />
		<cfset var keyWordCheck = "professionalDevelopmentDateFrom" />
		<cfset var postfix = "" />
		<cfset var removeAll = "" />
		<cfset var save = "" />
		
		<cfquery name="removeAll" datasource="#variables.settings.dsn#">
			DELETE FROM STEMAppsProfessionalDevelopment
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfloop collection="#arguments.aform#" item="key">
			<cfif Left(key, Len(keyWordCheck)) eq keyWordCheck>
				<cfset postfix = Right(key, Len(key) - Len(keyWordCheck))>
				
				<cfif IsDate(arguments.aform['professionalDevelopmentDateFrom' & postfix]) OR IsDate(arguments.aform['professionalDevelopmentDateTo' & postfix]) OR arguments.aform['professionalDevelopmentProfessionalDevelopment' & postfix] neq "">
					<cfquery name="save" datasource="#variables.settings.dsn#">
						INSERT INTO STEMAppsProfessionalDevelopment (applicationGUID, dateFrom, dateTo, professionalDevelopment)
						VALUES (
									<cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />,
									<cfif IsDate(DateToDBFormat(arguments.aform['professionalDevelopmentDateFrom' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['professionalDevelopmentDateFrom' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfif IsDate(DateToDBFormat(arguments.aform['professionalDevelopmentDateTo' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['professionalDevelopmentDateTo' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfqueryparam value="#arguments.aform['professionalDevelopmentProfessionalDevelopment' & postfix]#" cfsqltype="cf_sql_varchar" />
								)
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	<cffunction name="GetEmptyFormRow" access="public" returntype="string" output="false">
		
		<cfset var emptyFormRow = "" />
		<cfset var i = CreateUUID() />
		<cfset var option = "" />
		<cfset var options = "" />
		
		<cfoutput>
		<cfsavecontent variable="emptyFormRow">
		<div id="professionalDevelopmentRow-#i#" class="questionRow">
			<div class="questionSmall floatLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="professionalDevelopmentDateFrom-#i#" id="professionalDevelopmentDateFrom-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="professionalDevelopmentDateTo-#i#" id="professionalDevelopmentDateTo-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<textarea name="professionalDevelopmentProfessionalDevelopment-#i#" id="professionalDevelopmentProfessionalDevelopment-#i#" style="width:350px;"></textarea>
				</div>
			</div>
			
			<div class="floatLeft paddingLeft">
				<div class="floatLeft">
					#BuildButton("Delete", "RemoveProfessionalDevelopmentRow('professionalDevelopmentRow-#i#');")#
				</div>
			</div>
			
			<div class="clear">&nbsp;</div>
		</div>
		
		<script type="text/javascript">
			jQuery().ready(function () {
				datePickerController.createDatePicker({formElements:{"professionalDevelopmentDateFrom-#i#":"m-sl-d-sl-Y"}});
				datePickerController.createDatePicker({formElements:{"professionalDevelopmentDateTo-#i#":"m-sl-d-sl-Y"}});
			});
		</script>
		</cfsavecontent>
		</cfoutput>
		
		
		<cfreturn emptyFormRow />
	</cffunction>
	
	
	
	
	
</cfcomponent>