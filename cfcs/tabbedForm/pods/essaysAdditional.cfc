<cfcomponent displayname="essaysAdditional.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var i = "required" />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var unique = CreateUUID() />
		<cfset var value = "" />
		
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div class="questionSpacerSmall">
				Please provide any additional information you believe is relevant to your application. 
				This space may NOT be used to expand on the essay questions.
			</div>
			
			<cfif arguments.podType eq "edit">
				<textarea class="ckeditor" id="essayAdditionalInformation-#unique#" name="essayAdditionalInformation">#podValues.essayAdditionalInformation#</textarea>
				<input name="essayAdditionalInformation-#unique#WordCount" type="hidden" value="350" /><!--needed for word count-->
				
				<script type="text/javascript">
					jQuery().ready(function () {
						jQuery("##essayAdditionalInformation-#unique#").ckeditor( function() { /* callback code */ }, { /*skin : 'office2003'*/ toolbar: 'main' } );
					});
				</script>
			<cfelse>
				<div style="margin-left:15px;">
					<em class="summaryText">#podValues.essayAdditionalInformation#</em>
				</div>
			</cfif>
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Additional Information" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT essayAdditionalInformation
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var retVal = true />
		
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var save = "" />
		
		
		<cfquery name="save" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET <cfif StructKeyExists(arguments.aform, "essayAdditionalInformation") AND arguments.aform.essayAdditionalInformation neq "">
					essayAdditionalInformation = <cfqueryparam value="#arguments.aform.essayAdditionalInformation#" cfsqltype="cf_sql_longvarchar" />
				<cfelse>
					essayAdditionalInformation = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />
				</cfif>
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	
</cfcomponent>