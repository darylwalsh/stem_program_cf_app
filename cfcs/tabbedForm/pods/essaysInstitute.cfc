<cfcomponent displayname="essaysInstitute.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var i = "required" />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var unique = CreateUUID() />
		<cfset var value = "" />
		
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div class="tabDescription">
				Please be as thorough as possible in your responses.
			</div>
			
			<div id="InstituteEssays">
				<!--- <cfif podValues.programInstitute eq true> --->
					<div class="question questionSpacer" style="margin-top:20px;">
						<span class="required">*</span>						
						1. Why do you think STEM education is important to today's students and give two examples of how you have enhanced STEM education. 
						(350 maximum words)
					</div>
					<cfif arguments.podType eq "edit">
						<textarea class="ckeditor" id="essaySTEMInstitute1-#unique#" name="essaySTEMInstitute1">#podValues.essaySTEMInstitute1#</textarea>
						<input name="essaySTEMInstitute1-#unique#WordCount" type="hidden" value="350" /><!--needed for word count-->
					<cfelse>
						<div style="margin-left:15px;">
							<cfif podValues.essaySTEMInstitute1 neq "">
								<em class="summaryText">#podValues.essaySTEMInstitute1#</em>
							<cfelse>
								<span class="required summaryText"><strong>Not Set</strong></span>
							</cfif>
						</div>
					</cfif>
					
					<div class="question questionSpacer" style="margin-top:20px;">
						<span class="required">*</span>
						2. Explain what you hope to learn from attending the STARs and/or STEM Institute, what you can uniquely contribute 
						and what you would like to add to your classroom instruction upon completing the experience. 
						How will you share this information with your students and/or peers? (350 maximum words)
					</div>
					<cfif arguments.podType eq "edit">
						<textarea class="ckeditor" id="essaySTEMInstitute2-#unique#" name="essaySTEMInstitute2">#podValues.essaySTEMInstitute2#</textarea>
						<input name="essaySTEMInstitute2-#unique#WordCount" type="hidden" value="350" /><!--needed for word count-->
					<cfelse>
						<div style="margin-left:15px;">
							<cfif podValues.essaySTEMInstitute2 neq "">
								<em class="summaryText">#podValues.essaySTEMInstitute2#</em>
							<cfelse>
								<span class="required summaryText"><strong>Not Set</strong></span>
							</cfif>
						</div>
					</cfif>
				<!--- </cfif> --->
			</div>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					jQuery().ready(function () {
						<!--- <cfif podValues.programInstitute eq true> --->
							jQuery("##essaySTEMInstitute1-#unique#").ckeditor( function() { /* callback code */ }, { /*skin : 'office2003'*/ toolbar: 'main' } );
							jQuery("##essaySTEMInstitute2-#unique#").ckeditor( function() { /* callback code */ }, { /*skin : 'office2003'*/ toolbar: 'main' } );
						<!--- <cfelse> --->
							/*jQuery("##InstituteEssays").parent().parent().css("display", "none");*/
						<!--- </cfif> --->
					});
				</script>
			</cfif>
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "STARs and Institute Essay Questions" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT programInstitute, essaySTEMInstitute1, essaySTEMInstitute2
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = false />
		
		<cfif podValues.programInstitute neq true OR (podValues.essaySTEMInstitute1 neq "" AND podValues.essaySTEMInstitute2 neq "")>
			<cfset retVal = true />
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var save = "" />
		
		
		<cfquery name="save" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET <cfif StructKeyExists(arguments.aform, "essaySTEMInstitute1") AND arguments.aform.essaySTEMInstitute1 neq "">
					essaySTEMInstitute1 = <cfqueryparam value="#arguments.aform.essaySTEMInstitute1#" cfsqltype="cf_sql_longvarchar" />,
				<cfelse>
					essaySTEMInstitute1 = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />,
				</cfif>
				
				<cfif StructKeyExists(arguments.aform, "essaySTEMInstitute2") AND arguments.aform.essaySTEMInstitute2 neq "">
					essaySTEMInstitute2 = <cfqueryparam value="#arguments.aform.essaySTEMInstitute2#" cfsqltype="cf_sql_longvarchar" />
				<cfelse>
					essaySTEMInstitute2 = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />
				</cfif>
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	
</cfcomponent>