<cfcomponent displayname="technologyKnowledgeUse.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var i = "required" />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		
		<cfsavecontent variable="podHTML">
			<style type="text/css">
				table.survey {
					border-collapse:collapse;
					border: 1px solid #ccc;
					width: 582px;
					margin: 0px 0px 0px 10px;
				}
				table.survey th {
					font-size: 0.875em;
					font-style: italic;
					height: 23px;
					text-align: center;
				}
				table.survey td {
					min-width: 60px;
					padding: 0 5px !important;
					white-space: nowrap;
					height: 22px;
					text-align: center;
				}
				table.survey td:first-child {
					text-align: left;
				}
				table.survey tbody tr:nth-child(odd) {
					background-color: rgba(192,233,255,0.3);
				}
				table.survey input {
					margin: 0 !important;
					float: none;
				}
			</style>
			
			
			<cfoutput>
			<div class="questionSpacer">
				Please assess your familiarity with and use of technology in the classroom by selecting the <strong>most appropriate</strong> 
				response below. *Note that this information will not be used in the judging processs but will help us to plan accordingly for 
				the Institute.
			</div>
			
			<div class="bolded questionSpacerSmall paddingLeft">
				<span class="required">*</span>
				Select One:
				<cfif arguments.podType neq "edit" AND podValues.technologyKnowledge eq "">
					<span class="required"><strong>(Not Set)</strong></span>
				</cfif>
			</div>
			
			<div class="questionSpacerSmall paddingLeft">
				<div class="floatLeft" style="width:20px;">
					<cfif arguments.podType eq "edit">
						<input type="radio" name="technologyKnowledge" id="technologyKnowledge-advanced" value="advanced" <cfif podValues.technologyKnowledge eq "advanced">checked="checked"</cfif> />
					<cfelse>
						<cfif podValues.technologyKnowledge eq "advanced">
							<strong>X</strong>
						<cfelse>
							&nbsp;
						</cfif>
					</cfif>
				</div>
				<div class="floatLeft" style="width:600px;">
					I have advanced knowledge of technology and use it very frequently in the classroom.
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			
			<div class="questionSpacerSmall paddingLeft">
				<div class="floatLeft" style="width:20px;">
					<cfif arguments.podType eq "edit">
						<input type="radio" name="technologyKnowledge" id="technologyKnowledge-above-average" value="above-average" <cfif podValues.technologyKnowledge eq "above-average">checked="checked"</cfif> />
					<cfelse>
						<cfif podValues.technologyKnowledge eq "above-average">
							<strong>X</strong>
						<cfelse>
							&nbsp;
						</cfif>
					</cfif>
				</div>
				<div class="floatLeft" style="width:600px;">
					I have above-average knowledge of technology and use it somewhat often in the classroom.
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			
			<div class="questionSpacerSmall paddingLeft">
				<div class="floatLeft" style="width:20px;">
					<cfif arguments.podType eq "edit">
						<input type="radio" name="technologyKnowledge" id="technologyKnowledge-average" value="average" <cfif podValues.technologyKnowledge eq "average">checked="checked"</cfif> />
					<cfelse>
						<cfif podValues.technologyKnowledge eq "average">
							<strong>X</strong>
						<cfelse>
							&nbsp;
						</cfif>
					</cfif>
				</div>
				<div class="floatLeft" style="width:600px;">
					I have average knowledge of technology and sometimes use it in the classroom.
				</div>
				<div class="clear">&nbsp;</div>
			</div>
				
			<div class="questionSpacer paddingLeft">
				<div class="floatLeft" style="width:20px;">
					<cfif arguments.podType eq "edit">
						<input type="radio" name="technologyKnowledge" id="technologyKnowledge-limited" value="limited" <cfif podValues.technologyKnowledge eq "limited">checked="checked"</cfif> />
					<cfelse>
						<cfif podValues.technologyKnowledge eq "limited">
							<strong>X</strong>
						<cfelse>
							&nbsp;
						</cfif>
					</cfif>
				</div>
				<div class="floatLeft" style="width:600px;">
					I have very limited knowledge of technology and occasionally or rarely use it in the classroom.
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			
			<div class="bolded questionSpacerSmall paddingLeft">
				<span class="required">*</span>
				How often do you use the following tools?
			</div>
			
			<div class="paddingLeft">
				<table class="survey">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Very Often</th>
							<th>Sometimes</th>
							<th>Rarely</th>
							<th>Never</th>
						</tr>
					</thead>
					<tbody>
						<tr class="alt">
							<td>Wikis</td>
							<cfif arguments.podType neq "edit" AND podValues.technologyUseWikis eq "">
								<td colspan="4" style="text-align:center;">
									<span class="required"><strong>Not Set</strong></span>
								</td>
							<cfelse>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseWikis" value="very-often" <cfif podValues.technologyUseWikis eq "very-often">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseWikis eq "very-often">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseWikis" value="sometimes" <cfif podValues.technologyUseWikis eq "sometimes">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseWikis eq "sometimes">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseWikis" value="rarely" <cfif podValues.technologyUseWikis eq "rarely">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseWikis eq "rarely">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseWikis" value="never" <cfif podValues.technologyUseWikis eq "never">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseWikis eq "never">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
							</cfif>
						</tr>
						<tr>
							<td>Blogs/Forums</td>
							<cfif arguments.podType neq "edit" AND podValues.technologyUseBlogs eq "">
								<td colspan="4" style="text-align:center;">
									<span class="required"><strong>Not Set</strong></span>
								</td>
							<cfelse>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseBlogs" value="very-often" <cfif podValues.technologyUseBlogs eq "very-often">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseBlogs eq "very-often">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseBlogs" value="sometimes" <cfif podValues.technologyUseBlogs eq "sometimes">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseBlogs eq "sometimes">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseBlogs" value="rarely" <cfif podValues.technologyUseBlogs eq "rarely">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseBlogs eq "rarely">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseBlogs" value="never" <cfif podValues.technologyUseBlogs eq "never">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseBlogs eq "never">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
							</cfif>
						</tr>
						<tr class="alt">
							<td>Social Media (Facebook, Twitter, etc.)</td>
							<cfif arguments.podType neq "edit" AND podValues.technologyUseSocialMedia eq "">
								<td colspan="4" style="text-align:center;">
									<span class="required"><strong>Not Set</strong></span>
								</td>
							<cfelse>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseSocialMedia" value="very-often" <cfif podValues.technologyUseSocialMedia eq "very-often">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseSocialMedia eq "very-often">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseSocialMedia" value="sometimes" <cfif podValues.technologyUseSocialMedia eq "sometimes">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseSocialMedia eq "sometimes">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseSocialMedia" value="rarely" <cfif podValues.technologyUseSocialMedia eq "rarely">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseSocialMedia eq "rarely">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseSocialMedia" value="never" <cfif podValues.technologyUseSocialMedia eq "never">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseSocialMedia eq "never">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
							</cfif>
						</tr>
						<tr>
							<td>Digital Storytelling tools (MovieMaker, etc.)</td>
							<cfif arguments.podType neq "edit" AND podValues.technologyUseStorytelling eq "">
								<td colspan="4" style="text-align:center;">
									<span class="required"><strong>Not Set</strong></span>
								</td>
							<cfelse>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseStorytelling" value="very-often" <cfif podValues.technologyUseStorytelling eq "very-often">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseStorytelling eq "very-often">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseStorytelling" value="sometimes" <cfif podValues.technologyUseStorytelling eq "sometimes">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseStorytelling eq "sometimes">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseStorytelling" value="rarely" <cfif podValues.technologyUseStorytelling eq "rarely">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseStorytelling eq "rarely">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseStorytelling" value="never" <cfif podValues.technologyUseStorytelling eq "never">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseStorytelling eq "never">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
							</cfif>
						</tr>
						<tr class="alt">
							<td>Web 2.0 Applications</td>
							<cfif arguments.podType neq "edit" AND podValues.technologyUseWeb2_0 eq "">
								<td colspan="4" style="text-align:center;">
									<span class="required"><strong>Not Set</strong></span>
								</td>
							<cfelse>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseWeb2_0" value="very-often" <cfif podValues.technologyUseWeb2_0 eq "very-often">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseWeb2_0 eq "very-often">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseWeb2_0" value="sometimes" <cfif podValues.technologyUseWeb2_0 eq "sometimes">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseWeb2_0 eq "sometimes">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseWeb2_0" value="rarely" <cfif podValues.technologyUseWeb2_0 eq "rarely">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseWeb2_0 eq "rarely">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
								<td>
									<cfif arguments.podType eq "edit">
										<input type="radio" name="technologyUseWeb2_0" value="never" <cfif podValues.technologyUseWeb2_0 eq "never">checked="checked"</cfif> />
									<cfelse>
										<cfif podValues.technologyUseWeb2_0 eq "never">
											<strong>X</strong>
										<cfelse>
											&nbsp;
										</cfif>
									</cfif>
								</td>
							</cfif>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="clear">&nbsp;</div>
			</cfoutput>
			
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Technology Familiarity and Use Gauge" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT technologyKnowledge, technologyUseWikis, technologyUseBlogs, technologyUseSocialMedia, technologyUseStorytelling, technologyUseWeb2_0
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" /> <!--- cfqueryparam seems to have issues here? --->
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = false />
		
		
		<cfif podValues.technologyKnowledge neq "" AND podValues.technologyUseWikis neq "" AND podValues.technologyUseBlogs neq "" AND podValues.technologyUseSocialMedia neq "" AND podValues.technologyUseStorytelling neq "" AND podValues.technologyUseWeb2_0 neq "">
			<cfset retVal = true />
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var save = "" />
		
		
		<cfquery name="save" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET 
				<cfif StructKeyExists(arguments.aform, "technologyKnowledge") AND arguments.aform.technologyKnowledge neq "">
					technologyKnowledge = <cfqueryparam value="#arguments.aform.technologyKnowledge#" cfsqltype="cf_sql_varchar" />,
				<cfelse>
					technologyKnowledge = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />,
				</cfif>
				
				<cfif StructKeyExists(arguments.aform, "technologyUseWikis") AND arguments.aform.technologyUseWikis neq "">
					technologyUseWikis = <cfqueryparam value="#arguments.aform.technologyUseWikis#" cfsqltype="cf_sql_varchar" />,
				<cfelse>
					technologyUseWikis = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />,
				</cfif>
				
				<cfif StructKeyExists(arguments.aform, "technologyUseBlogs") AND arguments.aform.technologyUseBlogs neq "">
					technologyUseBlogs = <cfqueryparam value="#arguments.aform.technologyUseBlogs#" cfsqltype="cf_sql_varchar" />,
				<cfelse>
					technologyUseBlogs = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />,
				</cfif>
				
				<cfif StructKeyExists(arguments.aform, "technologyUseSocialMedia") AND arguments.aform.technologyUseSocialMedia neq "">
					technologyUseSocialMedia = <cfqueryparam value="#arguments.aform.technologyUseSocialMedia#" cfsqltype="cf_sql_varchar" />,
				<cfelse>
					technologyUseSocialMedia = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />,
				</cfif>
				
				<cfif StructKeyExists(arguments.aform, "technologyUseStorytelling") AND arguments.aform.technologyUseStorytelling neq "">
					technologyUseStorytelling = <cfqueryparam value="#arguments.aform.technologyUseStorytelling#" cfsqltype="cf_sql_varchar" />,
				<cfelse>
					technologyUseStorytelling = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />,
				</cfif>
				
				<cfif StructKeyExists(arguments.aform, "technologyUseWeb2_0") AND arguments.aform.technologyUseWeb2_0 neq "">
					technologyUseWeb2_0 = <cfqueryparam value="#arguments.aform.technologyUseWeb2_0#" cfsqltype="cf_sql_varchar" />
				<cfelse>
					technologyUseWeb2_0 = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />
				</cfif>
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	
</cfcomponent>