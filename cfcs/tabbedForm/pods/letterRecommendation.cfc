<cfcomponent displayname="letterRecommendation.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var podHTML = "" />
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div class="question questionSpacer">
				Your application requires the completion of one recommendation sheet by an administrator or supervisor. 
				The individual selected to complete your recommendation sheet should be able to evaluate your character, academic and/or teaching abilities.
			</div>
			
			<div class="question bolded questionSpacer" style="margin:20px 0px 20px 0px; text-align:center; width:100%;">
				<span class="required">-- User Action Required (See Below) --</span>
			</div>
			
			<div class="question questionSpacer">
				<span class="required">
					Please send the following URL to the person you have selected to provide a recommendation sheet. It is your responsibility 
					to ensure that they are notified and to follow up with them to make sure they complete the request. 
					<span style="text-decoration:underline;">The recommender should complete the recommendation sheet</span>, not the applicant. 
					<span style="text-decoration:underline;">You will receive a notification once your recommender has completed the form.</span>
				</span>
			</div>
			
			<div class="question questionSpacer">
				<cfset recLink =  "http://" & cgi.server_name & "/index.cfm?event=showSTEMApplicationRecommendation&applicationGUID=" & arguments.applicationGUID />
				<a href="#recLink#" target="_recommendation">#recLink#</a>
			</div>
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Recommendation" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = QueryNew("Empty", "VarChar") />
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var retVal = true />
		
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var save = "" />
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	
</cfcomponent>