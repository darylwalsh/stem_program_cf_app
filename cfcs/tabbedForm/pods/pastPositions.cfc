<cfcomponent displayname="pastPositions.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var i = "" />
		<cfset var option = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var toCheck = "" />
		<cfset var value = "" />
		
		<cfif arguments.podType eq "edit" AND podValues.RecordCount lte 0>
			<cfset QueryAddRow(podValues) />
		</cfif>
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div id="pastPositionsPOD">
				<div class="question questionSpacer">
					Please list your three most recent teaching experiences. (Most recent first). To select multiple grades taught hold your CTRL key down while clicking desired options.
				</div>
				
				<cfif arguments.podType eq "edit">
					<cfloop query="podValues">
						<cfset i = CreateUUID() />
						<cfset toCheck = ListAppend(toCheck, i) />
						
						<div id="pastPositionsRow-#i#" class="questionRow">
							<div class="questionSmall floatLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										From:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.dateFrom />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="pastPositionsDateFrom-#i#" id="pastPositionsDateFrom-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										To:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.dateTo />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="pastPositionsDateTo-#i#" id="pastPositionsDateTo-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										School/Organization:
									</div>
								</cfif>
								<div class="floatLeft">
									<input type="text" class="large" name="pastPositionsSchool-#i#" id="pastPositionsSchool-#i#" value="#podValues.school#" maxlength="100" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft questionSpacer">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										<nobr>Grades Taught:</nobr>
									</div>
								</cfif>
								<cfset value = podValues.grades />
								<cfset options = "K,1,2,3,4,5,6,7,8,9,10,11,12" />
								
								<nobr>&nbsp;&nbsp;&nbsp;<select name="pastPositionsGrades-#i#-ms" 
									id="pastPositionsGrades-#i#-ms" onclick="PopulateHiddenVersion('pastPositionsGrades-#i#');" style="width:43px;" multiple="multiple" size="4">
									<cfloop list="#options#" index="option">
										<option value="#option#" <cfif ListFindNoCase(value, option) gt 0>selected="selected"</cfif>>#option#</option>
									</cfloop>
								</select></nobr>
								<input type="hidden" name="pastPositionsGrades-#i#" id="pastPositionsGrades-#i#" value="#value#" />
							</div>
							
							<div class="questionSmall floatLeft paddingLeft questionSpacer">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										 Courses Taught (i.e., Biology):
									</div>
								</cfif>
								
								<div class="floatLeft">
									<input type="text" class="large" name="pastPositionsCourse-#i#" id="pastPositionsCourse-#i#" value="#podValues.course#" maxlength="100" />
								</div>
								
								
								<!---
								<cfset value = podValues.course />
								<cfset options = "Science,Technology,Engineering,Mathematics,Other" />
								
								<select name="pastPositionsCourse-#i#" id="pastPositionsCourse-#i#" onchange="PastPositionsCheckOther('#i#');">
									<option value="">- Select -</option>
									<cfloop list="#options#" index="option">
										<option value="#option#" <cfif option eq value>selected="selected"</cfif>>#option#</option>
									</cfloop>
								</select>
								--->
							</div>
							
							
							<!---
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall" style="color:##666666; font-size:10px; font-style: italic;">
										If other, please list
									</div>
								</cfif>
								<div class="floatLeft">
									<input type="text" class="large" name="pastPositionsCourseOther-#i#" id="pastPositionsCourseOther-#i#" value="#podValues.courseOther#" maxlength="100" />
								</div>
							</div>
							--->
							
							<div class="floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">&nbsp;</div>
								</cfif>
								<div class="floatLeft">
									<cfif podValues.CurrentRow eq 1>
										#BuildButton("Add Another", "AddPastPositionsRow();", "addPastPositionBtn")#
									<cfelse>
										#BuildButton("Delete", "RemovePastPositionsRow('pastPositionsRow-#i#');")#
									</cfif>
								</div>
							</div>
							
							<div class="clear">&nbsp;</div>
						</div>
					</cfloop>
					
				<cfelse>
					
					<table class="summary">
						<tr class="head">
							<td style="width:90px;">
								From:
							</td>
							<td style="width:90px;">
								To:
							</td>
							<td style="width:160px;">
								School/Organization:
							</td>
							<td style="width:60px;">
								Grades Taught:
							</td>
							<td style="width:190px;">
								Courses Taught (i.e., Biology):
							</td>
						</tr>
						<cfif podValues.RecordCount gt 0>
							<cfloop query="podValues">
								<tr class="row">
									<td>
										<cfset value = podValues.dateFrom />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfset value = podValues.dateTo />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.school neq "">
											#podValues.school#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.grades neq "">
											#podValues.grades#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.course neq "">
											#podValues.course#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<!---
									<td>
										#podValues.courseOther#
									</td>
									--->
								</tr>
							</cfloop>
							
						<cfelse>
							<tr>
								<td colspan="6">
									<em>0 entries entered</em>
								</td>
							</tr>
						</cfif>
					</table>
					
				</cfif>
			</div>
			</cfoutput>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					function AddPastPositionsRow ()
					{
						jQuery.post(remoteURL + "?method=AJAXCall", {componentName: "pastPositions", functionName: "GetEmptyFormRow"}, function(html){
							jQuery("#pastPositionsPOD").append(html);
							
							var rowCount = jQuery("input[name^='pastPositionsDateFrom-']");
							
							if(rowCount.length >= 3){
								jQuery("div[id='addPastPositionBtn']").hide();
							}
							
						});
					}
					
					function PastPositionsCheckOther (postfix)
					{
						var otherField = jQuery("#pastPositionsCourseOther-" + postfix);
						
						if (jQuery("#pastPositionsCourse-" + postfix).val().toLowerCase() == "other") {
							otherField.css("background-color", "#ffffff");
							otherField.attr("disabled", "");
						}
						else {
							otherField.val("");
							otherField.css("background-color", "#dddddd");
							otherField.attr("disabled", "true");
						}
					}
					
					
					function RemovePastPositionsRow (rowId)
					{
						jQuery("#" + rowId).remove();
						
						var rowCount = jQuery("input[name^='pastPositionsDateFrom-']");
							
						if(rowCount.length <= 2){
							jQuery("div[id='addPastPositionBtn']").show();
						}
					}
					
					
					jQuery().ready(function () {
						<cfoutput>
						<cfloop list="#toCheck#" index="value">
						PastPositionsCheckOther('#value#');
						
						datePickerController.createDatePicker({formElements:{"pastPositionsDateFrom-#value#":"m-sl-d-sl-Y"}});
						datePickerController.createDatePicker({formElements:{"pastPositionsDateTo-#value#":"m-sl-d-sl-Y"}});
						</cfloop>
						</cfoutput>
					});
				</script>
			</cfif>
		</cfsavecontent>
		
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Previous Teaching Position(s)/Experience" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT dateFrom, dateTo, school, grades, course
			FROM STEMAppsPastPositions (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY dateTo DESC
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = false />
		
		
		<cfset retVal = true />
		
		<cfloop query="podValues">
			<cfif NOT IsDate(podValues.dateFrom) OR NOT IsDate(podValues.dateTo) OR podValues.school eq "" OR podValues.grades eq "" OR podValues.course eq "">
				<cfset retVal = false />
			</cfif>
		</cfloop>
		
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var key = "" />
		<cfset var keyWordCheck = "pastPositionsDateFrom" />
		<cfset var postfix = "" />
		<cfset var removeAll = "" />
		<cfset var save = "" />
		
		
		<cfquery name="removeAll" datasource="#variables.settings.dsn#">
			DELETE FROM STEMAppsPastPositions
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfloop collection="#arguments.aform#" item="key">
			<cfif Left(key, Len(keyWordCheck)) eq keyWordCheck>
				<cfset postfix = Right(key, Len(key) - Len(keyWordCheck))>
				
				<cfif IsDate(arguments.aform['pastPositionsDateFrom' & postfix]) OR IsDate(arguments.aform['pastPositionsDateTo' & postfix]) OR arguments.aform['pastPositionsSchool' & postfix] neq "" OR
						arguments.aform['pastPositionsGrades' & postfix] neq "" OR arguments.aform['pastPositionsCourse' & postfix] neq "">
					<cfquery name="save" datasource="#variables.settings.dsn#">
						INSERT INTO STEMAppsPastPositions (applicationGUID, dateFrom, dateTo, school, grades, course) <!--- courseOther --->
						VALUES (
									<cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />,
									<cfif IsDate(DateToDBFormat(arguments.aform['pastPositionsDateFrom' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['pastPositionsDateFrom' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfif IsDate(DateToDBFormat(arguments.aform['pastPositionsDateTo' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['pastPositionsDateTo' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfqueryparam value="#arguments.aform['pastPositionsSchool' & postfix]#" cfsqltype="cf_sql_varchar" />,
									<cfqueryparam value="#arguments.aform['pastPositionsGrades' & postfix]#" cfsqltype="cf_sql_varchar" />,
									<cfqueryparam value="#arguments.aform['pastPositionsCourse' & postfix]#" cfsqltype="cf_sql_varchar" />
									<!---
									<cfif arguments.aform['pastPositionsCourse' & postfix] eq "Other">
										<cfqueryparam value="#arguments.aform['pastPositionsCourseOther' & postfix]#" cfsqltype="cf_sql_varchar" />
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />
									</cfif>
									--->
								)
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	<cffunction name="GetEmptyFormRow" access="public" returntype="string" output="false">
		
		<cfset var emptyFormRow = "" />
		<cfset var i = CreateUUID() />
		<cfset var option = "" />
		<cfset var options = "" />
		
		<cfoutput>
		<cfsavecontent variable="emptyFormRow">
		<div id="pastPositionsRow-#i#" class="questionRow">
			<div class="questionSmall floatLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="pastPositionsDateFrom-#i#" id="pastPositionsDateFrom-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="pastPositionsDateTo-#i#" id="pastPositionsDateTo-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="large" name="pastPositionsSchool-#i#" id="pastPositionsSchool-#i#" value="" maxlength="100" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft questionSpacer">
				<cfset options = "K,1,2,3,4,5,6,7,8,9,10,11,12" />
				
				<select name="pastPositionsGrades-#i#-ms" id="pastPositionsGrades-#i#-ms" onclick="PopulateHiddenVersion('pastPositionsGrades-#i#');" style="width:43px;" multiple="multiple" size="4">
					<cfloop list="#options#" index="option">
						<option value="#option#">#option#</option>
					</cfloop>
				</select>
				<input type="hidden" name="pastPositionsGrades-#i#" id="pastPositionsGrades-#i#" value="" />
			</div>
			
			<div class="questionSmall floatLeft paddingLeft questionSpacer">
				
				<div class="floatLeft">
					<input type="text" class="large" name="pastPositionsCourse-#i#" id="pastPositionsCourse-#i#" value="" maxlength="100" />
				</div>
				
				<!---
				<cfset options = "Science,Technology,Engineering,Mathematics,Other" />
				
				<select name="pastPositionsCourse-#i#" id="pastPositionsCourse-#i#" onchange="PastPositionsCheckOther('#i#');">
					<option value="">- Select -</option>
					<cfloop list="#options#" index="option">
						<option value="#option#">#option#</option>
					</cfloop>
				</select>
				--->
			</div>
			
			
			<!---
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="large" name="pastPositionsCourseOther-#i#" id="pastPositionsCourseOther-#i#" value="" maxlength="100" style="background-color:##dddddd;" disabled="disabled" />
				</div>
			</div>
			--->
			
			<div class="floatLeft paddingLeft">
				<div class="floatLeft">
					#BuildButton("Delete", "RemovePastPositionsRow('pastPositionsRow-#i#');")#
				</div>
			</div>
			
			<div class="clear">&nbsp;</div>
		</div>
		
		<script type="text/javascript">
			jQuery().ready(function () {
				datePickerController.createDatePicker({formElements:{"pastPositionsDateFrom-#i#":"m-sl-d-sl-Y"}});
				datePickerController.createDatePicker({formElements:{"pastPositionsDateTo-#i#":"m-sl-d-sl-Y"}});
			});
		</script>
		</cfsavecontent>
		</cfoutput>
		
		
		<cfreturn emptyFormRow />
	</cffunction>
	
	
	
	
	
</cfcomponent>