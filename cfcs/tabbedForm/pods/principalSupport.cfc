<cfcomponent displayname="principalSupport.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var i = "required" />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div class="bolded questionSpacerSmall">
				<span class="required">*</span>
				Does your principal support your application to partipate in this opportunity?
			</div>
			
			<div class="questionSpacer">
				<cfif arguments.podType eq "edit">
					<input type="radio" name="principalSupport" id="principalSupport-1" value="1" onclick="PrincipalSupportChecked();" <cfif podValues.principalSupport eq "1">checked="checked"</cfif> />
					Yes
					&nbsp;
					<input type="radio" name="principalSupport" id="principalSupport-0" value="0" onclick="PrincipalSupportChecked();" <cfif podValues.principalSupport eq "0">checked="checked"</cfif> />
					No
				<cfelse>
					<cfif podValues.principalSupport eq "1">
						<strong>Yes</strong>
					<cfelseif podValues.principalSupport eq "0">
						<strong>No</strong>
					<cfelse>
						<span class="required"><strong>Not Set</strong></span>
					</cfif>
				</cfif>
			</div>
			
			<cfif arguments.podType eq "edit" OR podValues.principalSupport eq "0">
				<div class="questionSpacerSmall">
					Please Comment if you answered "No":
				</div>
				<div class="questionSpacer">
					<cfif arguments.podType eq "edit">
						<textarea name="principalSupportWhyNo" id="principalSupportWhyNo">#podValues.principalSupportWhyNo#</textarea>
					<cfelse>
						<div style="margin-left:15px;">
							<cfif podValues.principalSupportWhyNo neq "">
								<em class="summaryText">#podValues.principalSupportWhyNo#</em>
							<cfelse>
								<span class="required"><strong>Not Set</strong></span>
							</cfif>
						</div>
					</cfif>
				</div>
			</cfif>
			
			<div class="questionSpacerSmall">
				Please provide the full name and email for your principal:
			</div>
			
			<div class="questionSmall floatLeft questionSpacer" style="width:50px;">
				<div class="questionSmall questionSpacerSmall">
					Prefix:
				</div>
				<cfset value = podValues.principalSupportPrefix />
				<cfset options = "Mr.,Miss,Ms.,Mrs.,Dr.,Prof." />
				
				<cfif arguments.podType eq "edit">
					<select name="principalSupportPrefix" id="principalSupportPrefix" style="width:50px;">
						<option value="">-</option>
						<cfloop list="#options#" index="option">
							<option value="#option#" <cfif option eq value>selected="selected"</cfif>>#option#</option>
						</cfloop>
					</select>
				<cfelse>
					<strong class="summaryText">#value#</strong>
				</cfif>
			</div>
			
			
			<div class="questionSmall floatLeft paddingLeft questionSpacer" style="width:110px;">
				<div class="questionSmall questionSpacerSmall">
					First Name:
				</div>
				<div class="floatLeft">
					<cfif arguments.podType eq "edit">
						<input type="text" class="large" name="principalSupportFirstName" id="principalSupportFirstName" value="#podValues.principalSupportFirstName#" />
					<cfelse>
						<strong class="summaryText">#podValues.principalSupportFirstName#</strong>
					</cfif>
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft questionSpacer" style="width:110px;">
				<div class="questionSmall questionSpacerSmall">
					Last Name:
				</div>
				<div class="floatLeft">
					<cfif arguments.podType eq "edit">
						<input type="text" class="large" name="principalSupportLastName" id="principalSupportLastName" value="#podValues.principalSupportLastName#" />
					<cfelse>
						<strong class="summaryText">#podValues.principalSupportLastName#</strong>
					</cfif>
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft questionSpacer">
				<div class="questionSmall questionSpacerSmall">
					Email:
				</div>
				<div class="floatLeft">
					<cfif arguments.podType eq "edit">
						<input type="text" class="large" name="principalSupportEmail" id="principalSupportEmail" value="#podValues.principalSupportEmail#" />
					<cfelse>
						<strong class="summaryText">#podValues.principalSupportEmail#</strong>
					</cfif>
				</div>
			</div>
			
			<div class="clear">&nbsp;</div>
			</cfoutput>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					function PrincipalSupportChecked ()
					{
						<!---
						var prefix = jQuery("#principalSupportPrefix");
						var fname = jQuery("#principalSupportFirstName");
						var lname = jQuery("#principalSupportLastName");
						var email = jQuery("#principalSupportEmail");
						
						
						if(String(jQuery("#principalSupport-1").attr("checked")).toLowerCase() == "true" ) {
							prefix.css("background-color", "#ffffff");
							prefix.attr("disabled", "");
							
							fname.css("background-color", "#ffffff");
							fname.attr("disabled", "");
							
							lname.css("background-color", "#ffffff");
							lname.attr("disabled", "");
							
							email.css("background-color", "#ffffff");
							email.attr("disabled", "");
							
						}
						else {
							prefix.val("");
							prefix.css("background-color", "#dddddd");
							prefix.attr("disabled", "true");
							
							fname.val("");
							fname.css("background-color", "#dddddd");
							fname.attr("disabled", "true");
							
							lname.val("");
							lname.css("background-color", "#dddddd");
							lname.attr("disabled", "true");
							
							email.val("");
							email.css("background-color", "#dddddd");
							email.attr("disabled", "true");
						}
						--->
						
						
						var noTA = jQuery("#principalSupportWhyNo");
						
						if(String(jQuery("#principalSupport-0").attr("checked")).toLowerCase() == "true" ) {
							noTA.css("background-color", "#ffffff");
							noTA.attr("disabled", "");
						}
						else {
							noTA.val("");
							noTA.css("background-color", "#dddddd");
							noTA.attr("disabled", "true");
						}
					}
					
					
					jQuery().ready(function () {
						
						PrincipalSupportChecked();
					});
				</script>
			</cfif>
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Administrative Support" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT principalSupport, principalSupportWhyNo, principalSupportPrefix, principalSupportFirstName, principalSupportLastName, principalSupportEmail
			FROM STEMApplications (nolock)
			WHERE applicationGUID = '#arguments.applicationGUID#' <!--- cfqueryparam seems to have issues here? --->
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = false />
		
		
		<cfif podValues.principalSupport eq true>
			<cfset retVal = true />
			
		<cfelseif podValues.principalSupport eq false AND podValues.principalSupportWhyNo neq "">
			<cfset retVal = true />
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var save = "" />
		
		
		<cfquery name="save" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET <cfif StructKeyExists(arguments.aform, "principalSupport") AND arguments.aform.principalSupport eq "1">
					principalSupport = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
					principalSupportWhyNo = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />,
				<cfelseif StructKeyExists(arguments.aform, "principalSupport") AND arguments.aform.principalSupport eq "0">
					principalSupport = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
					principalSupportWhyNo = <cfqueryparam value="#arguments.aform.principalSupportWhyNo#" cfsqltype="cf_sql_longvarchar" />,
				<cfelse>
					principalSupport = <cfqueryparam value="" null="true" cfsqltype="cf_sql_bit" />,
					principalSupportWhyNo = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />,
				</cfif>
				principalSupportPrefix = <cfqueryparam value="#arguments.aform.principalSupportPrefix#" cfsqltype="cf_sql_varchar" />, 
				principalSupportFirstName = <cfqueryparam value="#arguments.aform.principalSupportFirstName#" cfsqltype="cf_sql_varchar" />, 
				principalSupportLastName = <cfqueryparam value="#arguments.aform.principalSupportLastName#" cfsqltype="cf_sql_varchar" />, 
				principalSupportEmail = <cfqueryparam value="#arguments.aform.principalSupportEmail#" cfsqltype="cf_sql_varchar" />
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	
</cfcomponent>