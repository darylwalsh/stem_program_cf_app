<cfcomponent displayname="essays.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var i = "required" />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		
		<cfsavecontent variable="podHTML">
			
			Tab Content Coming Soon
			
			<!---
			<cfoutput>
			<div class="bolded questionSpacerSmall">
				<span class="requried">*</span>
				Does your principal support your application to partipate in this opportunity?
			</div>
			
			<div class="questionSpacer">
				<input type="radio" name="principalSupport" id="principalSupport-1" value="1" onclick="PrincipalSupportCheckYes();" <cfif podValues.principalSupport eq "1">checked="checked"</cfif> />Yes
				&nbsp;
				<input type="radio" name="principalSupport" id="principalSupport-0" value="0" onclick="PrincipalSupportCheckYes();" <cfif podValues.principalSupport eq "0">checked="checked"</cfif> />No
			</div>
			
			
			<div class="questionSpacerSmall">
				Please provide the full name and email for your principal:
			</div>
			
			<div class="questionSmall floatLeft questionSpacer">
				<div class="questionSmall questionSpacerSmall">
					Prefix:
				</div>
				<cfset value = podValues.principalSupportPrefix />
				<cfset options = "Mr.,Miss,Ms.,Mrs.,Dr.,Prof." />
				
				<select name="principalSupportPrefix" id="principalSupportPrefix" style="width:50px;">
					<option value="">-</option>
					<cfloop list="#options#" index="option">
						<option value="#option#" <cfif option eq value>selected="selected"</cfif>>#option#</option>
					</cfloop>
				</select>
			</div>
			
			
			<div class="questionSmall floatLeft paddingLeft questionSpacer">
				<div class="questionSmall questionSpacerSmall">
					First Name:
				</div>
				<div class="floatLeft">
					<input type="text" class="large" name="principalSupportFirstName" id="principalSupportFirstName" value="#podValues.principalSupportFirstName#" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft questionSpacer">
				<div class="questionSmall questionSpacerSmall">
					Last Name:
				</div>
				<div class="floatLeft">
					<input type="text" class="large" name="principalSupportLastName" id="principalSupportLastName" value="#podValues.principalSupportLastName#" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft questionSpacer">
				<div class="questionSmall questionSpacerSmall">
					Email:
				</div>
				<div class="floatLeft">
					<input type="text" class="large" name="principalSupportEmail" id="principalSupportEmail" value="#podValues.principalSupportEmail#" />
				</div>
			</div>
			
			<div class="clear">&nbsp;</div>
			</cfoutput>
			
			
			<script type="text/javascript">
				function PrincipalSupportCheckYes ()
				{
					var prefix = jQuery("#principalSupportPrefix");
					var fname = jQuery("#principalSupportFirstName");
					var lname = jQuery("#principalSupportLastName");
					var email = jQuery("#principalSupportEmail");
					
					
					if(String(jQuery("#principalSupport-1").attr("checked")).toLowerCase() == "true" ) {
						prefix.css("background-color", "#ffffff");
						prefix.attr("disabled", "");
						
						fname.css("background-color", "#ffffff");
						fname.attr("disabled", "");
						
						lname.css("background-color", "#ffffff");
						lname.attr("disabled", "");
						
						email.css("background-color", "#ffffff");
						email.attr("disabled", "");
						
					}
					else {
						prefix.val("");
						prefix.css("background-color", "#dddddd");
						prefix.attr("disabled", "true");
						
						fname.val("");
						fname.css("background-color", "#dddddd");
						fname.attr("disabled", "true");
						
						lname.val("");
						lname.css("background-color", "#dddddd");
						lname.attr("disabled", "true");
						
						email.val("");
						email.css("background-color", "#dddddd");
						email.attr("disabled", "true");
					}
				}
				
				
				jQuery().ready(function () {
					
					PrincipalSupportCheckYes();
				});
			</script>
			--->
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "STEM Institute Essay Questions" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT principalSupport, principalSupportPrefix, principalSupportFirstName, principalSupportLastName, principalSupportEmail
			FROM STEMApplications (nolock)
			WHERE applicationGUID = '#arguments.applicationGUID#' <!--- cfqueryparam seems to have issues here? --->
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var save = "" />
		
		<!---
		<cfquery name="save" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET <cfif StructKeyExists(arguments.aform, "principalSupport") AND arguments.aform.principalSupport eq "1">
					principalSupport = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
					principalSupportPrefix = <cfqueryparam value="#arguments.aform.principalSupportPrefix#" cfsqltype="cf_sql_varchar" />, 
					principalSupportFirstName = <cfqueryparam value="#arguments.aform.principalSupportFirstName#" cfsqltype="cf_sql_varchar" />, 
					principalSupportLastName = <cfqueryparam value="#arguments.aform.principalSupportLastName#" cfsqltype="cf_sql_varchar" />, 
					principalSupportEmail = <cfqueryparam value="#arguments.aform.principalSupportEmail#" cfsqltype="cf_sql_varchar" />
				<cfelseif StructKeyExists(arguments.aform, "principalSupport") AND arguments.aform.principalSupport eq "0">
					principalSupport = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
					principalSupportPrefix = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />, 
					principalSupportFirstName = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />, 
					principalSupportLastName = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />, 
					principalSupportEmail = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />
				<cfelse>
					principalSupport = <cfqueryparam value="" null="true" cfsqltype="cf_sql_bit" />,
					principalSupportPrefix = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />, 
					principalSupportFirstName = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />, 
					principalSupportLastName = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />, 
					principalSupportEmail = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />	
				</cfif>
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		--->
		
	</cffunction>
	
	
	<cffunction name="ValidatePod" access="public" returntype="string" hint="returns 'success' or 'failed'">
		<cfreturn "success" />
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	
</cfcomponent>