<cfcomponent displayname="activities.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var dateIds = "" />
		<cfset var ethnicityList = "American Indian or Alaska Native,Asian,Black or African American,Caucasian,Hispanic/Latino,Native Hawaiian or Pacific Islander,Other" />
		<cfset var i = "" />
		<cfset var ii = "" />
		<cfset var id = "" />
		<cfset var option = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		<cfif arguments.podType eq "edit" AND podValues.RecordCount lte 0>
			<cfset QueryAddRow(podValues) />
		</cfif>
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div id="demographicsPOD" style="margin-bottom:25px;border-bottom:1px solid white;">
				<div class="questionSmall floatLeft paddingLeft">
					<div class="questionSmall questionSpacerSmall">
						Gender:
						<input type="radio" name="optionalDemographicsGender" value="male" <cfif podValues.optionalDemographicsGender eq "male">checked="checked"</cfif> /> Male
						&nbsp;&nbsp;&nbsp;
						<input type="radio" name="optionalDemographicsGender" value="female" <cfif podValues.optionalDemographicsGender eq "female">checked="checked"</cfif> /> Female								
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
						Ethnicity:						
						<select name="optionalDemographicsEthnicity" style="width:200px;">
							<option value="">Select</option>
							<cfloop list="#ethnicityList#" index="ii">
								<option value="#ii#"<cfif ii eq podValues.optionalDemographicsEthnicity> selected="selected"</cfif>>#ii#</option>
							</cfloop>
						</select>
					</div>
				</div>
			</div>
		</cfoutput>
		</cfsavecontent>
		
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Optional Demographic Information" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT optionalDemographicsGender, optionalDemographicsEthnicity
			FROM STEMApplications WITH (NOLOCK)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var retVal = true />
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var update = "" />
		
		<cfparam name="arguments.aform.optionalDemographicsGender" default="" />
		<cfparam name="arguments.aform.optionalDemographicsEthnicity" default="" />
		
		<cfquery name="update" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET optionalDemographicsGender = <cfqueryparam value="#arguments.aform.optionalDemographicsGender#" cfsqltype="cf_sql_varchar" />,
				optionalDemographicsEthnicity = <cfqueryparam value="#arguments.aform.optionalDemographicsEthnicity#" cfsqltype="cf_sql_varchar" />
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_idstamp" />
		</cfquery>			
		
	</cffunction>
	
	
</cfcomponent>