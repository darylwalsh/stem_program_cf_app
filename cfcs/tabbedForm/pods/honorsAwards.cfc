<cfcomponent displayname="honorsAwards.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var appliedProgramYears = [] />
		<cfset var dateIds = "" />
		<cfset var i = "" />
		<cfset var id = "" />
		<cfset var ii = "" />
		<cfset var jj = "" />
		<cfset var option = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var podValues2 = GetPodValues2(arguments.applicationGUID) />
		<cfset var podValues3 = GetPodValues3(arguments.applicationGUID) />
		<cfset var programs = [] />
		<cfset var siemensRecognized = GetPodValues3(arguments.applicationGUID).siemensRecognized />
		<cfset var value = "" />
		
		
		<cfif podValues2.RecordCount lte 0> <!--- took this out of podType eq "edit" because it is always required --->
			<cfset QueryAddRow(podValues2) />
		</cfif>
		
		<cfsavecontent variable="podHTML">
			
			<cfoutput>
			<div class="bolded questionSpacerSmall">
				List up to three teaching-related honors or awards that you have received (most recent first).
			</div>
			
			<div id="honorsAwardsEntries">
				
				<cfif arguments.podType eq "edit">
					<cfif podValues.RecordCount lte 0>
						<cfset QueryAddRow(podValues) />
					</cfif>
					
					<cfloop query="podValues">
						<cfset i = CreateUUID() />
						<cfset dateIds = ListAppend(dateIds, "honorsAwardsDateReceived-" & i) />
						
						<div id="honorsAwardsRow-#i#" class="questionRow">
							<div class="questionSmall floatLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										Date Received:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.dateReceived />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="honorsAwardsDateReceived-#i#" id="honorsAwardsDateReceived-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										Honor/Award:
									</div>
								</cfif>
								<div class="floatLeft">
									<input type="text" class="large" name="honorsAwardsHonorAward-#i#" id="honorsAwardsHonorAward-#i#" value="#podValues.honorAward#" maxlength="100" />
								</div>
							</div>
							
							<div class="floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">&nbsp;</div>
								</cfif>
								<div class="floatLeft">
									<cfif podValues.CurrentRow eq 1>
										#BuildButton("Add Another", "AddHonorsAwardsRow();", "addHonorsAwardsBtn")#
									<cfelse>
										#BuildButton("Delete", "RemoveHonorsAwardsRow('honorsAwardsRow-#i#');")#
									</cfif>
								</div>
							</div>
							
							<div class="clear">&nbsp;</div>
						</div>
					</cfloop>
				
				<cfelse>
					
					<table class="summary" style="margin-bottom:12px;">
						<tr class="head">
							<td style="width:90px;">
								Date Received:
							</td>
							<td style="width:200px;">
								Honor/Award:
							</td>
						</tr>
						<cfif podValues.RecordCount gt 0>
							<cfloop query="podValues">
								<tr class="row">
									<td>
										<cfset value = podValues.dateReceived />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.honorAward neq "">
											#podValues.honorAward#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
								</tr>
							</cfloop>
						<cfelse>
							<tr class="rowEmpty">
								<td colspan="2">
									<em>0 entries entered</em>
								</td>
							</tr>
						</cfif>
					</table>
				</cfif>
			</div>
			
			
			<div class="bolded questionSpacerSmall">
				<span class="required">*</span>
				Have you ever been recognized by the Siemens Foundation or participated in a Siemens Foundation program?
			</div>
			
			<div class="questionSpacerSmall">
				<cfif arguments.podType eq "edit">
					<input type="radio" name="siemensRecognized" id="siemensRecognized-1" value="1" onclick="HonorsAwardsCheckYes();" <cfif siemensRecognized eq "1">checked="checked"</cfif> />
					Yes
					&nbsp;
					<input type="radio" name="siemensRecognized" id="siemensRecognized-0" value="0" onclick="HonorsAwardsCheckYes();" <cfif siemensRecognized eq "0">checked="checked"</cfif> />
					No
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span style="color:##888888; font-size:12px; font-style:italic;">
						If yes, please provide date(s) and program(s):
					</span>
				<cfelse>
					<cfif siemensRecognized eq "1">
						<strong>Yes</strong>
					<cfelseif siemensRecognized eq "0">
						<strong>No</strong>
					<cfelse>
						<span class="required"><strong>Not Set</strong></span>
					</cfif>
				</cfif>
			</div>
			
			<cfif arguments.podType eq "edit">
				<div id="siemensRecognizedEntries">
					<cfloop query="podValues2">
						<cfset i = CreateUUID() />
						<cfset dateIds = ListAppend(dateIds, "siemensRecognizedRecognizedDate-" & i) />
						
						<div id="siemensRecognizedRow-#i#" class="questionRow">
							<div class="questionSmall floatLeft">
								<div class="floatLeft">
									<cfset value = podValues2.recognizedDate />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="siemensRecognizedRecognizedDate-#i#" id="siemensRecognizedRecognizedDate-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<div class="floatLeft">
									<cfset programs = [] />
									<cfset programs[1] = "Siemens Awards for Advanced Placement" />
									<cfset programs[2] = "Siemens Competition" />
									<cfset programs[3] = "Siemens We Can Change the World Challenge" />
									<cfset programs[4] = "STARs Program at Oak Ridge National Labs" />
									<cfset programs[5] = "STARs Program at Pacific NW National Labs" />
									<cfset programs[6] = "STEM Institute" />
									<cfset programs[7] = "Siemens Teacher Scholarship" />
									<cfset programs[8] = "Other - please explain" />
									
									<cfset value = ValueList(podValues2.program) />
									
									<!---
									<cfloop from="1" to="#ArrayLen(programs)#" index="j">
										<input type="checkbox" name="siemensRecognizedProgram-#i#-ms" value="#programs[j]#"
											style="vertical-align:middle;"
											onclick="PopulateHiddenVersionCBVersion('siemensRecognizedProgram-#i#'); CheckOther(this.value, '#i#');"
											<cfif listfindnocase(podValues2.program,programs[j])>checked="checked"</cfif>>
											#programs[j]#
											
											<cfif j eq 8>
												&nbsp;&nbsp;
												<input type="text" name="siemensRecognizedProgramOther-#i#" id="siemensRecognizedProgramOther-#i#" <cfif Find("Other", podValues2.program) gt 0>value="#podValues2.programOther#"<cfelse>disabled="disabled" style="background-color:##eeeeee;" value=""</cfif> />
											</cfif>
											
											<br />
									</cfloop>
									--->
									
									<select type="text" class="large" style="width:300px;" name="siemensRecognizedProgram-#i#" id="siemensRecognizedProgram-#i#" onchange="CheckOther('#i#');">
										<option value="">- Select One -</option>
										<cfloop from="1" to="#ArrayLen(programs)#" index="j">
											<option value="#programs[j]#" <cfif programs[j] eq podValues2.program>selected="selected"</cfif>>#programs[j]#</option>
										</cfloop>
									</select>
									
								</div>
							</div>
							
							
							<div class="questionSmall floatLeft paddingLeft">
								<div class="floatLeft">
									<input type="text" name="siemensRecognizedProgramOther-#i#" id="siemensRecognizedProgramOther-#i#" <cfif Find("Other", podValues2.program) gt 0>value="#podValues2.programOther#"<cfelse>disabled="disabled" style="background-color:##eeeeee;" value=""</cfif> />
								</div>
							</div>
							
							
							<div class="floatLeft paddingLeft">
								<div class="floatLeft">
									<cfif podValues2.CurrentRow eq 1>
										#BuildButton("Add Another", "AddSiemensRecognizedRow();")#
									<cfelse>
										#BuildButton("Delete", "RemoveSiemensRecognizedRow('siemensRecognizedRow-#i#');")#
									</cfif>
								</div>
							</div>
							
							<div class="clear">&nbsp;</div>
						</div>
					</cfloop>
				</div>
				
				<div class="questionSpacerSmall">
					<span class="required">*</span>
					<strong>Have you applied for the STEM Institute or STARs Program in the past?</strong>
				</div>
				<div class="questionSpacerSmall">
					<input type="radio" name="appliedSTEMInPast" id="appliedSTEMInPast-1" value="1" onclick="EnableInPastDetails();" <cfif podValues3.appliedSTEMInPast eq "1">checked="checked"</cfif> />
					Yes
					&nbsp;
					<input type="radio" name="appliedSTEMInPast" id="appliedSTEMInPast-0" value="0" onclick="DisableInPastDetails();" <cfif podValues3.appliedSTEMInPast eq "0">checked="checked"</cfif> />
					No
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span style="color:##888888; font-size:12px; font-style:italic;">
						If yes, which program and when?
					</span>
				</div>
				
				<div id="inPastDetails" class="questionSpacer" style="<cfif podValues3.appliedSTEMInPast neq 1>display:none;</cfif>">
					<div class="questionRow">
						<div class="questionSmall floatLeft paddingLeft">
							<div class="questionSmall questionSpacerSmall">
								Program & Year: (You can select multiple items from the list)
							</div>
							
							<cfset applist = "Institute,STARs ORNL,STARs PNNL" />
							<cfset yearlist = "2009,2010,2011,2012" />
							<cfset value = ValueList(podValues3.appliedSTEMInPastValue) />
							
							<div class="floatLeft">
								<select name="appliedSTEMInPastValue-ms" id="appliedSTEMInPastValue-ms" size="#(listlen(applist) * listlen(yearlist))#" multiple="multiple"
									style="margin-left:50px;width:150px;"
									onclick="PopulateHiddenVersion('appliedSTEMInPastValue');">
									<cfloop list="#applist#" index="ii">							
									<cfloop list="#yearlist#" index="jj">
									<cfset showOption=1>
									<cfif ii is "STARs PNNL" and jj neq 2012>
										<cfset showOption=0>
									</cfif>
									<cfif variables.showOption>
										<option value="#ii# - #jj#"
										<cfif listfindnocase(replace(podValues3.appliedSTEMInPastValue,", ",",","all"),"#ii# - #jj#")>selected</cfif>>#ii# - #jj#
									</cfif>
									</cfloop>
									<cfif ii neq listlast(applist)>
									<option value="">#repeatstring("&nbsp;--",7)#
									</cfif>
								</cfloop>
								</select>
								<input type="hidden" name="appliedSTEMInPastValue" id="appliedSTEMInPastValue" value="#value#" />						
							</div>
						</div>
						
						<div class="clear">&nbsp;</div>
					</div>
				</div>
				
			<cfelse>
				
				<cfif siemensRecognized eq "1">
					<table class="summary">
						<tr class="head">
							<td style="width:90px;">
								Date:
							</td>
							<td style="width:200px;">
								Program:
							</td>
						</tr>
						<cfif podValues2.RecordCount gt 0>
							<cfloop query="podValues2">
								<tr class="row">
									<td>
										<cfset value = podValues2.recognizedDate />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues2.program neq "">
											#podValues2.program#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
								</tr>
							</cfloop>
						<cfelse>
							<tr class="rowEmpty">
								<td colspan="2">
									<em>0 entries entered</em>
								</td>
							</tr>
						</cfif>
					</table>
				</cfif>
				
				
				<div class="questionSpacerSmall">
					<span class="required">*</span>
					<strong>Have you applied for the STEM Institute or STARs Program in the past?</strong>
				</div>
				<div class="questionSpacerSmall">
					
					<cfif podValues3.appliedSTEMInPast eq "1">
						<strong class="summaryText">Yes</strong>
					<cfelseif podValues3.appliedSTEMInPast eq "0">
						<strong class="summaryText">No</strong>
					<cfelse>
						<span class="required summaryText"><strong>Not Set</strong></span>
					</cfif>
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span style="color:##888888; font-size:12px; font-style:italic;">
						If yes, which program and when?
					</span>
				</div>
				
				<cfif podValues3.appliedSTEMInPast eq "1">
					<div class="questionSpacer">
						<div class="questionRow">
							<div class="questionSmall floatLeft paddingLeft">
								<div class="questionSmall questionSpacerSmall">
									Program (year):
								</div>
								<div class="floatLeft">
								#replace(podValues3.appliedSTEMInPastValue,",","<br>","all")#
									<!--- 									
									<cfif ArrayLen(appliedProgramYears) lte 0>
										<span class="required summaryText"><strong>Not Set</strong></span>
									<cfelse>
										<strong class="summaryText">#ArrayToList(appliedProgramYears, ", ")#</strong>
									</cfif> --->
								</div>
							</div>
							
							<div class="clear">&nbsp;</div>
						</div>
					</div>
				</cfif>
				
			</cfif>
			</cfoutput>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					function AddHonorsAwardsRow ()
					{
						jQuery.post(remoteURL + "?method=AJAXCall", {componentName: "honorsAwards", functionName: "GetEmptyFormRow"}, function(html){
							jQuery("#honorsAwardsEntries").append(html);
							
							var rowCount = jQuery("input[name^='honorsAwardsDateReceived-']");
							
							if(rowCount.length >= 3){
								jQuery("div[id='addHonorsAwardsBtn']").hide();
							}
						});
					}
					
					
					function AddSiemensRecognizedRow ()
					{
						jQuery.post(remoteURL + "?method=AJAXCall", {componentName: "honorsAwards", functionName: "GetEmptyFormRow2"}, function(html){
							jQuery("#siemensRecognizedEntries").append(html);
						});
					}
					
					
					function CheckOther (rowId)
					{
						
						if(String(jQuery("#siemensRecognizedProgram-" + rowId).val()).toLowerCase().substr(0,5) == "other"){
							jQuery("#siemensRecognizedProgramOther-" + rowId).css("backgroundColor", "").attr("disabled", "");
						}
						else {
							jQuery("#siemensRecognizedProgramOther-" + rowId).val("").css("backgroundColor", "#eeeeee").attr("disabled", "disabled");
						}
					}
					
					
					function DisableInPastDetails ()
					{
						jQuery("#inPastDetails").css("display", "none");
					}
					
					
					function EnableInPastDetails ()
					{
						jQuery("#inPastDetails").css("display", "inline");
					}
					
					
					function HonorsAwardsCheckYes ()
					{
						if(String(jQuery("#siemensRecognized-1").attr("checked")).toLowerCase() == "true" ) {
							jQuery("#siemensRecognizedEntries").css("display", "");
						}
						else {
							jQuery("#siemensRecognizedEntries").css("display", "none");
						}
					}
					
					
					function RemoveHonorsAwardsRow (rowId)
					{
						jQuery("#" + rowId).remove();
						
						var rowCount = jQuery("input[name^='honorsAwardsDateReceived-']");
							
						if(rowCount.length <= 2){
							jQuery("div[id='addHonorsAwardsBtn']").show();
						}
					}
					
					
					function RemoveSiemensRecognizedRow (rowId)
					{
						jQuery("#" + rowId).remove();
					}
					
					jQuery().ready(function () {
						HonorsAwardsCheckYes();
						
						<cfoutput>
						<cfloop list="#dateIds#" index="id">
						datePickerController.createDatePicker({formElements:{"#id#":"m-sl-d-sl-Y"}});
						</cfloop>
						</cfoutput>
					});
				</script>
			</cfif>
			
		</cfsavecontent>
		
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Recognition" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT dateReceived, honorAward
			FROM STEMAppsHonorsAwards (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY dateReceived DESC
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var podValues2 = GetPodValues2(arguments.applicationGUID) />
		<cfset var podValues3 = GetPodValues3(arguments.applicationGUID) />
		<cfset var retVal = true />
		
		<cfif podValues.RecordCount gt 0>
			<cfif NOT IsDate(podValues.dateReceived) OR podValues.honorAward eq "">
				<cfset retVal = false />
			</cfif>
		</cfif>
		
		<cfif retVal eq true>
			<cfif podValues3.siemensRecognized eq true>
				<cfif podValues2.RecordCount lte 0>
					<cfset retVal = false />
				<cfelse>
					<cfloop query="podValues2">
						<cfif NOT IsDate(podValues2.recognizedDate) OR podValues2.program eq "">
							<cfset retVal = false />
						</cfif>
					</cfloop>
				</cfif>
			</cfif>
		</cfif>
		
		<cfif retVal eq true>
			<cfif NOT IsBoolean(podValues3.appliedSTEMInPast)>
				<cfset retVal = false />
			<cfelse>
				<cfif podValues3.appliedSTEMInPast eq true AND Not len(trim(podValues3.appliedSTEMInPastValue))>
					<cfset retVal = false />
				</cfif>
			</cfif>
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var key = "" />
		<cfset var keyWordCheck = "honorsAwardsDateReceived" />
		<cfset var keyWordCheck2 = "siemensRecognizedRecognizedDate" />
		<cfset var postfix = "" />
		<cfset var removeAll = "" />
		<cfset var save = "" />
		
		<cfquery name="removeAll" datasource="#variables.settings.dsn#">
			DELETE FROM STEMAppsHonorsAwards
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfquery name="removeAll" datasource="#variables.settings.dsn#">
			DELETE FROM STEMAppsSiemensRecognized
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<!--- <cfparam name="arguments.aform.appliedSTEMInPastValue" default=""> --->
		<cfparam name="form.appliedSTEMInPastValue" default="">
		<cfsavecontent variable="status">
		<cfdump var="#form#">
		</cfsavecontent>
		<cftry>
		<cfquery name="save" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET <cfif StructKeyExists(arguments.aform, "siemensRecognized") AND arguments.aform.siemensRecognized eq "1">
					siemensRecognized = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />
				<cfelseif StructKeyExists(arguments.aform, "siemensRecognized") AND arguments.aform.siemensRecognized eq "0">
					siemensRecognized = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />
				<cfelse>
					siemensRecognized = <cfqueryparam value="" null="true" cfsqltype="cf_sql_bit" />
				</cfif>
				
				<cfif StructKeyExists(arguments.aform, "appliedSTEMInPast")>
					,
					<cfif arguments.aform.appliedSTEMInPast eq "1">
						appliedSTEMInPast = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,						
						appliedSTEMInPastValue = <cfqueryparam value="#trim(form.appliedSTEMInPastValue)#" cfsqltype="cf_sql_longvarchar" />						
					<cfelseif arguments.aform.appliedSTEMInPast eq "0">
						appliedSTEMInPast = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						appliedSTEMInPastValue = <cfqueryparam value="" cfsqltype="cf_sql_varchar" />
					</cfif>
				</cfif>
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		<cfcatch>
			<cfset status="#cfcatch.message##chr(10)##cfcatch.detail##chr(10)##cfcatch.tagContext[1].template# LINE:#cfcatch.tagContext[1].line#">
		</cfcatch></cftry>
				
		<cfloop collection="#arguments.aform#" item="key">
			<cfif Left(key, Len(keyWordCheck)) eq keyWordCheck>
				<cfset postfix = Right(key, Len(key) - Len(keyWordCheck)) />
				
				<cfif IsDate(arguments.aform['honorsAwardsDateReceived' & postfix]) OR arguments.aform['honorsAwardsHonorAward' & postfix] neq "">
					<cfquery name="save" datasource="#variables.settings.dsn#">
						INSERT INTO STEMAppsHonorsAwards (applicationGUID, dateReceived, honorAward)
						VALUES (
									<cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />,
									<cfif IsDate(DateToDBFormat(arguments.aform['honorsAwardsDateReceived' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['honorsAwardsDateReceived' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfqueryparam value="#arguments.aform['honorsAwardsHonorAward' & postfix]#" cfsqltype="cf_sql_varchar" />
								)
					</cfquery>
				</cfif>
			</cfif>
			
			
			<cfif StructKeyExists(arguments.aform, "siemensRecognized") AND arguments.aform.siemensRecognized eq "1">
				<cfif Left(key, Len(keyWordCheck2)) eq keyWordCheck2>
					<cfset postfix = Right(key, Len(key) - Len(keyWordCheck2)) />
					
					<cfif IsDate(arguments.aform['siemensRecognizedRecognizedDate' & postfix]) OR arguments.aform['siemensRecognizedProgram' & postfix] neq "">
						<cfquery name="save" datasource="#variables.settings.dsn#">
							INSERT INTO STEMAppsSiemensRecognized (applicationGUID, recognizedDate, program, programOther)
							VALUES (
										<cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />,
										<cfif IsDate(DateToDBFormat(arguments.aform['siemensRecognizedRecognizedDate' & postfix]))>
											<cfqueryparam value="#DateToDBFormat(arguments.aform['siemensRecognizedRecognizedDate' & postfix])#" cfsqltype="cf_sql_timestamp" />,
										<cfelse>
											<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
										</cfif>
										<cfqueryparam value="#arguments.aform['siemensRecognizedProgram' & postfix]#" cfsqltype="cf_sql_varchar" />,
										<cfif Find("Other", arguments.aform['siemensRecognizedProgram' & postfix]) gt 0>
											<cfqueryparam value="#arguments.aform['siemensRecognizedProgramOther' & postfix]#" cfsqltype="cf_sql_varchar" />
										<cfelse>
											<cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />
										</cfif>
									)
						</cfquery>
					</cfif>
				</cfif>
			</cfif>
			
		</cfloop>
		
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	<cffunction name="GetEmptyFormRow" access="public" returntype="string" output="false">
		
		<cfset var emptyFormRow = "" />
		<cfset var i = CreateUUID() />
		<cfset var option = "" />
		<cfset var options = "" />
		
		<cfoutput>
		<cfsavecontent variable="emptyFormRow">
		<div id="honorsAwardsRow-#i#" class="questionRow">
			<div class="questionSmall floatLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="honorsAwardsDateReceived-#i#" id="honorsAwardsDateReceived-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="large" name="honorsAwardsHonorAward-#i#" id="honorsAwardsHonorAward-#i#" value="" maxlength="100" />
				</div>
			</div>
			
			<div class="floatLeft paddingLeft">
				<div class="floatLeft">
					#BuildButton("Delete", "RemoveHonorsAwardsRow('honorsAwardsRow-#i#');")#
				</div>
			</div>
			
			<div class="clear">&nbsp;</div>
		</div>
		
		<script type="text/javascript">
			jQuery().ready(function () {
				datePickerController.createDatePicker({formElements:{"honorsAwardsDateReceived-#i#":"m-sl-d-sl-Y"}});
			});
		</script>
		</cfsavecontent>
		</cfoutput>
		
		
		<cfreturn emptyFormRow />
	</cffunction>
	
	
	<cffunction name="GetEmptyFormRow2" access="public" returntype="string" output="false">
		
		<cfset var emptyFormRow = "" />
		<cfset var i = CreateUUID() />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var programs = [] />
		
		<cfoutput>
		<cfsavecontent variable="emptyFormRow">
		<div id="siemensRecognizedRow-#i#" class="questionRow">
			<div class="questionSmall floatLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="siemensRecognizedRecognizedDate-#i#" id="siemensRecognizedRecognizedDate-#i#" value="" maxlength="10" />
				</div>
			</div>

			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<cfset programs[1] = "Siemens Awards for Advanced Placement" />
					<cfset programs[2] = "Siemens Competition" />
					<cfset programs[3] = "Siemens We Can Change the World Challenge" />
					<cfset programs[4] = "STARs Program at Oak Ridge National Labs" />
					<cfset programs[5] = "STARs Program at Pacific NW National Labs" />
					<cfset programs[6] = "STEM Institute" />
					<cfset programs[7] = "Siemens Teacher Scholarship" />
					<cfset programs[8] = "Other - please explain" />
					
					<select type="text" class="large" style="width:300px;" name="siemensRecognizedProgram-#i#" id="siemensRecognizedProgram-#i#" onchange="CheckOther('#i#');">
						<option value="">- Select One -</option>
						<cfloop from="1" to="#ArrayLen(programs)#" index="j">
							<option value="#programs[j]#">#programs[j]#</option>
						</cfloop>
					</select>
					
				</div>
			</div>
			
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" name="siemensRecognizedProgramOther-#i#" id="siemensRecognizedProgramOther-#i#" value="" disabled="disabled" style="background-color:##eeeeee;" />
				</div>
			</div>
			
			<div class="floatLeft paddingLeft">
				<div class="floatLeft">
					#BuildButton("Delete", "RemoveSiemensRecognizedRow('siemensRecognizedRow-#i#');")#
				</div>
			</div>
			
			<div class="clear">&nbsp;</div>
		</div>
		
		<script type="text/javascript">
			jQuery().ready(function () {
				datePickerController.createDatePicker({formElements:{"siemensRecognizedRecognizedDate-#i#":"m-sl-d-sl-Y"}});
			});
		</script>
		</cfsavecontent>
		</cfoutput>
		
		
		<cfreturn emptyFormRow />
	</cffunction>
	
	
	<cffunction name="GetPodValues2" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT recognizedDate, program, programOther
			FROM STEMAppsSiemensRecognized (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY recognizedDate DESC
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="GetPodValues3" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT siemensRecognized, appliedSTEMInPast, appliedSTEMInPastValue
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
</cfcomponent>