
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!------ FOR 2012 - We need to upgrade the flash uploader and the the user process.        ------>
<!------ FOR 2012 - Create a new flash uploader.  The current one has many issues (% off)  ------>
<!------            and needs more funcionality (triggers flash functions) added to it.    ------>
<!------ FOR 2012 - Add application status to the left nav (for all tabs).                 ------>
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->

<cfcomponent displayname="uploadVideo.cfc" extends="podEXTENDS" implements="podINTERFACE">

	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>

	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />

		<cfset var i = "required" />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />


		<cfsavecontent variable="podHTML">
			<div class="questionSpacer">
				<span class="required">*</span>
				Please create a video and post it to YouTube that is <span style="text-decoration:underline;">no longer than two minutes</span>
				in length answering <span style="text-decoration:none;">one</span> of the following questions (Please note, if your
				video is longer than two minutes, only the first two minutes will be scored.):

				<cfif arguments.podType neq "edit" AND podValues.videoType eq "">
					<span class="required"><strong>(Not Set)</strong></span>
				</cfif>
			</div>

			<div class="questionSpacerSmall paddingLeft">
				<div class="floatLeft" style="width:20px;">
					<cfif arguments.podType eq "edit">
						<input type="radio" name="videoType" id="videoType-UtilizeInYourClassroom" value="UtilizeInYourClassroom" <cfif podValues.videoType eq "UtilizeInYourClassroom">checked="checked"</cfif> />
					<cfelse>
						<cfif podValues.videoType eq "UtilizeInYourClassroom">
							<strong>X</strong>
						<cfelse>
							&nbsp;
						</cfif>
					</cfif>
				</div>
				<div class="floatLeft" style="width:660px;">
					Demonstrate any new innovative ideas, materials, instructional strategies or techniques that you utilize in your classroom.					
				</div>
				<div class="clear">&nbsp;</div>
			</div>

			<div class="questionSpacer paddingLeft">
				<div class="floatLeft" style="width:20px;">
					<cfif arguments.podType eq "edit">
						<input type="radio" name="videoType" id="videoType-ProvideSTEMOutside" value="ProvideSTEMOutside" <cfif podValues.videoType eq "ProvideSTEMOutside">checked="checked"</cfif> />
					<cfelse>
						<cfif podValues.videoType eq "ProvideSTEMOutside">
							<strong>X</strong>
						<cfelse>
							&nbsp;
						</cfif>
					</cfif>
				</div>
				<div class="floatLeft" style="width:660px;">
					Demonstrate ways in which you provide student educational STEM experiences outside of the classroom.
				</div>
				<div class="clear">&nbsp;</div>
			</div>

			<div>
				<div class="questionSpacerSmall">
					<span class="required">*</span>
					<strong>YouTube URL</strong>
				</div>
				<div class="questionSpacer">
					<cfif arguments.podType eq "edit">
						<input type="text" name="videoUTube" id="videoUTube" value="<cfoutput>#podValues.videoUTube#</cfoutput>" style="width:300px;" maxlength="255" />
					<cfelse>
						<cfif podValues.videoUTube neq "">
							<strong class="summaryText">
								<cfoutput>
									<a href="<cfif NOT FindNoCase('http://', podValues.videoUTube) AND NOT FindNoCase('https://', podValues.videoUTube)>https://</cfif>#podValues.videoUTube#" target="_blank">#podValues.videoUTube#</a>
								</cfoutput>
							</strong>
						<cfelse>
							<span class="required summaryText"><strong>Not Set</strong></span>
						</cfif>
					</cfif>
				</div>
			</div>

			<div class="questionSpacer">
				The video will not be judged based on the technical quality - videos are reviewed for content only. 
				We simply want to see your excitement and passion for STEM. 
				You can use a flip cam, cell phone, etc. to create the video. 
				Once you are ready to upload the video, go to YouTube.com to create an account and 
				<a href="http://www.youtube.com/my_videos_upload" target="_blank">upload your video</a>.
				If you would not like your video to appear in any of YouTube's public spaces, or search results, please click
				<a href="http://www.google.com/support/youtube/bin/answer.py?answer=181547" target="_blank">here</a>.
			</div>

		</cfsavecontent>

		<cfreturn podHTML />
	</cffunction>


	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Video Upload" />
	</cffunction>


	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />

		<cfset var sectionValues = "" />

		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT videoType, videoUTube
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfreturn sectionValues />
	</cffunction>


	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />

		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = false />

		<cfif podValues.videoType neq "" AND podValues.videoUTube neq "">
			<cfset retVal = true />
		</cfif>

		<cfreturn retVal />
	</cffunction>


	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />

		<cfset var save = "" />


		<cfquery name="save" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET <cfif StructKeyExists(arguments.aform, "videoType") AND arguments.aform.videoType neq "">
					videoType = <cfqueryparam value="#arguments.aform.videoType#" cfsqltype="cf_sql_longvarchar" />,
				<cfelse>
					videoType = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />,
				</cfif>

				<cfif Trim(arguments.aform.videoUTube) neq "">
					videoUTube = <cfqueryparam value="#Trim(arguments.aform.videoUTube)#" cfsqltype="cf_sql_varchar" />
				<cfelse>
					videoUTube = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />
				</cfif>

			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>


	</cffunction>



</cfcomponent>