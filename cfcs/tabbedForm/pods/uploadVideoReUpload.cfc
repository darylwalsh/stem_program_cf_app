
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!------ FOR 2012 - We need to upgrade the flash uploader and the the user process.        ------>
<!------ FOR 2012 - Create a new flash uploader.  The current one has many issues (% off)  ------>
<!------            and needs more funcionality (triggers flash functions) added to it.    ------>
<!------ FOR 2012 - Add application status to the left nav (for all tabs).                 ------>
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->

<cfcomponent displayname="uploadVideo.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var i = "required" />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		
		<cfsavecontent variable="podHTML">
			
			<cfif StructKeyExists(url, "uploaded")>
				<div style="color:#000099; font-size:18px; font-weight:bold; margin-bottom:10px;">
					Thank you, your file has been received.
				</div>
			</cfif>
			
			<input type="hidden" name="applicationGUID" id="applicationGUID" value="<cfoutput>#url.applicationGUID#</cfoutput>" />
			
			<div style="margin-left:20px;">
				<div class="bolded question questionSpacerSmall paddingLeft">
					Please create a video and post it to YouTube that is <span style="text-decoration:underline;">no longer than two minutes</span>
				in length answering <span style="text-decoration:none;">one</span> of the following questions (Please note, if your
				video is longer than two minutes, only the first two minutes will be scored.): 
				</div>
				<div class="questionSmall questionSpacerSmall paddingLeft">
					(Only the first 60 seconds of any submitted video will be viewed.  Only the following file formats are acceptable: .flv, .mov, .mpg, and .wmv)
				</div>
			</div>
			
			<cfif podValues.videoFileName neq "" OR arguments.podType eq "summary">
				<div class="questionSpacer paddingLeft" style="margin-top:10px; margin-left:30px; font-size:14px;">
					<cfif podValues.videoFileName eq "">
						<span class="required"><strong>No video has been uploaded</strong></span>
					<cfelse>
						<cfoutput>
							<a href="http://static.redactededucation.com/feeds/stem-application-videos/#podValues.videoFileName#" target="_STEM">Click Here</a>
							to view your uploaded video. 
							<cfif arguments.podType eq "edit">
								(may take up to 5 minutes to view after upload)
							</cfif>
						</cfoutput>
					</cfif>
				</div>
			</cfif>
			
			<cfif arguments.podType eq "edit">
				<div id="uploadVideoContainer" style="border:1px solid #cccccc;">
					&nbsp;
				</div>
				
				<script type="text/javascript">
					function UploadCompleted (filename)
					{
						var formData = {};
						/*
						formData.componentName = "uploadVideoReUpload";
						formData.functionName = "SaveFile";
						formData.fileName = String(filename);
						formData.applicationGUID = jQuery("#applicationGUID").val();
						*/
						
						jQuery.post("/cfcs/tabbedForm/pods/uploadVideoReUpload.cfc?method=SaveFile&applicationGUID=" + jQuery("#applicationGUID").val() + "&fileName=" + String(filename), formData, function(resp){
							if(Trim(resp).toLowerCase() == "success"){
								<cfoutput>
								location.href = "/index.cfm?event=showSTEMApplicationReUpload&applicationGUID=#url.applicationGUID#&uploaded=1";
								</cfoutput>
							}
							else {
								alert("An error occurred during upload.  Your file was not saved.");
							}
						});
					}
					
					jQuery().ready(function () {
						var flashvars = {};
						
						flashvars.type = "Videos";
						<cfoutput>
						<cfif FindNoCase("local", cgi.server_name) gt 0>
							flashvars.pathUploadScript = "http://local.creative.redactededucation.com/functions/upload-stem-application-video.cfm";
						<cfelseif FindNoCase("dev", cgi.server_name) gt 0>
							flashvars.pathUploadScript = "http://creative.redactededucation.com/functions/upload-stem-application-video.cfm";
						<cfelseif FindNoCase("stage", cgi.server_name) gt 0>
							flashvars.pathUploadScript = "http://creative.redactededucation.com/functions/upload-stem-application-video.cfm";
						<cfelse>
							flashvars.pathUploadScript = "http://creative.redactededucation.com/functions/upload-stem-application-video.cfm";
						</cfif>
						</cfoutput>
						flashvars.maxupload = 104857600;
						flashvars.completeFunc = "UploadCompleted";
						
						var params = {};
						params.wmode = "transparent";
						params.allowScriptAccess = 'always'; //this must be here when using ajaxy type stuff
						
						var attributes = {};
						attributes.id = "uploadVideoContainer";
						swfobject.embedSWF("http://static.redactededucation.com/global/swf/generic-upload.swf", "uploadVideoContainer", "350", "150", "8.0.0", false, flashvars, params, attributes);
					});
				</script>
			</cfif>
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Upload Video" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT videoType, (SELECT TOP 1 videoFileName FROM STEMAppsVideoFiles (nolock) WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" /> ORDER BY created DESC)  AS videoFileName
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = false />
		
		<cfif podValues.videoType neq "" AND podValues.videoFileName neq "">
			<cfset retVal = true />
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var save = "" />
		
		
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	<cffunction name="SaveFile" access="remote" returntype="void" output="true">
		<cfargument name="applicationGUID" type="string" required="false" hint="getting passed via request scope (request.applicationGUID)." />
		<cfargument name="fileName" type="string" required="false" hint="getting passed via request scope (request.additionalArgument)." />
		
		<cfset var retVal = "failed" />
		<cfset var save = "" />
		
		<cfif StructKeyExists(arguments, "applicationGUID") AND StructKeyExists(arguments, "fileName")>
			<cfquery name="save" datasource="Sponsorships">
				INSERT INTO STEMAppsVideoFiles (applicationGUID, videoFileName)
				VALUES (
							<cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />,
							<cfqueryparam value="#arguments.fileName#" cfsqltype="cf_sql_varchar" />
						)
			</cfquery>
			<cfset retVal = "success" />
		</cfif>
		
		#retVal#
	</cffunction>
	
	
</cfcomponent>