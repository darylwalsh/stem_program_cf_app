<cfcomponent displayname="podEXTENDS.cfc">
	
	<!----------------------------->
	<!--- Initialize variables  --->
	<!----------------------------->
	
	<cfset variables = StructNew() />
	<cfset variables.settings = StructNew() />
	
	
	
	<!----------------------------->
	<!--- Setup Functions       --->
	<!----------------------------->
	
	<cffunction name="Init" access="public" returntype="any" output="true">
		<cfargument name="settings" type="struct" required="true" />
		
		<cfset variables.settings = arguments.settings />
		
		
		<cfreturn this />
	</cffunction>
	
	
	<!----------------------------->
	<!--- Public Functions      --->
	<!----------------------------->
	
	<cffunction name="BuildPod" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var podHTML = "" />
		<cfset var podTitle = GetPodTitle() />
		
		<cfoutput>
		<cfsavecontent variable="podHTML">
			<div class="pod">
				<cfif podTitle neq "" AND arguments.podType eq "edit">
					<div class="podTitleLeft">&nbsp;</div>
					<div class="podTitleCenter">
						<div class="padding">
							#podTitle#
						</div>
					</div>
					<div class="podTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>
				</cfif>
				<div class="podForm">
					#GetPodHTML(arguments.applicationGUID, arguments.podType)#
				</div>
			</div>
		</cfsavecontent>
		</cfoutput>
		
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="BuildButton" access="public" returntype="string" output="false">
		<cfargument name="displayText" type="string" required="true" />
		<cfargument name="onClickJSRun" type="string" required="true" hint="run this javascript when clicked.  Pass in a function to call." />
		<cfargument name="buttonID" type="string" required="false" default="" />
		
		<cfset var buttonHTML = "" />
		
		<cfset arguments.onClickJSRun = Replace(arguments.onClickJSRun, """", "'", "all") />
		
		<cfoutput>
		<cfsavecontent variable="buttonHTML">
			<div class="button" <cfif arguments.buttonID neq "">id="#arguments.buttonID#"</cfif> onmouseover="this.className='button buttonHover';" onmouseout="this.className='button';" onclick="#arguments.onClickJSRun#">
				<div class="left">
					&nbsp;
				</div>
				<div class="middle">
					#arguments.displayText#
				</div>
				<div class="right">
					&nbsp;
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div class="clear">&nbsp;</div>
		</cfsavecontent>
		</cfoutput>
		
		<cfreturn buttonHTML />
	</cffunction>
	
	
	<cffunction name="DateToDBFormat" access="public" returntype="string" output="false">
		<cfargument name="adate" type="string" required="true" />
		
		<!--- datetime data types are restricted with date range - 01/01/1753, through 12/31/9999 --->
		<cfif IsDate(arguments.adate) AND DateCompare(arguments.adate, "1793-01-01 00:00:00") gte 0 AND DateCompare(arguments.adate, "9999-12-31 23:59:59") lte 0>
			<cfset arguments.adate = DateFormat(arguments.adate, "yyyy-mm-dd") & " 12:00:00" />
		<cfelse>
			<cfset arguments.adate = "" />
		</cfif>
		
		<cfreturn arguments.adate />
	</cffunction>
	
	
</cfcomponent>