<cfcomponent displayname="responsibilites.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var dateIds = "" />
		<cfset var i = "" />
		<cfset var id = "" />
		<cfset var option = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		<cfif arguments.podType eq "edit" AND podValues.RecordCount lte 0>
			<cfset QueryAddRow(podValues) />
		</cfif>
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div id="responsibilitiesPOD">
				<div class="question questionSpacer">
					List up to three educational committees/organizations in which you participate (most recent first).
				</div>
				
				
				<cfif arguments.podType eq "edit">
					<cfloop query="podValues">
						<cfset i = CreateUUID() />
						<cfset dateIds = ListAppend(dateIds, "responsibilitiesDateFrom-" & i) />
						<cfset dateIds = ListAppend(dateIds, "responsibilitiesDateTo-" & i) />
						
						<div id="responsibilitiesRow-#i#" class="questionRow">
							<div class="questionSmall floatLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										From:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.dateFrom />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="responsibilitiesDateFrom-#i#" id="responsibilitiesDateFrom-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft" style="width:56px;">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall"><nobr>To Present?</nobr></div>
								</cfif>
								<div class="floatLeft">
									<input type="checkbox" name="responsibilitiesDateToPresent-#i#" id="responsibilitiesDateToPresent-#i#" value="1" style="margin:2px 0px 0px 20px" onclick="CheckResponsibilityPresent('#i#');" <cfif podValues.dateToPresent eq true>checked="checked"</cfif> />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										To:
									</div>
								</cfif>
								<div class="floatLeft" id="divDateTo-#i#" <cfif podValues.dateToPresent eq true>style="visibility:hidden;"</cfif>>
									<cfset value = podValues.dateTo />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="responsibilitiesDateTo-#i#" id="responsibilitiesDateTo-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										Organization:
									</div>
								</cfif>
								<div class="floatLeft">
									<input type="text" class="large" name="responsibilitiesOrganization-#i#" id="responsibilitiesOrganization-#i#" value="#podValues.organization#" maxlength="100" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										Role (e.g. member, leader):
									</div>
								</cfif>
								<div class="floatLeft">
									<input type="text" class="large" name="responsibilitiesRole-#i#" id="responsibilitiesRole-#i#" value="#podValues.role#" maxlength="100" />
								</div>
							</div>
							
							<div class="floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">&nbsp;</div>
								</cfif>
								<div class="floatLeft">
									<cfif podValues.CurrentRow eq 1>
										#BuildButton("Add Another", "AddResponsibilitiesRow();", "addResponsibilitiesBtn")#
									<cfelse>
										#BuildButton("Delete", "RemoveResponsibilitiesRow('responsibilitiesRow-#i#');")#
									</cfif>
								</div>
							</div>
							
							<div class="clear">&nbsp;</div>
						</div>
						
						<cfif podValues.dateToPresent eq true>
							<script type="text/javascript">
								jQuery().ready(function () {
									window.setTimeout("hideResponsibilityCalendar('#i#')", 700);
								});
							</script>
						</cfif>
						
					</cfloop>
					
					<script type="text/javascript">
						function hideResponsibilityCalendar (rowId)
						{
							jQuery("##fd-but-responsibilitiesDateTo-" + rowId).css("visibility", "hidden");
						}
					</script>
					
				<cfelse>
					
					<table class="summary">
						<tr class="head">
							<td style="width:90px;">
								From:
							</td>
							<td style="width:90px;">
								<nobr>To Present?</nobr>
							</td>
							<td style="width:90px;">
								To:
							</td>
							<td style="width:160px;">
								Organization:
							</td>
							<td style="width:160px;">
								Role (e.g. member, leader):
							</td>
						</tr>
						<cfif podValues.RecordCount gt 0>
							<cfloop query="podValues">
								<tr class="row">
									<td>
										<cfset value = podValues.dateFrom />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.dateToPresent eq true>X</cfif>
									</td>
									<td>
										<cfif podValues.dateToPresent eq true>
											-
										<cfelse>
											<cfset value = podValues.dateTo />
											<cfif IsDate(value)>
												<cfset value = DateFormat(value, "mm/dd/yyyy") />
												#value#
											<cfelse>
												<span class="required">Not Set</span>
											</cfif>
										</cfif>
									</td>
									<td>
										<cfif podValues.organization neq "">
											#podValues.organization#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.role neq "">
											#podValues.role#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
								</tr>
							</cfloop>
						<cfelse>
							<tr class="rowEmpty">
								<td colspan="4">
									<em>0 entries entered</em>
								</td>
							</tr>
						</cfif>
					</table>
					
				</cfif>	
			</div>
			</cfoutput>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					function AddResponsibilitiesRow ()
					{
						jQuery.post(remoteURL + "?method=AJAXCall", {componentName: "responsibilities", functionName: "GetEmptyFormRow"}, function(html){
							jQuery("#responsibilitiesPOD").append(html);
							
							var rowCount = jQuery("input[name^='responsibilitiesDateFrom-']");
							
							if(rowCount.length >= 3){
								jQuery("div[id='addResponsibilitiesBtn']").hide();
							}
							
						});
					}
					
					
					function CheckResponsibilityPresent (rowId)
					{
						if(jQuery("#responsibilitiesDateToPresent-" + rowId).attr("checked")) {
							jQuery("#responsibilitiesDateTo-" + rowId).val("");
							jQuery("#fd-but-responsibilitiesDateTo-" + rowId).css("visibility", "hidden");
							jQuery("#divDateTo-" + rowId).css("visibility", "hidden");
						}
						else {
							jQuery("#divDateTo-" + rowId).css("visibility", "visible");
							jQuery("#fd-but-responsibilitiesDateTo-" + rowId).css("visibility", "visible");
						}
					}
					
					
					function RemoveResponsibilitiesRow (rowId)
					{
						jQuery("#" + rowId).remove();
						
						var rowCount = jQuery("input[name^='responsibilitiesDateFrom-']");
							
						if(rowCount.length <= 2){
							jQuery("div[id='addResponsibilitiesBtn']").show();
						}
					}
					
					
					jQuery().ready(function () {
						<cfoutput>
						<cfloop list="#dateIds#" index="id">
						datePickerController.createDatePicker({formElements:{"#id#":"m-sl-d-sl-Y"}});
						</cfloop>
						</cfoutput>
					});
				</script>
			</cfif>
		</cfsavecontent>
		
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Memberships" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT dateFrom, dateToPresent, dateTo, organization, role
			FROM STEMAppsResponsibilities (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY dateTo DESC
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = true />
		
		<cfloop query="podValues">
			<cfif NOT IsDate(podValues.dateFrom) OR (podValues.dateToPresent neq true AND NOT IsDate(podValues.dateTo)) OR podValues.organization eq "" OR podValues.role eq "">
				<cfset retVal = false />
			</cfif>
		</cfloop>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var key = "" />
		<cfset var keyWordCheck = "responsibilitiesDateFrom" />
		<cfset var postfix = "" />
		<cfset var removeAll = "" />
		<cfset var save = "" />
		
		<cfquery name="removeAll" datasource="#variables.settings.dsn#">
			DELETE FROM STEMAppsResponsibilities
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfloop collection="#arguments.aform#" item="key">
			<cfif Left(key, Len(keyWordCheck)) eq keyWordCheck>
				<cfset postfix = Right(key, Len(key) - Len(keyWordCheck))>
				
				
				<cfif IsDate(arguments.aform['responsibilitiesDateFrom' & postfix]) OR IsDate(arguments.aform['responsibilitiesDateTo' & postfix]) OR arguments.aform['responsibilitiesOrganization' & postfix] neq "" OR arguments.aform['responsibilitiesRole' & postfix] neq "">
					<cfquery name="save" datasource="#variables.settings.dsn#">
						INSERT INTO STEMAppsResponsibilities (applicationGUID, dateFrom, dateToPresent, dateTo, organization, role)
						VALUES (
									<cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />,
									<cfif IsDate(DateToDBFormat(arguments.aform['responsibilitiesDateFrom' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['responsibilitiesDateFrom' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfif StructKeyExists(arguments.aform, "responsibilitiesDateToPresent" & postfix) >
										<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
									<cfelse>
										<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
									</cfif>
									<cfif IsDate(DateToDBFormat(arguments.aform['responsibilitiesDateTo' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['responsibilitiesDateTo' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfqueryparam value="#arguments.aform['responsibilitiesOrganization' & postfix]#" cfsqltype="cf_sql_varchar" />,
									<cfqueryparam value="#arguments.aform['responsibilitiesRole' & postfix]#" cfsqltype="cf_sql_varchar" />
								)
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	<cffunction name="GetEmptyFormRow" access="public" returntype="string" output="false">
		
		<cfset var emptyFormRow = "" />
		<cfset var i = CreateUUID() />
		<cfset var option = "" />
		<cfset var options = "" />
		
		<cfoutput>
		<cfsavecontent variable="emptyFormRow">
		<div id="responsibilitiesRow-#i#" class="questionRow">
			<div class="questionSmall floatLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="responsibilitiesDateFrom-#i#" id="responsibilitiesDateFrom-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft" style="width:56px;">
				<div class="floatLeft">
					<input type="checkbox" name="responsibilitiesDateToPresent-#i#" id="responsibilitiesDateToPresent-#i#" value="1" style="margin:2px 0px 0px 20px" onclick="CheckResponsibilityPresent('#i#');" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft" id="divDateTo-#i#">
				<div class="floatLeft">
					<input type="text" class="small" name="responsibilitiesDateTo-#i#" id="responsibilitiesDateTo-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="large" name="responsibilitiesOrganization-#i#" id="responsibilitiesOrganization-#i#" value="" maxlength="100" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="large" name="responsibilitiesRole-#i#" id="responsibilitiesRole-#i#" value="" maxlength="100" />
				</div>
			</div>
			
			<div class="floatLeft paddingLeft">
				<div class="floatLeft">
					#BuildButton("Delete", "RemoveResponsibilitiesRow('responsibilitiesRow-#i#');")#
				</div>
			</div>
			
			<div class="clear">&nbsp;</div>
		</div>
		
		<script type="text/javascript">
			jQuery().ready(function () {
				datePickerController.createDatePicker({formElements:{"responsibilitiesDateFrom-#i#":"m-sl-d-sl-Y"}});
				datePickerController.createDatePicker({formElements:{"responsibilitiesDateTo-#i#":"m-sl-d-sl-Y"}});
			});
		</script>
		</cfsavecontent>
		</cfoutput>
		
		
		<cfreturn emptyFormRow />
	</cffunction>
	
	
	
	
	
</cfcomponent>