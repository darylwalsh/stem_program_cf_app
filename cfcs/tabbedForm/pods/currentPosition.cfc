<cfcomponent displayname="currentPosition.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var i = "required" />
		<cfset var grades = "" />
		<cfset var option = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div id="currentPositionPOD">
				
				<div class="questionSpacerSmall">
					<span class="required">*</span>
					Is your current position full-time?
				</div>
				<div class="questionSpacer">
					<cfif arguments.podType eq "edit">
						<input type="radio" name="currentPositionFullTime" id="currentPositionFullTime-1" value="1" 
							class="currentPositionFullTime"
						<cfif podValues.currentPositionFullTime eq "1">checked="checked"</cfif> />
						Yes
						&nbsp;
						<input type="radio" name="currentPositionFullTime" id="currentPositionFullTime-0" value="0" 
						class="currentPositionFullTime" onclick="OpenWorkLB();"
						
						<cfif podValues.currentPositionFullTime eq "0">checked="checked"</cfif> />
						No
					<cfelse>
						<cfif podValues.currentPositionFullTime eq "1">
							<strong class="summaryText">Yes</strong>
						<cfelseif podValues.currentPositionFullTime eq "0">
							<strong class="summaryText">No</strong>
						<cfelse>
							<span class="required summaryText"><strong>Not Set</strong></span>
						</cfif>
					</cfif>
				</div>
				
				<div class="questionSpacerSmall">
					<span class="required">*</span>
					Do you expect to hold the same position in the 2012-13 school year?
				</div>
				<div class="questionSpacer">
					<cfif arguments.podType eq "edit">
						<input type="radio" name="currentPositionNextYear" id="currentPositionNextYear-1" value="1" onclick="CurrentPositionCheckNo();" <cfif podValues.currentPositionNextYear eq "1">checked="checked"</cfif> />
						Yes
						&nbsp;
						<input type="radio" name="currentPositionNextYear" id="currentPositionNextYear-0" value="0" onclick="CurrentPositionCheckNo();" <cfif podValues.currentPositionNextYear eq "0">checked="checked"</cfif> />
						No
					<cfelse>
						<cfif podValues.currentPositionNextYear eq "1">
							<strong class="summaryText">Yes</strong>
						<cfelseif podValues.currentPositionNextYear eq "0">
							<strong class="summaryText">No</strong>
						<cfelse>
							<span class="required summaryText"><strong>Not Set</strong></span>
						</cfif>
					</cfif>
				</div>
				
				<cfif arguments.podType eq "edit" OR podValues.currentPositionNextYear eq "0">
					<div class="questionSpacerSmall">
						If No, please explain below:
					</div>
					<div>
						<cfif arguments.podType eq "edit">
							<span class="required" style="display:none;" id="NextYearNoExplainAsterisk">*</span>
							<textarea name="currentPositionNextYearNoExplain" id="currentPositionNextYearNoExplain">#podValues.currentPositionNextYearNoExplain#</textarea>
						<cfelse>
							<div style="margin-left:15px;">
								<em class="summaryText">#podValues.currentPositionNextYearNoExplain#</em>
							</div>
						</cfif>
					</div>
				</cfif>
				
				<div class="clear">&nbsp;</div><br>
				<div class="questionSmall questionSpacer">
					<div class="questionSmall questionSpacerSmall">
						<span class="required">*</span> 
						Total number of years teaching experience in grades 6-12:
					</div>
					<cfset value = podValues.teachingYears />
					<cfset options = "< 1,1 - 5,6 - 10,11 - 15,16 - 20,20+" />
					
					<cfif arguments.podType eq "edit">
						<select name="currentPositionTeachingYears" id="currentPositionTeachingYears">
							<option value="">- Select -</option>
							<cfloop list="#options#" index="option">
								<option value="#option#" <cfif option eq value>selected="selected"</cfif>>#option#</option>
							</cfloop>
						</select>
					<cfelse>
						<cfif value neq "">
							<strong class="summaryText">#value#</strong>
						<cfelse>
							<span class="required summaryText"><strong>Not Set</strong></span>
						</cfif>
					</cfif>
				</div>
				
				<div class="bolded questionSpacerSmall" style="margin-top:8px;">
					Current Position: <span style="font-weight:normal;">To select multiple grades taught hold your CTRL key down while clicking desired options.</span>
				</div>
				
				<div class="questionSmall floatLeft questionSpacer" style="width:98px;">
					<div class="questionSmall questionSpacerSmall">
						<span class="required">*</span> 
						From:
					</div>
					<div class="floatLeft">
						<cfset value = podValues.currentPositionFrom />
						<cfif IsDate(value)>
							<cfset value = DateFormat(value, "mm/dd/yyyy") />
						</cfif>
						<cfif arguments.podType eq "edit">
							<input type="text" class="small" name="currentPositionFrom" id="currentPositionFrom" value="#value#" maxlength="10" />
						<cfelse>
							<cfif IsDate(value)>
								<strong class="summaryText">#value#</strong>
							<cfelse>
								<span class="required summaryText"><strong>Not Set</strong></span>
							</cfif>
						</cfif>
					</div>
				</div>
				
				<div class="questionSmall floatLeft questionSpacer" style="width:98px;">
					<div class="questionSmall questionSpacerSmall">
						<span class="required">*</span> 
						To:
					</div>
					<div class="floatLeft">
						<cfset value = podValues.currentPositionTo />
						<cfif IsDate(value)>
							<cfset value = DateFormat(value, "mm/dd/yyyy") />
						</cfif>
						<cfif arguments.podType eq "edit">
							<input type="text" class="small" name="currentPositionTo" id="currentPositionTo" value="#value#" maxlength="10" />
						<cfelse>
							<cfif IsDate(value)>
								<strong class="summaryText">#value#</strong>
							<cfelse>
								<span class="required summaryText"><strong>Not Set</strong></span>
							</cfif>
						</cfif>
					</div>
				</div>
				
				<div class="questionSmall floatLeft paddingLeft questionSpacer" style="width:112px;">
					<div class="questionSmall questionSpacerSmall">
						<span class="required">*</span> 
						School/Organization:
					</div>
					<div class="floatLeft">
						<cfif arguments.podType eq "edit">
							<input type="text" class="large" name="currentPositionSchool" id="currentPositionSchool" value="#podValues.currentPositionSchool#" maxlength="100" />
						<cfelse>
							<cfif podValues.currentPositionSchool neq "">
								<strong class="summaryText">#podValues.currentPositionSchool#</strong>
							<cfelse>
								<span class="required summaryText"><strong>Not Set</strong></span>
							</cfif>
						</cfif>
					</div>
				</div>
				
				<div class="questionSmall floatLeft paddingLeft questionSpacer">
					<div class="questionSmall questionSpacerSmall">
						<nobr><span class="required">*</span> 
						Grades you teach:</nobr>
					</div>
					<cfset value = podValues.currentPositionGrades />
					<cfset options = "K,1,2,3,4,5,6,7,8,9,10,11,12" />
					
					<cfif arguments.podType eq "edit">
						<nobr>&nbsp;&nbsp;&nbsp;<select name="currentPositionGrades-ms" id="currentPositionGrades-ms" 
						onclick="PopulateHiddenVersion('currentPositionGrades');" style="width:43px;" multiple="multiple" size="4">
							<cfloop list="#options#" index="option">
								<option value="#option#" <cfif ListFindNoCase(value, option) gt 0>selected="selected"</cfif>>#option#</option>
							</cfloop>
						</select></nobr>
						<input type="hidden" name="currentPositionGrades" id="currentPositionGrades" value="#value#">
					<cfelse>
						<cfif value neq "">
							
							<cfset grades = "" />
							<cfloop from="1" to="#ListLen(value)#" index="i">
								<cfset grades &= ListGetAt(value, i) />
								<cfif ListLen(value) neq i>
									<cfset grades &= "," />
								</cfif>
								<cfif i mod 3 eq 0>
									<cfset grades &= "<br />" />
								</cfif>
							</cfloop>
							<strong class="summaryText">#grades#</strong>
							
						<cfelse>
							<span class="required summaryText"><strong>Not Set</strong></span>
						</cfif>
					</cfif>
				</div>
				
				<div class="questionSmall floatLeft paddingLeft questionSpacer">
					<div class="questionSmall questionSpacerSmall">
						<nobr><span class="required">*</span> 
						Courses you teach:</nobr>
					</div>
					<!---
					<cfset value = podValues.currentPositionCourse />
					<cfset options = "Science,Technology,Engineering,Mathematics,Other" />
					--->
					
					<cfif arguments.podType eq "edit">
						<!---
						<select name="currentPositionCourse" id="currentPositionCourse" onchange="CurrentPositionCheckOther();">
							<option value="">- Select -</option>
							<cfloop list="#options#" index="option">
								<option value="#option#" <cfif option eq value>selected="selected"</cfif>>#option#</option>
							</cfloop>
						</select>
						--->
						
						
						<div class="floatLeft" style="text-align:center;">
							<nobr>&nbsp;&nbsp;&nbsp;<input type="text" class="large" name="currentPositionCourse" 
									id="currentPositionCourse" value="#podValues.currentPositionCourse#" maxlength="100" /></nobr>
						</div>
						
					<cfelse>
						<cfif podValues.currentPositionCourse neq "">
							<strong class="summaryText">#podValues.currentPositionCourse#</strong>
						<cfelse>
							<span class="required summaryText"><strong>Not Set</strong></span>
						</cfif>
					</cfif>
				</div>
				
				<div class="questionSmall floatLeft paddingLeft questionSpacer" style="width:50px;">
					<div class="questionSmall questionSpacerSmall">
						<nobr><span class="required">*</span> 
						Grades in your school:</nobr>
					</div>
					<cfset value = podValues.currentPositionGradesInSchool />
					<cfset options = "K-8,6-8,9-12" />
					
					<cfif arguments.podType eq "edit">
						<div class="floatLeft">
						<nobr>&nbsp;&nbsp;&nbsp;<input type="text" class="large" name="currentPositionGradesInSchool" id="currentPositionGradesInSchool" style="width:100px;"
							value="#value#"></nobr>
						</div>
						<!--- <select name="currentPositionGradesInSchool" id="currentPositionGradesInSchool" style="width:50px;" multiple="multiple">
							<cfloop list="#options#" index="option">
								<option value="#option#" <cfif ListFindNoCase(value, option) gt 0>selected="selected"</cfif>>#option#</option>
							</cfloop>
						</select>	 --->					
					<cfelse>
						<cfif value neq "">														
							<strong class="summaryText">#value#</strong>							
						<cfelse>
							<span class="required summaryText"><strong>Not Set</strong></span>
						</cfif>
					</cfif>
				</div>
				
				<!---
				<div class="questionSmall floatLeft paddingLeft questionSpacer">
					<div class="questionSmall questionSpacerSmall" style="color:##666666; font-size:10px; font-style: italic;">
						If other, please list
					</div>
					<div class="floatLeft">
						<cfif arguments.podType eq "edit">
							<input type="text" class="large" name="currentPositionCourseOther" id="currentPositionCourseOther" value="#podValues.currentPositionCourseOther#" maxlength="100" />
						<cfelse>
							<strong class="summaryText">#podValues.currentPositionCourseOther#</strong>
						</cfif>
					</div>
				</div>
				--->
				
				<div class="clear">&nbsp;</div>
				
				
				
				
				
				
			</div>
			</cfoutput>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					function CurrentPositionCheckNo ()
					{
						var noTA = jQuery("#currentPositionNextYearNoExplain");
						
						if(String(jQuery("#currentPositionNextYear-0").attr("checked")).toLowerCase() == "true" ) {
							noTA.css("background-color", "#ffffff");
							noTA.attr("disabled", "");
							jQuery('#NextYearNoExplainAsterisk').attr('style','display:inline');
						}
						else {
							noTA.val("");
							noTA.css("background-color", "#dddddd");
							noTA.attr("disabled", "true");
							jQuery('#NextYearNoExplainAsterisk').attr('style','display:none');
						}
					}
					
					
					function CurrentPositionCheckOther ()
					{
						var otherField = jQuery("#currentPositionCourseOther");
						
						if (jQuery("#currentPositionCourse").val().toLowerCase() == "other") {
							otherField.css("background-color", "#ffffff");
							otherField.attr("disabled", "");
						}
						else {
							otherField.val("");
							otherField.css("background-color", "#dddddd");
							otherField.attr("disabled", "true");
						}
					}
					
					
					function OpenWorkLB ()
					{
						OpenLightBox('/STEMApplication/lightboxes/currentPositionFullTime.html');
					}
					
					
					function PopulateHiddenVersion (idPrefix)
					{
						var multipleSelect = document.getElementById(idPrefix + "-ms");
						var retVal = new Array();
						
						for (var i = 0; i < multipleSelect.options.length; i++) {
							if (multipleSelect.options[i].selected) {
								retVal.push(multipleSelect.options[i].value);
							}
						}
						jQuery("#" + idPrefix).val(retVal.join(","));
					}
					
					
					jQuery().ready(function () {
						CurrentPositionCheckOther();
						CurrentPositionCheckNo();
						
						datePickerController.createDatePicker({formElements:{"currentPositionFrom":"m-sl-d-sl-Y"}});
						datePickerController.createDatePicker({formElements:{"currentPositionTo":"m-sl-d-sl-Y"}});
					});
				</script>
			</cfif>
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Teaching Experience" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT teachingYears, currentPositionFrom, currentPositionTo, currentPositionSchool, currentPositionGrades, currentPositionGradesInSchool, 	
			currentPositionCourse, currentPositionNextYear, currentPositionNextYearNoExplain, currentPositionFullTime
			FROM STEMApplications (nolock)
			WHERE applicationGUID = '#arguments.applicationGUID#' <!--- cfqueryparam seems to have issues here? --->
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = false />
		
		<cfif podValues.teachingYears neq "" AND IsDate(podValues.currentPositionFrom) AND IsDate(podValues.currentPositionTo) AND podValues.currentPositionSchool neq "" AND podValues.currentPositionGrades neq "" AND podValues.currentPositionCourse neq "" AND podValues.currentPositionFullTime neq "" AND (podValues.currentPositionNextYear eq true OR podValues.currentPositionNextYear eq false)>
			<cfset retVal = true />
			<cfif podValues.currentPositionNextYear is 0 and Not len(trim(podValues.currentPositionNextYearNoExplain))>
				<cfset retVal = false />
			</cfif>
			<cfif Not len(trim(podValues.currentPositionGradesInSchool))>
				<cfset retVal = false />
			</cfif>
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var save = "" />
		<cfparam name="arguments.aform.currentPositionGradesInSchool" default="">
		
		<cfquery name="save" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET teachingYears = <cfqueryparam value="#arguments.aform.currentPositionTeachingYears#" cfsqltype="cf_sql_varchar" />, 
				<cfif IsDate(DateToDBFormat(arguments.aform.currentPositionFrom))>
					currentPositionFrom = <cfqueryparam value="#DateToDBFormat(arguments.aform.currentPositionFrom)#" cfsqltype="cf_sql_timestamp" />, 
				<cfelse>
					currentPositionFrom = <cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
				</cfif>
				<cfif IsDate(DateToDBFormat(arguments.aform.currentPositionTo))>
					currentPositionTo = <cfqueryparam value="#DateToDBFormat(arguments.aform.currentPositionTo)#" cfsqltype="cf_sql_timestamp" />, 
				<cfelse>
					currentPositionTo = <cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
				</cfif>
				currentPositionSchool = <cfqueryparam value="#arguments.aform.currentPositionSchool#" cfsqltype="cf_sql_varchar" />, 
				currentPositionGrades = <cfqueryparam value="#arguments.aform.currentPositionGrades#" cfsqltype="cf_sql_varchar" />, 
				currentPositionGradesInSchool = <cfqueryparam value="#arguments.aform.currentPositionGradesInSchool#" cfsqltype="cf_sql_varchar" />, 
				currentPositionCourse = <cfqueryparam value="#arguments.aform.currentPositionCourse#" cfsqltype="cf_sql_varchar" />, 
				<!---
				<cfif arguments.aform.currentPositionCourse eq "other">
					currentPositionCourseOther = <cfqueryparam value="#arguments.aform.currentPositionCourseOther#" cfsqltype="cf_sql_varchar" />, 
				<cfelse>
					currentPositionCourseOther = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />, 
				</cfif>
				--->
				
				<cfif StructKeyExists(arguments.aform, "currentPositionFullTime")>
					currentPositionFullTime = <cfqueryparam value="#arguments.aform.currentPositionFullTime#" cfsqltype="cf_sql_bit" />,
				</cfif>
				
				<cfif StructKeyExists(arguments.aform, "currentPositionNextYear") AND arguments.aform.currentPositionNextYear eq "1">
					currentPositionNextYear = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />, 
					currentPositionNextYearNoExplain = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />
				<cfelseif StructKeyExists(arguments.aform, "currentPositionNextYear") AND arguments.aform.currentPositionNextYear eq "0">
					currentPositionNextYear = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />, 
					currentPositionNextYearNoExplain = <cfqueryparam value="#arguments.aform.currentPositionNextYearNoExplain#" cfsqltype="cf_sql_longvarchar" />
				<cfelse>
					currentPositionNextYear = <cfqueryparam value="" null="true" cfsqltype="cf_sql_bit" />, 
					currentPositionNextYearNoExplain = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />
				</cfif>
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	
</cfcomponent>