<cfcomponent displayname="activities.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var dateIds = "" />
		<cfset var i = "" />
		<cfset var id = "" />
		<cfset var option = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var value = "" />
		
		<cfif arguments.podType eq "edit" AND podValues.RecordCount lte 0>
			<cfset QueryAddRow(podValues) />
		</cfif>
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div id="activitiesPOD">
				<div class="question questionSpacer">
					Please list your extracurricular activities with students that are related to science, technology, engineering, or mathematics (STEM) education, including dates (most recent first). Please limit to 3 activities.
				</div>
				
				<cfif arguments.podType eq "edit">
					<cfloop query="podValues">
						<cfset i = CreateUUID() />
						<cfset dateIds = ListAppend(dateIds, "activitiesDateFrom-" & i) />
						<cfset dateIds = ListAppend(dateIds, "activitiesDateTo-" & i) />
						
						<div id="activitiesRow-#i#" class="questionRow">
							<div class="questionSmall floatLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										From:
									</div>
								</cfif>
								<div class="floatLeft">
									<cfset value = podValues.dateFrom />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="activitiesDateFrom-#i#" id="activitiesDateFrom-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft" style="width:56px;">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall"><nobr>To Present?</nobr></div>
								</cfif>
								<div class="floatLeft">
									<input type="checkbox" name="activitiesDateToPresent-#i#" id="activitiesDateToPresent-#i#" value="1" style="margin:2px 0px 0px 20px" onclick="CheckActivitiesPresent('#i#');" <cfif podValues.dateToPresent eq true>checked="checked"</cfif> />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										To:
									</div>
								</cfif>
								<div class="floatLeft" id="divDateTo-#i#" <cfif podValues.dateToPresent eq true>style="visibility:hidden;"</cfif>>
									<cfset value = podValues.dateTo />
									<cfif IsDate(value)>
										<cfset value = DateFormat(value, "mm/dd/yyyy") />
									</cfif>
									<input type="text" class="small" name="activitiesDateTo-#i#" id="activitiesDateTo-#i#" value="#value#" maxlength="10" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										Activity:
									</div>
								</cfif>
								<div class="floatLeft">
									<input type="text" class="large" name="activitiesActivity-#i#" id="activitiesActivity-#i#" value="#podValues.activity#" maxlength="100" />
								</div>
							</div>
							
							<div class="questionSmall floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">
										Level of Involvement: <br />
									</div>
								</cfif>
								<div class="floatLeft">
									<input type="text" class="large" name="activitiesInvolvement-#i#" id="activitiesInvolvement-#i#" value="#podValues.involvement#" maxlength="100" />
								</div>
							</div>
							
							<div class="floatLeft paddingLeft">
								<cfif podValues.CurrentRow eq 1>
									<div class="questionSmall questionSpacerSmall">(i.e. time commitment)</div>
								</cfif>
								<div class="floatLeft">
									<cfif podValues.CurrentRow eq 1>
										#BuildButton("Add Another", "AddActivitiesRow();")#
									<cfelse>
										#BuildButton("Delete", "RemoveActivitiesRow('activitiesRow-#i#');")#
									</cfif>
								</div>
							</div>
							
							<div class="clear">&nbsp;</div>
						</div>
							
						
						<cfif podValues.dateToPresent eq true>
							<script type="text/javascript">
								jQuery().ready(function () {
									window.setTimeout("hideActivityCalendar('#i#')", 700);
								});
							</script>
						</cfif>
						
					</cfloop>
					
					<script type="text/javascript">
						function hideActivityCalendar (rowId)
						{
							jQuery("##fd-but-activitiesDateTo-" + rowId).css("visibility", "hidden");
						}
					</script>
					
				<cfelse>
					
					<table class="summary">
						<tr class="head">
							<td style="width:90px;">
								From:
							</td>
							<td style="width:90px;">
								<nobr>To Present?</nobr>
							</td>
							<td style="width:90px;">
								To:
							</td>
							<td style="width:160px;">
								Activity: 
							</td>
							<td style="width:160px;">
								Involvement:
							</td>
						</tr>
						<cfif podValues.RecordCount gt 0>
							<cfloop query="podValues">
								<tr class="row">
									<td>
										<cfset value = podValues.dateFrom />
										<cfif IsDate(value)>
											<cfset value = DateFormat(value, "mm/dd/yyyy") />
											#value#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.dateToPresent eq true>X</cfif>
									</td>
									<td>
										<cfif podValues.dateToPresent eq true>
											-
										<cfelse>
											<cfset value = podValues.dateTo />
											<cfif IsDate(value)>
												<cfset value = DateFormat(value, "mm/dd/yyyy") />
												#value#
											<cfelse>
												<span class="required">Not Set</span>
											</cfif>
										</cfif>
									</td>
									<td>
										<cfif podValues.activity neq "">
											#podValues.activity#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
									<td>
										<cfif podValues.involvement neq "">
											#podValues.involvement#
										<cfelse>
											<span class="required">Not Set</span>
										</cfif>
									</td>
								</tr>
							</cfloop>
						<cfelse>
							<tr class="rowEmpty">
								<td colspan="4">
									<em>0 entries entered</em>
								</td>
							</tr>
						</cfif>
					</table>
					
				</cfif>
			</div>
			</cfoutput>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					function AddActivitiesRow ()
					{
						jQuery.post(remoteURL + "?method=AJAXCall", {componentName: "activities", functionName: "GetEmptyFormRow"}, function(html){
							jQuery("#activitiesPOD").append(html);
						});
					}
					
					
					function CheckActivitiesPresent (rowId)
					{
						if(jQuery("#activitiesDateToPresent-" + rowId).attr("checked")) {
							jQuery("#activitiesDateTo-" + rowId).val("");
							jQuery("#fd-but-activitiesDateTo-" + rowId).css("visibility", "hidden");
							jQuery("#divDateTo-" + rowId).css("visibility", "hidden");
						}
						else {
							jQuery("#divDateTo-" + rowId).css("visibility", "visible");
							jQuery("#fd-but-activitiesDateTo-" + rowId).css("visibility", "visible");
						}
					}
					
					
					function RemoveActivitiesRow (rowId)
					{
						jQuery("#" + rowId).remove();
					}
					
					jQuery().ready(function () {
						<cfoutput>
						<cfloop list="#dateIds#" index="id">
						datePickerController.createDatePicker({formElements:{"#id#":"m-sl-d-sl-Y"}});
						</cfloop>
						</cfoutput>
					});
				</script>
			</cfif>
		</cfsavecontent>
		
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "Extracurricular Activities" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT dateFrom, dateToPresent, dateTo, activity, involvement
			FROM STEMAppsActivities (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY dateTo DESC
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = true />
		
		<cfloop query="podValues">
			<cfif NOT IsDate(podValues.dateFrom) OR (podValues.dateToPresent neq true AND NOT IsDate(podValues.dateTo)) OR podValues.activity eq "" OR podValues.involvement eq "">
				<cfset retVal = false />
			</cfif>
		</cfloop>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var key = "" />
		<cfset var keyWordCheck = "activitiesDateFrom" />
		<cfset var postfix = "" />
		<cfset var removeAll = "" />
		<cfset var save = "" />
		
		<cfquery name="removeAll" datasource="#variables.settings.dsn#">
			DELETE FROM STEMAppsActivities
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfloop collection="#arguments.aform#" item="key">
			<cfif Left(key, Len(keyWordCheck)) eq keyWordCheck>
				<cfset postfix = Right(key, Len(key) - Len(keyWordCheck)) />
				
				<cfif IsDate(arguments.aform['activitiesDateFrom' & postfix]) OR IsDate(arguments.aform['activitiesDateTo' & postfix]) OR arguments.aform['activitiesActivity' & postfix] neq "" OR arguments.aform['activitiesInvolvement' & postfix] neq "">
					<cfquery name="save" datasource="#variables.settings.dsn#">
						INSERT INTO STEMAppsActivities (applicationGUID, dateFrom, dateToPresent, dateTo, activity, involvement)
						VALUES (
									<cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />,
									<cfif IsDate(DateToDBFormat(arguments.aform['activitiesDateFrom' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['activitiesDateFrom' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfif StructKeyExists(arguments.aform, "activitiesDateToPresent" & postfix) >
										<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
									<cfelse>
										<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
									</cfif>
									<cfif IsDate(DateToDBFormat(arguments.aform['activitiesDateTo' & postfix]))>
										<cfqueryparam value="#DateToDBFormat(arguments.aform['activitiesDateTo' & postfix])#" cfsqltype="cf_sql_timestamp" />,
									<cfelse>
										<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
									</cfif>
									<cfqueryparam value="#arguments.aform['activitiesActivity' & postfix]#" cfsqltype="cf_sql_varchar" />,
									<cfqueryparam value="#arguments.aform['activitiesInvolvement' & postfix]#" cfsqltype="cf_sql_varchar" />
								)
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	<cffunction name="GetEmptyFormRow" access="public" returntype="string" output="false">
		
		<cfset var emptyFormRow = "" />
		<cfset var i = CreateUUID() />
		<cfset var option = "" />
		<cfset var options = "" />
		
		<cfoutput>
		<cfsavecontent variable="emptyFormRow">
		<div id="activitiesRow-#i#" class="questionRow">
			<div class="questionSmall floatLeft">
				<div class="floatLeft">
					<input type="text" class="small" name="activitiesDateFrom-#i#" id="activitiesDateFrom-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft" style="width:56px;">
				<div class="floatLeft">
					<input type="checkbox" name="activitiesDateToPresent-#i#" id="activitiesDateToPresent-#i#" value="1" style="margin:2px 0px 0px 20px" onclick="CheckActivitiesPresent('#i#');" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft" id="divDateTo-#i#">
				<div class="floatLeft">
					<input type="text" class="small" name="activitiesDateTo-#i#" id="activitiesDateTo-#i#" value="" maxlength="10" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="large" name="activitiesActivity-#i#" id="activitiesActivity-#i#" value="" maxlength="100" />
				</div>
			</div>
			
			<div class="questionSmall floatLeft paddingLeft">
				<div class="floatLeft">
					<input type="text" class="large" name="activitiesInvolvement-#i#" id="activitiesInvolvement-#i#" value="" maxlength="100" />
				</div>
			</div>
			
			<div class="floatLeft paddingLeft">
				<div class="floatLeft">
					#BuildButton("Delete", "RemoveActivitiesRow('activitiesRow-#i#');")#
				</div>
			</div>
			
			<div class="clear">&nbsp;</div>
		</div>
		
		
		<script type="text/javascript">
			jQuery().ready(function () {
				datePickerController.createDatePicker({formElements:{"activitiesDateFrom-#i#":"m-sl-d-sl-Y"}});
				datePickerController.createDatePicker({formElements:{"activitiesDateTo-#i#":"m-sl-d-sl-Y"}});
			});
		</script>
		</cfsavecontent>
		</cfoutput>
		
		
		<cfreturn emptyFormRow />
	</cffunction>
	
	
	
</cfcomponent>