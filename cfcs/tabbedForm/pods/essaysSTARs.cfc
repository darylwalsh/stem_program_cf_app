<cfcomponent displayname="essaysSTARs.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var i = "required" />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var unique = CreateUUID() />
		<cfset var value = "" />
		
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div class="tabDescription" style="margin-bottom:15px;">
				Please be as thorough as possible in your responses.
			</div>
				
			<div id="STARsEssays">
				<!--- <cfif podValues.programSTARsOR eq true OR podValues.programSTARsPN eq true> --->
					<div class="question questionSpacer">
						<span class="required">*</span>						
						1. Please describe your laboratory skills related to using equipment such as balances, fume hoods, oscilloscopes, spectrometers, etc. 
						(150 maximum words)
					</div>
					<cfif arguments.podType eq "edit">
						<textarea class="ckeditor" id="essaySTARs1-#unique#" name="essaySTARs1">#podValues.essaySTARs1#</textarea>
						<input name="essaySTARs1-#unique#WordCount" type="hidden" value="150" /><!--needed for word count-->
					<cfelse>
						<div style="margin-left:15px;">
							<cfif podValues.essaySTARs1 neq "">
								<em class="summaryText">#podValues.essaySTARs1#</em>
							<cfelse>
								<span class="required summaryText"><strong>Not Set</strong></span>
							</cfif>
						</div>
					</cfif>
					
					<div class="question questionSpacer" style="margin-top:20px;">
						<div class="question questionSpacer">
						<span class="required">*</span>
						2. Have you previously worked in a science profession other than teaching (e.g. lab researcher, engineer, chemist)? 
						</div>					
						<cfif arguments.podType eq "edit">
						&nbsp;&nbsp;&nbsp;
						<input type="radio" name="essaySTARs2YN" value="yes"<cfif podValues.essaySTARs2YN is "yes"> checked</cfif>> Yes
						&nbsp;&nbsp;&nbsp;
						<input type="radio" name="essaySTARs2YN" value="no"<cfif podValues.essaySTARs2YN is "no"> checked</cfif>> No
						<br>
						</cfif>
						If yes, indicate the number of years you worked, your title, and a brief description of the position(s) below.
						(150 maximum words)	
						<!--- Describe three ways in which you believe the Siemens Teachers as Researchers professional development 
						program will improve teaching and learning in your classroom. (350 maximum words)  --->
					</div>
					
					<cfif arguments.podType eq "edit">
						<textarea class="ckeditor" id="essaySTARs2-#unique#" name="essaySTARs2">#podValues.essaySTARs2#</textarea>
						<input name="essaySTARs2-#unique#WordCount" type="hidden" value="150" /><!--needed for word count-->
					<cfelse>
						<div style="margin-left:15px;">
							<cfif podValues.essaySTARs2 neq "">
								<em class="summaryText">#podValues.essaySTARs2#</em>
							<cfelse>
								<span class="required summaryText"><strong>Not Set</strong></span>
							</cfif>
						</div>
					</cfif>
					
				<!--- </cfif> --->
			</div>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					jQuery().ready(function () {
						<cfif podValues.programSTARsOR eq true OR podValues.programSTARsPN eq true>
							jQuery("##essaySTARs1-#unique#").ckeditor( function() { /* callback code */ }, { /*skin : 'office2003'*/ toolbar: 'main' } );
							jQuery("##essaySTARs2-#unique#").ckeditor( function() { /* callback code */ }, { /*skin : 'office2003'*/ toolbar: 'main' } );
						<cfelse>
							jQuery("##STARsEssays").parent().parent().css("display", "none");
						</cfif>
					});
				</script>
			</cfif>
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "STARs Essay Questions" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT programSTARsOR, programSTARsPN, essaySTARs1, essaySTARs2, essaySTARs2YN
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var retVal = false />
		
		<!--- first check if active --->
		
		<cfif (podValues.programSTARsOR neq true AND podValues.programSTARsPN neq true) OR (podValues.essaySTARs1 neq "" AND podValues.essaySTARs2YN neq "")>
			<cfset retVal = true />			
		</cfif>
			<cfif podValues.essaySTARs2YN is "yes" and Not len(trim(podValues.essaySTARs2))>
				<cfset retVal = false />
			</cfif>
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
		<cfset var save = "" />
		<cfparam name="arguments.aform.essaySTARs2YN" default="">
		<cfparam name="arguments.aform.essaySTARs2" default="">
		
		<cfquery name="save" datasource="#variables.settings.dsn#">
			UPDATE STEMApplications
			SET <cfif StructKeyExists(arguments.aform, "essaySTARs1") AND arguments.aform.essaySTARs1 neq "">
					essaySTARs1 = <cfqueryparam value="#arguments.aform.essaySTARs1#" cfsqltype="cf_sql_longvarchar" />,
				<cfelse>
					essaySTARs1 = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />,
				</cfif>
				essaySTARs2YN = <cfqueryparam value="#arguments.aform.essaySTARs2YN#" cfsqltype="cf_sql_varchar" />,
				essaySTARs2 = <cfqueryparam value="#arguments.aform.essaySTARs2#" cfsqltype="cf_sql_longvarchar" />
				<!--- <cfif StructKeyExists(arguments.aform, "essaySTARs2") AND arguments.aform.essaySTARs2 neq "">
					essaySTARs2 = <cfqueryparam value="#arguments.aform.essaySTARs2#" cfsqltype="cf_sql_longvarchar" />
				<cfelse>
					essaySTARs2 = <cfqueryparam value="" null="true" cfsqltype="cf_sql_longvarchar" />
				</cfif> --->
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
	</cffunction>

	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	
</cfcomponent>