<cfcomponent displayname="letterRecommendationJudgingVersion.cfc" extends="podEXTENDS" implements="podINTERFACE">
	
	<!------------------------------------------>
	<!--- Required Functions from interface  --->
	<!------------------------------------------>
	
	<cffunction name="GetPodHTML" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="podType" type="string" required="true" hint="set to either edit or summary" />
		
		<cfset var i = "required" />
		<cfset var option = "" />
		<cfset var options = "" />
		<cfset var podHTML = "" />
		<cfset var podValues = GetPodValues(arguments.applicationGUID) />
		<cfset var podEmails = GetPodEmails(arguments.applicationGUID) />
		<cfset var podPhones = GetPodPhones(arguments.applicationGUID) />
		<cfset var recLink = "" />
		<cfset var value = "" />
		
		<cfif podEmails.RecordCount lte 0>
			<cfset QueryAddRow(podEmails) />
		</cfif>
		
		<cfif podPhones.RecordCount lte 0>
			<cfset QueryAddRow(podPhones) />
		</cfif>
		
		
		<cfsavecontent variable="podHTML">
			<cfoutput>
			<div>
				<div class="questionRow">
					<div class="question floatLeft" style="width:85px;">
						<div class="questionSmall questionSpacerSmall">
							Prefix:
						</div>
						<div class="floatLeft">
							<cfif arguments.podType eq "edit">
								<cfset options = "Mr.,Mrs.,Ms.,Dr.,Prof." />
								
								<select class="small" name="referencePrefix" id="referencePrefix">
									<option value="">- Select -</option>
									<cfloop list="#options#" index="option">
										<option value="#option#" <cfif podValues.referencePrefix eq option>selected="selected"</cfif>>#option#</option>
									</cfloop>
								</select>
							<cfelse>
								<strong>#podValues.referencePrefix#</strong>
							</cfif>
						</div>
					</div>
					
					<div class="question floatLeft paddingLeft" style="width:110px;">
						<div class="questionSmall questionSpacerSmall">
							<span class="required">*</span>
							First Name:
						</div>
						<div class="floatLeft">
							<cfif arguments.podType eq "edit">
								<input type="text" class="large" name="referenceFirstName" id="referenceFirstName" value="#podValues.referenceFirstName#" maxlength="100" />
							<cfelse>
								<cfif podValues.referenceFirstName neq "">
									<strong>#podValues.referenceFirstName#</strong>
								<cfelse>
									<span class="required"><strong>Not Set</strong></span>
								</cfif>
							</cfif>
						</div>
					</div>
					
					<div class="question floatLeft paddingLeft" style="width:73px;">
						<div class="questionSmall questionSpacerSmall">
							M. I.:
						</div>
						<div class="floatLeft">
							<cfif arguments.podType eq "edit">
								<input type="text" class="small" name="referenceMI" id="referenceMI" value="#podValues.referenceMI#" maxlength="1" />
							<cfelse>
								<strong>#podValues.referenceMI#</strong>
							</cfif>
						</div>
					</div>
					
					<div class="question floatLeft paddingLeft">
						<div class="questionSmall questionSpacerSmall">
							<span class="required">*</span>
							Last Name:
						</div>
						<div class="floatLeft">
							<cfif arguments.podType eq "edit">
								<input type="text" class="large" name="referenceLastName" id="referenceLastName" value="#podValues.referenceLastName#" maxlength="100" />
							<cfelse>
								<cfif podValues.referenceLastName neq "">
									<strong>#podValues.referenceLastName#</strong>
								<cfelse>
									<span class="required"><strong>Not Set</strong></span>
								</cfif>
							</cfif>
						</div>
					</div>
					
					<div class="clear">&nbsp;</div>
				</div>
				
				
				<div class="questionRow">
					<div class="question floatLeft" style="width:120px;">
						<div class="questionSmall questionSpacerSmall">
							<span class="required">*</span>
							School/Organization:
						</div>
						<div class="floatLeft">
							<cfif arguments.podType eq "edit">
								<input type="text" class="large" name="referenceSchool" id="referenceSchool" value="#podValues.referenceSchool#" maxlength="100" />
							<cfelse>
								<cfif podValues.referenceSchool neq "">
									<strong>#podValues.referenceSchool#</strong>
								<cfelse>
									<span class="required"><strong>Not Set</strong></span>
								</cfif>
							</cfif>
						</div>
					</div>
					
					<div class="question floatLeft paddingLeft" style="width:85px;">
						<div class="questionSmall questionSpacerSmall">
							<span class="required">*</span>
							Title:
						</div>
						<div class="floatLeft">
							<cfif arguments.podType eq "edit">
								<cfset options = "Administrator,Principal,Sr. Vice President,Supervisor,Vice President,Other" />
								<select class="small" name="referenceTitle" id="referenceTitle" onchange="TitleCheckOther();">
									<option value="">- Select -</option>
									<cfloop list="#options#" index="option">
										<option value="#option#" <cfif podValues.referenceTitle eq option>selected="selected"</cfif>>#option#</option>
									</cfloop>
								</select>
							<cfelse>
								<cfif podValues.referenceTitle neq "">
									<strong>#podValues.referenceTitle#</strong>
								<cfelse>
									<span class="required"><strong>Not Set</strong></span>
								</cfif>
							</cfif>
							
						</div>
					</div>
					
					<div class="question floatLeft paddingLeft" style="width:110px;">
						<div class="questionSmall questionSpacerSmall" style="color:##666666; font-size:10px; font-style: italic;">
							If Other, please list:
						</div>
						<div class="floatLeft">
							<cfif arguments.podType eq "edit">
								<input type="text" class="large" name="referenceTitleOther" id="referenceTitleOther" value="#podValues.referenceTitleOther#" maxlength="100" />
							<cfelse>
								<strong>#podValues.referenceTitleOther#</strong>
							</cfif>
						</div>
					</div>
					
					<div class="question floatLeft paddingLeft" style="width:115px;">
						<div class="questionSmall questionSpacerSmall">
							<span class="required">*</span>
							Relationship to You:
						</div>
						<div class="floatLeft">
							<cfif arguments.podType eq "edit">
								<cfset options = "Administrator,Colleague,Friend,Former Principal,Former Supervisor,Principal,Supervisor,Vice President,Other" />
								
								<select class="large" name="referenceRelationship" id="referenceRelationship" style="width:105px;" onchange="RelationshipCheckOther();">
									<option value="">- Select -</option>
									<cfloop list="#options#" index="option">
										<option value="#option#" <cfif podValues.referenceRelationship eq option>selected="selected"</cfif>>#option#</option>
									</cfloop>
								</select>
							<cfelse>
								<cfif podValues.referenceRelationship neq "">
									<strong>#podValues.referenceRelationship#</strong>
								<cfelse>
									<span class="required"><strong>Not Set</strong></span>
								</cfif>
							</cfif>
						</div>
					</div>
					
					<div class="question floatLeft paddingLeft" style="width:110px;">
						<div class="questionSmall questionSpacerSmall" style="color:##666666; font-size:10px; font-style: italic;">
							If Other, please list:
						</div>
						<div class="floatLeft">
							<cfif arguments.podType eq "edit">
								<input type="text" class="large" name="referenceRelationshipOther" id="referenceRelationshipOther" value="#podValues.referenceRelationshipOther#" maxlength="100" />
							<cfelse>
								<strong>#podValues.referenceRelationshipOther#</strong>
							</cfif>
						</div>
					</div>
					
					<div class="clear">&nbsp;</div>
				</div>
				
				
				<table class="summary">
					<tr class="head">
						<td style="width:160px;">
							<span class="required">*</span>
							Telephone (w/area code):
						</td>
						<td style="width:150px;">
							ext.:
						</td>
						<td style="width:150px;">
							Type:
						</td>
					</tr>
					<cfif podPhones.RecordCount gt 0>
						<cfloop query="podPhones">
							<tr class="row">
								<td>
									<cfif podPhones.phone neq "">
										#podPhones.phone#
									<cfelse>
										<span class="required">Not Set</span>
									</cfif>
								</td>
								<td>
									#podPhones.ext#
								</td>
								<td>
									#podPhones.phoneType#
								</td>
							</tr>
						</cfloop>
					<cfelse>
						<tr>
							<td colspan="3">
								<span class="required">Not Set</span>
							</td>
						</tr>
					</cfif>
				</table>
					
				
				<table class="summary">
					<tr class="head">
						<td style="width:160px;">
							<span class="required">*</span>
							Email:
						</td>
						<td style="width:150px;">
							Type:
						</td>
					</tr>
					<cfif podEmails.RecordCount gt 0>
						<cfloop query="podEmails">
							<tr class="row">
								<td>
									<cfif podEmails.email neq "">
										#podEmails.email#
									<cfelse>
										<span class="required">Not Set</span>
									</cfif>
								</td>
								<td>
									#podEmails.emailType#
								</td>
							</tr>
						</cfloop>
					<cfelse>
						<tr class="row">
							<td colspan="2">
								<span class="required">Not Set</span>
							</td>
						</tr>
					</cfif>
				</table>
			</div>
			</cfoutput>
			
			<cfif arguments.podType eq "edit">
				<script type="text/javascript">
					function AddEmailRow ()
					{
						jQuery.post(remoteURL + "?method=AJAXCall", {componentName: "letterRecommendation", functionName: "GetEmptyEmailRow"}, function(html){
							jQuery("#emailEntries").append(html);
						});
					}
					
					function AddPhoneRow ()
					{
						jQuery.post(remoteURL + "?method=AJAXCall", {componentName: "letterRecommendation", functionName: "GetEmptyPhoneRow"}, function(html){
							jQuery("#phoneEntries").append(html);
						});
					}
					
					function RelationshipCheckOther ()
					{
						var otherField = jQuery("#referenceRelationshipOther");
						
						if (jQuery("#referenceRelationship").val().toLowerCase() == "other") {
							otherField.css("background-color", "#ffffff");
							otherField.attr("disabled", "");
						}
						else {
							otherField.val("");
							otherField.css("background-color", "#dddddd");
							otherField.attr("disabled", "true");
						}
					}
					
					function RemoveEmailRow (rowId)
					{
						jQuery("#" + rowId).remove();
					}
					
					function RemovePhoneRow (rowId)
					{
						jQuery("#" + rowId).remove();
					}
					
					function TitleCheckOther ()
					{
						var otherField = jQuery("#referenceTitleOther");
						
						if (jQuery("#referenceTitle").val().toLowerCase() == "other") {
							otherField.css("background-color", "#ffffff");
							otherField.attr("disabled", "");
						}
						else {
							otherField.val("");
							otherField.css("background-color", "#dddddd");
							otherField.attr("disabled", "true");
						}
					}
					
					jQuery().ready(function () {
						TitleCheckOther();
						RelationshipCheckOther();
					});
				</script>
			</cfif>
		</cfsavecontent>
		
		<cfreturn podHTML />
	</cffunction>
	
	
	<cffunction name="GetPodTitle" access="public" returntype="string" output="false" hint="If blank string return it will not display the Pod Title with css">
		<cfreturn "" />
	</cffunction>
	
	
	<cffunction name="GetPodValues" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT referencePrefix, referenceFirstName, referenceMI, referenceLastName, referenceSchool, referenceTitle, referenceTitleOther, referenceRelationship, referenceRelationshipOther
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="IsPODComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var retVal = true />
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SavePod" access="public" returntype="void" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="aform" type="struct" required="true" />
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	<cffunction name="GetPodEmails" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT email, emailType
			FROM STEMAppsRecommendationEmails (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY sortOrder
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
	<cffunction name="GetPodPhones" access="public" returntype="query" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var sectionValues = "" />
		
		<cfquery name="sectionValues" datasource="#variables.settings.dsn#">
			SELECT phone, ext, phoneType
			FROM STEMAppsRecommendationPhones (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY sortOrder
		</cfquery>
		
		<cfreturn sectionValues />
	</cffunction>
	
	
</cfcomponent>