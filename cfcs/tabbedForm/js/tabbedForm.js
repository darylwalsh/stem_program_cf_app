
var tabsLocked = false;
var tabOpen = -1;
var remoteURL = "/tabbedForm/tabbedForm.cfc";  // This gets over written in tabbedFormEXTENDS to what ever path it actually is.


function DisplayTab ()
{
	jQuery("#formContentLoader").css("display", "none");
	jQuery("#formContent").css("display", "");
	
	tabsLocked = false;
}


function ExitApplication ()
{
	if (tabOpen != -1) {
		jQuery("#formContent").css("display", "none");
		jQuery("#formContentLoader").css("display", "");
		
		var formData = {};
		var formValues = jQuery("#tabForm").serializeArray();
		
		formData["applicationGUID"] = jQuery("#applicationGUID").val();
		formData["tabIndex"] = tabOpen;
		for (var i = 0; i < formValues.length; i++) {
			formData[String(formValues[i].name)] = String(formValues[i].value);
		}
		
		var now = new Date();
		var ticks = now.getTime();
		
		jQuery.post(remoteURL + "?method=SaveTab&ticks=" + ticks, formData, function(html){
			location.href = "/";
		});
	}
	else {
		location.href = "/";
	}
}


function OpenCalendar (valueId)
{
	jQuery("div[class~='calendar']").css("visibility", "hidden");
	
	var valueObj = jQuery("#" + valueId);
	var top = valueObj.position().top + 28;
	var left = valueObj.position().left + 10;
	
	jQuery("#jQueryCalendar").css("top", top);
	jQuery("#jQueryCalendar").css("left", left);
	
	// well this calendar blows - no buttons so change year - wtf
	jQuery("#jQueryCalendar").datepicker({
		onSelect: function(dateText, inst) {
			jQuery("#" + valueId).val(dateText);
			jQuery("div[class~='calendar']").css("visibility", "");
			jQuery("#jQueryCalendar").datepicker("destroy");
		}
	});
	
	jQuery("#jQueryCalendar").datepicker( "option", "changeYear", true );
	jQuery("#jQueryCalendar").datepicker( "option", "changeMonth", true );
	
	var minDate = new Date();
	minDate.setFullYear(1940,0,1);
	
	var maxDate = new Date();
	maxDate.setFullYear(2020,0,1);
	
	jQuery("#jQueryCalendar").datepicker( "option", "minDate", minDate);
	jQuery("#jQueryCalendar").datepicker( "option", "maxDate", maxDate);
}


function OpenLightBox (aLocation)
{
	jQuery.fancybox({
		'href' : aLocation,
		'width' : 600,
		'height' : 400,
		'autoScale' : false,
		'transitionIn' : 'none',
		'transitionOut' : 'none',
		'type' : 'iframe'
	});
}


function OpenPopup (aLocation)
{
	var newwindow = window.open(aLocation,'name','scrollbars,resizable,height=600,width=800');
	if (window.focus) {
		newwindow.focus()
	}
}


function OpenSummary ()
{
	OpenTab(-1);
}


function OpenTab (tabIndex, pageInitialOpen)
{
	var proceed = false;
	
	if(jQuery("#formContent").css("display") == "none") {
		proceed = true;
	}
	else {
		// run a check to see if anything has changed - if the content is open
		proceed = true; // for now set to true
	}
	
	if (proceed) {
		if (!tabsLocked) {
			tabsLocked = true;
			
			if (typeof(pageInitialOpen) == "undefined") {  // will be undefined or boolean
				window.scrollTo(0,300);
			}
			
			jQuery("#formContent").css("display", "none");
			jQuery("#formContentLoader").css("display", "");
			OpenTabNavImage(tabIndex);
			
			tabOpen = tabIndex;
			
			var now = new Date();
			var ticks = now.getTime();
			
			if (tabIndex == -1) {
				jQuery.post(remoteURL + "?method=BuildSummary&ticks=" + ticks, {}, function(html){
					jQuery("#formContent").html(html);
					window.setTimeout("DisplayTab()", 400);
				});
			}
			else {
				var formData = {
					tabIndex: tabIndex
				};
			
				jQuery.post(remoteURL + "?method=BuildTab&ticks=" + ticks, formData, function(html){
					jQuery("#formContent").html(html);
					window.setTimeout("DisplayTab()", 400);
				});
			}
		}
	}
}


function OpenTabNavImage (tabIndex)
{
	if (tabOpen != -1) {
		jQuery("#tab" + tabOpen).removeClass("tabOn");
		jQuery("#tab" + tabOpen).addClass("tabOff");
		jQuery("#tab" + tabOpen).live("click", function () {
			SaveAndContinue("tabClick", jQuery(this).attr("currentIndex"));
		});
	}
	
	if (tabIndex != -1) {
		jQuery("#tab" + tabIndex).removeClass("tabOff");
		jQuery("#tab" + tabIndex).addClass("tabOn");
		jQuery("#tab" + tabIndex).die("click");
	}
	
}


function PopulateHiddenVersion (idPrefix)
{
	var multipleSelect = document.getElementById(idPrefix + "-ms");
	var retVal = new Array();
	
	for (var i = 0; i < multipleSelect.options.length; i++) {
		if (multipleSelect.options[i].selected) {
			retVal.push(multipleSelect.options[i].value);
		}
	}
	jQuery("#" + idPrefix).val(retVal.join(","));
}

function PopulateHiddenVersionCBVersion (namePrefix)
{
	var cb = jQuery("input[name|=" + namePrefix + "]").filter("input[name!=" + namePrefix + "]");
	var retVal = new Array();
	
	for (var i = 0; i < cb.length; i++){
		if (cb[i].checked){
			retVal.push(cb[i].value);
		}
	}
	jQuery("#" + namePrefix).val(retVal.join(","));
}


function SaveAndContinue (clickType, tabIndex)
{
	if (!tabsLocked) {
		tabsLocked = true;
		jQuery("#formContent").css("display", "none");
		jQuery("#formContentLoader").css("display", "");
		
		OpenTabNavImage(tabIndex);
		
		if (tabOpen == -1) {
			if (clickType == "tabClick") {
				tabsLocked = false;
				OpenTab(tabIndex);
			}
		}
		else {
			var formData = {};
			var formValues = jQuery("#tabForm").serializeArray();
			
			//var fVals = "";
			
			formData["applicationGUID"] = jQuery("#applicationGUID").val();
			formData["tabIndex"] = tabOpen;
			for (var i = 0; i < formValues.length; i++) {
				formData[String(formValues[i].name)] = String(formValues[i].value);
			
				//fVals += String(formValues[i].name) + "=" + String(formValues[i].value) + " \n";
			}
			//alert(fVals);
			
			
			/*$("#tabForm").append(jQuery("#applicationGUID")).append("<input type='hidden' name='tabIndex' value='"+tabOpen+"' />");*/
			
			var now = new Date();
			var ticks = now.getTime();
			
			jQuery.post(remoteURL + "?method=SaveTab&ticks=" + ticks, /*jQuery("#tabForm").serialize()*/formData, function(html){
				html = Trim(html).toLowerCase();
				if (html == "success") {
					if (clickType == "tabClick") {
						tabsLocked = false;
						OpenTab(tabIndex);
					}
					else {
						if (clickType == "submitClick") {
							if (tabIndex <= 4) { // 4 reference will eventually be removed?
								tabsLocked = false;
								OpenTab(tabIndex + 1);
							}
							else {
								tabsLocked = false;
								OpenSummary();
							}
						}
					}
				}
				else if (html == "timeout") {
					alert("Your session has timed out.  Any information that was changed recenlty was not saved.  Please log back in to continue.");
					window.setTimeout("TimedOutApp()", 700);
				}
				else 
					if (html == "failed") {
						alert("Oops! An error has occured.");
						tabsLocked = false;
						OpenTab(tabIndex);
						/*OpenTab(tabOpen);*/
					}
					else {
						console.log(html);
					}
			});
		}
	}
}


function TimedOutApp ()
{
	location.href = "/";
}

