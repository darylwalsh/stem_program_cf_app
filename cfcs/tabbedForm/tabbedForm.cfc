<cfcomponent displayname="tabbedForm.cfc" extends="tabbedFormEXTENDS" implements="tabbedFormINTERFACE" output="false">
	
	
	<cffunction name="GetApplicationGUID" access="public" returntype="string" output="false">
		<cfargument name="userGUID" type="string" required="true" />
		
		<cfset var getApp = "" />
		
		<cfquery name="getApp" datasource="#GetDSN()#">
			SELECT applicationGUID FROM STEMApplications (nolock)
			WHERE 	registrationGUID = <cfqueryparam value="#arguments.userGUID#" cfsqltype="cf_sql_varchar" /> AND
					created > <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_timestamp" />
			ORDER BY created DESC
		</cfquery>
		
		<cfreturn getApp.applicationGUID />
	</cffunction>
	
	
	<cffunction name="GetDSN" access="public" returntype="String" output="false" hint="Used by remote functions.  Still needed?">
		<cfreturn "Sponsorships" />
	</cffunction>
	
	
	<cffunction name="GetFormSettings" access="public" returntype="struct" output="false">
		
		<cfset var aform = {} />
		<cfset var navLink = {} />
		<cfset var tab = {} />
		
		
		<!-------------------------->
		<!---  Setup Form        --->
		<!-------------------------->
		
		<cfset aform.title = "Siemens Summer of Learning Application" />
		<cfset aform.titleSub = 
			"A Siemens Foundation Initiative<br /><div style='color:##cc4444; font-size:12px; margin-top:5px;'>Applications due #dateformat(request.contestEnd)#</div>" />
		<cfset aform.formTitle = "#request.contestYear# STEM Academy Application Process" />
		<cfset aform.startDate = "#request.contestStart#" />
		<cfset aform.endDate = "#request.contestEnd#" />
		<cfset aform.emailFormFrom = "stem@redacted.com" />
		<cfset aform.emailAdministrator = "daryl_walsh@redacted.com" />
		<cfset aform.cfcsRoot = "cfcs.tabbedForm" />
		
		
		<!-------------------------->
		<!---  Setup Nav Links   --->
		<!-------------------------->
		
		<cfset aform.navLinks = [] />  <!--- there will be 3 types available: link, popup, script --->
		
		<!--- for now keep these 2 for reference --->
		<!---
		<cfset navLink = {} />
		<cfset navLink.title = "Application Process" />
		<cfset navLink.type = "link" />
		<cfset navLink.href = "http://www.yahoo.com" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		<cfset navLink = {} />
		<cfset navLink.title = "Eligibility Criteria" />
		<cfset navLink.type = "popup" />
		<cfset navLink.href = "/index.cfm?event=showEC" />
		<cfset navLink.width = 500 />
		<cfset navLink.height = 400 />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		<cfset navLink = {} />
		<cfset navLink.title = "Application Process" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenTab(1);" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		--->
		
		
		
		
		
		<!---
		<cfset navLink = {} />
		<cfset navLink.title = "Eligibility Criteria" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenLightBox('/STEMApplication/lightboxes/eligibilityCriteria.html');" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		--->
		
		
		<!---
		<cfset navLink = {} />
		<cfset navLink.title = "Judging Guidelines" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenLightBox('/STEMApplication/lightboxes/judgingGuidelines.html');" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		--->
		
		
		
		
		
		<cfset navLink = {} />
		<cfset navLink.title = "Institute Overview" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenLightBox('/STEMApplication/lightboxes/instituteOverview.html');" />
		<cfset navLink.id = "instituteNav1" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		
		<cfset navLink = {} />
		<cfset navLink.title = "Institute Eligibility Criteria" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenLightBox('/STEMApplication/lightboxes/instituteEligibilityCriteria.html');" />
		<cfset navLink.id = "instituteNav2" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		<cfset navLink = {} />
		<cfset navLink.title = "Institute Tentative Itinerary" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenLightBox('/STEMApplication/lightboxes/instituteTentativeItinerary.html');" />
		<cfset navLink.id = "instituteNav3" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		
		<cfset navLink = {} />
		<cfset navLink.title = "STARs Overview" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenLightBox('/STEMApplication/lightboxes/STARsOverview.html');" />
		<cfset navLink.id = "STARsNav1" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		
		<cfset navLink = {} />
		<cfset navLink.title = "STARs Eligibility Criteria" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenLightBox('/STEMApplication/lightboxes/STARsEligibilityCriteria.html');" />
		<cfset navLink.id = "STARsNav2" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		<cfset navLink = {} />
		<cfset navLink.title = "STARs Tentative Itinerary" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenLightBox('/STEMApplication/lightboxes/STARsTentativeItinerary.html');" />
		<cfset navLink.id = "STARsNav3" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		<cfset navLink = {} />
		<cfset navLink.title = "Video Tips" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "OpenLightBox('/STEMApplication/lightboxes/viewTips.html');" />
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		
		<cfset navLink = {} />
		<cfset navLink.title = "View Summary" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "SaveAndContinue('tabClick', -1);" />
		
		<cfset ArrayAppend(aform.navLinks, navLink) />
		
		<!--- <cfset navLink = {} />
		<cfset navLink.title = "View the 2013 Itinerary for STARs: Pacific Northwest" />
		<cfset navLink.type = "script" />
		<cfset navLink.callFunction = "window.open('/STEMApplication/2013_PNNL_Siemens_Teachers_as_Researchers_Schedule.pdf', '_STEM_file');" /> --->
		
		
		<!-------------------------->
		<!---  Setup Tabs        --->
		<!-------------------------->
		
		<cfset aform.tabs = [] />
<!--- 
Citizenship Status, 
Teaching Experience, 
Previous Teaching Position(s)/Experience, 
Post Secondary Educational Background, 
Memberships, 
Extracurricular Activities, 
Optional Demographic Information
 --->		
		<cfset tab = {} />
		<cfset tab.title = "EDUCATION AND EXPERIENCE" />
		<cfset tab.pods = [] />
		<cfset ArrayAppend(tab.pods, "citizenship") />
		
		<cfset ArrayAppend(tab.pods, "currentPosition") />
		<cfset ArrayAppend(tab.pods, "pastPositions") />
		<cfset ArrayAppend(tab.pods, "education") />
		<cfset ArrayAppend(tab.pods, "responsibilities") />
		<cfset ArrayAppend(tab.pods, "activities") />
		<cfset ArrayAppend(tab.pods, "demographics") />
		<cfset ArrayAppend(aform.tabs, tab) />
		
		<cfset tab = {} />
		<cfset tab.title = "ACHIEVEMENTS" />
		<cfset tab.pods = [] />
		<!---<cfset ArrayAppend(tab.pods, "professionalDevelopment") />--->
		<cfset ArrayAppend(tab.pods, "honorsAwards") />
		<!---<cfset ArrayAppend(tab.pods, "principalSupport") />--->
		<cfset ArrayAppend(aform.tabs, tab) />
		
		<!---
		<cfset tab = {} />
		<cfset tab.title = "TECHNICAL SKILLS" />
		<cfset tab.pods = [] />
		<cfset ArrayAppend(tab.pods, "technologyKnowledgeUse") />
		<cfset ArrayAppend(aform.tabs, tab) />
		--->
		
		<cfset tab = {} />
		<cfset tab.title = "RECOMMENDATION" />
		<cfset tab.pods = [] />
		<cfset ArrayAppend(tab.pods, "letterRecommendation") />
		<cfset ArrayAppend(aform.tabs, tab) />
		
		
		
		<cfset tab = {} />
		<!---<cfset tab.description = "Please be as thorough as possible in your responses." />--->
		<cfset tab.title = "ESSAYS" />
		<cfset tab.pods = [] />
		<cfset ArrayAppend(tab.pods, "essaysSTARs") />
		<cfset ArrayAppend(tab.pods, "essaysInstitute") />
		<cfset ArrayAppend(tab.pods, "essaysAdditional") />
		<cfset ArrayAppend(aform.tabs, tab) />
		
		<cfset tab = {} />
		<cfset tab.title = "VIDEO" />
		<cfset tab.pods = [] />
		<cfset ArrayAppend(tab.pods, "uploadVideo") />
		<cfset ArrayAppend(aform.tabs, tab) />
		
		
		<cfreturn aform />
	</cffunction>
	
	
	<cffunction name="GetFirstIncompleteTabIndex" access="public" returntype="numeric" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var getCompletedTabDates = "" />
		<cfset var tabIndex = -1 />
		
		
		<cfquery name="getCompletedTabDates" datasource="#GetDSN()#">
			SELECT tab1Completed, tab2Completed, tab3Completed, tab4Completed, tab5Completed
			FROM STEMApplications (nolock)
			WHERE 	applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" /> AND
					registrationGUID = <cfqueryparam value="#client.STEMRegistrationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfif NOT IsDate(getCompletedTabDates.tab1Completed)>
			<cfset tabIndex = 1 />
		<cfelseif NOT IsDate(getCompletedTabDates.tab2Completed)>
			<cfset tabIndex = 2 />
		<cfelseif NOT IsDate(getCompletedTabDates.tab3Completed)>
			<cfset tabIndex = 3 />
		<cfelseif NOT IsDate(getCompletedTabDates.tab4Completed)>
			<cfset tabIndex = 4 />
		<cfelseif NOT IsDate(getCompletedTabDates.tab5Completed)>
			<cfset tabIndex = 5 />
		</cfif>
		
		<cfreturn tabIndex />
	</cffunction>
	
	
	<cffunction name="GetPreFormHTML" access="public" returntype="string" output="false" hint="Can return blank string.  For now this is a workaround for STEM.  Need to think of a better way">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var getType = "" />
		<cfset var html = "" />
		<cfset var programType = "" />
		
		
		<cfquery name="getType" datasource="#variables.settings.dsn#">
			SELECT programInstitute, programSTARsOR, programSTARsPN 
			FROM STEMApplications (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfsavecontent variable="html">
			<div style="font-size:1.1em; font-weight:bold; padding:16px 0px 16px 17px;"> 
				
				<div style="float:left; width:500px;">
					Applying for: 
					<strong style="color:#2A6262;">
						<cfif getType.programInstitute eq 1 AND (getType.programSTARsOR eq 1 OR getType.programSTARsPN eq 1)>
							<cfset programType = "both" />
							INSTITUTE & STARs
						<cfelseif getType.programInstitute eq 1>
							<cfset programType = "institute" />
							INSTITUTE
						<cfelseif getType.programSTARsOR eq 1 OR getType.programSTARsPN eq 1>
							<cfset programType = "stars" />
							STARs
						</cfif>
					</strong>
					
					<cfif NOT IsApplicationSubmitted(arguments.applicationGUID)>
						<a id="changeSelectionLink" href="/index.cfm?event=showSTEMApplication&landing=1" style="font-size:0.75em; margin-left:7px;">change program selection</a>
					</cfif>
				</div>
				
				<div style="float:left; font-size:0.8em; font-style:italic; width:420px; text-align:right;">
					Fields with an asterisk(<em style="color:red;">*</em>) are required
				</div>
				
				<div class="clear">&nbsp;</div>
			</div>
			
			<cfif programType eq "institute" OR programType eq "stars">
				<script type="text/javascript">
					jQuery().ready(function () {
						<cfif programType eq "institute">
							jQuery("#STARsNav1").css("display", "none");
							jQuery("#STARsNav2").css("display", "none");
							jQuery("#STARsNav3").css("display", "none");
						<cfelseif programType eq "stars">
							jQuery("#instituteNav1").css("display", "none");
							jQuery("#instituteNav2").css("display", "none");
							jQuery("#instituteNav3").css("display", "none");
							
							jQuery("#STARsNav1").removeClass("navLinkMiddle");
							jQuery("#STARsNav1").addClass("navLinkFirst");
						</cfif>
						
						
						jQuery("#navMiddleLeft").removeClass("navMiddleLeft8");
						jQuery("#navMiddleLeft").addClass("navMiddleLeft5");
					});
				</script>
			</cfif>
			
			
		</cfsavecontent>
		
		
		<cfreturn html />
	</cffunction>
	
	
	<cffunction name="GetScriptsToLoadToHead"  access="public" returntype="string" output="false" hint="This function will run even if you don't need to load anything (return '' then).">
		
		<cfset var scriptsToHead = "" />
		
		<!--- 
			The purpose of this function is to allow you to load or not load jquery scripts or other 
			customized scripts.  Sometimes jQuery might already be loaded.
		--->
		
		<cfoutput>
		<cfsavecontent variable="scriptsToHead">
			<script type="text/javascript" src="#GetCFCBaseURL()#/tabbedForm/js/jquery-1.4.4.min.js"></script>
			
			<script type="text/javascript" src="/js/datepicker/js/datepicker.packed.js"></script>
			<link href="/js/datepicker/css/datepicker.css" rel="stylesheet" />
		</cfsavecontent>
		</cfoutput>
		
		<cfreturn scriptsToHead />
	</cffunction>
	
	
	<cffunction name="IsApplicationSubmitted" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var getStatus = "" />
		<cfset var retVal = false />
		
		
		<cfquery name="getStatus" datasource="#GetDSN()#">
			SELECT allTabsCompleted FROM STEMApplications
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfif IsDate(getStatus.allTabsCompleted)>
			<cfset retVal = true />
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="IsTabComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="tabIndex" type="numeric" required="true" />
		
		<cfset var getStatus = "" />
		<cfset var retVal = false />
		
		<cfquery name="getStatus" datasource="#GetDSN()#">
			SELECT tab1Completed, tab2Completed, tab3Completed, tab4Completed, tab5Completed, tab6Completed
			FROM STEMApplications (nolock)
			WHERE 	applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" /> AND
					registrationGUID = <cfqueryparam value="#client.STEMRegistrationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
		<cfif arguments.tabIndex eq 1 AND IsDate(getStatus.tab1Completed)>
			<cfset retVal = true />
		<cfelseif arguments.tabIndex eq 2 AND IsDate(getStatus.tab2Completed)>
			<cfset retVal = true />
		<cfelseif arguments.tabIndex eq 3 AND IsDate(getStatus.tab3Completed)>
			<cfset retVal = true />
		<cfelseif arguments.tabIndex eq 4 AND IsDate(getStatus.tab4Completed)>
			<cfset retVal = true />
		<cfelseif arguments.tabIndex eq 5 AND IsDate(getStatus.tab5Completed)>
			<cfset retVal = true />
		<cfelseif arguments.tabIndex eq 6 AND IsDate(getStatus.tab6Completed)>
			<cfset retVal = true />
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SaveTabCompleted" access="public" returntype="void" output="false" hint="Mark this in the database as completed">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="tabIndex" type="string" required="true" />
		<cfargument name="isComplete" type="boolean" required="true" />
		
		<cfset var save = "" />
		
		<cfquery name="save" datasource="#GetDSN()#">
			UPDATE STEMApplications
			SET		<cfif arguments.tabIndex eq 1>
						tab1LastUpdated = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
						<cfif arguments.isComplete>
							tab1Completed = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />
						<cfelse>
							tab1Completed = <cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />
						</cfif>
					<cfelseif arguments.tabIndex eq 2>
						tab2LastUpdated = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
						<cfif arguments.isComplete>
							tab2Completed = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />
						<cfelse>
							tab2Completed = <cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />
						</cfif>
					<cfelseif arguments.tabIndex eq 3>
						tab3LastUpdated = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
						<cfif arguments.isComplete>
							tab3Completed = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />
						<cfelse>
							tab3Completed = <cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />
						</cfif>
					<cfelseif arguments.tabIndex eq 4>
						tab4LastUpdated = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
						<cfif arguments.isComplete>
							tab4Completed = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />
						<cfelse>
							tab4Completed = <cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />
						</cfif>
					<cfelseif arguments.tabIndex eq 5>
						tab5LastUpdated = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
						<cfif arguments.isComplete>
							tab5Completed = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />
						<cfelse>
							tab5Completed = <cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />
						</cfif>
					<cfelseif arguments.tabIndex eq 6>
						tab6LastUpdated = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
						<cfif arguments.isComplete>
							tab6Completed = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />
						<cfelse>
							tab6Completed = <cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />
						</cfif>
					</cfif>
			WHERE 	applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" /> AND
					registrationGUID = <cfqueryparam value="#client.STEMRegistrationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>
		
	</cffunction>
	
	
	<cffunction name="SetApplicationComplete" access="remote" returntype="void" output="true" hint="either returns 'success' or 'failed'">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="confirmRecommendation" type="boolean" required="true" />
		<cfargument name="confirmTermsConditions" type="boolean" required="true" />
		<cfargument name="confirmNoPlagiarize" type="boolean" required="true" />
		
		
		<cfset var retVal = "failed" />
		<cfset var save = "" />
		
		
		<cfif StructKeyExists(arguments, "applicationGUID") AND IsValid("guid", arguments.applicationGUID) AND application.cfcs.register.isSTEMUserLoggedIn()>
			<cfquery name="save" datasource="#GetDSN()#">
				UPDATE STEMApplications 
				SET 	allTabsCompleted = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
						confirmRecommendation = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						confirmTermsConditions = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						confirmNoPlagiarize = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />
				WHERE 	registrationGUID = <cfqueryparam value="#client.STEMRegistrationGUID#" cfsqltype="cf_sql_varchar" /> AND
						applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			</cfquery>
			<cfset retVal = "success" />
		</cfif>
		
		<cfsetting enablecfoutputonly="true" />
		<cfcontent reset="true" />
		<cfoutput>#retVal#</cfoutput>
		
	</cffunction>
	
	
	
	<!------------------------------------------>
	<!--- Custom Functions                   --->
	<!------------------------------------------>
	
	<cffunction name="SaveLetterOfRecommendation" access="remote" returntype="void" output="true">
		
		<cfset var retVal = "failed" />
		<cfset var save = "" />
		
		<cfif StructKeyExists(arguments, "applicationGUID") AND IsValid("guid", arguments.applicationGUID) AND StructKeyExists(arguments, "fileName") AND arguments.fileName neq "">
			<cfquery name="save" datasource="#GetDSN()#">
				INSERT INTO STEMAppsRecommendationFiles (applicationGUID, fileName)
				VALUES ( 	<cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />,
							<cfqueryparam value="#arguments.fileName#" cfsqltype="cf_sql_varchar" />
						)
			</cfquery>
			<cfset retVal = "success" />
		</cfif>
		
		<cfsetting enablecfoutputonly="true" />
		<cfcontent reset="true" />
		<cfoutput>#retVal#</cfoutput>
	</cffunction>
	
	
	
</cfcomponent>