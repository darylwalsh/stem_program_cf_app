<cfinterface displayName="tabbedFormINTERFACE.cfc">
	
	<cffunction name="GetApplicationGUID" access="public" returntype="string" output="false">
		<cfargument name="userGUID" type="string" required="true" />
	</cffunction>
	
	
	<cffunction name="GetDSN" access="public" returntype="String" output="false" hint="Used by remote functions.  Still needed?">
		
	</cffunction>
	
	
	<cffunction name="GetFirstIncompleteTabIndex" access="public" returntype="numeric" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
	</cffunction>
	
	
	<cffunction name="GetFormSettings" access="public" returntype="struct" output="false">
		
	</cffunction>
	
	
	<cffunction name="GetPreFormHTML" access="public" returntype="string" output="false" hint="Can return blank string.">
		<cfargument name="applicationGUID" type="string" required="true" />
	</cffunction>
	
	
	<cffunction name="GetScriptsToLoadToHead"  access="public" returntype="string" output="false" hint="This function will run even if you don't need to load anything (return '' then).">
		
	</cffunction>
	
	
	<cffunction name="IsApplicationSubmitted" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
	</cffunction>
	
	
	<cffunction name="IsTabComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="tabIndex" type="numeric" required="true" />
		
	</cffunction>
	
	
	<cffunction name="SaveTabCompleted" access="public" returntype="void" output="false" hint="Mark this in the database as completed">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="tabIndex" type="string" required="true" />
		<cfargument name="isComplete" type="boolean" required="true" />
		
	</cffunction>
	
	
</cfinterface>