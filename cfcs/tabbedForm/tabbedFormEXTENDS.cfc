

<cfcomponent displayname="tabbedFormEXTENDS.cfc" output="false">
	
	<!----------------------------->
	<!--- Initialize variables  --->
	<!----------------------------->
	
	<cfset variables = {} />
	<cfset variables.app = {} />
	<cfset variables.sectionComponents = {} />
	<cfset variables.settings = {} />
	
	
	
	<!----------------------------->
	<!--- Setup Functions       --->
	<!----------------------------->
	
	<cffunction name="Init" access="public" returntype="any" output="true">
		
		<!---
		<cfset var cfcBaseDotPath = "" />
		<cfset var cfcBaseURL = "" />
		--->
		<cfset var curDir = "" />
		<cfset var currentCFDir = Replace(GetDirectoryFromPath(GetCurrentTemplatePath()), "\", "/", "all") />
		<cfset var currentCGIDir = Replace(GetDirectoryFromPath(cgi.cf_template_path), "\", "/", "all") />
		<cfset var dirMatch = false />
		
		
		<cfset variables.settings.dsn = GetDSN() />
		<cfset variables.formSettings = GetFormSettings() />
		<cfset variables.sectionComponents = SetupSectionComponents(variables.formSettings) />
		
		<!---
		<!--- lets calculate the cfcs url --->
		<cfloop condition="dirMatch eq false">
			<cfif currentCGIDir eq "" OR Left(currentCFDir, Len(currentCGIDir)) eq currentCGIDir>
				<cfset dirMatch = true />
			<cfelse>
				<cfset currentCGIDir = ListDeleteAt(currentCGIDir, ListLen(currentCGIDir, "/")) />
			</cfif>
		</cfloop>
		
		<cfset currentCFDir = Right(currentCFDir, Len(currentCFDir) - Len(currentCGIDir)) />
		
		<cfloop list="#currentCFDir#" delimiters="/" index="curDir">
			<cfif curDir eq "tabbedForm">
				<cfbreak />
			<cfelse>
				<cfset cfcBaseDotPath = cfcBaseDotPath & curDir & "." />
				<cfset cfcBaseURL = cfcBaseURL & "/" & curDir  />
			</cfif>
		</cfloop>
		
		<cfset variables.settings.cfcBaseDotPath = cfcBaseDotPath />
		<cfset variables.settings.cfcBaseURL = cfcBaseURL />
		--->
		
		
		<!--- will move this to a setting that the user will set in tabbedForm.cfc.  Maybe we will move this ... --->
		<cfset variables.settings.cfcBaseDotPath = "cfcs." />
		<cfset variables.settings.cfcBaseURL = "/cfcs" />
		
		
		<cfreturn this />
	</cffunction>
	
	
	<cffunction name="SetupSectionComponents" access="public" returntype="struct" output="true">
		<cfargument name="formSettings" type="Struct" required="true" />
		
		<cfset var comps = StructNew() />
		<cfset var i = 0 />
		<cfset var j = 0 />
		<cfset var md = StructNew() />
		<cfset var podName = "" />
		
		<cfloop from="1" to="#ArrayLen(arguments.formSettings.tabs)#" index="i">
			
			<cfif StructKeyExists(arguments.formSettings.tabs[i], "pods")>
				<cfloop from="1" to="#ArrayLen(arguments.formSettings.tabs[i].pods)#" index="j">
					<cfset podName = arguments.formSettings.tabs[i].pods[j] />
					<cfset comps[podName] = CreateObject("component", arguments.formSettings.cfcsRoot & ".pods." & podName).Init(variables.settings) />
					
					<!--- Verify that it has been extended and the interface has been implemented. --->
					<cfset md = GetMetaData(comps[podName]) />
					
					<cfif not StructKeyExists(md, "extends") OR not StructKeyExists(md.extends, "displayName") OR md.extends.displayName neq "podEXTENDS.cfc">
						<cfthrow type="#arguments.formSettings.cfcsRoot#.exception" message="All pods must extend the podINTERFACE." detail="#podName#.cfc doesn't extend the podINTERFACE." />
					<cfelseif not StructKeyExists(md, "implements") OR not StructKeyExists(md.implements, "podINTERFACE")>
						<cfthrow type="#arguments.formSettings.cfcsRoot#.exception" message="All pods must implement the podINTERFACE." detail="#podName#.cfc doesn't implement the podINTERFACE." />
					</cfif>
				</cfloop>
			</cfif>
		</cfloop>
		
		
		<cfreturn comps />
	</cffunction>
	
	
	
	<!----------------------------->
	<!--- Public Functions      --->
	<!----------------------------->
	
	<cffunction name="BuildButton" access="public" returntype="string" output="false">
		<cfargument name="displayText" type="string" required="true" />
		<cfargument name="onClickJSRun" type="string" required="true" hint="run this javascript when clicked.  Pass in a function to call." />
		<cfargument name="buttonID" type="string" required="false" default="" />
		
		<cfset var buttonHTML = "" />
		
		<cfset arguments.onClickJSRun = Replace(arguments.onClickJSRun, """", "'", "all") />
		
		<cfoutput>
		<cfsavecontent variable="buttonHTML">
			<div class="button" <cfif arguments.buttonID neq "">id="#arguments.buttonID#"</cfif> onmouseover="this.className='button buttonHover';" onmouseout="this.className='button';" onclick="#arguments.onClickJSRun#">
				<div class="left">
					&nbsp;
				</div>
				<div class="middle">
					#arguments.displayText#
				</div>
				<div class="right">
					&nbsp;
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div class="clear">&nbsp;</div>
		</cfsavecontent>
		</cfoutput>
		
		<cfreturn buttonHTML />
	</cffunction>
	
	
	<cffunction name="BuildForm" access="public" returntype="string" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		
		<cfset var classNames = "" />
		<cfset var formHTML = "" />
		<cfset var i = 0 />
		<cfset var id = "" />
		<cfset var isAppCompleted = IsApplicationSubmitted(arguments.applicationGUID) />
		<cfset var onclickScript = "" />
		<cfset var openTabIndex = 1 />
		<cfset var toHeadTag = "" />
		
		
		<cfoutput>
		<cfsavecontent variable="toHeadTag">
			<link rel="stylesheet" style="text/css" href="#GetCFCBaseURL()#/tabbedForm/css/tabbedForm.css" media="all" />
			<!--- This is an IE workaround, wouldn't need to do this if our header was STRICT XHTML.
					IE CSS expression doesn't seem to work, so CF calculates if it is IE.  --->
			<cfif FindNoCase("MSIE", cgi.http_user_agent) gt 0>
				<style type="text/css" media="all"> 
					div.app div.clear {
						margin:-1px 0px 0px 0px !important;
					}
					
					table.survey tr.alt td {
						background-color: ##f2f8ff;
					}
				</style>
			</cfif>
			#GetScriptsToLoadToHead()#
			
			<link type="text/css" href="#GetCFCBaseURL()#/tabbedForm/css/redmond/jquery-ui-1.8.6.custom.css" rel="stylesheet" />
			<link type="text/css" href="#GetCFCBaseURL()#/tabbedForm/js/fancybox/jquery.fancybox-1.3.1.css" rel="stylesheet" />
			
			<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
			
			<script type="text/javascript" src="#GetCFCBaseURL()#/tabbedForm/js/jquery-1.4.3.min.js"></script>
			<script type="text/javascript" src="#GetCFCBaseURL()#/tabbedForm/js/jquery-ui-1.8.6.custom.min.js"></script>
			<script type="text/javascript" src="#GetCFCBaseURL()#/tabbedForm/js/fancybox/jquery.fancybox-1.3.1.js"></script>
			
			<script type="text/javascript" src="#GetCFCBaseURL()#/tabbedForm/js/ckeditor/ckeditor.js"></script>
			<script type="text/javascript" src="#GetCFCBaseURL()#/tabbedForm/js/ckeditor/adapters/jquery.js"></script> <!--ckeditor jquery plugin-->
			
			<script type="text/javascript" src="#GetCFCBaseURL()#/tabbedForm/js/simpleValidation.js"></script>
			
			<script type="text/javascript" src="#GetCFCBaseURL()#/tabbedForm/js/tabbedForm.js"></script>
			<script type="text/javascript">
				// first make sure jQuery doesn't conflict with any other existing APIs.
				jQuery.noConflict();  // now any jQuery scripts must replace '$' with 'jQuery'
				
				var remoteURL = "#GetCFCBaseURL()#/tabbedForm/tabbedForm.cfc";
				jQuery().ready(function () {
					<cfif isAppCompleted>
						OpenTab(-1, true);
					<cfelse>
						<cfloop from="1" to="#ArrayLen(variables.formSettings.tabs)#" index="i">
							jQuery("##tab#i#").live("click", function () {
								// Save the current tab
								SaveAndContinue("tabClick", jQuery(this).attr("currentIndex"));
							});
						</cfloop>
						
						OpenTab(#GetFirstIncompleteTabIndex(arguments.applicationGUID)#, true);
					</cfif>
				});
			</script>
		</cfsavecontent>
		</cfoutput>
		<cfhtmlhead text="#toHeadTag#" />
		
		
		<cfoutput>
		<cfsavecontent variable="formHTML">
			<div class="app">
           		<h2 class="title">
					#variables.formSettings.title#
				</h2>
				<div class="titleSub">
					#variables.formSettings.titleSub#
				</div>
				
				<div class="formWrap">
					<div class="formWrapTopLeft">&nbsp;</div>
					<div class="formWrapTop">
						<img src="#GetCFCBaseURL()#/tabbedForm/images/formWrapTop.png" />
						<div class="formWrapTopText">#variables.formSettings.formTitle#</div>
					</div>
					<div class="formWrapTopRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>
					
					<div class="formWrapMiddle">
						<div style="width:100%;">
							#GetPreFormHTML(arguments.applicationGUID)#
							
							<div class="nav">
								<div class="navLeft">
									<div class="navTopLeft">
										<div class="arrows">
											<cfloop from="1" to="#ArrayLen(variables.formSettings.navLinks)#" index="i">
												<div class="arrow">
													<img src="#GetCFCBaseURL()#/tabbedForm/images/navArrow.png" />
												</div>
											</cfloop>
										</div>
									</div>
									<div id="navMiddleLeft" class="navMiddleLeft navMiddleLeft#ArrayLen(variables.formSettings.navLinks)#">&nbsp;</div>
									<div class="navBottomLeft">&nbsp;</div>
									<div class="clear">&nbsp;</div>
								</div>
								
								<div class="navRight">
									<cfloop from="1" to="#ArrayLen(variables.formSettings.navLinks)#" index="i">
										<cfif i eq 1>
											<cfset classNames = "navLink navLinkFirst" />
										<cfelseif i eq ArrayLen(variables.formSettings.navLinks)>
											<cfset classNames = "navLink navLinkLast" />
										<cfelse>
											<cfset classNames = "navLink navLinkMiddle" />
										</cfif>
										
										<cfif variables.formSettings.navLinks[i].type eq "link">
											<cfset onclickScript = "location.href='" & variables.formSettings.navLinks[i].href & "';" />
										<cfelseif variables.formSettings.navLinks[i].type eq "popup">
											<cfset onclickScript = "location.href='" & variables.formSettings.navLinks[i].href & "';" />
										<cfelseif variables.formSettings.navLinks[i].type eq "script">
											<cfset onclickScript = variables.formSettings.navLinks[i].callFunction />
										</cfif>
										
										<cfif StructKeyExists(variables.formSettings.navLinks[i], "id")>
											<cfset id = "id=""" & variables.formSettings.navLinks[i].id & """" />
										<cfelse>
											<cfset id = "" />
										</cfif>
										
										<div class="#classNames#" onclick="#onclickScript#" #id#>
											<div class="padding">
												#variables.formSettings.navLinks[i].title#
											</div>
										</div>
									</cfloop>
									<div class="clear">&nbsp;</div>
									
								</div>
								<div class="clear">&nbsp;</div>
							</div>
							
							<div class="tabbedForm">
								<input type="hidden" name="applicationGUID" id="applicationGUID" value="#arguments.applicationGUID#" />
								<div class="tabs">
									<cfloop from="1" to="#ArrayLen(formSettings.tabs)#" index="i">
										<div id="tab#i#" class="tab tabOff <cfif isAppCompleted>tabDisabled</cfif>" currentIndex="#i#">
											<div class="left">&nbsp;</div>
											<div class="middle">#formSettings.tabs[i].title#</div>
											<div class="right">&nbsp;</div>
											<div class="clear">&nbsp;</div>
										</div>
									</cfloop>
									<div class="clear">&nbsp;</div>
								</div>
								
								<div class="formContentWrap">
									<div class="minHeight">&nbsp;</div> <!--- This will make the content inside here to always be at least 400 pixels in height. --->
									<noscript>
										<div class="noscript">Please enable your javascript to view this page.</div>
									</noscript>
									
									<div id="formContent" class="formContent" style="display:none;"></div>
									<div id="formContentLoader" class="formContentLoader" style="display:none;">
										<img src="#GetCFCBaseURL()#/tabbedForm/images/formProcessing.gif" alt="" />
									</div>
									<div class="clear">&nbsp;</div>
								</div>
								
								<div class="clear">&nbsp;</div>
							</div>
							
						</div>
						<div class="clear">&nbsp;</div>
					</div>
					
					<div class="formWrapBottomLeft">&nbsp;</div>
					<div class="formWrapBottom">&nbsp;</div>
					<div class="formWrapBottomRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>
				</div>
           	    <div class="clear">&nbsp;</div>
			</div>
		</cfsavecontent>
		</cfoutput>
		
		
		<cfreturn formHTML />
	</cffunction>
	
	
	<cffunction name="GetCFCBaseURL" access="public" returntype="string" output="false">
		<cfreturn variables.settings.cfcBaseURL />
	</cffunction>
	
	
	<cffunction name="GetFormStaticInstance" access="public" returntype="struct" output="false">
		
		<!--- uncomment me later --->
		
		<cfif not StructKeyExists(application, "tabbedForm")>
			<!--- This should actually always exist --->
			<cfset application.tabbedForm = CreateObject("component", "cfcs.tabbedForm.tabbedForm").Init() /> <!--- variables.settings.cfcBaseDotPath --->
		</cfif>
		
		<cfreturn application.tabbedForm />
	</cffunction>
	
	
	<cffunction name="GetFormCFCVariables" access="public" returntype="struct" output="false" hint="used by remote functions">
		<cfreturn variables />
	</cffunction>
	
	
	
	<!----------------------------->
	<!--- Private Functions     --->
	<!----------------------------->
	
	<cffunction name="GetComponent" access="public" returntype="any" output="false">
		<cfargument name="component" type="string" required="true" />
		
		<cfreturn variables.sectionComponents[arguments.component] />
		
	</cffunction>
	
	
	
	<!----------------------------->
	<!--- Remote Functions      --->
	<!----------------------------->
	
	<cffunction name="AJAXCall" access="remote" returntype="void" output="true">
		<cfargument name="componentName" type="string" required="true" />
		<cfargument name="functionName" type="string" required="true" />
		<cfargument name="additionalArgument" type="string" required="false" hint="Pass argument to function.  Probably should implement something better." />
		<cfargument name="applicationGUID" type="string" required="false" hint="Pass argument to function.  Probably should implement something better." />
		
		
		<cfset var component = GetFormStaticInstance().GetComponent(arguments.componentName) />
		<cfset var fromComponent = "Function has not been called yet." />
		
		<cfif StructKeyExists(arguments, "additionalArgument") AND arguments.additionalArgument neq "">
			<cfset request.additionalArgument = arguments.additionalArgument />
		</cfif>
		<cfif StructKeyExists(arguments, "applicationGUID") AND arguments.applicationGUID neq "">
			<cfset request.applicationGUID = arguments.applicationGUID />
		</cfif>
		
		
		<cfset Evaluate("fromComponent = component." & arguments.functionName & "()") />
		
		<cfsetting enablecfoutputonly="false" />
		<cfcontent reset="true">
		<cfoutput>#fromComponent#</cfoutput>
	</cffunction>
	
	
	<cffunction name="BuildSummary" access="remote" returntype="void" output="true">
		
		<cfset var areAllTabsComplete = true />
		<cfset var applicationGUID = "" />
		<cfset var curSection = StructNew() />
		<cfset var i = 0 />
		<cfset var isAppCompleted = false />
		<cfset var isCurTabComplete = false />
		<cfset var tabIndex = 0 />
		<cfset var staticInst = GetFormStaticInstance() />
		<cfset var staticInstVars = staticInst.GetFormCFCVariables() />
		<cfset var formSettings = staticInstVars.formSettings />
		
		
		<cfif application.cfcs.register.isSTEMUserLoggedIn()>
			<cfset applicationGUID = GetApplicationGUID(client.STEMRegistrationGUID) />
			<cfset isAppCompleted = IsApplicationSubmitted(applicationGUID) />
			
			
			<div style="color:##323232; font-size:18px; font-weight:bold; margin:10px 10px 10px 10px;">
				Review Application Summary
			</div>
			
			
			<cfif isAppCompleted>
				<div style="color:##243566; font-size:14px; font-weight:bold; margin:10px 10px 20px 10px;">
					Thank You.  Your application has been submitted and received.
				</div>
			</cfif>
			
			
			<div class="summary">
				<cfloop from="1" to="#ArrayLen(formSettings.tabs)#" index="tabIndex">
					<cfset isCurTabComplete = IsTabComplete(applicationGUID, tabIndex) />
					<cfif not isCurTabComplete>
						<cfset areAllTabsComplete = false />
					</cfif>
					
					
					<div class="tabTitleLeft">&nbsp;</div>
					<div class="tabTitleCenter">
						<div class="padding">
							#formSettings.tabs[tabIndex].title#
							<cfif not isCurTabComplete>
								<span style="color:##cc0000;">(This section is not complete)</span>
							</cfif>
						</div>
					</div>
					<div class="tabTitleRight">&nbsp;</div>
					<div class="clear">&nbsp;</div>
					
					<div class="tabContent">
						<cfloop from="1" to="#ArrayLen(formSettings.tabs[tabIndex].pods)#" index="i">
							<cfset curSection = staticInstVars.sectionComponents[formSettings.tabs[tabIndex].pods[i]] />
							<div style="color:##323232; font-size:14px; font-weight:bold; margin:10px;">
								#curSection.GetPodTitle()#
							</div>
							#curSection.BuildPod(applicationGUID, "summary")#
						</cfloop>
						
						<cfif NOT isAppCompleted>
							<cfset callJS = "OpenTab(" & tabIndex & ")" />
							<div class="summaryButtons">
								<div class="rule">&nbsp;</div>
								<div class="editButton">
									#BuildButton("Edit", callJS)#
								</div>
								<div class="clear">&nbsp;</div>
							</div>
						</cfif>
					</div>
				</cfloop>
				
				<cfif NOT isAppCompleted>
					<div class="submitApplication">
						By acknowledging the statements below and submitting this application, I hereby agree to the terms of use and 
						privacy policy.
						
						<div style="margin:5px 0px 5px 0px;">
							<input type="checkbox" name="confirmRecommendation" id="confirmRecommendation" value="1" />
							I have sent my recommender the designated URL to complete my recommendation and understand it is my responsibility 
							to follow up with my recommender to ensure he/she completes the form.
							<span class="required" style="color:##cc0000;">*</span>
						</div>
						
						<div style="margin:0px 0px 5px 0px;">
							<input type="checkbox" name="confirmTermsConditions" id="confirmTermsConditions" value="1" />
							I agree to the terms and conditions.
							<span class="required" style="color:##cc0000;">*</span>
						</div>
						
						<div>
							<input type="checkbox" name="confirmNoPlagiarize" id="confirmNoPlagiarize" value="1" />
							I can honestly say I did not plagiarize.
							<span class="required" style="color:##cc0000;">*</span>
						</div>
						
					</div>
					
					<div class="submitApplicationButtons">
						<div class="rule">&nbsp;</div>
						
						<div class="submitButton">
							#BuildButton("Submit Application", "ValidateSubmitApplication();")#
						</div>
						<div class="exitButton">
							#BuildButton("Exit Application", "ExitApplication();")#
						</div>
						<div class="clear">&nbsp;</div>
					</div>
				</cfif>
			</div>
			
			<cfif NOT isAppCompleted>
				<script type="text/javascript">
					function ValidateSubmitApplication () 
					{
						<cfif areAllTabsComplete>
							if(jQuery("##confirmRecommendation").attr("checked") != true) {
								alert("Please verify and checkmark the following question: 'I have sent my recommender the designated URL to complete my recommendation and understand it is my responsibility to follow up with my recommender to ensure he/she completes the form.'");
							}
							else if(jQuery("##confirmTermsConditions").attr("checked") != true) {
								alert("Please verify and checkmark the following question: 'I agree to the terms and conditions.'");
							}
							else if(jQuery("##confirmNoPlagiarize").attr("checked") != true) {
								alert("Please verify and checkmark the following question: 'I can honestly say I did not plagiarize.'");
							}
							else {
								var formVars = {};
								formVars.applicationGUID = "#applicationGUID#";
								formVars.confirmRecommendation = jQuery("##confirmRecommendation").attr("checked");
								formVars.confirmTermsConditions = jQuery("##confirmTermsConditions").attr("checked");
								formVars.confirmNoPlagiarize = jQuery("##confirmNoPlagiarize").attr("checked");
								
								jQuery.post(remoteURL + "?method=SetApplicationComplete", formVars, function(resp){
									if(Trim(resp).toLowerCase() == "success"){
										jQuery("##changeSelectionLink").css("display", "none");
										
										for (var i = 0; i <= 5; i++) {  // 5 will need to be replaced or a better jQuery select used instead.
											jQuery("##tab" + i).removeClass("tabOn");
											jQuery("##tab" + i).addClass("tabOff");
											jQuery("##tab" + i).addClass("tabDisabled");
											jQuery("##tab" + i).die("click");
										}
										
										OpenSummary();
									}
									else {
										alert("An error occurred during submition.  Your application has not been confirmed as being completed.  Please try again later.");
									}
								});
							}
						<cfelse>
							alert("All required questions must be completed first before submiting this application.");
						</cfif>
					}
				</script>
			</cfif>
		</cfif>
		
	</cffunction>	
	
	
	<cffunction name="BuildTab" access="remote" returntype="void" output="true"> <!--- returntype="string" output="true" --->
		<cfargument name="tabIndex" type="numeric" required="true" />
		
		<cfset var applicationGUID = "" />
		<cfset var callJS = "" />
		<cfset var curSection = StructNew() />
		<cfset var i = 0 />
		<cfset var staticInst = GetFormStaticInstance() />
		<cfset var staticInstVars = staticInst.GetFormCFCVariables() />
		<cfset var formSettings = staticInstVars.formSettings />
		
		
		<cfif application.cfcs.register.isSTEMUserLoggedIn()>
			<cfset applicationGUID = GetApplicationGUID(client.STEMRegistrationGUID) />
			
			<cfif IsApplicationSubmitted(applicationGUID)>
				<!--- We should never even get here --->
				<script type="text/javascript">
					jQuery().ready(function () {
						OpenSummary();
					});
				</script>
				
			<cfelse>
				<span id="jQueryCalendar" class="jQueryCalendar" style="position:absolute; top:20px; left:20px;"></span>
				
				<form id="tabForm">
					<cfif StructKeyExists(formSettings.tabs[arguments.tabIndex], "description")>
						<div class="tabDescription">
							#formSettings.tabs[arguments.tabIndex].description#
						</div>
					</cfif>
					
					<cfloop from="1" to="#ArrayLen(formSettings.tabs[arguments.tabIndex].pods)#" index="i">
						<cfset curSection = staticInstVars.sectionComponents[formSettings.tabs[arguments.tabIndex].pods[i]] />
						#curSection.BuildPod(applicationGUID, "edit")#
					</cfloop>
					
					<div class="tabButtons">
						<!--- <div class="rule">&nbsp;</div> --->
						<div class="saveButton">
							<cfset callJS = "SaveAndContinue('submitClick', " & arguments.tabIndex & ")" />
							#BuildButton("Save & Continue", callJS)#
						</div>
						<div class="exitButton">
							#BuildButton("Exit Application","ExitApplication();")#
						</div>
						<div class="clear">&nbsp;</div>
					</div>
				</form>
			
			</cfif>
			
		<cfelse>
			<div class="noscript">You must be logged in to view your application.</div>
		</cfif>
	</cffunction>
	
	
	<cffunction name="IsTabComplete" access="public" returntype="boolean" output="false">
		<cfargument name="applicationGUID" type="string" required="true" />
		<cfargument name="tabIndex" type="numeric" required="true" />
		
		<cfset var retVal = false />
		
		<cfset retVal = true />
		
		<cfreturn retVal />
	</cffunction>
	
	
	<cffunction name="SaveTab" access="remote" returntype="void" output="true">
		<cfargument name="applicationGUID" type="string" required="true">
		<cfargument name="tabIndex" type="string" required="true" />
		
		<cfset var curSection = "" />
		<cfset var err = "" />
		<cfset var isTabComplete = true />
		<cfset var key = "" />
		<cfset var returnStr = "Success" />
		<cfset var staticInst = GetFormStaticInstance() />
		<cfset var staticInstVars = staticInst.GetFormCFCVariables() />
		<cfset var formSettings = staticInstVars.formSettings />
		
		
		<cfif application.cfcs.register.isSTEMUserLoggedIn()>
			<cftry>
				<cfloop from="1" to="#ArrayLen(formSettings.tabs[arguments.tabIndex].pods)#" index="i">
					<cfset curSection = staticInstVars.sectionComponents[formSettings.tabs[arguments.tabIndex].pods[i]] />
					<cfset curSection.SavePod(arguments.applicationGUID, arguments) />
					
					<cfif curSection.IsPODComplete(arguments.applicationGUID) eq false>
						<cfset isTabComplete = false />
					</cfif>
				</cfloop>
				
				<cfset SaveTabCompleted(arguments.applicationGUID, arguments.tabIndex, isTabComplete) />
				
				<cfcatch>
					<cfset returnStr = "Failed" />
					
					<!--- Lets initially monitor for errors --->
					<cfsavecontent variable="err">
						<div style="font-size:12px;">
							<strong>CFCATCH</strong><br />
							<strong>Message:</strong> #cfcatch.message#<br />
							<strong>Detail:</strong> #cfcatch.detail#<br />
							<strong>Template:</strong> #cfcatch.TagContext[1].Template#<br />
							<strong>Line:</strong> #cfcatch.TagContext[1].Line#<br />
							<strong>Type:</strong> #cfcatch.TagContext[1].Type#<br />
							<strong>Time:</strong> #DateFormat(Now(), "m/y/yyyy")# at #TimeFormat(Now(), "h:mm tt")#
							
							<br /><br />
							
							<strong>ARGUMENTS</strong><br />
							<cfloop collection="#arguments#" item="key">
								<strong>#key#:</strong> #arguments[key]#<br />
							</cfloop>
							<br /><br />
							
							<strong>CGI</strong><br />
							<cfloop collection="#cgi#" item="key">
								<strong>#key#:</strong> #cgi[key]#<br />
							</cfloop>
							<br /><br />
						</div>
					</cfsavecontent>
					
					<cfif Left(cgi.server_name, 5) eq "local">
						<!--- <cfset server.tools.DumpToFile(cfcatch, "SaveTabErrorCaught") /> --->
						<cfset errors="#cfcatch.message##chr(10)##cfcatch.detail##chr(10)##cfcatch.tagContext[1].template# LINE:#cfcatch.tagContext[1].line#">
						<cfset fileWrite(expandPath("/errors/"&getTickCount()&".txt"),errors)>
					<cfelse>
						<cfmail from="stemError@redacted.com" to="daryl_walsh@redacted.com" subject="STEM ERROR" type="html">#err#</cfmail>
					</cfif>
					
				</cfcatch>
			</cftry>
			
		<cfelse>
			<cfset returnStr = "Timeout" />
			
		</cfif>
		
		<cfsetting enablecfoutputonly="true" />
		<cfcontent reset="true" />
		<cfoutput>#returnStr#</cfoutput>
		
	</cffunction>
	
	
</cfcomponent>