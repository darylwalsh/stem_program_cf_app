<cfcomponent extends="ajax" displayname="MDR Department Lookup">

	<cffunction access="public" name="getStates" output="no" returntype="query">
        <cfset var qStates = "" />
		
		<cfquery name="qStates" datasource="EDU_SEC">
            select distinct m.mstate, s.region_name
            from MDR.dbo.mdr_master_us_curr m WITH (nolock)
                inner join edu_sec.dbo.SYSTEM_COUNTRY_REGION s WITH (nolock)
				 ON m.mstate = s.region_code_alpha
            where (m.filetype in ('02','04','07','09','10','13'))
            and m.current_ind <> 'D'
            order by s.region_name
        </cfquery>
		
        <cfreturn qStates>
	</cffunction>
    
	
	<cffunction access="public" name="getCities" output="no" returntype="query">
		<cfargument name="strState" type="string" required="yes">
        
		<cfset var qGetCities = "" />
		
		
        <cfquery name="qGetCities" datasource="EDU_SEC">
            select distinct mcity
            from MDR.dbo.mdr_master_us_curr m WITH (nolock)
            where mstate = '#arguments.strState#'
            and (m.filetype in ('02','04','07','09','10','13'))
    		and m.current_ind <> 'D'
            order by mcity
        </cfquery>
		
		<cfreturn qGetCities>
	</cffunction>
    
	
	<cffunction access="public" name="getRegions" output="no" returntype="query">
		<cfargument name="COUNTRY_CODE" type="string" required="yes">
        
		<cfset var states = "" />
		
		
        <cfquery name="states" datasource="EDU_SEC">
            EXEC usp_SYSTEM_COUNTRY_REGION_SelectContains 
					@COUNTRY_CODE = <cfqueryparam value="#arguments.COUNTRY_CODE#" cfsqltype="cf_sql_varchar" />,
					@ORDERBY = <cfqueryparam value="REGION_NAME" cfsqltype="cf_sql_varchar" />
        </cfquery>
		
		<cfreturn states />
	</cffunction>
	
	
    <cffunction access="public" name="getSchools" output="no" returntype="string">
		<cfargument name="strState" type="string" required="yes">
        <cfargument name="strCity" type="string" required="yes">
        
        <cfset strHTML = "">
        
        <cfquery name="qGetSchools" datasource="EDU_SEC">
            select 
                  m.filetype,
                  m.PID,
                  REPLACE(m.NAME,CHAR(39),char(96)) NAME,	/* Replace single quote with accent, so javascript does not blowup */
                  REPLACE(m.MSTREET,CHAR(39),char(96)) MSTREET,
                  REPLACE(m.MCITY,CHAR(39),char(96)) MCITY,
                  m.MSTATE,
                  m.MSTATE STATE,
                  m.MZIPCODE,
                  m.enrollment,
                  case isnumeric(m.lowgrade) 
                       when 0 then '00' 
                       else m.lowgrade end
                  as lowgrade,
                  isnull(m.highgrade,'99') as highgrade
                ,m.ParentPID
                ,(select top 1 x.NAME from MDR.dbo.mdr_master_us_curr x WITH (nolock) where x.PID = m.ParentPID) as ParentName
                , m.areacode
                , m.exchange
                , m.number
                , '(' + m.areacode + ') ' + m.exchange + ' - ' + m.number phonenumber
          from MDR.dbo.mdr_master_us_curr m WITH (nolock)
          where m.mcity = '#arguments.strCity#'
          and m.mstate = '#arguments.strState#'
          and m.filetype in ('02','04','07','09','10','13')
          and m.current_ind <> 'D'
          order by m.name, m.mcity, m.mstate, m.mzipcode
        </cfquery>
        
        <cfif qGetSchools.recordcount>
        	<cfloop query="qGetSchools">
        		<cfset strHTML = "#strHTML#<tr style='height:20px; cursor:pointer; background:##f6f6f6;' class='myRollover' onmouseover=""this.style.backgroundColor='Yellow'"" onmouseout=""this.style.backgroundColor='##f6f6f6'"" onClick=""copyToOpener('#PID#','#NAME#','#MSTREET#','#MCITY#','#MSTATE#','#STATE#','#MZIPCODE#','#phonenumber#','US');"";><td class='MyData' width='260'>#NAME#</td><td class='MyData' width='184'>#MSTREET#</td><td class='MyData' width='144'>#MCITY#, #STATE#</td><td class='MyData' width='63' align='center'>#MZIPCODE#&nbsp;</td></tr>">
            </cfloop>
		<cfelse>
        	<cfset strHTML = "<tr><td colspan='4' align='center' style='font-family:Arial, Helvetica, sans-serif; font-size: 12px;'>- no schools found -</td></tr>">
		</cfif>
        
        <cfset strHTML = "<table cellspacing='0' class='MyData'>#strHTML#</table>">
        
		<cfreturn strHTML>
	</cffunction>
	
	
	<cffunction access="remote" name="ACCOUNTSelectContains" output="no" returntype="query">
		<cfargument name="inpform" type="struct" required="yes">
		<cfargument name="LoggedOnUserID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="NAME" type="string" required="No" default="#arguments.inpform.NAME#">
		<cfargument name="ACCOUNT_GUID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="ENTITY_TYPE_GUID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="LANGUAGE_CODE" type="string" required="No" default="~">
		<cfargument name="TZOFFSET" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="ADDRESS_GUID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="Partner_GUID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="Partner_Key" type="string" required="No" default="~">
		<cfargument name="ADDRESS1" type="string" required="No" default="~">
		<cfargument name="ADDRESS2" type="string" required="No" default="~">
		<cfargument name="ADDRESS3" type="string" required="No" default="~">
		<cfargument name="CITY" type="string" required="No" default="~">
		<cfargument name="POSTAL_CODE" type="string" required="No" default="~">
		<cfargument name="REGION_CODE" type="string" required="No" default="~">
		<cfargument name="COUNTRY_CODE" type="string" required="No" default="~">
		<cfargument name="MAIN_PHONE" type="string" required="No" default="~">
		<cfargument name="FAX_PHONE" type="string" required="No" default="~">
		<cfargument name="ORDERBY" type="string" required="No" default="~">
		<cfargument name="Contains" type="string" required="No" default="~">
		<cfargument name="DOLOG" type="string" required="No" default="~">
		<cfargument name="ANDOR" type="string" required="No" default="~">
		<cfstoredproc procedure="usp_ACCOUNT_SelectContains" datasource="EDU_SEC">
			
			<cfprocparam type="In" value="#arguments.LoggedOnUserID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.NAME#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ACCOUNT_GUID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ENTITY_TYPE_GUID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.LANGUAGE_CODE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.TZOFFSET#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ADDRESS_GUID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.Partner_GUID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.Partner_Key#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ADDRESS1#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ADDRESS2#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ADDRESS3#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.CITY#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.POSTAL_CODE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.REGION_CODE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.COUNTRY_CODE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.MAIN_PHONE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.FAX_PHONE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ORDERBY#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.Contains#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.DOLOG#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ANDOR#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="usp_ACCOUNT_SelectContains_Result" resultset="1">
		</cfstoredproc>
		<cfreturn usp_ACCOUNT_SelectContains_Result>
	</cffunction>    
	
	
	<cffunction access="remote" name="SITESelectContains" output="no" returntype="string">
		
		<cfargument name="inpform" type="struct" required="yes">
		<cfargument name="LoggedOnUserID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="ACCOUNT_GUID" type="guid" required="No" default="#arguments.inpform.ACCOUNT_GUID#">
		<cfargument name="NAME" type="string" required="No" default="~">
		<cfargument name="SITE_GUID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="ENTITY_TYPE_GUID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="LANGUAGE_CODE" type="string" required="No" default="~">
		<cfargument name="TZOFFSET" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="Partner_GUID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="ADDRESS_GUID" type="guid" required="No" default="00000000-0000-0000-0000-000000000000">
		<cfargument name="PASSCODE" type="string" required="No" default="~">
		<cfargument name="Partner_Key" type="string" required="No" default="~">
		<cfargument name="ADDRESS1" type="string" required="No" default="~">
		<cfargument name="ADDRESS2" type="string" required="No" default="~">
		<cfargument name="ADDRESS3" type="string" required="No" default="~">
		<cfargument name="CITY" type="string" required="No" default="~">
		<cfargument name="POSTAL_CODE" type="string" required="No" default="~">
		<cfargument name="REGION_CODE" type="string" required="No" default="~">
		<cfargument name="COUNTRY_CODE" type="string" required="No" default="~">
		<cfargument name="MAIN_PHONE" type="string" required="No" default="~">
		<cfargument name="FAX_PHONE" type="string" required="No" default="~">
		<cfargument name="ORDERBY" type="string" required="No" default="~">
		<cfargument name="Contains" type="string" required="No" default="~">
		<cfargument name="DOLOG" type="string" required="No" default="~">
		<cfargument name="ANDOR" type="string" required="No" default="~">
        
		<cfstoredproc procedure="usp_SITE_SelectContains" datasource="EDU_SEC">			
			<cfprocparam type="In" value="#arguments.LoggedOnUserID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ACCOUNT_GUID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.NAME#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.SITE_GUID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ENTITY_TYPE_GUID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.LANGUAGE_CODE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.TZOFFSET#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.Partner_GUID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ADDRESS_GUID#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.PASSCODE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.Partner_Key#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ADDRESS1#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ADDRESS2#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ADDRESS3#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.CITY#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.POSTAL_CODE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.REGION_CODE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.COUNTRY_CODE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.MAIN_PHONE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.FAX_PHONE#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ORDERBY#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.Contains#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.DOLOG#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="In" value="#arguments.ANDOR#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qResult" resultset="1">
		</cfstoredproc>
        
		<cfset strHTML = "">
		<cfif qResult.recordcount>
        	<cfloop query="qResult">
        		<cfset strHTML = "#strHTML#<tr style='height:20px;' class='myRollover' onmouseover=""this.style.backgroundColor='Yellow'"" onmouseout=""this.style.backgroundColor='##f7f7f7'"";><td class='MyData' width='39%'>#qResult.account_guid#</td><td class='MyData' width='39%'>#qResult.site_guid#</td><td class='MyData' align='left'>#qResult.name#</td></tr>">
            </cfloop>
		<cfelse>
        	<cfset strHTML = "<tr><td colspan='3' align='center' style='font-family:Arial, Helvetica, sans-serif; font-size: 12px;'>- no schools found -</td></tr>">
		</cfif>
        
        <cfset strHTML = "<table cellspacing='0' class='MyData' style='overflow: auto; overflow-x: hidden; width: 94%; border: none; background-color: ##f7f7f7;'>#strHTML#</table>">
        
		<cfreturn strHTML>
	</cffunction>
	
	
</cfcomponent>
