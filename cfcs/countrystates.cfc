<cfcomponent>
	<cffunction access="remote" name="getCountries" hint="This is used to return the raw query of countries" returnType="query">
		<cfquery name="qCountries" datasource="#Application.dbinfo.strGlobalDatasource#">			
            SELECT COUNTRY_CODE, COUNTRY_NAME
            FROM SYSTEM_COUNTRY (nolock)
            ORDER BY CASE WHEN COUNTRY_NAME = 'United States' THEN '_United States' ELSE COUNTRY_NAME END /* force USA to top of list */
		</cfquery>
		<cfreturn qCountries>
	</cffunction>
    
    <cffunction access="remote" name="getCountriesStates" output="false" returntype="query">				
        <cfquery name="qStatesByCountry" datasource="#Application.dbinfo.strGlobalDatasource#">
            SELECT r.COUNTRY_CODE
                , r.REGION_CODE
                , r.REGION_NAME AS StateName
                , r.REGION_CODE_ALPHA AS StateCode
                , isNull(isNull(r.LANGUAGE_CODE, c.LANGUAGE_CODE),'~') LANGUAGE_CODE	--default to ~ so scott's stored proc will work
            FROM SYSTEM_COUNTRY_REGION r (nolock)
                INNER JOIN SYSTEM_COUNTRY c (nolock) ON r.COUNTRY_CODE = c.COUNTRY_CODE
            ORDER BY r.REGION_NAME
        </cfquery>
		<cfreturn qStatesByCountry>
	</cffunction>
    
     <cffunction access="remote" name="getTimeZones" output="false" returntype="query">		
		<cfquery name="qTimeZones" datasource="#Application.dbinfo.strGlobalDatasource#">
        	SELECT
                TZOFFSET_GUID
                , NAME strTimeZone
                , UDTOffset
            FROM TZOFFSET  (nolock)
            ORDER BY CASE 
                WHEN NAME LIKE 'US/%' THEN '__' + NAME
                WHEN NAME LIKE 'America/%' THEN '_' + NAME
                ELSE NAME END /* force USA and America to top of list */
		</cfquery>

		<cfreturn qTimeZones>
	</cffunction>  
    
</cfcomponent>