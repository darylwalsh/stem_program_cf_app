<cfcomponent displayname="reportsEXTENDS.cfc" output="false">
	
	<!-------------------------------->
	<!--- CONSTRUCTOR              --->
	<!-------------------------------->
	
	<cfset variables.settings = StructNew() />
	
	<cffunction name="Init" access="public" output="false" returntype="any">
		<cfargument name="settings" type="Struct" required="true" />
		
		<cfset variables.settings = arguments.settings />
		
		<cfreturn this />
	</cffunction>
	
	
	
	<!-------------------------------->
	<!--- PUBLIC FUNCTIONS         --->
	<!-------------------------------->
	
	<cffunction name="DisplayReportsPage" access="public" output="true" returntype="void">
		
		#GetReportsHeaderHTML()#
		#GetReportsScripts()#
		<h2>#GetReportsTitle()#</h2>
		
		<cfif CanViewReports()>
			#GetReportsHTML()#
		<cfelse>
			#GetReportsLoginMessage()#
		</cfif>
		#GetReportsFooterHTML()#
		
	</cffunction>
	
	
	<cffunction name="GenerateCSVString" access="public" output="false" returntype="string">
		<cfargument name="rcds" type="query" required="true" />
		
		<cfset var addComma = false />
		<cfset var csvFile = CreateObject("java", "java.lang.StringBuffer").init() />
		<cfset var fieldValue = "" />
		<cfset var fieldValueDisplay = "" />
		<cfset var i = 0 />
		<cfset var md = GetMetaData(arguments.rcds) />
		<cfset var wccColumnName = "" />
		
		
		<!--- create the header row for the column titles --->
		<cfloop from="1" to="#ArrayLen(md)#" index="i">
			<cfif i eq ArrayLen(md)>
				<cfset csvFile.append(AddQuotes(Replace(md[i].Name, "_", " ", "all"), false)) />
			<cfelse>
				<cfset csvFile.append(AddQuotes(Replace(md[i].Name, "_", " ", "all"), true)) />
			</cfif>
		</cfloop>
		<cfset csvFile.append(chr(13) & chr(10)) />
		
		<!--- find out how many records there are, if > 60K, then make this become a txt file instead of a csv file --->
		
		
		<!--- append the data --->
		<cfloop query="rcds">
			<cfloop from="1" to="#ArrayLen(md)#" index="i">
				<cfset addComma = true />
				<cfif i eq ArrayLen(md)>
					<cfset addComma = false />
				</cfif>
				
				<cfset fieldValue = rcds[md[i].Name][rcds.CurrentRow] />
				<cfset fieldValueDisplay = " " />
				<cfif md[i].TypeName eq "datetime">
					<cfif IsDate(fieldValue)>
						<cfset fieldValueDisplay = DateFormat(fieldValue, "mm/dd/yyyy") & " " & TimeFormat(fieldValue, "hh:mm tt") />
					</cfif>
					
				<cfelseif md[i].TypeName eq "bit">
					<cfif fieldValue eq 1>
						<cfset fieldValueDisplay = "X" />
					</cfif>
					
				<cfelseif md[i].Name eq "MENTOR_BIRTHDATE" OR md[i].Name eq "BIRTHDATE" OR md[i].Name eq "hsmember1bd" OR md[i].Name eq "hsmember2bd" OR md[i].Name eq "hsmember3d"> <!--- Special Processing for certain WCC Fields --->
					<cfif IsDate(fieldValue)>
						<cfset fieldValueDisplay = DateFormat(fieldValue, "mm/dd/yyyy") />
					</cfif>
					
				<cfelse>
					<cfif fieldValue neq "">
						<cfset fieldValueDisplay = fieldValue />
					</cfif>
					
				</cfif>
				
				<cfset csvFile.append(AddQuotes(fieldValueDisplay, addComma)) />
			</cfloop>
			
			<cfset csvFile.append(chr(13) & chr(10)) />
		</cfloop>
		
		<cfreturn csvFile.toString() />
	</cffunction>
	
	
	
	<!-------------------------------->
	<!--- PRIVATE FUNCTIONS        --->
	<!-------------------------------->
	
	<cffunction name="AddQuotes" access="private" output="false" returntype="string" hint="wraps string with qoutes and corrects quotes inside text">
		<cfargument name="toQuote" type="string" />
		<cfargument name="addComma" type="boolean" required="false" default="0" />
		
		<cfset arguments.toQuote = """" &  Replace(arguments.toQuote, """", """""", "all") &  """" />
		<cfif arguments.addComma>
			<cfset arguments.toQuote = arguments.toQuote & "," />
		</cfif>
		
		
		<cfreturn arguments.toQuote />
	</cffunction>
	
	
	<cffunction name="StripTags" access="private" output="false" returntype="string" hint="Not perfect but works well">
		<cfargument name="toStrip" type="string" required="true" />
		
		<cfset arguments.toStrip = REReplaceNoCase(arguments.toStrip, "<(.|\n)*?>", "", "all") />
		<cfset arguments.toStrip = ReplaceNoCase(arguments.toStrip, "&nbsp;", "", "all") />
		<cfset arguments.toStrip = Trim(arguments.toStrip) />
		
		<cfreturn arguments.toStrip />
	</cffunction>
	
	
</cfcomponent>