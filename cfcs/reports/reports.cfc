<cfcomponent displayname="reports" extends="reportsEXTENDS" implements="reportsINTERFACE" output="false">

	<cfset variables.dsn = "Sponsorships" />

	<!---------------------------------------------------------->
	<!--- Functions to Implement From the reportsINTERFACE   --->
	<!---------------------------------------------------------->

	<cffunction name="CanViewReports" access="public" output="false" returntype="boolean">

		<cfset var canView = false />
		<cfset var getRoles = "" />

		<cfif StructKeyExists(cookie, "app_user_guid") and IsValid("guid", cookie.app_user_guid)>
			<cfquery name="getRoles" datasource="EDU_SEC">
				SELECT r.CODE
				FROM user_role AS ur with (nolock)
					INNER JOIN role AS r with (nolock)
					ON (
							ur.role_guid = r.role_guid
						)
				WHERE app_user_guid = <cfqueryparam value="#cookie.app_user_guid#" cfsqltype="cf_sql_varchar" />
			</cfquery>

			<cfloop query="getRoles">
				<cfif ListFindNoCase("GLOBALADMIN,DSCADMIN,USSYSADMIN,DATAINTEGRITY,IMPLEMENTATION,RENEWAL,SALES", getRoles.CODE) gt 0>
					<cfset canView = true />
					<cfbreak />
				</cfif>
			</cfloop>
		<cfelseif StructKeyExists(client, "STEMJudgeGUID") AND IsValid("guid", client.STEMJudgeGUID)>
			<cfset canView = true />
		</cfif>

		<cfreturn canView />
	</cffunction>


	<cffunction name="GetCSVReport" access="remote" output="true" returntype="void" hint="Only this function will need to be customized for different reports">
		<cfargument name="report" type="string" required="true" default="blank" />

		<cfset var fileName = "EmptyReport.csv" />
		<cfset var queryString = "" />
		<cfset var reportData = QueryNew("") />

		<cfif CanViewReports()>
			<cfsetting requesttimeout="300" />

			<cfif arguments.report eq "allRegistrations">
				<cfsavecontent variable="queryString">
					SELECT * FROM STEMRegistrations (nolock)
					ORDER BY created DESC
				</cfsavecontent>

				<cfset GetCSVReportLowMemMode(fileName="STEMAllRegistrations.csv", queryString=queryString) />

			<cfelseif arguments.report eq "allApplications">
				<cfsavecontent variable="queryString">
				SELECT	
sr.firstName, sr.lastName, sr.email, sr.phone, sr.de_updates,		
sa.applicationGUID,sa.registrationGUID,sa.created,sa.lastUpdated,sa.tab1LastUpdated,sa.tab1Completed,sa.tab2LastUpdated,sa.tab2Completed,sa.tab3LastUpdated,sa.tab3Completed,sa.tab4LastUpdated,sa.tab4Completed,sa.tab5LastUpdated,sa.tab5Completed,sa.tab6LastUpdated,sa.tab6Completed,sa.allTabsCompleted,sa.programInstitute2011,sa.programSTARs2011,sa.programPreference,sa.teachingYears,sa.currentPositionFrom,sa.currentPositionTo,sa.currentPositionSchool,sa.currentPositionGrades,sa.currentPositionGradesInSchool,sa.currentPositionCourse,sa.currentPositionNextYear,sa.currentPositionNextYearNoExplain,sa.siemensRecognized,sa.principalSupport,sa.principalSupportWhyNo,sa.principalSupportPrefix,sa.principalSupportFirstName,sa.principalSupportLastName,sa.principalSupportEmail,sa.technologyKnowledge,sa.technologyUseWikis,sa.technologyUseBlogs,sa.technologyUseSocialMedia,sa.technologyUseStorytelling,sa.technologyUseWeb2_0,sa.referencePrefix,sa.referenceFirstName,sa.referenceMI,sa.referenceLastName,sa.referenceSchool,sa.referenceTitle,sa.referenceTitleOther,sa.referenceRelationship,sa.referenceRelationshipOther,sa.videoType,sa.essaySTARs1,sa.essaySTARs2YN,sa.essaySTARs2,sa.essaySTARs3,sa.essaySTEMInstitute1,sa.essaySTEMInstitute2,sa.essaySTEMInstitute3,sa.essayAdditionalInformation,sa.confirmRecommendation,sa.confirmTermsConditions,sa.confirmNoPlagiarize,sa.programInstitute,sa.programSTARsOR,sa.programSTARsPN,sa.programPreference1,sa.programPreference2,sa.areYouLegalResident,sa.currentPositionFullTime,sa.appliedSTEMInPast,sa.appliedSTEMInPastValue,sa.appliedSTEMInPastInstitute2010,sa.appliedSTEMInPastInstitute2011,sa.appliedSTEMInPastSTARs2010,sa.appliedSTEMInPastSTARs2011,sa.videoUTube,sa.optionalDemographicsGender,sa.optionalDemographicsEthnicity,
IsDate(srs.created) AS hasRecommendationSheet
							<!--- sa.videoUTube,
				 			<!--- (SELECT TOP (1) fileName
				            	FROM STEMAppsRecommendationFiles WITH (nolock)
				            	WHERE applicationGUID = sa.applicationGUID
				            	ORDER BY created DESC) AS recommendationFileName, --->
							sa.applicationGUID, sa.registrationGUID, sa.created, sa.lastUpdated,
							sa.tab1LastUpdated, sa.tab1Completed, sa.tab2LastUpdated, sa.tab2Completed,
							sa.tab3LastUpdated, sa.tab3Completed, sa.tab4LastUpdated, sa.tab4Completed,
							sa.tab5LastUpdated, sa.tab5Completed,
							sa.allTabsCompleted,  --->
							
<!--- sa.programInstitute, sa.programSTARsOR, sa.programSTARsPN, sa.programPreference1, sa.programPreference2, --->
							
							<!--- will need to add reference info later --->
					FROM	STEMApplications AS sa (nolock)
						LEFT JOIN STEMRegistrations AS sr (nolock)
						ON (
								sr.registrationGUID = sa.registrationGUID
							)
							LEFT JOIN STEMAppsRecommendationSheets AS srs
							ON (
									srs.applicationGUID = sa.applicationGUID
								)
					WHERE sa.created > #request.contestStart#
					and sa.applicationGUID IN (Select applicationGUID from STEMappsJudgingApproved WITH (NOLOCK))
					ORDER BY	sa.allTabsCompleted DESC,
								sa.tab5Completed DESC, sa.tab4Completed DESC,
								sa.tab3Completed DESC, sa.tab2Completed DESC, sa.tab1Completed DESC
				<!--- SELECT	
					sa.videoUTube,
					sa.applicationGUID, sa.registrationGUID, sa.created, sa.lastUpdated,
					sa.tab1LastUpdated, sa.tab1Completed, sa.tab2LastUpdated, sa.tab2Completed,
					sa.tab3LastUpdated, sa.tab3Completed, sa.tab4LastUpdated, sa.tab4Completed,
					sa.tab5LastUpdated, sa.tab5Completed,
					sa.allTabsCompleted, IsDate(rs.created) AS hasRecommendationSheet,
					case when sa.programInstitute = 1 then 1 else 0 end as programInstitute, 
					case when sa.programSTARsOR = 1 then 1 else 0 end as programSTARsOR, 
					case when sa.programSTARsPN = 1 then 1 else 0 end as programSTARsPN,
					sa.programPreference1, sa.programPreference2,
					sr.firstName, sr.lastName, sr.email, sr.phone, sr.de_updates
													
				FROM STEMApplications sa WITH (NOLOCK)
				inner join STEMRegistrations sr WITH (NOLOCK) ON (sr.registrationGUID = sa.registrationGUID)
				LEFT JOIN STEMAppsRecommendationSheets rs WITH (NOLOCK) ON (rs.applicationGUID = sa.applicationGUID)
				WHERE sa.allTabsCompleted IS NOT NULL
					and sa.created >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />
					and sa.allTabsCompleted >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />	
					and IsDate(rs.created) = 1
					and sa.videoUTube is not null
					and
						((
						sr.created >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />
							or
						sr.lastUpdated >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />
						))						
				ORDER BY sa.allTabsCompleted DESC		 --->	
				</cfsavecontent>

				<cfset GetCSVReportLowMemMode(fileName="#request.contestYear#STEMAllApplications.csv", queryString=queryString) />			
			
			
			<cfelseif arguments.report eq "allApplicationsApproved">
				<cfsavecontent variable="queryString">
				SELECT	
sr.firstName, sr.lastName, sr.email, sr.phone, sr.de_updates,		
sa.applicationGUID,sa.registrationGUID,sa.created,sa.lastUpdated,sa.tab1LastUpdated,sa.tab1Completed,sa.tab2LastUpdated,sa.tab2Completed,sa.tab3LastUpdated,sa.tab3Completed,sa.tab4LastUpdated,sa.tab4Completed,sa.tab5LastUpdated,sa.tab5Completed,sa.tab6LastUpdated,sa.tab6Completed,sa.allTabsCompleted,sa.programInstitute2011,sa.programSTARs2011,sa.programPreference,sa.teachingYears,sa.currentPositionFrom,sa.currentPositionTo,sa.currentPositionSchool,sa.currentPositionGrades,sa.currentPositionGradesInSchool,sa.currentPositionCourse,sa.currentPositionNextYear,sa.currentPositionNextYearNoExplain,sa.siemensRecognized,sa.principalSupport,sa.principalSupportWhyNo,sa.principalSupportPrefix,sa.principalSupportFirstName,sa.principalSupportLastName,sa.principalSupportEmail,sa.technologyKnowledge,sa.technologyUseWikis,sa.technologyUseBlogs,sa.technologyUseSocialMedia,sa.technologyUseStorytelling,sa.technologyUseWeb2_0,sa.referencePrefix,sa.referenceFirstName,sa.referenceMI,sa.referenceLastName,sa.referenceSchool,sa.referenceTitle,sa.referenceTitleOther,sa.referenceRelationship,sa.referenceRelationshipOther,sa.videoType,sa.essaySTARs1,sa.essaySTARs2YN,sa.essaySTARs2,sa.essaySTARs3,sa.essaySTEMInstitute1,sa.essaySTEMInstitute2,sa.essaySTEMInstitute3,sa.essayAdditionalInformation,sa.confirmRecommendation,sa.confirmTermsConditions,sa.confirmNoPlagiarize,sa.programInstitute,sa.programSTARsOR,sa.programSTARsPN,sa.programPreference1,sa.programPreference2,sa.areYouLegalResident,sa.currentPositionFullTime,sa.appliedSTEMInPast,sa.appliedSTEMInPastValue,sa.appliedSTEMInPastInstitute2010,sa.appliedSTEMInPastInstitute2011,sa.appliedSTEMInPastSTARs2010,sa.appliedSTEMInPastSTARs2011,sa.videoUTube,sa.optionalDemographicsGender,sa.optionalDemographicsEthnicity,
IsDate(srs.created) AS hasRecommendationSheet
						
					FROM	STEMApplications AS sa (nolock)
						LEFT JOIN STEMRegistrations AS sr (nolock)
						ON (
								sr.registrationGUID = sa.registrationGUID
							)
							LEFT JOIN STEMAppsRecommendationSheets AS srs
							ON (
									srs.applicationGUID = sa.applicationGUID
								)
					WHERE sa.created > #request.contestStart#
					and sa.applicationGUID IN (Select applicationGUID from STEMappsJudgingApproved where isApproved = 1)
					ORDER BY	sa.allTabsCompleted DESC,
								sa.tab5Completed DESC, sa.tab4Completed DESC,
								sa.tab3Completed DESC, sa.tab2Completed DESC, sa.tab1Completed DESC
				
				</cfsavecontent>

				<cfset GetCSVReportLowMemMode(fileName="#request.contestYear#STEMAllApplicationsApproved.csv", queryString=queryString) />			
			
			
			
			<cfelseif arguments.report eq "preapprovalstats">
				<cfsavecontent variable="queryString">				
					SELECT 
						case 
							when c.isApproved = 3 then 'Not Judged Yet' 
							when c.isApproved = 2 then 'Maybe'
							when c.isApproved = 1 then 'Approved'
							when c.isApproved = 0 then 'Denied'
							end as Status,
						Convert(varchar(50),a.allTabsCompleted,101) as Completed,
						c.displayID,a.applicationGUID as AppID,b.Firstname,b.Lastname,
						case 
							when c.Type = 'Institute' 
							then 'Institute'
							else ''
							end as Institute,
						case 
							when c.Type = 'STARs' and a.programSTARsOR = 1
							then 'STARsOR'
							else ''
							end as STARsOR,
						case 
							when c.Type = 'STARs' and a.programSTARsPN = 1
							then 'STARsPN'
							else ''
							end as STARsPN
						
					FROM STEMApplications a WITH (NOLOCK)
					inner join STEMRegistrations b WITH (NOLOCK) on b.registrationGUID = a.registrationGUID
					inner join STEMAppsJudgingApproved c WITH (NOLOCK) on c.applicationGUID = a.applicationGUID
					Order by c.isApproved desc, Cast(c.displayID as int)
				</cfsavecontent>
			
			<cfset GetCSVReportLowMemMode(fileName="#request.contestYear#STEMpreapprovalstats.csv", queryString=queryString) />
			
			<cfelseif arguments.report eq "toteBags">
				<cfset fileName = "STEMToteBags.csv" />

				<cfquery name="reportData" datasource="#variables.dsn#">
					SELECT street.SPONSOR_USER_GUID, street.CREATE_DT, street.UserName, street.Street, city.City, state.State, zip.Zip, optIn.optInDEEmails
					FROM (
							SELECT spr.SPONSOR_USER_GUID, spr.CREATE_DT, su.FIRST_NAME + ' ' + su.LAST_NAME AS UserName, spr.SETTING_VALUE AS Street
							FROM SPONSOR_PROF AS sp (nolock)
								INNER JOIN SPONSOR_PROF_SETTING AS sps (nolock)
								ON sps.SPONSOR_PROF_GUID = sp.SPONSOR_PROF_GUID
									INNER JOIN SPONSOR_PROF_RESP AS spr (nolock)
									ON spr.SPONSOR_PROF_SETTING_GUID = sps.SPONSOR_PROF_SETTING_GUID
										INNER JOIN SPONSOR_USER AS su (nolock)
										ON su.SPONSOR_USER_GUID = spr.SPONSOR_USER_GUID
							WHERE	sp.CODE = 'STEM_TOTE_BAG_2010' AND
									sps.DESCRIPTION = 'Street'
						) AS street
						LEFT JOIN (
										SELECT spr.SPONSOR_USER_GUID, spr.SETTING_VALUE AS City
										FROM SPONSOR_PROF AS sp (nolock)
											INNER JOIN SPONSOR_PROF_SETTING AS sps (nolock)
											ON sps.SPONSOR_PROF_GUID = sp.SPONSOR_PROF_GUID
												INNER JOIN SPONSOR_PROF_RESP AS spr (nolock)
												ON spr.SPONSOR_PROF_SETTING_GUID = sps.SPONSOR_PROF_SETTING_GUID
													INNER JOIN SPONSOR_USER AS su (nolock)
													ON su.SPONSOR_USER_GUID = spr.SPONSOR_USER_GUID
										WHERE	sp.CODE = 'STEM_TOTE_BAG_2010' AND
												sps.DESCRIPTION = 'City'
									) AS city
						ON city.SPONSOR_USER_GUID = street.SPONSOR_USER_GUID
							LEFT JOIN (
											SELECT spr.SPONSOR_USER_GUID, spr.SETTING_VALUE AS State
											FROM SPONSOR_PROF AS sp (nolock)
												INNER JOIN SPONSOR_PROF_SETTING AS sps (nolock)
												ON sps.SPONSOR_PROF_GUID = sp.SPONSOR_PROF_GUID
													INNER JOIN SPONSOR_PROF_RESP AS spr (nolock)
													ON spr.SPONSOR_PROF_SETTING_GUID = sps.SPONSOR_PROF_SETTING_GUID
														INNER JOIN SPONSOR_USER AS su (nolock)
														ON su.SPONSOR_USER_GUID = spr.SPONSOR_USER_GUID
											WHERE	sp.CODE = 'STEM_TOTE_BAG_2010' AND
													sps.DESCRIPTION = 'State'
										) AS state
							ON state.SPONSOR_USER_GUID = street.SPONSOR_USER_GUID
								LEFT JOIN (
												SELECT spr.SPONSOR_USER_GUID, spr.SETTING_VALUE AS Zip
												FROM SPONSOR_PROF AS sp (nolock)
													INNER JOIN SPONSOR_PROF_SETTING AS sps (nolock)
													ON sps.SPONSOR_PROF_GUID = sp.SPONSOR_PROF_GUID
														INNER JOIN SPONSOR_PROF_RESP AS spr (nolock)
														ON spr.SPONSOR_PROF_SETTING_GUID = sps.SPONSOR_PROF_SETTING_GUID
															INNER JOIN SPONSOR_USER AS su (nolock)
															ON su.SPONSOR_USER_GUID = spr.SPONSOR_USER_GUID
												WHERE	sp.CODE = 'STEM_TOTE_BAG_2010' AND
														sps.DESCRIPTION = 'Zip'
											) AS zip
								ON zip.SPONSOR_USER_GUID = street.SPONSOR_USER_GUID
									LEFT JOIN (
													SELECT spr.SPONSOR_USER_GUID, spr.SETTING_VALUE AS optInDEEmails
													FROM SPONSOR_PROF AS sp (nolock)
														INNER JOIN SPONSOR_PROF_SETTING AS sps (nolock)
														ON sps.SPONSOR_PROF_GUID = sp.SPONSOR_PROF_GUID
															INNER JOIN SPONSOR_PROF_RESP AS spr (nolock)
															ON spr.SPONSOR_PROF_SETTING_GUID = sps.SPONSOR_PROF_SETTING_GUID
																INNER JOIN SPONSOR_USER AS su (nolock)
																ON su.SPONSOR_USER_GUID = spr.SPONSOR_USER_GUID
													WHERE	sp.CODE = 'STEM_TOTE_BAG_2010' AND
															sps.DESCRIPTION LIKE '%redacted Education Emails%'
												) AS optIn
									ON optIn.SPONSOR_USER_GUID = street.SPONSOR_USER_GUID
					ORDER BY street.CREATE_DT DESC
				</cfquery>

			<cfelseif arguments.report eq "plushBlanket">
				<cfsavecontent variable="queryString">
					SELECT spb.*, sr.firstName, sr.lastName
					FROM STEMPlushBlanket AS spb (nolock)
						INNER JOIN STEMRegistrations AS sr (nolock)
						ON sr.registrationGUID = spb.registrationGUID
					ORDER BY spb.created DESC
				</cfsavecontent>

				<cfset GetCSVReportLowMemMode(fileName="STEMPlushBlanket.csv", queryString=queryString) />

			<cfelseif arguments.report eq "dryerasemarkers"><!--- just substitute whatever i.e. sharkusb --->
				<cfsavecontent variable="queryString">
				SELECT 
				r.lastname,r.firstname,
				p.street,p.city,p.state,p.zip,
				r.email,r.phone,convert(varchar(50),dtCreated,101) as DateCreated,c.title,c.body,
				r.schoolname,r.schoolcity
				
				FROM Sponsorships.dbo.cms_resources c
				inner join Sponsorships.dbo.stemRegistrations r on r.registrationguid = c.createdBy
				left join Sponsorships.dbo.STEMPlushBlanket p on p.registrationguid = r.registrationguid
				where c.dtCreated >= convert(date,'2012-09-27 00:00:00.000')  
				order by r.lastname,r.firstname
				</cfsavecontent>
					<!--- just substitute whatever filename i.e. sharkusbgiveaway.csv --->
				<cfset GetCSVReportLowMemMode(fileName="dryerasemarkergiveaway.csv", queryString=queryString) />
				
			<cfelseif arguments.report eq "judgeResults" AND StructKeyExists(arguments, "appType")>
				<cfset fileName = "STEMJudgeResults" & arguments.appType & ".csv" />

				<cfif not StructKeyExists(application, "judging")>
					<cfset application.judging = CreateObject("component", "cfcs.judging").Init() />
				</cfif>

				<cfset reportData = application.judging.GetFinalistsFull(arguments.appType) />
				<cfset application.judging.AttachJudgeScoresToQuery(reportData, arguments.appType) /> 

			</cfif>
		</cfif>

		<cfsetting enablecfoutputonly="true" />
		<cfcontent reset="true" />
		<cfcontent type="application/x-unknown" />
		<cfheader name="Content-Disposition" value="attachment;filename=#fileName#" />
		<cfoutput>#GenerateCSVString(reportData)#</cfoutput>
	</cffunction>


	<cffunction name="GetCSVReportLowMemMode" access="private" output="true" returntype="void" hint="maybe move to reportsEXTENDS.cfc">
		<cfargument name="fileName" type="string" required="true" />
		<cfargument name="queryString" type="string" required="true" hint="pass in sql string that would be used in SQL Server Management Studio" />

		<cfset var cols = [] />
		<cfset var colValue = "" />
		<cfset var i = 0 />
		<cfset var nl = chr(13) & chr(10) />
		<cfset var rowOutput = CreateObject("java", "java.lang.StringBuffer").init() />

		<cfset var conn = CreateObject("java", "coldfusion.server.ServiceFactory").getDatasourceService().getDatasource(variables.dsn).getConnection() />
		<cfset var stmt = conn.createStatement() />
		<cfset var resultSet = stmt.executeQuery(Trim(arguments.queryString)) />
		<cfset var md = resultSet.getMetaData() />
		<cfset var columnCount = md.getColumnCount() />

		<cfsetting enablecfoutputonly="true" />
		<cfcontent reset="true" />
		<cfcontent type="application/x-unknown" />
		<cfheader name="Content-Disposition" value="attachment;filename=#arguments.fileName#" />


		<cfloop from="1" to="#columnCount#" index="i">
			<cfset cols[i] = {} />
			<cfset cols[i].Name = md.getColumnName(i) />
			<cfset cols[i].TypeName = md.getColumnTypeName(i) />
		</cfloop>

		<cfloop from="1" to="#ArrayLen(cols)#" index="i">
			<cfset rowOutput.append(cols[i].Name) />
			<cfif i neq ArrayLen(cols)>
				<cfset rowOutput.append(",") />
			</cfif>
		</cfloop>
		<cfset rowOutput.append(nl) />
		<cfoutput>#rowOutput.toString()#</cfoutput>


		<cfloop condition="resultSet.next()">
			<cfset rowOutput.delete(0, rowOutput.length()) />
			<cfloop from="1" to="#ArrayLen(cols)#" index="i">
				<cfset colValue = JavaCast("string", resultSet.getObject(cols[i].Name)) />

				<cfif colValue eq "">
					<cfset colValue = " " />
				<cfelseif cols[i].TypeName eq "datetime">
					<cfset colValue = DateFormat(colValue, "mm/dd/yy") & " " & TimeFormat(colValue, "h:mm tt") />
				</cfif>

				<cfset rowOutput.append("""" & ReplaceNoCase(colValue,"""","'","all") & """") />
				<cfif i neq ArrayLen(cols)>
					<cfset rowOutput.append(",") />
				</cfif>
			</cfloop>
			<cfset rowOutput.append(nl) />
			<cfoutput>#rowOutput.toString()#</cfoutput>
		</cfloop>

		<cfset resultSet.close() />
		<cfset stmt.close() />
		<cfset conn.close() />
		<cfabort />

	</cffunction>


	<cffunction name="GetReportsFooterHTML" access="public" output="false" returntype="string">

		<cfset var html = "" />


		<cfsavecontent variable="html">
			</div>
		</cfsavecontent>

		<cfreturn html />
	</cffunction>


	<cffunction name="GetReportsHeaderHTML" access="public" output="false" returntype="string">

		<cfset var html = "" />


		<cfsavecontent variable="html">
			<div style="padding-left:20px;">
		</cfsavecontent>


		<cfreturn html />
	</cffunction>


	<cffunction name="GetReportsHTML" access="public" output="false" returntype="string">

		<cfset var getCount = "" />
		<cfset var html = "" />

		<cfoutput>
		<cfsavecontent variable="html">
			<div class="reportBox">
				<cfquery name="getCount" datasource="#variables.dsn#">
					SELECT	COUNT(*) AS cnt
					FROM STEMRegistrations AS sa (nolock)
				</cfquery>
				
				STEM - Total website registrations =
				<strong>#NumberFormat(getCount.cnt)#</strong>
				<div style="padding-left:100px;">
				&bull; <a href="/cfcs/reports/reports.cfc?method=GetCSVReport&report=allRegistrations">Click here</a> to download them all.
				</div>
				<br /><br />


				<cfquery name="getCount" datasource="#variables.dsn#">
					SELECT	COUNT(sa.applicationGUID) AS cnt
					FROM	STEMApplications AS sa (nolock)
						INNER JOIN STEMRegistrations AS sr (nolock)
						ON (
								sr.registrationGUID = sa.registrationGUID
							)
					WHERE sa.created > <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />
					<!--- SELECT count(applicationGUID) as cnt
					FROM STEMApplications WITH (NOLOCK)
					WHERE exists (Select 1 From STEMRegistrations a WITH (NOLOCK) Where a.registrationGUID = STEMApplications.registrationGUID)
					and allTabsCompleted IS NOT NULL
					and created >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />
					and allTabsCompleted >= <cfqueryparam value="#request.contestStart#" cfsqltype="cf_sql_date" />		 --->			
				</cfquery>				
				
				<nobr>STEM Institute/STARs (#DateFormat(request.contestStart)#) - Total applications =
				<strong>#NumberFormat(getCount.cnt)#</strong></nobr>
				<div style="padding-left:100px;">
				&bull; <a href="/cfcs/reports/reports.cfc?method=GetCSVReport&report=allApplications">Click here</a> to download them all.		
				</div>						
				<br /><br />
				
				<cfquery name="getCount" datasource="#variables.dsn#">
					SELECT count(applicationGUID) as cnt
					FROM STEMAppsJudgingApproved WITH (NOLOCK)					
				</cfquery>		
				
				<nobr>STEM Institute/STARs (#DateFormat(request.contestStart)#) - #request.contestYear# All Applications Approval =
				<strong>#NumberFormat(getCount.cnt)#</strong></nobr>
				<div style="padding-left:100px;">
				&bull; <a href="/cfcs/reports/reports.cfc?method=GetCSVReport&report=allApplicationsApproved">Click here</a> to download them all.		
				</div>						
				<br /><br />
				
				<nobr>STEM Institute/STARs (#DateFormat(request.contestStart)#) - #request.contestYear# Pre-Approval Stats =
				<strong>#NumberFormat(getCount.cnt)#</strong></nobr>
				<div style="padding-left:100px;">
				&bull; <a href="/cfcs/reports/reports.cfc?method=GetCSVReport&report=preapprovalstats">Click here</a> to download them all.		
				</div>						
				<br /><br />


				<!--- <cfquery name="getCount" datasource="#variables.dsn#">
					SELECT COUNT(*) AS cnt FROM STEMPlushBlanket (nolock)
				</cfquery>
				Total Plush Blanket entries =
				<strong>#NumberFormat(getCount.cnt)#</strong> <br />
				&bull; <a href="/cfcs/reports/reports.cfc?method=GetCSVReport&report=plushBlanket">Click here</a> to download them all.
				<br /><br /> --->
				
				<cfquery name="getCount" datasource="#variables.dsn#">
				SELECT COUNT(*) AS cnt FROM cms_resources
				where dtCreated >= convert(date,'2012-09-27 00:00:00.000')  
				</cfquery>
				
				Dry Erase Markers Giveaway - Total entries = <strong>#NumberFormat(getCount.cnt)#</strong>
				<div style="padding-left:100px;">
				&bull; <a href="/cfcs/reports/reports.cfc?method=GetCSVReport&report=dryerasemarkers">Click here</a> to download them all.	
				</div>			
				<br /><br />
				
				<!--- Total Shark USB Giveaway entries =
				<strong>#NumberFormat(getCount.cnt)#</strong> <br />
				&bull; <a href="/cfcs/reports/reports.cfc?method=GetCSVReport&report=sharkusb">Click here</a> to download all Shark USB Giveaway entries.
				<br /><br /> --->

				<!---
				Judge Results - Judge data will populate as they enter there scores.<br />
				&bull; <a href="/cfcs/reports/reports.cfc?method=GetCSVReport&report=judgeResults&appType=Institute">Click here</a> for Institute Judge results. <br />
				&bull; <a href="/cfcs/reports/reports.cfc?method=GetCSVReport&report=judgeResults&appType=STARs">Click here</a> for STARs Judge results.
				--->
			</div>

		</cfsavecontent>
		</cfoutput>

		<cfreturn html />
	</cffunction>


	<cffunction name="GetReportsLoginMessage" access="public" output="false" returntype="string">

		<cfset var html = "" />

		<cfsavecontent variable="html">
			<div class="reportBox">
				<strong style="color:#b70100;">To view this page you must be logged in as an internal redacted Education administrator.</strong> <br />
				&bull; Go to <a href="http://www.redactededucation.com" target="_DE">www.redactededucation.com</a> and login using your internal redacted Education administrator username and password. <br />
				&bull; Once logged in, you can <a href="index.cfm">refresh</a> this page to view these reports.
			</div>
		</cfsavecontent>

		<cfreturn html />
	</cffunction>


	<cffunction name="GetReportsScripts" access="public" output="false" returntype="string" hint="Any CSS or JS">

		<cfset var scripts = "" />

		<cfsavecontent variable="scripts">
			<style type="text/css">
				h3 {
					margin-bottom:0px;
				}

				div.reportBox {
					background-color:#ffffff;
					border:1px solid #cccccc;
					font-family:Verdana, Geneva, Arial, Helvetica, sans-serif;
					font-size:14px;
					margin:10px 0px 10px 0px;
					padding:15px;
					width:775px;
				}

				div.reportBox div.next, .reportWide div.next {
					font-size:0px;
					height:8px;
					line-height:0px;
					margin:0px;
					padding:0px;
					width:100%;
				}

				div.reportBox a {
					color:#369dd8;
				}

				div.reportBox div.statTitle {
					float:left;
					width:500px;
				}

				div.reportBox div.statResult {
					float:left;
					width:300px;
				}

				div.reportBox div.clear {
					clear:both;
					font-size:0px;
					height:0px;
					line-height:0px;
					margin:0px;
					padding:0px;
					width:100%;
				}
			</style>
		</cfsavecontent>

		<cfreturn scripts />
	</cffunction>


	<cffunction name="GetReportsTitle" access="public" output="false" returntype="string">
		<cfreturn "Reports" />
	</cffunction>

</cfcomponent>