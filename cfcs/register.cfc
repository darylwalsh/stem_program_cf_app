<cfcomponent extends="ajax" displayname="register">

	<!------------------------------>
	<!--- Public Functions       --->
	<!------------------------------>

	<cffunction access="public" name="emailError" output="false" returntype="void">
		<cfargument name="caught" type="any" required="true" />

		<!--- <cfset server.tools.DumpToFile(arguments.caught, "__CAUGHT") /> --->

		<!--- Only send at most 1 email per 15 minutes per server to prevent email flood --->
		<cfif not StructKeyExists(application, "lastErrorEmailed") OR not IsDate(application.lastErrorEmailed) OR DateDiff("n", application.lastErrorEmailed, Now()) gte 10>
			<cfmail from="stemError@redacted.com" to="simone_henry-cw@redacted.com" subject="Error on STEM Registration" type="html">
				<div style="margin:10px; padding:10px; width:1020px;">
					ERROR:
					<cfdump var="#arguments.caught#" />
					<br /><br />

					URL:
					<cfdump var="#url#" />
					<br /><br />

					FORM:
					<cfdump var="#form#" />
					<br /><br />

					CGI:
					<cfdump var="#cgi#" />
					<br /><br />
				</div>
			</cfmail>

			<cfset application.lastErrorEmailed = Now() />
		</cfif>

	</cffunction>


	<cffunction access="public" name="getRecommendationSheet" output="false" returntype="query">
		<cfargument name="applicationGUID" type="string" required="true" />

		<cfset var getSheets = "" />

		<cfquery name="getSheets" datasource="#application.dbinfo.strSponsorshipsDatasource#">
			SELECT * FROM STEMAppsRecommendationSheets (nolock)
			WHERE applicationGUID = <cfqueryparam value="#arguments.applicationGUID#" cfsqltype="cf_sql_varchar" />
			ORDER BY created DESC
		</cfquery>

		<cfreturn getSheets />
	</cffunction>


	<cffunction access="public" name="getRegistration" output="false" returntype="query">
		<cfargument name="registrationGUID" type="string" required="true" />

		<cfset var registration = "" />

		<cfquery name="registration" datasource="#application.dbinfo.strSponsorshipsDatasource#">
			SELECT * FROM STEMRegistrations (nolock)
			WHERE registrationGUID = <cfqueryparam value="#arguments.registrationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfreturn registration />
	</cffunction>


	<cffunction access="public" name="getReminderQuestions" output="false" returntype="query">
    	<cfargument name="SECURITY_QUESTION_GUID" type="string" required="no" hint="String input GUID">

		<cfset var questions = "" />

		<cfquery name="questions" datasource="#application.dbinfo.strGlobalDatasource#">
			SELECT QUESTION, SECURITY_QUESTION_GUID, LEGACY_ID
			FROM SECURITY_QUESTION  (nolock)
            <cfif arguments.SECURITY_QUESTION_GUID neq "">
				WHERE SECURITY_QUESTION_GUID = '#arguments.SECURITY_QUESTION_GUID#'
			</cfif>
            ORDER BY QUESTION
		</cfquery>

		<cfreturn questions />
	</cffunction>


	<cffunction access="public" name="getSTEMUser" output="false" returntype="query">
		<cfargument name="registrationGUID" type="string" required="true" />

		<cfset var getUser = "" />

		<cfquery name="getUser" datasource="#application.dbinfo.strSponsorshipsDatasource#">
			SELECT * FROM STEMRegistrations (nolock)
			WHERE registrationGUID = <cfqueryparam value="#arguments.registrationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfreturn getUser />
	</cffunction>


	<cffunction access="public" name="getUserTitles" output="false" returnType="query">

		<cfset var userTitles = "" />

        <cfquery name="userTitles" datasource="#application.dbinfo.strGlobalDatasource#">
			SELECT up.NAME dropdownName, up.PROPERTY_GUID, upd.DISPLAY_TEXT title_descrp, upd.VALUE, upd.USER_PROPERTY_DOM_VAL_GUID title_id
            FROM USER_PROPERTY up (nolock)
                INNER JOIN USER_PROPERTY_DOMAIN_VALUE upd (nolock)
				ON up.PROPERTY_GUID = upd.PROPERTY_GUID
            WHERE up.CODE = 'USERTITLES'
            ORDER BY CASE WHEN upd.DISPLAY_TEXT = 'Other' THEN 'aa' ELSE upd.DISPLAY_TEXT END
		</cfquery>

		<cfreturn userTitles />
	</cffunction>


	<cffunction access="public" name="isSTEMUserLoggedIn" output="false" returntype="boolean">
		<cfargument name="canForceProfile" type="boolean" required="false" default="false" />

		<cfset var checkForceProfile = "" />
		<cfset var checkForceProfileDate = "" />
		<cfset var isLoggedIn = false />

		<cfif StructKeyExists(client, "STEMRegistrationGUID") AND IsValid("guid", client.STEMRegistrationGUID) AND StructKeyExists(cookie, "FIRST_NAME")>

			<cfif arguments.canForceProfile AND FindNoCase("event=showRegister", cgi.query_string) lte 0>
				<cfquery name="checkForceProfile" datasource="#application.dbinfo.strSponsorshipsDatasource#">
					SELECT created, lastUpdated
					FROM STEMRegistrations (nolock)
					WHERE registrationGUID = <cfqueryparam value="#client.STEMRegistrationGUID#" cfsqltype="cf_sql_varchar" />
				</cfquery>

				<cfif IsDate(checkForceProfile.lastUpdated)>
					<cfset checkForceProfileDate = checkForceProfile.lastUpdated />
				<cfelseif IsDate(checkForceProfile.created)>
					<cfset checkForceProfileDate = checkForceProfile.created />
				</cfif>

				<cfif not IsDate(checkForceProfileDate) OR DateCompare(request.contestStart, checkForceProfileDate) gte 0>
					<cfif FindNoCase("event=getFile", cgi.query_string) gt 0>
						<cflocation url="/index.cfm?event=showRegister&up=1&forwardURL=#URLEncodedFormat(cgi.HTTP_REFERER)#" addtoken="false" />
					<cfelse>
						<cflocation url="/index.cfm?event=showRegister&up=1&forwardURL=#cgi.script_name##URLEncodedFormat('?')##cgi.query_string#" addtoken="false" />
					</cfif>

				</cfif>
			</cfif>

			<cfset isLoggedIn = true />
		</cfif>

		<cfreturn isLoggedIn />
	</cffunction>


	<cffunction access="public" name="loginUser" returntype="boolean">
		<cfargument name="email" type="string" required="true" />
		<cfargument name="password" type="string" required="true" />

		<cfset var findUser = "" />
		<cfset var userLoggedIn = false />

		<cfquery name="findUser" datasource="#application.dbinfo.strSponsorshipsDatasource#">
			SELECT * FROM STEMRegistrations (nolock)
			WHERE 	username = <cfqueryparam value="#arguments.email#" cfsqltype="cf_sql_varchar" /> AND
					password = <cfqueryparam value="#arguments.password#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfif findUser.RecordCount gt 0>
			<cfset client.STEMRegistrationGUID = findUser.registrationGUID />

			<cfcookie name="SPONSOR_USER_GUID" value="#findUser.registrationGUID#" />
			<cfcookie name="registrationGUID" value="#findUser.registrationGUID#" />
			<cfcookie name="FIRST_NAME" value="#findUser.firstName#" />
			<cfcookie name="LAST_NAME" value="#findUser.lastName#" />
			<cfcookie name="EMAIL" value="#findUser.email#"  />

			<cfset userLoggedIn = true />
		</cfif>

		<cfreturn userLoggedIn />
	</cffunction>


	<cffunction access="public" name="logoutUser" returntype="void">

		<cfset client.STEMRegistrationGUID = "" />
		<cfset StructDelete(client, "STEMRegistrationGUID") />

	</cffunction>


	<cffunction access="public" name="saveRecommendationSheet" output="false" returntype="void">
		<cfargument name="aform" type="struct" required="true" />

		<cfset var save = "" />
		<cfset var stemApplicant = "" />

		<cfquery name="save" datasource="#application.dbinfo.strSponsorshipsDatasource#">
			INSERT INTO STEMAppsRecommendationSheets (
														applicationGUID, firstName, lastName, title, institution, street, street2, city, state, zip, phone, email,
														capacityKnowApplicant, describeApplicant,
														categoryClassroomInstructionalSkills, categorySubjectMatterKnowledge, categoryInitiativeAndSelfReliance,
														categoryCreativity, categoryIntegrity, categoryLeadershipSkills, categoryInterpersonalSkills,
														categoryOralCommunicationSkills, categoryWrittenCommunicationSkills, categoryTeamworkSkills
													)
			VALUES (
						<cfqueryparam value="#arguments.aform.applicationGUID#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.firstName#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.lastName#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.title#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.institution#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.street#" cfsqltype="cf_sql_varchar" />,
						<cfif StructKeyExists(arguments.aform, "street2") and arguments.aform.street2 neq "">
							<cfqueryparam value="#arguments.aform.street2#" cfsqltype="cf_sql_varchar" />,
						<cfelse>
							<cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />,
						</cfif>
						<cfqueryparam value="#arguments.aform.city#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.state#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.zip#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.phone#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.email#" cfsqltype="cf_sql_varchar" />,

						<cfqueryparam value="#arguments.aform.capacityKnowApplicant#" cfsqltype="cf_sql_longvarchar" />,
						<cfqueryparam value="#arguments.aform.describeApplicant#" cfsqltype="cf_sql_longvarchar" />,

						<cfqueryparam value="#arguments.aform.categoryClassroomInstructionalSkills#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.categorySubjectMatterKnowledge#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.categoryInitiativeAndSelfReliance#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.categoryCreativity#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.categoryIntegrity#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.categoryLeadershipSkills#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.categoryInterpersonalSkills#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.categoryOralCommunicationSkills#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.categoryWrittenCommunicationSkills#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.aform.categoryTeamworkSkills#" cfsqltype="cf_sql_varchar" />
					);
		</cfquery>

		<cfquery name="stemApplicant" datasource="Sponsorships">
			SELECT sr.firstName, sr.lastName, sr.email
			FROM STEMApplications AS sa (nolock)
				INNER JOIN STEMRegistrations AS sr (nolock)
				ON sr.registrationGUID = sa.registrationGUID
			WHERE sa.applicationGUID = <cfqueryparam value="#arguments.aform.applicationGUID#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfmail from="siemensstemacademy@redacted.com" to="#arguments.aform.email#" subject="STEM Recommendation Rating Sheet" type="html">
			<div style="margin:10px; width:900px;">
				Dear Recommender,
				<br /><br />

				Thank you for completing and submitting the recommendation form for <cfoutput>#stemApplicant.firstName# #stemApplicant.lastName#</cfoutput>
				and supporting their application for the #request.contestYear# Siemens Summer of Learning professional development programs.
				Your input is greatly appreciated.
				<br /><br />

				If you have any questions, please email <a href="mailto:siemensstemacademy@redacted.com">siemensstemacademy@redacted.com</a>.
				<br /><br />

				Best,
				<br /><br />
				The Siemens Summer of Learning Team
			</div>
		</cfmail>

		<cfmail from="siemensstemacademy@redacted.com" to="#stemApplicant.email#" subject="STEM Recommendation Rating Sheet" type="html">
			<div style="margin:10px; width:900px;">
				Dear Fellow,
				<br /><br />

				This email is being sent to notify you that your recommender has completed and submitted the recommendation form
				for your application to the #request.contestYear# Siemens Summer of Learning professional development program(s). Their input will
				be taken into consideration when evaluating your application.
				<br /><br />

				If you have any questions, please email <a href="mailto:siemensstemacademy@redacted.com">siemensstemacademy@redacted.com</a>.
				<br /><br />

				Good luck!
				<br /><br />

				The Siemens Summer of Learning Team
			</div>
		</cfmail>


	</cffunction>



	<!------------------------------>
	<!--- Remote Functions       --->
	<!------------------------------>

	<cffunction access="remote" name="emailExists" output="false" returntype="boolean" returnformat="plain">
		<cfargument name="email" type="string" required="true" />

		<cfset var check = "" />
		<cfset var exists = false />


		<cfquery name="check" datasource="#application.dbinfo.strSponsorshipsDatasource#">
			SELECT email FROM STEMRegistrations (nolock)
			WHERE email = <cfqueryparam value="#arguments.email#" cfsqltype="cf_sql_varchar" />
					<cfif StructKeyExists(client, "STEMRegistrationGUID") AND IsValid("guid", client.STEMRegistrationGUID)>
						AND registrationGUID != <cfqueryparam value="#client.STEMRegistrationGUID#" cfsqltype="cf_sql_varchar" />
					</cfif>
		</cfquery>

		<cfif check.RecordCount gt 0>
			<cfset exists = true />
		</cfif>

		<cfreturn exists />
	</cffunction>


	<cffunction access="remote" name="ForgotPass" output="true" returntype="void">
		<cfargument name="email" type="string" required="true" />

		<cfset var check = "" />

		<cfquery name="check" datasource="#application.dbinfo.strSponsorshipsDatasource#">
			SELECT username AS uid, password AS pwd
			FROM STEMRegistrations (nolock)
			WHERE email = <cfqueryparam value="#arguments.email#" cfsqltype="cf_sql_varchar" />
		</cfquery>

		<cfif check.RecordCount gte 1>
            success
            <cfmail type="text/plain" from="info@redactededucation.com" to="#arguments.email#" subject="Siemens STEM Academy - Forgotten Username/Password">Your username is: #check.uid#
Your password is: #check.pwd#</cfmail>
		<cfelse>
            failed
		</cfif>
	</cffunction>


	<cffunction access="remote" name="registerUser" output="false" returntype="boolean" returnformat="plain">
		<cfargument name="APP_USER_GUID" type="string" required="false" default="" />
		<cfargument name="email" type="string" required="true" />
		<cfargument name="password" type="string" required="true" />
		<cfargument name="firstName" type="string" required="true" />
		<cfargument name="lastName" type="string" required="true" />
		<cfargument name="title" type="string" required="true" />
		<cfargument name="areaPhone" type="string" required="true" />
		<cfargument name="prePhone" type="string" required="true" />
		<cfargument name="phone" type="string" required="true" />
		<cfargument name="schoolName" type="string" required="true" />
		<cfargument name="schoolAddress" type="string" required="true" />
		<cfargument name="schoolAddress2" type="string" required="false" default="" />
		<cfargument name="schoolCity" type="string" required="true" />
		<cfargument name="schoolState" type="string" required="true" />
		<cfargument name="schoolZipCode" type="string" required="true" />
		<cfargument name="schoolPhone" type="string" required="true" />
		<cfargument name="schoolCountry" type="string" required="true" />
		<cfargument name="locationType" type="string" required="true" />
		<cfargument name="grades" type="string" required="true" />
		<cfargument name="subjects" type="string" required="true" />
		<cfargument name="studentCount" type="string" required="true" />
		<cfargument name="yearsTeaching" type="string" required="true" />
		<cfargument name="SECURITY_QUESTION_GUID" type="string" required="true" />
		<cfargument name="SECURITY_QUESTION_ANSWER" type="string" required="true" />
		<cfargument name="de_updates" type="string" default="0" required="false" />


		<cfset var insertUser = "" />
		<cfset var blnError = false />
		<cfset var updateUser = "" />

		<cftry>
			<cfif NOT StructKeyExists(arguments, "registrationGUID") OR NOT IsValid("guid", arguments.registrationGUID)>
				<cfquery name="insertUser" datasource="#application.dbinfo.strSponsorshipsDatasource#">
					INSERT INTO STEMRegistrations ( APP_USER_GUID, created, lastUpdated, username, password,
													firstName, lastName, title, email, phone,
													PID, schoolName, schoolAddress, schoolAddress2, schoolCity, schoolState, schoolZipCode, schoolPhone, schoolCountry,
													locationType,
													gradeK, grade1, grade2, grade3, grade4, grade5, grade6, grade7, grade8, grade9, grade10, grade11, grade12,
													subjectScience, subjectTechnology, subjectEngineering, subjectMath, subjectIntegrated, subjectOther,
													studentCount, yearsTeaching, SECURITY_QUESTION_GUID, SECURITY_QUESTION_ANSWER, de_updates
					)
					VALUES (
						<cfif IsValid("guid", arguments.APP_USER_GUID)>
							<cfqueryparam value="#arguments.APP_USER_GUID#" cfsqltype="cf_sql_varchar" />,
						<cfelse>
							<cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />,
						</cfif>
						<cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
						<cfqueryparam value="" null="true" cfsqltype="cf_sql_timestamp" />,
						<cfqueryparam value="#arguments.email#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.password#" cfsqltype="cf_sql_varchar" />,

						<cfqueryparam value="#arguments.firstName#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.lastName#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.title#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.email#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.areaPhone#-#arguments.prePhone#-#arguments.phone#" cfsqltype="cf_sql_varchar" />,

						<cfqueryparam value="#arguments.PID#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.schoolName#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.schoolAddress#" cfsqltype="cf_sql_varchar" />,
						<cfif StructKeyExists(arguments, "schoolAddress2")>
							<cfqueryparam value="#arguments.schoolAddress2#" cfsqltype="cf_sql_varchar" />,
						<cfelse>
							<cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />,
						</cfif>
						<cfqueryparam value="#arguments.schoolCity#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.schoolState#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.schoolZipCode#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.schoolPhone#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.schoolCountry#" cfsqltype="cf_sql_varchar" />,

						<cfqueryparam value="#arguments.locationType#" cfsqltype="cf_sql_varchar" />,

						<cfif ListFindNoCase(arguments.grades, "gradeK")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade1")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade2")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade3")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade4")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade5")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade6")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade7")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade8")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade9")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade10")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade11")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade12")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>

						<cfif ListFindNoCase(arguments.subjects, "Science")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Technology")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Engineering")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Math")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Integrated")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Other")>
							<cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							<cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>

						<cfqueryparam value="#arguments.studentCount#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.yearsTeaching#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.SECURITY_QUESTION_GUID#" cfsqltype="cf_sql_varchar" />,
						<cfqueryparam value="#arguments.SECURITY_QUESTION_ANSWER#" cfsqltype="cf_sql_varchar" />,
                              <cfqueryparam value="#arguments.de_updates#" cfsqltype="cf_sql_bit" />
					)
				</cfquery>

			<cfelse>

				<cfquery name="updateUser" datasource="#application.dbinfo.strSponsorshipsDatasource#">
					UPDATE STEMRegistrations
					SET
						lastUpdated = <cfqueryparam value="#Now()#" cfsqltype="cf_sql_timestamp" />,
						username = <cfqueryparam value="#arguments.email#" cfsqltype="cf_sql_varchar" />,
						password = <cfqueryparam value="#arguments.password#" cfsqltype="cf_sql_varchar" />,
						firstName = <cfqueryparam value="#arguments.firstName#" cfsqltype="cf_sql_varchar" />,
						lastName = <cfqueryparam value="#arguments.lastName#" cfsqltype="cf_sql_varchar" />,
						title = <cfqueryparam value="#arguments.title#" cfsqltype="cf_sql_varchar" />,
						email = <cfqueryparam value="#arguments.email#" cfsqltype="cf_sql_varchar" />,
						phone = <cfqueryparam value="#arguments.areaPhone#-#arguments.prePhone#-#arguments.phone#" cfsqltype="cf_sql_varchar" />,

						PID = <cfqueryparam value="#arguments.PID#" cfsqltype="cf_sql_varchar" />,
						schoolName = <cfqueryparam value="#arguments.schoolName#" cfsqltype="cf_sql_varchar" />,
						schoolAddress = <cfqueryparam value="#arguments.schoolAddress#" cfsqltype="cf_sql_varchar" />,
						<cfif StructKeyExists(arguments, "schoolAddress2")>
							schoolAddress2 = <cfqueryparam value="#arguments.schoolAddress2#" cfsqltype="cf_sql_varchar" />,
						<cfelse>
							schoolAddress2 = <cfqueryparam value="" null="true" cfsqltype="cf_sql_varchar" />,
						</cfif>
						schoolCity = <cfqueryparam value="#arguments.schoolCity#" cfsqltype="cf_sql_varchar" />,
						schoolState = <cfqueryparam value="#arguments.schoolState#" cfsqltype="cf_sql_varchar" />,
						schoolZipCode = <cfqueryparam value="#arguments.schoolZipCode#" cfsqltype="cf_sql_varchar" />,
						schoolPhone = <cfqueryparam value="#arguments.schoolPhone#" cfsqltype="cf_sql_varchar" />,
						schoolCountry = <cfqueryparam value="#arguments.schoolCountry#" cfsqltype="cf_sql_varchar" />,

						locationType = <cfqueryparam value="#arguments.locationType#" cfsqltype="cf_sql_varchar" />,

						<cfif ListFindNoCase(arguments.grades, "gradeK")>
							gradeK = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							gradeK = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade1")>
							grade1 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade1 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade2")>
							grade2 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade2 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade3")>
							grade3 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade3 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade4")>
							grade4 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade4 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade5")>
							grade5 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade5 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade6")>
							grade6 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade6 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade7")>
							grade7 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade7 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade8")>
							grade8 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade8 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade9")>
							grade9 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade9 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade10")>
							grade10 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade10 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade11")>
							grade11 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade11 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.grades, "grade12")>
							grade12 = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							grade12 = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>

						<cfif ListFindNoCase(arguments.subjects, "Science")>
							subjectScience = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							subjectScience = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Technology")>
							subjectTechnology = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							subjectTechnology = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Engineering")>
							subjectEngineering = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							subjectEngineering = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Math")>
							subjectMath = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							subjectMath = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Integrated")>
							subjectIntegrated = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							subjectIntegrated = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>
						<cfif ListFindNoCase(arguments.subjects, "Other")>
							subjectOther = <cfqueryparam value="1" cfsqltype="cf_sql_bit" />,
						<cfelse>
							subjectOther = <cfqueryparam value="0" cfsqltype="cf_sql_bit" />,
						</cfif>

						studentCount = <cfqueryparam value="#arguments.studentCount#" cfsqltype="cf_sql_varchar" />,
						yearsTeaching = <cfqueryparam value="#arguments.yearsTeaching#" cfsqltype="cf_sql_varchar" />,
						SECURITY_QUESTION_GUID = <cfqueryparam value="#arguments.SECURITY_QUESTION_GUID#" cfsqltype="cf_sql_varchar" />,
						SECURITY_QUESTION_ANSWER = <cfqueryparam value="#arguments.SECURITY_QUESTION_ANSWER#" cfsqltype="cf_sql_varchar" />,
                              de_updates = <cfqueryparam value="#arguments.de_updates#" cfsqltype="cf_sql_bit" />

					WHERE registrationGUID = <cfqueryparam value="#arguments.registrationGUID#" cfsqltype="cf_sql_varchar" />
				</cfquery>

			</cfif>

			<cfset loginUser(arguments.email, arguments.password) />

			<cfcatch>
				<cfset blnError = true />
				<cfset emailError(cfcatch) />
			</cfcatch>
		</cftry>


		<cfreturn blnError />
	</cffunction>


</cfcomponent>